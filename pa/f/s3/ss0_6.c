
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 -- System Stream Function Definitions
//
//     System Stream Function 4.
//
//
//
//

// Initial version derived from ../../f/s3/ss0.c Revision 1.12.

// Specific ASP usage requires inter-file symbol definitions ...
#include "noasp.h"

// ... end of specific usage definitions.

#include <std.h>
#include <alg.h>
#include <logp.h>

#include  <std.h>
#include <xdas.h>

#include <acp.h>
#include <acp_mds.h>

#include <acptype.h>
#include <acpbeta.h>
#include <acperr.h>

#include <thxsys.h>

#include <ss0.h>

//
// systemStream4 : audio stream control functions (sub)
//
//   Automatic selection of BGC or BPLM based on whether the subwoofer 
//   is of type THX Ultra 2 specified.
//    
//   Automatic selection of the distance mode for ASA 3 cases:
//   1. Back speakers < 12 ft apart
//   2. Back speakers between 12 and 48 ft apart
//   3. Back speakers > 48 ft apart
//

Int
systemStream4Compute (const PAF_SST_Params *pP, PAF_SST_Config *pC, LgUns x[])
{
    PAF_SystemStatus *pStatus = pC->pStatus;

#ifndef NOBGC
#ifndef NOBC
    {
        Int bplMode=0, bgcUse=0, bplStatus = 0;
        if((pStatus->speakerSubw & 0x80)== PAF_SYS_SPEAKERFREQ_THX_ULTRA2)
        {
            bplMode = 0;
            bgcUse = 1;
        }
        else if((pStatus->speakerSubw & 0x40)== PAF_SYS_SPEAKERFREQ_THX_NOULTRA2) 
        {
            bplMode = 1;
            bgcUse = 0x0;
            bplStatus = 1;
        }
        
        x[0] = bplMode;
        x[1] = bgcUse;
        x[2] = bplStatus;
    }
#endif // NOBC
#endif // NOBGC

#ifndef NOASA
    {
        ACP_Unit distance =0;
        if((pStatus->speakerBack & 0xc0) == PAF_SYS_SPEAKER_DISTANCE_NEAR)
            distance = 0;
        if((pStatus->speakerBack & 0xc0) == PAF_SYS_SPEAKER_DISTANCE_FAR)
            distance = 1;
        if((pStatus->speakerBack & 0xc0) == PAF_SYS_SPEAKER_DISTANCE_VERYFAR)
            distance = 2;    

        x[3] = distance;
    }
#endif // NOASA

    return 0;
}

Int
systemStream4Transmit (const PAF_SST_Params *pP, PAF_SST_Config *pC, LgUns x[])
{
    Int ss = pP->ss;

    ACP_Handle acp = pC->acp;

#ifndef NOBGC
#ifndef NOBC
    {
        Int errno;
        Int bplMode, bgcUse, bplStatus;
        ACP_Unit fromBpl[3];
        ACP_Unit fromBgc[4];
        ACP_Unit fromVol[4],toVol[4];

        bplMode = x[0];
        bgcUse = x[1];
        bplStatus= x[2];

        fromBpl[0] = 0xc902;
        fromBpl[1] = 0xca49;
        fromBpl[2] = 0x0400 + bplMode;
        
        if (errno = acp->fxns->sequence(acp, fromBpl, NULL)) {
            if (errno == ACPERR_APPLY_NOBETA) {
                // Return without reporting to trace Log if BPLM is not
                // part of the stream.
                    return 1;
            }
            else {
                LOG_printf(&trace, "SS%d: BPLM sequence processing error (0x%04x)", 
                    ss, errno);
            }
        }

        fromBgc[0] = 0xc902;
        fromBgc[1] = 0xca48;
        fromBgc[2] = 0x0600 + bgcUse;
        if (errno = acp->fxns->sequence(acp, fromBgc, NULL)) {
            if (errno == ACPERR_APPLY_NOBETA) {
                // Return without reporting to trace Log if BGC is not
                // part of the stream.
                    return 1;
            }
            else {
                LOG_printf(&trace, "SS%d: BGC sequence processing error (0x%04x)", 
                    ss, errno);
            }
        }

        if(bplStatus)
        {
            fromVol[0] = 0xc902;
            fromVol[1] = 0xc326;
            fromVol[2] = 0x007c;
        
            if (errno = acp->fxns->sequence(acp, fromVol, toVol)) {
                if (errno == ACPERR_APPLY_NOBETA) {
                    // Return without reporting to trace Log if VOL is not
                    // part of the stream.
                    return 1;
                }
                else {
                    LOG_printf(&trace, "SS%d: VOL sequence processing error (0x%04x)", 
                        ss, errno);
                }
            }
            fromBpl[0] = 0xc902;
            fromBpl[1] = 0xca49;
            fromBpl[2] = 0x0800 + (((char)(toVol[3])) & 0xff);    
            if (errno = acp->fxns->sequence(acp, fromBpl, NULL))
                LOG_printf(&trace, "SS%d: BPLM sequence processing error "
                    "(0x%04x)", ss, errno);    
        }
    }
#endif // NOBC
#endif // NOBGC

#ifndef NOASA
    {
        Int errno;
        ACP_Unit distance;
        ACP_Unit fromAsa[3];

        distance = x[3];

        fromAsa[0] = 0xc902;
        fromAsa[1] = 0xca4d;
        fromAsa[2] = 0x0600 + distance;
        if (errno = acp->fxns->sequence(acp, fromAsa, NULL)) {
            if (errno == ACPERR_APPLY_NOBETA) {
                // Return without reporting to trace Log if ASA is not
                // part of the stream.
			    return 1;
            }
			else {
                LOG_printf(&trace, "SS%d: ASA sequence processing error (0x%04x)", 
                    ss, errno);
            }
        }
    }
#endif // NOASA

    return 0;
}

