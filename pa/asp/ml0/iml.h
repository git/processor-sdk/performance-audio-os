
/*
* Copyright (C) 2004-2017 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common MIPS Load Demonstration algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the MIPS Load Demonstration algorithm.
 */
#ifndef IML_
#define IML_

#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include <ti/xdais/xdas.h>

#include "icom.h"
#include "paftyp.h"

/*
 *  ======== IML_Obj ========
 *  Every implementation of IML *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IML_Obj {
    struct IML_Fxns *fxns;    /* function list: standard, public, private */
} IML_Obj;

/*
 *  ======== IML_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IML_Obj *IML_Handle;

/*
 *  ======== IML_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.  This structure is actually
 *  instantiated and initialized in iml.c.
 */
typedef volatile struct IML_Status {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 unused;
    XDAS_UInt16 count;
} IML_Status;

/*
 *  ======== IML_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IML_Config {
    Int dummy;
} IML_Config;

/*
 *  ======== IML_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a ML object.
 *
 *  Every implementation of IML *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 *  This structure is actually instantiated and initialized in iml.c.
 */
typedef struct IML_Params {
    Int size;
    const IML_Status *pStatus;
    IML_Config config;
} IML_Params;

/*
 *  ======== IML_PARAMS ========
 *  Default instance creation parameters (defined in iml.c)
 */
extern const IML_Params IML_PARAMS;

/*
 *  ======== IML_Fxns ========
 *  All implementation's of ML must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is ML_<vendor>_IML, where
 *  <vendor> is the vendor name.
 */
typedef struct IML_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IML_Handle, PAF_AudioFrame *);
    Int         (*apply)(IML_Handle, PAF_AudioFrame *);
    /* private */
} IML_Fxns;

/*
 *  ======== IML_Cmd ========
 *  The Cmd enumeration defines the control commands for the ML
 *  control method.
 */
typedef enum IML_Cmd {
    IML_NULL                    = ICOM_NULL,
    IML_GETSTATUSADDRESS1       = ICOM_GETSTATUSADDRESS1,
    IML_GETSTATUSADDRESS2       = ICOM_GETSTATUSADDRESS2,
    IML_GETSTATUS               = ICOM_GETSTATUS,
    IML_SETSTATUS               = ICOM_SETSTATUS
} IML_Cmd;

#endif  /* IML_ */
