
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library top-level header file
//
// All rights reserved.
//
//

#if (((PAF_DEVICE&0xFF000000) == 0xD8000000) ||  \
     ((PAF_DEVICE&0xFF000000) == 0xDD000000) ||  \
     ((PAF_DEVICE&0xFF000000) == 0xDA000000))

#include <pafhjt.h>

#else  // PAF_DEVICE != 0xD8000000 && != 0xDD000000

#ifndef CPL_
#define CPL_

#include "cpl_vecops.h"

#ifndef CPL_VECOPS_ONLY
#include "cpl_cdm.h"
#include "cpl_fft.h"
#include "cpl_dec.h"
#include "cpl_moddemod.h"
#include "cpl_window.h"
#include "cpl_fifo.h"

#include "cpl_cdm_fxns.h"
#include "cpl_delay.h"
#endif /* CPL_VECOPS_ONLY */

#define CPL_CALL(x) CPL_##x

#endif

#endif // PAF_DEVICE = 0xD8000000 ---------------------------------------------
