/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  IDEL default instance creation parameters
 */
#include <xdc\std.h>

#include <idel.h>

#include <paftyp.h>

/*
 *  ======== IDEL_PARAMS_STATUS_10CH ========
 *  This static initialization defines the default parameters used to
 *  create instances of DEL objects - 10-channels.
 */

const IDEL_Status IDEL_PARAMS_STATUS_10CH = {
    sizeof(IDEL_Status), /* size */
    1, /* mode */
    1, /* unit */
    PAF_MAXNUMCHAN, 10, /* numc, nums */
    0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 0, 0, 0, /* delay */
    0, /* masterDelay */
};

/*
 *  ======== IDEL_Config10 ========
 */

typedef struct IDEL_Config10 {
    Int size;
	XDAS_UInt16 sizeofScrach;
	XDAS_UInt8 useDATCopy;
	XDAS_UInt8 delCommonMemNumber;
	XDAS_UInt16 unused;
    union {
        PAF_ChannelMask bits;
        XDAS_Int32 not;
    } mask;
    PAF_DelayState state[10];
} IDEL_Config10;

/*
 *  ======== IDEL_PARAMS_CONFIG_WIDE ========
 *  This static initialization defines the default parameters used to
 *  create instances of DEL objects - 10-channels (including LWID and RWID).
 */

const IDEL_Config10 IDEL_PARAMS_CONFIG_WIDE = {
    sizeof(IDEL_Config10), /* "size" of the structure */
    
	/* "sizeofScratch" buffer when "useDATCopy" = 1. This will not be allocated when "useDATCopy" = 0 ! */
	256*4,                
	
	/* "useDATCopy" when set to 1, will make use of DMA to transfer between external and internal RAM.*/
	1,
	
	/* "delCommonMemNumber" is the number of the desired COMMON_MEMORY space where delay samples are stored */
	5,                    
	/* unused */
	0,
    (1<<PAF_LEFT)|(1<<PAF_RGHT)|(1<<PAF_CNTR)|(1<<PAF_LSUR)|(1<<PAF_RSUR)|(1<<PAF_LBAK)|(1<<PAF_RBAK)|(1<<PAF_SUBW)
	|(1<<PAF_LWID|(1<<PAF_RWID)),
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
};

/*
 *  ======== IDEL_PARAMS_WIDE ========
 *  Params structure for 10 channels (including LWID and RWID).
 */

const IDEL_Params IDEL_PARAMS_WIDE = {
    sizeof(IDEL_Params), /* size */
    &IDEL_PARAMS_STATUS_10CH, /* pStatus */
    (const IDEL_Config *)&IDEL_PARAMS_CONFIG_WIDE, /* pConfig */
};

/*
 *  ======== IDEL_PARAMS_CONFIG_HGHT ========
 *  This static initialization defines the default parameters used to
 *  create instances of DEL objects - 10-channels (including LHED and RHED).
 */

const IDEL_Config10 IDEL_PARAMS_CONFIG_HGHT = {
    sizeof(IDEL_Config10), /* size */
    /* "sizeofScratch" buffer when "useDATCopy" = 1. This will not be allocated when "useDATCopy" = 0 ! */
	256*4,                
	
	/* "useDATCopy" when set to 1, will make use of DMA to transfer between external and internal RAM.*/
	1,
	
	/* "delCommonMemNumber" is the number of the desired COMMON_MEMORY space where delay samples are stored */
	5,                    
	
	/* unused */
	0,
    (1<<PAF_LEFT)|(1<<PAF_RGHT)|(1<<PAF_CNTR)|(1<<PAF_LSUR)|(1<<PAF_RSUR)|(1<<PAF_LBAK)|(1<<PAF_RBAK)|(1<<PAF_SUBW)
	|(1<<PAF_LHED|(1<<PAF_RHED)),
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
};

/*
 *  ======== IDEL_PARAMS_HGHT ========
 *  Params structure for 10 channels (including LHED and RHED).
 */

const IDEL_Params IDEL_PARAMS_HGHT = {
    sizeof(IDEL_Params), /* size */
    &IDEL_PARAMS_STATUS_10CH, /* pStatus */
    (const IDEL_Config *)&IDEL_PARAMS_CONFIG_HGHT, /* pConfig */
};


/*
 *  ======== IDEL_PARAMS_12CH ========
 *  This static initialization defines the parameters used to create 
 *  instances of DEL objects - 12-channels.
 */

const IDEL_Status IDEL_PARAMS_STATUS_12CH = {
    sizeof(IDEL_Status), /* size */
    1, /* mode */
    1, /* unit */
    PAF_MAXNUMCHAN, 12, /* numc, nums */
    0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 0, 0, 0, /* delay */
    0, /* masterDelay */
};

typedef struct IDEL_Config12 {
    Int size;
	XDAS_UInt16 sizeofScrach;
	XDAS_UInt8 useDATCopy;
	XDAS_UInt8 delCommonMemNumber;
	XDAS_UInt16 unused;
    union {
        PAF_ChannelMask bits;
        XDAS_Int32 not;
    } mask;
    PAF_DelayState state[12];
} IDEL_Config12;

const IDEL_Config12 IDEL_PARAMS_CONFIG_12CH = {
    sizeof(IDEL_Config12), /* size */
	/* "sizeofScratch" buffer when "useDATCopy" = 1. This will not be allocated when "useDATCopy" = 0 ! */
	256*4,                
	
	/* "useDATCopy" when set to 1, will make use of DMA to transfer between external and internal RAM.*/
	1,
	
	/* "delCommonMemNumber" is the number of the desired COMMON_MEMORY space where delay samples are stored */
	5,                    
	
	/* unused */
	0,
    (1<<PAF_LEFT)|(1<<PAF_RGHT)|(1<<PAF_CNTR)|(1<<PAF_LSUR)|(1<<PAF_RSUR)|(1<<PAF_LBAK)|(1<<PAF_RBAK)|(1<<PAF_SUBW)
	|(1<<PAF_LWID)|(1<<PAF_RWID)|(1<<PAF_LHED|(1<<PAF_RHED)),
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
    0, 0, 15*48, 0, NULL, 0, 
};
const IDEL_Params IDEL_PARAMS_12CH = {
    sizeof(IDEL_Params), /* size */
    &IDEL_PARAMS_STATUS_12CH, /* pStatus */
    (const IDEL_Config *)&IDEL_PARAMS_CONFIG_12CH, /* pConfig */
};

