/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  

// std headers
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <clk.h>
#include <log.h>
#include  <xdc/std.h>
#include <ti/xdais/xdas.h>
#include <paftyp.h>

// FIL headers
#include <ifil.h>
#include <fil_tii.h>
#include <fil_tii_priv.h>
#include <filerr.h>
#include <filextern.h>
#include <filtyp.h>
#include <filters.h>
#include <fil_table.h>

// Test table structure
typedef struct 
{
    FIL_filterFnPtr filterFn; // Function ptr that is tested
    Uint type;                // type field that will call the function, in FIL
    Int  taps;                // Taps per cascade
    Int  ch;                  // Channels
    Int  uniCoef;             // Unicoef(1) or not(0)
    Int  inplace;             // Inplace(1) or not(0)
    Int  dPrec;               // Input/Output data precision is SP(1) or DP(0)
    Int  cPrec;               // Coefficient data precision is SP(1) or DP(0)
    Int  cascade;             // No of cascades - 1. ie if 4 taps, then this field is 1 (2(cascades) - 1)
} testTable;
                                        
#define FIL_TEST_TOTAL         (384)

testTable gTestArray[FIL_TEST_TOTAL] = {

// --------------------------Non-Unicoef & Non-Inplace -------------------------------------
	 // fiterFn			 type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
{Filter_iirT2Ch2_c2_i_df2, 0,      2,     2,      0,         1,      1,        1,      1   }, // 0
{Filter_iirT2Ch2_c2_i_df2, 0,      2,     2,      1,         1,      1,        1,      1   },
{Filter_iirT2Ch2_c3_u_df2, 0,      2,     2,      1,         0,      1,        1,      2   }, 
{Filter_iirT2Ch2_c3_u_df2, 0,      2,     2,      1,         1,      1,        1,      2   }, 
{Filter_iirT2Ch2_c4_u_df2, 0,      2,     2,      1,         0,      1,        1,      3   }, 
{Filter_iirT2Ch2_c4_u_df2, 0,      2,     2,      1,         1,      1,        1,      3   },
{Filter_iirT2Ch2_c5_u_df2, 0,      2,     2,      1,         0,      1,        1,      4   }, 
{Filter_iirT2Ch2_c5_u_df2, 0,      2,     2,      1,         1,      1,        1,      4   }, 


// Generic
{Filter_iirT2Ch1_c2_df2,      0,      2,     1,      0,         0,      1,        1,      1   }, // 8
{Filter_iirT2Ch1_c3_df2,   0,      2,     1,      0,         0,      1,        1,      2   }, 
{Filter_iirT2Ch1_c4_df2,   0,      2,     1,      0,         0,      1,        1,      3   }, //10
{Filter_iirT2Ch1_c5_df2,   0,      2,     1,      0,         0,      1,        1,      4   },
// Inplace
{Filter_iirT2Ch1_c2_df2,   0,      2,     1,      0,         1,      1,        1,      1   }, 
{Filter_iirT2Ch1_c3_df2,   0,      2,     1,      0,         1,      1,        1,      2   }, 
{Filter_iirT2Ch1_c4_df2,   0,      2,     1,      0,         1,      1,        1,      3   }, 
{Filter_iirT2Ch1_c5_df2,   0,      2,     1,      0,         1,      1,        1,      4   },
// Unicoeff
{Filter_iirT2Ch1_c2_df2,   0,      2,     1,      1,         0,      1,        1,      1   }, 
{Filter_iirT2Ch1_c3_df2,   0,      2,     1,      1,         0,      1,        1,      2   }, 
{Filter_iirT2Ch1_c4_df2,   0,      2,     1,      1,         0,      1,        1,      3   }, 
{Filter_iirT2Ch1_c5_df2,   0,      2,     1,      1,         0,      1,        1,      4   },
// Unicoeff and inplace
{Filter_iirT2Ch1_c2_df2,   0,      2,     1,      1,         1,      1,        1,      1   }, //20
{Filter_iirT2Ch1_c3_df2,   0,      2,     1,      1,         1,      1,        1,      2   }, 
{Filter_iirT2Ch1_c4_df2,   0,      2,     1,      1,         1,      1,        1,      3   }, 
{Filter_iirT2Ch1_c5_df2,   0,      2,     1,      1,         1,      1,        1,      4   },

// T2Ch1_cN
// Generic
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,      1   }, // 24
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,      2   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,      3   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,      4   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,      5   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,      6   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,      7   }, //30
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,      8   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,      9   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,     10   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,     11   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,     12   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,     13   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,     14   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         0,      1,        1,     15   }, 
// Inplace
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,      1   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,      2   }, //40
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,      3   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,      4   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,      5   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,      6   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,      7   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,      8   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,      9   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,     10   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,     11   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,     12   }, //50
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,     13   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,     14   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      0,         1,      1,        1,     15   },
// Unicoeff
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,      1   },
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,      2   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,      3   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,      4   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,      5   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,      6   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,      7   }, //60
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,      8   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,      9   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,     10   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,     11   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,     12   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,     13   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,     14   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         0,      1,        1,     15   },
// Unicoeff and inplace
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,      1   },
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,      2   }, //70
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,      3   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,      4   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,      5   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,      6   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,      7   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,      8   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,      9   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,     10   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,     11   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,     12   }, //80
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,     13   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,     14   }, 
{Filter_iirT2Ch1_cN_df2,   0,      2,     1,      1,         1,      1,        1,     15   },

	 // fiterFn			 type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
// T2Ch2_cN
// Generic
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,      1   }, //84
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,      2   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,      3   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,      4   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,      5   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,      6   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,      7   }, //90
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,      8   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,      9   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,     10   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,     11   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,     12   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,     13   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,     14   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         0,      1,        1,     15   }, 

// Inplace
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,      1   },
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,      2   }, //100
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,      3   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,      4   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,      5   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,      6   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,      7   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,      8   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,      9   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,     10   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,     11   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,     12   }, //110
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,     13   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,     14   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      0,         1,      1,        1,     15   },
// Unicoeff
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,      1   },
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,      2   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,      3   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,      4   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,      5   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,      6   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,      7   }, //120
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,      8   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,      9   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,     10   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,     11   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,     12   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,     13   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,     14   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         0,      1,        1,     15   },
// Unicoeff and inplace
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,      1   },
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,      2   }, //130
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,      3   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,      4   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,      5   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,      6   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,      7   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,      8   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,      9   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,     10   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,     11   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,     12   }, //140
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,     13   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,     14   }, 
{Filter_iirT2Ch2_cN_df2,   0,      2,     2,      1,         1,      1,        1,     15   },

	 // fiterFn			 type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
// T2ChN_cN (2 channels)
// Generic
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,      1   }, // 144
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,      2   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,      7   }, //150
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,     12   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         0,      1,        1,     15   }, 
// Inplace
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,      1   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,      2   }, //160
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,      7   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,     12   }, //170
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      0,         1,      1,        1,     15   },
// Unicoeff
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,      1   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,      2   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,      7   }, //180
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,     12   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         0,      1,        1,     15   },
// Unicoeff and inplace
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,      1   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,      2   }, //190
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,      7   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,     12   }, //200
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     2,      1,         1,      1,        1,     15   },

	 // fiterFn			 type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
// T2ChN_cN (3 channels)
// Generic
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,      1   }, // 204
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,      2   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,      7   }, //210
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,     12   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         0,      1,        1,     15   }, 
// Inplace      
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,      1   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,      2   }, //220
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,      7   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,     12   }, //230
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      0,         1,      1,        1,     15   },
// Unicoeff     
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,      1   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,      2   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,      7   }, //240
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,     12   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         0,      1,        1,     15   },
// Unicoeff and inplace
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,      1   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,      2   }, //250
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,      7   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,     12   }, //260
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     3,      1,         1,      1,        1,     15   },

	 // fiterFn			 type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
// T2ChN_cN (6 channels)
// Generic
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,      1   }, // 264
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,      2   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,      7   }, //270
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,     12   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         0,      1,        1,     15   }, 
// Inplace      
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,      1   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,      2   }, //280
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,      7   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,     12   }, //290
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      0,         1,      1,        1,     15   },
// Unicoeff     
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,      1   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,      2   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,      7   }, //300
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,     12   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         0,      1,        1,     15   },
// Unicoeff and inplace
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,      1   },
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,      2   }, //310
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,      7   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,     12   }, //320
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     6,      1,         1,      1,        1,     15   },

	 // fiterFn			 type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
// T2ChN_cN (12 channels)
// Generic
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,      1   }, // 324
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,      2   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,      7   }, //330
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,     12   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         0,      1,        1,     15   }, 
// Inplace      
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,      1   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,      2   }, //340
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,      7   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,     12   }, //350
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      0,         1,      1,        1,     15   },
// Unicoeff     
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,      1   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,      2   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,      7   }, //360
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,     12   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         0,      1,        1,     15   },
// Unicoeff and inplace
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,      1   },
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,      2   }, //370
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,      3   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,      4   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,      5   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,      6   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,      7   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,      8   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,      9   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,     10   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,     11   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,     12   }, //380
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,     13   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,     14   }, 
{Filter_iirT2ChN_cN_df2,   0,      2,     12,      1,         1,      1,        1,     15   }, //383

};

