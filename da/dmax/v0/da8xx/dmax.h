
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX Constants and Data structures definitions
//
//
//

#ifndef dMAX_H_
#define dMAX_H_
 
/* dMAX constants */
/* MAX0 and MAX1 ID's */
#define dMAX_MAX0_ID     0
#define dMAX_MAX1_ID     1

/* MAX and MAX1 RAM size */
#define dMAX_MAX_ISIZE   4096
#define dMAX_MAX_DSIZE   512

/* MAX0 Instruction RAM and Data RAM constants */

#define dMAX_MAX0_ISIZE  dMAX_MAX_ISIZE  
#define dMAX_MAX0_DSIZE  dMAX_MAX_DSIZE  

#define dMAX_MAX1_ISIZE  dMAX_MAX_ISIZE
#define dMAX_MAX1_DSIZE  dMAX_MAX_DSIZE

/* dMAX Events */
#ifndef BOARD_E4
#define McASP0_TX_EVENT  5
#define McASP0_RX_EVENT  6
#define McASP1_TX_EVENT  7
#define McASP1_RX_EVENT  8
#define McASP2_TX_EVENT  9
#define McASP2_RX_EVENT  10
#define EPWM0_EVENT      24
#define EPWM1_EVENT      25
#define EPWM2_EVENT      26
#else
#define McASP0_TX_EVENT  4
#define McASP0_RX_EVENT  5
#define McASP1_TX_EVENT  6
#define McASP1_RX_EVENT  7
#define McASP2_TX_EVENT  8
#define McASP2_RX_EVENT  9
#endif

#define NONE_EVENT       -1

#define CPU_EVENT_MSK (1<<CPU_OUT00_EVENT|1<<CPU_OUT16_EVENT|    \
                       1<<CPU_OUT01_EVENT|1<<CPU_OUT17_EVENT|    \
                       1<<CPU_OUT02_EVENT|1<<CPU_OUT18_EVENT|    \
                       1<<CPU_OUT03_EVENT|1<<CPU_OUT19_EVENT|    \
                       1<<CPU_OUT20_EVENT|1<<CPU_OUT21_EVENT)
#define IS_CPU_EVENT(EVT) (CPU_EVENT_MSK&1<<(EVT))

/* Event polarity */
#define dMAX_POL_LOW     0
#define dMAX_POL_HIGH    1

/* Event Priority */
#define dMAX_PRI_HIGH    0
#define dMAX_PRI_LOW     1

/* dMAX interrupts */
#define dMAX_NUMINT      8
#define dMAX_INT0        0
#define dMAX_INT1        1 
#define dMAX_INT2        2 
#define dMAX_INT3        3 
#define dMAX_INT4        4 
#define dMAX_INT5        5 
#define dMAX_INT6        6 
#define dMAX_INT7        7 
#define dMAX_INTANY      -1
#define dMAX_INTNONE     -2

/* dMAX transfer completion codes */
#define dMAX_NUMTCC      16 
#define dMAX_TCCANY      -1 
#define dMAX_TCCNONE     -2

/* dMAX fifo status codes */
#define dMAX_NUMFSC      16 
#define dMAX_FSCANY      -1 
#define dMAX_FSCNONE     -2

/* dMAX parameter table */
#define dMAX_PTANY       -1
#define dMAX_PTNONE      -2
#endif // dMAX_H_
