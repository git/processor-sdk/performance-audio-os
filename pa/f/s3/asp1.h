
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 -- Audio Stream 1 ASP Declarations
//
//
//

#ifndef ASP1_
#define ASP1_

#include <asp0.h>
#include <acpbeta.h>

// Audio stream (non-specific) processing chain initialization data

#define ACP_SERIES_END 0
#define END_BETA_END 0

#if (ACP_SERIES_END<<16)+(END_BETA_END&0xffff) != NULL
#error internal error
#endif

#define PAF_ASP_LINKNONE { 0, 0, NULL, NULL, NULL, }

#define PAF_ASP_LINKINIT(SERIES,NAME,VENDOR) { \
    (IALG_Fxns *)&NAME##_##VENDOR##_I##NAME, \
    NULL, \
    0, \
    (ACP_SERIES_##SERIES<<16)+(SERIES##_BETA_##NAME&0xffff), \
    NAME##_##VENDOR##_init, \
}

#define PAF_ASP_LINKINITPARAMS(SERIES,NAME,VENDOR,PARAMS) { \
    (IALG_Fxns *)&NAME##_##VENDOR##_I##NAME, \
    (const IALG_Params *)(PARAMS), \
    0, \
    (ACP_SERIES_##SERIES<<16)+(SERIES##_BETA_##NAME&0xffff), \
    NAME##_##VENDOR##_init, \
}

#define PAF_ASP_LINKJOIN(SERIES0,NAME0,SERIES1,NAME1,VENDOR) { \
    (IALG_Fxns *)&NAME1##_##VENDOR##_I##NAME1, \
    NULL, \
    (ACP_SERIES_##SERIES0<<16)+(SERIES0##_BETA_##NAME0&0xffff), \
    (ACP_SERIES_##SERIES1<<16)+(SERIES1##_BETA_##NAME1&0xffff), \
    NAME1##_##VENDOR##_init, \
}

#define PAF_ASP_LINKJOINPARAMS(SERIES0,NAME0,SERIES1,NAME1,VENDOR,PARAMS) { \
    (IALG_Fxns *)&NAME1##_##VENDOR##_I##NAME1, \
    (const IALG_Params *)(PARAMS), \
    (ACP_SERIES_##SERIES0<<16)+(SERIES0##_BETA_##NAME0&0xffff), \
    (ACP_SERIES_##SERIES1<<16)+(SERIES1##_BETA_##NAME1&0xffff), \
    NAME1##_##VENDOR##_init, \
}

// Audio stream (specific) processing chain initialization data

    /* None, for now. --Kurt */

#endif  /* ASP1_ */

