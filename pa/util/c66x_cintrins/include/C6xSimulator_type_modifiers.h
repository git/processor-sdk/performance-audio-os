/****************************************************************************/
/*  C6xSimulator_type_modifiers.h                                           */
/****************************************************************************/

/* 
 *
 * Copyright (C) 1997-2005 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*  
 *  Header file which defines/undefines certain keywords.
 *  Abstracted into its own file because different environments may
 *  support/not-support different keywords.
 *  For example 'restrict' is newly supported in C99. Some environments
 *  and compilers may support this whilst others may not. Those that dont
 *  should undefine restrict.
 *
 *  By virtue of this abstraction, several options exist for the user: -
 *  1. use this file as is
 *  2. dont use this file. Instead do defines/undefines in your makefile
 *  3. use this file and modify it as per the keyword support in your host-env.
 */


#ifndef C6XSIMULATOR_TYPE_MODIFIERS_H
#define C6XSIMULATOR_TYPE_MODIFIERS_H

/* this internal header file is relevant *only* in host environment */
#ifndef _TMS320C6X          

/* compiler directives : default settings used */
#define far
#define inline 
#define interrupt
#define _nassert(a)           assert(a)
#define near
#define restrict
#define volatile

#endif          /* #ifndef _TMS320C6X */       

#endif          /* C6XSIMULATOR_TYPE_MODIFIERS_H */

/*
 *  Automated Revision Information
 *  Changed: $Date: 2011-01-13 10:30:15 -0600 (Thu, 13 Jan 2011) $
 *  Revision: $Revision: 9959 $
 */

