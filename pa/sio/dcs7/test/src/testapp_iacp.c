
/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Common Alpha Code Processor Algorithm interface implementation
//
//
//

/*
 *  IACP default instance creation parameters
 */

#include <xdc/std.h>

#include <iacp.h>
#include <acpbeta.h> /* STD_BETA_PRIME_* */

#if (PAF_IROM_BUILD == 0xD610A004) || (PAF_IROM_BUILD == 0xD710E001)
#define CINITBUGFIXED
#endif

/*
 *  A basic alpha code processing beta table series.
 */

struct
{
    Int size;
    IALG_Status *pStatus[512];
} IACP_STD_BETA_TABLE =
{
    512 * sizeof (IALG_Status *) + 4,
#if ! defined (CINITBUGPATCHED) && ! defined (CINITBUGFIXED)
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
#endif /* CINITBUGPATCHED */
};
struct
{
    Int size;
    IALG_Status *pStatus[512];
} IACP_CUS_BETA_TABLE =
{
    512 * sizeof (IALG_Status *) + 4,
#if ! defined (CINITBUGPATCHED) && ! defined (CINITBUGFIXED)
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
#endif /* CINITBUGPATCHED */
};
IACP_BetaTable *IACP_BETA_SERIES[4] =
{
    (IACP_BetaTable *) &IACP_STD_BETA_TABLE,
    NULL,
    NULL,
    (IACP_BetaTable *) &IACP_CUS_BETA_TABLE,
};

/*
 *  A basic alpha code processing phi table series.
 */
/*
void * IACP_echo(int);
LgInt  IACP_arithmetic(LgInt, LgInt, LgInt, LgInt);
void * IACP_alloc(LgInt, LgInt);
void * IACP_free(LgInt, void *, LgInt);
LgInt  IACP_sleep(LgUns);
LgInt  IACP_stat(Int, Int);*/

//#include <mem.h> /* for MEM_* */

struct
{
    Int size;
    IACP_PhiFunction *pFunction[8];
} IACP_STD_PHI_TABLE =
{
    8 * sizeof(IACP_PhiFunction *) + 4,
    NULL, //(IACP_PhiFunction *)IACP_echo, // ACP_PHI_ECHO
    NULL, //(IACP_PhiFunction *)IACP_arithmetic, // ACP_PHI_ARITHMETIC
    NULL, //(IACP_PhiFunction *)IACP_alloc, // ACP_PHI_ALLOC
    NULL, //(IACP_PhiFunction *)IACP_free, // ACP_PHI_FREE
    NULL, //(IACP_PhiFunction *)IACP_sleep, // ACP_PHI_SLEEP
    NULL, //(IACP_PhiFunction *)IACP_stat, // ACP_PHI_STAT
#if ! defined (CINITBUGPATCHED) && ! defined (CINITBUGFIXED)
    NULL,
    NULL,
#endif /* CINITBUGPATCHED */
        };
struct
{
    Int size;
    IACP_PhiFunction *pFunction[8];
} IACP_CUS_PHI_TABLE =
{
    8 * sizeof(IACP_PhiFunction *) + 4,
#if ! defined (CINITBUGPATCHED) && ! defined (CINITBUGFIXED)
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
#endif /* CINITBUGPATCHED */
};
IACP_PhiTable *IACP_PHI_SERIES[4] = {
    (IACP_PhiTable *)&IACP_STD_PHI_TABLE,
    NULL,
    NULL,
    (IACP_PhiTable *)&IACP_CUS_PHI_TABLE,
};

/*
 *  A basic alpha code processing sigma table series.
 */

/*// Read Type 1 Sigma 1 : ready
const ACP_Unit ready_s[] = {
    0xc902,                 // encapsulated alpha code sequence list
    0xcdf0, 0xc100,         // args[0] = 0xffffc100
                            // return (args[0])
};*/

struct
{
    Int size;
    const ACP_Unit *pSequence[64];
} IACP_STD_SIGMA_TABLE =
{
    64 * sizeof (const ACP_Unit *) + 4,
    NULL, //ready_s,
#if ! defined (CINITBUGPATCHED) && ! defined (CINITBUGFIXED)
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
#endif /* CINITBUGPATCHED */
};
struct
{
    Int size;
    const ACP_Unit *pSequence[64];
} IACP_CUS_SIGMA_TABLE =
{
    64 * sizeof (const ACP_Unit *) + 4,
#if ! defined (CINITBUGPATCHED) && ! defined (CINITBUGFIXED)
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
#endif /* CINITBUGPATCHED */
};
IACP_SigmaTable *IACP_SIGMA_SERIES[4] =
{
    (IACP_SigmaTable *) &IACP_STD_SIGMA_TABLE,
    NULL,
    NULL,
    (IACP_SigmaTable *) &IACP_CUS_SIGMA_TABLE,
};

/*
 *  ======== IACP_PARAMS ========
 *  This static initialization defines the default parameters used to
 *  create instances of ACP objects.
 */
/* const */IACP_Status IACP_PARAMS_STATUS =
{
    sizeof(IACP_Status),
    0, STD_BETA_PRIME_BASE, STD_BETA_PRIME_OFFSET,
    0, 0,
#ifndef CINITBUGFIXED
    0, 0, 0, 0,
#endif /* CINITBUGFIXED */
        };
const IACP_Params IACP_PARAMS =
{
    sizeof(IACP_Params),
    &IACP_PARAMS_STATUS,
    IACP_BETA_SERIES,
    IACP_PHI_SERIES,
    IACP_SIGMA_SERIES,
#ifndef CINITBUGFIXED
    NULL,
#endif /* CINITBUGFIXED */
};
const IACP_Params IACP_PARAMS_SRECORD_SUPPORT =
{
    sizeof(IACP_Params),
    &IACP_PARAMS_STATUS,
    IACP_BETA_SERIES,
    IACP_PHI_SERIES,
    IACP_SIGMA_SERIES,
    (IACP_SRecordState *)256,
};
