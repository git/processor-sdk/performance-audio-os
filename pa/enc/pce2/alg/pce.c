
/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Standard PCM Encoder algoritm functionality implementation
//
//
//

/*
 *  PCE Module - implements all functions and defines all constant
 *  structures common to all PCM Encoder algoritm implementations.
 */
#include <xdc/std.h> //<std.h>
#include <alg.h>

#include <pce.h>

/*
 *  ======== PCE_encode ========
 */
Int PCE_encode(PCE_Handle handle, ALG_Handle sioHandle, PAF_EncodeInStruct *pEncodeInStruct, PAF_EncodeOutStruct *pEncodeOutStruct)
{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->encode(handle, sioHandle, pEncodeInStruct, pEncodeOutStruct);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

/*
 *  ======== PCE_exit ========
 *  Module finalization
 */
Void PCE_exit()
{
}

/*
 *  ======== PCE_info ========
 */
Int PCE_info(PCE_Handle handle, ALG_Handle sioHandle, PAF_EncodeControl *pEncodeControl, PAF_EncodeStatus *pEncodeStatus)

{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->info(handle, sioHandle, pEncodeControl, pEncodeStatus);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

/*
 *  ======== PCE_init ========
 *  Module initialization
 */
Void PCE_init()
{
}

/*
 *  ======== PCE_reset ========
 */
Int PCE_reset(PCE_Handle handle, ALG_Handle sioHandle, PAF_EncodeControl *pEncodeControl, PAF_EncodeStatus *pEncodeStatus)

{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->reset(handle, sioHandle, pEncodeControl, pEncodeStatus);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

