/*
* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  Module functional implementation  
 */

#include <std.h>
#include <ialg.h>

#include <imdDeFormat.h>
#include <mdDeFormat_mds.h>
#include <mdDeFormat_mds_priv.h>

#include <mdDeFormaterr.h>

#include <xdas.h>

#include "paftyp.h"
#include "cpl.h"

#include "logp.h"
#include <stdio.h>  // printf in case of programming error

#include <tsk.h> // debug
#include <hwi.h>

//#define TRACE_MDDEFORMAT
#ifdef TRACE_MDDEFORMAT
    //#include "dp.h"
    //#define TRACE(a)  dp a
    #define TRACE(a)  LOG_printf a
#else
    #define TRACE(a)
#endif


#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif /* PAF_AUDIODATATYPE_FIXED */

#ifndef _TMS320C6X
#define restrict
#endif /* _TMS320C6X */

#define MDFORMAT_SYNC_MARKER_SAMP_DELTA         ( 64 )      // distance in samples between successive sync markers
#define MDFORMAT_SYNC_MARKER                    ( 0xA5 )    // sync marker
#define MDFORMAT_SYNC_MARKER_NBYTES             ( 1 )       // number of bytes used for marker
#define MDFORMAT_SYNC_PHASE_NBYTES              ( 1 )       // number of bytes used for phase
#define MDFORMAT_SYNC_DATA_NBYTES               ( MDFORMAT_SYNC_MARKER_NBYTES + MDFORMAT_SYNC_PHASE_NBYTES )

/* De-format decoder frame */
static Int defmtDecFrame(
    MDDEFORMAT_MDS_Obj *mdDeFormat, 
    PAF_AudioFrame *pAudioFrame
);

/* Generate NULL PAF audio frame */
static Int genNullPafAudioFrame(
    PAF_AudioFrame *pAudioFrame
);

/* Zero-fill PAF audio frame PCM data */
static Int zfPafAudioFramePcmAll(
    PAF_AudioFrame *pAudioFrame
);

/* Get number of channels, packed channel data pointer array */
static Int getChPtrs(
    PAF_AudioFrame *pAudioFrame,
    Uint8 *pNumChs,
    PAF_AudioData **chPtrs
);


/*
 *  ======== MDDEFORMAT_MDS_apply ========
 *  This function is called after the MDDEFORMAT_MDS_reset function, below, and
 *  deals with control data AND sample data.
 *
 */

 Int
MDDEFORMAT_MDS_apply(IMDDEFORMAT_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    MDDEFORMAT_MDS_Obj *restrict mdDeFormat = (Void *)handle;
    //IMDDEFORMAT_Config *pCfg = mdDeFormat->pConfig;
    Bool done;
    Int16 rxNSamps;
    Int32 *syncCh;  // use pointer to int since transport format data
    Uint8 syncB;
    Int16 rxPhase;
    Int16 phase0Idx;
    Uint8 chIdx;
    Int16 sampIdx;
    Int16 maxNChs;
    PAF_AudioData *pSrcData, *pDstData;
    Int16 i, j;
    PAF_AudioData tempData;
    Uint8 status;

    if (mdDeFormat->pStatus->mode == 0)
    {
        return 0;
    }

    ///* Check PAF_AudioFrame max M <= Deformatter max M */
    ///* Check PAF_AudioFrame sampleCount-1 <= Deformatter max N */
    //if ((pAudioFrame->data.nChannels > mdDeFormat->pConfig->maxNumChs) ||

    /* Check PAF_AudioFrame max M >= Deformatter max M */
    /* Check PAF_AudioFrame sampleCount <= Deformatter max N  */
    if ((mdDeFormat->pConfig->maxNumChs > pAudioFrame->data.nChannels) ||
        (pAudioFrame->sampleCount > mdDeFormat->pConfig->maxNumSamps))
    {
        TRACE((&trace, "Defmt: inv params"));
        return 1; //MDDEFORMATERR_INVPRMS;
    }

    /* Get Rx number of samples */
    rxNSamps = pAudioFrame->sampleCount;
    //TRACE((&trace, "Defmt: rxNSamps=%d.", rxNSamps));

    /* Get sync channel */
    syncCh = (Int32 *)pAudioFrame->data.sample[0];

    //if (mdDeFormat->pStatus->syncd == XDAS_TRUE)
    //{
        /* Check if re-sync required */
        i = mdDeFormat->pStatus->syncMarkerRxIdx;
        TRACE((&trace, "Defmt: syncMarkerRxIdx=%d.", mdDeFormat->pStatus->syncMarkerRxIdx));
        done = FALSE;
        while ((i < rxNSamps) && (done == FALSE))
        {
            syncB = (Uint8)(syncCh[i] & 0xFF);
            if (syncB == MDFORMAT_SYNC_MARKER)
            {
                i += MDFORMAT_SYNC_MARKER_SAMP_DELTA;
            }
            else
            {
                done = TRUE;
                mdDeFormat->pStatus->syncd = XDAS_FALSE;
                mdDeFormat->pStatus->rxFrameCount = 0;
                mdDeFormat->pStatus->syncCnt++;

                TRACE((&trace, "Defmt: resync=%d", i));
            }
        }
    //}

    if (mdDeFormat->pStatus->syncd == XDAS_FALSE)
    {
        /* Check for sync */

        /* Check for marker in first sample block */
        done = FALSE;
        i = 0;
        while ((done == FALSE) && (i < MDFORMAT_SYNC_MARKER_SAMP_DELTA))
        {
            syncB = (Uint8)syncCh[i] & 0xFF;
            if (syncB != MDFORMAT_SYNC_MARKER)
            {
                i++;
            }
            else
            {
                TRACE((&trace, "Defmt: mark 0=%d.", i));
                done = TRUE;
            }
        }

        /* Check for markers in remaining sample blocks */
        j = 1;
        while ((done == TRUE) && (j < rxNSamps/MDFORMAT_SYNC_MARKER_SAMP_DELTA))
        {
            syncB = (Uint8)syncCh[i + j*MDFORMAT_SYNC_MARKER_SAMP_DELTA] & 0xFF;
            if (syncB == MDFORMAT_SYNC_MARKER)
            {
                j++;
            }
            else
            {
                TRACE((&trace, "Defmt: mark %d=X.", j));
                done = FALSE;
            }
        }

        mdDeFormat->pStatus->syncd = done;

        if (mdDeFormat->pStatus->syncd == XDAS_TRUE)
        {
            TRACE((&trace, "Defmt: sync"));

            /* Update sync marker index */
            mdDeFormat->pStatus->syncMarkerRxIdx = i;

            /* Update Rx phase of sample 0 in audio frame */
            syncB = (Uint8)syncCh[i+1] & 0xFF;
            mdDeFormat->pStatus->rxPhase = syncB * MDFORMAT_SYNC_MARKER_SAMP_DELTA - i;
            if (mdDeFormat->pStatus->rxPhase < 0)
            {
                mdDeFormat->pStatus->rxPhase += rxNSamps;
            }

            /* Tx / Rx frames are aligned, so no need to buffer frame data */
            if (mdDeFormat->pStatus->rxPhase == 0)
            {
                mdDeFormat->pStatus->rxFrameCount++;
            }

            /* Update index of phase 0 in Rx audio frame */
            mdDeFormat->pStatus->phase0Idx = (mdDeFormat->pStatus->rxPhase > 0) ? (rxNSamps - mdDeFormat->pStatus->rxPhase) : 0;
        }
        else
        {
            /* Generate NULL PAF audio frame */
            status = genNullPafAudioFrame(pAudioFrame);
            if (status != 0)
            {
                TRACE((&trace, "Defmt: genNullPafAudioFrame() error=%d", status));
                return status;
            }

            TRACE((&trace, "Defmt: no sync"));
            return 2; //MDDEFORMATERR_NOSYNC;
        }
    }

    /* Get maximum number of channels supported by audio frame */
    //maxNChs = pAudioFrame->data.nChannels;
    /* Get maximum number of channels supported by de-formatter */
    maxNChs = mdDeFormat->pConfig->maxNumChs;

    /* Get Rx phase and phase index of phase 0 */
    rxPhase = mdDeFormat->pStatus->rxPhase;
    phase0Idx = mdDeFormat->pStatus->phase0Idx;

    if (rxNSamps < rxPhase) 
    {
        /* Guard for overrun */
        return 2; 
    }
    if (rxPhase < 0)
    {
        return 2;
    }

    if (mdDeFormat->pStatus->rxFrameCount > 0)
    {
        if (mdDeFormat->pStatus->rxPhase > 0)
        {
            /* Reconstruct transport frame */

            /* Put samples used from reconstruction of previous frame in persistent buffer */
            // copy (( rxNSamps - rxPhase ) x maxNChs ) PAF_AudioData 
            //     from offset=0 in Rx audio buffer
            //     to offset=rxPhase in persistent buffer
            for (chIdx = 0; chIdx < maxNChs; chIdx++)
            {
                memcpy(&mdDeFormat->sampleBufP[chIdx][rxPhase], &pAudioFrame->data.sample[chIdx][0], 
                    (rxNSamps-rxPhase)*sizeof(PAF_AudioData));
            }

            /* Translate samples from current frame in Rx audio buffer */
            // copy ( rxPhase x maxNChs ) PAF_AudioData
            //     from offset=(rxNSamps-rxPhase) in Rx audio buffer
            //     to offset=0 in Rx audio buffer
            for (chIdx = 0; chIdx < maxNChs; chIdx++)
            {
                pSrcData = &pAudioFrame->data.sample[chIdx][rxNSamps-rxPhase];
                pDstData = &pAudioFrame->data.sample[chIdx][0];
                for (sampIdx = 0; sampIdx < rxPhase; sampIdx++)
                {
                    *pDstData = *pSrcData;
                    pSrcData++;
                    pDstData++;
                }
            }

            /* Swap samples between persistent and Rx audio buffers --
               Put samples from previous frame in Rx audio buffer, 
               Put samples from current frame in persistent buffer */ 
            // Swap ( rxPhase x maxNChs ) PAF_AudioData between
            //     offset=0 in persistent buffer and 
            //     offset=0 in Rx audio frame
            for (chIdx = 0; chIdx < maxNChs; chIdx++)
            {
                pSrcData = &pAudioFrame->data.sample[chIdx][0];
                pDstData = &mdDeFormat->sampleBufP[chIdx][0];
                for (sampIdx = 0; sampIdx < rxPhase; sampIdx++)
                {
                    tempData = *pSrcData;
                    *pSrcData = *pDstData;
                    *pDstData = tempData;
                    pSrcData++;
                    pDstData++;
                }
            }

            /* Put remaining samples from current frame in Rx audio buffer */
            // copy (( rxNSamps - rxPhase ) x maxNChs ) PAF_AudioData 
            //     from offset=(rxNSamps-rxPhase) in Rx audio buffer
            //     to offset=(rxNSamps-rxPhase) in persistent buffer
            for (chIdx = 0; chIdx < maxNChs; chIdx++)
            {
                memcpy(&pAudioFrame->data.sample[chIdx][rxPhase], &mdDeFormat->sampleBufP[chIdx][rxPhase], 
                    (rxNSamps-rxPhase)*sizeof(PAF_AudioData));
            }
        }

        /* De-format transport frame in PAF_AudioFrame audio buffer */
        status = defmtDecFrame(mdDeFormat, pAudioFrame);
        if (status != 0)
        {
            TRACE((&trace, "Defmt: defmtDecFrame() error=%d", status));
            return status;
        }
    }
    else
    {
        /* Store samples used for reconstruction of next frame */
        /* No need to use scratch memory in this case */
        // copy ( rxPhase x maxNChs ) PAF_AudioData 
        //     from offset=phase0Idx in Rx audio frame 
        //     to offset=0 in de-formatter transport buffer (state).
        for (chIdx = 0; chIdx < maxNChs; chIdx++)
        {
            memcpy(&mdDeFormat->sampleBufP[chIdx][0], &pAudioFrame->data.sample[chIdx][phase0Idx], 
                rxPhase*sizeof(PAF_AudioData));
        }

        /* Generate NULL PAF audio frame */
        status = genNullPafAudioFrame(pAudioFrame);
        if (status != 0)
        {
            TRACE((&trace, "Defmt: genNullPafAudioFrame() error=%d", status));
            return status;
        }

        mdDeFormat->pStatus->rxFrameCount++;
    }

    return 0;
}

/*
 *  ======== MDDEFORMAT_MDS_reset ========
 *  v's implementation of the reset function, which is like an "information"
 *  operation.  This is always called by the framework before calling the MDDEFORMAT_MDS_apply
 *  function, and only deals with control data, not sample data.
 */

Int 
MDDEFORMAT_MDS_reset(IMDDEFORMAT_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    MDDEFORMAT_MDS_Obj * restrict mdDeFormat = (Void *)handle;

    TRACE((&trace, "%s.%d.\n", __FUNCTION__, __LINE__));

    mdDeFormat->pStatus->syncMarkerRxIdx = 0;
    mdDeFormat->pStatus->rxPhase = 0;
    mdDeFormat->pStatus->phase0Idx = 0;
    mdDeFormat->pStatus->syncd = XDAS_FALSE;
    mdDeFormat->pStatus->rxFrameCount = 0;
    mdDeFormat->pStatus->syncCnt = 0;

    return 0;
}

/* De-format decoder frame */
static Int defmtDecFrame(
    MDDEFORMAT_MDS_Obj *mdDeFormat, 
    PAF_AudioFrame *pAudioFrame
)
{
    Uint8 numChs;
    PAF_AudioData *chPtrs[PAF_MAXNUMCHAN_HD];
    Int32 *chPtrsI[PAF_MAXNUMCHAN_HD];
    Uint8 numChMd;
    Uint8 chIdx;
    Int16 sampIdx;
    Uint8 tempB;
    Uint8 *pDstData;
    PAF_PrivateMetadata *pPrivateMd;
    Uint16 i, j;

    if (pAudioFrame == NULL)
    {
        return 1; //MDDEFORMATERR_INVPRMS;
    }

    /* Get number of channels, packed channel data pointer arrays */
    getChPtrs(pAudioFrame, &numChs, &chPtrs[0]);
    for (chIdx = 0; chIdx < numChs; chIdx++)
    {
        chPtrsI[chIdx] = (Int32 *)chPtrs[chIdx];
    }

    /*-------------------------------*
     * Extract control channel data  *
     *-------------------------------*/
    /* Initialize sample index */
    sampIdx = MDFORMAT_SYNC_DATA_NBYTES;

    /* Extract number of channels used for transmission of private metadata */
    numChMd = (Uint8)(chPtrsI[0][sampIdx++] & 0xFF);
    //TRACE((&trace, "defmtDecFrame.%d:  numChMd: %d.\n", __LINE__, numChMd));

    /* Extract bit-stream metadata */
    /* bitstream metadata update flag */
    tempB = (Uint8)(chPtrsI[0][sampIdx++] & 0xFF);
    pAudioFrame->pafBsMetadataUpdate = (XDAS_Bool)tempB;
    TRACE((&trace, "defmtDecFrame.%d:  pafBsMetadataUpdate: %d.\n", __LINE__, pAudioFrame->pafBsMetadataUpdate));

    if (pAudioFrame->pafBsMetadataUpdate == XDAS_TRUE)
    {
        /* 1)  metadata type */
        pDstData = (Uint8 *)&pAudioFrame->bsMetadata_type;
        for (i = 0; i < sizeof(PAF_bsMetadataTypes); i++)
        {
            *pDstData = (Uint8)(chPtrsI[0][sampIdx++] & 0xFF);
            pDstData++;
        }

        /* 2) sample rate */
        tempB = (Uint8)(chPtrsI[0][sampIdx++] & 0xFF);
        pAudioFrame->sampleRate = (Int8)tempB;

        /* 3) channel configuration */
        pDstData = (Uint8 *)&pAudioFrame->channelConfigurationStream;
        for (i = 0; i < sizeof(PAF_ChannelConfiguration); i++)
        {
            *pDstData = (Uint8)(chPtrsI[0][sampIdx++] & 0xFF);
            pDstData++;
        }

        /* 4) offset */
        pDstData = (Uint8 *)&pAudioFrame->bsMetadata_offset;
        for (i = 0; i < sizeof(XDAS_UInt16); i++)
        {
            *pDstData = (Uint8)(chPtrsI[0][sampIdx++] & 0xFF);
            pDstData++;
        }
    }

    //TRACE((&trace, "defmtDecFrame.%d:  sampleCount: %d.\n", __LINE__, pAudioFrame->sampleCount));
     /* Perform fixed- to float conversion on control channel data */
    for (sampIdx = 0; sampIdx < pAudioFrame->sampleCount; sampIdx++)
    {
        chPtrs[0][sampIdx] = INT2F(chPtrsI[0][sampIdx]);
    }

    /* -------------------------*
     * Extract private metadata *
     * -------------------------*/
    /* Initialize channel and sample indices */
    chIdx = 1;      /* channel index */
    sampIdx = 0;    /* sample index */

    /* Private metadata only transmitted in object-audio mode */
    if ((pAudioFrame->bsMetadata_type == PAF_bsMetadata_Evolution) || 
        (pAudioFrame->bsMetadata_type == PAF_bsMetadata_DdpEvolution))
    {
        /* number of private metadata */
        tempB = (Uint8)(chPtrsI[chIdx][sampIdx] & 0xFF);
        pAudioFrame->numPrivateMetadata = tempB;
        chPtrs[chIdx++][sampIdx] = INT2F(chPtrsI[chIdx][sampIdx]);
        if (chIdx >= numChMd)
        {
            chIdx = 1;
            sampIdx++;
        }
        //TRACE((&trace, "defmtDecFrame.%d:  numPrivateMetadata: %d.\n", __LINE__, tempB));

        /* Guard of out of bound access of pafPrivateMetadata[] array */
        if (pAudioFrame->numPrivateMetadata > PAF_MAX_NUM_PRIVATE_MD) 
        {
            return 2;
        }

        for (i = 0; i < pAudioFrame->numPrivateMetadata; i++)
        {
            pPrivateMd = &pAudioFrame->pafPrivateMetadata[i];

            /* offset */
            tempB = (Uint8)(chPtrsI[chIdx][sampIdx] & 0xFF);
            pPrivateMd->offset = tempB;
            chPtrs[chIdx++][sampIdx] = INT2F(chPtrsI[chIdx][sampIdx]);
            if (chIdx >= numChMd)
            {
                chIdx = 1;
                sampIdx++;
            }
            tempB = (Uint8)(chPtrsI[chIdx][sampIdx] & 0xFF);
            pPrivateMd->offset |= tempB<<8;
            chPtrs[chIdx++][sampIdx] = INT2F(chPtrsI[chIdx][sampIdx]);
            if (chIdx >= numChMd)
            {
                chIdx = 1;
                sampIdx++;
            }

            /* size */
            tempB = (Uint8)(chPtrsI[chIdx][sampIdx] & 0xFF);
            pPrivateMd->size = tempB;
            chPtrs[chIdx++][sampIdx] = INT2F(chPtrsI[chIdx][sampIdx]);
            if (chIdx >= numChMd)
            {
                chIdx = 1;
                sampIdx++;
            }
            tempB = (Uint8)(chPtrsI[chIdx][sampIdx] & 0xFF);
            pPrivateMd->size |= tempB<<8;
            chPtrs[chIdx++][sampIdx] = INT2F(chPtrsI[chIdx][sampIdx]);
            if (chIdx >= numChMd)
            {
                chIdx = 1;
                sampIdx++;
            }

            /* Guard of writing more than the size of pMdBuf */
            if (pPrivateMd->size > PAF_MAX_PRIVATE_MD_SZ)
            {
                return 2;
            }

            /* de-serialized private metadata frame */
            pDstData = pPrivateMd->pMdBuf;
            for (j = 0; j < pPrivateMd->size; j++)
            {
                *pDstData = (Uint8)(chPtrsI[chIdx][sampIdx] & 0xFF);
                chPtrs[chIdx++][sampIdx] = INT2F(chPtrsI[chIdx][sampIdx]);
                pDstData++;

                if (chIdx >= numChMd)
                {
                    chIdx = 1;
                    sampIdx++;
                }
            }
        }
    }

    return 0;
}

/* Generate NULL PAF audio frame */
static Int genNullPafAudioFrame(
    PAF_AudioFrame *pAudioFrame
)
{
    Int status = 0;

    if (pAudioFrame == NULL)
    {
        status = 1; //MDDEFORMATERR_INVPRMS;
    }

    if (status == 0)
    {
        /* No bit-stream metadata update */
        pAudioFrame->pafBsMetadataUpdate = XDAS_FALSE;

        /* No private metadata */
        pAudioFrame->numPrivateMetadata = 0;

        /* Zero-fill PAF audio frame PCM data */
        status = zfPafAudioFramePcmAll(pAudioFrame);
    }

    return status;
}

/* Zero-fill PAF audio frame PCM data */
static Int zfPafAudioFramePcmAll(
    PAF_AudioFrame *pAudioFrame
)
{
    Uint8 chIdx;
    Int16 sampIdx;

    /* Basic parameter checking */
    if (pAudioFrame == NULL)
    {
        return 1; //MDDEFORMATERR_INVPRMS;
    }

    /* Zero-fill PCM data */
    for (chIdx = 0; chIdx < pAudioFrame->data.nChannels; chIdx++)
    {
        if (pAudioFrame->data.sample[chIdx] != NULL)
        {
            for (sampIdx = 0; sampIdx < pAudioFrame->sampleCount; sampIdx++)
            {
                pAudioFrame->data.sample[chIdx][sampIdx] = 0.0;
            }
        }
    }

    return 0;
}

/* Get number of channels, packed channel data pointer array */
static Int getChPtrs(
    PAF_AudioFrame *pAudioFrame,
    Uint8 *pNumChs,
    PAF_AudioData **chPtrs
)
{
    Uint8 numChs;
    PAF_ChannelMask_HD streamMask;
    Uint8 i;

    /* Get channel mask */
    streamMask = pAudioFrame->fxns->channelMask(pAudioFrame, 
        pAudioFrame->channelConfigurationStream);

    /* Compute number of channels.
    Pack channel data pointer array */
    numChs = 0;
    for (i = 0; i < PAF_MAXNUMCHAN_HD; i++)
    {
        if ((streamMask >> i) & 0x1)
        {
            chPtrs[numChs] = &pAudioFrame->data.sample[i][0];
            numChs++;
        }
    }

    *pNumChs = numChs;

    return 0;
}
