
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

 /*
 *  FEB Module IALG implementation - TII's implementation of the
 *  IALG interface for the FIL-ASP Example Demonstration algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAIS.
 */ 

/* Standard headers */
#include <std.h>
#include <ialg.h>

/* FEB headers */
#include <ifeb.h>
#include <feb_tii.h>
#include <feb_tii_priv.h>

/*
 *  ======== FEB_TII_activate ========
 */
  /* COM_TIH_activate */

/*
 *  ======== FEB_TII_alloc ========
 */
Int FEB_TII_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    Int i, n = 0;
    const IFEB_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IFEB_PARAMS;  /* set default parameters */
    }

    /* Request memory for FEB object */
    memTab[0].size = (sizeof(FEB_TII_Obj) + 3) /4*4 + 
                     (sizeof(FEB_TII_Status) + 3 ) /4*4;
    memTab[0].alignment = 4;
    memTab[0].space = IALG_SARAM;
    memTab[0].attrs = IALG_PERSIST;
    
    /* Call FIL alg alloc, for 10 bands, with respective memTab pointers */
    for(i = 0; i < 10; i++)
        n += FEB_TII_IFEB.filFxns->ialg.algAlloc((const IALG_Params *)params->pFilParams,
                                                                     pf, &memTab[1 + n]);
    
    return (1 + n); /* Return the number of memory requests */
}

/*
 *  ======== FEB_TII_deactivate ========
 */
  /* COM_TIH_deactivate */

/*
 *  ======== FEB_TII_free ========
 */
Int FEB_TII_free(IALG_Handle handle, IALG_MemRec memTab[])
{
    FEB_TII_Obj *feb = (Void *)handle;
    FIL_Handle fil = (FIL_Handle)feb->bandHandle[0];
    Int i, n;
    
    /* Get the number of memory requests for one FIL object */
    n = fil->fxns->ialg.algNumAlloc();    
    
    /* FIL memTabs are set for free by calling FIL free function */
    for(i = 0; i < 10; i++)
    {
        fil = (FIL_Handle)feb->bandHandle[i];
        fil->fxns->ialg.algFree((IALG_Handle)fil, &memTab[1 + i*n]);
    }        
    
    /* Call alloc function to set the remaining parameters of memTabs */
    return (*handle->fxns->algAlloc)(NULL, NULL, memTab);
}

/*
 *  ======== FEB_TII_initObj ========
 */
Int FEB_TII_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    FEB_TII_Obj *feb = (Void *)handle;
    const IFEB_Params *params = (Void *)algParams;
    FIL_Handle fil;  
    Int i, n;

    if (params == NULL) 
    {
        params = &IFEB_PARAMS;  /* set default parameters */
    }
    
    /* Get the status and config. pointers */
    feb->pStatus = (FEB_TII_Status *)((char *)feb + (sizeof(FEB_TII_Obj)+3)/4*4);
    *feb->pStatus = *params->pStatus;    
    feb->config = *params->pConfig;
    
    n = FEB_TII_IFEB.filFxns->ialg.algNumAlloc();
        
    /* Reset the filter coefficients */    
    for(i = 0; i < 50; i++)
        feb->coef[i] = 0;
        
    /* For each band, create and intialize the FIL object */
    for(i = 0; i < 10; i++)
    {
        /* Get FIL handle ptr of FIL band object */
        feb->bandHandle[i] = fil = (FIL_Handle)memTab[1 + i*n].base; 
        
        /* Initialize the fxns pointer */        
        fil->fxns = ((IFEB_Fxns *)feb->alg.fxns)->filFxns;
        
        /* Call FIL algInit function for each band */
        fil->fxns->ialg.algInit((IALG_Handle)fil, &memTab[1 + i*n], 
                                   p,( const IALG_Params *)params->pFilParams); 
                                   
        /* Initialize the band coefficients to behave as All Pass filters */
        feb->coef[i*5] = 1.0; /* B0 */
                                           
        feb->filCoef[i] = *(PAF_FilCoef_Void *)IFIL_PARAMS_FEB.pConfig->pCoefs;
        
        /* Set the coefficient pointer for the filter object */                                    
        feb->filCoef[i].cPtr[0] = (Void *)&feb->coef[i*5]; 
        
        ((FIL_TII_Obj *)fil)->pConfig->pCoefs = (PAF_FilCoef *)&feb->filCoef[i];
        
        /* Initialize the band gains to 0dB */                                        
        feb->pStatus->bandGain[i]   = 0;  
        feb->pStatus->gainStatus[i] = 0;                                                                                    
    }
             
    return (IALG_EOK);
}

/*
 *  ======== FEB_TII_control ========
 */
  /* COM_TIH_control */

/*
 *  ======== FEB_TII_moved ========
 */
  /* COM_TIH_moved */

Int FEB_TII_numAlloc()
{
    /* Additional requests for 10 such FIL objects is returned */ 
    return(1 + 10*FEB_TII_IFEB.filFxns->ialg.algNumAlloc());
}
