
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <dab_csl_mcbsp.h>

CSL_Status
    CSL_mcbspGetBaseAddress (
        CSL_InstNum             mcbspNum,
        CSL_McbspParam          *pMcbspParam,
        CSL_McbspBaseAddress    *pBaseAddress
)
{
    CSL_Status status = CSL_SOK;

    if (pBaseAddress == NULL) {
        status = CSL_ESYS_INVPARAMS;
	}
    else {
        switch (mcbspNum) {
            case CSL_MCBSP_0:
                pBaseAddress->regs = (CSL_McbspRegsOvly)CSL_MCBSP_0_REGS;
                break;

            case CSL_MCBSP_1:
                pBaseAddress->regs = (CSL_McbspRegsOvly)CSL_MCBSP_1_REGS;
                break;

            default:
                pBaseAddress->regs = (CSL_McbspRegsOvly)NULL;
                status = CSL_ESYS_FAIL;
                break;
        }
    }
    return (status);
}


CSL_McbspHandle  CSL_mcbspOpen (
    CSL_McbspObj        *pMcbspObj,
    CSL_InstNum         mcbspNum,
    CSL_McbspParam      *pMcbspParam,
    CSL_Status          *pStatus
)
{
    CSL_Status              status;
    CSL_McbspHandle         hMcbsp = NULL;
    CSL_McbspBaseAddress    baseAddress;

    if (pStatus == NULL) {
        /* do nothing */
    }
    else if (pMcbspObj == NULL) {
        *pStatus = CSL_ESYS_INVPARAMS;
    }
    else{
        status = CSL_mcbspGetBaseAddress(mcbspNum, pMcbspParam, &baseAddress);
    
        if (status == CSL_SOK) {
            pMcbspObj->regs = baseAddress.regs;
            pMcbspObj->perNum = (CSL_InstNum)mcbspNum;
            hMcbsp = (CSL_McbspHandle)pMcbspObj;
        }
        else {
            pMcbspObj->regs = (CSL_McbspRegsOvly)NULL;
            pMcbspObj->perNum = (CSL_InstNum)-1;
            hMcbsp = (CSL_McbspHandle)NULL;
        }
    
        *pStatus = status;
    }

    return hMcbsp;
}

CSL_Status  CSL_mcbspHwSetupRaw (
    CSL_McbspHandle     hMcbsp,
    CSL_McbspConfig     *config
)
{
    CSL_Status status = CSL_SOK;

    if (hMcbsp == NULL) {
        status = CSL_ESYS_BADHANDLE;
    }
    else if (config == NULL ) {
        status = CSL_ESYS_INVPARAMS;
    }
    else{
        hMcbsp->regs->SPCR     = config->SPCR;
        hMcbsp->regs->RCR      = config->RCR ;      
        hMcbsp->regs->XCR      = config->XCR ;      
        hMcbsp->regs->SRGR     = config->SRGR;   
        hMcbsp->regs->MCR      = config->MCR ;  
        hMcbsp->regs->RCERAB   = config->RCERAB;   
        hMcbsp->regs->XCERAB   = config->XCERAB;   
        hMcbsp->regs->PCR      = config->PCR  ;  
        hMcbsp->regs->RCERCD   = config->RCERCD;   
        hMcbsp->regs->XCERCD   = config->XCERCD;   
        hMcbsp->regs->RCEREF   = config->RCEREF;   
        hMcbsp->regs->XCEREF   = config->XCEREF;   
        hMcbsp->regs->RCERGH   = config->RCERGH;   
        hMcbsp->regs->XCERGH   = config->XCERGH;
        hMcbsp->regs->REVTCR   = config->REVTCR;   
        hMcbsp->regs->XEVTCR   = config->XEVTCR;   
        hMcbsp->regs->RFLR   = config->RFLR;   
        hMcbsp->regs->XFLR   = config->XFLR;   
        hMcbsp->regs->RSYNCCNT   = config->RSYNCCNT;   
        hMcbsp->regs->XSYNCCNT   = config->XSYNCCNT;     
    }
    return status;
}
