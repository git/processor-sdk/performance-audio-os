
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX API implementation for DA8xx
//
//
//

#include <std.h>
#include <hwi.h>
#include <sys.h>
#include <tsk.h>

#include "cslr_dmaxintc.h"
#include "cslr_dmaxpdsp.h"

#include "dmax.h"
#include "dmaxintc.h"
#include "dmaxpdsp.h"
#include "dmax_struct.h"

#include <logp.h>

// -----------------------------------------------------------------------------
// cp_intc spec indicates that the control register can perform a soft
// reset but I don't see it -- all I see is a wakeup mode enable

void dMAX_gioRst (dMAX_Handle handle)
{
}

// -----------------------------------------------------------------------------

void dMAX_gioRstRel (dMAX_Handle handle)
{
    // should I write a 1 to the global enable register?
}

// -----------------------------------------------------------------------------
// dMAX Event Polarity

void dMAX_evtPol (dMAX_Handle handle, Uint32 evt, Uint32 pol)
{
}

// -----------------------------------------------------------------------------
// dMAX Enable Event

void dMAX_enaEvt (dMAX_Handle handle, Uint32 evt)
{
    volatile CSL_DmaxintcRegs *pIntc = (volatile CSL_DmaxintcRegs *) DMAXINTC_BASEADDR;
    const Int oldMask = HWI_disable ();

    pIntc->ENIDXSET = evt;

    HWI_restore (oldMask);
} //dMAX_enaEvt

// -----------------------------------------------------------------------------
// dMAX Disable Evt

void dMAX_disEvt (dMAX_Handle handle, Uint32 evt)
{
    volatile CSL_DmaxintcRegs *pIntc = (volatile CSL_DmaxintcRegs *) DMAXINTC_BASEADDR;
    const Int oldMask = HWI_disable ();

    pIntc->ENIDXCLR = evt;

    HWI_restore (oldMask);
} //dMAX_disEvt

// -----------------------------------------------------------------------------
// dMAX Disable All (64) Events

void dMAX_disEvtAll (dMAX_Handle handle)
{
    volatile CSL_DmaxintcRegs *pIntc = (volatile CSL_DmaxintcRegs *) DMAXINTC_BASEADDR;
    const Int oldMask = HWI_disable ();

    pIntc->ENABLECLR0 = 0xFFFFFFFF;
    pIntc->ENABLECLR1 = 0xFFFFFFFF;

    HWI_restore (oldMask);
} //dMAX_disEvtAll

// -----------------------------------------------------------------------------
// dMAX Event Priority
//
// There are 64 events (32 from peripherals and 32 for TCC functionality)
// There are 10 channels and 10 host interrupts (a 1-1 mapping between them)
//      Note that this mapping is configured via dMAX_init
// HINT[0]   is connected to PDSP0/1 Register 31 bit 30
// HINT[1]   is connected to PDSP0/1 Register 31 bit 31
// HINT[2:9] is connected to event_out[0:7]
//
// By convention we assume PDSP0 will poll for HINT0 and PDSP1 for HINT1
// 
void dMAX_evtPri (dMAX_Handle handle, Uint32 maxid, Uint32 evt)
{
    volatile CSL_DmaxintcRegs *pIntc = (volatile CSL_DmaxintcRegs *) DMAXINTC_BASEADDR;

    // since there are 16 channel map registers each controlling the setting for
    // 4 events we can use the event number as a byte offset.
    ((volatile unsigned char *) &pIntc->CHANMAP0)[evt] = maxid;

} //dMAX_evtPri

// -----------------------------------------------------------------------------
// dMAX Clear Event Flag

void dMAX_clrEvtFlg (dMAX_Handle handle, Uint32 evt)
{
    volatile CSL_DmaxintcRegs *pIntc = (volatile CSL_DmaxintcRegs *) DMAXINTC_BASEADDR;
    const Int oldMask = HWI_disable ();

    pIntc->STATIDXCLR = evt;

    HWI_restore (oldMask);
} //dMAX_clrEvtFlg

// -----------------------------------------------------------------------------
// dMAX Clear All (64) Event Flags

void dMAX_clrEvtFlgAll (dMAX_Handle handle)
{
    volatile CSL_DmaxintcRegs *pIntc = (volatile CSL_DmaxintcRegs *) DMAXINTC_BASEADDR;
    const Int oldMask = HWI_disable ();

    pIntc->STATCLRINT0 = 0xFFFFFFFF;
    pIntc->STATCLRINT1 = 0xFFFFFFFF;

    HWI_restore (oldMask);
} //dMAX_clrEvtFlgAll

// -----------------------------------------------------------------------------

/* dMAX Direction */
void dMAX_dirInpAll(
    dMAX_Handle handle)
{
}

// -----------------------------------------------------------------------------
// dMAX Reset

void dMAX_rst (dMAX_Handle handle, Uint32 maxid)
{
    volatile CSL_DmaxpdspRegs *pPDSP;
    const Int oldMask = HWI_disable ();

    if (maxid == 0) 
        pPDSP = (volatile CSL_DmaxpdspRegs *) DMAXPDSP0_BASEADDR;
    else
        pPDSP = (volatile CSL_DmaxpdspRegs *) DMAXPDSP1_BASEADDR;

    // disable, set PC to 0, and software reset
    pPDSP->CONTROL &= ~CSL_DMAXPDSP_CONTROL_ENABLE_MASK;
    pPDSP->CONTROL &= ~CSL_DMAXPDSP_CONTROL_PCRESET_MASK;
    pPDSP->CONTROL &= ~CSL_DMAXPDSP_CONTROL_SOFTRESET_MASK;

    HWI_restore (oldMask);
} //dMAX_rst

// -----------------------------------------------------------------------------
// dMAX Reset Release 

void dMAX_rstRel (dMAX_Handle handle, Uint32 maxid)
{
    volatile CSL_DmaxpdspRegs *pPDSP;
    const Int oldMask = HWI_disable ();

    if (maxid == 0) 
        pPDSP = (volatile CSL_DmaxpdspRegs *) DMAXPDSP0_BASEADDR;
    else
        pPDSP = (volatile CSL_DmaxpdspRegs *) DMAXPDSP1_BASEADDR;

    // enable cycle count and then PDSP itself
    pPDSP->CONTROL |= CSL_DMAXPDSP_CONTROL_CYCLECOUNT_MASK;
    pPDSP->CONTROL |= CSL_DMAXPDSP_CONTROL_ENABLE_MASK;

    HWI_restore (oldMask);
} //dMAX_rstRel

// -----------------------------------------------------------------------------
// dMAX Initialize Instruction RAM

void dMAX_initIRAM(
    dMAX_Handle handle,
    Uint32 maxid)
{
    memcpy((void*)handle->max[maxid]->ibase,
           handle->max[maxid]->code,
           handle->max[maxid]->length);
}

// -----------------------------------------------------------------------------
// dMAX Initialize Data RAM

void dMAX_initDRAM (dMAX_Handle handle, Uint32 maxid)
{
    Int32 i;

    // clear all data memory
    for (i=0; i < 128; i++)
        handle->max[maxid]->dbase[i] = 0x00;

    for(i=0;i < (dMAX_MAX_DSIZE>>2)/32; i++)
        handle->max[maxid]->alloc[i] = 0x00;

    // Update allocation table with EET and PTT allocation
    for(i=0; i< (dMAX_EET_SIZE>>2)+(dMAX_PTT_SIZE>>2); i++)
        handle->max[maxid]->alloc[i>>5]|=1<<(i&0x1F);
}

// -----------------------------------------------------------------------------
// dMAX Update Event Entry Table (EET)

void dMAX_updateEET(
    dMAX_Handle handle,
    Uint32 maxid,
    Uint32 evt,
    Uint32 ee)
{
    handle->max[maxid]->eet[evt+1]=ee;
}

// -----------------------------------------------------------------------------
// dMAX Update Pending Transfer Table (PTT)

void dMAX_updatePTT (dMAX_Handle handle, Uint32 maxid, Uint32 pp, Uint32 evt)
{
    handle->max[maxid]->ptt[pp] = evt+1;
} //dMAX_updatePTT

// -----------------------------------------------------------------------------
// dMAX Allocate Parameter Table

Uint32 * dMAX_allocPT (dMAX_Handle handle, Uint32 maxid, Uint32 size)
{
    Int32 i;
    Int32 sizeInWords;
    const Int oldMask = HWI_disable ();    

    i = (dMAX_EET_SIZE+dMAX_PTT_SIZE) >> 2;
    while (i < dMAX_MAX_DSIZE >> 2) {
        sizeInWords = size+3 >> 2;
      
        while (i < dMAX_MAX_DSIZE>>2 &&
               handle->max[maxid]->alloc[i>>5]&1<<(i&0x1F))
            i++;

        while (i < dMAX_MAX_DSIZE>>2 && sizeInWords &&
              !(handle->max[maxid]->alloc[i>>5]&1<<(i&0x1F))) {
            sizeInWords--;
            i++;
        }

        if (!sizeInWords) {
            sizeInWords = size+3>>2;
            while (sizeInWords--) {
                i--;
                handle->max[maxid]->alloc[i>>5] |= 1<<(i&0x1F);
            }
            HWI_restore (oldMask);
            LOG_printf(&trace, "dMAX_allocPT: OK: id %d, size %d, i: %d.", maxid, size, i);
            return handle->max[maxid]->dbase+i;
        }
    }

    HWI_restore (oldMask);
    LOG_printf(&trace, "dMAX_allocPT: FAILED: id %d, size %d, i: %d.", maxid, size, i);
    asm( " SWBP 0" );
    return 0;
} //dMAX_allocPT

// -----------------------------------------------------------------------------
// dMAX Parameter Table configuration

void dMAX_cfgPT (dMAX_Handle handle, Uint32 *pt, Uint32 *pCfg, Uint32 size)
{
    memcpy (pt,pCfg,size);
}

// -----------------------------------------------------------------------------
// dMAX Free Parameter Table 

void dMAX_freePT (dMAX_Handle handle, Uint32 maxid, Uint32 *pt, Uint32 size)
{
    Uint32 sizeInWords,off;
    const Int oldMask = HWI_disable ();
    
    sizeInWords = size+3>>2;
    off = pt-handle->max[maxid]->dbase;
    while (sizeInWords--) {
        handle->max[maxid]->alloc[off>>5]&=~(1<<(off&0x1F));
        off++;
    }

    HWI_restore (oldMask);
} //dMAX_freePT

// -----------------------------------------------------------------------------
// dMAX allocate event

Int32 dMAX_allocEvt (dMAX_Handle handle, Int32 evt)
{
    Int32 evt_select=NONE_EVENT;
    const Int oldMask = HWI_disable ();

    if (!(handle->evtalloc & 1<<evt)) {
        handle->evtalloc |= 1 << evt;
        evt_select = evt;
    }

    HWI_restore (oldMask);
    return evt_select;
} //dMAX_allocEvt

// -----------------------------------------------------------------------------
// dMAX Free event

void dMAX_freeEvt (dMAX_Handle handle, Uint32 evt)
{
    const Int oldMask = HWI_disable ();

    handle->evtalloc &= ~(1<<evt);

    HWI_restore (oldMask);
} //dMAX_freeEvt

// -----------------------------------------------------------------------------
// dMAX Allocate interrupt
//      There are 8 events which the dMAX can generate to the GEM's 
//      internal INTC or the INTC in front of the ARM. In INTC 
//      parlance these are known as system interrupts. Unfortunately
//      the actual event numbers are different for the DSP vs. the ARM.
//      Since this API is, currently, DSP specific we don't have to worry about
//      this complication.
//
//      So this API returns one of the 8 non-reserved dMAX's INTC Host Interrupts
//      (HINT[2:9]) which map to the dMAX boundary signals EVTOUT[0:7] which are 
//      inputs into the GEM's INTC with this mapping (see sprs483.pdf)
//
//      EVTOUT0 =  6
//      EVTOUT1 = 17
//      EVTOUT2 = 22
//      EVTOUT3 = 35
//      EVTOUT4 = 66
//      EVTOUT5 = 39
//      EVTOUT6 = 44
//      EVTOUT7 = 50
//      

static Uint8 eventIntMap[8] = {6, 17, 22, 35, 66, 39, 44, 50};

Int32 dMAX_allocInt (dMAX_Handle handle, Int32 intnum)
{
    Int32 int_select = dMAX_INTNONE;
    const Int oldMask = HWI_disable ();
    Int32 i;


    if (intnum == dMAX_INTANY) {
        for(i=0; i < dMAX_NUMINT; i++)
            if (!(handle->intalloc & 1<<i)) {
                handle->intalloc |= 1<<i;
                int_select = i;
                break;
            }
    }
    else {
        if (!(handle->intalloc & 1<<intnum)){
            handle->intalloc |= 1<<intnum;
            int_select = intnum;
        }
    }

    if (int_select != dMAX_INTANY)
        int_select = eventIntMap [int_select];

    HWI_restore(oldMask);
    return int_select;
} //dMAX_allocInt

// -----------------------------------------------------------------------------
// Mapping from dMAX interrupt to cpu interrupt
//      The GEM has 12 (of 16) configurable interrupts. Some of these are
//      reserved, just based on a gentlemen's agreement. This function maps the
//      event allocated in dMAX_allocInt to one of the non-reserved interrupts.

Int32 dMAX_mapInt (dMAX_Handle handle, Uint32 intNum)
{
    static Uint8 dMAX2CPUInt[] = {6,8,9,10,11,12,13,15};
    int            intSelect;
    int                    i;


    intSelect = dMAX_INTANY;
    for (i=0; i < dMAX_NUMINT; i++) {
        if (intNum == eventIntMap[i]) {
            intSelect = i;
            break;
        }
    }

    if (intSelect == dMAX_INTANY)
        return intSelect;

    return dMAX2CPUInt[intSelect];
} //dMAX_mapInt

// -----------------------------------------------------------------------------
// dMAX Free interrupt

void dMAX_freeInt (dMAX_Handle handle, Uint32 intNum)
{
    const Int   oldMask = HWI_disable ();
    int       intSelect;
    int               i;


    intSelect = dMAX_INTANY;
    for (i=0; i < dMAX_NUMINT; i++) {
        if (intNum == eventIntMap[i]) {
            intSelect = i;
            break;
        }
    }

    if (intSelect != dMAX_INTANY) {
        handle->intalloc &= ~(1 << intSelect);
    }
 
    HWI_restore(oldMask);
} //dMAX_freeInt

// -----------------------------------------------------------------------------
// dMAX Allocate transfer completion code
//   On DA8xx we use the second 32 of 64 events for TCCs. The first 32 are for
//   actual system events. Since we use a single integer (32bits) to store the
//   allocation mask we add 32 to the value for return to the customer. If ths
//   allocation is successfull then we enable the event in the INTC.
//   Since on DA8xx we are using the INTC's abililty to generate a host
//   interrupt whenever a system interrupt is latched (read TCC) then we
//   need to map that TCC to appropriate host interrupt. However we don't
//   necessarily have that info here traditionally so we added an extra
//   parameter over the older interface.

Int32 dMAX_allocTCC (dMAX_Handle handle, Int32 tcc, Uint32 intNum)
{
    volatile CSL_DmaxintcRegs *pIntc = (volatile CSL_DmaxintcRegs *) DMAXINTC_BASEADDR;
    Int32   tccSelect = dMAX_TCCNONE;
    Int32   intSelect;
    const Int oldMask = HWI_disable ();
    Int32 i;


    // first find the index of EVTOUT[0:7] which corresponds to intNum
    // if we can't find one then return error. Note that we add 2 since
    // EVTOUT[0:7] is mapped at the dMAX boundary from HINT[2:9] of the INTC.
    intSelect = dMAX_INTANY;
    for (i=0; i < dMAX_NUMINT; i++) {
        if (intNum == eventIntMap[i]) {
            intSelect = i + 2;
            break;
        }
    }
    
    if (intSelect == dMAX_INTANY)
        return dMAX_TCCANY;

    if (tcc == dMAX_TCCANY) {
        for (i=0; i < dMAX_NUMTCC; i++)
            if (!(handle->tccalloc & 1<<i)) {
                handle->tccalloc |= 1<<i;
                tccSelect = i;
                break;
            }
    }
    else{
        if (!(handle->tccalloc & 1<<tcc)) {
            handle->tccalloc |= 1<<tcc;
            tccSelect=tcc;
        }
    }
    
    if (tccSelect != dMAX_TCCANY) {
        tccSelect       += 32;
        pIntc->ENIDXSET  = tccSelect;
        ((volatile unsigned char *) &pIntc->CHANMAP0)[tccSelect] = intSelect;        
    }
    
    HWI_restore(oldMask);
    return tccSelect;
} //dMAX_allocTCC

// -----------------------------------------------------------------------------
// dMAX Check transfer completion code is set/clear

Uint32 dMAX_chkTCC (dMAX_Handle handle, Uint32 tcc)
{
    volatile CSL_DmaxintcRegs *pIntc = (volatile CSL_DmaxintcRegs *) DMAXINTC_BASEADDR;
    
    if (tcc < 32)
        return (pIntc->STATSETINT0 & (1 << tcc));
    else
        return (pIntc->STATSETINT1 & (1 << (tcc-32)));
} //dMAX_chkTCC

// -----------------------------------------------------------------------------
// dMAX Set transfer completion code

void dMAX_setTCC (dMAX_Handle handle, Uint32 tcc)
{
    volatile CSL_DmaxintcRegs *pIntc = (volatile CSL_DmaxintcRegs *) DMAXINTC_BASEADDR;
    const Int oldMask = HWI_disable ();

    pIntc->STATIDXSET = tcc;

    HWI_restore (oldMask);
} //dMAX_setTCC

// -----------------------------------------------------------------------------
// dMAX Clear transfer completion code

void dMAX_clrTCC (dMAX_Handle handle, Uint32 tcc)
{
    dMAX_clrEvtFlg (handle, tcc);
} //dMAX_clrTCC

// -----------------------------------------------------------------------------
// dMAX Free transfer completion code
//    First disable TCC event in INTC then clear from alloc mask. Note that we
//    need to subtract 32 since we added 32 in the alloc function.

void dMAX_freeTCC (dMAX_Handle handle, Uint32 tcc)
{
    volatile CSL_DmaxintcRegs *pIntc = (volatile CSL_DmaxintcRegs *) DMAXINTC_BASEADDR;
    const Int oldMask = HWI_disable ();


    pIntc->ENIDXCLR  = tcc;
    tcc             -= 32;
    handle->tccalloc &= ~(1 << tcc);

    HWI_restore (oldMask);
} //dMAX_freeTCC

// -----------------------------------------------------------------------------
// dMAX Allocate fifo status code

Int32 dMAX_allocFSC (dMAX_Handle handle, Int32 fsc)
{
    const Int oldMask = HWI_disable ();
    Int32 fsc_select = dMAX_FSCNONE;
    Int32 i;


    if (fsc == dMAX_FSCANY) {
        for (i=0; i < dMAX_NUMFSC; i++)
            if(!(handle->fscalloc & 1<<i)) {
                handle->fscalloc |= 1<<i;
                fsc_select = i;
                break;
            }
    }
    else {
        if (!(handle->fscalloc & 1<<fsc)) {
            handle->fscalloc |= 1<<fsc;
            fsc_select = fsc;
        }
    }

    HWI_restore(oldMask);
    return fsc_select;
} //dMAX_allocFSC

// -----------------------------------------------------------------------------

/* dMAX Check fifo status code is set/clear */
Uint32 dMAX_chkFSC(
    dMAX_Handle handle,
    Uint32 fsc)
{
    return 0;
}

/* dMAX Set fifo status code */
void dMAX_setFSC(
    dMAX_Handle handle,
    Uint32 fsc)
{
}

/* dMAX Clear fifo status code */
void dMAX_clrFSC(
    dMAX_Handle handle,
    Uint32 fsc)
{
}

// -----------------------------------------------------------------------------
// dMAX Free transfer completion code

void dMAX_freeFSC (dMAX_Handle handle, Uint32 fsc)
{
    const Int oldMask = HWI_disable ();

    handle->fscalloc &= ~(1 << fsc);

    HWI_restore (oldMask);
} //dMAX_freeFSC

// -----------------------------------------------------------------------------
// dMAX Allocate pending priorities

Int32 dMAX_allocPP (dMAX_Handle handle, Uint32 maxid, Int32 pp)
{
    const Int oldMask = HWI_disable ();
    Int32 pp_select = dMAX_PPNONE;
    Int32 i;


    if (pp == dMAX_PPANY) {
        for(i=0; i < dMAX_NUMPP; i++)
            if(!(handle->max[maxid]->ppalloc & 1<<i)) {
                handle->max[maxid]->ppalloc |= 1<<i;
                pp_select = i;
                break;
            }
    }
    else {
        if (!(handle->max[maxid]->ppalloc & 1<<pp)) {
            handle->max[maxid]->ppalloc |= 1<<pp;
            pp_select = pp;
        }
    } 

    HWI_restore(oldMask);
    return pp_select;
} //dMAX_allocPP

// -----------------------------------------------------------------------------
// dMAX Free pending priorities

void dMAX_freePP (dMAX_Handle handle, Uint32 maxid, Uint32 pp)
{
    const Int oldMask = HWI_disable ();

    handle->max[maxid]->ppalloc &= ~(1<<pp);

    HWI_restore(oldMask);
}

// -----------------------------------------------------------------------------
// dMAX set CPU event

void dMAX_setCPUEvt (Uint32 evt)
{
}

// -----------------------------------------------------------------------------
// dMAX event to CPU OUT mapping

Int32 dMAX_evt2CPUOUT (Int32 evt)
{
    return -1;
}

// -----------------------------------------------------------------------------

/* dMAX isr */ 
void dMAX_isr(
    SEM_Handle sem)
{
    SEM_ipost(sem);
}

/* dMAX Fxn Table instantiation */
dMAX_Fxns dMAX_FXNS={
    dMAX_gioRst,
    dMAX_gioRstRel,
    dMAX_evtPol,
    dMAX_enaEvt,
    dMAX_disEvt,
    dMAX_disEvtAll,
    dMAX_evtPri,
    dMAX_clrEvtFlg,
    dMAX_clrEvtFlgAll,
    dMAX_dirInpAll,
    dMAX_rst,
    dMAX_rstRel,
    dMAX_initIRAM,
    dMAX_initDRAM,
    dMAX_updateEET,
    dMAX_updatePTT,
    dMAX_allocPT,
    dMAX_cfgPT,
    dMAX_freePT,
    dMAX_allocEvt,
    dMAX_freeEvt,
    dMAX_allocInt,
    dMAX_mapInt,
    dMAX_freeInt,
    dMAX_allocTCC,
    dMAX_chkTCC,
    dMAX_setTCC,
    dMAX_clrEvtFlg, //clrTcc
    dMAX_freeTCC,
    dMAX_allocFSC,
    dMAX_chkFSC,
    dMAX_setFSC,
    dMAX_clrFSC,
    dMAX_freeFSC,
    dMAX_allocPP,
    dMAX_freePP,
    dMAX_setCPUEvt,
    dMAX_evt2CPUOUT,
    dMAX_isr,
};

