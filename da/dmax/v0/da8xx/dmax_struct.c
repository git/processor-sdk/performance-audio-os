
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX Data structures instantiation 
//
//
//

#include <dmax_code.c>

#include "cslr_dmaxintc.h"
#include "cslr_dmaxpdsp.h"

#include "dmax.h"
#include "dmaxintc.h"
#include "dmax_struct.h"


/* MAX0 Object instantiation */
MAX_Obj MAX0_OBJ={
    (Uint32*) DMAXPDSP0_IRAM_BASEADDR,    // IRAM base 
    dMAX_CODE_MAX0,              // micro code base
    sizeof(dMAX_CODE_MAX0),      // micro code length
    dMAX_MAX0_ID,                // MAX id
    0,                           // unused
    0,                           // pending priorities
    (Uint32*) DMAX_DRAM0_BASEADDR,    // DRAM base
    (Uint32*)(DMAX_DRAM0_BASEADDR+dMAX_EET_BASE), // EET base
    (Uint8*) (DMAX_DRAM0_BASEADDR+dMAX_PTT_BASE), // PTT base
                                 // allocation table uninitialized
};

/* MAX1 Object instantiation */
MAX_Obj MAX1_OBJ={
    (Uint32*) DMAXPDSP1_IRAM_BASEADDR,    // IRAM base 
    dMAX_CODE_MAX1,              // micro code base
    sizeof(dMAX_CODE_MAX1),      // micro code length
    dMAX_MAX1_ID,                // MAX id
    0,                           // unused
    0,                           // pending priorities
    (Uint32*) DMAX_DRAM1_BASEADDR,    // DRAM base
    (Uint32*)(DMAX_DRAM1_BASEADDR+dMAX_EET_BASE), // EET base
    (Uint8*) (DMAX_DRAM1_BASEADDR+dMAX_PTT_BASE), // PTT base
                                 // allocation table uninitialized
};

/* dMAX Object instantiation */
dMAX_Obj dMAX_OBJ={
    &dMAX_FXNS,                  // Fxn table
    (Uint32*)DMAXINTC_BASEADDR,  // GIO base
    0,                           // event allocation
#if 0
    1<<dMAX_INT0|1<<dMAX_INT1,   // interrupt allocation table
#else
    0,
#endif
    0,                           // tcc allocation table
    0,                           // fsc allocation table
    &MAX0_OBJ,                   // MAX0 Object
    &MAX1_OBJ,                   // MAX1 Object
};

/* dMAX Handle instantiation */
dMAX_Handle dMAX_HANDLE=&dMAX_OBJ;

// -----------------------------------------------------------------------------
// dMAX initialization custom

void dMAX_initCustom (dMAX_Handle handle, const Uint32* max0_code, const Uint32* max1_code)
{
    volatile CSL_DmaxintcRegs *pIntc = (volatile CSL_DmaxintcRegs *) DMAXINTC_BASEADDR;
    Uint32 i;


    // INTC global enable
    pIntc->GLBLEN = 1;

    // On Primus we always use a 1-1 mapping between channels and host interrupts
    // TODO: need a symbolic definition for the number of channels/host interrupts
    for (i=0; i < 10; i++) {
        ((volatile unsigned char *) &pIntc->HOSTMAP0)[i] = i;

        // enable host interrupt
        pIntc->HSTINTENIDX = i;
    }
   
    // On Primus, the system events from peripherals are active high pulse (except interrupts?
    // TODO: confirm
    pIntc->POLARITY0 = 0xFFFFFFFF;
    pIntc->TYPE0     = 0x1C000000;

    // software generated events are active high pulse
    // TODO: we must set the polarity explictly (why?)
    pIntc->POLARITY1 = 0xFFFFFFFF;
    pIntc->TYPE1     = 0x00000000;

    // Check for Config override
    if(max0_code)
        handle->max[dMAX_MAX0_ID]->code=max0_code;
    if(max1_code)
        handle->max[dMAX_MAX1_ID]->code=max1_code;

    // Assert GIO,MAX0 and MAX1 reset
    handle->fxns->gioRst(handle);
    for(i=0; i < DMAXPDSP_PER_CNT; ++i)
        handle->fxns->rst(handle,i);

    // Configure GIO for interrupts
    handle->fxns->disEvtAll(handle);
    handle->fxns->dirInpAll(handle);
    handle->fxns->clrEvtFlgAll(handle);

    // Initialize MAX0 and MAX1 IRAM and DRAM
    for(i=0; i < DMAXPDSP_PER_CNT; ++i) {
        handle->fxns->initIRAM (handle,i);
        handle->fxns->initDRAM (handle,i);
    }

    // Deassert GIO,MAX0 and MAX1 reset
    handle->fxns->gioRstRel (handle);
    for (i=0; i < DMAXPDSP_PER_CNT; ++i)
        handle->fxns->rstRel(handle,i);

} //dMAX_initCustom

// -----------------------------------------------------------------------------

void dMAX_init (dMAX_Handle handle)
{
    dMAX_initCustom(handle,NULL,NULL);    
} //dMAX_init

// -----------------------------------------------------------------------------

