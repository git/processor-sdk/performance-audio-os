/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/**
 *  @file   fil_table.h
 *
 *  @brief  This header contains declaration of FIL filter function prototypes.
 *
 */
/*
 *  ======== fil_table.h ========
 *  This header contains declaration of FIL filter function prototypes 
 */
 
#ifndef __FIL_TABLE_
#define __FIL_TABLE_

/* IIR filters */

/** Global function declarations */
/* Tap = 1 */
extern    Int Filter_iirT1Ch1(PAF_FilParam * );
extern    Int Filter_iirT1Ch2(PAF_FilParam * );
extern    Int Filter_iirT1Ch3(PAF_FilParam * );
extern    Int Filter_iirT1Ch4(PAF_FilParam * );
extern    Int Filter_iirT1Ch5_u(PAF_FilParam * );
extern    Int Filter_iirT1Ch6_ui(PAF_FilParam * );
extern    Int Filter_iirT1Ch7_ui(PAF_FilParam * );
extern    Int Filter_iirT1ChN(PAF_FilParam * );
/* Tap = 2 */
extern    Int Filter_iirT2Ch1(PAF_FilParam * );

extern    Int Filter_iirT2Ch2(PAF_FilParam * );

extern    Int Filter_iirT2Ch3(PAF_FilParam * );

extern    Int Filter_iirT2Ch4_u(PAF_FilParam * );
extern    Int Filter_iirT2Ch4_ui(PAF_FilParam * );
extern    Int Filter_iirT2ChN(PAF_FilParam * );
/* Tap = 4 */

/* Tap = N */
extern    Int Filter_iirTNChN(PAF_FilParam * );


/* FIR filters */
extern    Int Filter_firTNCh1(PAF_FilParam * );
extern    Int Filter_firTNChN(PAF_FilParam * );

/* IIR-Cascade */
/* DF2 */
extern    Int Filter_iirT2Ch1_c2_df2( PAF_FilParam *);
extern    Int Filter_iirT2Ch1_c3_df2( PAF_FilParam *);
extern    Int Filter_iirT2Ch1_c4_df2( PAF_FilParam *);
extern    Int Filter_iirT2Ch1_c5_df2( PAF_FilParam *);
extern    Int Filter_iirT2Ch2_c2_i_df2( PAF_FilParam *);
extern    Int Filter_iirT2Ch1_cN_df2( PAF_FilParam *pParam );
extern    Int Filter_iirT2ChN_cN_df2( PAF_FilParam *pParam );
/* No gain */
extern    Int Filter_iirT2Ch1_c4_ng_df2( PAF_FilParam *);
extern    Int Filter_iirT2Ch1_c5_ng_df2( PAF_FilParam *);

/* Customized filters */
/* BM */

extern    Int Filter_iirT2Ch2_ui_mx_BM1( PAF_FilParam *);
extern    Int Filter_iirT2Ch2_ui_mx_BM2( PAF_FilParam *);

extern    Int Filter_iirT1Ch1_mx(PAF_FilParam * );
extern    Int Filter_iirT2Ch1_mx(PAF_FilParam * );
extern    Int Filter_iirT2Ch1_mx_s(PAF_FilParam * );   // mx
extern    Int Filter_iirT2Ch1_ui_mx(PAF_FilParam * );  // mx
extern    Int Filter_iirT2Ch2_mx(PAF_FilParam * );     // mx
extern    Int Filter_iirT2Ch2_u_mx(PAF_FilParam * );   // mx
extern    Int Filter_iirT2Ch2_ui_mx(PAF_FilParam * );  // mx
extern    Int Filter_iirT2ChN_mx(PAF_FilParam * );     // mx
extern    Int Filter_iirT4Ch1_mx(PAF_FilParam * );     // mx
extern    Int Filter_iirT4ChN_mx(PAF_FilParam * );     // mx


#ifdef PAF_DEVICE
/* New kernels for Antara */

extern    Int Filter_iirT1Ch8_u(PAF_FilParam * ); 

extern    Int Filter_iirT2Ch5_u(PAF_FilParam * ); 
extern    Int Filter_iirT2Ch6_u(PAF_FilParam * );
extern    Int Filter_iirT2Ch7_u(PAF_FilParam * );
extern    Int Filter_iirT2Ch8_u(PAF_FilParam * );
extern    Int Filter_iirT1Ch1_mx1(PAF_FilParam * );      // mx1,coef=SP,w=DP
extern    Int Filter_iirT2Ch1_mx1(PAF_FilParam * );      // mx1,coef=SP,w=DP
extern    Int Filter_iirT2Ch2_u_mx1(PAF_FilParam * );    // mx1,coef=SP,w=DP
extern    Int Filter_iirT2Ch3_u_mx1(PAF_FilParam * );    // mx1,coef=SP,w=DP
extern    Int Filter_iirT2Ch4_u_mx1(PAF_FilParam * );    // mx1,coef=SP,w=DP
extern    Int Filter_iirT2ChN_mx1(PAF_FilParam * );      // mx1,coef=SP,w=DP
extern    Int Filter_iirT2ChN_mx1_DF1(PAF_FilParam * );  // mx1,coef=SP,w=DP
extern    Int Filter_iirT4Ch1_mx1(PAF_FilParam * );      // mx1,coef=SP,w=DP
extern    Int Filter_iirT4ChN_mx1(PAF_FilParam * );      // mx1,coef=SP,w=DP

extern    Int Filter_iirT2Ch2_cN_df2( PAF_FilParam *pParam );

extern    Int Filter_iirT2Ch2_c3_u_df2( PAF_FilParam *);  // IIR-Cas DF-2
extern    Int Filter_iirT2Ch2_c4_u_df2( PAF_FilParam *);  // IIR-Cas DF-2
extern    Int Filter_iirT2Ch2_c5_u_df2( PAF_FilParam *);  // IIR-Cas DF-2
/* IIR-Cascade Mixed precision-1 */
/* DF2 */
extern    Int Filter_iirT2Ch1_c2_df2_mx1( PAF_FilParam *);     // IIR-Cas DF-2
extern    Int Filter_iirT2Ch1_cN_df2_mx1( PAF_FilParam *);     // IIR-Cas DF-2
extern    Int Filter_iirT2ChN_cN_df2_mx1( PAF_FilParam *);     // IIR-Cas DF-2
/* No gain */
extern    Int Filter_iirT2Ch1_c2_ng_df2_mx1( PAF_FilParam *);  // IIR-Cas DF-2

/* DF2T */
extern    Int Filter_iirT2Ch1_df2t( PAF_FilParam *pParam );
extern    Int Filter_iirT2Ch2_df2t( PAF_FilParam *pParam );
extern    Int Filter_iirT2Ch3_df2t( PAF_FilParam *pParam );
extern    Int Filter_iirT2Ch5_u_df2t( PAF_FilParam *pParam );
extern    Int Filter_iirT2Ch6_u_df2t( PAF_FilParam *pParam );
extern 	  Int Filter_iirT2ChN_df2t( PAF_FilParam *pParam ) ;

/*  IIR-Cascade DF2t */

extern Int Filter_iirT2Ch1_c2_df2t( PAF_FilParam *pParam ) ;
extern Int Filter_iirT2Ch1_cN_df2t( PAF_FilParam *pParam ) ;
extern Int Filter_iirT2ChN_cN_df2t( PAF_FilParam *pParam ) ;
/* No gain */
extern Int Filter_iirT2Ch1_c2_ng_df2t( PAF_FilParam *pParam ) ;

#endif

#endif /* __FIL_TABLE_ */
