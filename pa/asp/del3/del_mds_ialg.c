/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  DEL Module IALG implementation - MDS's implementation of the
 *  IALG interface for the Speaker Location Delay algoritm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <xdc/std.h>
#include <ti/xdais/ialg.h>

#include <idel.h>
#include <del_mds.h>
#include <del_mds_priv.h>

/*
 *  ======== DEL_MDS_activate ========
 */
  /* COM_TII_activate */

/*
 *  ======== DEL_MDS_alloc ========
 */
Int DEL_MDS_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
                 /* pf cannot be used here! --Kurt */
{
    const IDEL_Params *params = (Void *)algParams;

    Int i, tabCntr = 0;

    if (params == NULL) {
        params = &IDEL_PARAMS;  /* set default parameters */
    }

    /* Validate request as per definition of numAlloc below */
    if (4+params->pStatus->nums >= DEL_MDS_numAlloc())
        return 0; // Error, for now. --Kurt

    /* Request memory for DEL object */
    memTab[tabCntr].size = (sizeof(DEL_MDS_Obj)+3)/4*4;
    memTab[tabCntr].alignment = 4;
    memTab[tabCntr].space = IALG_SARAM;
    memTab[tabCntr++].attrs = IALG_PERSIST;

	memTab[tabCntr].size = (sizeof(IDEL_Status)+3)/4*4;
    memTab[tabCntr].alignment = 4;
    memTab[tabCntr].space = IALG_SARAM;
    memTab[tabCntr++].attrs = IALG_PERSIST;

    memTab[tabCntr].size = (sizeof (Int) + sizeof (XDAS_Int16) + sizeof (XDAS_UInt8) + sizeof (XDAS_UInt8) +
        sizeof (XDAS_UInt16) + sizeof (XDAS_UInt32) + sizeof (PAF_DelayState) * params->pStatus->nums + 3)/4*4;
    memTab[tabCntr].alignment = 4;
    memTab[tabCntr].space = IALG_SARAM;
    memTab[tabCntr++].attrs = IALG_PERSIST;

	memTab[tabCntr].size = (sizeof(IDEL_Scrach)+3)/4*4;
	memTab[tabCntr].alignment = 4;
	memTab[tabCntr].space = IALG_SARAM;
	memTab[tabCntr++].attrs = IALG_PERSIST; // Though scratch, but very small.

	// do not allocate the following if DAT copy isnt used!
	if(params->pConfig->useDATCopy){
	    memTab[tabCntr].size = params->pConfig->sizeofScrach;
	    memTab[tabCntr].alignment = 4;
	    memTab[tabCntr].space = IALG_SARAM;
	    memTab[tabCntr++].attrs = IALG_SCRATCH;
	}


    memTab[tabCntr].size = (sizeof (IDEL_Active)+3)/4*4;
    memTab[tabCntr].alignment = 4;
	if(params->pConfig->useDATCopy)
        memTab[tabCntr].space = IALG_EXTERNAL;
	else
        memTab[tabCntr].space = IALG_SARAM;
    memTab[tabCntr++].attrs = PAF_IALG_COMMONN(params->pConfig->delCommonMemNumber);

    // alignment and sizes are changed for DA8xx build MID#2226
    for (i=0; i < params->pStatus->nums; i++) {
        memTab[tabCntr+i].size = ((sizeof (PAF_AudioData) * params->pConfig->state[i].length)+127)/128*128;
        memTab[tabCntr+i].alignment = 128;
	if(params->pConfig->useDATCopy)
        memTab[tabCntr].space = IALG_EXTERNAL;
	else
        memTab[tabCntr].space = IALG_SARAM;
        memTab[tabCntr+i].attrs = PAF_IALG_COMMONN(params->pConfig->delCommonMemNumber);
    }

    return (tabCntr+i);
}

/*
 *  ======== DEL_MDS_deactivate ========
 */
  /* COM_TII_deactivate */

/*
 *  ======== DEL_MDS_free ========
 */
 /* COM_TII_free */

/*
 *  ======== DEL_MDS_initObj ========
 */
Int DEL_MDS_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    DEL_MDS_Obj *del = (Void *)handle;
    const IDEL_Params *params = (Void *)algParams;

    Int i;

    if (params == NULL) {
        params = &IDEL_PARAMS;  /* set default parameters */
    }

    del->pStatus = memTab[1].base;
    del->pConfig = memTab[2].base;
	del->pScrach = memTab[3].base;
    del->pActive = memTab[5].base;

	del->pScrach->delScrachPtr = memTab[4].base;

    del->pStatus->size = params->pStatus->size;
    del->pStatus->mode = params->pStatus->mode;
    del->pStatus->unit = params->pStatus->unit;
    del->pStatus->numc = params->pStatus->numc;
    del->pStatus->nums = params->pStatus->nums;

	del->pConfig->useDATCopy = params->pConfig->useDATCopy;

    for (i=0; i < params->pStatus->numc; i++)
        del->pStatus->delay[i] = params->pStatus->delay[i];

    del->pConfig->size = params->pConfig->size;
    del->pConfig->mask = params->pConfig->mask;
    for (i=0; i < params->pStatus->nums; i++) {
        del->pConfig->state[i] = params->pConfig->state[i];
        del->pConfig->state[i].base = memTab[6+i].base;
    }
	
	del->pStatus->masterDelay = params->pStatus->masterDelay;
	
    if (params->pActive)
        *del->pActive = *params->pActive;    
    else {
        del->pActive->size = 0;
        del->pActive->flag = 0;
    }

    return (IALG_EOK);
}

/*
 *  ======== DEL_MDS_control ========
 */
  /* COM_TIH_control */

/*
 *  ======== DEL_MDS_moved ========
 */
  /* COM_TII_moved */

/*
 *  ======== DEL_MDS_numAlloc ========
 */
Int DEL_MDS_numAlloc()
{
    return 6+PAF_MAXNUMCHAN; // maximum allowed channels spec'd here
}
