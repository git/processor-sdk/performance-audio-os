
/* 
    Memory regions.
    This command file uses the memory layout of C6424 by default.
    If executing on hardware(non C6424) and not simulator, please
    adjust the below definitions to match your target
    memory map.
    See the manual of your target chip for memory mapping details.
*/

-heap  0x20000
-stack 0x20000


MEMORY
{
    L2RAM:      o = 0x10800000  l = 0x00010000
    SDRAM:       o = 80500000h  l = 00200000h
}

SECTIONS
{
    .args       >   SDRAM
    .bss        >   SDRAM
    .cinit      >   SDRAM
    .cio        >   SDRAM
    .const      >   SDRAM
    .data       >   SDRAM
    .far        >   SDRAM
    .stack      >   SDRAM
    .switch     >   SDRAM
    .sysmem     >   SDRAM
    .text       >   SDRAM
    .ddr2       >   SDRAM
}

