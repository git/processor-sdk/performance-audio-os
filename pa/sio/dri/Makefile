
# 
#  Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 

#
#
# DRI component makefile
#
#
# -----------------------------------------------------------------------------

# First invocation is normally from primary sources directory for the component.
# It is assumed the name of this directory doesn't match either of the standard
# target directory names (i.e. debug or release). So this first call then
# includes the target.mk file which only has one target -- the target directory.
# This is a phony target which causes a recursive call to this file every time
# but rooted in the target directory. Upon this second call the else statement
# is executed which performs the conventional make process.
ifeq (,$(filter debug release ,$(notdir $(CURDIR))))
include ../../build/target.mk
else
include ../../../../build/tools.mk

CFLAGS += --define=DSPLINK --define=MAX_DSPS=1 --define=MAX_PROCESSORS=2 --define=ID_GPP=1 --define=DA8XX --define=PROC_COMPONENT --define=POOL_COMPONENT --define=NOTIFY_COMPONENT --define=MPCS_COMPONENT --define=RINGIO_COMPONENT --define=MPLIST_COMPONENT --define=MSGQ_COMPONENT --define=MSGQ_ZCPY_LINK --define=CHNL_COMPONENT --define=CHNL_ZCPY_LINK --define=ZCPY_LINK --define=PROCID=0 --define=DA8XXGEM --define=DA8XXGEM_INTERFACE=SHMEM_INTERFACE --define=PHYINTERFACE=SHMEM_INTERFACE --define=DSP_TSK_MODE --define=PAF_DEVICE=0xD8000001 --fp_reassoc=on --sat_reassoc=off --symdebug:dwarf --fp_mode=strict --mem_model:const=data --mem_model:data=far --silicon_version=6740 --preproc_with_compile -Dxdc_target_types__="ti/targets/std.h" -Dxdc_target_name__=C674

LIBNAME=dri

# architecture independent settings
SOURCES=											\
    dri.c  \
    dri_params.c \
    dri_ringio.c

INCLUDES += -I"../../../../../da/dmax/v0"
INCLUDES += -I"../.."
INCLUDES += -I"../../../paf"
INCLUDES += -I"../../../../asp/std"
INCLUDES += -I"${LINK_BASE}/dsplink/dsp/inc"
INCLUDES += -I"${LINK_BASE}/dsplink/dsp/inc/DspBios"
INCLUDES += -I"${LINK_BASE}/dsplink/dsp/inc/DspBios/6.XX"
INCLUDES += -I"${LINK_BASE}/dsplink/dsp/inc/DspBios/6.XX/DA8XXGEM"
INCLUDES += -I"${LINK_BASE}/dsplink/dsp/inc/C64XX"
INCLUDES += -I"${LINK_BASE}/dsplink/dsp/BUILD/DA8XXGEM_0/INCLUDE"
INCLUDES += -I"${LINK_BASE}/dsplink/dsp/inc/DA8XXGEM"
INCLUDES += -I"${LINK_BASE}/dsplink/dsp/BUILD/DA8XXGEM_0/INCLUDE"

vpath %.c $(SRCDIR)

include ../../../../build/rules.mk

endif
