
/*
*  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
*  ALL RIGHTS RESERVED 
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  ISRC default instance creation parameters
 */
#include <xdc/std.h>

#include "isrc.h"
#include "src_tih.h"
#include "paftyp.h"

extern float cf_1toH[];
extern float cf_HtoQ[];
extern float cf_1to2[];
extern float cf_2to4[];

// standard memRec

const ISRC_memRec	ISRC_MEMREC_DS2[3] = {
    {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +		// 0 = SRC ALG
     (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
    {sizeof(PAF_AudioData) * 2 * (24 + 128), IALG_SARAM, IALG_PERSIST},				// 1 - SRC FILTER STATE
    {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
     (IALG_MemAttrs)PAF_IALG_COMMONN(0)},											// 2 - SRC SCRATCH
};

const ISRC_memRec	ISRC_MEMREC_DS8[3] = {
    {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +		// 0 = SRC ALG
     (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
    {sizeof(PAF_AudioData) * 8 * (24 + 128), IALG_SARAM, IALG_PERSIST},				// 1 - SRC FILTER STATE
    {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
     (IALG_MemAttrs)PAF_IALG_COMMONN(0)},											// 2 - SRC SCRATCH
};
const ISRC_memRec	ISRC_MEMREC_DS10[3] = {
    {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +		// 0 = SRC ALG
     (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
    {sizeof(PAF_AudioData) * 10 * (24 + 128), IALG_SARAM, IALG_PERSIST},				// 1 - SRC FILTER STATE
    {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
     (IALG_MemAttrs)PAF_IALG_COMMONN(0)},											// 2 - SRC SCRATCH
};

const ISRC_memRec	ISRC_MEMREC_US2[3] = {
    {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +		// 0 - SRC ALG
     (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
    {sizeof(PAF_AudioData) * 2 * (19 + 1 + 131 + 1) / 2, IALG_SARAM, IALG_PERSIST}, // 1 - SRC FILTER STATE
    {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
     (IALG_MemAttrs)PAF_IALG_COMMONN(0)},											// 2 - SRC SCRATCH
};

const ISRC_memRec	ISRC_MEMREC_US8[3] = {
    {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +		// 0 - SRC ALG
     (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
    {sizeof(PAF_AudioData) * 8 * (19 + 1 + 131 + 1) / 2, IALG_SARAM, IALG_PERSIST},	// 1 - SRC FILTER STATE
    {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
     (IALG_MemAttrs)PAF_IALG_COMMONN(0)},											// 2 - SRC SCRATCH
};

const ISRC_memRec	ISRC_MEMREC_US10[3] = {
    {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +		// 0 - SRC ALG
     (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
    {sizeof(PAF_AudioData) * 10 * (19 + 1 + 131 + 1) / 2, IALG_SARAM, IALG_PERSIST},	// 1 - SRC FILTER STATE
    {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
     (IALG_MemAttrs)PAF_IALG_COMMONN(0)},											// 2 - SRC SCRATCH
};


const ISRC_memRec	ISRC_MEMREC_MAX192[3] = {
    {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +		// 0 - SRC ALG
     (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
    {sizeof(PAF_AudioData) * 8 * (24 + 128), IALG_SARAM, IALG_PERSIST},				// 1 - SRC FILTER STATE
    {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
     (IALG_MemAttrs)PAF_IALG_COMMONN(0)},											// 2 - SRC SCRATCH
};

const ISRC_memRec	ISRC_MEMREC_MAX48[3] = {
    {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +		// 0 - SRC ALG
     (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
    {sizeof(PAF_AudioData) * 2 * (24 + 128), IALG_SARAM, IALG_PERSIST},				// 1 - SRC FILTER STATE
    {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
     (IALG_MemAttrs)PAF_IALG_COMMONN(0)},											// 2 - SRC SCRATCH
};

const ISRC_memRec	ISRC_MEMREC_MIN32[3] = {
    {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +		// 0 - SRC ALG
     (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},														// 0 - SRC PERSIST
    {sizeof(PAF_AudioData) * 8 * (19 + 1 + 131 + 1) / 2, IALG_SARAM, IALG_PERSIST},	// 1 - SRC FILTER STATE
    {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
     (IALG_MemAttrs)PAF_IALG_COMMONN(0)},											// 2 - SRC SCRATCH
};

// standard set, high MIPS/quality

const FilterCoefs ISRC_cf_1toH = {
	1,
	24,
	cf_1toH
};

const FilterCoefs ISRC_cf_HtoQ = {
	1,
	128,
	cf_HtoQ
};

const FilterCoefs ISRC_cf_1to2 = {
	1,
	131,
	cf_1to2
};

const FilterCoefs ISRC_cf_2to4 = {
	1,
	19,
	cf_2to4
};

/*
 *  ======== ISRC_PARAMS ========
 *  This static initialization defines the default parameters used to
 *  create instances of SRC objects.
 */

const ISRC_Status ISRC_PARAMS_STATUS = {
    sizeof(ISRC_Status),
    1,
    0x00,
    0x00,
    PAF_SAMPLERATE_UNKNOWN,
    &ISRC_cf_1toH,
    &ISRC_cf_HtoQ,
    &ISRC_cf_1to2,
    &ISRC_cf_2to4
};

const ISRC_Status ISRC_PARAMS_STATUS_DS = {
    sizeof(ISRC_Status),
    1,
    0x00,
    0x00,
    PAF_SAMPLERATE_UNKNOWN,
    &ISRC_cf_1toH,
    &ISRC_cf_HtoQ,
    NULL,
    NULL
};

const ISRC_Status ISRC_PARAMS_STATUS_US = {
    sizeof(ISRC_Status),
    1,
    0x00,
    0x00,
    PAF_SAMPLERATE_UNKNOWN,
	NULL,
    NULL,
    &ISRC_cf_1to2,
    &ISRC_cf_2to4
};


const ISRC_Status ISRC_PARAMS_STATUS_MAX192 = {
    sizeof(ISRC_Status),
    1,
    0x80,
    0x00,
    PAF_SAMPLERATE_UNKNOWN,
    &ISRC_cf_1toH,
    &ISRC_cf_HtoQ,
    &ISRC_cf_1to2,
    &ISRC_cf_2to4
};

const ISRC_Status ISRC_PARAMS_STATUS_MAX48 = {
    sizeof(ISRC_Status),
    1,
    0x82,
    0x00,
    PAF_SAMPLERATE_UNKNOWN,
    &ISRC_cf_1toH,
    &ISRC_cf_HtoQ,
    NULL,
    NULL
};

const ISRC_Status ISRC_PARAMS_STATUS_MIN32 = {
    sizeof(ISRC_Status),
    1,
    0x80,
    0x00,
    PAF_SAMPLERATE_UNKNOWN,
    NULL,
    NULL,
    &ISRC_cf_1to2,
    &ISRC_cf_2to4
};

const ISRC_Params ISRC_PARAMS_DS_2CH = {
    sizeof(ISRC_Params),
    &ISRC_PARAMS_STATUS_DS,
    2,
    3,
	0,
    NULL,
	NULL,
	ISRC_MEMREC_DS2
};

const ISRC_Params ISRC_PARAMS_DS_8CH = {
    sizeof(ISRC_Params),
    &ISRC_PARAMS_STATUS_DS,
    8,
    3,
	0,
    NULL,
	NULL,
	ISRC_MEMREC_DS8
};
const ISRC_Params ISRC_PARAMS_DS_10CH = {
    sizeof(ISRC_Params),
    &ISRC_PARAMS_STATUS_DS,
    10,
    3,
	0,
    NULL,
	NULL,
	ISRC_MEMREC_DS10
};

const ISRC_Params ISRC_PARAMS_US_2CH = {
    sizeof(ISRC_Params),
    &ISRC_PARAMS_STATUS_US,
    2,
    3,
	0,
    NULL,
	NULL,
	ISRC_MEMREC_US2
};

const ISRC_Params ISRC_PARAMS_US_8CH = {
    sizeof(ISRC_Params),
    &ISRC_PARAMS_STATUS_US,
    8,
    3,
	0,
    NULL,
	NULL,
	ISRC_MEMREC_US8
};
const ISRC_Params ISRC_PARAMS_US_10CH = {
    sizeof(ISRC_Params),
    &ISRC_PARAMS_STATUS_US,
    10,
    3,
	0,
    NULL,
	NULL,
	ISRC_MEMREC_US10
};

const ISRC_Params ISRC_PARAMS_MAX192 = {
    sizeof(ISRC_Params),
    &ISRC_PARAMS_STATUS_MAX192,
    8,
    3,
	0,
    NULL,
	NULL,
	ISRC_MEMREC_MAX192
};

const ISRC_Params ISRC_PARAMS_MAX48 = {
    sizeof(ISRC_Params),
    &ISRC_PARAMS_STATUS_MAX48,
    2,
    3,
	0,
    NULL,
	NULL,
	ISRC_MEMREC_MAX48
};

const ISRC_Params ISRC_PARAMS_MIN32 = {
    sizeof(ISRC_Params),
    &ISRC_PARAMS_STATUS_MIN32,
    8,
    3,
	0,
    NULL,
	NULL,
	ISRC_MEMREC_MIN32
};

asm(" .global ISRC_PARAMS ");
asm("ISRC_PARAMS .set ISRC_PARAMS_MAX192");

// EOF
