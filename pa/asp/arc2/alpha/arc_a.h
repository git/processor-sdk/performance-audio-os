
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// ARC Algorithm alpha codes
//
//
//

#ifndef _ARC_A
#define _ARC_A

#include <acpbeta.h>

#define  readARCMode 0xc200+STD_BETA_ARC,0x0400
#define writeARCModeDisable 0xca00+STD_BETA_ARC,0x0400
#define writeARCModeEnable 0xca00+STD_BETA_ARC,0x0401

#define  readARCInputsPerOutputQ24 0xc400+STD_BETA_ARC,0x0008
#define wroteARCInputsPerOutputQ24 0xcc00+STD_BETA_ARC,0x0008 // ,LSW(NN),MSW(NN)
#define writeARCInputsPerOutputQ24(NN) 0xcc00+STD_BETA_ARC,0x0008,LSW(NN),MSW(NN)

#define  readARCOutputsRemainingQ24 0xc400+STD_BETA_ARC,0x000c

#define  readARCMaxAllowedInputsPerOutputQ24     0xc400+STD_BETA_ARC,0x0010
#define wroteARCMaxAllowedInputsPerOutputQ24     0xcc00+STD_BETA_ARC,0x0010 // ,LSW(NN),MSW(NN)

#endif /* _ARC_A */
