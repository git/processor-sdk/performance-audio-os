
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  @file        csl_dat2.c
 *
 * @brief       Implements the CSL DAT2 API using EDMA3 Driver and Resource
 *              Manager Package (EDMA3LLD).
 *              Behaviour mimics DAT2 on EDMA2 hardware
 */
#define NO_EDMA_DRIVER
#define QDMA
#ifndef NO_EDMA_DRIVER

#include <stdio.h>
#include <csl_dat.h>

/*
 * Includes for accessing EDMA3 DRV and RM layer
 */
#include <ti/sdo/edma3/drv/edma3_drv.h>


/*
 * Include for setting up the EDMA3 LLD
 */
#include <csl2_dat_edma3lld.h>


/*
 * extern declarations
 */

/*
 * OS dependent functions, that must be implemented by user of CSL DAT adapter
 * These functions mark the entry and exit to critical sections of the code
 */
extern void _dat_critical_section_enter();
extern void _dat_critical_section_exit();

/*
 * Flag to indicate if EDMA3 LLD was initialized before calling the DAT APIs
 */
extern int DAT_EDMA3LLD_initCalled;

/*
 * typedef declarations
 */

typedef struct DAT_EDMA3LLD_ChannelDesc {

    /* Holds the param number allocated */
    Uint32 paramNo;

    /* Holds the tcc number allocated */
    Uint32 tccNo;

}  DAT_EDMA3LLD_ChannelDesc;

/*
 * static variable definitions
 */

/*
 * Max size array to hold the allocated resources for DAT
 */
static DAT_EDMA3LLD_ChannelDesc
        DAT_allocatedChannels[DAT_EDMA3LLD_HW_MAXPHYCHANNELS];
/*
 * Flag to indicate that the DAT instance has been opened
 */
static Uint32 DAT_EDMA3LLD_openFlag = 0;

/*
 * 64 bit internal Transfer completion register, used to hold status
 * of the channel
 * It has a bit set to one whenever a Transfer is submitted, and cleared
 * when it completes
 */
static volatile Uint32 TransferCompleteH = 0x0;
static volatile Uint32 TransferCompleteL = 0x0;

/*
 * Set to the last allocated bit index number in the TransferComplete fxn
 */
static Uint32 lastAllocatedIndex = DAT_INVALID_ID;

/*
 * static function declarations
 */
/*
 * Obtain the next free channel from available channels
 */
static inline Uint32 _getFreeChannel(Uint32 *tccNum);

/*
 * global variable definitions
 */

/*
 * Holds the EDMA3 driver handle
 */
EDMA3_DRV_Handle DAT_EDMA3LLD_hEdma = NULL;

/*
 * number of EDMA3 channels allocated for the DAT module
 */
int DAT_EDMA3LLD_numAllocatedChannels = 0;


/*
 * global function definitions
 */


/*
 *  ======== DAT_open =========
 * Opens the DAT module, called before all other DAT APIs
 * Ignore all paramters of DAT_open, open any channel,
 * all channels are opened with equal priority
 * All channels can be used in 2D mode etc, flags are not applicable
 */
int edma_DAT_open(int chNum, int priority, Uint32 flags) {

    int i = 0;
    int j = 0;
    Uint32 chaNum = DAT_INVALID_ID;
    Uint32 tccNum = DAT_INVALID_ID;

    chNum = chNum;
    priority = priority;
    flags = flags;

    /*
     * Ensure _initCalled is called before DAT_open
     * Also ensure DAT_open is called only once
     */
    _dat_critical_section_enter();

    if (DAT_EDMA3LLD_initCalled == 0) {
        _dat_critical_section_exit();
        return 0;
    }

    if (1 == DAT_EDMA3LLD_openFlag) {
        _dat_critical_section_exit();
        return 0;
    }
    DAT_EDMA3LLD_openFlag = 1;
    _dat_critical_section_exit();

    /*
     * Request default number of channels and Tccs from the EDMA3 DRV package
     */
    for(i=0; i < DAT_EDMA3LLD_numAllocatedChannels; i++) {
        chaNum = EDMA3_DRV_DMA_CHANNEL_ANY;
        tccNum = EDMA3_DRV_TCC_ANY;

        /*
         * EDMA3 DRV call to request for channel and tcc resources
         */
        if (EDMA3_DRV_SOK != EDMA3_DRV_requestChannel(DAT_EDMA3LLD_hEdma,
                &chaNum, &tccNum,
                (EDMA3_RM_EventQueue)DAT_EDMA3LLD_HW_EVT_QUEUE_ID,
                NULL /*(EDMA3_RM_TccCallback)&_transferComplete*/, NULL)) {

            /*
             * Error requesting channels, Clean up all channels requested so far
             */
            for(j = i-1; j >=0; j--) {
                EDMA3_DRV_freeChannel(DAT_EDMA3LLD_hEdma,
                    DAT_allocatedChannels[j].paramNo);
                DAT_allocatedChannels[i].paramNo = DAT_INVALID_ID;
                DAT_allocatedChannels[i].tccNo = DAT_INVALID_ID;
            }

            if (EDMA3_DRV_SOK != EDMA3_DRV_close(DAT_EDMA3LLD_hEdma, NULL)){
                printf("Error closing DRV instance \n");
            }
            else {
                if (EDMA3_DRV_SOK != EDMA3_DRV_delete(DAT_EDMA3LLD_HW_INST_ID,
                    NULL)){
                    printf("Error deleting EDMA3 DRV\n");
                }
            }

            DAT_EDMA3LLD_openFlag = 0;
            return 0;
        }

        /*
         * Store the allocated Channels in an array
         */
        /* DMA/QDMA Channel */
        DAT_allocatedChannels[i].paramNo = chaNum;
        /* TCC */ 
        DAT_allocatedChannels[i].tccNo = tccNum;
    }

    return 1;
}

/*
 *  ======== DAT_close =========
 * Close the DAT module
 */
void DAT_close() {
    int i = 0;

    /*
     * Ensure DAT_open was called
     */
    _dat_critical_section_enter();
    if (DAT_EDMA3LLD_openFlag == 0)
    {
        _dat_critical_section_exit();
        return;
    }
    DAT_EDMA3LLD_openFlag = 0;
    _dat_critical_section_exit();

    /*
     * Wait for all pending transfers to complete
     */
    edma_DAT_wait(DAT_XFRID_WAITALL);

    /*
     * Free all requested channels
     */
    for(i=0; i < DAT_EDMA3LLD_numAllocatedChannels; i++) {
        EDMA3_DRV_freeChannel(DAT_EDMA3LLD_hEdma,
                DAT_allocatedChannels[i].paramNo);
        DAT_allocatedChannels[i].paramNo = DAT_INVALID_ID;
        DAT_allocatedChannels[i].tccNo = DAT_INVALID_ID;
    }

}

Uint32 edma_DAT_copy(void *src, void *dst, Uint16 byteCnt ) {
    Uint32 chNum = 0;
    Uint32 tccNum = 0;
    Uint32 gacount=128;
    Uint32 acount,bcount,tail;

    acount=gacount;
    if(acount > byteCnt)
              acount = byteCnt;
    bcount = byteCnt/acount;
    tail = byteCnt - bcount*acount;

    if(tail !=0)
    {

       	EDMA3_DRV_PaRAMRegs paramSet = {0,0,0,0,0,0,0,0,0,0,0,0};

    	chNum = _getFreeChannel(&tccNum);

    	paramSet.srcAddr    = (unsigned int)(src);
    	paramSet.destAddr   = (unsigned int)(dst);
    	paramSet.srcBIdx    = 0;
    	paramSet.destBIdx   = 0;
    	paramSet.srcCIdx    = 0;
    	paramSet.destCIdx   = 0;
    	paramSet.aCnt       = tail;
    	paramSet.bCnt       = 1;
		/* Trigger word is CCNT */
    	paramSet.cCnt       = 1;
    	paramSet.bCntReload = 0;
    	paramSet.linkAddr   = 0xFFFFu;
    	/* Src & Dest are in INCR modes */
    	paramSet.opt &= 0xFFFFFFFCu;
    	/* Program the TCC */
    	paramSet.opt |= ((tccNum << OPT_TCC_SHIFT) & OPT_TCC_MASK);

    	paramSet.opt |= (1 << OPT_ITCCHEN_SHIFT);

    	/* Enable Final transfer completion interrupt */

    	paramSet.opt |= (1 << OPT_TCINTEN_SHIFT);

		/* A Sync Transfer Mode */
    	paramSet.opt |= (0 << OPT_SYNCDIM_SHIFT);
		/* Clear the static bit */
    	paramSet.opt |= (0 << OPT_STATIC_SHIFT);


	/* Now, write the PaRAM Set. */
        EDMA3_DRV_setPaRAM(DAT_EDMA3LLD_hEdma, chNum, &paramSet);

    /*
     * Set a 1 bit in the TransferComplete register corresponding to the tcc
     */
       if (tccNum < 32) {
          SET_REGISTER32_BIT(TransferCompleteL,tccNum);
       }
       else {
            SET_REGISTER32_BIT(TransferCompleteH,tccNum - 32);
       }

    /*
     * Clear error bits before starting transfer
     */
       EDMA3_DRV_clearErrorBits(DAT_EDMA3LLD_hEdma, chNum);

    /*
     * Enable transfer
     */
       if (EDMA3_DRV_SOK != EDMA3_DRV_enableTransfer(DAT_EDMA3LLD_hEdma,chNum,
            EDMA3_DRV_TRIG_MODE_MANUAL)) {

        printf("Error enabling transfer \n");
        if (tccNum < 32) {
            CLEAR_REGISTER32_BIT(TransferCompleteL,tccNum);
        }
        else {
            CLEAR_REGISTER32_BIT(TransferCompleteH,tccNum - 32);
        }

        return DAT_INVALID_ID;
       }

       edma_DAT_wait(tccNum);

//      memcpy (dst, src, tail);

    }

    {
        EDMA3_DRV_PaRAMRegs paramSet = {0,0,0,0,0,0,0,0,0,0,0,0};
        chNum = 0;
        tccNum = 0;
        chNum = _getFreeChannel(&tccNum);

            /*
                 * Set up Transfer Paramters for this channel
                 */
                 /* Fill the PaRAM Set with transfer specific information */

        paramSet.srcAddr    = (unsigned int)(src) + tail;
        paramSet.destAddr   = (unsigned int)(dst) + tail;
        paramSet.srcBIdx    = acount;
        paramSet.destBIdx   = acount;
        paramSet.srcCIdx    = 0;
        paramSet.destCIdx   = 0;
        paramSet.aCnt       = acount;
        paramSet.bCnt       = bcount;
	/* Trigger word is CCNT */
        paramSet.cCnt       = 1;
        paramSet.bCntReload = 0;
        paramSet.linkAddr   = 0xFFFFu;
        /* Src & Dest are in INCR modes */
        paramSet.opt &= 0xFFFFFFFCu;
       /* Program the TCC */
        paramSet.opt |= ((tccNum << OPT_TCC_SHIFT) & OPT_TCC_MASK);

        paramSet.opt |= (1 << OPT_ITCCHEN_SHIFT);

    	/* Enable Final transfer completion interrupt */
    	paramSet.opt |= (1 << OPT_TCINTEN_SHIFT);

	/* AB Sync Transfer Mode */
        paramSet.opt |= (0 << OPT_SYNCDIM_SHIFT);
	/* Set the static bit */
        paramSet.opt |= (0 << OPT_STATIC_SHIFT);

     /*
     * Set a 1 bit in the TransferComplete register corresponding to the tcc
     */
        if (tccNum < 32) {
                 SET_REGISTER32_BIT(TransferCompleteL,tccNum);
        }
        else {
             SET_REGISTER32_BIT(TransferCompleteH,tccNum - 32);
        }

	/* trigger the transfer */

	EDMA3_DRV_setPaRAM(DAT_EDMA3LLD_hEdma, chNum, &paramSet);



	/*
          * Clear error bits before starting transfer
        */
        EDMA3_DRV_clearErrorBits(DAT_EDMA3LLD_hEdma, chNum);

            /*
              * Enable transfer
             */
        if (EDMA3_DRV_SOK != EDMA3_DRV_enableTransfer(DAT_EDMA3LLD_hEdma,chNum,
            EDMA3_DRV_TRIG_MODE_MANUAL)) {

            printf("Error enabling transfer \n");
            if (tccNum < 32) {
               CLEAR_REGISTER32_BIT(TransferCompleteL,tccNum);
            }
            else {
               CLEAR_REGISTER32_BIT(TransferCompleteH,tccNum - 32);
            }

            return DAT_INVALID_ID;
        }
    }
	return tccNum;
}

/*
 *  ======== DAT_copy2d =========
 * 2-dimensional copy from src to dst of lineCnt lines each of length lineLen
 * bytes. The pitch for the second dimension is linePitch bytes
 */
Uint32 DAT_copy2d(Uint32 type, void *src, void *dst, Uint16 lineLen,
        Uint16 lineCnt, Uint16 linePitch) {
    Uint32 chNum = 0;
    Uint32 tccNum = 0;
    EDMA3_DRV_PaRAMRegs paramSet = {0,0,0,0,0,0,0,0,0,0,0,0};

    /*
     * Obtain a free channel
     */
    chNum = _getFreeChannel(&tccNum);

    /* Fill the PaRAM Set with transfer specific information */
    paramSet.srcAddr    = (unsigned int)(src);
    paramSet.destAddr   = (unsigned int)(dst);
    paramSet.aCnt       = lineLen;
    paramSet.bCnt       = lineCnt;
	/* Trigger word is CCNT */
    paramSet.cCnt       = 1;
    paramSet.bCntReload = 0;
    paramSet.linkAddr   = 0xFFFFu;
    /* Src & Dest are in INCR modes */
    paramSet.opt &= 0xFFFFFFFCu;
    /* Program the TCC */
    paramSet.opt |= ((tccNum << OPT_TCC_SHIFT) & OPT_TCC_MASK);
    /* Enable Final transfer completion interrupt */
    paramSet.opt |= (1 << OPT_TCINTEN_SHIFT);
	/* AB Sync Transfer Mode */
    paramSet.opt |= (1 << OPT_SYNCDIM_SHIFT);
	/* Set the static bit */
    paramSet.opt |= (1 << OPT_STATIC_SHIFT);

    /*
     * Depending on type of transfer set the src and dest BIdx
     * Different types of transfers differ only in the Src and Dst BIdx-es
     */
    switch (type) {
        case DAT_1D2D:
		    paramSet.srcBIdx    = lineLen;
		    paramSet.destBIdx   = linePitch;
		    paramSet.srcCIdx    = 0;
		    paramSet.destCIdx   = 0;
            break;
        case DAT_2D1D:
		    paramSet.srcBIdx    = linePitch;
		    paramSet.destBIdx   = lineLen;
		    paramSet.srcCIdx    = 0;
		    paramSet.destCIdx   = 0;
            break;
        case DAT_2D2D:
		    paramSet.srcBIdx    = linePitch;
		    paramSet.destBIdx   = linePitch;
		    paramSet.srcCIdx    = 0;
		    paramSet.destCIdx   = 0;
            break;
    }

    /*
     * Set a 1 bit in the TransferComplete register corresponding to the tcc
     */
    if (tccNum < 32) {
        SET_REGISTER32_BIT(TransferCompleteL,tccNum);
    }
    else {
        SET_REGISTER32_BIT(TransferCompleteH,tccNum - 32);
    }

	/* trigger the transfer */
	EDMA3_DRV_setPaRAM(DAT_EDMA3LLD_hEdma, chNum, &paramSet);

	return tccNum;
}


/*
 *  ======== DAT_fill =========
 * Fills up dst with byteCnt bytes of the pattern pointed to be 'value'
 */
Uint32 DAT_fill(void *dst, Uint16 byteCnt, Uint32 *value) {
    Uint32 chNum = 0;
    Uint32 tccNum = 0;
    EDMA3_DRV_PaRAMRegs paramSet = {0,0,0,0,0,0,0,0,0,0,0,0};

    /*
     * Obtain a free channel
     */
    chNum = _getFreeChannel(&tccNum);

    /* Fill the PaRAM Set with transfer specific information */
    paramSet.srcAddr    = (unsigned int)(value);
    paramSet.destAddr   = (unsigned int)(dst);
    paramSet.srcBIdx    = 0;
    paramSet.destBIdx   = 8;
    paramSet.srcCIdx    = 0;
    paramSet.destCIdx   = 0;
    paramSet.aCnt       = 8;
    paramSet.bCnt       = byteCnt>>3;
	/* Trigger word is CCNT */
    paramSet.cCnt       = 1;
    paramSet.bCntReload = 0;
    paramSet.linkAddr   = 0xFFFFu;
    /* Src & Dest are in INCR modes */
    paramSet.opt &= 0xFFFFFFFCu;
    /* Program the TCC */
    paramSet.opt |= ((tccNum << OPT_TCC_SHIFT) & OPT_TCC_MASK);
    /* Enable Final transfer completion interrupt */
    paramSet.opt |= (1 << OPT_TCINTEN_SHIFT);
	/* AB Sync Transfer Mode */
    paramSet.opt |= (1 << OPT_SYNCDIM_SHIFT);
	/* Set the static bit */
    paramSet.opt |= (1 << OPT_STATIC_SHIFT);

    /*
     * Set a 1 bit in the TransferComplete register corresponding to the tcc
     */
    if (tccNum < 32) {
        SET_REGISTER32_BIT(TransferCompleteL,tccNum);
    }
    else {
        SET_REGISTER32_BIT(TransferCompleteH,tccNum - 32);
    }

	/* trigger the transfer */
	EDMA3_DRV_setPaRAM(DAT_EDMA3LLD_hEdma, chNum, &paramSet);

	return tccNum;
}



/*
 *  ======== DAT_wait =========
 * Wait for the transfer identified by waitId, to complete
 */
void edma_DAT_wait(Uint32 waitId) {
	int i;
	Uint32 tcc;

    /*
     * If both the registers are zero, we're done !!
     */
    if (0x0 == (TransferCompleteL | TransferCompleteH)) {
        return;
    }

    /*
     * Check if we need to wait for all transfers or just this one ?
     */
    if (DAT_XFRID_WAITALL == waitId) {
		for (i = 0 ; i < DAT_EDMA3LLD_numAllocatedChannels; i++) {
			tcc = DAT_allocatedChannels[i].tccNo;
			if (tcc < 32) {
				if (GET_REGISTER32_BIT(TransferCompleteL, tcc)) {
					EDMA3_DRV_waitAndClearTcc (DAT_EDMA3LLD_hEdma, tcc);
					CLEAR_REGISTER32_BIT(TransferCompleteL, tcc);
				}
			} else {
				if (GET_REGISTER32_BIT(TransferCompleteH, tcc - 32)) {
					EDMA3_DRV_waitAndClearTcc (DAT_EDMA3LLD_hEdma, tcc);
					CLEAR_REGISTER32_BIT(TransferCompleteH, tcc - 32);
				}
			}
		}
    } else {
    	EDMA3_DRV_waitAndClearTcc (DAT_EDMA3LLD_hEdma, waitId);

		/*
	     * Mark zero in bit position tccNum
	     */
	    if (waitId < 32) {
	        CLEAR_REGISTER32_BIT(TransferCompleteL, waitId );
	    }
	    else {
	        CLEAR_REGISTER32_BIT(TransferCompleteH, waitId -32 );
	    }
	}
}

/*
 *  ======== DAT_busy =========
 * Check the busy status of transfer identified by waitId
 */
int DAT_busy(Uint32 waitId) {
	Uint16 status = 0;

    /*
     * Check if the particular transfer has completed by returning the status
     * from the itnernal completion register
     */
    if (waitId < 32) {
        if (GET_REGISTER32_BIT(TransferCompleteL,waitId)) {
			/** Bit is still set, check in the IPR and clear it
			 * if the xfer has finished. Clear the local reg too.
			 * Else leave everything like that
			 */
			EDMA3_DRV_checkAndClearTcc(DAT_EDMA3LLD_hEdma, waitId, &status);
			if (status == 1) {
				/* xfer finished, update local reg */
				CLEAR_REGISTER32_BIT(TransferCompleteL, waitId);
				return 0;
			} else {
				/* xfer not finished, leave... */
				return 1;
			}

        } else
        	return 0;
    }
    else {
        if (GET_REGISTER32_BIT(TransferCompleteH,waitId - 32)) {
			EDMA3_DRV_checkAndClearTcc(DAT_EDMA3LLD_hEdma, waitId, &status);
			if (status == 1) {
				CLEAR_REGISTER32_BIT(TransferCompleteH, waitId - 32);
				return 0;
			} else {
				return 1;
			}
        } else
        	return 0;
    }
}

/*
 * static function definitions
 */

/*
 *  ======== _getFreeChannel =========
 * Used to obtain the next available channel to set up a new transfer
 * Function spins till a channel becomes available
 */
static inline Uint32 _getFreeChannel(Uint32 *tccNum) {
    Uint32 chNum,index ;
	Uint16 status=0;

    /*
     * Start looking for available channels from the index after the one
     * that was allocated previously
     */
    index = (lastAllocatedIndex + 1)%(DAT_EDMA3LLD_numAllocatedChannels);

    /*
     * Spins till a free bit in TransferComplete is found
     */
    while (1) {
        *tccNum = DAT_allocatedChannels[index].tccNo;

        if (*tccNum < 32) {
            if((GET_REGISTER32_BIT(TransferCompleteL,*tccNum)) == 0) {
				/* tcc not allocated or already freed */
                chNum = DAT_allocatedChannels[index].paramNo;
                lastAllocatedIndex = index;
                return chNum;
            } else {
	            /** Tcc allocated but not yet freed, check the IPR bit now
	             * corresponding to this tcc
	             * if the bit is set, it means that xfer has finished
	             * so clear it and use this tcc
	             * else leave it like that
				 */
            	EDMA3_DRV_checkAndClearTcc(DAT_EDMA3LLD_hEdma, *tccNum, &status);
				if (1 == status)
					{
					/* xfer completed, IPR cleared too */
					chNum = DAT_allocatedChannels[index].paramNo;
		            lastAllocatedIndex = index;

					/* clear the reg too */
					CLEAR_REGISTER32_BIT(TransferCompleteL, *tccNum);
		            return chNum;
					}
            }
            
        }
        else {
            if((GET_REGISTER32_BIT(TransferCompleteH,*tccNum - 32)) == 0) {
                chNum = DAT_allocatedChannels[index].paramNo;
                lastAllocatedIndex = index;
                return chNum;
            } else {
            	EDMA3_DRV_checkAndClearTcc(DAT_EDMA3LLD_hEdma, *tccNum, &status);
				if (1 == status)
					{
					chNum = DAT_allocatedChannels[index].paramNo;
		            lastAllocatedIndex = index;

					CLEAR_REGISTER32_BIT(TransferCompleteH, *tccNum - 32);
		            return chNum;
					}
            }
        }
		
        /*
         * Increment index
         */
        index = (index + 1)%(DAT_EDMA3LLD_numAllocatedChannels);
    }
}
#else
#define RDE_EDMA_CP_BASE_CH (10)
#include "primus.h"
#include "cslr_edmacc_001.h"
#include "stdio.h"

#define NUM_DEPTH 4
char qdma_chan[]={0,1,2,3,};
#ifdef QDMA
char edma_chan[]={64,65,66,67};
#else
char edma_chan[]={22,23,24,25};
#endif
char tcc[]={22,23,24,25};
char chanAvailable[]={1,1,1,1,};

int edma_DAT_setup()
{
	int i;
	for(i=0;i<NUM_DEPTH;i++)
		if(chanAvailable[i]==1)
			return i;
	return -1;
}

int edma_DAT_open(int chNum, int priority, Uint32 flags) {
}

#ifdef QDMA
int gArr[256];
void edma_DAT_wait(Uint32 index) {
	int i;

	volatile CSL_EdmaccRegsOvly edmaccRegs;
	int param,cnt=0;
	int waitId=tcc[index];
 edmaccRegs = (CSL_EdmaccRegsOvly) CSL_EDMACC_0_REGS;

	  while (0x1 != (((edmaccRegs->IPR)>>waitId) & 0x1)) {
		  param = edma_chan[index];
		  //gArr[cnt++]=edmaccRegs->PARAMENTRY[param].A_B_CNT;
		  //printf("%d\n",edmaccRegs->PARAMENTRY[param].A_B_CNT);

    }
    (edmaccRegs->ICR)=edmaccRegs->ICR |(1<<waitId);
    chanAvailable[index]=1;
    return;

}

Uint32 edma_DAT_copy(void *srcPtr, void *dstPtr, Uint32 byteCnt)
{
	Uint16 param;
	 Uint32 chNum = 0;
	    Uint32 tccNum = 0;
	    Uint32 gacount=128;
	    Uint32 acount,bcount,tail;
	    int index;
	    CSL_EdmaccRegsOvly edmaccRegs;
	    	CSL_EdmaccParamentryRegs * paramStruct;
	    acount=gacount;
	    if(acount > byteCnt)
	              acount = byteCnt;
	    bcount = byteCnt/acount;
	    tail = byteCnt - bcount*acount;


	edmaccRegs = (CSL_EdmaccRegsOvly) CSL_EDMACC_0_REGS;

	index=edma_DAT_setup();
	if(index == -1)
	{
	//	printf("More than  NUM_DEPTH simultaneous copies not allowed");
		return index;
	}
		edmaccRegs->ICR |= 1<<(tcc[index]);

		// mapping qdma channel 0 to param set 10 and trigger word is dst ie 3
		edmaccRegs->QCHMAP[qdma_chan[index]]=(edma_chan[index]<<5)+ 0x0c;
		//shadow region
		//edmaccRegs->QRAE[0] |= 1;
		// enable qdma channel 0 in QEESR
		edmaccRegs->QEESR |= (1 << qdma_chan[index]);
		//map qdma channel to event que 0
		edmaccRegs->QDMAQNUM = qdma_chan[index];

		param = edma_chan[index];

		 edmaccRegs->PARAMENTRY[param].OPT = \
				 		CSL_FMKT(EDMACC_OPT_PRIVID, RESETVAL) | \
				         CSL_FMKT(EDMACC_OPT_ITCCHEN, DISABLE) | \
				         CSL_FMKT(EDMACC_OPT_TCCHEN, DISABLE) | \
				         CSL_FMKT(EDMACC_OPT_ITCINTEN, ENABLE) | \
				         CSL_FMKT(EDMACC_OPT_TCINTEN, ENABLE) | \
				         CSL_FMK(EDMACC_OPT_TCC, tcc[index]) | \
				         CSL_FMKT(EDMACC_OPT_TCCMODE, NORMAL) | \
				         CSL_FMKT(EDMACC_OPT_FWID, RESETVAL) | \
				         CSL_FMKT(EDMACC_OPT_STATIC, STATIC) | \
				         CSL_FMKT(EDMACC_OPT_SYNCDIM, ABSYNC) | \
				         CSL_FMKT(EDMACC_OPT_DAM, INCR) | \
				         CSL_FMKT(EDMACC_OPT_SAM, INCR);
		 edmaccRegs->PARAMENTRY[param].LINK_BCNTRLD = 0;;
		 // 16 bytes offset between each I or Q sample
		 		 	edmaccRegs->PARAMENTRY[param].SRC_DST_CIDX = 0;
		 			// number of I&Q samples to store before triggering a completion interrupt
		 				 	edmaccRegs->PARAMENTRY[param].CCNT = 1;


	 				 	edmaccRegs->PARAMENTRY[param].SRC_DST_BIDX = acount + (acount<<16);
	 if(tail !=0)
	    {
		 edmaccRegs->PARAMENTRY[param].SRC = (uint32_t) srcPtr;
		 	// BCnt = # of words in receive buffer = 8, ACNT = 2 = 2 byte
		 	edmaccRegs->PARAMENTRY[param].A_B_CNT = tail+( 1 <<16);
		 	edmaccRegs->PARAMENTRY[param].DST = (uint32_t)dstPtr;;//SDR_RxPing1;
		 	//memcpy((void*)&edmaccRegs->PARAMENTRY[param],(void*)&paramStruct,32);
		 		edma_DAT_wait(index);

	    }

			 	// 2 bytes offset between I & Q

			 edmaccRegs->PARAMENTRY[param].SRC = (uint32_t) srcPtr+tail;
			 	// BCnt = # of words in receive buffer = 8, ACNT = 2 = 2 byte
			 	edmaccRegs->PARAMENTRY[param].A_B_CNT = acount+( bcount <<16);
			 	edmaccRegs->PARAMENTRY[param].DST = (uint32_t)dstPtr+tail;;//SDR_RxPing1;
			 	//memcpy((void*)&edmaccRegs->PARAMENTRY[param],(void*)&paramStruct,32);
			 	chanAvailable[index]=0;
	return index;
}
#else

int gcopy;
void RDE_CP_ISR()
{
		//SEM_post(&SEM_RDECpy);
		gcopy=1;
}

#if 0
int edma_DAT_open(int chNum, int priority, Uint32 flags) {
		Uint16 param;
	CSL_EdmaccRegsOvly edmaccRegs;
	edmaccRegs = (CSL_EdmaccRegsOvly ) CSL_EDMACC_0_REGS;

	edmaccRegs->DRA[1].DRAE |= 1<<(RDE_EDMA_CP_BASE_CH);

	edmaccRegs->ICR |= 1<<(RDE_EDMA_CP_BASE_CH);

	edmaccRegs->IESR |= 1<<(RDE_EDMA_CP_BASE_CH);

	/* Clear the Secondary Event Enable Registers */
	edmaccRegs->ECR |= 1<<(RDE_EDMA_CP_BASE_CH);
	edmaccRegs->EECR |= 1<<(RDE_EDMA_CP_BASE_CH);
	edmaccRegs->SECR |= 1<<(RDE_EDMA_CP_BASE_CH);

	edmaccRegs->DMAQNUM[1] = 0x00000100;

	param = RDE_EDMA_CP_BASE_CH;
	edmaccRegs->PARAMENTRY[param].OPT = \
		CSL_FMKT(EDMACC_OPT_PRIVID, RESETVAL) | \
        CSL_FMKT(EDMACC_OPT_ITCCHEN, DISABLE) | \
        CSL_FMKT(EDMACC_OPT_TCCHEN, DISABLE) | \
        CSL_FMKT(EDMACC_OPT_ITCINTEN, DISABLE) | \
        CSL_FMKT(EDMACC_OPT_TCINTEN, DISABLE) | \
        CSL_FMK(EDMACC_OPT_TCC, RDE_EDMA_CP_BASE_CH) | \
        CSL_FMKT(EDMACC_OPT_TCCMODE, NORMAL) | \
        CSL_FMKT(EDMACC_OPT_FWID, RESETVAL) | \
        CSL_FMKT(EDMACC_OPT_STATIC, NORMAL) | \
        CSL_FMKT(EDMACC_OPT_SYNCDIM, ABSYNC) | \
        CSL_FMKT(EDMACC_OPT_DAM, INCR) | \
        CSL_FMKT(EDMACC_OPT_SAM, INCR);
        //edmaEventHook(RDE_EDMA_CP_BASE_CH,RDE_CP_ISR); 
}
#endif
void RID_triggerEDMA(int32_t channel)
{
        CSL_EdmaccRegsOvly edmaccRegs;
        edmaccRegs = (CSL_EdmaccRegsOvly)CSL_EDMACC_0_REGS;

        if (channel < 32)
                edmaccRegs->ESR |= (1 << channel);
    else
                edmaccRegs->ESRH |= (1 << (channel-32));
}
int edma_Channel_Enable(Uint32 channel)
{
	CSL_EdmaccRegsOvly edmaccRegs;
	edmaccRegs = (CSL_EdmaccRegsOvly) CSL_EDMACC_0_REGS;

	if (channel <= 63)
	{
		if (channel < 32)
			edmaccRegs->EESR |= (1 << channel);
		else
			edmaccRegs->EESRH |= (1 << (channel - 32));

		return 0;
	}
	else
		return 1;
}


int gArr[512];
int gcnt=0;
void edma_DAT_wait(Uint32 index) {
	int i;

	volatile CSL_EdmaccRegsOvly edmaccRegs;
	int param;
	int waitId=tcc[index];
 edmaccRegs = (CSL_EdmaccRegsOvly) CSL_EDMACC_0_REGS;

	  while (0x1 != (((edmaccRegs->IPR)>>waitId) & 0x1)) {
		  param = edma_chan[index];
		  gArr[gcnt++]=edmaccRegs->PARAMENTRY[param].A_B_CNT;
		  if(gcnt>500)
			  gcnt=0;
		  //printf("%d\n",edmaccRegs->PARAMENTRY[param].A_B_CNT);

    }
    (edmaccRegs->ICR)=edmaccRegs->ICR |(1<<waitId);
    chanAvailable[index]=1;
    return;

}

Uint32 edma_DAT_copy(void *srcPtr, void *dstPtr, int byteCnt)
{
	Uint16 param;
	int index;
	 Uint32 chNum = 0;
	    Uint32 tccNum = 0;
	    Uint32 gacount=128;
	    Uint32 acount,bcount,tail;
	    CSL_EdmaccRegsOvly edmaccRegs;
	    	CSL_EdmaccParamentryRegs paramStruct;
	    acount=gacount;
	    if(acount > byteCnt)
	              acount = byteCnt;
	    bcount = byteCnt/acount;
	    tail = byteCnt - bcount*acount;


	edmaccRegs = (CSL_EdmaccRegsOvly) CSL_EDMACC_0_REGS;
	
	//param = RDE_EDMA_CP_BASE_CH;

	index=edma_DAT_setup();
	param = edma_chan[index];
		if(index == -1)
		{
			printf("More than  NUM_DEPTH simultaneous copies not allowed");
			return index;
		}
		edmaccRegs->DRA[1].DRAE |= 1<<(tcc[index]);

			edmaccRegs->ICR |= 1<<(tcc[index]);

			edmaccRegs->IESR |= 1<<(tcc[index]);

			/* Clear the Secondary Event Enable Registers */
			edmaccRegs->ECR |= 1<<(tcc[index]);
			edmaccRegs->EECR |= 1<<(tcc[index]);
			edmaccRegs->SECR |= 1<<(tcc[index]);

			edmaccRegs->DMAQNUM[1] = 0x00000100;
	 if(tail !=0)
	    {
		 paramStruct.OPT = \
		 		CSL_FMKT(EDMACC_OPT_PRIVID, RESETVAL) | \
		         CSL_FMKT(EDMACC_OPT_ITCCHEN, DISABLE) | \
		         CSL_FMKT(EDMACC_OPT_TCCHEN, DISABLE) | \
		         CSL_FMKT(EDMACC_OPT_ITCINTEN, ENABLE) | \
		         CSL_FMKT(EDMACC_OPT_TCINTEN, ENABLE) | \
		         CSL_FMK(EDMACC_OPT_TCC, tcc[index]) | \
		         CSL_FMKT(EDMACC_OPT_TCCMODE, NORMAL) | \
		         CSL_FMKT(EDMACC_OPT_FWID, RESETVAL) | \
		         CSL_FMKT(EDMACC_OPT_STATIC, STATIC) | \
		         CSL_FMKT(EDMACC_OPT_SYNCDIM, ASYNC) | \
		         CSL_FMKT(EDMACC_OPT_DAM, INCR) | \
		         CSL_FMKT(EDMACC_OPT_SAM, INCR);


		 	// 2 bytes offset between I & Q
		 	paramStruct.SRC_DST_BIDX = 0;
		 	paramStruct.LINK_BCNTRLD = 0;;
		 	// 16 bytes offset between each I or Q sample
		 	paramStruct.SRC_DST_CIDX = 0;
		 	// number of I&Q samples to store before triggering a completion interrupt
		 	paramStruct.CCNT = 1;
		 paramStruct.SRC = (uint32_t) srcPtr;
		 	// BCnt = # of words in receive buffer = 8, ACNT = 2 = 2 byte
		 	paramStruct.A_B_CNT = tail+( 1 <<16);
		 	paramStruct.DST = (uint32_t)dstPtr;;//SDR_RxPing1;
		 	memcpy((void*)&edmaccRegs->PARAMENTRY[param],(void*)&paramStruct,32);
		 		edma_Channel_Enable(tcc[index]);
		 		RID_triggerEDMA(tcc[index]);
		 		edma_DAT_wait(index);

	    }

	 paramStruct.OPT = \
			 		CSL_FMKT(EDMACC_OPT_PRIVID, RESETVAL) | \
			         CSL_FMKT(EDMACC_OPT_ITCCHEN, DISABLE) | \
			         CSL_FMKT(EDMACC_OPT_TCCHEN, DISABLE) | \
			         CSL_FMKT(EDMACC_OPT_ITCINTEN, ENABLE) | \
			         CSL_FMKT(EDMACC_OPT_TCINTEN, ENABLE) | \
			         CSL_FMK(EDMACC_OPT_TCC, tcc[index]) | \
			         CSL_FMKT(EDMACC_OPT_TCCMODE, NORMAL) | \
			         CSL_FMKT(EDMACC_OPT_FWID, RESETVAL) | \
			         CSL_FMKT(EDMACC_OPT_STATIC, STATIC) | \
			         CSL_FMKT(EDMACC_OPT_SYNCDIM, ABSYNC) | \
			         CSL_FMKT(EDMACC_OPT_DAM, INCR) | \
			         CSL_FMKT(EDMACC_OPT_SAM, INCR);


			 	// 2 bytes offset between I & Q
			 	paramStruct.SRC_DST_BIDX = acount + (acount<<16);
			 	paramStruct.LINK_BCNTRLD = 0;;
			 	// 16 bytes offset between each I or Q sample
			 	paramStruct.SRC_DST_CIDX = 0;
			 	// number of I&Q samples to store before triggering a completion interrupt
			 	paramStruct.CCNT = 1;
			 paramStruct.SRC = (uint32_t) srcPtr+tail;
			 	// BCnt = # of words in receive buffer = 8, ACNT = 2 = 2 byte
			 	paramStruct.A_B_CNT = acount+( bcount <<16);
			 	paramStruct.DST = (uint32_t)dstPtr+tail;;//SDR_RxPing1;
			 	memcpy((void*)&edmaccRegs->PARAMENTRY[param],(void*)&paramStruct,32);
			 		edma_Channel_Enable(tcc[index]);
			 		RID_triggerEDMA(tcc[index]);


	//SEM_pend(&SEM_RDECpy,SYS_FOREVER);
	gcopy=0;
	return index;
}
#endif

#endif
#include <ti/sysbios/hal/Cache.h>
Uint32 DAT_cacheop_and_copy(void *src, void *dst, Uint16 byteCnt ) {
Cache_wb(src,byteCnt,Cache_Type_ALL,TRUE);
Cache_inv(dst,byteCnt,Cache_Type_ALL,TRUE);
return edma_DAT_copy(src,dst, byteCnt );
}
