/****************************************************************************/
/*  c66_conversion_macros.h                                                 */
/****************************************************************************/

/* 
 *
 * Copyright (C) 1997-2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* Assume this is included in C6xSimulator.h */

/* These macros convert between how the host intrinsics implementation       */
/* represents the types and how the CPU specification C code model           */
/* represents those same types.                                              */

#ifndef C66_CONVERSION_MACROS_H
#define C66_CONVERSION_MACROS_H

#ifndef _TMS320C6X

#if defined(TMS320C66X) || defined(TMS320C67X)

#define INT64_LL_TO_DWORD(from, to)                              \
   do {                                                          \
      to.high = (uword) ((from >> 32) & 0xffffffff);             \
      to.low  = (uword) (from & 0xffffffff);                     \
   } while (0)

#define __X128_T_TO_QWORD(from, to)                              \
   do {                                                          \
      to.word0 = from.word0;                                     \
      to.word1 = from.word1;                                     \
      to.word2 = from.word2;                                     \
      to.word3 = from.word3;                                     \
   } while (0)

#define __FLOAT2_T_TO_DWORD(from, to)                            \
   do {                                                          \
      to.low  = from.word0;                                      \
      to.high = from.word1;                                      \
   } while (0)

#define UINT32_TO_UWORD(from, to) to = from

#define INT32_TO_UWORD(from, to)  to = from

#define FLOAT32_TO_UWORD(from, to) to = *((uword *) &from)

#define DOUBLE64_TO_DWORD(from, to)                                        \
   do {                                                                    \
      to.high = (uword) ((*((uint64_ll *) (&from)) >> 32) & 0xffffffff);   \
      to.low  = (uword) (*((uint64_ll *) (&from)) & 0xffffffff);           \
   } while (0)

#define DWORD_TO_INT64_LL(from, to)                              \
   to = (int64_ll) from.high << 32 | from.low

#define QWORD_TO___X128_T(from, to)                              \
   do {                                                          \
      to.word0 = from.word0;                                     \
      to.word1 = from.word1;                                     \
      to.word2 = from.word2;                                     \
      to.word3 = from.word3;                                     \
   } while (0)

#define DWORD_TO___FLOAT2_T(from, to)                            \
   do {                                                          \
      to.word0 = from.low;                                       \
      to.word1 = from.high;                                      \
   } while (0)

#define UWORD_TO_UINT32(from, to) to = from

#define UWORD_TO_INT32(from, to)  to = from

#define UWORD_TO_FLOAT32(from, to)  to = *((float32 *) &from)

#define DWORD_TO_DOUBLE64(from, to)                              \
   do {                                                          \
      uint64_ll _tmp;                                            \
      _tmp = (uint64_ll) from.high << 32 | from.low;             \
      to = *((double64 *) (&_tmp));                              \
   } while (0)

#endif      // defined(TMS320C66X) || defined(TMS320C67X)

#endif      // #ifndef _TMS320C6X

#endif      // C66_DATA_SIM_H


/*
 *  Automated Revision Information
 *  Changed: $Date: 2011-02-15 11:12:01 -0600 (Tue, 15 Feb 2011) $
 *  Revision: $Revision: 9999 $
 */

