
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 -- Audio Stream Processing Declarations
//
//
//

#ifndef _ASP0_H_
#define _ASP0_H_

// Inclusions
#include "fwkPort.h"
//#include <ti/procsdk_audio/procsdk_audio_typ.h>
#include <procsdk_audio_typ.h>

#include <xdc/std.h>
#include <ti/sysbios/heaps/HeapMem.h>

#include <acp.h>

#include "alg.h"
#include "logp.h"
#include "iasp.h"

#ifndef _TMS320C6X
#define STATIC static
#else
#define STATIC
#endif

// ASP Chain/Link Declarations

#define PAF_ASP_ALPHACODE(SERIES,NAME) \
    (ACP_SERIES_##SERIES<<16)+(SERIES##_BETA_##NAME&0xffff)

typedef union PAF_ASP_AlphaCode {
    LgUns full;
    struct {
        MdUns beta;
        MdUns series;
    } part;
} PAF_ASP_AlphaCode;

typedef IASP_Handle ASP_Handle;

struct PAF_ASP_Link;

typedef struct PAF_ASP_Link {
    struct PAF_ASP_Link *next;
    PAF_ASP_AlphaCode code;
    ASP_Handle alg;
} PAF_ASP_Link;

struct PAF_ASP_ChainFxns;

typedef struct PAF_ASP_Chain {
    const struct PAF_ASP_ChainFxns *fxns;
    PAF_ASP_Link *head;
    MdUns stream;
    MdUns unused;
    ACP_Handle acp;
    LOG_Obj *log;
} PAF_ASP_Chain;

typedef struct PAF_ASP_LinkInit {
    IALG_Fxns *ialg_fxns;
    const IALG_Params *ialg_prms;
    PAF_ASP_AlphaCode linkCode;
    PAF_ASP_AlphaCode thisCode;
    Void (*init_func)();
} PAF_ASP_LinkInit;

#define PAF_ASP_CHAINFRAMEFXNS_RESET 0
#define PAF_ASP_CHAINFRAMEFXNS_APPLY 1
#define PAF_ASP_CHAINFRAMEFXNS_FINAL 2

struct PAF_ASP_ChainFxns {

    PAF_ASP_Chain *
    (*chainInit)(
        PAF_ASP_Chain *this,
        const struct PAF_ASP_ChainFxns *fxns,
        HeapMem_Handle heap, //int heap,
        Uns stream,
        ACP_Handle acp,
        LOG_Obj *log,
        const PAF_ASP_LinkInit *pInit,
        PAF_ASP_Chain *from,
        IALG_MemRec *common,
        PAF_IALG_Config *pafConfig);
    
    PAF_ASP_Chain *
    (*chainLink)(
        PAF_ASP_Chain *this,
        HeapMem_Handle heap, //int heap,
        const PAF_ASP_LinkInit *pInit,
        PAF_ASP_Chain *from,
        IALG_MemRec *common,
        PAF_IALG_Config *pafConfig);
    
    PAF_ASP_Link *
    (*chainFind)(
        PAF_ASP_Chain *this,
        PAF_ASP_AlphaCode);
    
    Int (*chainFrameFunction[3]) (PAF_ASP_Chain *, PAF_AudioFrame *);
    // Int (*chainReset) (PAF_ASP_Chain *, PAF_AudioFrame *);
    // Int (*chainApply) (PAF_ASP_Chain *, PAF_AudioFrame *);
    // Int (*chainFinal) (PAF_ASP_Chain *, PAF_AudioFrame *);

    PAF_ASP_Link *
    (*linkData)(
        PAF_ASP_Link *this,
        const PAF_ASP_LinkInit *pInit,
        Uns stream,
        ACP_Handle acp,
        LOG_Obj *log,
        IALG_MemRec *common,
        PAF_IALG_Config *pafConfig);
    
    PAF_ASP_Link *
    (*linkCopy)(
        PAF_ASP_Link *this,
        const PAF_ASP_LinkInit *pInit,
        PAF_ASP_Link *from);
    
};

extern const struct PAF_ASP_ChainFxns PAF_ASP_chainFxns;

STATIC inline
PAF_ASP_Chain *
PAF_ASP_chainInit (
    PAF_ASP_Chain *this,
    const struct PAF_ASP_ChainFxns *fxns,
    HeapMem_Handle heap, //int heap,
    Uns stream,
    ACP_Handle acp,
    LOG_Obj *log,
    const PAF_ASP_LinkInit *pInit,
    PAF_ASP_Chain *from,
    IALG_MemRec *common,
    PAF_IALG_Config *pafConfig)
{
    return fxns->chainInit (this, fxns, heap, stream, acp, log, pInit, from, common, pafConfig);
}

STATIC inline
PAF_ASP_Chain *
PAF_ASP_chainLink (
    PAF_ASP_Chain *this,
    HeapMem_Handle heap, //int heap,
    const PAF_ASP_LinkInit *pInit,
    PAF_ASP_Chain *from,
    IALG_MemRec *common,
    PAF_IALG_Config *pafConfig)
{
    return this->fxns->chainLink (this, heap, pInit, from, common, pafConfig);
}

STATIC inline
PAF_ASP_Link *
PAF_ASP_chainFind (PAF_ASP_Chain *this, PAF_ASP_AlphaCode code)
{
    return this->fxns->chainFind (this, code);
}

STATIC inline
Int
PAF_ASP_chainReset (PAF_ASP_Chain *this, PAF_AudioFrame *pAudioFrame)
{
    return this->fxns->chainFrameFunction[PAF_ASP_CHAINFRAMEFXNS_RESET] 
        (this, pAudioFrame);
}

STATIC inline
Int
PAF_ASP_chainApply (PAF_ASP_Chain *this, PAF_AudioFrame *pAudioFrame)
{
    return this->fxns->chainFrameFunction[PAF_ASP_CHAINFRAMEFXNS_APPLY] 
        (this, pAudioFrame);
}

STATIC inline
Int
PAF_ASP_chainFinal (PAF_ASP_Chain *this, PAF_AudioFrame *pAudioFrame)
{
    return this->fxns->chainFrameFunction[PAF_ASP_CHAINFRAMEFXNS_FINAL] 
        (this, pAudioFrame);
}

#endif  /* _ASP0_H_ */

