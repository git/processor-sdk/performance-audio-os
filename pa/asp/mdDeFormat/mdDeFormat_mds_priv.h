/*
* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  Internal vendor specific (TII) interface header for MDDEFORMAT
 *  algorithm. Only the implementation source files include
 *  this header; this header is not shipped as part of the
 *  algorithm.
 *
 *  This header contains declarations that are specific to this
 *  implementation and which do not need to be exposed in order for
 *  an application to use the MDDEFORMAT algorithm.
 */
#ifndef MDDEFORMAT_MDS_PRIV_
#define MDDEFORMAT_MDS_PRIV_

#include <ialg.h>
#include <log.h>

#include "imdDeFormat.h"
#include "mdDeFormat_mds.h"

 
#include "com_tii_priv.h" 

typedef struct MDDEFORMAT_MDS_Obj {
    IALG_Obj alg;           /* MUST be first field of all XDAS algs */
    int mask;
    MDDEFORMAT_MDS_Status *pStatus; /* public interface */
    MDDEFORMAT_MDS_Config *pConfig;   /* private interface */

    /* Persistent buffer for storage of transport samples */
    PAF_AudioData *sampleBufP[PAF_MAXNUMCHAN_HD];
    /* Current channel configuration */
    PAF_ChannelConfiguration channelConfigurationStream;
} MDDEFORMAT_MDS_Obj;

extern Int MDDEFORMAT_MDS_alloc(const IALG_Params *algParams, IALG_Fxns **pf,
                        IALG_MemRec memTab[]);
extern Int MDDEFORMAT_MDS_initObj(IALG_Handle handle,
                          const IALG_MemRec memTab[], IALG_Handle parent,
                          const IALG_Params *algParams);
extern Int MDDEFORMAT_MDS_apply(IMDDEFORMAT_Handle, PAF_AudioFrame *);
extern Int MDDEFORMAT_MDS_reset(IMDDEFORMAT_Handle, PAF_AudioFrame *);
extern Int MDDEFORMAT_MDS_numAlloc();

#define MDDEFORMAT_MDS_free COM_TII_free

#define MDDEFORMAT_MDS_control COM_TII_control

#define MDDEFORMAT_MDS_moved COM_TII_moved

#define MDDEFORMAT_MDS_activate COM_TII_activate

#define MDDEFORMAT_MDS_deactivate COM_TII_deactivate


/* Fixed- to floating-point conversion macros */
//#define INT2F_SCALE  (float) 128.0f/0x7FFFFFFF
#define INT2F_SCALE  1.0f/(float)0x7FFFFF
/* Performs 24-bit fixed-point to floating-point conversion.
Fixed-point value is left-justified in 32-bit word. */
#define INT2F(x)            ((float)(INT2F_SCALE * (x >> 8)))

#endif  /* MDDEFORMAT_MDS_PRIV_ */
