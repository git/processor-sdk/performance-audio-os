
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (TII) Audio Stream Split Algorithm IALG implementation
//
//
//

/*
 *  ASS Module IALG implementation - TII's implementation of the
 *  IALG interface for the Audio Stream Split Algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <std.h>
#include <ialg.h>

#include "iass.h"
#include "ass_tii.h"
#include "ass_tii_priv.h"

/*
 *  ======== ASS_TII_activate ========
 */
  /* COM_TIH_activate */

/*
 *  ======== ASS_TII_alloc ========
 */
Int ASS_TII_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IASS_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IASS_PARAMS3;  /* set default parameters */
    }

    /* Request memory for ASS object */
    memTab[0].size = (sizeof(ASS_TII_Obj)+3)/4*4 + (sizeof(ASS_TII_Status)+3)/4*4;
    memTab[0].alignment = 4;
    memTab[0].space = IALG_SARAM;
    memTab[0].attrs = IALG_PERSIST;

    return (1);
}

/*
 *  ======== ASS_TII_deactivate ========
 */
  /* COM_TIH_deactivate */

/*
 *  ======== ASS_TII_free ========
 */
  /* COM_TIH_free */

/*
 *  ======== ASS_TII_initObj ========
 */
Int ASS_TII_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    ASS_TII_Obj *ass = (Void *)handle;
    const IASS_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IASS_PARAMS3;  /* set default parameters */
    }

    ass->pStatus = (ASS_TII_Status *)((char *)ass + (sizeof(ASS_TII_Obj)+3)/4*4);

    *ass->pStatus = *params->pStatus;
    ass->config = params->config;

    return (IALG_EOK);
}

/*
 *  ======== ASS_TII_control ========
 */
  /* COM_TIH_control */

/*
 *  ======== ASS_TII_moved ========
 */
  /* COM_TIH_moved */

