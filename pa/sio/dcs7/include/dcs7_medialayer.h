/******************************************************************************
* Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// DCS low level layer
//


#ifndef __DCS7_MEDIALAYER_H__
#define __DCS7_MEDIALAYER_H__

// Allocate resources used by media layer
Uint32 DCS7_resAllocMediaLayer(DCS7_Handle handle);

// Low level read call to handle intermidiate protocol conversion and
// device LLD interface.
Uint32 DCS7_ReadIssue(DCS7_Handle handle, Uint16 *aBuf, Uint16 aSize);

// Low level write call to handle intermidiate protocol conversion and
// device LLD interface.
Uint32 DCS7_WriteIssue(DCS7_Handle handle, Uint16 *aBuf, Uint16 aSize);

/* Callback functions used by interface */
#ifdef UARTCALLBACK
void UART_transferCallback(UART_Handle handle,
                           void *buffer, size_t count);

// SWI function
void UartSwiFxn(UArg arg0, UArg arg1);

// Parameters used in UART SWI
typedef struct DCS7_swiObj
{
    Swi_Handle swi;                                  /* UART SWI handle */
    Clock_Handle clk;                                /* UART clock handle, used for timeout */
    Uint32 s1_count;                                 /* current S1 count */
    Uint32 readBufIndex;                             /* Read buffer head offset */
    Uint8 rxS1Size[SRECORD_REC_MAX_S1_CNT];          /* received S1 records */
    Uint32 swiBreakFlag;                             /* Flag to indicate final data received */
}DCS7_swiObj;

// UART callback Object
typedef struct DCS7_callbackObj
{
    void *globalSem;      /* Semaphore for UART task */
    Uint32 transferSize;  /* UART transfer size */
    Swi_Handle swi;       /* Swi handle */
    Uint32 opMode;        /* Indicate the type of UART operation was performed */
} DCS7_callbackObj;

#endif

#ifdef SPICALLBACK
void SPI_transferCallback(SPI_Handle handle,
                          SPI_Transaction *transaction);
#endif

#endif
