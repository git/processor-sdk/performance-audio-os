
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// I/O device configuration data structure declarations for E17 board.
//
//
//

#ifndef DAP_E17_H
#define DAP_E17_H

#include <std.h>
#include <xdas.h>

#include <dap.h>
#include <pafsio.h>

// mode is a bitfield
//      bit 0:  SYNC 0 = synchronous, 1 = asynchronous (Tx)
//      bit 1:  Reserved
//      bit 2-5 RATE 1 = 32, 2 = 48, 3 = 96, (Rx ADC only and Tx Async)
//      bit 6-7:  MCLK 0 = DIR, 1 = OSC, 2 = AUX
//      bit 8-9:  MODE 0 = STD  1 = 1394 (Rx and Tx Async) 2 = HDMI 3 = Lynx

#define E17_SYNC_MASK   0x01
#define E17_SYNC_SHIFT  0x00
#define E17_SYNC_SYNC   0
#define E17_SYNC_ASYNC  1

#define E17_RATE_MASK    0x3C
#define E17_RATE_SHIFT   0x02
#define E17_RATE_32KHZ   0x1
#define E17_RATE_48KHZ   0x2
#define E17_RATE_96KHZ   0x3
#define E17_RATE_192KHZ  0x4

#define E17_MCLK_MASK   0xC0
#define E17_MCLK_SHIFT  0x06
#define E17_MCLK_DIR    0
#define E17_MCLK_OSC    1
#define E17_MCLK_AUX    2
#define E17_MCLK_HDMI   3

#define E17_MODE_MASK   0x300
#define E17_MODE_SHIFT  0x08
#define E17_MODE_STD    0
#define E17_MODE_1394   1
#define E17_MODE_LYNX   2
#define E17_MODE_HDMI   3

//
// Device parameter data types, recieve
//

struct DAP_E17_Rx_Params_
{
    XDAS_UInt16 mode;
    XDAS_UInt8 unused[2];
};
typedef struct DAP_E17_Rx_Params
{
    Int size;                           // Type-specific size
    struct DXX_Params_ sio;             // Common parameters
    struct DAP_Params_ dap;             // Device parameters
    struct DAP_E17_Rx_Params_ e17rx;      // Board Receive parameters
} DAP_E17_Rx_Params;

extern const DAP_E17_Rx_Params DAP_E17_RX_DIR;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_SLAVE;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_STEREO_SLAVE;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_48000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_6CH_48000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_STEREO_48000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_96000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_6CH_96000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_STEREO_96000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_SACD;
extern const DAP_E17_Rx_Params DAP_E17_RX_DSDtest;
extern const DAP_E17_Rx_Params DAP_E17_RX_1394_STEREO;
extern const DAP_E17_Rx_Params DAP_E17_RX_1394;
extern const DAP_E17_Rx_Params DAP_E17_RX_LYNX;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_48000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_6CH_48000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_2CH_48000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_192000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_HDMI_STEREO;
extern const DAP_E17_Rx_Params DAP_E17_RX_HDMI;
//
// Device parameter data types, transmit
//

struct DAP_E17_Tx_Params_
{
    XDAS_UInt8 mode;
    XDAS_UInt8 unused[3];
};
typedef struct DAP_E17_Tx_Params
{
    Int size;                           // Type-specific size
    struct DXX_Params_ sio;             // Common parameters
    struct DAP_Params_ dap;             // Device parameters
    struct DAP_E17_Tx_Params_ e17tx;      // Board Transmit parameters
} DAP_E17_Tx_Params;

extern const DAP_E17_Tx_Params DAP_E17_TX_DAC;
extern const DAP_E17_Tx_Params DAP_E17_TX_STEREO_DAC;
extern const DAP_E17_Tx_Params DAP_E17_TX_DAC_SLAVE;
extern const DAP_E17_Tx_Params DAP_E17_TX_STEREO_DAC_SLAVE;
extern const DAP_E17_Tx_Params DAP_E17_TX_2STEREO_DAC_SLAVE;
extern const DAP_E17_Tx_Params DAP_E17_TX_DAC_ASYNC_32000HZ;
extern const DAP_E17_Tx_Params DAP_E17_TX_STEREO_DAC_ASYNC_32000HZ;
extern const DAP_E17_Tx_Params DAP_E17_TX_DAC_ASYNC_48000HZ;
extern const DAP_E17_Tx_Params DAP_E17_TX_STEREO_DAC_ASYNC_48000HZ;
extern const DAP_E17_Tx_Params DAP_E17_TX_DIT;
extern const DAP_E17_Tx_Params DAP_E17_TX_DAC_SLAVE;
extern const DAP_E17_Tx_Params DAP_E17_TX_STEREO_DAC_SLAVE;
extern const DAP_E17_Tx_Params DAP_E17_TX_2STEREO_DAC_SLAVE;
extern const DAP_E17_Tx_Params DAP_E17_TX_DAC_AUX_32000HZ;
extern const DAP_E17_Tx_Params DAP_E17_TX_DAC_AUX_48000HZ;
extern const DAP_E17_Tx_Params DAP_E17_TX_DAC_AUX_6CH_48000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_6CH_96000HZ;
extern const DAP_E17_Rx_Params DAP_E17_RX_ADC_AUX_6CH_192000HZ;
extern const DAP_E17_Tx_Params DAP_E17_TX_DAC_AUX_192000HZ;

#endif // DAP_E17_H

