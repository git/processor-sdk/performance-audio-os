/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/*
 *  ======== iirntapnch.c ========
 *  IIR filter implementation for tap-N & channels-N.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER N    and CHANNELS N ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2 + b3*z~3 + - - - -        */
/*                           ------------------------------------------      */
/*                            1  - a1*z~1 - a2*z~2 - a3*z~3 - - - - -        */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n)   + b1*x(n-1) + - - +                   */
/*                           a1*y(n-1) + a2*y(n-2) + - -                     */
/*                                                                           */
/*   Canonical form : w(n) = x(n)    + a1*w(n-1) + a2*w(n-2) + - - + - -     */
/*                    y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2) + - - + - -     */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      w(n) -             w(n-k) - wk                                       */
/*      b0 - filtCfsB[0], bk - filtCfsB[k]                                   */
/*                        ak - filtCfsA[k]                                   */
/*                                                                           */
/*                                                                           */
/*                              w(n)    b0                                   */
/*     x(n)-->---[+]---->--------@------>>-----[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+    b1     |                            */
/*               [+]----<<----| Z~1 |---->>----[+]                           */
/*                |           +-----+           |                            */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a2    +-----+    b2     |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                |           +-----+           |                            */
/*                                                                           */
/*                ^                             ^                            */
/*                ^                             ^                            */
/*                ^                             ^                            */
/*                                                                           */
/*                |     ak    +-----+    bk     |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include <filters.h>

/*
 *  ======== Filter_iirTNChN() ========
 *  IIR filter, taps-N(except 1 ) & channels-N 
 */
/* Memory section for the function code */
Int Filter_iirTNChN( PAF_FilParam *pParam ) 
{
    Int count; /* Sample counter */
    Int i, t, taps, ch;
    float accY, accYtemp, accW; /* Accumulator registers */
    float varW = 0, varW_temp; /* State reg */
    
    float * restrict x; /* I/p sample ptr */
    float * restrict y; /* O/p sample ptr */
    float * restrict filtVars; /* Vars ptrs */
    float * restrict filtCfsB, * restrict filtCfsA; /* Coeff ptrs */

    count = pParam->sampleCount; /* I/p sample block lenght */
    taps  = pParam->use; /* Filter taps is stored in 'use' element */

    /* Channel-wise filtering */
    for (ch = 0; ch < pParam->channels; ch++)
    {
        x = (float *)pParam->pIn[ch]; /* I/p sample ptr */
        y = (float *)pParam->pOut[ch]; /* O/p sample ptr */            

        filtVars  = (float *)pParam->pVar[ch]; /* Get var ptr for ch */
        
        filtCfsB  = (float *)pParam->pCoef[ch]; /* Get f.fwd ptr of ch */
        filtCfsA  = filtCfsB  + taps + 1; /* Derive f.back ptr of ch */
    
        /* Reset accumulators */
        accW = 0; 
        accY = 0;
    
        varW_temp = filtVars[0]; /* Get first var of ch */
        accW = *x; /* Start with input already in accum */
        
        /* Loop that runs for i/p sample count times taps */
        #pragma MUST_ITERATE(16, 2048, 4)
        for (i = 0, t = 0; i < (count * taps); i ++)
        {
            accW += filtCfsA[t] * varW_temp; /* accW = ak*w(n-k), k=1..t */
            
            accY += filtCfsB[t+1] * varW_temp; /* accY = bk*w(n-k), k=0..t */
            accYtemp = accY; /* Temp copy, for optimization */ 
              
            filtVars[t] = varW; /* Shifting memory, w(n-k) <- w(n-k+1) */                       
            varW = varW_temp; /* Copy of w(n-k), for next shift */ 

            t++; /* Increment tap counter */
            
            varW_temp = filtVars[t]; /* Get the next state */
            
            /* If tap counter = taps */
            if ( t == taps)
            {
                varW_temp = accW; /* For y(n+1), w'(n-1) is w(n) */ 
                
                accY = 0; /* Reset o/p accumulator */
                t = 0;    /* Reset tap counter */ 
                
                accYtemp += ( accW )*( filtCfsB[0] ); /* += wn*b0 */
                accW = *(++x); /* Reset state accumulator */
                *y++ = accYtemp; /* Store the calculated o/p */
            }
            
        } /* for (i = 0, t = 0; i < (count * taps); i ++) */
        
        filtVars[0] = varW_temp; /* Finally put put w(n) in vars[0] */

    } /* for (ch = 0; ch < pParam->channels; ch++) */

    return( FIL_SUCCESS );   
      
} /* Int Filter_iirTNChN() */
