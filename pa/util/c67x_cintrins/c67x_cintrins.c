
/*
Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

// C67x C intrinsics host implementation.

#ifndef _TMS320C6X

#include <math.h>
#include "c67x_cintrins.h"

float powf (float a, float b)
{
	return (float) pow (a, b);
}

unsigned __int64 _itoll (UWord32 a, UWord32 b)
{
	unsigned __int64 nTmp;

	nTmp = a;
	nTmp = (nTmp << 32);
	nTmp |= b;

	return (nTmp);
}

float _fabsf (float a)
{

	return (float) fabs (a);
}

float ldexpf (float a, int b)
{
	return (float) ldexp (a, b);
}

#define _lmbd  lmbd_c

Word32 dpint_c (double src1)
{
	Word32 src2;
	int neg = 0;

	if (src1 < 0)
		neg = 1, src1 = -src1;
	if ((src1 - (int) src1) > (float) 0.5)
		src2 = (int) src1 + 1;
	else
		src2 = (int) src1;
	if (neg)
		src2 = -src2;

	return (src2);
}

Word32 SAT_Bit;

Word32 abs_c (Word32 src1)
{
	if (src1 == MIN_32)
		return ((Word40) MAX_32);
	else
		return ((src1 < 0) ? -src1 : src1);
}

Word32 add2_c (Word32 src1, Word32 src2)
{
	return ((((src1 & 0x0ffff) + (src2 & 0x0ffff)) & 0x0ffff) | ((((src1 & 0x0ffff0000) >> 16) + ((src2 & 0x0ffff0000) >> 16)) << 16));
}

UWord32 clr_c (UWord32 src1, UWord32 csta, UWord32 cstb)
{
	UWord32 i;
	Word32 maska = 0x0;
	Word32 maskb = 0x0ffffffff;

	if (csta <= cstb)
	{
		for (i = 0; i < csta; i++)
			maska = (maska << 1) + 1;
		for (i = 0; i <= cstb; i++)
			maskb = maskb << 1;

		maska = maska | maskb;

		return (maska & src1);
	}
	else
	{
		return (src1);
	}
}

Word32 ext_c (Word32 src1, UWord32 csta, UWord32 cstb)
{
	return ((src1 << csta) >> cstb);
}

UWord32 extu_c (UWord32 src1, UWord32 csta, UWord32 cstb)
{
	return ((src1 << csta) >> cstb);
}

UWord32 lmbd_c (UWord32 src1, UWord32 src2)
{
	Word32 i = 0;
	UWord32 mask = 0x80000000;

	if (src1 & 0x01)
	{														/* Search for a 1 */
		while ((src2 & mask) == 0)
		{
			mask = mask >> 1;
			i++;

			if (i == 32)
				return (32);
		}
	}
	else
	{														/* Search for a 0 */
		while ((src2 & mask) != 0)
		{
			mask = mask >> 1;
			i++;

			if (i == 32)
				return (32);
		}
	}

	return (i);

}

#define MASK39 (Word40)0x8000000000

UWord32 lnorm_c (Word40 src2)
{
	Word32 i = 0;

	if ((src2 >> 39) == 0)
	{														/* Positive Number */
		while ((src2 >> 39) != 1)
		{
			src2 = src2 << 1;
			i++;
			if (i == 40)
				return (39);
		}
	}
	else
	{														/* Negative Number */
		while ((src2 >> 39) != 0)
		{
			src2 = src2 << 1;
			i++;
			if (i == 40)
				return (39);
		}
	}
	return (i - 1);
}

Word40 lsadd_c (int src1, Word40 src2)
{
	Word40 result;

	result = src1 + src2;

	if (((src1 >> 31) ^ (src2 >> 39)) == 0)
	{
		if (((result ^ src2) >> 39) != 0)
		{
			if (src2 < 0)
				result = ((Word40) MIN_32 << 8);
			else
				result = (((Word40) MAX_32 << 8)) + 0xff;
			SAT_Bit = 1;
		}
	}
	return (result);

}

Word40 lssub_c (Word16 src1, Word40 src2)
{
	Word40 result;

	result = (Word40) src1 - src2;

	if ((((Word40) src1 >> 31) ^ (src2 >> 39)) != 0)
	{
		if (((result >> 39) ^ ((Word40) src1 >> 31)) != 0)
		{
			if (src2 < 0)
				result = ((Word40) MIN_32 << 8);
			else
				result = (((Word40) MAX_32 << 8)) + 0xff;
		}
	}

	return ((Word40) result);

}

Word32 mpy_c (Word32 src1, Word32 src2)
{
	return ((Word16) src1 * (Word16) src2);
}

Word32 mpysu_c (Word32 src1, UWord32 src2)
{
	return ((Word16) src1 * (UWord16) src2);
}

Word32 mpyus_c (UWord32 src1, Word32 src2)
{
	return ((UWord16) src1 * (Word16) src2);
}

Word32 mpyu_c (UWord32 src1, UWord32 src2)
{
	return ((UWord16) src1 * (UWord16) src2);
}

Word32 mpyh_c (Word32 src1, Word32 src2)
{
	return ((Word16) ((src1 & 0x0ffff0000) >> 16) * (Word16) ((src2 & 0x0ffff0000) >> 16));
}

Word32 mpyhus_c (UWord32 src1, Word32 src2)
{
	return ((UWord16) ((src1 & 0x0ffff0000) >> 16) * (Word16) ((src2 & 0x0ffff0000) >> 16));
}

Word32 mpyhsu_c (Word32 src1, UWord32 src2)
{
	return ((Word16) ((src1 & 0x0ffff0000) >> 16) * (UWord16) ((src2 & 0x0ffff0000) >> 16));
}

Word32 mpyhu_c (UWord32 src1, UWord32 src2)
{
	return ((UWord16) ((src1 & 0x0ffff0000) >> 16) * (UWord16) ((src2 & 0x0ffff0000) >> 16));
}

Word32 mpyhl_c (Word32 src1, Word32 src2)
{
	return ((Word16) ((src1 & 0x0ffff0000) >> 16) * (Word16) src2);
}

Word32 mpyhuls_c (UWord32 src1, Word32 src2)
{
	return ((UWord16) ((src1 & 0x0ffff0000) >> 16) * (Word16) src2);
}

Word32 mpyhslu_c (Word32 src1, UWord32 src2)
{
	return ((Word16) ((src1 & 0x0ffff0000) >> 16) * (UWord16) src2);
}

Word32 mpyhlu_c (UWord32 src1, UWord32 src2)
{
	return ((UWord16) ((src1 & 0x0ffff0000) >> 16) * (UWord16) src2);
}

Word32 mpylh_c (Word32 src1, Word32 src2)
{
	return ((Word16) src1 * (Word16) ((src2 & 0x0ffff0000) >> 16));
}

Word32 mpyluhs_c (UWord32 src1, Word32 src2)
{
	return ((UWord16) src1 * (Word16) ((src2 & 0x0ffff0000) >> 16));
}

Word32 mpylshu_c (Word32 src1, UWord32 src2)
{
	return ((Word16) src1 * (UWord16) ((src2 & 0x0ffff0000) >> 16));
}

Word32 mpylhu_c (UWord32 src1, UWord32 src2)
{
	return ((UWord16) src1 * (UWord16) ((src2 & 0x0ffff0000) >> 16));
}

#define MASK31 (int)0x80000000

UWord32 norm_c (Word32 src2)
{
	Word32 i = 0;

	if (!(src2 & MASK31))
	{														/* Positive Number */
		while ((src2 & MASK31) != MASK31)
		{
			src2 = src2 << 1;
			i++;
			if (i == 32)
				return (31);
		}
	}
	else
	{														/* Negative Number */
		while ((src2 & MASK31) != 0)
		{
			src2 = src2 << 1;
			i++;
			if (i == 32)
				return (31);
		}
	}

	return (i - 1);

}

Word32 sadd_c (Word32 src1, Word32 src2)
{
	Word32 result;

	result = src1 + src2;

	if (((src1 ^ src2) & MIN_32) == 0)
	{
		if ((result ^ src1) & MIN_32)
		{
			result = (src1 < 0) ? MIN_32 : MAX_32;
			SAT_Bit = 1;
		}
	}

	return (result);
}

Word32 sat_c (Word40 src2)
{
	if (src2 > MAX_32)
	{
		return (MAX_32);
		SAT_Bit = 1;
	}
	else
	{
		if (src2 < MIN_32)
		{
			SAT_Bit = 1;
			return (MIN_32);
		}
		else
		{
			return (Word32) src2;
		}
	}
}

UWord32 set_c (UWord32 src1, UWord32 csta, UWord32 cstb)
{
	UWord32 i;
	Word32 maska = 0x0ffffffff;
	Word32 maskb = 0x01;

	if (csta <= cstb)
	{
		for (i = 0; i < csta; i++)
			maska = maska << 1;
		for (i = 0; i < cstb; i++)
			maskb = (maskb << 1) + 1;
		maska = maska & maskb;
		return (maska | src1);
	}
	else
	{
		return (src1);
	}
}

Word32 smpy_c (Word32 src1, Word32 src2)
{
	Word32 result;

	result = ((Word16) src1 * (Word16) src2) << 1;

	if (result != MIN_32)
		return (result);
	else
	{
		return (MAX_32);
		SAT_Bit = 1;
	}
}

Word32 smpyh_c (Word32 src1, Word32 src2)
{
	Word32 result;

	result = ((Word16) ((src1 & 0x0ffff0000) >> 16) * (Word16) ((src2 & 0x0ffff0000) >> 16)) << 1;

	if (result != MIN_32)
		return (result);
	else
	{
		SAT_Bit = 1;
		return (MAX_32);
	}
}

Word32 smpyhl_c (Word32 src1, Word32 src2)
{
	Word32 result;

	result = ((Word16) ((src1 & 0x0ffff0000) >> 16) * (Word16) src2) << 1;

	if (result != MIN_32)
		return (result);
	else
	{
		return (MAX_32);
		SAT_Bit = 1;
	}
}

Word32 smpylh_c (Word32 src1, Word32 src2)
{
	Word32 result;

	result = ((Word16) src1 * (Word16) ((src2 & 0x0ffff0000) >> 16)) << 1;

	if (result != MIN_32)
		return (result);
	else
	{
		return (MAX_32);
		SAT_Bit = 1;
	}
}

Word32 sshl_c (Word32 src2, UWord32 src1)
{
	UWord32 i;

	src1 = src1 & 0x01f;									/* Consider only the five least significant bits */

	if (norm_c (src2) >= src1)
	{
		for (i = 0; i < src1; i++)
			src2 = src2 << 1;
		return (src2);
	}
	else
	{
		SAT_Bit = 1;
		return ((src2 > 0) ? MAX_32 : MIN_32);
	}
}

Word32 ssub_c (Word32 src1, Word32 src2)
{
	Word32 result;

	result = src1 - src2;

	if (((src1 ^ src2) & MIN_32) != 0)
	{
		if ((result ^ src1) & MIN_32)
		{
			result = (src1 < 0) ? MIN_32 : MAX_32;
			SAT_Bit = 1;
		}
	}

	return (result);
}

Word32 sub2_c (Word32 src1, Word32 src2)
{
	return ((((src1 & 0x0ffff) - (src2 & 0x0ffff)) & 0x0ffff) | ((((src1 & 0xffff0000) >> 16) - ((src2 & 0xffff0000) >> 16)) << 16));
}

UWord32 subc_c (UWord32 src1, UWord32 src2)
{
	if (src1 >= src2)
		return (((src1 - src2) << 1) + 1);
	else
		return (src1 << 1);
}

UWord32 lo_c (fl64 src1)
{
	UWord32 *src2;

	src2 = (UWord32 *) (&src1);

	return (*src2);
}

UWord32 hi_c (fl64 src1)
{
	UWord32 *src2;

	src2 = (UWord32 *) (&src1);

	return (*(src2 + 1));
}

fl64 ltod_c (Word40 src1)
{
	return *(fl64 *) & src1;
}

fl32 itof_c (UWord32 src1)
{
	fl32 *src2;

	src2 = (fl32 *) (&src1);

	return (*src2);
}

UWord32 ftoi_c (fl32 src1)
{
	UWord32 *src2;

	src2 = (UWord32 *) (&src1);

	return (*src2);
}

Word32 spint_c (fl32 src1)
{
	Word32 src2;
	int neg = 0;

	if (src1 < 0)
		neg = 1, src1 = -src1;
	if ((src1 - (int) src1) > (float) 0.5)
		src2 = (int) src1 + 1;
	else
		src2 = (int) src1;
	if (neg)
		src2 = -src2;

	return (src2);
}

fl32 fabs_c (fl32 src1)
{
	Word32 src2;

	src2 = ftoi_c (src1);
	src2 = src2 & 0x7fffffff;
	return itof_c (src2);
}

typedef struct _INT32X2
{
Word16 lo;
Word16 hi;
} int32x2;
typedef struct _INT32X4
{
Word8 lo1;
Word8 lo2;
Word8 hi1;
Word8 hi2;
} int32x4;
typedef struct _INT32X2U
{
UWord16 lo;
UWord16 hi;
} int32x2u;
typedef struct _INT32X4U
{
UWord8 lo1;
UWord8 lo2;
UWord8 hi1;
UWord8 hi2;
} int32x4u;

union reg32 
{
Word32 x1;
int32x2 x2;
int32x4 x4;

UWord32 x1u;
int32x2u x2u;
int32x4u x4u;

fl32 xf;
};
UWord32 _swap4(UWord32 a)
{
union reg32 a32,y32;

a32.x1u = a;

y32.x4u.hi2 = a32.x4u.hi1;
y32.x4u.hi1 = a32.x4u.hi2;
y32.x4u.lo2 = a32.x4u.lo1;
y32.x4u.lo1 = a32.x4u.lo2;

return(y32.x1u);
}

#endif
