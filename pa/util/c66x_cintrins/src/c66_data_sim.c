/****************************************************************************/
/*  c66_data_sim.c                                                          */
/****************************************************************************/

/* 
 *
 * Copyright (C) 1997-2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


/****************************************************************************/
/* the includes                                                             */
#include <math.h>
#include <float.h>
#include <assert.h>
#include "C6xSimulator.h"

#define TYPE_PUN(type, var) (*((type *) &var))
#define LL_TO_LO32(type, ll) ((type) ((ll) & 0xffffffff))
#define LL_TO_HI32(type, ll) ((type) (((ll) >> 32) & 0xffffffff))
#define DBL_TO_LO32(type, d) (LL_TO_LO32(type, TYPE_PUN(uint64_ll, d)))
#define DBL_TO_HI32(type, d) (LL_TO_HI32(type, TYPE_PUN(uint64_ll, d)))

#ifdef TMS320C66X
__x128_t _ito128(uint32 arg3, uint32 arg2, uint32 arg1, uint32 arg0)
{
   __x128_t temp;
   temp.word0 = arg0;
   temp.word1 = arg1;
   temp.word2 = arg2;
   temp.word3 = arg3;
   return temp;
}

__x128_t _fto128(float32 arg3, float32 arg2, float32 arg1, float32 arg0)
{
   __x128_t temp;
   temp.word0 = TYPE_PUN(uint32, arg0);
   temp.word1 = TYPE_PUN(uint32, arg1);
   temp.word2 = TYPE_PUN(uint32, arg2);
   temp.word3 = TYPE_PUN(uint32, arg3);
   return temp;
}
                                  
__x128_t _llto128(int64_ll arg1, int64_ll arg0)
{
   __x128_t temp;
   temp.word0 = LL_TO_LO32(uint32, arg0);
   temp.word1 = LL_TO_HI32(uint32, arg0);
   temp.word2 = LL_TO_LO32(uint32, arg1);
   temp.word3 = LL_TO_HI32(uint32, arg1);
   return temp;
}

__x128_t _dto128(double64 arg1, double64 arg0)
{
   __x128_t temp;
   temp.word0 = DBL_TO_LO32(uint32, arg0);
   temp.word1 = DBL_TO_HI32(uint32, arg0);
   temp.word2 = DBL_TO_LO32(uint32, arg1);
   temp.word3 = DBL_TO_HI32(uint32, arg1);
   return temp;
}

__x128_t _f2to128(__float2_t arg1, __float2_t arg0)
{
   __x128_t temp;
   temp.word0 = arg0.word0;
   temp.word1 = arg0.word1;
   temp.word2 = arg1.word0;
   temp.word3 = arg1.word1;
   return temp;
}

__x128_t _dup32_128(uint32 arg)
{
   __x128_t temp;
   temp.word0 = arg;
   temp.word1 = arg;
   temp.word2 = arg;
   temp.word3 = arg;
   return temp;
}

double64 _lltod(int64_ll arg)
{
   return TYPE_PUN(double64, arg);
}

__float2_t _lltof2(int64_ll arg)
{
   __float2_t temp;
   temp.word0 = (uint32) (arg & 0xffffffff);
   temp.word1 = (uint32) ((arg >> 32) & 0xffffffff);
   return temp;
}

int64_ll _dtoll(double64 arg)
{
   return TYPE_PUN(int64_ll, arg);
}

int64_ll _f2toll(__float2_t arg)
{
   return (int64_ll) arg.word1 << 32 | arg.word0;
}

float32 _hif(double64 arg)
{
   uint32 temp = DBL_TO_HI32(uint32, arg);
   return TYPE_PUN(float32, temp);
}

float32 _lof(double64 arg)
{
   uint32 temp = DBL_TO_LO32(uint32, arg);
   return TYPE_PUN(float32, temp);
}

float32 _hif2(__float2_t arg)
{
   uint32 temp = arg.word1;
   return TYPE_PUN(float32, temp);
}

float32 _lof2(__float2_t arg)
{
   uint32 temp = arg.word0;
   return TYPE_PUN(float32, temp);
}

int64_ll _hi128(__x128_t arg)
{
   return (int64_ll) arg.word3 << 32 | arg.word2;
}

double64 _hid128(__x128_t arg)
{
   int64_ll temp = (int64_ll) arg.word3 << 32 | arg.word2;
   return TYPE_PUN(double64, temp);
}

__float2_t _hif2_128(__x128_t arg)
{
   __float2_t temp;
   temp.word1 = arg.word3;
   temp.word0 = arg.word2;
   return temp;
}

int64_ll _lo128(__x128_t arg)
{
   return (int64_ll) arg.word1 << 32 | arg.word0;
}

double64 _lod128(__x128_t arg)
{
   int64_ll temp = (int64_ll) arg.word1 << 32 | arg.word0;
   return TYPE_PUN(double64, temp);
}

__float2_t _lof2_128(__x128_t arg)
{
   __float2_t temp;
   temp.word1 = arg.word1;
   temp.word0 = arg.word0;
   return temp;
}

//----------------------------------------------------------------------------
// For the get32 functions, the second argument is limited by the TI   
// compiler to a constant literal (i.e. variables and expressions not 
// accepted) in the range 0-3.  Those limitations cannot be modeled
// here.  The assert check helps a bit.
//----------------------------------------------------------------------------
uint32 _get32_128(__x128_t arg, unsigned index)
{
   assert(0 <= index && index <= 3);
   
   switch (index)
   {
      case 0 : return arg.word0;
      case 1 : return arg.word1;
      case 2 : return arg.word2;
      case 3 : return arg.word3;
   }

   return 0;      // Not reached.  But silences compiler warnings.
}

float32 _get32f_128(__x128_t arg, unsigned index)
{
   assert(0 <= index && index <= 3);
   
   switch (index)
   {
      case 0 : return TYPE_PUN(float32, arg.word0);
      case 1 : return TYPE_PUN(float32, arg.word1);
      case 2 : return TYPE_PUN(float32, arg.word2);
      case 3 : return TYPE_PUN(float32, arg.word3);
   }

   return 0.0;    // Not reached.  But silences compiler warnings.
}

#endif // #ifdef TMS320C66X


/*
 *  Automated Revision Information
 *  Changed: $Date: 2011-02-15 11:12:01 -0600 (Tue, 15 Feb 2011) $
 *  Revision: $Revision: 9999 $
 */
