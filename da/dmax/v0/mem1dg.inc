/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#ifndef MEM1DG_
#define MEM1DG_

// The MEM1DG service routine facilitates 1D:1D trasnfer. The
// following Parameter Table is used to specify a 1D:1D transfer.
//
// 31        24        16        8     6      4     3      2         1          0  
//  ____________________________________________________________________________
// | PEND_PRI| TCC     | TCINT   | RES | ESIZE| RES |SYNC  | LINKFLG | TCINTFLG |
// |_________|_________|_________|_____|______|_____|______|_________|__________|
// |                    SRC                                                     |
// |____________________________________________________________________________|
// |                    DST                                                     |
// |____________________________________________________________________________|
// | RES     | BURSTLEN|           CNT                                          |
// |_________|_________|________________________________________________________|
// | DIDX              | SIDX                                                   |
// |___________________|________________________________________________________| 
// |                    LINK                                                    |
// |____________________________________________________________________________|
//
#define MEM1DG_TCINT_DIS 0       // TCINT Disable 
#define MEM1DG_TCINT_ENA 1       // TCINT Enable
#define MEM1DG_TCINT_MSK 1       // TCINT Mask 
#define MEM1DG_TCINT_SHFT 0      // TCINT Shift 

#define MEM1DG_LINK_DIS  0       // LINK Disable
#define MEM1DG_LINK_ENA  1       // LINK Enable
#define MEM1DG_LINK_MSK  1       // LINK Mask 
#define MEM1DG_LINK_SHFT 1       // LINK Shift 

#define MEM1DG_SYNC_DIS  0       // SYNC Disable
#define MEM1DG_SYNC_ENA  1       // SYNC Enable
#define MEM1DG_SYNC_MSK  1       // SYNC Mask 
#define MEM1DG_SYNC_SHFT 2       // SYNC Shift 

#define MEM1DG_ESIZE8    0       // ESIZE 8-bit
#define MEM1DG_ESIZE16   1       // ESIZE 16-bit
#define MEM1DG_ESIZE32   2       // ESIZE 32-bit
#define MEM1DG_ESIZE_MSK 3       // ESIZE Mask 
#define MEM1DG_ESIZE_SHFT 4      // ESIZE Shift 

// MEM1DG parameter field lengths and offsets
#define MEM1DG_CTRL_OFF  0       // MEM1DG ctrl offset
#define MEM1DG_CTRL_LEN  1       // MEM1DG ctrl length       
#define MEM1DG_TCINT_OFF 1       // MEM1DG tcint offset
#define MEM1DG_TCINT_LEN 1       // MEM1DG tcint length       
#define MEM1DG_TCC_OFF   2       // MEM1DG tcc offset
#define MEM1DG_TCC_LEN   1       // MEM1DG tcc length       
#define MEM1DG_PEND_OFF  3       // MEM1DG pend offset
#define MEM1DG_PEND_LEN  1       // MEM1DG pend length       
#define MEM1DG_SRC_OFF   4       // MEM1DG src offset
#define MEM1DG_SRC_LEN   4       // MEM1DG src length       
#define MEM1DG_DST_OFF   8       // MEM1DG dst offset
#define MEM1DG_DST_LEN   4       // MEM1DG dst length       
#define MEM1DG_CNT_OFF   12      // MEM1DG cnt offset
#define MEM1DG_CNT_LEN   2       // MEM1DG cnt length       
#define MEM1DG_BLEN_OFF  14      // MEM1DG blen offset
#define MEM1DG_BLEN_LEN  1       // MEM1DG blen length       
#define MEM1DG_RES_OFF   15      // MEM1DG res offset
#define MEM1DG_RES_LEN   1       // MEM1DG res length       
#define MEM1DG_SIDX_OFF  16      // MEM1DG sidx offset
#define MEM1DG_SIDX_LEN  2       // MEM1DG sidx length       
#define MEM1DG_DIDX_OFF  18      // MEM1DG didx offset
#define MEM1DG_DIDX_LEN  2       // MEM1DG didx length       
#define MEM1DG_LINK_OFF  20      // MEM1DG link offset
#define MEM1DG_LINK_LEN  4       // MEM1DG link length       

#endif // MEM1DG_
