
/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Standard PCM Encoder algoritm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the PCM Encoder algoritm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the PCE
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef PCE_
#define PCE_

#include <alg.h>
#include <ipce.h>
#include <ialg.h>
#include <paf_alg.h>

/*
 *  ======== PCE_Handle ========
 *  PCM Encoder algoritm instance handle
 */
typedef struct IPCE_Obj *PCE_Handle;

/*
 *  ======== PCE_Params ========
 *  PCM Encoder algoritm instance creation parameters
 */
typedef struct IPCE_Params PCE_Params;

/*
 *  ======== PCE_PARAMS ========
 *  Default instance parameters
 */
#define PCE_PARAMS IPCE_PARAMS

/*
 *  ======== PCE_Status ========
 *  Status structure for getting PCE instance attributes
 */
typedef volatile struct IPCE_Status PCE_Status;

/*
 *  ======== PCE_Cmd ========
 *  This typedef defines the control commands PCE objects
 */
typedef IPCE_Cmd   PCE_Cmd;

/*
 * ===== control method commands =====
 */
#define PCE_NULL		IPCE_NULL
#define PCE_GETSTATUSADDRESS1	IPCE_GETSTATUSADDRESS1
#define PCE_GETSTATUSADDRESS2	IPCE_GETSTATUSADDRESS2
#define PCE_GETSTATUS		IPCE_GETSTATUS
#define PCE_SETSTATUS		IPCE_SETSTATUS
#define PCE_MININFO		IPCE_MININFO
#define PCE_MINSAMGEN		IPCE_MINSAMGEN
#define PCE_MAXSAMGEN		IPCE_MAXSAMGEN

/*
 *  ======== PCE_create ========
 *  Create an instance of a PCE object.
 */
static inline PCE_Handle PCE_create(const IPCE_Fxns *fxns, const PCE_Params *prms)
{
    return ((PCE_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== PCE_delete ========
 *  Delete a PCE instance object
 */
static inline Void PCE_delete(PCE_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== PCE_encode ========
 */
extern Int PCE_encode(PCE_Handle, ALG_Handle, PAF_EncodeInStruct *, PAF_EncodeOutStruct *);

/*
 *  ======== PCE_exit ========
 *  Module finalization
 */
extern Void PCE_exit(Void);

/*
 *  ======== PCE_info ========
 */
extern Int PCE_info(PCE_Handle, ALG_Handle, PAF_EncodeControl *, PAF_EncodeStatus *);

/*
 *  ======== PCE_init ========
 *  Module initialization
 */
extern Void PCE_init(Void);

/*
 *  ======== PCE_reset ========
 */
extern Int PCE_reset(PCE_Handle, ALG_Handle, PAF_EncodeControl *, PAF_EncodeStatus *);

#endif  /* PCE_ */
