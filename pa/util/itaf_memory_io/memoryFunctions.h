#include <stdio.h>
#include <string.h>

/* Initialization required to use memory read modules */
void mReadInit(FILE* fp);

/* Initialization required to use memory write modules */
void mWriteInit(void);

/* Equivalent to  C library function fread */
/* Instead of Reading from a file read from predefined memeory address */
size_t mRead(void *ptr, size_t size, size_t count, FILE* fp);

/* Equivalent to  C library function fwrite */
/* Instead of writing to file write predefined memeory address */
size_t mWrite(void *ptr, size_t size, size_t count,FILE* fp);

/* Equivalent to  C library function fsetpos */
/* Instead of aligning FIlE Pointer function align memory index */
int mWriteSetPos(FILE *fp, const fpos_t *pos);

/* Equivalent to  C library function fgetpos */
/* Instead of reading FIlE Pointer position reads memory address index*/
int mWriteGetPos(FILE *fp, fpos_t *pos);

/* Equivalent to  C library function fgetpos */
/* Align memory address index */
size_t mWriteSeek(FILE* fp,  long offset, int whence);

/* Equivalent to  C library function fseek */
/* Align memory address index */
size_t mReadSeek(FILE* fp, long offset, int whence);

/* Equivalent to  C library function fgetpos */
/* Instead of reading FIlE Pointer position reads memory address index*/
int mReadGetPos(FILE *fp, fpos_t *pos);

/* Equivalent to  C library function fsetpos */
/* Instead of aligning FIlE Pointer function align memory index */
int mReadSetPos(FILE *fp, const fpos_t *pos);
