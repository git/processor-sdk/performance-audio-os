/******************************************************************************
* Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// DA10x SPI, I2C and UART slave control status (DCS7) implementation
//
//

#include <string.h>
#include <ti/sysbios/knl/Swi.h>

#include "dcs7.h"
#include "dcs7_priv.h"
#include "dcs7_medialayer.h"

// SPI includes
#include <ti/drv/spi/SPI.h>
#include <ti/drv/spi/src/v0/SPI_v0.h>
/* I2C LLD Header files */
#include <ti/drv/i2c/I2C.h>

#include <ti/drv/i2c/soc/I2C_v0.h>    

/* UART LLD Header files */
#include <ti/drv/uart/UART.h>

DCS7_Handle DCS7_HANDLE = NULL;


/**
 *  @brief     Reconfigure interface in case of error
 *
 *  @param[in] handle    DCS handle
 *
 */
void DCS7_errorRecover(DCS7_Handle handle)
{
    handle->fxns->errorAssert();
    TRACE_TERSE0("DCS7: Error Recover");
    handle->fxns->reset(handle);
    handle->fxns->configure(handle);
    handle->ecnt++;
    handle->fxns->errorDeassert();
}

/**
 *  @brief     Handle SLC commands
 *
 *  @param[in] handle    DCS handle
 *
 */
void DCS7_slc(DCS7_Handle handle)
{
    DCS7_CommonParam *p =  &handle->common;
    Uint16 slc, sls;

    // get he SLC message
    slc = *(Uint16*) (p->rbuf_start + p->rbuf_poff);

    if ((slc == SLC_RESET_QUERY) || (slc == SLC_READY_QUERY))
    {
        if (slc == SLC_RESET_QUERY)  // reset command
        {
            handle->fxns->errorRecover(handle);
        }

        sls = SLS_OK;  // indicates successful completion of the SLC
    }
    else
    {
        sls = SLS_SLC_NOT_DEFINED;  // indicates that the SLC is not defined
    }

    TRACE_TERSE1("DCS7:slcRx: 0x%x", slc);
    TRACE_TERSE1("DCS7:slsTx: 0x%x", sls);
    *(Uint16*) p->xbuf_start = sls;  // Set SLS message

    // send response of SLC
    handle->fxns->writeIssue(handle, (void*)(p->xbuf_start), SL_SIZE);

}

/**
 *  @brief     DCS read
 *
 *  @param[in] handle      DCS handle
 *
 *  @param[in] aBuf       data to send
 *
 *  @param[in] aSize       number of bytes in aBuf
 *
 *  @retval    Success or fail
 *
 */
Uint32 DCS7_read(DCS7_Handle handle,
                 Uint16 *aBuf,
                 Uint16 aSize)
{
    DCS7_CommonParam *p = &handle->common;
    Uint32 len = 0;
    Uint16 rbuf_poff, rbuf_len;
    Uint32 word0;
    Uint32 i;

    // reset the offset for new read
    p->rbuf_poff = 0;
    p->rbuf_off  = 0;

    while (!len)
    {
        // Read alpda command
        len = handle->fxns->readIssue(handle,
                                      (void *) (p->rbuf_start + p->rbuf_poff),
                                      aSize);
        // Check for error
        if ((len == 0) && handle->params->edr)
        {
            handle->fxns->errorRecover(handle);  // error recovery
            // Clear received buffer
            memset((void *) (handle->mediaLayer.rxBuf), 0, aSize);
            if (handle->config->cacheMode == DCS7_CONFIG_CACHEMODE_ENABLE)
            {
                handle->fxns->cacheWbInv(handle,
                                         (void*) (handle->mediaLayer.rxBuf),
                                         aSize);
            }
        }
        else  // No error
        {
            rbuf_poff = p->rbuf_poff;
            rbuf_len = p->rbuf_len;

            if (rbuf_poff == rbuf_len)
            {
                rbuf_poff = 0;
            }

            word0 = *(Uint16*) (p->rbuf_start + rbuf_poff);
            if ((word0 & SL_MASK) == SLC_RESET_QUERY)  // slc command
            {
                handle->fxns->slc(handle);
                len = 0;
                continue;
            }
            else                                      // alpha sequence
            {
                len = word0;
            }
        }
    }
    // Check if the input size is large
    if ((len << 1) > aSize)
    {
        TRACE_TERSE0("DCS7: Application buffer size too small");
        p->rbuf_poff = 0;
        return RETURN_ERROR1;
    }
    else
    {
        // copy data from Rx buffer
        for (i = 0; i <= len; ++i)
        {
            aBuf[i] = *(Uint16*) (p->rbuf_start + rbuf_poff);
            TRACE_TERSE1("DCS7: Rx: 0x%x", aBuf[i]);
            rbuf_poff += 2;
        }
        p->rbuf_poff = rbuf_poff;
        return RETURN_SUCCESS;
    }
}

/**
 *  @brief     DCS write
 *
 *  @param[in] handle      DCS handle
 *
 *  @param[out] aBuf       data to send
 *
 *  @param[in] aSize       number of bytes in aBuf
 *
 *  @retval    Success or fail
 *
 */
Uint32 DCS7_write(DCS7_Handle handle,
                  Uint16 *aBuf,
                  Uint16 aSize)
{
    DCS7_CommonParam *p = &handle->common;
    Uint16 len = aBuf[0]; // total length
    Uint16 xbuf_off, xbuf_len;
    Uint32 i;

    if (len)
    {
        xbuf_off = p->xbuf_off;
        xbuf_len = p->xbuf_len;
        // Check if length is large
        if ((len << 1) > xbuf_len)
        {
            TRACE_TERSE0("DCS7: Response too big");
            return RETURN_ERROR1;
        }
        else
        {
            xbuf_off = 0;
            // Copy data into tx buffer
            for (i = 0; i <= len; ++i)
            {
                *(Uint16*) (p->xbuf_start + xbuf_off) = aBuf[i];
                TRACE_TERSE1("DCS7: Tx: 0x%x", aBuf[i]);
                xbuf_off += 2;
                if (xbuf_off >= xbuf_len)
                {
                    xbuf_off -= xbuf_len;
                }
            }
            p->xbuf_off = xbuf_off;
        }
    }

    // Low level write
    handle->fxns->writeIssue(handle, (void*)(p->xbuf_start), (len+1)<<1);

    return RETURN_SUCCESS;
}

/**
 *  @brief     Configure SPI interface
 *
 *  @param[in] handle      DCS handle
 *
 *  @retval    Success or fail
 *
 */
Uint32 DCS7_configureSPI(DCS7_Handle handle)
{
    SPI_Handle spi = NULL;
    DCS7_Params_Dev_Spi *pSpi = handle->params->pDev->pDevData.pSpi;
    SPI_Params *spiParams = pSpi->spiParams;
    Uint32 spiInstance = 0;
    SPI_v0_HWAttrs spi_cfg;

    // Select the instance
    if (handle->params->dev == DCS7_PARAMS_DEV_SPI0)
        spiInstance = 0;
    else if (handle->params->dev == DCS7_PARAMS_DEV_SPI1)
        spiInstance = 1;
    else
        return RETURN_ERROR1;

    // Check if spi LLD is already open
    //if (pSpi->hSpi)
    //    return;

    // Get the default SPI init configurations
    SPI_socGetInitCfg(spiInstance, &spi_cfg);

    // Set pin mode
    spi_cfg.pinMode = (pSpi->np == DCS7_PARAMS_DEV_SPI_NP_4_SCS) ?
                       SPI_PINMODE_4_PIN :
                       SPI_PINMODE_3_PIN;
    // Set SPI init configurations
    SPI_socSetInitCfg(spiInstance, &spi_cfg);

    SPI_init();

    spi = SPI_open(spiInstance, spiParams);

    // Set spi LLD handle
    pSpi->hSpi = spi;

    return RETURN_SUCCESS;
}

/**
 *  @brief     Configure I2C interface
 *
 *  @param[in] handle      DCS handle
 *
 *  @retval    Success or fail
 *
 */
Uint32 DCS7_configureI2C(DCS7_Handle handle)
{
    I2C_Handle i2c = NULL;
    DCS7_Params_Dev_I2c *pI2c = handle->params->pDev->pDevData.pI2c;
    I2C_Params *i2cParams = &(pI2c->i2cParams);
    Uint32 i2cInstance = 0;
    I2C_HwAttrs i2c_cfg;

    // Select instance
    if (handle->params->dev == DCS7_PARAMS_DEV_I2C0)
        i2cInstance = 0;
    else if (handle->params->dev == DCS7_PARAMS_DEV_I2C1)
        i2cInstance = 1;
    else
        return RETURN_ERROR1;

    // Check if i2c LLD is already open
    //if (pI2c->hI2c)
    //    return;

    /* Get the default i2c init configurations */
    I2C_socGetInitCfg(i2cInstance, &i2c_cfg);

    /* Modify the default i2c configurations if necessary */
    I2C_socSetInitCfg(i2cInstance, &i2c_cfg);

    I2C_init();

    i2c = I2C_open(i2cInstance, i2cParams);

    // Set i2c LLD handle
    pI2c->hI2c = i2c;

    return RETURN_SUCCESS;
}

/**
 *  @brief     Configure UART interface
 *
 *  @param[in] handle      DCS handle
 *
 *  @retval    Success or fail
 *
 */
Uint32 DCS7_configureUART(DCS7_Handle handle)
{
    UART_Handle uart = NULL;
    DCS7_Params_Dev_Uart *pUart = handle->params->pDev->pDevData.pUart;
    UART_Params *uartParams = &(pUart->uartParams);
    Uint32 uartInstance = 0;

    // Select instance
    if (handle->params->dev == DCS7_PARAMS_DEV_UART0)
        uartInstance = 0;
    else if (handle->params->dev == DCS7_PARAMS_DEV_UART1)
        uartInstance = 1;
    else if (handle->params->dev == DCS7_PARAMS_DEV_UART2)
        uartInstance = 2;
    else
        return RETURN_ERROR1;   //  return error

    // Check if UART LLD is already open
    //if (pUart->hUart)
    //    return;

#ifdef DCS7_UART_EDMA_ENABLE
    /* Configure EDMA */
    if (handle->params->pDev->pDevData.pUart->isEMDAConfigured != 0)
    {
        UART_HwAttrs uart_cfg;
        /* Get the default UART init configurations */
        UART_socGetInitCfg(BOARD_UART_INSTANCE, &uart_cfg);

        /* Modify the default UART configurations if necessary */
        UART_socSetInitCfg(BOARD_UART_INSTANCE, &uart_cfg);
        
        handle->params->pDev->pDevData.pUart->isEMDAConfigured = 1;
    }
#else
    {
        UART_HwAttrs uart_cfg;
        /* Get the default UART init configurations */
        UART_socGetInitCfg(uartInstance, &uart_cfg);
        
        /* Modify the default UART configurations if necessary */
        UART_socSetInitCfg(uartInstance, &uart_cfg);
    }
#endif

    UART_init();

    /* Create a UART with data processing. */
    UART_Params_init(uartParams);

    // Set UART LLD parameters here
    uartParams->baudRate = pUart->baudRate;         // this is the required baud rate
    uartParams->readDataMode = UART_DATA_BINARY;    // do not handle CRLF
    uartParams->writeDataMode = UART_DATA_BINARY;   // do not handle CRLF
#ifndef DCS7_UART_EDMA_ENABLE
    uartParams->readReturnMode = UART_RETURN_FULL;  // return only when all data
                                                    // is received
#endif
    uartParams->readEcho = UART_ECHO_OFF;  // donot Echo received data back
    uartParams->dataLength = UART_LEN_8;   // 8 Bits Data length for UART
    uartParams->stopBits = UART_STOP_ONE;  // 1 Stop bit
    uartParams->parityType = UART_PAR_NONE;

    uartParams->readTimeout = BIOS_WAIT_FOREVER;
    uartParams->writeTimeout = BIOS_WAIT_FOREVER;

#ifdef UARTCALLBACK
    // NOTE: Callback mode is selected to have a
    // variable transfer timeouts, as individual
    // transfer timeout is not supported in present
    // LLD implementation.

    // Set same callback function, presuming
    // they will never be called simultaneously.
    uartParams->readCallback = pUart->transferCallbackFxn;
    uartParams->readMode = UART_MODE_CALLBACK;
    uartParams->writeCallback = pUart->transferCallbackFxn;
    uartParams->writeMode = UART_MODE_CALLBACK;
#else
    // Blocking mode
    uartParams->readMode = UART_MODE_BLOCKING;
    uartParams->writeMode = UART_MODE_BLOCKING;
#endif
    uart = UART_open(uartInstance, uartParams);

    // Set UART LLD handle
    pUart->hUart = uart;
    return RETURN_SUCCESS;
}

/**
 *  @brief     Common Configure
 *
 *  @param[in] handle      DCS handle
 *
 *  @retval    Success or fail
 *
 */
Uint32 DCS7_configure(DCS7_Handle handle)
{
    Uint32 returnValue = RETURN_SUCCESS;
    /* Configure Commmon parameters */
    handle->common.rbuf_start = (Uint32) handle->rxBuf;
    handle->common.rbuf_len = handle->params->rxSize;

    handle->common.xbuf_len = handle->params->txSize;
    handle->common.xbuf_start = (Uint32) handle->txBuf;

    handle->common.rbuf_off = 0;
    handle->common.rbuf_poff = 0;
    handle->common.xbuf_off = 0;
    handle->common.xbuf_poff = 0;

    /* Configure selected device */
    if (handle->params->dev == DCS7_PARAMS_DEV_SPI0 ||
        handle->params->dev == DCS7_PARAMS_DEV_SPI1)
        returnValue = handle->fxns->configureSPI(handle);
    else if (handle->params->dev == DCS7_PARAMS_DEV_I2C0 ||
             handle->params->dev == DCS7_PARAMS_DEV_I2C1)
        returnValue = handle->fxns->configureI2C(handle);
    else if (handle->params->dev == DCS7_PARAMS_DEV_UART0)
        returnValue = handle->fxns->configureUART(handle);

    return returnValue;
}

/**
 *  @brief     Reset SPI interface
 *
 *  @param[in] handle      DCS handle
 *
 *
 */
void DCS7_resetSPI(DCS7_Handle handle)
{
    SPI_Handle spi = handle->params->pDev->pDevData.pSpi->hSpi;

    if (spi != NULL)
    {
        // Abort SPI LLD read/write
        SPI_transferCancel(spi);
        // Close SPI LLD
        SPI_close(spi);
        handle->params->pDev->pDevData.pSpi->hSpi = NULL;
    }
}

/**
 *  @brief     Reset I2C interface
 *
 *  @param[in] handle      DCS handle
 *
 *
 */
void DCS7_resetI2C(DCS7_Handle handle)
{
    I2C_Handle i2c = handle->params->pDev->pDevData.pI2c->hI2c;

    if (i2c != NULL)
    {
        // Close I2C LLD
        I2C_close(i2c);
        handle->params->pDev->pDevData.pI2c->hI2c = NULL;
    }
}

/**
 *  @brief     Reset UART interface
 *
 *  @param[in] handle      DCS handle
 *
 */
void DCS7_resetUART(DCS7_Handle handle)
{
    DCS7_Params_Dev_Uart *pUart = handle->params->pDev->pDevData.pUart;
    UART_Handle uart = pUart->hUart;

    if (uart != NULL)
    {
#ifdef UARTCALLBACK
         // Reset SWI parameters
         pUart->hSwi->readBufIndex = 0;
         pUart->hSwi->s1_count = 0;
         pUart->hSwi->swiBreakFlag = 0;
         memset(pUart->hSwi->rxS1Size, 0, SRECORD_REC_MAX_S1_CNT);
         pUart->hCallback->opMode = UART_CALLBACK_CANCEL;
#endif
         // Abort UARD LLD read/write
         UART_readCancel(uart);
         UART_writeCancel(uart);
         // Close UART LLD
         UART_close(uart);
         handle->params->pDev->pDevData.pUart->hUart = NULL;
    }
}

/**
 *  @brief     Generic Reset
 *
 *  @param[in] handle      DCS handle
 *
 */
void DCS7_reset(DCS7_Handle handle)
{
    /* Reset selected device */
    if (handle->params->dev == DCS7_PARAMS_DEV_SPI0 ||
            handle->params->dev == DCS7_PARAMS_DEV_SPI1)
        handle->fxns->resetSPI(handle);
    else if (handle->params->dev == DCS7_PARAMS_DEV_I2C0 ||
            handle->params->dev == DCS7_PARAMS_DEV_I2C1)
        handle->fxns->resetI2C(handle);
    else if (handle->params->dev == DCS7_PARAMS_DEV_UART0)
        handle->fxns->resetUART(handle);

    // reset semaphore
    handle->fxns->semReset(handle, handle->sem, 0);
}

/**
 *  @brief     SPI interface resource allocation
 *
 *  @param[in] handle      DCS handle
 *
 */
void DCS7_resAllocSPI(DCS7_Handle handle)
{
    SPI_Transaction *transaction =
            &(handle->params->pDev->pDevData.pSpi->transaction);

    // Assign transfer buffers for SPI
    transaction->txBuf = handle->mediaLayer.txBuf;
    transaction->rxBuf = handle->mediaLayer.rxBuf;
    transaction->arg = (void *) handle;  // used for callback mode
}

/**
 *  @brief     I2C interface resource allocation
 *
 *  @param[in] handle      DCS handle
 *
 */
void DCS7_resAllocI2C(DCS7_Handle handle)
{
    I2C_Transaction *transaction =
            &(handle->params->pDev->pDevData.pI2c->transaction);

    // Assign transfer buffers for I2C
    transaction->writeBuf = handle->mediaLayer.txBuf;
    transaction->readBuf = handle->mediaLayer.rxBuf;

    // Set Own Slave Address
    transaction->slaveAddress =
            handle->params->pDev->pDevData.pI2c->ownSlaveAddress;
}

/**
 *  @brief     UART interface resource allocation
 *
 *  @param[in] handle      DCS handle
 *
 */
Uint32 DCS7_resAllocUART(DCS7_Handle handle)
{
    DCS7_Params_Dev_Uart *pUartParam = handle->params->pDev->pDevData.pUart;

    // Allocate scratch buffer for temporary use
    pUartParam->scratchBuf = (Uint8*) handle->fxns->memAlloc(handle,
            handle->config->bufMemSeg, pUartParam->scratchBufSize,
            handle->config->cacheMode == DCS7_CONFIG_CACHEMODE_ENABLE ?
                    handle->config->cacheBufAlign : 4);

    if (pUartParam->scratchBuf == NULL)
    {
        return RETURN_ERROR4;
    }

    return RETURN_SUCCESS;
}

/**
 *  @brief     Generic resource allocation
 *
 *  @param[in] handle      DCS handle
 *
 */
Uint32 DCS7_resAlloc(DCS7_Handle handle)
{
    Uint32 returnValue = RETURN_SUCCESS;

    /* Allocate buffers */
    handle->rxBuf = 
            (Uint16*) handle->fxns->memAlloc(handle,
                                             handle->config->bufMemSeg, 
                                             handle->params->rxSize,
                                             handle->config->cacheMode == 
                                             DCS7_CONFIG_CACHEMODE_ENABLE ?
                                             handle->config->cacheBufAlign : 4);
    handle->txBuf = 
            (Uint16*) handle->fxns->memAlloc(handle,
                                             handle->config->bufMemSeg,
                                             handle->params->txSize,
                                             handle->config->cacheMode == 
                                             DCS7_CONFIG_CACHEMODE_ENABLE ?
                                             handle->config->cacheBufAlign : 4);
    if (!handle->rxBuf || !handle->txBuf)
    {
        TRACE_TERSE0("DCS7: Buffer allocation failed");
        return RETURN_ERROR1;
    }

    /* Allocate semaphore for synchronization between task and interrupt */
    if (!(handle->sem = handle->fxns->semAlloc(handle)))
    {
        TRACE_TERSE0("DCS7: Semaphore allocation failed");
        return RETURN_ERROR2;
    }

    /* Allocate Media Layer resources */
    if (handle->fxns->resAllocMediaLayer(handle))
    {
        TRACE_TERSE0("DCS7: Media Layer allocation failed");
        return RETURN_ERROR3;
    }

    /* Allocate selected device */
    if (handle->params->dev == DCS7_PARAMS_DEV_SPI0)
        handle->fxns->resAllocSPI(handle);
    else if (handle->params->dev == DCS7_PARAMS_DEV_I2C0 ||
            handle->params->dev == DCS7_PARAMS_DEV_I2C1)
        handle->fxns->resAllocI2C(handle);
    else if (handle->params->dev == DCS7_PARAMS_DEV_UART0)
        returnValue = handle->fxns->resAllocUART(handle);

    return returnValue;
}

/**
 *  @brief     DCS open, allocate resource and initilize the interface
 *
 *  @param[in] p      DCS parameters
 *
 *  @param[in] c      DCS configuration
 *
 *  @param[in] f      DCS Functions
 *
 *  @param[in] a      Not used
 *
 *  @retval    Number of byte written
 *
 */
DCS7_Handle DCS7_open(const DCS7_Params *p,
                      const DCS7_Config *c,
                      const DCS7_Fxns *f,
                      ACP_Handle a)
{
    Uint32 returnValue = RETURN_SUCCESS;

    if (!p)
        p = (DCS7_Params*) &DCS7_PARAMS;
    if (!c)
        c = (DCS7_Config*) &DCS7_CONFIG;
    if (!f)
        f = (DCS7_Fxns*) &DCS7_FXNS;
    
    TRACE_TERSE0("DCS7: LOG started");

    /* Allocate space for DCS7 object */
    if (!(DCS7_HANDLE = f->memAlloc(NULL, c->objMemSeg,
                                    sizeof(DCS7_Obj), 4)))
    {
        TRACE_TERSE0("DCS7: Object allocation failed");
    }
    else
    {
        DCS7_HANDLE->size = sizeof(DCS7_Obj);
        DCS7_HANDLE->fxns = f;
        DCS7_HANDLE->params = p;
        DCS7_HANDLE->config = c;
        DCS7_HANDLE->ecnt = 0;
        DCS7_HANDLE->mediaLayer.size = sizeof(DCS7_MediaLayerObj);

        /* Allocate resources */
        if (!DCS7_HANDLE->fxns->resAlloc(DCS7_HANDLE))
        {
            /* Configure */
            DCS7_HANDLE->fxns->reset(DCS7_HANDLE);
            returnValue = DCS7_HANDLE->fxns->configure(DCS7_HANDLE);
            if (returnValue != RETURN_SUCCESS)
            {
                TRACE_TERSE0("DCS7: Interface configuration failed");
                return NULL;
            }
            DCS7_HANDLE->fxns->errorInit();
        }
        else
        {
            TRACE_TERSE0("DCS7: Resource allocation failed");
            return NULL;
        }
    }


    return DCS7_HANDLE;
}

