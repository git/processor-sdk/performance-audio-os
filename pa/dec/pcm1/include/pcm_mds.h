/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Vendor-specific (MDS) PCM algorithm interface declarations
//
//

/*
 *  Vendor specific (MDS) interface header for PCM algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular PCM implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef PCM_MDS_
#define PCM_MDS_

#include <ti/xdais/ialg.h> //<ialg.h>

#include <ipcm.h>
#include <icom.h>
#include <com_tii_priv.h>

/*
 *  ======== PCM_MDS_exit ========
 *  Required module finalization function
 */
#define PCM_MDS_exit COM_TII_exit

/*
 *  ======== PCM_MDS_init ========
 *  Required module initialization function
 */
#define PCM_MDS_init COM_TII_init

/*
 *  ======== PCM_MDS_IALG ========
 *  MDS's implementation of PCM's IALG interface
 */
extern IALG_Fxns PCM_MDS_IALG; 

/*
 *  ======== PCM_MDS_IPCM ========
 *  MDS's implementation of PCM's IPCM interface
 */
extern const IPCM_Fxns PCM_MDS_IPCM; 


/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of MDS's PCM algorithm.
 *  However, other implementation specific operations can also
 *  be added.
 */

/*
 *  ======== PCM_MDS_Handle ========
 */
typedef struct PCM_MDS_Obj *PCM_MDS_Handle;

/*
 *  ======== PCM_MDS_Params ========
 *  We don't add any new parameters to the standard ones defined by IPCM.
 */
typedef IPCM_Params PCM_MDS_Params;

/*
 *  ======== PCM_MDS_Status ========
 *  We don't add any new status to the standard one defined by IPCM.
 */
typedef IPCM_Status PCM_MDS_Status;

/*
 *  ======== PCM_MDS_Config ========
 *  We don't add any new config to the standard one defined by IPCM.
 */
typedef IPCM_Config PCM_MDS_Config;

/*
 *  ======== PCM_MDS_Active ========
 *  We don't add any new active to the standard one defined by IPCM.
 */
typedef IPCM_Active PCM_MDS_Active;

/*
 *  ======== PCM_MDS_Scrach ========
 *  We don't add any new active to the standard one defined by IAC3.
 */
typedef IPCM_Scrach PCM_MDS_Scrach;


/*
 *  ======== PCM_MDS_PARAMS ========
 *  Define our default parameters.
 */
#define PCM_MDS_PARAMS   IPCM_PARAMS

/*
 *  ======== PCM_MDS_create ========
 *  Create a PCM_MDS instance object.
 */
extern PCM_MDS_Handle PCM_MDS_create(const PCM_MDS_Params *params);

/*
 *  ======== PCM_MDS_delete ========
 *  Delete a PCM_MDS instance object.
 */
#define PCM_MDS_delete COM_TII_delete

#endif  /* PCM_MDS_ */
