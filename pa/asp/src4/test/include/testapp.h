/*
 *  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
 *  ALL RIGHTS RESERVED 
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *  @file  srctest.c
 *
 *  @brief SRC test application main code.
 *
 *          This is a SRC test application.
 *          The application test the various implementation of SRC.
 *
 *          It expects the input from a wave file and generate the output
 *          wave file after sampling rate conversion.
 *
 *          The SRC parameters are recieved from external tables.
 *          The test application will configure the neccessary parameters
 *          and allocate memory for SRC input/output buffer
 *          and SRC memory states.
 *****************************************************************************/

/************************************************************
                     Include files
*************************************************************/
#ifndef __TESTAPP_H__
#define __TESTAPP_H__

#include <xdc\std.h>
#include <isrc.h>
#include <src.h>
#include <src_tih.h>
#include <pafhjt.h>
#include <stdasp.h>
#include <paftyp_a.h>

/************************************************************
                      Macro Definitions
*************************************************************/
#define MAX_FILE_NAME_SIZE  70
#define MAX_CHANNEL        32
#define MAX_NSAMPLES 1024

/************************************************************
                     Enum's & Struct's
*************************************************************/
/** @brief Global SRC table index*/
typedef enum {
    TESTAPP_UPSAMPLE_2X,
    TESTAPP_UPSAMPLE_4X,
    TESTAPP_DOWNSAMPLE_2X,
    TESTAPP_DOWNSAMPLE_4X,
    TESTAPP_SRC_COUNT
} TESTAPP_SRC_INDEX;

/** @brief Test table representing SRC test case inputs*/
typedef struct {
    ISRC_Params *srcParams; // SRC params structure
    Int chSat;              // Sat Channels
    Int chSub;              //Sub channels
    Int inBufSampleCount;   // Macro AUDIO_SAMPLES
    Int outBufSampleCount;  //Outbuf SampleCount
    Int outputSampleRate;   //output sample rate
}tTestTable, *pTestTable;

/************************************************************
                Function Declarations
*************************************************************/
/**
 *  @brief     Sampling Rate mapping
 *
 *  @param[in] samplingRate
 *
 *  @retval    Return PAF mapped sampling rate macro for the input
 *
 *  @remark    We need the above for channel configuration in pAudioframe
 *
 */
int testApp_sampleRate_mapping(int samplingRate);

/**
 *  @brief     Channel mapping
 *
 *  @param[in] reqChCount
 *
 *  @retval    Return number of channels for given PAF channel macro
 *
 *  @remark    We need this to give number of channels input to FIO
 *
 */
int testApp_channel_mapping(int reqChCount);

#endif  /* __TESTAPP_H__ */
/***************************End of file***************************************/

