/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/************************************************************
                     Include files
*************************************************************/
#include <stdio.h>
#include <math.h>
#include <xdc/std.h>
#include <alg.h>
#include <pcm.h>
#include <stdlib.h>
#include <string.h>
#include "paftyp.h"
#include "pcm_test.h"

const test_case pcm_test_cases[28] = {

		{//1
				"Stereo Pass through",
				//Channel Configuraion
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/stereo_48k.wav",
				"test_vectors/output/output_1.wav",
				0,
				2,
				2,
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//2
				"Stereo Revert channel Map",
				//Channel Configuraion
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_RGHT,PAF_LEFT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/stereo_48k.wav",
				"test_vectors/output/output_2.wav",
				0,
				2,
				2,
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//3
				"Stereo Revert channel Map & Increase Amplitude",
				//Channel configuration
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/stereo_48k.wav",
				"test_vectors/output/output_3.wav",
				3,
				2,
				2,
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//4
				"Stereo Revert channel Map & Reduce Amplitude",
				//Channel configuration
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/stereo_48k.wav",
				"test_vectors/output/output_4.wav",
				-3,
				2,
				2,
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//5
				"Stereo case with fixed to floating point conversion",
				//Channel configuration
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/stereo_48k.wav",
				"test_vectors/output/output_5.wav",
				-3,
				2,
				2,
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				1
		},{//6
				"5.1 input Pass through ",
				//Channel configuration
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//"test_vectors/input/32bit_5.1.wav",
				"test_vectors/input/32bit_5.1.wav",
				"test_vectors/output/output_6.wav",
				0,
				6,
				6,
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				0
		},{//7
				"5.1 input amplitude increased",
				//Channel configuration
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_5.1.wav",
				"test_vectors/output/output_7.wav",
				3,
				6,
				6,
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				0
		},{//8
				"5.1 input amplitude reduced ",
				//Channel configuration
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_5.1.wav",
				"test_vectors/output/output_8.wav",
				-3,
				6,
				6,
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				0
		},{//9
				"5.1 input Altering Mapping ",
				//Channel configuration
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_RGHT,PAF_LEFT,PAF_RSUR,PAF_LSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_5.1.wav",
				"test_vectors/output/output_9.wav",
				0,
				6,
				6,
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				0
		},{//10
				"5.1 input Altering Mapping & fixed to floating point decode",
				//Channel configuration
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_RGHT,PAF_LEFT,PAF_RSUR,PAF_LSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_5.1.wav",
				"test_vectors/output/output_10.wav",
				0,
				6,
				6,
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				1
		},{//11
				"5.1 input to Stereo Downmix",
				//Channel configuration
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_5.1.wav",
				"test_vectors/output/output_11.wav",
				0,
				6,
				2,
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//12
				"5.1 input to Stereo Downmix, scaled - amplified ",
				//Channel configuration
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_5.1.wav",
				"test_vectors/output/output_12.wav",
				3,
				6,
				2,
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//13
				"7.1 input Pass through ",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_13.wav",
				0,
				8,
				8,
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				0
		},{//14
				"7.1 input Pass through, scaled (Amplitude increased)",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_14.wav",
				3,
				8,
				8,
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				0
		},{//15
				"7.1 input Pass through, scaled (Amplitude decreased)",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_15.wav",
				-3,
				8,
				8,
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				0
		},{//16
				"7.1 input Pass through, fixed to floating point decode",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_16.wav",
				-3,
				8,
				8,
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				1
		},{//17
				"7.1 input to 5.1 downmix",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_17.wav",
				0,
				8,
				6,
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				0
		},{//18
				"7.1 input to stereo downmix ",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_18.wav",
				0,
				8,
				2,
				PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//19
				"5.1 input to stereo-3 downmix ",
				//Channel configuration
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//"test_vectors/input/32bit_5.1.wav",
				"test_vectors/input/32bit_5.1.wav",
				"test_vectors/output/output_19.wav",
				-3,
				6,
				3,
				PAF_CC_SAT_3STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//20
				"7.1 input to stereo-3 downmix ",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_20.wav",
				-6,
				8,
				3,
				PAF_CC_SAT_3STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//21
				"Surround4 to Panthom2 ",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_21.wav",
				0,
				8,
				4,
				PAF_CC_SAT_PHANTOM2, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//22
				"Surround4 to panthom3 ",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
		 		//Channel Mapping From
				//0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//0, 1, 4, 5, 6, 7, 6, 7, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_22.wav",
				0,
				8,
				5,
				PAF_CC_SAT_PHANTOM3, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//23
				"Surround4 to panthom1 ",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_23.wav",
				0,
				8,
				3,
				PAF_CC_SAT_PHANTOM1, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//24
				"Surround4 to panthom4 ",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_24.wav",
				0,
				8,
				3,
				PAF_CC_SAT_PHANTOM4, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//25
				"Surround4 to surround2 ",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_25.wav",
				0,
				8,
				5,
				PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//26
				"Surround4 to surround3 ",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_26.wav",
				0,
				8,
				6,
				PAF_CC_SAT_SURROUND3, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//27
				"Surround4 to surround1 ",
				//Channel configuration
				PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
				//Channel Mapping From
				0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				//Channel Mapping To
				PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB,-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
				"test_vectors/input/32bit_7.1.wav",
				"test_vectors/output/output_27.wav",
				0,
				8,
				4,
				PAF_CC_SAT_SURROUND1, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
				0
		},{//28
			"Stereo to Mono downmix",
			//Channel Configuraion
			PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
			//Channel Mapping From
			0, 1, 4, 5, 2, -3, 6, 7, -3, -3, -3, -3, -3, -3, 3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
			//Channel Mapping To
			PAF_CNTR,PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_LCTR,PAF_RCTR,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LTFT,PAF_RTFT,PAF_LTMD,PAF_RTMD,PAF_LSUB,PAF_RSUB, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
			"test_vectors/input/stereo_48k.wav",
			"test_vectors/output/output_28.wav",
			0,
			2,
			1,
			PAF_CC_SAT_MONO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
			0
	   }
};
