/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/************************************************************
                     Include files
*************************************************************/
#include <stdio.h>
#include <math.h>
#include <xdc/std.h>
#include <alg.h>
#include <pcm.h>
#include <stdlib.h>
#include <string.h>
#include "paftyp.h"
#include "procsdk_audio_typ.h"
#include "ialg.h"
#include "alg.h"
#include "stdasp.h"
#include "pcm_mds.h"

typedef struct test_case_s{
	char charTestCaseName[100];/* Test case name*/
	PAF_ChannelConfiguration channelConfigurationStream;/* Input channel configuration*/
    PAF_ChannelMap_HD channelMap;/* Channel Mapping */
    char charInFileName[50];/* Input audio file name */
    char charOutFileName[50];/* Output audio file name */
    int scaleVolume;/* Volume in dB*/
    int numOfInChannels;/* Number of input channels*/
    int numOfOutChannels;/* Number of output channels*/
	PAF_ChannelConfiguration channelConfigurationRequest;/* Output channel configuration*/
	int typeConversionEnabled;/* Fixed point to floating point conversion*/
} test_case;


/* Wav file header format

	char chunk_id[4];
    int chunk_size;
    char format[4];
    char subchunk1_id[4];
    int subchunk1_size;
    short int audio_format;
    short int num_channels;
    int sample_rate;
    int byte_rate;
    short int block_align;
    short int bits_per_sample;
    char subchunk2_id[4];
    int subchunk2_size;
*/
/* Prepared three structures as the subchunk data can be of variable size based on the wav file type */
typedef struct
{
    char chunk_id[4];
    int chunk_size;
    char format[4];
    char subchunk1_id[4];
    int subchunk1_size;
}header_1, *header_1p;
typedef struct
{
	char subchunk2_id[4];
	int subchunk2_size;
} header_2, *header_2p;
typedef struct{
	short int audio_format;
	short int num_channels;
	int sample_rate;
	int byte_rate;
	short int block_align;
	short int bits_per_sample;
}subchunk_data;
