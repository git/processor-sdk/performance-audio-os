
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Channel Configuration Declarations
//
//
//

#ifndef PAFCC_
#define PAFCC_

// Quoting the user guide:
// The PA Framework contains sophisticated controls for channel configuration. The channel
// configuration describes how the audio data is organized into channels. Many facets of
// the listening experience can be controlled via the channel configuration, such as the number
// of channels, the content of those channels, etc. 
// Specifically, the channel configuration tells us whether or not a channel is in use, and 
// it also tries to tell us what that channel is used for.

// The channel configuration has grown over time.  This documentation assumes that
// the system is configured with PAF_MAXNUMCHAN = 32 and ATMOS defined.
//
// A channel configuration is a 64-bit value that can either be interpreted
// in parts or in full. The initial fields are "original", from the days when 
// 8 channels was all anyone could ever ask for.  The enumerated form of the sat member 
// reflects this.
// The extMask members are more simply bit fields.
// There are sometimes more than one way to describe a channel as active.
//



#include <xdc/std.h>
#include <ti/xdais/xdas.h>

typedef union PAF_ChannelConfiguration {
    struct {
        XDAS_Int8 sat;  // describes channels up to 7.x
        XDAS_Int8 sub;  // simply how many subwoofers
        XDAS_Int8 aux;  // indicates coding methods like matrixing
        XDAS_Int8 extMask;  // describes channels between 8 and 16.
        XDAS_Int8 extMask2; // mostly describes channels between 23 and 31.
        XDAS_Int8 extMask3; // covers other cases.
        XDAS_Int8 legacyMode;  // replaced reserved1; used by DTSX
        XDAS_Int8 reserved2;
    } part;
    struct {
        XDAS_Int32 lowWord;
        XDAS_Int32 hiWord; 
    } words;
    XDAS_Int32 legacy;  // old code might only look at the lower 32 bits.
    long long full;
} PAF_ChannelConfiguration;

// An enumeration is used to populate the sat field, describing "satellite" speakers.
//
// The basic terminology is well-known, but some discussion of particulars
// is needed:
//
// . The terms Mono, Stereo, 3 Stereo, Phantom (Center), and Surround have
//   their Dolby meanings.
//
// . Qualifiers of the form "R.S" or "R_S" are appended to many terms,
//   meaning R rear (surround + back) and S subwoofers.
//
// . The terms Phantom 0.X and Surround 0.X are preferred over Stereo and
//   3 Stereo in some contexts for technical reasons.
//
// . The unqualified terms Mono and Stereo assume no subwoofer while others,
//   including 3 Stereo, assume a subwoofer, though the contents of the sub field apply.
//
// The locations of each channel (in the audio frame's data pointer array) is specified
// by symbols like PAF_LEFT defined in paftyp_a.h.


// Satellite field usage:
enum PAF_CC_SAT {
    PAF_CC_SAT_UNKNOWN,
    PAF_CC_SAT_NONE,
    PAF_CC_SAT_MONO,       // center only
    PAF_CC_SAT_STEREO,     // L, R                             
    PAF_CC_SAT_PHANTOM1,   // L, R, single surround             
    PAF_CC_SAT_PHANTOM2,   // L, R, single surround, single back
    PAF_CC_SAT_PHANTOM3,   // L, R, L/R surround, single back   
    PAF_CC_SAT_PHANTOM4,   // L, R, L/R surround, L/R back      
    PAF_CC_SAT_3STEREO,    // L, R, C
    PAF_CC_SAT_SURROUND1,  // L, R, C, single surround
    PAF_CC_SAT_SURROUND2,  // L, R, C, single surround, single back
    PAF_CC_SAT_SURROUND3,  // L, R, C, L/R surround, single back
    PAF_CC_SAT_SURROUND4,  // L, R, C, L/R surround, L/R back
    PAF_CC_SAT_N
};


#define PAF_CC_SAT_PHANTOM0  PAF_CC_SAT_STEREO
#define PAF_CC_SAT_SURROUND0 PAF_CC_SAT_3STEREO

// Sub field usage:
enum PAF_CC_SUB {
    PAF_CC_SUB_ZERO,    // no sub
    PAF_CC_SUB_ONE,     // channel 12 (PAF_SUBW) is active for one sub
    PAF_CC_SUB_TWO,     // channels 12 (PAF_SUBW) and 13 (PAF_RSUB) are active for two subs
    PAF_CC_SUB_N
};

// Aux field usage when satellite is 2/0.S:
enum PAF_CC_AUX_STEREO {
    PAF_CC_AUX_STEREO_UNKNOWN,
    PAF_CC_AUX_STEREO_STEREO,
    PAF_CC_AUX_STEREO_LTRT,
    PAF_CC_AUX_STEREO_MONO,
    PAF_CC_AUX_STEREO_DUAL,             /* indicator only */
    PAF_CC_AUX_STEREO_N
};

// Aux field usage when satellite is X/2.S:
enum PAF_CC_AUX_SURROUND2 {
    PAF_CC_AUX_SURROUND2_UNKNOWN,       /* indicator or request */
    PAF_CC_AUX_SURROUND2_STEREO,        /* indicator only */
    PAF_CC_AUX_SURROUND2_LTRT,          /* indicator only */
    PAF_CC_AUX_SURROUND2_MONO,          /* indicator only */
    PAF_CC_AUX_SURROUND2_N
};

// Aux field usage when satellite is X/4.S:
enum PAF_CC_AUX_SURROUND4 {
    PAF_CC_AUX_SURROUND4_UNKNOWN,       /* indicator or request */
    PAF_CC_AUX_SURROUND4_STEREO,        /* indicator only */
    PAF_CC_AUX_SURROUND4_LTRT,          /* indicator only */
    PAF_CC_AUX_SURROUND4_MONO,          /* indicator only */
    PAF_CC_AUX_SURROUND4_N
};

// ............................................................................
// The extMask field is populated with bit masks specifying that a 
// channel or a pair of channels is in use.
enum PAF_CC_EXT {
    // "Wide" speakers are nominally surrounds.  
    // They occupy channels PAF_LWID (4) and PAF_RWID (5)
    PAF_CC_EXT_LwRw,        
    // left and right "center" speakers are nominally front speakers.
    // They occupy channels PAF_LCTR_HD (6) and PAF_RCTR_HD (7)
    // The "OVR" channels are usually specified to occupy 6 and 7.
    PAF_CC_EXT_LcRc,
    // left and right "head" spearkers are above.
    // They occupy channels PAF_LHED (14) and PAF_RHED (15)
    PAF_CC_EXT_LhRh,
    // channel 6 (PAF_CHED, or PAF_LSD) contains one speaker above.
    PAF_CC_EXT_Cvh,
    // channel 7 (PAF_OVER_HD, PAF_RSD, PAF_RHSI) contains one speaker.
    PAF_CC_EXT_Ts,
    // channels 6 (PAF_LHSI) and 7 (PAF_RHSI) are active.
    PAF_CC_EXT_LhsRhs,
    // channels 20 and 21 (PAF_LHBK, PAF_RHBK) are active nominally for upper rear speakers.
    PAF_CC_EXT_LhrRhr,
    // channel 22 (PAF_CHBK) is active for a center rear speaker.
    PAF_CC_EXT_Chr,
    PAF_CC_EXT_N            
};

// masks defining the use of the extMask field
// see comments defining usage directly above.
#define PAF_CC_EXTMASK_LwRw    (1u << PAF_CC_EXT_LwRw)   // 1
#define PAF_CC_EXTMASK_LcRc    (1u << PAF_CC_EXT_LcRc)   // 2
#define PAF_CC_EXTMASK_LhRh    (1u << PAF_CC_EXT_LhRh)   // 4
#define PAF_CC_EXTMASK_Cvh     (1u << PAF_CC_EXT_Cvh)    // 8
#define PAF_CC_EXTMASK_Ts      (1u << PAF_CC_EXT_Ts)     // 0x10
#define PAF_CC_EXTMASK_LhsRhs  (1u << PAF_CC_EXT_LhsRhs) // 0x20
#define PAF_CC_EXTMASK_LhrRhr  (1u << PAF_CC_EXT_LhrRhr) // 0x40
#define PAF_CC_EXTMASK_Chr     (1u << PAF_CC_EXT_Chr)    // 0x80

// ............................................................................
// bits defining the use of the extMask2 field
enum PAF_CC_EXT2 {
    // channels 30 and 31 contain data nominally for left and right screen.
    PAF_CC_EXT2_LscRsc,
    // channels 16 and 17 contain data nominally for left and right "surround 1"
    PAF_CC_EXT2_Ls1Rs1,
    // channels 18 and 19 contain data nominally for left and right "surround 2"
    PAF_CC_EXT2_Ls2Rs2,
    // channels 26 and 27 contain data nominally for left and right "surround 1"
    PAF_CC_EXT2_Lrs1Rrs1,
    // channels 28 and 29 contain data nominally for left and right "surround 2"
    PAF_CC_EXT2_Lrs2Rrs2,
    // channels 24 and 25 contain data nominally for left and right "Center Surround"
    PAF_CC_EXT2_LcsRcs,
    // channel 23 contains data nominally for Center Surround
    PAF_CC_EXT2_Cs,
    // channels 6 and 7 contain data for LSD and RSD.
    PAF_CC_EXT2_LsdRsd,
    PAF_CC_EXT2_N
};

// masks defining the use of the extMask2 field
// see definitions above.
#define PAF_CC_EXTMASK_LscRsc   (1u << PAF_CC_EXT2_LscRsc)       // 1    
#define PAF_CC_EXTMASK_Ls1Rs1   (1u << PAF_CC_EXT2_Ls1Rs1)       // 2    
#define PAF_CC_EXTMASK_Ls2Rs2   (1u << PAF_CC_EXT2_Ls2Rs2)       // 4    
#define PAF_CC_EXTMASK_Lrs1Rrs1	(1u << PAF_CC_EXT2_Lrs1Rrs1)     // 8    
#define PAF_CC_EXTMASK_Lrs2Rrs2	(1u << PAF_CC_EXT2_Lrs2Rrs2)     // 0x10 
#define PAF_CC_EXTMASK_LcsRcs   (1u << PAF_CC_EXT2_LcsRcs)       // 0x20 
#define PAF_CC_EXTMASK_Cs       (1u << PAF_CC_EXT2_Cs)           // 0x40 
#define PAF_CC_EXTMASK_LsdRsd   (1u << PAF_CC_EXT2_LsdRsd)       // 0x80 

// ............................................................................
enum PAF_CC_EXT3 {
    // specifies channels 14 and 15 (PAF_LTFH, PAF_RTFH) are in use.
    PAF_CC_EXT3_LfhRfh,
    // specifies that channels 14 and 15 (PAF_LTFT, PAF_RTFT) are in use. DTS
    // specifies that channels 14 and 5 (PAF_LTFT, PAF_RTFT) are in use. Non-DTS
    PAF_CC_EXT3_LtfRtf,
    // specifies channels 6 and 7 (PAF_LTMD, PAF_RTMD) are in use.
    PAF_CC_EXT3_LtmRtm,
    // specifies channels 4 and 5 (PAF_LTRR, PAF_RTRR) are in use.
    PAF_CC_EXT3_LtrRtr,
    // specifies channels 4 and 5 (PAF_LTRH, PAF_RTRH) are in use.
    PAF_CC_EXT3_LrhRrh,
    // Specifies that channels 2 and 3 are in use for PAF_LCTR, PAF_RCTR
    PAF_CC_EXT3_LctRct,     
    // Specifies that channels 20 (PAF_LHBK) and 21 (PAF_RHBK) are in use. 
    PAF_CC_EXT3_LhbkRhbk,
    // specifies that channel 22 (PAF_CHBK) is in use.
    PAF_CC_EXT3_Chbk,
    PAF_CC_EXT3_N
};

// masks defining the use of the extMask3 field
// see definitions above.
#define PAF_CC_EXTMASK_LfhRfh    (1u << PAF_CC_EXT3_LfhRfh)     // 1   
#define PAF_CC_EXTMASK_LtfRtf    (1u << PAF_CC_EXT3_LtfRtf)     // 2    
#define PAF_CC_EXTMASK_LtmRtm    (1u << PAF_CC_EXT3_LtmRtm)     // 4    
#define PAF_CC_EXTMASK_LtrRtr    (1u << PAF_CC_EXT3_LtrRtr)     // 8    
#define PAF_CC_EXTMASK_LrhRrh    (1u << PAF_CC_EXT3_LrhRrh)     // 0x10 
#define PAF_CC_EXTMASK_LctRct    (1u << PAF_CC_EXT3_LctRct)     // 0x20 
#define PAF_CC_EXTMASK_LhbkRhbk  (1u << PAF_CC_EXT3_LhbkRhbk)   // 0x40 
#define PAF_CC_EXTMASK_Chbk      (1u << PAF_CC_EXT3_Chbk)       // 0x80 

//---------------------------------------------------------------------
// Shorthands for full channel config
//---------------------------------------------------------------------

// ............................................................................
// shorthand for "5.1" 6 channel channel config
// sat: PAF_CC_SAT_SURROUND2.  Sets bits 0x0307 (bits 0,1,3, 8, 9)
// sub;  PAF_CC_SUB_ONE.  Sets bits 0x1000 (bit 12)
// aux;  0.
// extMask:  0
// extMask2: 0.
// extMask3; 0
// reserved1 = reserved2 = 0

#define PAFCC_SIX_CHANNEL_CONFIG_FULL 0x000000000000010ALL
// ............................................................................
// shorthand for "7.1" eight channel channel config
// sat: PAF_CC_SAT_SURROUND4.  Sets bits 0x0F07 (bits 0,1,3, 8, 9, 10, 11)
// sub;  PAF_CC_SUB_ONE.  Sets bits 0x1000 (bit 12)
// aux;  0.
// extMask:  0
// extMask2: 0.
// extMask3; 0
// reserved1 = reserved2 = 0
#define PAFCC_EIGHT_CHANNEL_CONFIG_FULL 0x000000000000010CLL

// ............................................................................
// Added for DTSX
#define PAFCC_TEN_CHANNEL_CONFIG_FULL 0x000000000400010CLL

// shorthand for "11.1" 12 channel channel mask (ATMOS version)
// PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LTRR,PAF_RTRR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,PAF_LTFT,PAF_RTFT,   
// sat: PAF_CC_SAT_SURROUND4.  Sets bits 0x0F07 (bits 0,1,2, 8, 9, 10, 11)  PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK
// sub;  PAF_CC_SUB_ONE.  Sets bits 0x1000 (bit 12)
// aux;  0.
// extMask:  0.
// extMask2: 0.
// extMask3; PAF_CC_EXTMASK_LtrRtr +  PAF_CC_EXTMASK_LtfRtf   = 0x0A
//      bits/channels     4,5                    6,7
//  PAF_LTRR,PAF_RTRR,PAF_LTFT,PAF_RTFT,   
// reserved1 = reserved2 = 0
#define PAFCC_TWELVE_CHANNEL_ATMOS_CONFIG_FULL 0x00000A000000010CLL

// ............................................................................
// shorthand for "11.1" 12 channel channel mask (non ATMOS version)
// PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LWID,PAF_RWID,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK,PAF_SUBW,PAF_LHED,PAF_RHED
// sat: PAF_CC_SAT_SURROUND4.  Sets bits 0x0F07 (bits 0,1,2, 8, 9, 10, 11)  PAF_LEFT,PAF_RGHT,PAF_CNTR,PAF_LSUR,PAF_RSUR,PAF_LBAK,PAF_RBAK
// sub;  PAF_CC_SUB_ONE.  Sets bits 0x1000 (bit 12)
// aux;  0.
// extMask:  0x05
//      bits/channels     4,5                    14,15
//              PAF_LWID,PAF_RWID,      PAF_LHED,PAF_RHED
// extMask2: 0.
// extMask3
// reserved1 = reserved2 = 0
#define PAFCC_TWELVE_CHANNEL_CONFIG_FULL 0x000000000500010CLL
 
// Added for DTSX
// ............................................................................
#define SATMASK_24 PAF_CC_SAT_PHANTOM4
#define SUBMASK_24 PAF_CC_SUB_TWO
#define AUXMASK_24 0
#define EXTMASK_24 (PAF_CC_EXTMASK_LwRw + PAF_CC_EXTMASK_LcRc + PAF_CC_EXTMASK_LhRh + PAF_CC_EXTMASK_LhrRhr + PAF_CC_EXTMASK_Chr)
#define EXTMASK2_24 (PAF_CC_EXTMASK_Ls1Rs1 + PAF_CC_EXTMASK_Ls2Rs2 + PAF_CC_EXTMASK_Cs)
#define EXTMASK3_24 (PAF_CC_EXTMASK_LtfRtf + PAF_CC_EXTMASK_LtmRtm + PAF_CC_EXTMASK_LtrRtr + PAF_CC_EXTMASK_LctRct)
#define RSVD2_24 0
#define RSVD1_24 0
// [rsvd2][rsvd1][ext3][ext2][ext][aux][sub][sat]LL
#define PAFCC_TWENTYFOUR_CHANNEL_CONFIG_FULL \
((RSVD2_24<<56)+(RSVD1_24<<48)+(EXTMASK3_24<<40)+(EXTMASK2_24<<32)+(EXTMASK_24<<24)+(AUXMASK_24<<16)+(SUBMASK_24<<8)+(SATMASK_24<<0))LL

// ............................................................................
// shorthand for "14.2" 16 channel channel mask
// sat: PAF_CC_SAT_PHANTOM4.  Sets bits 0x0F03 (bits 0,1 8, 9, 10, 11)
// sub;  PAF_CC_SUB_TWO.  Sets bits 0x3000 (bits 12 and 13)
// aux;  0.
// extMask:  PAF_CC_EXTMASK_LwRw (1) (bits 4 and 5)
// extMask2: 0.
// extMask3; PAF_CC_EXTMASK_LtrRtr + 
//         bits     4,5
// PAF_CC_EXTMASK_LctRct + PAF_CC_EXTMASK_LtfRtf + PAF_CC_EXTMASK_LtmRtm. (0x26)
//           bits 2,3, 6,7, 14,15
// reserved1 = reserved2 = 0
#define PAFCC_SIXTEEN_CHANNEL_CONFIG_FULL 0x0000260001000207LL

// ............................................................................
// Added for DTSX
#define PAFCC_NINE_TWO_FOUR_CHANNEL_CONFIG_FULL 0x000002000500020CLL

// shorthand for 32 channel channel mask
// sat: PAF_CC_SAT_PHANTOM4.  Sets bits 0x0F03 (bits 0,1 8,9, 10,11)
// sub;  PAF_CC_SUB_TWO.  Sets bits 0x3000 (bits 12 and 13)
// aux;  0.
// extMask: PAF_CC_EXTMASK_LwRw (1) (bits 4 and 5)
// extMask2: 0x7F:  bits 16,17, 18,19, 23, 24,25, 26,27, 28,29, 30,31; 
// extMask3; PAF_CC_EXTMASK_LctRct + PAF_CC_EXTMASK_LtfRtf + PAF_CC_EXTMASK_LtmRtm 
//         + PAF_CC_EXTMASK_LhbkRhbk + PAF_CC_EXT3_Chbk  (0xE6)
//           bits 2,3, 6,7, 14,15, 20,21, 22
// reserved1 = reserved2 = 0
#define PAFCC_THIRTYTWO_CHANNEL_CONFIG_FULL 0x00E67F01000207LL

// ............................................................................
// Qualified forms

#define PAF_CC_NONE_0 (PAF_CC_SAT_NONE+(PAF_CC_SUB_ZERO<<8))
#define PAF_CC_MONO_0 (PAF_CC_SAT_MONO+(PAF_CC_SUB_ZERO<<8))
#define PAF_CC_PHANTOM0_0 (PAF_CC_SAT_PHANTOM0+(PAF_CC_SUB_ZERO<<8))
#define PAF_CC_PHANTOM1_0 (PAF_CC_SAT_PHANTOM1+(PAF_CC_SUB_ZERO<<8))
#define PAF_CC_PHANTOM2_0 (PAF_CC_SAT_PHANTOM2+(PAF_CC_SUB_ZERO<<8))
#define PAF_CC_PHANTOM3_0 (PAF_CC_SAT_PHANTOM3+(PAF_CC_SUB_ZERO<<8))
#define PAF_CC_PHANTOM4_0 (PAF_CC_SAT_PHANTOM4+(PAF_CC_SUB_ZERO<<8))
#define PAF_CC_SURROUND0_0 (PAF_CC_SAT_SURROUND0+(PAF_CC_SUB_ZERO<<8))
#define PAF_CC_SURROUND1_0 (PAF_CC_SAT_SURROUND1+(PAF_CC_SUB_ZERO<<8))
#define PAF_CC_SURROUND2_0 (PAF_CC_SAT_SURROUND2+(PAF_CC_SUB_ZERO<<8))
#define PAF_CC_SURROUND3_0 (PAF_CC_SAT_SURROUND3+(PAF_CC_SUB_ZERO<<8))
#define PAF_CC_SURROUND4_0 (PAF_CC_SAT_SURROUND4+(PAF_CC_SUB_ZERO<<8))

#define PAF_CC_NONE_1 (PAF_CC_SAT_NONE+(PAF_CC_SUB_ONE<<8))
#define PAF_CC_MONO_1 (PAF_CC_SAT_MONO+(PAF_CC_SUB_ONE<<8))
#define PAF_CC_PHANTOM0_1 (PAF_CC_SAT_PHANTOM0+(PAF_CC_SUB_ONE<<8))
#define PAF_CC_PHANTOM1_1 (PAF_CC_SAT_PHANTOM1+(PAF_CC_SUB_ONE<<8))
#define PAF_CC_PHANTOM2_1 (PAF_CC_SAT_PHANTOM2+(PAF_CC_SUB_ONE<<8))
#define PAF_CC_PHANTOM3_1 (PAF_CC_SAT_PHANTOM3+(PAF_CC_SUB_ONE<<8))
#define PAF_CC_PHANTOM4_1 (PAF_CC_SAT_PHANTOM4+(PAF_CC_SUB_ONE<<8))
#define PAF_CC_SURROUND0_1 (PAF_CC_SAT_SURROUND0+(PAF_CC_SUB_ONE<<8))
#define PAF_CC_SURROUND1_1 (PAF_CC_SAT_SURROUND1+(PAF_CC_SUB_ONE<<8))
#define PAF_CC_SURROUND2_1 (PAF_CC_SAT_SURROUND2+(PAF_CC_SUB_ONE<<8))
#define PAF_CC_SURROUND3_1 (PAF_CC_SAT_SURROUND3+(PAF_CC_SUB_ONE<<8))
#define PAF_CC_SURROUND4_1 (PAF_CC_SAT_SURROUND4+(PAF_CC_SUB_ONE<<8))

#define PAF_CC_NONE_2 (PAF_CC_SAT_NONE+(PAF_CC_SUB_TWO<<8))
#define PAF_CC_MONO_2 (PAF_CC_SAT_MONO+(PAF_CC_SUB_TWO<<8))
#define PAF_CC_PHANTOM0_2 (PAF_CC_SAT_PHANTOM0+(PAF_CC_SUB_TWO<<8))
#define PAF_CC_PHANTOM1_2 (PAF_CC_SAT_PHANTOM1+(PAF_CC_SUB_TWO<<8))
#define PAF_CC_PHANTOM2_2 (PAF_CC_SAT_PHANTOM2+(PAF_CC_SUB_TWO<<8))
#define PAF_CC_PHANTOM3_2 (PAF_CC_SAT_PHANTOM3+(PAF_CC_SUB_TWO<<8))
#define PAF_CC_PHANTOM4_2 (PAF_CC_SAT_PHANTOM4+(PAF_CC_SUB_TWO<<8))
#define PAF_CC_SURROUND0_2 (PAF_CC_SAT_SURROUND0+(PAF_CC_SUB_TWO<<8))
#define PAF_CC_SURROUND1_2 (PAF_CC_SAT_SURROUND1+(PAF_CC_SUB_TWO<<8))
#define PAF_CC_SURROUND2_2 (PAF_CC_SAT_SURROUND2+(PAF_CC_SUB_TWO<<8))
#define PAF_CC_SURROUND3_2 (PAF_CC_SAT_SURROUND3+(PAF_CC_SUB_TWO<<8))
#define PAF_CC_SURROUND4_2 (PAF_CC_SAT_SURROUND4+(PAF_CC_SUB_TWO<<8))

#define PAF_CC_STEREO_UNKNOWN (PAF_CC_SAT_STEREO+(PAF_CC_AUX_STEREO_UNKNOWN<<16))
#define PAF_CC_STEREO_STEREO (PAF_CC_SAT_STEREO+(PAF_CC_AUX_STEREO_STEREO<<16))
#define PAF_CC_STEREO_LTRT (PAF_CC_SAT_STEREO+(PAF_CC_AUX_STEREO_LTRT<<16))
#define PAF_CC_STEREO_MONO (PAF_CC_SAT_STEREO+(PAF_CC_AUX_STEREO_MONO<<16))
#define PAF_CC_STEREO_DUAL (PAF_CC_SAT_STEREO+(PAF_CC_AUX_STEREO_DUAL<<16))

// Unqualified forms - tricky!

#define PAF_CC_UNKNOWN PAF_CC_SAT_UNKNOWN
#define PAF_CC_NONE PAF_CC_NONE_0
#define PAF_CC_MONO PAF_CC_MONO_0
#define PAF_CC_STEREO PAF_CC_STEREO_STEREO
#define PAF_CC_PHANTOM0 PAF_CC_PHANTOM0_1
#define PAF_CC_PHANTOM1 PAF_CC_PHANTOM1_1
#define PAF_CC_PHANTOM2 PAF_CC_PHANTOM2_1
#define PAF_CC_PHANTOM3 PAF_CC_PHANTOM3_1
#define PAF_CC_PHANTOM4 PAF_CC_PHANTOM4_1
#define PAF_CC_3STEREO PAF_CC_SURROUND0_1
#define PAF_CC_SURROUND0 PAF_CC_SURROUND0_1
#define PAF_CC_SURROUND1 PAF_CC_SURROUND1_1
#define PAF_CC_SURROUND2 PAF_CC_SURROUND2_1
#define PAF_CC_SURROUND3 PAF_CC_SURROUND3_1
#define PAF_CC_SURROUND4 PAF_CC_SURROUND4_1

#endif  /* PAFCC_ */
