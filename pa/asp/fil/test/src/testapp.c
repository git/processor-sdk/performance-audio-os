/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/**
 *  @file   testapp.c
 *
 *  @brief  Filter test application main code.
 *
 *          This is a filter Layer 2 test application.
 *          The application test the various implementations of filters.
 *
 *          It expects the input from a wave file and generate the output
 *          wave file after filter operation.
 *
 *          The filter coefficients and implementation parameters are recieved
 *          from external tables. The test application will configure the neccessary
 *          parameters and allocate memory for filter input/output buffer and filter
 *          memory states.
 *
 *****************************************************************************/


#include <string.h>
#include <stdio.h>
#include <math.h>

#include "testapp.h"
#include "util_file_io.h"
#include "util_c66x_cache.h"


// Defines
#define AUDIO_SAMPLES 256
#define MAX_STATES    10
#define MAX_CHANNEL   10
#define MAX_TAPS      4
#define IS_DECIMAL(x)     ((((Int)(x*100000)) % 100000) == 0)

// Extern filter params table
extern pTestTable testapp_filterTable[];
extern char (*testName[])[];


// Extern Filter coefficient table
extern const Float **iir_coeff[];
extern const Float **fir_coeff[];
extern const double **iir_mx_coeff[];
extern const Float **iir_cas_coeff[];
// Extern gains for no-gain cascaded filters
extern const Float iir_cas_2tap_c4_ng_48_gain;
extern const Float iir_cas_2tap_c5_ng_48_gain;


// Filter states array
Float *filterState[MAX_STATES];
double *iir_mx_filterState[MAX_CHANNEL];


// Debug data capture can be enabled by below macro
//#define DATA_CAPTURE
#ifdef DATA_CAPTURE
#define FRAME_CAP_NUM 30
Float channel_zero[FRAME_CAP_NUM*AUDIO_SAMPLES];
Int frame_count = 0;
#endif


// To profile the filter fucntions
#define PROFILER

#ifdef PROFILER
#include "util_profiling.h"
#endif


/**
 *  @brief     Calculate filter state memory requirements per channel
 *
 *  @param[in] taps         Number of taps in filter
 *
 *  @param[in] casStages    Cascade stateg in filter
 *
 *  @param[in] filterType   Filter type, one of TESTAPP_FILTER_INDEX
 *
 *  @retval    Number of bytes required per channel for filter memory states
 *
 */
Int FIL_varBytesPerCh(Int taps, Int casStages, Int filterType);



/**
 *  @brief     Main
 *
 */
void main(void)
{
    Int error = 0;
    Int count;
    Int testCount;
    Int chIndex, filterType;
    Int chStateSize_bytes;
    Int ch, taps, inplace, casStages;
    char outFileName[100];
    char inFileName[100] = "test_vectors/input/input.wav";
    char *pTestName = NULL;
    unsigned char *filter_state = NULL;
    Float *pIn[MAX_CHANNEL], *pOut[MAX_CHANNEL];

    PAF_FilParam pParam;
    tTestTable *pTestTable;
    PAF_FilParam_use *fp_use;
    tFIOHandle *pFIO = (tFIOHandle*)calloc(1, sizeof(tFIOHandle));

#ifdef PROFILER
    tPrfl profiler;
#endif
     memarchcfg_cacheEnable();

    // test all filter function for different filter design
    for (filterType = TESTAPP_IIR_FILTER; filterType < TESTAPP_FILTER_COUNT; filterType++)
    {
        // get the filter param table for a perticular filter type
        pTestTable = testapp_filterTable[filterType];

        for (testCount = 0; pTestTable[testCount].filterFn; testCount++) //
        {
            // Get a local copy of the filter params
            ch        = pTestTable[testCount].ch;
            taps      = pTestTable[testCount].taps;
            inplace   = pTestTable[testCount].inplace;
            casStages = pTestTable[testCount].cascade;

            casStages = casStages == 0 ? 1 : casStages;    // for ease of multiplication

            // get the test name
            pTestName = &((*testName[filterType])[testCount*MAX_FILE_NAME_SIZE]);
            sprintf(outFileName, "test_vectors/output/%s.wav", pTestName);

            printf("Test %s: ", pTestName);

            // Initialize and configure the audio file module as per test specification
            error = FIO_Init(pFIO, inFileName, outFileName);
            error |= FIO_Config(pFIO, ch, AUDIO_SAMPLES);

            if (error != FIO_SUCCESS)
                return;

            // allocate memory for filter states
            chStateSize_bytes = FIL_varBytesPerCh(taps, casStages, filterType);
            filter_state = (unsigned char*) malloc(chStateSize_bytes * ch);



            // get data pointers and filter states
            for (chIndex = 0; chIndex < ch; chIndex++)
            {
                pIn[chIndex]  = &pFIO->inBuf[chIndex * AUDIO_SAMPLES];
                pOut[chIndex] = &pFIO->outBuf[chIndex * AUDIO_SAMPLES];

                // assign & clear filter states
                if (filterType != TESTAPP_IIR_MX_FILTER)
                {
                    filterState[chIndex] = (Float*) &filter_state[chIndex * chStateSize_bytes];
                    memset(filterState[chIndex], 0, chStateSize_bytes);
                }
                else  // for TESTAPP_IIR_MX_FILTER
                {
                    iir_mx_filterState[chIndex] = (double*) &filter_state[chIndex * chStateSize_bytes];
                    memset(iir_mx_filterState[chIndex], 0, chStateSize_bytes);
                }
            }

            // Initialize FIL Param Structure
            switch (filterType)
            {
                case TESTAPP_IIR_FILTER:
                    pParam.pCoef = (void **)iir_coeff[testCount];
                    pParam.pVar  = (void **)filterState;
                    pParam.use = taps;         // Initialize number of taps for N tap filters
                    break;
                case TESTAPP_FIR_FILTER:
                    pParam.pCoef = (void **)fir_coeff[testCount];
                    pParam.pVar  = (void **)filterState;
                    fp_use = (PAF_FilParam_use *)&(pParam.use);
                    fp_use->taps = taps;         // Initialize number of taps
                    fp_use->cIdx = 0;            // Circular buffer Indexing
                    break;
                case TESTAPP_IIR_MX_FILTER:
                    pParam.pCoef = (void **)iir_mx_coeff[testCount];
                    pParam.pVar  = (void **)iir_mx_filterState;
                    pParam.use = 0;              // not used presently
                    break;
                case TESTAPP_IIR_CAS_FILTER:
                    pParam.pCoef = (void **)iir_cas_coeff[testCount];
                    pParam.pVar  = (void **)filterState;
                    pParam.use   = pTestTable[testCount].cascade * 2;       // Set number of cascades for N cascaded filter implementation
                    break;
                default:
                    break;
            }

            // set data pointers
            pParam.pIn   = (void **)pIn;
            pParam.pOut  = (void **)pOut;

            // set channel and samples to process
            pParam.channels = ch;
            pParam.sampleCount = AUDIO_SAMPLES;


            count = 0;
#ifdef PROFILER
            // Init and clear profiling variables
            Prfl_Init(&profiler);
#endif
            // process the input data in chucks
            while (FIO_Read(pFIO) == FIO_SUCCESS)
            {
                Int i;
#ifdef PROFILER
                Prfl_Start(&profiler);
#endif

                // Special test case where external gain are applied for no-gain filter
                // Refer the filter param and coefficient table for more detail
                if ((filterType == TESTAPP_IIR_CAS_FILTER) && ((testCount == 3) || (testCount == 8)))
                {                                  // for test 3                // for test 8
                    Float gain = (testCount == 3 ? iir_cas_2tap_c4_ng_48_gain : iir_cas_2tap_c5_ng_48_gain);
                    for (i = 0;  i < ch*AUDIO_SAMPLES; i++)
                        pFIO->inBuf[i] = pFIO->inBuf[i] * gain;
                }

                // Perform filter operation
                pTestTable[testCount].filterFn(&pParam);

#ifdef DATA_CAPTURE
                if (frame_count < FRAME_CAP_NUM)
                {
                    memcpy(&channel_zero[frame_count*AUDIO_SAMPLES], &pFIO->outBuf[0], sizeof(Float) * AUDIO_SAMPLES);
                    frame_count++;
                }
#endif

#ifdef PROFILER
                Prfl_Stop(&profiler);
#endif

                // for filters which execute inplace
                // copy the processed output in output buffer
                if (inplace)
                {
                    for (i = 0;  i < ch*AUDIO_SAMPLES; i++)
                        pFIO->outBuf[i] = pFIO->inBuf[i];
                }

                // Write the processed output in output file
                FIO_Write(pFIO);

                // increament process count
                count++;
            }
            printf("%d: Number of frames in the input wave file are %d\n", testCount, count);
#ifdef PROFILER
            printf("Total Process Cycles %llu, Peak Process Cycles %llu Process Blocks size %d Process Count %u Input sample rate %d \n****************\n",
                    profiler.total_process_cycles, profiler.peak_process_cycles, AUDIO_SAMPLES, profiler.process_count, pFIO->inFileInfo.sample_rate);
            {   // write porfiling info in file
                FILE *pStdout_profile = NULL;
                pStdout_profile = fopen("fil_profile.txt", "a");
                fprintf(pStdout_profile, "Filter function %s: Total Process Cycles %llu, Peak Process Cycles %llu Process Blocks size %d Process Count %u Input sample rate %d",
                        pTestName, profiler.total_process_cycles, profiler.peak_process_cycles, AUDIO_SAMPLES, profiler.process_count, pFIO->inFileInfo.sample_rate);
                fprintf(pStdout_profile, "\n****************\n");
                fclose(pStdout_profile);
            }

#endif
            // Clear and deInit call
            free(filter_state);
            FIO_DeInit(pFIO);

        }
    }
    free(pFIO);

     memarchcfg_cacheFlush()
}


/**
 *  @brief     Calculate filter state memory requirements per channel
 *
 */
Int FIL_varBytesPerCh(Int taps, Int casStages, Int filterType)
{
    Int chStateSize_bytes = 0;

    switch (filterType)
    {
        case TESTAPP_IIR_MX_FILTER:
            chStateSize_bytes = sizeof(double) * taps * casStages;
            break;

        case  TESTAPP_FIR_FILTER:
            {
                // Filters states is required to be ^2 by FIR filter for circular buffer
                Int memStates = 0;
                Int pow_var = 0;
                double d_2 = 2;
                double tap_log2;

                tap_log2 = log((double)taps)/log(d_2);

                if (IS_DECIMAL(tap_log2))  // power of 2 or not
                {
                    memStates = taps;
                }
                else
                {   // assign the next higher ^2 as number of memory states
                    pow_var = ((Int)(tap_log2) + 1);
                    memStates = (Int)pow(d_2, pow_var);
                }

                chStateSize_bytes = sizeof(Float)  * memStates * casStages;
            }
            break;

        default:    // other filters
            chStateSize_bytes = sizeof(Float)  * taps * casStages;
            break;
    }
    return chStateSize_bytes;
}
