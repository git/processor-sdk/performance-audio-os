/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  
/*
 *  ======== iir2tap2ch_cN_df2.c ========
 *  IIR filter implementation(DF2) for tap-2,channels-2, cascade-N and SP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 2, CHANNELS 2 & CASCADE N **************************/
/*                                                                           */
/* Filter equation  : H(z) =  1  + b1*z~1 + b2*z~2                           */
/*                           ----------------------                          */
/*                            1  - a1*z~1 - a2*z~2                           */
/*                                                                           */
/*   Direct form    : y(n) = x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2)    */
/*                                                                           */
/*   Canonical form : w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                    */
/*                    y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                    */
/*                                                                           */
/*                                                                           */
/*          g                   w(n)                                         */
/*     x(n)-->---[+]---->--------@------->-----[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+    b1     |                            */
/*               [+]----<<----| Z~1 |---->>----[+]                           */
/*                |           +-----+           |                            */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a2    +-----+    b2     |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"
#include "fil_table.h"

/*
 *  ======== Filter_iirT2Ch2_cN_df2() ========
 *  IIR filter, taps-2, channels-2, cascade-N and in SP 
 */
/* Memory section for the function code */
#pragma CODE_SECTION(Filter_iirT2Ch2_cN_df2, ".text:Filter_iirT2Ch2_cN_df2")
Int Filter_iirT2Ch2_cN_df2( PAF_FilParam *pParam ) 
{
    Void ** pIn, ** pOut, ** pCoef, ** pVar; /* Handle parameters */
    Int i; /* Channel counter */
    Int channels, error = 0;
    Uint uniCoef=0, inplace = 0;
    Int casc;
    
    /* Get handle parameters into temp local variables */
    pIn      = pParam->pIn;
    pOut     = pParam->pOut;
    pCoef    = pParam->pCoef;
    pVar     = pParam->pVar;
    channels = 2;
    casc     = pParam->use >> 1;

    /* Check whether uniCoeff */
    
    for( i = 0; i < channels; i++ ){
      inplace |= ( ( Uint )pParam->pIn[i]   ^  ( Uint )pParam->pOut[i]  );    
      uniCoef |= ( ( Uint )pParam->pCoef[0] ^  ( Uint )pParam->pCoef[i] );
    }

    /* Finish up the i_2Casc channels, if casc = 2 */
    if( (!inplace) && (casc == 2) )
        /* Call i_2casc with proper channel offsets */
            error += Filter_iirT2Ch2_c2_i_df2(pParam);

    /* Finish up the 3Casc channels, if casc = 3 */
    else if( (!uniCoef) && (casc == 3) )
            error += Filter_iirT2Ch2_c3_u_df2(pParam);
            
    else if( (!uniCoef) && (casc == 4) )
    		error += Filter_iirT2Ch2_c4_u_df2(pParam);        
    
    else if( (!uniCoef) && (casc == 5) )
    		error += Filter_iirT2Ch2_c5_u_df2(pParam);
    
    else{

    			/* Call 1ch_Ncasc with proper offset for channels */        
    		for(i = 0; i < channels; i++){
    			Filter_iirT2Ch1_cN_df2(pParam);
        		FIL_offsetParam( pParam, 1 );
    		}
    }		
        
    /* Put back the handle elements */
    pParam->pIn   = pIn;
    pParam->pOut  = pOut;
    pParam->pCoef = pCoef;
    pParam->pVar  = pVar;  
    
    return(error);     
} /* Int Filter_iirT2Ch2_cN_df2() */
