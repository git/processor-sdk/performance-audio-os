/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/*
 *  ======== iir2tap2ch_mx.c ========
 *  IIR filter implementation for tap-2 & channels-2, SP-DP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 2    and CHANNELS 2 ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2                           */
/*                           ----------------------                          */
/*                            1  - a1*z~1 - a2*z~2                           */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2) */
/*                                                                           */
/*   Implementation : w(n) = b0*x(n) + a1*w(n-1) + a2*w(n-2)                 */
/*                    y(n) = w(n) + b1/b0*w(n-1) + b2/b0*w(n-2)              */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      w(n) -             w(n-1) - w1         w(n-2) - w2                   */
/*      b0 - filtCfsB[0], b1/b0 - filtCfsB[1], b2/b0 - filtCfsB[2]           */
/*      a1 - filtCfsA[0], a2 - filtCfsA[1]                                   */
/*                                                                           */
/*                                                                           */
/*           b0                 w(n)                                         */
/*     x(n)-->>--[+]---->--------@------->-----[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+  b1/b0    |                            */
/*               [+]----<<----| Z~1 |---->>----[+]                           */
/*                |           +-----+           |                            */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a2    +-----+  b2/b0    |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"
#include "fil_table.h"

/*
 *  ======== Filter_iirT2Ch2_mx() ========
 *  IIR filter, taps-2 & channels-2, SP-DP 
 */
/* Memory section for the function code */
Int Filter_iirT2Ch2_mx( PAF_FilParam *pParam )
{
    Int error = 0;
    Void ** pIn, ** pOut, ** pCoef, ** pVar; /* Handle parameters */
    
    /* Get handle parameters into temp local variables */
    pIn   = pParam->pIn;
    pOut  = pParam->pOut;
    pCoef = pParam->pCoef;
    pVar  = pParam->pVar;

    /* Unicoeff/Inplace */
    if( (pParam->pIn[0] == pParam->pOut[0])    &&   (pParam->pCoef[0] == pParam->pCoef[1]) )
    {
        /* b2 = 1.0 */
        if( (((double *)pParam->pCoef[0])[2] == 1.0 ) && ( ((double *)pParam->pCoef[0])[0] == 1.0 ) )
        {
            if( ((double *)pParam->pCoef[0])[1] == 2.0 )       /* b1 = 2.0  */
                return( Filter_iirT2Ch2_ui_mx_BM1( pParam ) );
            else if( ((double *)pParam->pCoef[0])[1] == -2.0 ) /* b1 = -2.0 */
                return( Filter_iirT2Ch2_ui_mx_BM2( pParam ) ); 
        }
            
        return( Filter_iirT2Ch2_ui_mx( pParam ) );
    }
        
    /* Unicoeff */
    else if ( pParam->pCoef[0] == pParam->pCoef[1] )
        return( Filter_iirT2Ch2_u_mx( pParam ) );
    
    /* General, as 1-1 chs */    
    else 
    {
        error += Filter_iirT2Ch1_mx( pParam );
        FIL_offsetParam( pParam, 1 );
        error += Filter_iirT2Ch1_mx( pParam );
    }

    /* Put back the handle elements */
    pParam->pIn   = pIn;
    pParam->pOut  = pOut;
    pParam->pCoef = pCoef;
    pParam->pVar  = pVar;
    
    return( error );        
        
} /* Int Filter_iirT2Ch2_mx */

