#include "memoryFunctions.h"

unsigned int nFileSize = 0;
unsigned int pAddrReadIndex = 0;
unsigned int pADDRReadMaxIndex = 0;
/* Base address of memory read pointer */
unsigned char *ADDRREAD = (unsigned char *)0xB0000000;

/* Initialization required to use memory read modules */
void mReadInit(FILE* fp)
{
    pAddrReadIndex = 0;
    fseek(fp, 0L, SEEK_END);
    nFileSize = ftell(fp);
    fseek(fp, 0L, SEEK_SET); 
}

/* Equivalent to  C library function fread */
/* Instead of Reading from a file read from predefined memeory address */
size_t mRead(void *ptr, size_t size, size_t count,FILE* fp)
{
    if(!nFileSize)
    {
        printf("Memory Initialization not performed\n");
        return 0;
    }
    
    if(pAddrReadIndex < nFileSize)
    {
        unsigned int nReadCount = size*count;
        if((pAddrReadIndex + nReadCount) > nFileSize)
        {
            nReadCount = (nFileSize - pAddrReadIndex);
        }

        memcpy(ptr, ADDRREAD+pAddrReadIndex, nReadCount);
        pAddrReadIndex += nReadCount;
        if(pADDRReadMaxIndex < pAddrReadIndex)
        {
            pADDRReadMaxIndex = pAddrReadIndex;
        }
        return (nReadCount / size);
    }
    else
    {
         return 0;
    }
}
/* Equivalent to  C library function fseek */
/* Align memory address index */
size_t mReadSeek(FILE* fp,  long offset, int whence)
{
        switch(whence)
        {
        case SEEK_SET:
            pAddrReadIndex = 0;
            pAddrReadIndex += offset;
            if(pADDRReadMaxIndex < pAddrReadIndex)
            {
                pADDRReadMaxIndex = pAddrReadIndex;
            }
            break;
        case SEEK_CUR:
            pAddrReadIndex += offset;
            if(pADDRReadMaxIndex < pAddrReadIndex)
            {
                pADDRReadMaxIndex = pAddrReadIndex;
            }
            break;
        case SEEK_END:
            pAddrReadIndex = pADDRReadMaxIndex;
            pAddrReadIndex += offset;
            if(pADDRReadMaxIndex < pAddrReadIndex)
            {
                pADDRReadMaxIndex = pAddrReadIndex;
            }
            break;
        default: break;
        }
        return 0;
}

/* Equivalent to  C library function fgetpos */
/* Insted of reading FIlE Pointer position reads memory address index*/
int mReadGetPos(FILE *fp, fpos_t *pos)
{
    *pos = pAddrReadIndex;
    return 0;
}

/* Equivalent to  C library function fsetpos */
/* Insted of aligning FIlE Pointer function align memory index */
int mReadSetPos(FILE *fp, const fpos_t *pos)
{
    pAddrReadIndex = *pos;
    if(pADDRReadMaxIndex < pAddrReadIndex)
    {
        pADDRReadMaxIndex = pAddrReadIndex;
    }
    return 0;
}
