/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/*
 *  ======== firntapnch.c ========
 *  FIR filter implementation for tap-N & channels-N.
 */

/************************ FIR FILTER *****************************************/
/****************** ORDER N    and CHANNELS N ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2 + b3*z~3 + - - - -        */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n) + b1*x(n-1) + - - +                     */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      b0 - filtCfsB[0], bk - filtCfsB[k]                                   */
/*                                                                           */
/*                                                                           */
/*                        b0                                                 */
/*     x(n)-->----@------->>----[+]--->--y(n)                                */
/*                |              |                                           */
/*                v              ^                                           */
/*             +-----+    b1     |                                           */
/*             | Z~1 |---->>----[+]                                          */
/*             +-----+           |                                           */
/*                |              |                                           */
/*                v              ^                                           */
/*             +-----+    b2     |                                           */
/*             | Z~1 |---->>----[+]                                          */
/*             +-----+           |                                           */
/*                |                                                          */
/*                v              ^                                           */
/*                v              ^                                           */
/*                v              ^                                           */
/*                |                                                          */
/*             +-----+    bk     |                                           */
/*             | Z~1 |---->>-----                                            */
/*             +-----+                                                       */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include <filters.h>
#include "fil_table.h"

/*
 *  ======== Filter_firTNChN() ========
 *  FIR filter, taps-N & channels-N 
 */
/* Memory section for the function code */
Int Filter_firTNChN( PAF_FilParam *pParam ) 
{
    Int i, error = 0;
    Void ** pIn, ** pOut, ** pCoef, ** pVar; /* Filter ptrs */
    /* Additional FIR param structure, pointed by use */
    PAF_FilParamFir *fir_param = (PAF_FilParamFir *)(&pParam->use);     
    Short cIdxStart = fir_param->cIdx; /* Circular index */
    
    /* Get the parameters into safe local variables */
    pIn   = pParam->pIn;
    pOut  = pParam->pOut;
    pCoef = pParam->pCoef;
    pVar  = pParam->pVar;        
    
    /* N channel FIR is implemented by calling 1ch FIR N times */
    for( i = 0; i < pParam->channels; i++ )
    {
            fir_param->cIdx = cIdxStart; /* Update the circular index */
            error += Filter_firTNCh1( pParam ); /* 1ch FIR */
            FIL_offsetParam( pParam, 1 ); /* Offset the layer2 ptrs by 1 */
    }
    
    /* Put back original ptr values */
    pParam->pIn   = pIn;
    pParam->pOut  = pOut;
    pParam->pCoef = pCoef;
    pParam->pVar  = pVar;      
    
    return(FIL_SUCCESS);
} /* Int Filter_firTNChN() */
