
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Output Buffer Declarations
//
//
//

#ifndef OUTBUF_
#define OUTBUF_

#include <xdc/std.h>
#include <ti/xdais/xdas.h>
#include <paftyp.h>
#include <pafdec.h>

typedef volatile struct PAF_OutBufStatus {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 sioSelect;
    XDAS_Int8 sampleRate;
    XDAS_Int8 audio;
    XDAS_Int8 clock;
    XDAS_Int8 flush;
    XDAS_Int8 rateTrackMode;
    XDAS_Int8 markerMode;

#ifdef HSE
// DFO entries 
    XDAS_Int16 fileSelect;
    XDAS_Int16 mediaSelect;
	XDAS_Int8  pauseReq;
	XDAS_Int8  unused; 
    XDAS_Int16 dirSelect;
    XDAS_Int8  unused2[2];
#endif //HSE

    XDAS_UInt8 maxNumBufOverride;
    XDAS_UInt8 numBufOverride[PAF_SOURCE_N];

} PAF_OutBufStatus;

#define PAF_OB_AUDIO_QUIET      0x00
#define PAF_OB_AUDIO_SOUND      0x01
#define PAF_OB_AUDIO_FLUSH      0x02
#define PAF_OB_AUDIO_MUTED      0x10

#define PAF_OB_CLOCK_EXTERNAL   0
#define PAF_OB_CLOCK_INTERNAL   1

#define PAF_OB_FLUSH_DISABLE    0
#define PAF_OB_FLUSH_ENABLE     1

#define PAF_OB_MARKER_DISABLED 0
#define PAF_OB_MARKER_ENABLED  1

typedef struct PAF_OutBufConfig {
    PAF_UnionPointer base;
    PAF_UnionPointer pntr;
    PAF_UnionPointer head;
    XDAS_Int32 sizeofBuffer;
    XDAS_Int8 sizeofElement;
    XDAS_Int8 precision;
    XDAS_Int8 stride;
    XDAS_Int8 unused;
    XDAS_Int32 lengthofFrame;
    XDAS_Int32 lengthofData;
    XDAS_Int16 putCount;
    XDAS_Int16 putStride;
    XDAS_Int32 allocation;
} PAF_OutBufConfig;

#endif  /* OUTBUF_ */
