
/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Vendor-specific (TII) PCM Encoder algoritm interface declarations
//
//
//

/*
 *  Vendor specific (TII) interface header for PCM Encoder algoritm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular PCE implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef PCE_TII_
#define PCE_TII_

#include <ti/xdais/ialg.h> //<ialg.h>

#include <ipce.h>
#include <icom.h>
#include <com_tii_priv.h>

/*
 *  ======== PCE_TII_exit ========
 *  Required module finalization function
 */
#define PCE_TII_exit COM_TII_exit

/*
 *  ======== PCE_TII_init ========
 *  Required module initialization function
 */
#define PCE_TII_init COM_TII_init

/*
 *  ======== PCE_TII_IALG ========
 *  TII's implementation of PCE's IALG interface
 */
extern IALG_Fxns PCE_TII_IALG; 

/*
 *  ======== PCE_TII_IPCE ========
 *  TII's implementation of PCE's IPCE interface
 */
extern const IPCE_Fxns PCE_TII_IPCE; 


/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of TII's PCM Encoder algoritm.
 *  However, other implementation specific operations can also
 *  be added.
 */

/*
 *  ======== PCE_TII_Handle ========
 */
typedef struct PCE_TII_Obj *PCE_TII_Handle;

/*
 *  ======== PCE_TII_Params ========
 *  We don't add any new parameters to the standard ones defined by IPCE.
 */
typedef IPCE_Params PCE_TII_Params;

/*
 *  ======== PCE_TII_Status ========
 *  We don't add any new status to the standard one defined by IPCE.
 */
typedef IPCE_Status PCE_TII_Status;

/*
 *  ======== PCE_TII_Config ========
 *  We don't add any new config to the standard one defined by IPCE.
 */
typedef IPCE_Config PCE_TII_Config;

/*
 *  ======== PCE_TII_Active ========
 *  We don't add any new active to the standard one defined by IPCE.
 */
typedef IPCE_Active PCE_TII_Active;

/*
 *  ======== PCE_TII_Scrach ========
 *  We don't add any new scrach to the standard one defined by IPCE.
 */
typedef IPCE_Scrach PCE_TII_Scrach;

/*
 *  ======== PCE_TII_PARAMS ========
 *  Define our default parameters.
 */
#define PCE_TII_PARAMS   IPCE_PARAMS

/*
 *  ======== PCE_TII_create ========
 *  Create a PCE_TII instance object.
 */
extern PCE_TII_Handle PCE_TII_create(const PCE_TII_Params *params);

/*
 *  ======== PCE_TII_delete ========
 *  Delete a PCE_TII instance object.
 */
#define PCE_TII_delete COM_TII_delete

#endif  /* PCE_TII_ */
