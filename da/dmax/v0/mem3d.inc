/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#ifndef MEM3D_
#define MEM3D_

/* MEM3d Event Entry constants */
#define MEM3D_ETYPE_MSK  0x1F    // ETYPE Mask
#define MEM3D_ETYPE_SHFT 0       // ETYPE Shift

#define MEM3D_PP_P1_MSK  0x7     // PP Part1 Mask
#define MEM3D_PP_P1_SHFT 5       // PP Part1 Shift 
#define MEM3D_PP_P2_MSK  1       // PP Part2 Mask
#define MEM3D_PP_P2_SHFT 21      // PP Part2 Shift 
#define MEM3D_PP_P3_MSK  1       // PP Part3 Mask
#define MEM3D_PP_P3_SHFT 28      // PP Part3 Shift 

#define MEM3D_PTE_MSK    0x7F    // PTE Mask
#define MEM3D_PTE_SHFT   8       // PTE Shift

#define MEM3D_ESIZE8     0       // ESIZE8
#define MEM3D_ESIZE16    1       // ESIZE16
#define MEM3D_ESIZE32    2       // ESIZE32
#define MEM3D_ESIZE_MSK  3       // ESIZE Mask
#define MEM3D_ESIZE_SHFT 16      // ESIZE Shift

#define MEM3D_CC0        0       // CC0 
#define MEM3D_CC1        1       // CC1
#define MEM3D_CC2        2       // CC2 
#define MEM3D_CC3        3       // CC3 
#define MEM3D_CC_MSK     3       // CC3 
#define MEM3D_CC_SHFT    18      // CC3 

#define MEM3D_RLOAD_DIS  0       // RLOAD disabled 
#define MEM3D_RLOAD_ENA  1       // RLOAD enabled 
#define MEM3D_RLOAD_MSK  1       // RLOAD Mask 
#define MEM3D_RLOAD_SHFT 20      // RLOAD Shift 

#define MEM3D_TCINT_DIS  0       // TCINT disabled
#define MEM3D_TCINT_ENA  1       // TCINT enabled
#define MEM3D_TCINT_MSK  1       // TCINT Mask 
#define MEM3D_TCINT_SHFT 22      // TCINT Shift 

#define MEM3D_ATCINT_DIS 0       // ATCINT disabled
#define MEM3D_ATCINT_ENA 1       // ATCINT enabled
#define MEM3D_ATCINT_MSK 1       // ATCINT Mask 
#define MEM3D_ATCINT_SHFT 23     // ATCINT Shift 

#define MEM3D_TCC_MSK    0x0F    // TCC Mask
#define MEM3D_TCC_SHFT   24      // TCC Shift

#define MEM3D_SYNC_DIS   0       // Sync disabled
#define MEM3D_SYNC_ENA   1       // Sync enabled
#define MEM3D_SYNC_MSK   1       // Sync Mask 
#define MEM3D_SYNC_SHFT  29      // Sync Shift 

#define MEM3D_QTSL0      0       // QTSL0 - 1 element
#define MEM3D_QTSL1      1       // QTSL1 - 4 elements
#define MEM3D_QTSL2      2       // QTSL2 - 8 elements
#define MEM3D_QTSL3      3       // QTSL3 - 16 elements
#define MEM3D_QTSL_MSK   3       // QTSL Mask
#define MEM3D_QTSL_SHFT  30      // QTSL Shift

/* MEM3D Parameter Table constants */
#define MEM3D_PP0        0       // PP0 - Active Parameter 0
#define MEM3D_PP1        1       // PP0 - Active Parameter 1
#define MEM3D_PP1_MSK    1       // PP Mask 
#define MEM3D_PP1_SHFT   31      // PP Shift 

// MEM3D parameter field lengths and offsets
#define MEM3D_SRC_OFF    0       // MEM3D src offset
#define MEM3D_SRC_LEN    4       // MEM3D src length 
#define MEM3D_DST_OFF    4       // MEM3D dst offset
#define MEM3D_DST_LEN    4       // MEM3D dst length
#define MEM3D_CNT_OFF    8       // MEM3D cnt offset
#define MEM3D_CNT_LEN    4       // MEM3D cnt length
#define MEM3D_SRC_IDX0_OFF 12    // MEM3D src_idx0 offset
#define MEM3D_SRC_IDX0_LEN 2     // MEM3D src_idx0 length
#define MEM3D_DST_IDX0_OFF 14    // MEM3D dst_idx0 offset
#define MEM3D_DST_IDX0_LEN 2     // MEM3D dst_idx0 length
#define MEM3D_SRC_IDX1_OFF 16    // MEM3D src_idx1 offset
#define MEM3D_SRC_IDX1_LEN 2     // MEM3D src_idx1 length
#define MEM3D_DST_IDX1_OFF 18    // MEM3D dst_idx1 offset
#define MEM3D_DST_IDX1_LEN 2     // MEM3D dst_idx1 length
#define MEM3D_SRC_IDX2_OFF 20    // MEM3D src_idx2 offset
#define MEM3D_SRC_IDX2_LEN 2     // MEM3D src_idx2 length
#define MEM3D_DST_IDX2_OFF 22    // MEM3D dst_idx2 offset
#define MEM3D_DST_IDX2_LEN 2     // MEM3D dst_idx2 length
#define MEM3D_CNT_REF_OFF 24     // MEM3D cnt_ref offset
#define MEM3D_CNT_REF_LEN 4      // MEM3D cnt_ref length
#define MEM3D_SRC_RLD0_OFF 28    // MEM3D src_rld0 offset
#define MEM3D_SRC_RLD0_LEN 4     // MEM3D src_rld0 length 
#define MEM3D_DST_RLD0_OFF 32    // MEM3D dst_rld0 offset
#define MEM3D_DST_RLD0_LEN 4     // MEM3D dst_rld0 length 
#define MEM3D_SRC_RLD1_OFF 36    // MEM3D src_rld1 offset
#define MEM3D_SRC_RLD1_LEN 4     // MEM3D src_rld1 length 
#define MEM3D_DST_RLD1_OFF 40    // MEM3D dst_rld1 offset
#define MEM3D_DST_RLD1_LEN 4     // MEM3D dst_rld1 length 

#endif // MEM1D_
