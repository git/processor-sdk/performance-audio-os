/******************************************************************************
 * Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

//
//
// Vendor-specific (TII) Downmix algorithm
// interface declarations
//
//
//
/*
 *  Vendor specific (TII) interface header for
 *  Downmix algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular DM implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef DM_TII_
#define DM_TII_

#include <ti/xdais/ialg.h>

#include <idm.h>
#include <icom.h>

/*
 *  ======== DM_TII_exit ========
 *  Required module finalization function
 */
#define DM_TII_exit COM_TII_exit

/*
 *  ======== DM_TII_init ========
 *  Required module initialization function
 */
#define DM_TII_init COM_TII_init

/*
 *  ======== DM_TII_IALG ========
 *  TII's implementation of DM's IALG interface
 */
extern IALG_Fxns DM_TII_IALG;

/*
 *  ======== DM_TII_IDM ========
 *  TII's implementation of DM's IDM interface
 */
extern const IDM_Fxns DM_TII_IDM;

/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of TII's
 *  Downmix algorithm. However, other
 *  implementation specific operations can also be added.
 */

/*
 *  ======== DM_TII_Handle ========
 */
typedef struct DM_TII_Obj *DM_TII_Handle;

/*
 *  ======== DM_TII_Params ========
 *  We don't add any new parameters to the standard ones defined by IDM.
 */
typedef IDM_Params DM_TII_Params;

/*
 *  ======== DM_TII_Status ========
 *  We don't add any new status to the standard one defined by IDM.
 */
typedef IDM_Status DM_TII_Status;

/*
 *  ======== DM_TII_Config ========
 *  We don't add any new config to the standard one defined by IDM.
 */
typedef IDM_Config DM_TII_Config;

/*
 *  ======== DM_TII_PARAMS ========
 *  Define our default parameters.
 */
#define DM_TII_PARAMS   IDM_PARAMS

/*
 *  ======== DM_TII_create ========
 *  Create a DM_TII instance object.
 */
extern DM_TII_Handle DM_TII_create(const DM_TII_Params *params);

/*
 *  ======== DM_TII_delete ========
 *  Delete a DM_TII instance object.
 */
#define DM_TII_delete COM_TII_delete

#endif  /* DM_TII_ */
