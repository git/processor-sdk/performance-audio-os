
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Initial version obtained from ver 1.10 of i12/evmda830/atboot.c
//

#include <acptype.h>
#include <pafaip_a.h>

#include "alpha\pa_y15_evmda830_io_a.h"

#define CUS_ATBOOT_S \
	writeDECChannelMapTo16  (PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_CNTR,PAF_SUBW,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LHED,PAF_RHED,-3,-3,-3,-3), \
	writeENCChannelMapFrom16(PAF_LEFT,PAF_RGHT,PAF_LSUR,PAF_RSUR,PAF_CNTR,PAF_SUBW,PAF_LBAK,PAF_RBAK,PAF_LWID,PAF_RWID,PAF_LHED,PAF_RHED,-3,-3,-3,-3), \
    0xcd09,0x0401, \
    writeENCChannelMapFrom16(PAF_LEFT,PAF_RGHT,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
     0xcd09,0x0400, \
    execPAYInAnalog, \
    execPAYOutPrimaryAnalog, \
    execPAYOutSecondaryNone
   

#pragma DATA_SECTION(cus_atboot_s0_patch, ".none")
const ACP_Unit cus_atboot_s0_patch[] = {
    0xc900 + 0 - 1,
    CUS_ATBOOT_S,
};

const ACP_Unit cus_atboot_s_patch[] = {
    0xc900 + sizeof(cus_atboot_s0_patch)/2 - 1,
    CUS_ATBOOT_S,
};

const ACP_Unit cus_atboot_s[] = {
    0xc900 + sizeof(cus_atboot_s0_patch)/2 - 1,
    CUS_ATBOOT_S,
};

const ACP_Unit cus_attime_s[] = {
    0xc900,
};
// EOF
