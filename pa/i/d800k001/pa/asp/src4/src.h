
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the Synchronous Rate Conversion Algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the SRC
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef SRC_
#define SRC_

#include <alg.h>
#include <ialg.h>
#include <paf_alg.h>

#include "isrc.h"
#include "paftyp.h"

/*
 *  ======== SRC_Handle ========
 *  Synchronous Rate Conversion Algorithm instance handle
 */
typedef struct ISRC_Obj *SRC_Handle;

/*
 *  ======== SRC_Params ========
 *  Synchronous Rate Conversion Algorithm instance creation parameters
 */
typedef struct ISRC_Params SRC_Params;

/*
 *  ======== SRC_PARAMS ========
 *  Default instance parameters
 */
#define SRC_PARAMS        ISRC_PARAMS
#define SRC_PARAMS_MAX192 ISRC_PARAMS_MAX192
#define SRC_PARAMS_MAX48  ISRC_PARAMS_MAX48
#define SRC_PARAMS_MIN32  ISRC_PARAMS_MIN32

/*
 *  ======== SRC_Status ========
 *  Status structure for getting SRC instance attributes
 */
typedef volatile struct ISRC_Status SRC_Status;

/*
 *  ======== SRC_Cmd ========
 *  This typedef defines the control commands SRC objects
 */
typedef ISRC_Cmd   SRC_Cmd;

/*
 * ===== control method commands =====
 */
#define SRC_NULL ISRC_NULL
#define SRC_GETSTATUSADDRESS1 ISRC_GETSTATUSADDRESS1
#define SRC_GETSTATUSADDRESS2 ISRC_GETSTATUSADDRESS2
#define SRC_GETSTATUS ISRC_GETSTATUS
#define SRC_SETSTATUS ISRC_SETSTATUS

/*
 *  ======== SRC_create ========
 *  Create an instance of a SRC object.
 */
static inline SRC_Handle SRC_create(const ISRC_Fxns *fxns, const SRC_Params *prms)
{
    return ((SRC_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== SRC_delete ========
 *  Delete a SRC instance object
 */
static inline Void SRC_delete(SRC_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== SRC_apply ========
 */
extern Int SRC_apply(SRC_Handle, PAF_AudioFrame *);

/*
 *  ======== SRC_reset ========
 */
extern Int SRC_reset(SRC_Handle, PAF_AudioFrame *);

/*
 *  ======== SRC_exit ========
 *  Module finalization
 */
extern Void SRC_exit(Void);

/*
 *  ======== SRC_init ========
 *  Module initialization
 */
extern Void SRC_init(Void);

#endif  /* SRC_ */
