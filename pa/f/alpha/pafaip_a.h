
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Alpha Interval Processing alpha codes
//
//
//

#ifndef _PAFAIP_A
#define _PAFAIP_A

#include <acpbeta.h>
#include <paftyp_a.h>

#define  readAIPMode 0xc200+STD_BETA_SYSINT,0x0400
#define  readAIPMask 0xc200+STD_BETA_SYSINT,0x0500
#define  readAIPTime 0xc300+STD_BETA_SYSINT,0x0006

#define writeAIPModeDisable 0xca00+STD_BETA_SYSINT,0x0400
#define writeAIPModeBootOnly 0xca00+STD_BETA_SYSINT,0x0401
#define writeAIPModeSuspend 0xca00+STD_BETA_SYSINT,0x0402
#define writeAIPModeEnable 0xca00+STD_BETA_SYSINT,0x0403

#define writeAIPMask(N) 0xca00+STD_BETA_SYSINT,0x0500+((N)&0xff)

#define writeAIPTime(N) 0xcb00+STD_BETA_SYSINT,0x0006,((N)&0xffff)
#define wroteAIPTime 0xcb00+STD_BETA_SYSINT,0x0006

#define  readAIPStatus 0xc508,STD_BETA_SYSINT
#define  readAIPControl \
         readAIPMode, \
         readAIPMask, \
         readAIPTime


#endif /* _PAFAIP_A */
