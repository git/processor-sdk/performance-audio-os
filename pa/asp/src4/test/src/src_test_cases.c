/*
*  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
*  ALL RIGHTS RESERVED 
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  @file  src_test_cases.c
 *
 *  @brief This file contains different test cases for SRC.
 *
 *
 **/

/************************************************************
                     Include files
*************************************************************/
#include "testapp.h"

/************************************************************
                  global variables
*************************************************************/
extern const FilterCoefs ISRC_cf_1to2;
extern const FilterCoefs ISRC_cf_2to4;
extern const FilterCoefs ISRC_cf_1toH;
extern const FilterCoefs ISRC_cf_HtoQ;

//Mono
 const ISRC_memRec  ISRC_MEMREC_UPSAMPLE1[3] = {
    {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +        // 0 - SRC ALG
     (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
    {sizeof(PAF_AudioData) * 1 * (19 + 1 + 131 + 1) / 2, IALG_SARAM, IALG_PERSIST}, // 1 - SRC FILTER STATE
    {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
     (IALG_MemAttrs)PAF_IALG_COMMONN(0)},                                           // 2 - SRC SCRATCH
};

 const ISRC_memRec  ISRC_MEMREC_DOWNSAMPLE1[3] = {
     {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +       // 0 = SRC ALG
      (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
     {sizeof(PAF_AudioData) * 1 * (24 + 128), IALG_SARAM, IALG_PERSIST},                // 1 - SRC FILTER STATE
     {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
      (IALG_MemAttrs)PAF_IALG_COMMONN(0)},                                          // 2 - SRC SCRATCH
 };

 //Stereo
  const ISRC_memRec ISRC_MEMREC_UPSAMPLE2[3] = {
     {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +       // 0 - SRC ALG
      (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
     {sizeof(PAF_AudioData) * 2 * (19 + 1 + 131 + 1) / 2, IALG_SARAM, IALG_PERSIST}, // 1 - SRC FILTER STATE
     {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
      (IALG_MemAttrs)PAF_IALG_COMMONN(0)},                                          // 2 - SRC SCRATCH
 };

  const ISRC_memRec ISRC_MEMREC_DOWNSAMPLE2[3] = {
      {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +      // 0 = SRC ALG
       (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
      {sizeof(PAF_AudioData) * 2 * (24 + 128), IALG_SARAM, IALG_PERSIST},           // 1 - SRC FILTER STATE
      {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
       (IALG_MemAttrs)PAF_IALG_COMMONN(0)},                                         // 2 - SRC SCRATCH
  };

  //5.1 channel
  const ISRC_memRec  ISRC_MEMREC_UPSAMPLE5_1[3] = {
     {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +        // 0 - SRC ALG
      (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
     {sizeof(PAF_AudioData) * 6 * (19 + 1 + 131 + 1) / 2, IALG_SARAM, IALG_PERSIST}, // 1 - SRC FILTER STATE
     {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
      (IALG_MemAttrs)PAF_IALG_COMMONN(0)},                                           // 2 - SRC SCRATCH
 };

  const ISRC_memRec  ISRC_MEMREC_DOWNSAMPLE5_1[3] = {
          {(sizeof(SRC_TIH_Obj) + 3) / 4 * 4 + (sizeof(ISRC_Config) + 3) / 4 * 4 +      // 0 = SRC ALG
           (sizeof(ISRC_Status) + 3) / 4 * 4, IALG_SARAM, IALG_PERSIST},
          {sizeof(PAF_AudioData) * 6 * (24 + 128), IALG_SARAM, IALG_PERSIST},           // 1 - SRC FILTER STATE
          {sizeof(PAF_AudioData) * (24 + 512 + 128 + 256), IALG_SARAM,
           (IALG_MemAttrs)PAF_IALG_COMMONN(0)},                                         // 2 - SRC SCRATCH
      };

 const ISRC_Status ISRC_PARAMS_STATUS_UPSAMPLE_2X = {
    sizeof(ISRC_Status),
    1,
    0x83,
    0x00,
    PAF_SAMPLERATE_UNKNOWN,
    NULL,
    NULL,
    &ISRC_cf_1to2,
    &ISRC_cf_2to4
};

const ISRC_Status ISRC_PARAMS_STATUS_UPSAMPLE_4X = {
    sizeof(ISRC_Status),
    1,
    0x84,
    0x00,
    PAF_SAMPLERATE_UNKNOWN,
    NULL,
    NULL,
    &ISRC_cf_1to2,
    &ISRC_cf_2to4
};

const ISRC_Status ISRC_PARAMS_STATUS_DOWNSAMPLE_2X = {
    sizeof(ISRC_Status),
    1,
    0x81,
    0x00,
    PAF_SAMPLERATE_UNKNOWN,
    &ISRC_cf_1toH,
    &ISRC_cf_HtoQ,
    NULL,
    NULL
};

const ISRC_Status ISRC_PARAMS_STATUS_DOWNSAMPLE_4X = {
    sizeof(ISRC_Status),
    1,
    0x82,
    0x00,
    PAF_SAMPLERATE_UNKNOWN,
    &ISRC_cf_1toH,
    &ISRC_cf_HtoQ,
    NULL,
    NULL
};

 // Test Case 1: For upsampling by 2X(double) (Ex: 48khz to 96khz)
 const ISRC_Params ISRC_PARAMS_UPSAMPLE_2X = {
    sizeof(ISRC_Params),
    &ISRC_PARAMS_STATUS_UPSAMPLE_2X,
    1,
    3,
    0,
    NULL,
    NULL,
    ISRC_MEMREC_UPSAMPLE1
};

// Test Case 2: For upsampling by 4X(quadruple) (Ex: 48khz to 96khz)
 const ISRC_Params ISRC_PARAMS_UPSAMPLE_4X = {
    sizeof(ISRC_Params),
    &ISRC_PARAMS_STATUS_UPSAMPLE_4X,
    1,
    3,
    0,
    NULL,
    NULL,
    ISRC_MEMREC_UPSAMPLE1
};

 // Test Case 3: For dowsampling by 2X(half) (Ex: 96khz to 48khz)
  const ISRC_Params ISRC_PARAMS_DOWNSAMPLE_2X = {
     sizeof(ISRC_Params),
     &ISRC_PARAMS_STATUS_DOWNSAMPLE_2X,
     1,
     3,
     0,
     NULL,
     NULL,
    ISRC_MEMREC_DOWNSAMPLE1
 };

 // Test Case 4: For dowsampling by 4X(quarter) (Ex: 192khz to 48khz)
  const ISRC_Params ISRC_PARAMS_DOWNSAMPLE_4X = {
     sizeof(ISRC_Params),
     &ISRC_PARAMS_STATUS_DOWNSAMPLE_4X,
     1,
     3,
     0,
     NULL,
     NULL,
     ISRC_MEMREC_DOWNSAMPLE1
 };

  // Test Case 5: For upsampling by 2X(double) for 2 channel input(Ex: 48khz to 96khz)
  const ISRC_Params ISRC_PARAMS_UPSAMPLE_2X_2ch = {
     sizeof(ISRC_Params),
     &ISRC_PARAMS_STATUS_UPSAMPLE_2X,
     2,
     3,
     0,
     NULL,
     NULL,
     ISRC_MEMREC_UPSAMPLE2
 };

  // Test Case 6: For upsampling by 4X(quadruple) for 2 channel input (Ex: 48khz to 96khz)
  const ISRC_Params ISRC_PARAMS_UPSAMPLE_4X_2ch = {
     sizeof(ISRC_Params),
     &ISRC_PARAMS_STATUS_UPSAMPLE_4X,
     2,
     3,
     0,
     NULL,
     NULL,
     ISRC_MEMREC_UPSAMPLE2
  };

  // Test Case 7: For dowsampling by 2X(half) for 2 channel input (Ex: 96khz to 48khz)
   const ISRC_Params ISRC_PARAMS_DOWNSAMPLE_2X_2ch = {
      sizeof(ISRC_Params),
      &ISRC_PARAMS_STATUS_DOWNSAMPLE_2X,
      2,
      3,
      0,
      NULL,
      NULL,
     ISRC_MEMREC_DOWNSAMPLE2
   };

   // Test Case 8: For dowsampling by 4X for 2 channel input (Ex: 96khz to 48khz)
   const ISRC_Params ISRC_PARAMS_DOWNSAMPLE_4X_2ch = {
      sizeof(ISRC_Params),
      &ISRC_PARAMS_STATUS_DOWNSAMPLE_4X,
      2,
      3,
      0,
      NULL,
      NULL,
      ISRC_MEMREC_DOWNSAMPLE2
   };

   // Test Case 9: For upsampling by 2X for 5.1 channel input (Ex: 48khz to 96khz)
   const ISRC_Params ISRC_PARAMS_UPSAMPLE_2X_5_1 = {
      sizeof(ISRC_Params),
      &ISRC_PARAMS_STATUS_UPSAMPLE_2X,
      6,
      3,
      0,
      NULL,
      NULL,
      ISRC_MEMREC_UPSAMPLE5_1
   };

   // Test Case 10: For upsampling by 4X for 5.1 channel input (Ex: 48khz to 192khz)
   const ISRC_Params ISRC_PARAMS_UPSAMPLE_4X_5_1 = {
      sizeof(ISRC_Params),
      &ISRC_PARAMS_STATUS_UPSAMPLE_4X,
      6,
      3,
      0,
      NULL,
      NULL,
      ISRC_MEMREC_UPSAMPLE5_1
   };

   // Test Case 11: For downsampling by 2X for 5.1 channel input (Ex: 192khz to 96khz)
   const ISRC_Params ISRC_PARAMS_DOWNSAMPLE_2X_5_1 = {
      sizeof(ISRC_Params),
      &ISRC_PARAMS_STATUS_DOWNSAMPLE_2X,
      6,
      3,
      0,
      NULL,
      NULL,
      ISRC_MEMREC_DOWNSAMPLE5_1
   };

   // Test Case 12: For downsampling by 4X for 5.1 channel input (Ex: 192khz to 48khz)
   const ISRC_Params ISRC_PARAMS_DOWNSAMPLE_4X_5_1 = {
      sizeof(ISRC_Params),
      &ISRC_PARAMS_STATUS_DOWNSAMPLE_4X,
      6,
      3,
      0,
      NULL,
      NULL,
      ISRC_MEMREC_DOWNSAMPLE5_1
   };

//  {srcParams,   chSat,chSub,inBufSampleCount,outBufSampleCount,outputSampleRate}

tTestTable gTestArray_Up2X[]={
 {(ISRC_Params *)&ISRC_PARAMS_UPSAMPLE_2X,PAF_CC_SAT_MONO,PAF_CC_SUB_ZERO,256,512,96000},
 {(ISRC_Params *)&ISRC_PARAMS_UPSAMPLE_2X_2ch,PAF_CC_SAT_STEREO,PAF_CC_SUB_ZERO,256,512,96000},
 {(ISRC_Params *)&ISRC_PARAMS_UPSAMPLE_2X_5_1,PAF_CC_SAT_SURROUND2,PAF_CC_SUB_ONE,256,512,96000},
      {NULL               ,0    ,0    ,0       ,0}      //
};

tTestTable gTestArray_Up4X[]={
 {(ISRC_Params *)&ISRC_PARAMS_UPSAMPLE_4X,PAF_CC_SAT_MONO,PAF_CC_SUB_ZERO,256,1024,192000},
 {(ISRC_Params *)&ISRC_PARAMS_UPSAMPLE_4X_2ch,PAF_CC_SAT_STEREO,PAF_CC_SUB_ZERO,256,1024,192000},
 {(ISRC_Params *)&ISRC_PARAMS_UPSAMPLE_4X_5_1,PAF_CC_SAT_SURROUND2,PAF_CC_SUB_ONE,256,1024,192000},
      {NULL               ,0    ,0    ,0       ,0}      //
};

tTestTable gTestArray_Down2X[]={
 {(ISRC_Params *)&ISRC_PARAMS_DOWNSAMPLE_2X,PAF_CC_SAT_MONO,PAF_CC_SUB_ZERO,256,128,96000},
 {(ISRC_Params *)&ISRC_PARAMS_DOWNSAMPLE_2X_2ch,PAF_CC_SAT_STEREO,PAF_CC_SUB_ZERO,256,128,96000},
 {(ISRC_Params *)&ISRC_PARAMS_DOWNSAMPLE_2X_5_1,PAF_CC_SAT_SURROUND2,PAF_CC_SUB_ONE,256,128,96000},
     {NULL               ,0    ,0    ,0       ,0}      //
};

tTestTable gTestArray_Down4X[]={
 {(ISRC_Params *)&ISRC_PARAMS_DOWNSAMPLE_4X,PAF_CC_SAT_MONO,PAF_CC_SUB_ZERO,256,64,48000},
 {(ISRC_Params *)&ISRC_PARAMS_DOWNSAMPLE_4X_2ch,PAF_CC_SAT_STEREO,PAF_CC_SUB_ZERO,256,64,48000},
 {(ISRC_Params *)&ISRC_PARAMS_DOWNSAMPLE_4X_5_1,PAF_CC_SAT_SURROUND2,PAF_CC_SUB_ONE,256,64,48000},
      {NULL               ,0    ,0    ,0       ,0}      //
};


char up2x_outFileName[][MAX_FILE_NAME_SIZE]={
    "..//..//test_vectors//output//UPSAMPLE_2X_48Khz_96Khz",
    "..//..//test_vectors//output//UPSAMPLE_2X_48Khz_96Khz_stereo",
    "..//..//test_vectors//output//UPSAMPLE_2X_48Khz_96Khz_5_1"
};

char up4x_outFileName[][MAX_FILE_NAME_SIZE]={
    "..//..//test_vectors//output//UPSAMPLE_4X_48Khz_192Khz",
    "..//..//test_vectors//output//UPSAMPLE_4X_48Khz_192Khz_stereo",
    "..//..//test_vectors//output//UPSAMPLE_4X_48Khz_192Khz_5_1"
};

char down2x_outFileName[][MAX_FILE_NAME_SIZE]={
    "..//..//test_vectors//output//DOWNSAMPLE_2X_192Khz_96khz",
    "..//..//test_vectors//output//DOWNSAMPLE_2X_192Khz_96khz_stereo",
    "..//..//test_vectors//output//DOWNSAMPLE_2X_192Khz_96Khz_5_1"
};

char down4x_outFileName[][MAX_FILE_NAME_SIZE]={
    "..//..//test_vectors//output//DOWNSAMPLE_4X_192Khz_48khz",
    "..//..//test_vectors//output//DOWNSAMPLE_4X_192Khz_48khz_stereo",
    "..//..//test_vectors//output//DOWNSAMPLE_4X_192Khz_48Khz_5_1"
};

char up2x_inFileName[][MAX_FILE_NAME_SIZE]={
    "..//..//test_vectors//input//chirp_48k",
    "..//..//test_vectors//input//chirp_48k_stereo",
    "..//..//test_vectors//input//chirp_48k_5_1"
};

char up4x_inFileName[][MAX_FILE_NAME_SIZE]={
    "..//..//test_vectors//input//chirp_48k",
    "..//..//test_vectors//input//chirp_48k_stereo",
    "..//..//test_vectors//input//chirp_48k_5_1"
};

char down2x_inFileName[][MAX_FILE_NAME_SIZE]={
    "..//..//test_vectors//input//chirp_192k",
    "..//..//test_vectors//input//chirp_192k_stereo",
    "..//..//test_vectors//input//chirp_192k_5_1"
};

char down4x_inFileName[][MAX_FILE_NAME_SIZE]={
    "..//..//test_vectors//input//chirp_192k",
    "..//..//test_vectors//input//chirp_192k_stereo",
     "..//..//test_vectors//input//chirp_192k_5_1"
};

/** @brief SRC table*/
pTestTable testapp_srcTable[] = {
    gTestArray_Up2X,       /**< Up sample by 2X  */
    gTestArray_Up4X,       /**< Up sample by 4X  */
    gTestArray_Down2X,    /**< Down sample by 2X*/
    gTestArray_Down4X    /**< Down sample by 4X*/
};

/** @brief Avalaible src functions*/
char (*testOutFileName[])[] = {
    up2x_outFileName,
    up4x_outFileName,
    down2x_outFileName,
    down4x_outFileName
};

/** @brief Avalaible src functions*/
char (*testInFileName[])[] = {
    up2x_inFileName,
    up4x_inFileName,
    down2x_inFileName,
    down4x_inFileName
};
 /***************************End of file***************************************/
