/******************************************************************************
 * Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/**
 *  @file   util_file_io.c
 *
 *  @brief  This file contains the common definitions for operations
 *          used to read/write audio data from files.
 *
 *          Presently, .wav files are only supported (32bit PCM).
 *
 *  @todo   Further modifications are required to handle metadata and read
 *          encoded (.mlp, .mat, .ec3 and etc.) file.
 *
 *****************************************************************************/

#include "util_file_io.h"
#include "stdio.h"
#include <math.h>

/** Defines */
#define AMPLITUDE_FACTOR       1.0f
#define BITS_PER_BYTE          8
#define INT_MAX_POSITIVE_ABS   2147483647.0f
#define INT_MAX_NEGATIVE_ABS   2147483648.0f

#define MAX(a,b) (((a)>(b))?(a):(b))

/**
 *  @brief     Initialization call
 *
 */
int FIO_Init(tFIOHandle *file, char *inFile, char *outFile)
{
  int error = RETRUN_ERROR;

  file->inFile = fopen(inFile, "rb");       // Open input file
  if (file->inFile)
  {
    file->outFile = fopen(outFile, "wb");   // Open output file
    if (file->outFile)
    {
      // Get input file info
            fread((void*) &file->header_1_data, 1, sizeof(header_1),
                    file->inFile);
            fread((void*) &file->subchunk_data, 1,
                    file->header_1_data.subchunk1_size, file->inFile);
            fread((void*) &file->header_2_data, 1, sizeof(header_2),
                    file->inFile);
            error = RETRUN_SUCCESS;
    }
  }

  return error;
}

/**
 *  @brief     Configuration call
 *
 */
int FIO_Config(tFIOHandle *file, int inBufChannelCount)
{
    int error = RETRUN_ERROR;
    if (file)
    {
        int i;
        char *subchunk_temp = (char*) malloc(sizeof(char) * 50);
        //(subchunk_data)file->subchunk_data;
        subchunk_data *subCunkData = (subchunk_data *) subchunk_temp; 

        for (i = 0; i < file->header_1_data.subchunk1_size; i++)
            *(subchunk_temp + i) = file->subchunk_data[i];

        int bitsPerSample = subCunkData->bits_per_sample;
        //int inFileChannels = subCunkData->num_channels;
        //int inBufSampleCount = file->inBufSampleCount;
        //int outBufSampleCount = file->outBufSampleCount;
        int outputSampleRate = file->outputSampleRate;
        int sampleRate = subCunkData->sample_rate;
        int output_ChannelCount = file->outBufChannelCount;
        header_1 outHeader1 = file->header_1_data;
        header_2 outHeader2 = file->header_2_data;

        if ((file->inBuf == NULL) || (file->outBuf == NULL)
                || (file->tempBuf == NULL)) 
        {
            printf("file->in/out/temp->Buf is not allocated mememory");
            return RETRUN_ERROR;
        }

        // Set the requested configuration in handle
        file->inBufChannelCount = inBufChannelCount;

        // only 32 bit PCM input is supported. Can extend the support
        if (bitsPerSample != 32)
        {
            printf("Supports only for bit depth of 32bit file");
            return RETRUN_ERROR;
        }

        // For channel request which is not equal to
        // number of channels present in input, need
        // to update the informations in the output
        // header file
        if (inBufChannelCount != output_ChannelCount)
        {
            if(inBufChannelCount < output_ChannelCount)
            {
                printf("Requested more number of channels");
                return RETRUN_ERROR;
            }
            else
            {
                //update the RIFF Header with new output channel configuration
                subCunkData->num_channels = output_ChannelCount;
                outHeader2.subchunk2_size = outHeader2.subchunk2_size
                        * ((output_ChannelCount * inBufChannelCount));
            }

        }
        if(sampleRate !=outputSampleRate)
        {
            printf("Input-output sampling rates are not equal");
            return RETRUN_ERROR;
        }

        // write wav header into output file
        fwrite((void*) &outHeader1, 1, sizeof(header_1), file->outFile);
        fwrite((void*) subCunkData, 1, file->header_1_data.subchunk1_size,
                file->outFile);
        fwrite((void*) &outHeader2, 1, sizeof(header_2), file->outFile);
        error = RETRUN_SUCCESS;
    }

    return error;
}

/**
 *  @brief     Data read from input file
 *
 */
int FIO_Read(tFIOHandle *file)
{
    int error = RETRUN_ERROR;

    if (file && (!feof(file->inFile)))
    {
        int smplCount, ch, inputIndex;
        int readCount = 0, zeroPadding = 0;
        int tempData = 0, duplicateData = 0;
        int requestedSmplCount = file->inBufSampleCount;
        int requestedChCount = file->inBufChannelCount;
        subchunk_data *subCunkData = (subchunk_data *) file->subchunk_data;

        int inputChCount = subCunkData->num_channels;
        int bitsPerSample = subCunkData->bits_per_sample;
        int inFileSampleSize = bitsPerSample / BITS_PER_BYTE;
        unsigned char *tempBuf = file->tempBuf;

        // If requested number of channels are not equal to input file channels
        // we nned to dublicate the data present in one input channels to other
        // channels
        if ((requestedChCount != inputChCount) && (inputChCount == 1))
        {
            duplicateData = 1;
        }

        // read audio data from input file in local buf
        readCount = fread(tempBuf, 1,
                inputChCount * requestedSmplCount * inFileSampleSize,
                file->inFile);

        if (readCount == 0)
            return RETRUN_ERROR;

        // Check if input has lesser number of samples than requested
        // in this case we need to add zero samples at remaining samples
        readCount = readCount/(inFileSampleSize * inputChCount);
        if (readCount < requestedSmplCount)
        {
            zeroPadding = requestedSmplCount - readCount;
        }

        /* rearrange the data in the inBuf and duplicate the read
        data on all requested channels if required*/
        inputIndex = 0;
        for (smplCount = 0; smplCount <
                    (requestedSmplCount - zeroPadding); smplCount++)
        {
            for (ch = 0; ch < requestedChCount; ch++)
            {
                // get one sample
                memcpy(&tempData, &tempBuf[inputIndex * inFileSampleSize],
                        inFileSampleSize);

                // check sample sign
                if (tempData >= 0)
                    file->inBuf[(ch * requestedSmplCount) + smplCount] =
                            (float) ((tempData / INT_MAX_POSITIVE_ABS) *
                            AMPLITUDE_FACTOR);
                else
                    file->inBuf[(ch * requestedSmplCount) + smplCount] =
                            (float) ((tempData / INT_MAX_NEGATIVE_ABS) *
                            AMPLITUDE_FACTOR);

                if (duplicateData == 0)
                    inputIndex++;
            }
            if (duplicateData == 1)
                inputIndex++;
        }

        // zero samples added
        for (ch = 0; ch < requestedChCount; ch++)
        {
            memset(&file->inBuf[(ch * requestedSmplCount) + smplCount], 0,
                    zeroPadding * inFileSampleSize);
        }

        error = RETRUN_SUCCESS;
    }

    return error;
}

/**
 *  @brief     Data write to output file
 *
 */
int FIO_Write(tFIOHandle *file)
{
    int error = RETRUN_ERROR;

    if (file)
    {
        int tempData = 0;
        int smplCount, ch, outputIndex;
        int requestedChCount = file->outBufChannelCount;
        int requestedSmplCount = file->outBufSampleCount;
        subchunk_data *subCunkData = (subchunk_data *) file->subchunk_data;
        int bitsPerSample = subCunkData->bits_per_sample;
        int inFileSampleSize = bitsPerSample / BITS_PER_BYTE;
        unsigned char *tempBuf = file->tempBuf;

        // arrange the data in the tempBuf for all the channels

            outputIndex = 0;
            for (smplCount = 0; smplCount < requestedSmplCount; smplCount++)
            {
                for (ch = 0; ch < requestedChCount; ch++)
                {
                // check sample value sign
                if (file->outBuf[(ch * requestedSmplCount) + smplCount] >= 0)
                    tempData = (int) (file->outBuf[(ch * requestedSmplCount)
                            + smplCount] * INT_MAX_POSITIVE_ABS
                            / AMPLITUDE_FACTOR);
                else
                    tempData = (int) (file->outBuf[(ch * requestedSmplCount)
                            + smplCount] * INT_MAX_NEGATIVE_ABS
                            / AMPLITUDE_FACTOR);

                memcpy(&tempBuf[outputIndex * inFileSampleSize], &tempData,
                        inFileSampleSize);
                outputIndex++;
            }
        }

        // write data into output file
        fwrite(tempBuf, 1,
                requestedChCount * requestedSmplCount * inFileSampleSize,
                file->outFile);
        error = RETRUN_SUCCESS;
    }

    return error;
}

/**
 *  @brief     Deinitialization call
 *
 */
int FIO_DeInit(tFIOHandle *file) 
{
    int error = RETRUN_ERROR;
    if (file) 
    {
        fclose(file->inFile);
        fclose(file->outFile);
        free(file->inBuf);
        free(file->outBuf);
        free(file->tempBuf);
        file->inFile = NULL;
        file->outFile = NULL;
        file->inBuf = NULL;
        file->outBuf = NULL;
        file->tempBuf = NULL;

        error = RETRUN_SUCCESS;
    }

    return error;
}
