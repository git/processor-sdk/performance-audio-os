/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/*
 *  ======== iir1tap3ch.c ========
 *  IIR filter implementation for tap-1 & channels-3, SP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 1    and CHANNELS 3 ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1                                    */
/*                           -------------                                   */
/*                            1  - a1*z~1                                    */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n) + b1*x(n-1) + a1*y(n-1)                 */
/*                                                                           */
/*   Canonical form : w(n) = x(n)    + a1*w(n-1)                             */
/*                    y(n) = b0*w(n) + b1*w(n-1)                             */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      w(n) -  w,         w(n-1) - w1                                       */
/*      b0 - filtCfsB[0], b1 - filtCfsB[1], a1 - filtCfsA[0]                 */
/*                                                                           */
/*                                                                           */
/*                              w(n)    b0                                   */
/*     x(n)-->---[+]----->-------@------>>-----[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+    b1     |                            */
/*                 -----<<----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"

/*
 *  ======== Filter_iirT1Ch1() ========
 *  IIR filter, taps-1 & channels-1  
 */
/* Memory section for the function code */
Int Filter_iirT1Ch3( PAF_FilParam *pParam ) 
{
    FIL_CONST Float *x1, *x2, *x3; /* Input ptr */
    Float * restrict y1, * restrict y2, * restrict y3; /* Output ptr */
    FIL_CONST Float *filtCfsB1,*filtCfsB2,*filtCfsB3; /* F.frwd ptrs */
    FIL_CONST Float *filtCfsA1,*filtCfsA2,*filtCfsA3; /* F.back ptrs */
    /* Var mem ptrs */
    Float * restrict filtVars1, * restrict filtVars2,
                  * restrict filtVars3;
    Int count, samp; /* Loop counters */
    Float w_1, w_2, w_3, w1_1, w1_2, w1_3;  /* Filter state regs */
    
    /* Get i/p ptr */   
    x1 = (Float *)pParam->pIn[0];
    x2 = (Float *)pParam->pIn[1];
    x3 = (Float *)pParam->pIn[2];

    /* Get o/p ptr */
    y1 = (Float *)pParam->pOut[0];
    y2 = (Float *)pParam->pOut[1];
    y3 = (Float *)pParam->pOut[2];
        
    /* Ch-1 */
    filtCfsB1 = (Float *)pParam->pCoef[0]; /* Feedforward ptr */
    filtCfsA1 = filtCfsB1 + 2; /* Derive feedback coeff ptr */
       
    /* Ch-2 */
    filtCfsB2 = (Float *)pParam->pCoef[1]; /* Feedforward ptr */
    filtCfsA2 = filtCfsB2 + 2; /* Derive feedback coeff ptr */       
    
    /* Ch-3 */
    filtCfsB3 = (Float *)pParam->pCoef[2]; /* Feedforward ptr */
    filtCfsA3 = filtCfsB3 + 2; /* Derive feedback coeff ptr */       
    
    /* Get filter var ptr */
    filtVars1 = (Float *)pParam->pVar[0];
    filtVars2 = (Float *)pParam->pVar[1];
    filtVars3 = (Float *)pParam->pVar[2];
    
    count = pParam->sampleCount; /* I/p sample block-length */
    
    /* Get the filter states into corresponding regs */
    /* Ch-1 */
    w_1  = filtVars1[0];
    w1_1 = filtVars1[0];
    
    /* Ch-2 */
    w_2  = filtVars2[0];
    w1_2 = filtVars2[0];
    
    /* Ch-3 */
    w_3  = filtVars3[0];
    w1_3 = filtVars3[0];
    
    /* IIR filtering for i/p block length */  
    #pragma MUST_ITERATE(16, 2048, 4)  
    for(samp=0; samp<count; ++samp)
    {
        /* Ch-1 */
        w_1 = x1[samp] + filtCfsA1[0]*w_1; /* x(n)+a1*w(n-1) */
        y1[samp] = filtCfsB1[0]*w_1 + filtCfsB1[1]*w1_1; /* b0*w(n)+b1*w(n-1) */
        w1_1  = w_1; /* State reg shift */
        
        /* Ch-2 */
        w_2 = x2[samp] + filtCfsA2[0]*w_2; /* x(n)+a1*w(n-1) */
        y2[samp] = filtCfsB2[0]*w_2 + filtCfsB2[1]*w1_2; /* b0*w(n)+b1*w(n-1) */
        w1_2  = w_2; /* State reg shift */
        
        /* Ch-3 */
        w_3 = x3[samp] + filtCfsA3[0]*w_3; /* x(n)+a1*w(n-1) */
        y3[samp] = filtCfsB3[0]*w_3 + filtCfsB3[1]*w1_3; /* b0*w(n)+b1*w(n-1) */
        w1_3  = w_3; /* State reg shift */
    }
    
    /* Update state memory */
    filtVars1[0] = w_1;    
    filtVars2[0] = w_2;    
    filtVars3[0] = w_3;    

    return(FIL_SUCCESS);    
} /* Int Filter_iirT1Ch3() */

