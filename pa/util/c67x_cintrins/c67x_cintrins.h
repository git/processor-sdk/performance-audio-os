/*
Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com/
All rights reserved.

* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions
* are met:
*
* Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
*
* Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the
* distribution.
*
* Neither the name of Texas Instruments Incorporated nor the names of
* its contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

// C67x C intrinsics host implementation interface

#ifndef C67X_CINTRINS_H
#define C67X_CINTRINS_H

#ifndef _TMS320C6X

#ifdef ARMCOMPILE
#define __int64 long long
#endif

typedef __int64 word64;

typedef unsigned __int64 uword64;

/* don't use long */
typedef signed __int64 Word40;
typedef unsigned __int64 UWord40;
typedef double fl64;
typedef float fl32;
typedef int Word32;
typedef unsigned int UWord32;
typedef short Word16;
typedef unsigned short UWord16;
typedef char Word8;
typedef unsigned char UWord8;

#define MIN_32 (Word32)0x80000000
#define MAX_32 (Word32)0x7fffffff
#define MIN_40 (Word40)0x8000000000
#define MAX_40 (Word40)0x7fffffffff

#define	_abs	 	abs_c
#define	_add2	 	add2_c
#define	_clr		clr_c
#define	_ext		ext_c
#define	_extu		extu_c
#define	_lmbd		lmbd_c
#define	_norm		norm_c
#define	_lnorm		lnorm_c
#define	_sadd		sadd_c
#define	_lsadd		lsadd_c
#define	_ssub		ssub_c
#define	_lssub		lssub_c
#define	_sat		sat_c
#define	_set		set_c
#define	_sshl		sshl_c
#define _sub2		sub2_c
#define _subc		subc_c

#define	_mpy		mpy_c
#define	_mpyus		mpyus_c
#define	_mpysu		mpysu_c
#define	_mpyu		mpyu_c
#define	_mpyh		mpyh_c
#define	_mpyhus		mpyhus_c
#define	_mpyhsu		mpyhsu_c
#define	_mpyhu		mpyhu_c
#define	_mpylh		mpylh_c
#define	_mpyluhs	mpyluhs_c
#define	_mpylshu	mpylshu_c
#define	_mpylhu		mpylhu_c
#define	_mpyhl		mpyhl_c
#define	_mpyhuls	mpyhuls_c
#define	_mpyhslu	mpyhslu_c
#define	_mpyhlu		mpyhlu_c
#define	_smpy		smpy_c
#define	_smpyh		smpyh_c
#define	_smpylh		smpylh_c
#define	_smpyhl		smpyhl_c

#define _lo         lo_c
#define _hi         hi_c
#define _ltod		ltod_c

#define _itof       itof_c
#define _ftoi       ftoi_c
#define _spint		spint_c
#define _dpint		dpint_c
#define _fabs		fabs_c
#define _nassert(x)	assert(x)
#define _swap4 swap4_c
#define DWORD_ALIGNED(p)

/* don't use long */
Word32 abs_c(Word32 src1);
Word32 add2_c(Word32 src1, Word32 src2);
UWord32 clr_c(UWord32 src1, UWord32 csta, UWord32 cstb);
Word32 ext_c(Word32 src1, UWord32 csta, UWord32 cstb);
UWord32 extu_c(UWord32 src1, UWord32 csta, UWord32 cstb);
UWord32 lmbd_c(UWord32 src1, UWord32 src2);
UWord32 lnorm_c(Word40 src2);
Word40 lsadd_c(Word32 src1, Word40 src2);

Word32 mpy_c(Word32 src1, Word32 src2);
Word32 mpyus_c(UWord32 src1, Word32 src2);
Word32 mpysu_c(Word32 src1, UWord32 src2);
Word32 mpyu_c(UWord32 src1, UWord32 src2);
Word32 mpyh_c(Word32 src1, Word32 src2);
Word32 mpyhus_c(UWord32 src1, Word32 src2);
Word32 mpyhsu_c(Word32 src1, UWord32 src2);
Word32 mpyhu_c(UWord32 src1, UWord32 src2);
Word32 mpyhl_c(Word32 src1, Word32 src2);
Word32 mpyhuls_c(UWord32 src1, Word32 src2);
Word32 mpyhslu_c(Word32 src1, UWord32 src2);
Word32 mpyhlu_c(UWord32 src1, UWord32 src2);
Word32 mpylh_c(Word32 src1, Word32 src2);
Word32 mpyluhs_c(UWord32 src1, Word32 src2);
Word32 mpylshu_c(Word32 src1, UWord32 src2);
Word32 mpylhu_c(UWord32 src1, UWord32 src2);
fl64 mpyid_c(Word32 src1, Word32 src2);

UWord32 norm_c(Word32 src2);
Word32 sadd_c(Word32 src1, Word32 src2);
Word32 sat_c(Word40 src2);
UWord32 set_c(UWord32 src1, UWord32 csta, UWord32 cstb);
Word32 smpy_c(Word32 src1, Word32 src2);
Word32 smpyh_c(Word32 src1, Word32 src2);
Word32 smpyhl_c(Word32 src1, Word32 src2);
Word32 smpylh_c(Word32 src1, Word32 src2);
Word32 sshl_c(Word32 src2, UWord32 src1);
Word32 ssub_c(Word32 src1, Word32 src2);
Word32 sub2_c(Word32 src1, Word32 src2);
UWord32 subc_c(UWord32 src1, UWord32 src2);

UWord32 hi_c(fl64);
UWord32 lo_c(fl64);
fl64 ltod_c(Word40);

UWord32 ftoi_c(fl32);
Word32 spint_c(fl32);

Word32 dpint_c(double);
fl32 itof_c(UWord32);
fl32 fabs_c(fl32);

float powf(float a, float b);
uword64 _itoll(UWord32 a, UWord32 b);
float _fabsf(float a);
float ldexpf(float a, int b);
UWord32 _swap4(UWord32 a);
#else

typedef signed long Word40;
typedef unsigned long UWord40;

#define DWORD_ALIGNED(p)	_nassert(((unsigned int)(p) & 7) == 0)

#endif /* _TMS320C6X */

#endif /* CINTRINS_H */

/* END OF FILE */
