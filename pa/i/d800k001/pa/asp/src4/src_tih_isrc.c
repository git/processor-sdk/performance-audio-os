
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

// #define SINGLEONLY /* define for testing to force use of non-dual filters */

/*
 *  SRC Module implementation - TIH implementation of a Synchronous Rate Conversion Algorithm.
 */

#include <std.h>
#include  <std.h>
#include <xdas.h>

#include <isrc.h>
#include <src_tih.h>
#include <src_tih_priv.h>
#include <srcerr.h>

#include "paftyp.h"
#include "cpl.h"

/* For now; to co-exist with CDM (FIXME) */
//#undef CPL_CALL 
//#define CPL_CALL(cplfxn) (src->cplFxns->cplfxn)

#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif /* PAF_AUDIODATATYPE_FIXED */

#ifndef _TMS320C6X
#define restrict
#endif /* _TMS320C6X */

Int nbits (Int x);

/*
 *  ======== SRC_TIH_apply ========
 *  TIH's implementation of the apply operation.
 */

Int SRC_TIH_apply_patch(ISRC_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    SRC_TIH_Obj *src = (Void *)handle;

    Uns ratio = src->pStatus->rateStream;

    if (src->pStatus->mode == 0)
        return 0;

    if (ratio > 0
        && handle->fxns->ratio[ratio-1]
        && src->pStatus->sampleRate != PAF_SAMPLERATE_NONE
        && ! handle->fxns->ratio[ratio-1] (handle, pAudioFrame, 1)) {
        pAudioFrame->sampleRate = src->pStatus->sampleRate;

#ifdef PAF_PROCESS_SRC
        PAF_PROCESS_SET (pAudioFrame->sampleProcess, SRC);
#endif /* PAF_PROCESS_SRC */
    }

    return 0;
}

/*
 *  ======== SRC_TIH_ratio2 ========
 *  TIH's implementation of the ratio2 operation.
 *
 *  Downsample by 2. Requires matching filter coefficients.
 */

Int SRC_TIH_ratio2_patch(ISRC_Handle handle, PAF_AudioFrame *pAudioFrame, Int apply)
{
    SRC_TIH_Obj *src = (Void *)handle;
    Int iMax = src->config.nChannels;
    Int kMax = pAudioFrame->sampleCount >> 1;
    Int mask = pAudioFrame->fxns->channelMask (pAudioFrame, pAudioFrame->channelConfigurationStream);
    Int order = src->pStatus->dnHtoQ->order;
	PAF_AudioData *cf = src->pStatus->dnHtoQ->cf;
	
    if (apply) {
    if (src->config.nChannels < nbits (mask)) return 3;
	}

	if (!(src->pStatus->dnHtoQ)) return 1;

    pAudioFrame->sampleCount = kMax;

    if (! apply || src->config.mask != mask) {
        CPL_CALL(vecSet)(0.0f, src->config.state, iMax * order);
        src->config.mask = mask;
    }

    if (apply) {
        Int i, iS;
        for (i = 0, iS = 0; i < pAudioFrame->data.nChannels && iS < iMax; i++) {
            PAF_AudioData *x = src->config.tmp;
            PAF_AudioData *y = pAudioFrame->data.sample[i];
            PAF_AudioData *s = &src->config.state[order * iS];
            
            if (!(mask & (1 << i))) continue;
            ++iS;
            
			CPL_CALL(vecScale)(1.0f, s, x, order);
			CPL_CALL(vecScale)(1.0f, y, x + order, kMax * 2);
            handle->fxns->dsamp2x(x, cf, y, kMax, order);
			CPL_CALL(vecScale)(1.0f, x + kMax * 2, s, order);
        }
    }
    return 0;
}

/*
 *  ======== SRC_TIH_ratio4 ========
 *  TIH's implementation of the ratio4 operation.
 *
 *  Downsample by 4. Requires matching filter coefficients.
 */

Int SRC_TIH_ratio4_patch(ISRC_Handle handle, PAF_AudioFrame *pAudioFrame, Int apply)
{
    SRC_TIH_Obj *src = (Void *)handle;
    Int		iMax = src->config.nChannels;
    Int		kMax = pAudioFrame->sampleCount >> 2;
    Int		mask = pAudioFrame->fxns->channelMask (pAudioFrame, pAudioFrame->channelConfigurationStream);
    Int		order1 = src->pStatus->dn1toH->order;
    Int		order2 = src->pStatus->dnHtoQ->order;
    float	*cf1 = src->pStatus->dn1toH->cf;
	float	*cf2 = src->pStatus->dnHtoQ->cf;
	
	if (apply) {
    if (src->config.nChannels < nbits (mask)) return 3;
	}

	if (!(src->pStatus->dnHtoQ) || !(src->pStatus->dn1toH) ) return 1;

    pAudioFrame->sampleCount = kMax;

    if (! apply || src->config.mask != mask) {
        CPL_CALL(vecSet)(0.0f, src->config.state, iMax * (order1 + order2));
        src->config.mask = mask;
    }

    if (apply) {
        Int i, iS;
        for (i = 0, iS = 0; i < pAudioFrame->data.nChannels && iS < iMax; i++) {
            PAF_AudioData *x1 = src->config.tmp;
            PAF_AudioData *x2 = src->config.tmp + order1 + kMax * 4;
            PAF_AudioData *y = pAudioFrame->data.sample[i];
            PAF_AudioData *s1 = &src->config.state[order2 * iMax + order1 * iS];
            PAF_AudioData *s2 = &src->config.state[order2 * iS];
            
            if (!(mask & (1 << i))) continue;
            ++iS;

			CPL_CALL(vecScale)(1.0f, s1, x1, order1);
			CPL_CALL(vecScale)(1.0f, y, x1 + order1, kMax * 4);            
            handle->fxns->dsamp2x(x1, cf1, x2 + order2, kMax * 2, order1);
			CPL_CALL(vecScale)(1.0f, x1 + kMax * 4, s1, order1);            

			CPL_CALL(vecScale)(1.0f, s2, x2, order2);			
			handle->fxns->dsamp2x(x2, cf2, y, kMax, order2);
			CPL_CALL(vecScale)(1.0f, x2 + kMax * 2, s2, order2);
        }
    }
    return 0;
}


/*
 *  ======== SRC_TIH_ratioD ========
 *  TIH's implementation of the ratioD operation.
 *
 *  Upsample by 2 (double). Requires matching filter coefficients.
 */
Int SRC_TIH_ratioD_patch(ISRC_Handle handle, PAF_AudioFrame *pAudioFrame, Int apply)
{
    SRC_TIH_Obj *src = (Void *)handle;
    Int ratio = 2;
    Int iMax = src->config.nChannels;
    Int kMax = pAudioFrame->sampleCount; 
	PAF_AudioData *cf1 = src->pStatus->up1to2->cf;
	Int order = src->pStatus->up1to2->order;
    Int jMax = order / 2 + 1;
	
    Int mask = pAudioFrame->fxns->channelMask (pAudioFrame, pAudioFrame->channelConfigurationStream);

    if (ratio * kMax > pAudioFrame->data.nSamples) return 1;

    if (apply) {
	    if (src->config.nChannels < nbits (mask)) return 3;
	}

	if (!(src->pStatus->up1to2)) return 1;

    pAudioFrame->sampleCount = ratio * kMax;

    if (!apply || src->config.mask != mask) {
        CPL_CALL(vecSet)(0.0f, src->config.state, iMax * jMax);
        src->config.mask = mask;
    }

    if (apply) {
        int				i,iS;

        for (i = 0, iS = 0; i < pAudioFrame->data.nChannels && iS < iMax; i++) {
		    PAF_AudioData *x = src->config.tmp;
            PAF_AudioData *y = &pAudioFrame->data.sample[i][0];
            PAF_AudioData *s = &src->config.state[jMax * iS];

            if (!(mask & (1 << i))) continue;
            ++iS;
			
			// put together state/input buffer

			CPL_CALL(vecScale)(1.0f, s, x, jMax);
	        CPL_CALL(vecScale)(2.0f, y, x + jMax, kMax);
			handle->fxns->usamp2x(x, cf1, y, kMax * ratio, order);                
     		CPL_CALL(vecScale)(1.0f, x + kMax, s, jMax);		// update state
		}
    }
    return 0;
}

/*
 *  ======== SRC_TIH_ratioQ ========
 *  TIH's implementation of the ratioQ operation.
 *
 *  Upsample by 4 (quadruple). Requires matching filter coefficients.
 */
Int SRC_TIH_ratioQ_patch(ISRC_Handle handle, PAF_AudioFrame *pAudioFrame, Int apply)
{
    SRC_TIH_Obj *src = (Void *)handle;
    Int ratio = 4;
    Int iMax = src->config.nChannels;
    Int kMax = pAudioFrame->sampleCount; 
	PAF_AudioData *cf1 = src->pStatus->up1to2->cf;
	PAF_AudioData *cf2 = src->pStatus->up2to4->cf;
	Int order1 = src->pStatus->up1to2->order;
	Int order2 = src->pStatus->up2to4->order;
    Int jMax1 = order1 / 2 + 1;
    Int jMax2 = order2 / 2 + 1;
    	
    Int mask = pAudioFrame->fxns->channelMask (pAudioFrame, pAudioFrame->channelConfigurationStream);

    if (ratio * kMax > pAudioFrame->data.nSamples) return 1;
    
	if (apply) {
	    if (src->config.nChannels < nbits (mask)) return 3;
	}

	if (!(src->pStatus->up1to2) || !(src->pStatus->up2to4)) return 1;
    pAudioFrame->sampleCount = ratio * kMax;

    if (!apply || src->config.mask != mask) {
        CPL_CALL(vecSet)(0.0f, src->config.state, iMax * (jMax1 + jMax2));
        src->config.mask = mask;
    }

    if (apply) {
        int i,iS;

        for (i = 0, iS = 0; i < pAudioFrame->data.nChannels && iS < iMax; i++) {
		    PAF_AudioData *x1 = src->config.tmp;
		    PAF_AudioData *x2 = src->config.tmp + jMax1 + kMax;
            PAF_AudioData *y = &pAudioFrame->data.sample[i][0];
            PAF_AudioData *s1 = &src->config.state[jMax1 * iS];
            PAF_AudioData *s2 = &src->config.state[jMax1 * iMax + jMax2 * iS];

            if (!(mask & (1 << i))) continue;
            ++iS;
			
			// put together state/input buffer
			CPL_CALL(vecScale)(1.0f, s1, x1, jMax1);
	        CPL_CALL(vecScale)(4.0f, y, x1 + jMax1, kMax);
			handle->fxns->usamp2x(x1, cf1, x2 + jMax2, kMax * ratio / 2, order1);
     		CPL_CALL(vecScale)(1.0f, x1 + kMax, s1, jMax1);

			CPL_CALL(vecScale)(1.0f, s2, x2, jMax2);
			handle->fxns->usamp2x(x2, cf2, y, kMax * ratio, order2);
     		CPL_CALL(vecScale)(1.0f, x2 + kMax * ratio / 2, s2, jMax2);
		}
    }
    return 0;
}
