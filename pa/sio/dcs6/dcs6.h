
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DA8xx SPI/I2C slave control status (DCS6) implementation
//

//
//

#ifndef _DCS6
#define _DCS6    1
#ifdef BIOS6_NONLEGACY
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/IHeap.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/family/c64p/Cache.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/ipc/Semaphore.h>
#include <ti/sysbios/heaps/HeapMem.h>
#else
#include <std.h>
#include <hwi.h>
#include <c64.h>
#include <tsk.h>
#include <sem.h>
#include <mem.h>
#include <log.h>
#include <ti/sysbios/family/c64p/Cache.h>
#endif

#include <acp.h>
#include <acp_mds.h>
#include <acp_mds_priv.h>

#include <dmax.h>
#include <dmax_struct.h>

#include <dcs6_params.h>

#include <primus.h>

/* Parameter Table Definition */
#define DCS6_dMAXPARAM_TYPE                24
#define DCS6_dMAXPARAM_SIZE                72
#define DCS6_dMAXPARAM_CONTROL_PSFIL_DIS   0
#define DCS6_dMAXPARAM_CONTROL_PSFIL_ENA   1
#define DCS6_dMAXPARAM_CONTROL_PSFIL_MSK   1
#define DCS6_dMAXPARAM_CONTROL_PSFIL_SHFT  0
#define DCS6_dMAXPARAM_CONTROL_PSLC_DIS    0
#define DCS6_dMAXPARAM_CONTROL_PSLC_ENA    1
#define DCS6_dMAXPARAM_CONTROL_PSLC_MSK    2
#define DCS6_dMAXPARAM_CONTROL_PSLC_SHFT   1
#define DCS6_dMAXPARAM_CONTROL_PR_DIS      0
#define DCS6_dMAXPARAM_CONTROL_PR_ENA      1
#define DCS6_dMAXPARAM_CONTROL_PR_MSK      4
#define DCS6_dMAXPARAM_CONTROL_PR_SHFT     2
#define DCS6_dMAXPARAM_CONTROL_PW_DIS      0
#define DCS6_dMAXPARAM_CONTROL_PW_ENA      1
#define DCS6_dMAXPARAM_CONTROL_PW_MSK      8
#define DCS6_dMAXPARAM_CONTROL_PW_SHFT     3
#define DCS6_dMAXPARAM_CONTROL_SRDY_DIS    0
#define DCS6_dMAXPARAM_CONTROL_SRDY_ENA    1
#define DCS6_dMAXPARAM_CONTROL_SRDY_MSK    16
#define DCS6_dMAXPARAM_CONTROL_SRDY_SHFT   4 
#define DCS6_dMAXPARAM_CONTROL_EDR_DIS     0
#define DCS6_dMAXPARAM_CONTROL_EDR_ENA     1
#define DCS6_dMAXPARAM_CONTROL_EDR_MSK     32
#define DCS6_dMAXPARAM_CONTROL_EDR_SHFT    5 
#define DCS6_dMAXPARAM_CONTROL_PROT_SHFT   24
#define DCS6_dMAXPARAM_STATUS_NOLINK       0
#define DCS6_dMAXPARAM_STATUS_LINK         1
#define DCS6_dMAXPARAM_STATUS_LINK_MSK     1
#define DCS6_dMAXPARAM_STATUS_LINK_SHFT    0
#define DCS6_dMAXPARAM_INTERRUPT_SPI0      11
#define DCS6_dMAXPARAM_INTERRUPT_SPI1      12
#define DCS6_dMAXPARAM_INTERRUPT_I2C0      15
#define DCS6_dMAXPARAM_INTERRUPT_I2C1      16
typedef struct DCS6_dMAXParam{
    Uint32 control;
    Uint32 drr;
    Uint32 rbuf_start;
    Uint16 rbuf_len;
    Uint8 ec;
    Uint8 rc;
    Uint16 rbuf_hwm;
    Uint16 rbuf_lwm;
    Uint32 dxr;
    Uint32 xbuf_start;
    Uint16 xbuf_len;
    Uint16 res1;
    Uint32 status;
    Uint32 ext_status;
    Uint16 rbuf_off;
    Uint16 rbuf_poff;
    Uint16 xbuf_off;
    Uint16 xbuf_poff;
    Uint32 std_beta_base;
    Uint32 alt_beta_base;
    Uint32 oem_beta_base;
    Uint32 cus_beta_base;
    Uint8 beta_prime_value;
    Uint8 beta_prime_base;
    Uint8 beta_prime_offset;
    Uint8 res2;
    Uint32 err_mask;
} DCS6_dMAXParam;
#define DCS6_dMAXPARAM_INTERRUPT_UART0     13
#define DCS6_dMAXPARAM_INTERRUPT_UART1     14
#define DCS6_dMAXPARAM_INTERRUPT_UART2     17
typedef struct DCS6_dMAXUartParam{
    Uint32 control;
    Uint32 drr;
    Uint32 rbuf_start;
    Uint16 rbuf_len;
    Uint8  ec;
    Uint8  rc;
    Uint16 rbuf_hwm;
    Uint16 rbuf_lwm;
    Uint32 dxr;
    Uint32 xbuf_start;
    Uint16 xbuf_len;
    Uint16 res1;
    Uint32 status;
    Uint32 ext_status;
    Uint16 rbuf_off;
    Uint16 rbuf_poff;
    Uint16 xbuf_off;
    Uint16 xbuf_poff;
    Uint32 std_beta_base;
    Uint32 alt_beta_base;
    Uint32 oem_beta_base;
    Uint32 cus_beta_base;
    Uint8  beta_prime_value;
    Uint8  beta_prime_base;
    Uint8  beta_prime_offset;
    Uint8  res2;
    Uint32 err_mask;
    Uint8  rx_line_cnt;
    Uint8  tx_line_cnt;
    Uint8  rxfiflen;
    Uint8  res3;
    Uint8  errcnt;
    Uint8  errflg;
    Uint16 res4;
    Uint32 string_base;
    Uint32 xrbuf_start;
    Uint16 xrbuf_len;
    Uint16 res5;
    Uint16 xrbuf_off;
    Uint16 xrbuf_poff;
    Uint32 xxbuf_start;
    Uint16 xxbuf_len;
    Uint16 res6;
    Uint16 xxbuf_off;
    Uint16 xxbuf_poff;
} DCS6_dMAXUartParam;
extern const Uint8 UART_STRINGS[];
/* Event Entry Table Definition */
#define DCS6_dMAXPARAM_MKEE(P,T)    (((P)&0x000001FF)<<8|(T))

/* Configuration definition */
typedef struct DCS6_Config_Dmax{
    Uint32 size;
    Int8 hint;
    Int8 rc;
    Int8 ec;
    Int8 res1;
}DCS6_Config_Dmax;

#define DCS6_CONFIG_CACHEMODE_DISABLE    0
#define DCS6_CONFIG_CACHEMODE_ENABLE     1
typedef struct DCS6_Config{
    Uint32 size;
    void *objMemSeg;
    void *bufMemSeg;
    Uint8 cacheMode;
    Uint8 res;
    Uint16 cacheBufAlign;
    DCS6_Config_Dmax *dmax;
}DCS6_Config;
extern const DCS6_Config DCS6_CONFIG;

/* Handle definition */
typedef struct DCS6_Obj *DCS6_Handle;

/* Function Table definition */
typedef struct DCS6_Fxns *DCS6_Fxns_Ptr;
typedef struct DCS6_Fxns{
    void (*log)(DCS6_Handle,void*,char*);
    void (*log1)(DCS6_Handle,void*,char*,Int32);
    void* (*semAlloc)(DCS6_Handle);
    Uint32 (*semPend)(DCS6_Handle,void*);
    void (*semPost)(DCS6_Handle,void*);
    void* (*memAlloc)(DCS6_Handle,void*,Uint32,Uint32);
    void (*taskDisable)(DCS6_Handle);
    void (*taskEnable)(DCS6_Handle);
    void (*configureInt)(DCS6_Handle);
    void (*cacheInv)(DCS6_Handle,void*,Uint32);
    void (*cacheWbInv)(DCS6_Handle,void*,Uint32);
    void (*errorInit)();
    void (*errorAssert)();
    void (*errorDeassert)();
    void (*errorRecover)(DCS6_Handle);
    void (*slc)(DCS6_Handle);
    Uint32 (*read)(DCS6_Handle,Uint16*,Uint16);
    Uint32 (*write)(DCS6_Handle,Uint16*,Uint16);
    void (*isr)(UArg);
    void (*configuredMAX)(DCS6_Handle);
    void (*configureSPI)(DCS6_Handle);
    void (*configureI2C)(DCS6_Handle);
    void (*configureUART)(DCS6_Handle);
    void (*configure)(DCS6_Handle);
    void (*resetSPI)(DCS6_Handle);
    void (*resetI2C)(DCS6_Handle);
    void (*resetUART)(DCS6_Handle);
    void (*reset)(DCS6_Handle);
    Uint32 (*resAllocdMAX)(DCS6_Handle);
    Uint32 (*resAllocSPI)(DCS6_Handle);
    Uint32 (*resAllocI2C)(DCS6_Handle);
    Uint32 (*resAllocUART)(DCS6_Handle);
    Uint32 (*resAlloc)(DCS6_Handle);
    DCS6_Handle (*open)(const DCS6_Params*,const DCS6_Config*,const DCS6_Fxns_Ptr,dMAX_Handle,ACP_Handle);
}DCS6_Fxns;
extern const DCS6_Fxns DCS6_FXNS;

/* Object definition */
typedef struct DCS6_dMAXObj{
    Uint32 size;
    DCS6_dMAXParam *param;
    Int32 cpusi;
    Uint32 cpuint;
    Int32 rc;
    Int32 ec;
}DCS6_dMAXObj;

typedef struct DCS6_Obj{
    Uint32 size;
    const DCS6_Fxns *fxns;
    const DCS6_Params *params;
    const DCS6_Config *config;
    dMAX_Handle dmaxHandle;
    ACP_Handle acp;
    Uint16 *rxBuf;
    Uint16 *txBuf;
    Uint8 *xrxBuf;
    Uint8 *xtxBuf;
    void *sem;
    DCS6_dMAXObj *dmax;
    Uint32 ecnt;
}DCS6_Obj;
#endif
