/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#ifndef SPI_I2C_
#define SPI_I2C_ 1

// SPI_I2C Control field constants
#define SPI_I2C_SFIL_DIS 0       // SPI_I2C fill processing disable 
#define SPI_I2C_SFIL_ENA 1       // SPI_I2C fill processing enable 
#define SPI_I2C_SFIL_MSK 1       // SPI_I2C fill mask 
#define SPI_I2C_SFIL_SHFT 0      // SPI_I2C fill shift

#define SPI_I2C_SLC_DIS  0       // SPI_I2C slc processing disable 
#define SPI_I2C_SLC_ENA  1       // SPI_I2C slc processing enable 
#define SPI_I2C_SLC_MSK  1       // SPI_I2C slc mask 
#define SPI_I2C_SLC_SHFT 1       // SPI_I2C slc shift

#define SPI_I2C_PREAD_DIS 0      // SPI_I2C read processing disable 
#define SPI_I2C_PREAD_ENA 1      // SPI_I2C read processing enable 
#define SPI_I2C_PREAD_MSK 1      // SPI_I2C read mask 
#define SPI_I2C_PREAD_SHFT 2     // SPI_I2C read shift

#define SPI_I2C_PWRITE_DIS 0     // SPI_I2C write processing disable 
#define SPI_I2C_PWRITE_ENA 1     // SPI_I2C write processing enable 
#define SPI_I2C_PWRITE_MSK 1     // SPI_I2C write mask 
#define SPI_I2C_PWRITE_SHFT 3    // SPI_I2C write shift

#define SPI_I2C_SRDY_DIS 0       // SPI_I2C SRDY disable 
#define SPI_I2C_SRDY_ENA 1       // SPI_I2C SRDY enable 
#define SPI_I2C_SRDY_MSK 1       // SPI_I2C SRDY mask 
#define SPI_I2C_SRDY_SHFT 4      // SPI_I2C SRDY shift

#define SPI_I2C_EDR_DIS 0        // SPI_I2C EDR disable 
#define SPI_I2C_EDR_ENA 1        // SPI_I2C EDR enable 
#define SPI_I2C_EDR_MSK 1        // SPI_I2C EDR mask 
#define SPI_I2C_EDR_SHFT 5       // SPI_I2C EDR shift

// SPI_I2C Status field constants
#define SPI_I2C_NOLINK   0       // SPI_I2C No link 
#define SPI_I2C_LINK     1       // SPI_I2C link 
#define SPI_I2C_LINK_MSK 1       // SPI_I2C link mask 
#define SPI_I2C_LINK_SHFT 0      // SPI_I2C link shift

#define SPI_I2C_NOCPU    0       // SPI_I2C No payload submitted to CPU 
#define SPI_I2C_CPU      1       // SPI_I2C payload submitted to CPU 
#define SPI_I2C_CPU_MSK  1       // SPI_I2C payload submitted to CPU mask 
#define SPI_I2C_CPU_SHFT 1       // SPI_I2C payload submitted to CPU shift

#define SPI_I2C_NOWM     0       // SPI_I2C no watermark 
#define SPI_I2C_WM       1       // SPI_I2C hit watermark 
#define SPI_I2C_WM_MSK   1       // SPI_I2C watermark mask 
#define SPI_I2C_WM_SHFT  2       // SPI_I2C watermark shift 

#define SPI_I2C_NOERR    0       // SPI_I2C no error 
#define SPI_I2C_ERR      1       // SPI_I2C error 
#define SPI_I2C_ERR_MSK  1       // SPI_I2C error mask 
#define SPI_I2C_ERR_SHFT 3       // SPI_I2C error shift 

// SPI_I2C Extended Status field constants
#define SPI_I2C_RX_BYTE1  0      // SPI_I2C RX state byte1
#define SPI_I2C_RX_BYTE2  1      // SPI_I2C RX state byte2
#define SPI_I2C_RX_PAYLOAD 2     // SPI_I2C RX state payload 

#define SPI_I2C_TX_BYTE1  0      // SPI_I2C TX state byte1
#define SPI_I2C_TX_BYTE2  1      // SPI_I2C TX state byte2
#define SPI_I2C_TX_PAYLOAD 2     // SPI_I2C TX state payload 

// SPI_I2C parameter field lengths and offsets
#define SPI_I2C_CONTROL_OFF             0    // SPI_I2C control offset
#define SPI_I2C_CONTROL_LEN             4    // SPI_I2C control length
#define SPI_I2C_DRR_OFF                 4    // SPI_I2C drr offset
#define SPI_I2C_DRR_LEN                 4    // SPI_I2C drr length
#define SPI_I2C_RBUF_START_OFF          8    // SPI_I2C rbuf_start offset
#define SPI_I2C_RBUF_START_LEN          4    // SPI_I2C rbuf_start length
#define SPI_I2C_RBUF_LEN_OFF            12   // SPI_I2C rbuf_len offset
#define SPI_I2C_RBUF_LEN_LEN            2    // SPI_I2C rbuf_len length
#define SPI_I2C_EC_OFF                  14   // SPI_I2C ec offset
#define SPI_I2C_EC_LEN                  1    // SPI_I2C ec length
#define SPI_I2C_RC_OFF                  15   // SPI_I2C rc offset
#define SPI_I2C_RC_LEN                  1    // SPI_I2C rc length
#define SPI_I2C_RBUF_HWM_OFF            16   // SPI_I2C rbuf_hwm offset
#define SPI_I2C_RBUF_HWM_LEN            2    // SPI_I2C rbuf_hwm length
#define SPI_I2C_RBUF_LWM_OFF            18   // SPI_I2C rbuf_lwm offset
#define SPI_I2C_RBUF_LWM_LEN            2    // SPI_I2C rbuf_lwm length
#define SPI_I2C_DXR_OFF                 20   // SPI_I2C dxr offset
#define SPI_I2C_DXR_LEN                 4    // SPI_I2C dxr length
#define SPI_I2C_XBUF_START_OFF          24   // SPI_I2C xbuf_start offset
#define SPI_I2C_XBUF_START_LEN          4    // SPI_I2C xbuf_start length
#define SPI_I2C_XBUF_LEN_OFF            28   // SPI_I2C xbuf_len offset
#define SPI_I2C_XBUF_LEN_LEN            2    // SPI_I2C xbuf_len length
#define SPI_I2C_RES1_OFF                30   // SPI_I2C reserved1 offset
#define SPI_I2C_RES1_LEN                2    // SPI_I2C reserved1 length
#define SPI_I2C_STATUS_OFF              32   // SPI_I2C status offset
#define SPI_I2C_STATUS_LEN              4    // SPI_I2C status length
#define SPI_I2C_EXT_STATUS_OFF          36   // SPI_I2C ext_status offset
#define SPI_I2C_EXT_STATUS_LEN          4    // SPI_I2C ext_status length
#define SPI_I2C_RBUF_OFF_OFF            40   // SPI_I2C rbuf_off offset
#define SPI_I2C_RBUF_OFF_LEN            2    // SPI_I2C rbuf_off length
#define SPI_I2C_RBUF_POFF_OFF           42   // SPI_I2C rbuf_poff offset
#define SPI_I2C_RBUF_POFF_LEN           2    // SPI_I2C rbuf_poff length
#define SPI_I2C_XBUF_OFF_OFF            44   // SPI_I2C xbuf_off offset
#define SPI_I2C_XBUF_OFF_LEN            2    // SPI_I2C xbuf_off length
#define SPI_I2C_XBUF_POFF_OFF           46   // SPI_I2C xbuf_poff offset
#define SPI_I2C_XBUF_POFF_LEN           2    // SPI_I2C xbuf_poff length
#define SPI_I2C_STD_BETA_BASE_OFF       48   // SPI_I2C std_beta_base offset
#define SPI_I2C_STD_BETA_BASE_LEN       4    // SPI_I2C std_beta_base length
#define SPI_I2C_ALT_BETA_BASE_OFF       52   // SPI_I2C alt_beta_base offset
#define SPI_I2C_ALT_BETA_BASE_LEN       4    // SPI_I2C alt_beta_base length
#define SPI_I2C_OEM_BETA_BASE_OFF       56   // SPI_I2C oem_beta_base offset
#define SPI_I2C_OEM_BETA_BASE_LEN       4    // SPI_I2C oem_beta_base length
#define SPI_I2C_CUS_BETA_BASE_OFF       60   // SPI_I2C cus_beta_base offset
#define SPI_I2C_CUS_BETA_BASE_LEN       4    // SPI_I2C cus_beta_base length
#define SPI_I2C_BETA_PRIME_VALUE_OFF    64   // SPI_I2C beta_prime_value offset
#define SPI_I2C_BETA_PRIME_VALUE_LEN    1    // SPI_I2C beta_prime_value length
#define SPI_I2C_BETA_PRIME_BASE_OFF     65   // SPI_I2C beta_prime_base offset
#define SPI_I2C_BETA_PRIME_BASE_LEN     1    // SPI_I2C beta_prime_base length
#define SPI_I2C_BETA_PRIME_OFF_OFF      66   // SPI_I2C beta_prime_offset offset
#define SPI_I2C_BETA_PRIME_OFF_LEN      1    // SPI_I2C beta_prime_offset length
#define SPI_I2C_ERR_MASK_OFF            68   // SPI_I2C err_mask offset
#define SPI_I2C_ERR_MASK_LEN            4    // SPI_I2C err_mask length

// SPI_I2C protocol constants
#define SP_B2            0       // Serial Protocol Binary 2 
#define SP_B8            1       // Serial Protocol Binary 8 
#define SP_B16           2       // Serial Protocol Binary 16 

// SPI_I2C interrupt numbers
#define I2C0_INTERRUPT   15
#define I2C1_INTERRUPT   16
#define SPI0_INTERRUPT   11
#define SPI1_INTERRUPT   12

// SPI_I2C status flags
#define I2C_ICSTR_ICRRDY 3
#define I2C_ICSTR_ICXRDY 4
#define SPIFLG_RXINTFLG  8
#define SPIFLG_TXINTFLG  9

// SPI_I2C flag register offsets
//#define _DEBUG           1
#ifndef _DEBUG
#define I2CSTR_OFF       0x8
#define SPIFLG_OFF       0x10
#else
#define I2CSTR_OFF       0x4
#define SPIFLG_OFF       0x50
#endif 

#endif // SPI_I2C_
