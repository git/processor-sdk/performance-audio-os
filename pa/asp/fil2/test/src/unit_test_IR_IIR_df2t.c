/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  
// std headers
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <clk.h>
#include <log.h>
#include  <xdc/std.h>
#include <ti/xdais/xdas.h>
#include <paftyp.h>

// fil headers
#include <ifil.h>
#include <fil_tii.h>
#include <fil_tii_priv.h>
#include <filerr.h>
#include <filextern.h>
#include <filtyp.h>
#include <filters.h>
#include <fil_table.h>

#define NSAMPLES 256
#define NRUNS 64

// Constant
PAF_AudioData gInArray[NSAMPLES];
PAF_AudioData gCoeff[NSAMPLES];  

// Ref Output
PAF_AudioData gOutArrayRef[PAF_MAXNUMCHAN][NSAMPLES];
float gVarsRef[NSAMPLES] = {0};

// Test Output 
PAF_AudioData gOutArray[PAF_MAXNUMCHAN][NSAMPLES];
float gVars[NSAMPLES] = {0};

// Allocation for the param pointers 
PAF_AudioData *pIn[PAF_MAXNUMCHAN];
PAF_AudioData *pOut[PAF_MAXNUMCHAN];
float *pVar[PAF_MAXNUMCHAN];
PAF_AudioData *pCoef[PAF_MAXNUMCHAN];

extern int FUNC_UNDER_TEST[], NCHANNELS[];
int iir_generic( PAF_FilParam * pParam );

int main(void)
{
    int (*funcUnderTest)(PAF_FilParam * pParam ) 
            = (int (*)(PAF_FilParam *))(FUNC_UNDER_TEST);
    int i, j, nchan = (int)NCHANNELS;
    PAF_FilParam param;
    double clks = 0, toterr = 0, maxerr = 0;

    for (j = 0; j < NSAMPLES; ++j)
        gInArray[i] = 2.0f*(rand()/RAND_MAX) - 1.0f;
    for (i = 0; i < nchan; ++i) {
#if 0
        gCoeff[5*i + 0] = 4.0f*(rand()/RAND_MAX) - 2.0f;
        gCoeff[5*i + 1] = 4.0f*(rand()/RAND_MAX) - 2.0f;
        gCoeff[5*i + 2] = 4.0f*(rand()/RAND_MAX) - 2.0f;
        gCoeff[5*i + 3] = 4.0f*(rand()/RAND_MAX) - 2.0f;
        gCoeff[5*i + 4] = 4.0f*(rand()/RAND_MAX) - 2.0f;
#else
        gCoeff[5*i + 0] = 1.0004043000e+000f;
        gCoeff[5*i + 1] = -9.9413821000e-001f;
        gCoeff[5*i + 2] = 0.0000000000e+000f;
        gCoeff[5*i + 3] = 9.9454249000e-001f;
        gCoeff[5*i + 4] = 0.0000000000e+000f;
#endif
    }
    for (i = 0; i < nchan; ++i) {
        memcpy(gOutArrayRef[i], gInArray, NSAMPLES*sizeof(float));
        pOut[i] = pIn[i] = gOutArrayRef[i];
        pVar[i] = &(gVarsRef[2*i]);
        pCoef[i] = &(gCoeff[5*i]);
    }
    
    param.sampleCount = NSAMPLES;
    param.use = 2;
    param.channels = nchan;
    param.pIn = (void **)pIn;
    param.pOut = (void **)pOut;
    param.pCoef = (void **)pCoef;
    param.pVar = (void **)pVar;
    for (i = 0; i < NRUNS; ++i)
        iir_generic(&param);
    
    for (i = 0; i < nchan; ++i) {
        memcpy(gOutArray[i], gInArray, NSAMPLES*sizeof(float));
        pOut[i] = pIn[i] = gOutArray[i];
        pVar[i] = &(gVars[2*i]);
        pCoef[i] = &(gCoeff[5*i]);
    }
    param.sampleCount = NSAMPLES;
    param.use = 2;
    param.channels = nchan;
    param.pIn = (void **)pIn;
    param.pOut = (void **)pOut;
    param.pVar = (void **)pVar;
    param.pCoef = (void **)pCoef;
    for (i = 0; i < NRUNS; ++i) {
        unsigned clk1, clk2;
        clk1 = clock();
        (*funcUnderTest)(&param);
        //iir_generic(&param);
        clk2 = clock();
        clks += (clk2 - clk1);
    }
    for (i = 0; i < nchan; ++i) {
        for (j = 0; j < NSAMPLES; ++j) {
            double err = fabs(gOutArray[i][j] - gOutArrayRef[i][j]);
            if (err > maxerr)
                maxerr = err;
            toterr += err;
        }
    }
    //printf("Func under test = 0x%08X, nchan = %d\n", funcUnderTest, nchan);
    printf("nchan = %d clocks/sample = %.1f, maxerr = %f, avgerr = %f\n",
            nchan,
            clks/((double)nchan*NSAMPLES*NRUNS),
            maxerr, toterr/((double)nchan*NSAMPLES));
    return 0;
}
int iir_generic( PAF_FilParam * pParam ) {
    Int count;
    Int i, t, taps, ch;
    PAF_AudioData accY, accYtemp, accW;
    PAF_AudioData varW, varW_temp;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtVars, * restrict filtVars0;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    PAF_AudioData filtVarsTemp0, filtVarsTemp1;
    
    count = pParam->sampleCount;
    taps  = pParam->use;
    
    for (ch = 0; ch < pParam->channels; ch++) {
        x = (PAF_AudioData *)pParam->pIn[ch];
        y = (PAF_AudioData *)pParam->pOut[ch];            

        filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;

        filtVars  = (PAF_AudioData *)pParam->pVar[ch];
        
        for (i = 0; i < count; i ++) {
             accY = 0; 
             accW = *x;
             
             for( t = 0; t < taps; t++ ) {
                 accW += filtCfsA[t]   * filtVars[t];  
                 
                 accY += filtCfsB[t+1] * filtVars[t]; 
             }
             
             accY += accW * filtCfsB[0]; 
             
             *y++ = accY;
             x++; 
             
             filtVarsTemp0 = filtVars[0];
             
             for( t = 0; t < taps; t++ ) {
                 filtVarsTemp1 =  filtVars[t + 1];
                 
                 filtVars[t + 1] = filtVarsTemp0;
                 
                 filtVarsTemp0 =  filtVarsTemp1; 
             }   
             filtVars[taps] = filtVarsTemp1;
                       
             
             filtVars[0] = accW;
             
         }
     }
     return( FIL_SUCCESS );      
}
