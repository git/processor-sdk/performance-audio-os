/******************************************************************************
* Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/**
 *  @file  testapp.c
 *
 *  @brief DCS test application main code.
 *
 *************************************************************/

/************************************************************
 Include files
 *************************************************************/
#include <stdio.h>
#include <stdlib.h>

/* EDMA3 Header files */
#include <ti/sdo/edma3/drv/edma3_drv.h>
#include <ti/sdo/edma3/rm/edma3_rm.h>
#include <ti/sdo/edma3/rm/sample/bios6_edma3_rm_sample.h>

#include <ti/sysbios/knl/Task.h>

#include "testapp.h"
#include "dcs7_priv.h"
#include "util_c66x_cache.h"


/************************************************************
 Macro and Definitions
 *************************************************************/
// Comment/Uncomment to enable or disable printf messages,
// These when enabled will effect the regress test behaviour.
//#define REGRESS_TEST
// Comment/Uncomment to enable or disable profiling
//#define PROFILER

#ifdef PROFILER
#include "util_profiling.h"
#define PRFL_MAX_COUNT 1
#define REGRESS_TEST
#endif

#ifdef REGRESS_TEST
#define printf(fmt, ...)
#endif

/************************************************************/


extern tTestapp_Obj tTestapp_OBJ;

#ifdef DCS7_UART_EDMA_ENABLE
extern UART_HwAttrs uartInitCfg[];
extern EDMA3_RM_InstanceInitConfig sampleInstInitConfig[][EDMA3_MAX_REGIONS];
extern EDMA3_RM_GblConfigParams sampleEdma3GblCfgParams[];
#endif

// testapp sizes in Bytes
#define ALPHA_CMD_MAX_SIZE       128
#define ALPHA_CMD_RES_MAX_SIZE   256

// testapp Counts
#ifdef PROFILER
#define TESTAPP_ALPHA_MAX_CNT    8
#else
#define TESTAPP_ALPHA_MAX_CNT    21
#endif

/************************************************************
 global variables
 *************************************************************/

Uint16 alphaSequenceReceived[TESTAPP_ALPHA_MAX_CNT][ALPHA_CMD_MAX_SIZE] =
{
#ifdef PROFILER
        {0x0002, 0xc425, 0x0088},           // readENCListeningFormat
        {0x0002, 0xc224, 0x0a00},           // readDECSourceProgram
        {0x0002, 0xc424, 0x00a0},           // readDECProgramFormat
        {0x0003, 0xCB26, 0x0010, 0x0000},   // writeVOLControlMasterN(0)
        {0x0002, 0xca20, 0x0732},           // writeSYSSpeakerMainLarge2
        {0x000A, 0xCE25, 0x6010, 0x0800, 0xFDFD, 0x0901, 0xFDFD,
         0xFDFD, 0x0A02, 0x0B03, 0xFDFD},   // writeENCChannelMapTo16(0,8,-3,-3,1,9,-3,-3,-3,-3,2,10,3,11,-3,-3)},
        {0x0004, 0xCC20, 0x0020, 0x010C, 0x0000}, //writeSYSChannelConfigurationRequestSurround4_1}
        {0x004A, 0xc22e, 0x0400, 0xc22e, 0x0600, 0xc22e, 0x0700,
         0xc22e, 0x0800, 0xc22e, 0x0900, 0xc22e, 0x0a00, 0xc22e,
         0x0b00, 0xc22e, 0x0c00, 0xc22e, 0x0d00, 0xc22e, 0x0e00,
         0xc22e, 0x0f00, 0xc22e, 0x1000, 0xc22e, 0x1100, 0xc22e,
         0x1300, 0xc22e, 0x1400, 0xc32e, 0x0016, 0xc32e, 0x0018,
         0xc32e, 0x001a, 0xc32e, 0x001a, 0xc32e, 0x001c, 0xc32e,
         0x001e, 0xc32e, 0x001e, 0xc32e, 0x0020, 0xc32e, 0x0022,
         0xc32e, 0x0022, 0xc32e, 0x0024, 0xc32e, 0x0026, 0xc32e,
         0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a,
         0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0xc32e,
         0x0030, 0xc32e, 0x0032, 0xc32e, 0x0034},  // readPCEControl
#else
        {0x0002, 0xC625, 0x6020},                                  // 0
        {0x0002, 0xC024, 0x0A49},                                  // 1
        {0x0002, 0xC805, 0x0D00},                                  // 2
        {0x0001, 0xC100},                                          // 3
        {0x0001, 0xDEAD},                                          // 4
        {0x0002, 0xC224, 0x0400},                                  // 5
        {0x0002, 0xCA24, 0x2E01},                                  // 6
        {0x0002, 0xC324, 0x0030},                                  // 7
        {0x0003, 0xCB20, 0x0024, 0x0000},                          // 8
        {0x0002, 0xC424, 0x0040},                                  // 9
        {0x0004, 0xCC24, 0x0038, 0x0108, 0x0000},                  // 10
        {0x0006, 0xCE34, 0x1808, 0x0000, 0x0000, 0x0000, 0x0000},  // 11
        {0x0002, 0xC509, 0x0400},                                  // 12
        {0x0002, 0xCD09, 0x0400},                                  // 13
        {0x0002, 0xC425, 0x0088},                                  // 14
        {0x0002, 0xC224, 0x0A00},                                  // 15
        {0x0002, 0xC424, 0x00A0},                                  // 16
        {0x0003, 0xCB26, 0x0010, 0x0000},                          // 17
        {0x0002, 0xCA20, 0x0732},                                  // 18
        {0x000A, 0xCE25, 0x6010, 0x0800, 0xFDFD, 0x0901, 0xFDFD,
         0xFDFD, 0x0A02, 0x0B03, 0xFDFD},                          // 19
        {0x0004, 0xCC20, 0x0020, 0x010C, 0x0000}                   // 20
#endif
};

Uint16 alphaSequenceResponse[TESTAPP_ALPHA_MAX_CNT][ALPHA_CMD_RES_MAX_SIZE] =
{
#ifdef PROFILER
        {0x0004, 0xCC25, 0x0088, 0x0000, 0x0000},
        {0x0002, 0xCA24, 0x0A00},
        {0x0004, 0xCC24, 0x00A0, 0x0000, 0x0000},
        {0x0000},
        {0x0000},
        {0x0000},
        {0x0006, 0xCE20, 0x2008, 0x010C, 0x0200,
         0x007F, 0x0000},
        {0x004A, 0xc22e, 0x0400, 0xc22e, 0x0600, 0xc22e, 0x0700,
         0xc22e, 0x0800, 0xc22e, 0x0900, 0xc22e, 0x0a00, 0xc22e,
         0x0b00, 0xc22e, 0x0c00, 0xc22e, 0x0d00, 0xc22e, 0x0e00,
         0xc22e, 0x0f00, 0xc22e, 0x1000, 0xc22e, 0x1100, 0xc22e,
         0x1300, 0xc22e, 0x1400, 0xc32e, 0x0016, 0xc32e, 0x0018,
         0xc32e, 0x001a, 0xc32e, 0x001a, 0xc32e, 0x001c, 0xc32e,
         0x001e, 0xc32e, 0x001e, 0xc32e, 0x0020, 0xc32e, 0x0022,
         0xc32e, 0x0022, 0xc32e, 0x0024, 0xc32e, 0x0026, 0xc32e,
         0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a,
         0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0xc32e,
         0x0030, 0xc32e, 0x0032, 0xc32e, 0x0034},  // readPCEControl
#else
        {0x0012, 0xCE25, 0x6020, 0xFDFD, 0xFDFD, 0xFDFD,
         0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD,
         0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD,
         0xFDFD},                                   // 0
        {0x0002, 0x0000, 0x0000},                   // 1
        {0x0002, 0xD300, 0x0002},                   // 2
        {0x0001, 0xC100},                           // 3
        {0x0001, 0xDEAD},                           // 4
        {0x0002, 0xCA24, 0x0401},                   // 5
        {0x0000},                                   // 6
        {0x0003, 0xCB24, 0x0030, 0x0000},           // 7
        {0x0000},                                   // 8
        {0x0004, 0xCC24, 0x0040, 0x0000, 0x0000},   // 9
        {0x0000},                                   // 10
        {0x0000},                                   // 11
        {0x0002, 0xCD09, 0x0400},                   // 12
        {0x0000},                                   // 13
        {0x0004, 0xCC25, 0x0088, 0x0000, 0x0000},   // 14
        {0x0002, 0xCA24, 0x0A00},                   // 15
        {0x0004, 0xCC24, 0x00A0, 0x0000, 0x0000},   // 16
        {0x0000},                                   // 17
        {0x0000},                                   // 18
        {0x0000},                                   // 19
        {0x0006, 0xCE20, 0x2008, 0x010C, 0x0200,
         0x007F, 0x0000}                            // 20
#endif
};


/************************************************************
 Function Definitions
 *************************************************************/

void dcsTsk();

int main()
{
    Task_Handle task;

    // cache configuration
    memarchcfg_cacheEnable();

    task = Task_create(dcsTsk, NULL, NULL);
    if (task == NULL)
    {
        BIOS_exit(0);
    }
    printf("\nTask started\n");
    BIOS_start(); /* does not return */
    return (0);
}

void dcsTsk()
{
    ptTestapp_Handle handle = &tTestapp_OBJ;
    Int rxIdx, alphaSeqCnt, testCount;
#ifdef DCS7_UART_EDMA_ENABLE
    EDMA3_DRV_Result edmaResult = EDMA3_DRV_E_INVALID_PARAM;
    EDMA3_RM_Handle gEdmaHandle = NULL;
    uint32_t edma3Id;
    uint32_t edmaEvent[2], i, chnMapping, chnMappingIdx;
#endif
#ifdef PROFILER
    tPrfl profilerTestCases[TESTAPP_ALPHA_MAX_CNT] = {0};
    tPrfl profiler;
    Int prfl_count = 0;

    /* Init and clear profiling variables*/
    Prfl_Init(&profiler);
#endif



#ifdef  DCS7_UART_EDMA_ENABLE
    if (handle->params->dcs7Params->dev == DCS7_PARAMS_DEV_UART0)
    {
        UART_getEdmaInfo(BOARD_UART_INSTANCE, &edma3Id, edmaEvent);
        /* Set the RX/TX ownDmaChannels and dmaChannelHwEvtMap */
        for (i = 0; i < 2; i++)
        {
            chnMapping = edmaEvent[i];
            if (chnMapping < 32)
                chnMappingIdx = 0;
            else
            {
                chnMapping -= 32;
                chnMappingIdx = 1;
            }
            sampleInstInitConfig[edma3Id][0].ownDmaChannels[chnMappingIdx] |=
                                                             (1 << chnMapping);
            sampleInstInitConfig[edma3Id][0].ownTccs[chnMappingIdx] |=
                                                             (1 << chnMapping);
            sampleInstInitConfig[edma3Id][0].ownPaRAMSets[chnMappingIdx] |=
                                                             (1 << chnMapping);
            sampleEdma3GblCfgParams[edma3Id].dmaChannelHwEvtMap[chnMappingIdx] |=
                                                             (1 << chnMapping);
        }

        gEdmaHandle = (EDMA3_RM_Handle) edma3init(edma3Id, &edmaResult);
        if (edmaResult != EDMA3_DRV_SOK)
        {
            /* Report EDMA Error */
            printf("DCS7: EDMA driver initialization FAIL");
        }
        else
        {
            printf("DCS7: EDMA driver initialization Complete");
        }
        uartInitCfg[BOARD_UART_INSTANCE].edmaHandle = gEdmaHandle;
    }
#endif

    // Initialize DCS
    if (!(handle->dcs7 = handle->dcs7Fxns->open(handle->params->dcs7Params,
                                                handle->dcs7Config,
                                                (DCS7_Fxns_Ptr)handle->dcs7Fxns,
                                                NULL)))
        return;

    handle->rxBuf =
                (Uint16*) handle->dcs7Fxns->memAlloc(handle->dcs7,
                                                     handle->pBufSeg,
                                                     handle->params->rxBufSize,
                                                     4);
    handle->txBuf =
                (Uint16*) handle->dcs7Fxns->memAlloc(handle->dcs7,
                                                     handle->pBufSeg,
                                                     handle->params->txBufSize,
                                                     4);

    testCount = 0;
    while (testCount < TESTAPP_ALPHA_MAX_CNT)
    {
        // read alpha sequence
        handle->dcs7->fxns->read(handle->dcs7,
                                 handle->rxBuf,
                                 handle->params->rxBufSize);

#ifdef PROFILER
        Prfl_Start(&profiler);
#endif
        rxIdx = 0;
        alphaSeqCnt = handle->rxBuf[rxIdx++];
        if (alphaSeqCnt && (alphaSeqCnt != SLC_MASK))
        {
            Uint32 i, alphaSeqFound;
            Uint32 alphaSeqIndex = 0;

            alphaSeqCnt++;  // All received 16 bit word count

            printf("\nread data: ");
            for (i = 0; i < alphaSeqCnt; i++)
            {
                printf("0x%x  ", handle->rxBuf[i]);
            }
            printf("\n");

            // Prepare response
            for (alphaSeqIndex = 0;
                 alphaSeqIndex < TESTAPP_ALPHA_MAX_CNT;
                 alphaSeqIndex++)
            {
                for (i = 0; i < alphaSeqCnt; i++)
                {
                    if (handle->rxBuf[i] ==
                            alphaSequenceReceived[alphaSeqIndex][i])
                    {
                        alphaSeqFound = 1;
                    }
                    else
                    {
                        alphaSeqFound = 0;
                        break;
                    }
                }
                if (alphaSeqFound == 1)
                    break;
            }

            // Response found
            if (alphaSeqFound == 1)
            {
                // get number of bytes
                Uint16 alphaResponseCnt =
                                alphaSequenceResponse[alphaSeqIndex][0] + 1;
                for (i = 0; i < alphaResponseCnt; i++)
                {
                    handle->txBuf[i] = alphaSequenceResponse[alphaSeqIndex][i];
                }
#ifdef PROFILER
                Prfl_Stop(&profiler);
#endif
                // Write response
                handle->dcs7->fxns->write(handle->dcs7,
                                          handle->txBuf,
                                          handle->params->txBufSize);
                printf("\nsend data: ");
                for (i = 0; i < alphaResponseCnt; i++)
                {
                   printf("0x%x  ", handle->txBuf[i]);
                }
                printf("\n");
            }
            else // response not found
            {
#ifdef PROFILER
                Prfl_Stop(&profiler);
#endif
                 // send back the received data, echo program
                 handle->dcs7->fxns->write(handle->dcs7,
                                           handle->rxBuf,
                                           (alphaSeqCnt) << 1 /*Byte count*/);

                printf("Response not found. \r\n");
            }
        }
#ifdef PROFILER
        // Log profile into array
        prfl_count = profiler.process_count;
        if (prfl_count >= PRFL_MAX_COUNT)
        {
            profilerTestCases[testCount].total_process_cycles =
                    profiler.total_process_cycles;
            profilerTestCases[testCount].peak_process_cycles =
                    profiler.peak_process_cycles;
            profilerTestCases[testCount].process_count =
                    profiler.process_count;

            // Clear profiling data
            Prfl_Init(&profiler);
            testCount++;
        }
#endif

    }
#ifdef PROFILER
    // write porfiling info in file
    {
        testCount = 0;
        while (testCount < TESTAPP_ALPHA_MAX_CNT)
        {
            FILE *pStdout_profile =
                    fopen("..\\..\\test_vectors\\output\\"
                          "dcs7_profile.txt", "a");
            fprintf(pStdout_profile,
                    "Total Process Cycles %llu, "
                    "Peak Process Cycles %llu "
                    "Process Count %u",
                    profilerTestCases[testCount].total_process_cycles,
                    profilerTestCases[testCount].peak_process_cycles,
                    profilerTestCases[testCount].process_count);
            fprintf(pStdout_profile, "\n****************\n");
            fclose(pStdout_profile);
            testCount++;
        }
    }
#endif
}
