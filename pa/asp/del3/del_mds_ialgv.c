/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This file contains the function table definitions for all
 *  interfaces implemented by the DEL_MDS module that derive
 *  from IALG
 *
 *  We place these tables in a separate file for two reasons:
 *     1. We want allow one to one to replace these tables
 *        with different definitions.  For example, one may
 *        want to build a system where the DEL is activated
 *        once and never deactivated, moved, or freed.
 *
 *     2. Eventually there will be a separate "system build"
 *        tool that builds these tables automatically 
 *        and if it determines that only one implementation
 *        of an API exists, "short circuits" the vtable by
 *        linking calls directly to the algorithm's functions.
 */
#include <xdc/std.h>
#include <ti/xdais/ialg.h>

#include <idel.h>
#include <del_mds.h>
#include <del_mds_priv.h>

#include <stdasp.h>

#define IALGFXNS \
    &DEL_MDS_IALG,       /* module ID */                         \
    DEL_MDS_activate,    /* activate */                          \
    DEL_MDS_alloc,       /* alloc */                             \
    DEL_MDS_control,     /* control */                           \
    DEL_MDS_deactivate,  /* deactivate */                        \
    DEL_MDS_free,        /* free */                              \
    DEL_MDS_initObj,     /* init */                              \
    DEL_MDS_moved,       /* moved */                             \
    DEL_MDS_numAlloc     /* numAlloc */                          \

/*
 *  ======== DEL_MDS_IDEL ========
 *  This structure defines MDS's implementation of the IDEL interface
 *  for the DEL_MDS module.
 */
const IDEL_Fxns DEL_MDS_IDEL = {       /* module_vendor_interface */
    IALGFXNS,
    DEL_MDS_reset,
    DEL_MDS_apply,
};

/*
 *  ======== DEL_MDS_IALG ========
 *  This structure defines MDS's implementation of the IALG interface
 *  for the DEL_MDS module.
 */
#ifdef _TMS320C6X

#ifdef __TI_EABI__
asm("DEL_MDS_IALG .set DEL_MDS_IDEL");
#else /* __TI_EABI__*/
asm("_DEL_MDS_IALG .set _DEL_MDS_IDEL");
#endif /* __TI_EABI__*/

#else

/*
 *  We duplicate the structure here to allow this code to be compiled and
 *  run non-DSP platforms at the expense of unnecessary data space
 *  consumed by the definition below.
 */
IALG_Fxns DEL_MDS_IALG = {       /* module_vendor_interface */
    IALGFXNS
};

#endif

