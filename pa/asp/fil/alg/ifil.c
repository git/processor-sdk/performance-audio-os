/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/


/**
 *  @file   ifil.c
 *
 *  @brief  IFIL default instance creation parameters
 */
/*
 *  ======== ifil.c ========
 *  IFIL default instance creation parameters
 */

#include <xdc/std.h>
#include <ifil.h>

/*
 *  ======== IFIL_PARAMS ========
 *  This static initialization defines the default parameters used to
 *  create instances of FIL objects.
 */
 
/* FIL default status */
const IFIL_Status IFIL_PARAMS_STATUS = {
    sizeof(IFIL_Status),     /* size */
    0x0,                     /* mode */
    0x0,                     /* use */                 
    0x0,                     /* mask select */ 
    0x0,                     /* mask status */ 
};

/* FIL default config */
const IFIL_Config IFIL_PARAMS_CONFIG = {
    sizeof(IFIL_Config),     /* size */
    (const PAF_FilCoef *)0,  /* Bgc filter Coefficients */
    (Void *)0,
    (IFIL_FunRec (*)[FIL_IR_IIRTAPGEN][FIL_IR_IIRCHGEN])FIL_IIRFilterRec,
    (IFIL_FunRec (*)[FIL_IR_FIRTAPGEN][FIL_IR_FIRCHGEN])FIL_FIRFilterRec,
    (IFIL_FunRec (*)[FIL_CASC_IR_SOS_DF2_CH_GEN][FIL_CASC_IR_SOS_DF2_CASC_GEN])FIL_IIR_SOS_DF2_FilterRec        
};

/* FIL default param */
const IFIL_Params IFIL_PARAMS = {
    sizeof(IFIL_Params),     /* size */  
    0,
    (IFIL_Status *)&IFIL_PARAMS_STATUS,
    (IFIL_Config *)&IFIL_PARAMS_CONFIG,
};

