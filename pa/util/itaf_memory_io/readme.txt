Memory IO Module : 
  (*) (mIO) Module facilitate usage of Memory Read and Write operations instead of C standard IO calls
  
    -> Usage of C library IO module involves more time compared to memory read and write operations.
    -> Running of ITAF involves running of multiple test cases in order to speed up test case execution fread and fwrite usage is avoided
    -> Instead of reading data at frame level, the entire input file is loaded to predefined memory location.
    -> Instead of writing data at frame level, predefined memory location is updated and output memory data is written once to a file.
    -> loadti command line arguments are used to load and write memory data to IO files ((-mlr, -msr) command line arguements) 
    
  (*) Usage:
      If C library read modules need to be replaced by memory read modules mRead need to be used instead of fread.
      Before using of mIO read modules mIO read Initialization need to carried using mReadInit function module

      /* Initialization required to use memory read modules */
      void mReadInit(FILE* fp);      
      
      If C library write modules need to be replaced by memory write modules mWrite need to be used instead of fwrite.
      Before using of mIO write modules mIO write Initialization need to carried using mWriteInit function module

      /* Initialization required to use memory write modules */
      void mWriteInit(void); 
      
      In addtion to read and write modules other supporting IO modules are also added.

      Based on the requirement more modules can be added.
    
  (*) C equivalent IO operations provided using IO module:
    
    1. fread => mRead
       /* Equivalent to  C library function fread */
       /* Instead of Reading from a file read from predefined memeory address */
       size_t mRead(void *ptr, size_t size, size_t count, FILE* fp);
    
    2. fwrite => mWrite
       /* Equivalent to  C library function fwrite */
       /* Instead of writing to file write predefined memeory address */
       size_t mWrite(void *ptr, size_t size, size_t count,FILE* fp);
    
    3. fwritepos => mWriteSetPos
       /* Equivalent to  C library function fsetpos */
       /* Instead of aligning FIlE Pointer function align memory index */
       int mWriteSetPos(FILE *fp, const fpos_t *pos);
    
    4. fgetpos => mWriteGetPos
       /* Equivalent to  C library function fgetpos */
       /* Instead of reading FIlE Pointer position reads memory address index*/
       int mWriteGetPos(FILE *fp, fpos_t *pos);
    
    5. fseek => mWriteSeek
       /* Equivalent to  C library function fgetpos */
       /* Align memory address index */
       size_t mWriteSeek(FILE* fp,  long offset, int whence);
       
  (*) Limitation:
      Currently only single read and write operation support is added, to support multiple file IO operations code can be modified.
  
    