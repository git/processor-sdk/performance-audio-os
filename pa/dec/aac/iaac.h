
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Standard AAC algorithm interface declarations
//
//
//

#ifndef IAAC_
#define IAAC_

#include <ialg.h>
#include  <std.h>
#include <xdas.h>
#include <alg.h> /* for SIO */
#include <cpl.h>
#include "icom.h"
#include "pafdec.h"

/*
 *  ======== IAAC_Obj ========
 *  Every implementation of IAAC *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IAAC_Obj {
    struct IAAC_Fxns *fxns;    /* function list: standard, public, private */
} IAAC_Obj;

/*
 *  ======== IAAC_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IAAC_Obj *IAAC_Handle;

/*
 *  ======== IAAC_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.
 */
typedef volatile struct IAAC_Status {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 AACCRCCheckMode;
    XDAS_Int8 AACHeaderMode;
    XDAS_Int8 AACCRCErrorFlag;
    XDAS_Int8 AACChannelAssignmentMode;
    XDAS_Int8 LFEDownmixInclude;
    XDAS_Int8 LFEDownmixVolume;
    XDAS_Int8 ARIBDownmix;
    XDAS_Int8 DMXForExtPseudoSurrProcessor;
	XDAS_Int8 retainPCEInfo;
    XDAS_Int8 dummy[8];
} IAAC_Status;

/*
 *  ======== IAAC_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IAAC_Config {
    Int size;
    void * pDummy; // CPL_TII_Fxns *pCPL_fxns;
} IAAC_Config;

/*
 *  ======== IAAC_Bsi ========
 *  Structure contains Bit stream information
 */
typedef struct IAAC_Bsi {
    Int size;
    XDAS_Int16 bsi[16];   /* Bit Stream Information */
    XDAS_Int32 bitRate;
    XDAS_Int16 bsi1[8];    
}IAAC_Bsi;

/*
 *  ======== IAAC_Active ========
 *  Active structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm and which must maintain value
 *  while the algorithm is running from invocation to invocation.
 */
typedef struct IAAC_Active {
    Int size;
	Int flag;
    PAF_DecodeStatus *pDecodeStatus;     
    const PAF_InpBufConfig *pInpBufConfig;    
} IAAC_Active;

/*
 *  ======== IAAC_Scratch ========
 *  Scratch structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm and which must maintain value
 *  while the algorithm is running during a single invocation.
 */
typedef struct IAAC_Scrach {
    Int size;
    XDAS_Int8 unused[4];
} IAAC_Scrach;

/*
 *  ======== IAAC_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a AAC object.
 *
 *  Every implementation of IAAC *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */
typedef struct IAAC_Params {
    Int size;
    const IAAC_Status *pStatus;
    const IAAC_Config *pConfig;
    const IAAC_Active *pActive;
    const IAAC_Scrach *pScrach;
} IAAC_Params;

/*
 *  ======== IAAC_PARAMS ========
 *  Default instance creation parameters (defined in iaac.c)
 */
extern const IAAC_Params IAAC_PARAMS;

/*
 *  ======== IAAC_Fxns ========
 *  All implementation's of AAC must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is AAC_<vendor>_IAAC, where
 *  <vendor> is the vendor name.
 */
typedef struct IAAC_FXNS
{
   /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IAAC_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);
    Int         (*info)(IAAC_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);
    Int         (*decode)(IAAC_Handle, ALG_Handle, PAF_DecodeInStruct *, PAF_DecodeOutStruct *);
} IAAC_Fxns, *IAAC_FxnsPtr;


/*
 *  ======== IAAC_Cmd ========
 *  The Cmd enumeration defines the control commands for the AAC
 *  control method.
 */
typedef enum IAAC_Cmd {
    IAAC_NULL                   = ICOM_NULL,
    IAAC_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IAAC_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IAAC_GETSTATUS              = ICOM_GETSTATUS,
    IAAC_SETSTATUS              = ICOM_SETSTATUS,
    IAAC_MINSAMGEN,
    IAAC_MININFO,
    IAAC_MAXSAMGEN,
    IAAC_RESTART,
    IAAC_FRAMETIME,
    IAAC_FRAMESIZE
} IAAC_Cmd;

#endif  /* IAAC_ */
