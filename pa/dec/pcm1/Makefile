##############################################################################
# Copyright (c) 2018, Texas Instruments Incorporated - http://www.ti.com
#   All rights reserved.
#
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions are met:
#       # Redistributions of source code must retain the above copyright
#         notice, this list of conditions and the following disclaimer.
#       # Redistributions in binary form must reproduce the above copyright
#         notice, this list of conditions and the following disclaimer in the
#         documentation and/or other materials provided with the distribution.
#       # Neither the name of Texas Instruments Incorporated nor the
#         names of its contributors may be used to endorse or promote products
#         derived from this software without specific prior written permission.
#
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
#   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
#   THE POSSIBILITY OF SUCH DAMAGE.
#############################################################################/

#
#
# PCM1 Component Makefile
#
#
# -----------------------------------------------------------------------------

# First invocation is normally from primary sources directory for the component.
# It is assumed the name of this directory doesn't match either of the standard
# target directory names (i.e. debug or release). So this first call then
# includes the target.mk file which only has one target -- the target directory.
# This is a phony target which causes a recursive call to this file every time
# but rooted in the target directory. Upon this second call the else statement
# is executed which performs the conventional make process.
ifeq (,$(filter debug release ,$(notdir $(CURDIR))))
include ../../build/target.mk
else
include ../../../../build/tools.mk

LIBNAME=pcm1_elf

# architecture independent settings
SOURCES=										\
	ipcm.c										\
	pcm.c										\
	pcm_mds_ext.c								\
	pcm_mds_ialg.c								\
	pcm_mds_ialgv.c								\
	pcm_mds_ipcm.c

# component specific includes
INCLUDES += -I../..
INCLUDES += -I"../../../../../pa/dec/pcm1/alg"
INCLUDES += -I"../../../../../pa/dec/pcm1/include"
INCLUDES += -I"../../../../../pa/sio/acp1"
INCLUDES += -"I../../../../../pa/dec/pcm1/test/include"
INCLUDES += -I"../../../../../pa/f/include"
INCLUDES += -I"../../../../../pa/f/s3"
INCLUDES += -I"../../../../../pa/f/alpha"
INCLUDES += -I"../../../../../pa/asp/std"
INCLUDES += -I"../../../../../pa/asp/com"
INCLUDES += -I"../../../../../pa/dec/com"
INCLUDES += -I"../../../../../da/psp/dat"
INCLUDES += -I"../../../../util/statusop_common"

vpath %.c $(SRCDIR)/alg 

ifeq ($(ARCH),c66x)
include ../../../../build/rules.mk
endif

ifeq ($(ARCH),a15)
#CFLAGS := $(CFLAGS:-O3=-g)
include ../../../../build/rules_a15.mk
endif

endif
