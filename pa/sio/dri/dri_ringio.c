
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.  
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// LINK RingIO interfaces for DRI
//
//
//

#include <std.h>
#include <log.h>
#include <swi.h>
#include <sys.h>
#include <sio.h>
#include <tsk.h>
#include <pool.h>
#include <gbl.h>
#include <string.h>

#include <failure.h>
#include <dsplink.h>
#include <platform.h>
#include <notify.h>

#include <ringio.h>
#include <sma_pool.h>

#include <paftyp.h>

#include "dri.h"
#include "dri_ringio.h"
#include "drierr.h"

// #define ENABLE_TRACE
#ifdef ENABLE_TRACE
  #include <logp.h>
  #include "dp.h"
  // #define TRACE(a) LOG_printf a
  #define TRACE(a) dp a
  #define LINE_END "\n"
#else
  #define TRACE(a)
#endif


// -----------------------------------------------------------------------------

inline void FillData(PAF_InpBufConfig *pBufConfig, char *sourcePtr, Uint32 copySize)
{
    Uint32 tempSize = copySize;
    Char *writePtr = (char *)pBufConfig->head.pVoid;

    TRACE((&trace, "\nDRI's FillData: sourcePtr: src 0x%x.  writePtr: 0x%x. size %d." LINE_END, sourcePtr, writePtr, copySize));

    if (((int)writePtr + tempSize) >= ((int)pBufConfig->base.pVoid + pBufConfig->sizeofBuffer))
    {
        tempSize  = ((int)pBufConfig->base.pVoid + pBufConfig->sizeofBuffer) - ((int)writePtr);
        memcpy(writePtr, sourcePtr, tempSize);
        sourcePtr = (char *)((int)sourcePtr + tempSize);
        writePtr = (char *)pBufConfig->base.pVoid;
        tempSize  = copySize - tempSize;
    }
    memcpy(writePtr, sourcePtr, tempSize);

    /* Update head of the frame value */
    pBufConfig->head.pVoid = (Ptr)((int)writePtr + tempSize); /* writePtr holds the already wrapped around address */

    return;
}


inline void FillZeros(PAF_InpBufConfig *pBufConfig, Uint32 copySize)
{
    int tempSize = copySize;
    Char *writePtr = (char *)pBufConfig->head.pVoid;

    TRACE((&trace, "DRI's FillZeros: writePtr: 0x%x. size %d." LINE_END, writePtr, copySize));

    if (((int)writePtr + tempSize) > ((int)pBufConfig->base.pVoid + pBufConfig->sizeofBuffer))
    {
        tempSize = ((int)pBufConfig->base.pVoid + pBufConfig->sizeofBuffer) - ((int)writePtr);
        memset(writePtr, 0, tempSize);
        writePtr = (char *)pBufConfig->base.pVoid;
        tempSize = copySize - tempSize;
    }
    memset(writePtr, 0, tempSize);

    /* Update head of the frame value */
    pBufConfig->head.pVoid = (Ptr)((int)writePtr + tempSize); /* writePtr holds the already wrapped around address */

    return;
}

int count_meta = 0;
int count_data = 0;
int count_errsize = 0;
int count_failure = 0;
int count_pendingdata = 0;

Int DRI_RingIO_Reclaim(DRI_DeviceExtension *pDevExt,
                       PAF_InpBufConfig *pBufConfig)

{
    Bool        semStatus = TRUE;
    Int      rdRingStatus = RINGIO_SUCCESS;
    Int            status = SYS_OK;
    Uint32  readerAcqSize = 0;
    Char      *readerBuf;
    PAD_MetaData attrs;
    Uint32 meta_size;
    Uint32 param;
    Uint16 type;
    Uint32 size = 0;

    meta_size = sizeof(attrs);
    rdRingStatus = RingIO_getvAttribute(pDevExt->RingHandle, &type, &param, (RingIO_BufPtr) & attrs, &meta_size);
    if (rdRingStatus == RINGIO_EFAILURE)
    {
        count_failure++;
    }
    if (rdRingStatus == RINGIO_EPENDINGDATA)
    {
        count_pendingdata++;
    }

    if ((RINGIO_SUCCESS == rdRingStatus) || (RINGIO_SPENDINGATTRIBUTE == rdRingStatus))
    {
        pDevExt->meta = attrs;
        count_meta++;
    }
    size = pDevExt->meta.frame_size * pDevExt->meta.sample_width;
    while (size != 0)
    {
        readerAcqSize = size;
        //    if (size != 0) {
       //readerAcqSize = size = pDevExt->meta.frame_size * pDevExt->meta.sample_width;
        rdRingStatus  = RingIO_acquire(pDevExt->RingHandle, (RingIO_BufPtr *)&readerBuf, &readerAcqSize);

//        writePtr = (char *)pBufConfig->head.pVoid;

        if ((rdRingStatus == RINGIO_EFAILURE) || (rdRingStatus == RINGIO_EBUFEMPTY))
        {

            if (pDevExt->freadEnd == TRUE)
            {
                // size contains the requisite size of data to be zero padded
                FillZeros(pBufConfig, size);

                pDevExt->firstRead = TRUE;
                pDevExt->freadEnd = FALSE;

                return SYS_OK;
            } else
            {
                // Wait for the read buffer to be available
                semStatus = SEM_pend(&(pDevExt->readerSemObj), 1000);
                if (semStatus == FALSE)
                {
                    status = RINGIO_EFAILURE;
                    SET_FAILURE_REASON(status);
                    return status;
                } else status = SYS_OK;
            }
        } else if ((rdRingStatus == RINGIO_SUCCESS) ||
                   (((readerAcqSize > 0) && ((rdRingStatus == RINGIO_ENOTCONTIGUOUSDATA) ||
                                             (rdRingStatus == RINGIO_SPENDINGATTRIBUTE)))))
        {
            count_data++;

            if (readerBuf)                 
                FillData(pBufConfig, readerBuf, readerAcqSize);

            size -= readerAcqSize;
            rdRingStatus = RingIO_release(pDevExt->RingHandle, readerAcqSize);

            if (rdRingStatus != RINGIO_SUCCESS) SET_FAILURE_REASON(rdRingStatus);
        }
        /* If RingIO_acquire returns with readerAcqSize = 0 because of the presence of Attributes then exit */
        else if ((readerAcqSize == 0) && ((rdRingStatus == RINGIO_ENOTCONTIGUOUSDATA) || (rdRingStatus == RINGIO_SPENDINGATTRIBUTE)))
        {
            count_errsize++;
            return SYS_OK;
        }
    } //while

    return status;
} //DRI_RingIO_Reclaim



// -----------------------------------------------------------------------------
