
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#ifndef CPL_FIFO_H_
#define CPL_FIFO_H_

typedef struct 
{
   void *pStore;  // fifo buffer
   int   size;    // size of the fifo in bytes
   int   r;       // read offset
   int   curSize; // current size
}CPL_Fifo;

// 
// Initialize/Reset the fifo.
// 
// Description:
// This call sets r,curSize to zero. 
// It is user's responsibility to initialize pStore and size
// before calling this function.
// 
//
int CPL_fifoInit_(CPL_Fifo * pFifo);

// 
// Returns the fifo size 
// 
int CPL_fifoSize_(const CPL_Fifo *pFifo);

// 
// return the number of bytes available for reading
// 
int CPL_fifoSizeFull_(const CPL_Fifo *pFifo);

//
// return the number of bytes available for writing
//
int CPL_fifoSizeFree_(const CPL_Fifo *pFifo);

//
// return bytes that can read linearly
//
int CPL_fifoSizeFullLinear_(const CPL_Fifo *pFifo);

//
// return bytes that can written linearly
//
int CPL_fifoSizeFreeLinear_(const CPL_Fifo *pFifo);

//
// read 'size' number of bytes into pBuf from fifo
// and return the actual number of bytes copied
//
// return value can be lessthan 'size' when bytes available are lessthan 
// the bytes requested. 
// 
int CPL_fifoRead_(CPL_Fifo * pFifo, void *pBuf, int size);

//
// write 'size' number of bytes into fifo from pBuf
// and return the actual number of bytes copied
//
// return value can be lessthan 'size' when space available writing is 
// less than the bytes requested to be written.
// 
int CPL_fifoWrite_(CPL_Fifo * pFifo, void *pBuf, int size);

//
// CPL_fifoReadNoPosMv and CPL_fifoReadDone together are alternative to
// using CPL_fifoRead API.
// Advantage being Application can retain data tail in fifo.
// 
// read 'size' number of bytes into pBuf from fifo
// and return the actual number of bytes copied.
//
// return value can be lessthan 'size' when bytes available are lessthan 
// the bytes requested.
// 
// This function does not move the read pointer i.e. if the function 
// is called again, function gives the same data.
// confirm the read using CPL_fifoReadDone.
// 
// Intended Usage is user wants to read more data but retain the tail 
// in the fifo.
// 
int CPL_fifoReadNoPosMv_(CPL_Fifo * pFifo, void *pBuf, int size);

// 
// CPL_fifoGetReadPtr and CPL_fifoReadDone together are alternative to
// using CPL_fifoRead API.
// Advantage being Application gets a pointer where the data is available
// linearly.
// 
// Get the ReadPtr (where data is avaiable linearly)
// Returns the number of bytes that can be read from '*p'
// Return value can lessthan 'size', when bytes available for reading 
// in fifo are lessthan the 'size'
// 
// NOTE: CPL_fifoGetReadPtr considers that data is not read.
//       Application has to use CPL_fifoReadDone for fifo 
//       to consider that bytes are read.
//
int CPL_fifoGetReadPtr_(CPL_Fifo * pFifo, int size, void **p);
//
// Intended to be used after CPL_fifoGetReadPtr
// 
// Considers that 'size' number of bytes are read from fifo
// Returns the number of bytes considered read.
// Return value can lessthan 'size', when bytes available for reading 
// in fifo are lessthan the 'size'
// 
// Note: can also be used a dummy reading (or skip reading) function
// 
int CPL_fifoReadDone_(CPL_Fifo * pFifo, int size);

// 
// CPL_fifoGetWritePtr and CPL_fifoWrote together are alternative to
// using CPL_fifoWrite API.
// Advantage being Application gets a pointer where the data can be written
// linearly.
// 
// Get the WritePtr (where data can be written linearly)
// Returns the number of bytes that can be written at '*p'
// Return value can lessthan 'size', when bytes available for writing 
// in fifo are lessthan the 'size'
// 
// NOTE: CPL_fifoGetWritePtr considers that data is not written.
//       Application has to use CPL_fifoWrote for fifo 
//       to consider that bytes are actually written.
//
int CPL_fifoGetWritePtr_(CPL_Fifo * pFifo, int size, void **p);

//
// Intended to be used after CPL_fifoGetWritePtr
// 
// Considers that 'size' number of bytes are written to fifo
// 
// Returns the number of bytes considered written.
// Return value can lessthan 'size', when bytes available for writting 
// in fifo are lessthan the 'size'
//
int CPL_fifoWrote_(CPL_Fifo * pFifo, int size);

//
// This function moves the read position (read index) to a specified 
// location in fifo.
// 
// This function is used by CPL_fifoGetReadPtr and CPL_fifoGetWritePtr
// to move that read index such that pointers can be made available to the
// user where data can be read or written linerly.
// 
// Algorithm:
// a) determine if left shift is closer or right shift is better
// b) check is if free space available, if not available, unwrite upto 16 
//    bytes, writeback after shift is complete.
// c) do the shifting until target is reached.
//
int CPL_fifoMoveReadLocation_(CPL_Fifo * pFifo, int location);

void CPL_rotateLeft_(unsigned char * restrict p,int circ_size, int offset, int size, int rotate);
void CPL_rotateRight_(unsigned char * restrict p,int circ_size, int offset, int size, int rotate);

#ifdef DEBUG_PRINTF
#define DBGPRNT DBG_prnt
#else
#define DBGPRNT(a,b)
#endif

// backword compatibility
#define CPL_fifoReadPtr CPL_fifoGetReadPtr 
#define CPL_fifoWritePtr CPL_fifoGetWritePtr

#endif /* CPL_FIFO_H_ */

