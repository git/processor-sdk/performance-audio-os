
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Standard AAC algorithm interface declarations
//
//
//

#ifndef AAC_TII_
#define AAC_TII_

#include <ialg.h>
#include <icom.h>
//#include <Aaclib.h>
//#include <aac_prototype.h>
#include <iaac.h>/*To be reverted once iaac.h is in placePArtha*/

/*
 *  ======== AAC_TII_exit ========
 *  Required module finalization function
 */
#define AAC_TII_exit COM_TII_exit

/*
 *  ======== AAC_TII_init ========
 *  Required module initialization function
 */
#define AAC_TII_init COM_TII_init

/*
 *  ======== AAC_TII_IALG ========
 *  MDS's implementation of AC3's IALG interface
 */
extern IALG_Fxns AAC_TII_IALG; 

/*
 *  ======== AAC_TII_IAAC ========
 *  MDS's implementation of AC3's IAAC interface
 */
extern const IAAC_Fxns AAC_TII_IAAC; 


/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of MDS's AC3 algorithm.
 *  However, other implementation specific operations can also
 *  be added.
 */

/*
 *  ======== AAC_TII_Handle ========
 */
typedef struct AAC_TII_Obj *AAC_TII_Handle;

/*
 *  ======== AAC_TII_Params ========
 *  We don't add any new parameters to the standard ones defined by IAAC.
 */
typedef IAAC_Params AAC_TII_Params;

/*
 *  ======== AAC_TII_Status ========
 *  We don't add any new status to the standard one defined by IAAC.
 */
typedef IAAC_Status AAC_TII_Status;

/*
 *  ======== AAC_TII_Config ========
 *  We don't add any new config to the standard one defined by IAAC.
 */
typedef IAAC_Config AAC_TII_Config;

/*
 *  ======== AAC_TII_Bsi ========
 *  Structure contains Bit stream information
 */
typedef IAAC_Bsi AAC_TII_Bsi;


/*
 *  ======== AAC_TII_Active ========
 *  We don't add any new active to the standard one defined by IAAC.
 */
typedef IAAC_Active AAC_TII_Active;

/*
 *  ======== AAC_TII_Scrach ========
 *  We don't add any new active to the standard one defined by IAAC.
 */
typedef IAAC_Scrach AAC_TII_Scrach;

/*
 *  ======== AAC_TII_PARAMS ========
 *  Define our default parameters.
 */
#define AAC_TII_PARAMS   IAAC_PARAMS

/*
 *  ======== AAC_TII_create ========
 *  Create a AAC_TII instance object.
 */
extern AAC_TII_Handle AAC_TII_create(const AAC_TII_Params *params);

/*
 *  ======== AAC_TII_delete ========
 *  Delete a AAC_TII instance object.
 */
#define AAC_TII_delete COM_TII_delete

#endif  /* AAC_TII_ */
