/*
* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// MDDEFORMAT ASP alpha codes
//

#ifndef _MDDEFORMAT_A
#define _MDDEFORMAT_A

// This has to be defined in cusbeta.h; cross check the definition in cusbeta.h or include that file here
#include "stdbeta.h"
#ifndef STD_BETA_MDDEFORMAT
 #define STD_BETA_MDDEFORMAT     0x88
 #warn  STD_BETA_MDDEFORMAT should be defined in stdbeta.h.
#endif

#define readMDDEFORMATMode                  0xc200+STD_BETA_MDDEFORMAT,0x0400
#define writeMDDEFORMATModeDisable          0xca00+STD_BETA_MDDEFORMAT,0x0400
#define writeMDDEFORMATModeEnable           0xca00+STD_BETA_MDDEFORMAT,0x0401

// for debug
#define readMDDEFORMATSyncMarkerRxIdx       0xc300+STD_BETA_MDDEFORMAT,0x0008
#define writeMDDEFORMATSyncMarkerRxIdxN(N)  0xcb00+STD_BETA_MDDEFORMAT,0x0008,N
#define readMDDEFORMATRxPhase               0xc300+STD_BETA_MDDEFORMAT,0x000a
#define readMDDEFORMATPhase0Idx             0xc300+STD_BETA_MDDEFORMAT,0x000c
#define readMDDEFORMATSyncd                 0xc200+STD_BETA_MDDEFORMAT,0x0e00
#define writeMDDEFORMATSyncdDisable         0xca00+STD_BETA_MDDEFORMAT,0x0e00
#define readMDDEFORMATRxFrameCount          0xc200+STD_BETA_MDDEFORMAT,0x0f00
#define writeMDDEFORMATRxFrameCountN(N)     0xca00+STD_BETA_MDDEFORMAT,0x0f00+((N)&0xff)


#endif /* _MDDEFORMAT_A */
