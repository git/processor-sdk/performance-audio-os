
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Reverse Ring IO driver
//
//
//

#include <mem.h>
#include <que.h>
#include <sem.h>
#include <sys.h>

#include "dro.h"
#include "droerr.h"
#include "dro_ringio.h"

#include <string.h>

#include <clk.h>
#include <log.h>

//grab from samrat.c
#include <paftyp.h>
#include <stdasp.h>
#include <math.h>

Int   DRO_ctrl (DEV_Handle device, Uns code, Arg Arg);
Int   DRO_idle (DEV_Handle device, Bool Flush);
Int   DRO_issue (DEV_Handle device);
Int   DRO_open (DEV_Handle device, String Name);
Int   DRO_reclaim (DEV_Handle device);
Int   DRO_reset (DEV_Handle device,PAF_OutBufConfig *pBufConfig);
Int   DRO_flush (DEV_Handle device);

// Driver function table.
DRO_Fxns DRO_FXNS = {
    NULL,              // close not used in PA/F systems
    DRO_ctrl,
    DRO_idle,
    DRO_issue,
    DRO_open,
    NULL,              // ready not used in PA/F systems
    DRO_reclaim,
    DRO_reset,
    DRO_flush
};

// macros assume pDevExt is available and pDevExt->pFxns is valid

#define DRO_FTABLE_reset(_a,_b)               (*pDevExt->pFxns->reset)(_a,_b)
#define DRO_FTABLE_flush(_a)     			  (*pDevExt->pFxns->flush)(_a)

// .............................................................................
// syncState

enum
{
    SYNC_NONE,
    SYNC_PCM_FORCED
};

// -----------------------------------------------------------------------------

Int DRO_issue (DEV_Handle device)
{
    DRO_DeviceExtension *pDevExt = (DRO_DeviceExtension *) device->object;
    Int                   status = 0,xferSize;
    DEV_Frame          *srcFrame;
    PAF_OutBufConfig      *pBufConfig;

	// need valid status pointers
    if (!pDevExt->pBufStatus || !pDevExt->pEncStatus)
        return SYS_EINVAL;

    srcFrame   = QUE_get (device->todevice);
    pBufConfig = (PAF_OutBufConfig *) srcFrame->addr;
    if (!pBufConfig)
        return SYS_EINVAL;

    QUE_put (device->fromdevice, srcFrame);

    // if first issue after open then init internal buf config view
    if (pDevExt->syncState == SYNC_NONE) {
        status = DRO_FTABLE_reset (device, pBufConfig);
        if (status)
            return status;
        pDevExt->syncState = SYNC_PCM_FORCED;
    }

	pDevExt->lengthofData    = pBufConfig->stride * pBufConfig->lengthofFrame;//Specifies the number of total samples
    pBufConfig->lengthofData = pDevExt->lengthofData;
	//xferSize size is calculated based on the number of channels, framelength and number of bytes per sample
	//xferSize is in number of bytes
	//All these parameters read from pBufConfig structure
	xferSize = pBufConfig->stride * pBufConfig->sizeofElement * pBufConfig->lengthofFrame;
     
	status = DRO_RingIO_Issue(pDevExt,pBufConfig,xferSize);
    	
    pDevExt->state = DRO_STATE_RUNNING;
    return status;
}// DRO_issue

// -----------------------------------------------------------------------------

// Although interface allows for arbitrary BufConfigs we only support 1 -- so
// we can assume the one on the todevice is the one we want

Int DRO_reclaim (DEV_Handle device)
{
    DRO_DeviceExtension *pDevExt = (DRO_DeviceExtension *) device->object;
    DEV_Frame          *sourceFrame;
   	PAF_OutBufConfig	*pBufConfig;
   	
	if (pDevExt->state == DRO_STATE_IDLE)
        return DROERR_NOTRUNNING;

    sourceFrame = (DEV_Frame *) QUE_head (device->fromdevice);

    if (sourceFrame == (DEV_Frame *) device->fromdevice)
        return DROERR_UNSPECIFIED;
    if (!sourceFrame->addr)
        return DROERR_UNSPECIFIED;

    pBufConfig = (Ptr) sourceFrame->addr;

    sourceFrame->size = sizeof (PAF_OutBufConfig);
	
    return SYS_OK;
} // DRO_reclaim

//--------------------------------------------------------------------------------
Int DRO_initStats (DEV_Handle device, float seed)
{
    DRO_DeviceExtension   *pDevExt = (DRO_DeviceExtension *) device->object;
    int iseed = (unsigned int)(seed+0.1);
        
    // allocate structure only once to avoid fragmentation
    if (!pDevExt->pStats) {
        pDevExt->pStats = MEM_alloc (device->segid, (sizeof(PAF_SIO_Stats)+3)/4*4, 4);
        if (!pDevExt->pStats)
            return SYS_EALLOC;
    }
    if(iseed == 48000)
            pDevExt->pStats->dpll.dv = 3.637978807091713e-007;
    else if(iseed == 44100)
            pDevExt->pStats->dpll.dv = 3.959703462896842e-007;
    else if(iseed == 32000)
            pDevExt->pStats->dpll.dv = 5.4569682106375695e-007;

    return SYS_OK;
} //DRO_initStats

//--------------------------------------------------------------------------------

Int DRO_ctrl (DEV_Handle device, Uns code, Arg arg)
{
    DRO_DeviceExtension		*pDevExt = (DRO_DeviceExtension *) device->object;
    int                     status = 0;
	const Dro_Params *pParams;
	
    switch (code) {
	
		case PAF_SIO_CONTROL_OPEN:
            
            pParams = (const Dro_Params *) arg;
            pDevExt->pParams = pParams;

            // extract necessary info from pParams
            if (pParams->pWriterHandle == NULL)
            {
                pDevExt->dri_dro_link = FALSE;
				//pDevExt->numChans = pParams->numChans;
                pDevExt->RRingHandle  = PA_DSPLINK_writerHandle;
            }
            else
            {
                pDevExt->dri_dro_link = TRUE;
                pDevExt->RRingHandle  = (RingIO_Handle)(*pParams->pWriterHandle);
                //pDevExt->numChans = pParams->numChans;                
            }
            // set syncState to none to force reset in first issue call after
            // open which is needed to init internal bufConfig view.
            pDevExt->syncState   = SYNC_NONE;	
						
            break;								   
            			
		case PAF_SIO_CONTROL_IDLE:

            // do nothing if not running
            if (pDevExt->state == DRO_STATE_RUNNING)
			{
                DRO_FTABLE_flush(device);
				DRO_FTABLE_reset(device,NULL);
				status = RingIO_sendNotify (pDevExt->RRingHandle, (RingIO_NotifyMsg)NOTIFY_DATA_END);
				pDevExt->state = DRO_STATE_IDLE;				
			}         
            break;
			
		case PAF_SIO_CONTROL_SET_BUFSTATUSADDR:
            pDevExt->pBufStatus = (PAF_OutBufStatus *) arg;
            break;

        case PAF_SIO_CONTROL_SET_ENCSTATUSADDR:
            pDevExt->pEncStatus = (PAF_EncodeStatus *) arg;
            break;        
        		
        case PAF_SIO_CONTROL_ENABLE_STATS:
            DRO_initStats (device, (float) arg);
            break;
        case PAF_SIO_CONTROL_GET_STATS:
            // pass pointer to local stats structure
            *((Ptr *) arg) = pDevExt->pStats;
            break;
			//PAF_SIO_CONTROL_UNMUTE
			//PAF_SIO_CONTROL_MUTE
          
        default:
            break;
    }
    return status;
} // DRO_ctrl

// -----------------------------------------------------------------------------

Int DRO_idle (DEV_Handle device, Bool flush)
{
    DRO_DeviceExtension   *pDevExt = (DRO_DeviceExtension *) device->object;
    Int                     status = 0;

    status = DRO_FTABLE_flush (device);

    status = DRO_FTABLE_reset (device,NULL);
	
    if (status)
        return status;

	//pDevExt->state = DRO_STATE_IDLE;
	
    return 0;
} // DRO_idle

// -----------------------------------------------------------------------------

Int DRO_open (DEV_Handle device, String name)
{
    DRO_DeviceExtension *pDevExt;
    DEV_Device            *entry;
    (Void)                  name; // To avoid compiler warning
    // only one frame interface supported
    if (device->nbufs != 1)
        return SYS_EALLOC;

    if ((pDevExt = MEM_alloc (device->segid, sizeof (DRO_DeviceExtension), 0)) == MEM_ILLEGAL) {
        SYS_error ("DRO MEM_alloc", SYS_EALLOC);
        return SYS_EALLOC;
    }
  
    pDevExt->pBufStatus  = NULL;
    pDevExt->pEncStatus  = NULL;
    pDevExt->pStats      = NULL;
    device->object       = (Ptr) pDevExt;
    pDevExt->device      = device;

    // use dev match to fetch function table pointer for DRO
    name = DEV_match ("/DRO", &entry);
    if (entry == NULL) {
        SYS_error ("DRO", SYS_ENODEV);
        return SYS_ENODEV;
    }
    pDevExt->pFxns     = (DRO_Fxns *) entry->fxns;
    pDevExt->syncState = SYNC_NONE;// to identify first time call
	
    return 0;
} // DRO_open

// -----------------------------------------------------------------------------
// Although this is void it is still needed since BIOS calls all DEV inits on
// startup.

Void DRO_init (Void)
{
} // DRO_init

// -----------------------------------------------------------------------------

Int DRO_reset (DEV_Handle device, PAF_OutBufConfig * pBufConfig)
{
    DRO_DeviceExtension *pDevExt = (DRO_DeviceExtension *) device->object;
    
	 if (pBufConfig) {
	 
		pBufConfig->pntr          = pBufConfig->base;
        pBufConfig->head          = pBufConfig->base;
        
        pBufConfig->lengthofData  = 0;
		pBufConfig->sizeofElement = pDevExt->pParams->sio.wordSize;
		pBufConfig->stride = pDevExt->pParams->no_channel; 
		pBufConfig->precision = pDevExt->pParams->sio.precision;//Not used
       
        pBufConfig->sizeofBuffer = pBufConfig->allocation;	
			
        // initialize internal view with external buf config
        pDevExt->bufConfig = *pBufConfig;
    }

    pDevExt->syncState     = SYNC_NONE;
	 	 
    return 0;
} //DRO_reset

Int DRO_flush (DEV_Handle device)
{
   
    while (!QUE_empty (device->todevice))
        QUE_enqueue (device->fromdevice, QUE_dequeue (device->todevice));

     while (!QUE_empty (device->fromdevice))
        QUE_enqueue (&((SIO_Handle) device)->framelist, QUE_dequeue (device->fromdevice));

     return 0;
} // DRO_flush

// -----------------------------------------------------------------------------
