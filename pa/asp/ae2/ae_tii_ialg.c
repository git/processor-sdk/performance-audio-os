
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  AE Module IALG implementation - TII's implementation of the
 *  IALG interface for the ASP Example Demonstration algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <std.h>
#include <ialg.h>

#include <iae.h>
#include <ae_tii.h>
#include <ae_tii_priv.h>

#define NTABS (   1 /* Handle  */ \
                + AE_MAX_NUMCHAN /* reverbIn[] */ \
                + AE_MAX_NUMCHAN /* reverbOut[] */ \
                + 1 /* state and coefficient memory */ \
                + AE_MAX_NUMCHAN*AE_MAX_COMB /* comb filter delays */ \
                + AE_MAX_NUMCHAN*AE_MAX_ALL_PASS /* all pass filter delays */ \
                + 4 /* Ping-pong buffers for DMA */ \
            )


/*
 *  ======== AE_TII_activate ========
 */
  /* COM_TIH_activate */

/*
 *  ======== AE_TII_alloc ========
 */
Int AE_TII_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IAE_Params *params = (Void *)algParams;
    int ndx = 0, i, j;

    /* Request memory for AE object */
    memTab[ndx].size = (sizeof(AE_TII_Obj)+3)/4*4 + (sizeof(AE_TII_Status)+3)/4*4;
    memTab[ndx].alignment = 4;
    memTab[ndx].space = IALG_SARAM;
    memTab[ndx].attrs = IALG_PERSIST;
    ++ndx;

    //
    // Request memory for scratch buffers -- Place them in IRAM
    //
    for (i = 0; i < 2*AE_MAX_NUMCHAN; ++i, ++ndx) {
        memTab[ndx].size = AE_MAX_FSIZE*sizeof(PAF_AudioData);
        memTab[ndx].alignment = 4;
        memTab[ndx].space = IALG_SARAM;
        memTab[ndx].attrs = IALG_SCRATCH;
    }
    
    //
    //  Request memory for state and coefficients memory
    //      We allocate 4 state and coefficients for all
    //      filters irrespective of what we actually need
    //      since the wastage is quite insignificant
    memTab[ndx].size = (AE_MAX_COMB + AE_MAX_ALL_PASS)*
            AE_MAX_NUMCHAN*2*4*sizeof(PAF_AudioData);
    memTab[ndx].alignment = 4;
    memTab[ndx].space = IALG_SARAM;
    memTab[ndx].attrs = IALG_PERSIST;
    ++ndx;

    //
    // Request memory for reverb buffers -- Placed in SDRAM
    //
    for (i = 0; i < AE_MAX_NUMCHAN; ++i) {
        for (j = 0; j  < params->config.numComb; ++j, ++ndx) {
            memTab[ndx].size = params->config.combLen[i][j]*sizeof(PAF_AudioData);
            memTab[ndx].alignment = 4;
            memTab[ndx].space = IALG_EXTERNAL;
            memTab[ndx].attrs = IALG_PERSIST;
        }
    }
    for (i = 0; i < AE_MAX_NUMCHAN; ++i) {
        for (j = 0; j  < params->config.numAllPass; ++j, ++ndx) {
            memTab[ndx].size = params->config.allPassLen[i][j]*sizeof(PAF_AudioData);
            memTab[ndx].alignment = 4;
            memTab[ndx].space = IALG_EXTERNAL;
            memTab[ndx].attrs = IALG_PERSIST;
        }
    }
    {
        int nsbuf;
        switch (params->config.cdlAccess) {
        case IAE_CDL_ACCESS_DIRECT:
            nsbuf = 0;
            break;
        case IAE_CDL_ACCESS_SIMPLE_DMA:
            nsbuf = 1;
            break;
        case IAE_CDL_ACCESS_CONCURRENT_DMA:
#if (PAF_DEVICE&0xFF000000) == 0xD7000000
            nsbuf = 4;
#else
            nsbuf = 2;
#endif
        }
         for (i = 0; i < nsbuf; ++i, ++ndx) {
            memTab[ndx].size = AE_MAX_FSIZE*sizeof(PAF_AudioData);
            memTab[ndx].alignment = 4;
            memTab[ndx].space = IALG_SARAM;
            memTab[ndx].attrs = IALG_SCRATCH;
        }
    }

    return ndx;
}

/*
 *  ======== AE_TII_deactivate ========
 */
void AE_TII_deactivate(IALG_Handle handle)
{
   //AE_TII_Obj *ae = (Void *)handle;
}

/*
 *  ======== AE_TII_free ========
 */
  /* COM_TIH_free */

/*
 *  ======== AE_TII_initObj ========
 */
Int AE_TII_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    AE_TII_Obj *ae = (Void *)handle;
    int ndx = 1, i, j;
    const IAE_Params *params = (Void *)algParams;
    PAF_AudioData *pMisc;

    ae->pStatus = (AE_TII_Status *)((char *)ae + (sizeof(AE_TII_Obj)+3)/4*4);

    *ae->pStatus = *params->pStatus;
    ae->config = params->config;
	
    for (i = 0; i < AE_MAX_NUMCHAN; ++i, ndx += 2) {
        ae->reverbOut[i] = memTab[ndx].base;
        ae->reverbIn[i] = memTab[ndx+1].base;
    }
    pMisc = memTab[ndx++].base;

    for (i = 0; i < AE_MAX_NUMCHAN; ++i) {
        for (j = 0; j  < params->config.numComb; ++j, ++ndx) {
            ae->combBuf[i][j].ptr = memTab[ndx].base;
            ae->combBuf[i][j].len = params->config.combLen[i][j];
            ae->combBuf[i][j].ndx = 0;
            ae->combCoef[i][j] = pMisc;
            pMisc += 4;
            ae->combState[i][j] = pMisc;
            pMisc += 4;
        }
    }
    for (i = 0; i < AE_MAX_NUMCHAN; ++i) {
        for (j = 0; j  < params->config.numAllPass; ++j, ++ndx) {
            ae->allPassBuf[i][j].ptr = memTab[ndx].base;
            ae->allPassBuf[i][j].len = params->config.allPassLen[i][j];
            ae->allPassBuf[i][j].ndx = 0;
            ae->allPassCoef[i][j] = pMisc;
            pMisc += 4;
        }
    }
    {
        int nsbuf;
        switch (params->config.cdlAccess) {
        case IAE_CDL_ACCESS_DIRECT:
            nsbuf = 0;
            break;
        case IAE_CDL_ACCESS_SIMPLE_DMA:
            nsbuf = 1;
            break;
        case IAE_CDL_ACCESS_CONCURRENT_DMA:
#if (PAF_DEVICE&0xFF000000) == 0xD7000000
            nsbuf = 4;
#else
            nsbuf = 2;
#endif
        }
        for (i = 0; i < 4; ++i, ++ndx)
            if (i < nsbuf)
                ae->pScratch[i] = memTab[ndx].base;
            else
                ae->pScratch[i] = NULL;
    }
    return (IALG_EOK);
}

/*
 *  ======== AE_TII_numAlloc========
 */

Int AE_TII_numAlloc(void)
{
    return NTABS;
}

/*
 *  ======== AE_TII_control ========
 */
  /* COM_TIH_control */

/*
 *  ======== AE_TII_moved ========
 */
  /* COM_TIH_moved */
