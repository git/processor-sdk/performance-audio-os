
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
// PAF_DEVICE_VERSION Symbol Definitions

#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)

//
// Framework Declarations
//

#include <asp1.h>
#include <stdasp.h>
#include <as1-f2-params.h>
#include <pafsio_ialg.h>
#define PAF_AST_params_fxnsPA PAF_AST_params_fxnsPA17
//
// Audio Data Representation Definitions
//
//   External declarations to patched IROM provide standard functionality.
//

/* audio frame "width" in channels */

const SmInt PAF_AST_params_numchan[2] =
{
    32,                                                     // (MID 1933) 13.1 channels
    2
};

///
// Audio Stream Processing Function Table Definition
//

const PAF_AudioFunctions PAF_AST_params_audioFrameFunctions =
{
    &PAF_ASP_dB2ToLinear,
    &PAF_ASP_channelMask,
    &PAF_ASP_programFormat,
    &PAF_ASP_sampleRateHz,
    &PAF_ASP_delay,
};

//
// Source Select Array Declarations -- algorithm keys & sio map
//
//   External declarations to patched IROM provide standard functionality.
//

const PAF_ASP_AlgKey PAF_ASP_params_decAlgKey =
{
    PAF_SOURCE_N,                                           // length
    /* Relies on the fact that ACP_SERIES_* != 0 here */
    0,                                                      // PAF_SOURCE_UNKNOWN
    0,                                                      // PAF_SOURCE_NONE
    0,                                                      // PAF_SOURCE_PASS
    PAF_ASP_ALPHACODE (STD, SNG),                           // PAF_SOURCE_SNG
    0,                                                      // PAF_SOURCE_AUTO
    0,                                                      // PAF_SOURCE_BITSTREAM
    PAF_ASP_ALPHACODE (STD, DTSHD),                         // PAF_SOURCE_DTSALL
    PAF_ASP_ALPHACODE (STD, PCM),                           // PAF_SOURCE_PCMAUTO
    PAF_ASP_ALPHACODE (STD, PCM),                           // PAF_SOURCE_PCM
    PAF_ASP_ALPHACODE (STD, PCN),                           // PAF_SOURCE_PC8 /* unused */
    PAF_ASP_ALPHACODE (STD, DDP),                           // PAF_SOURCE_AC3
    PAF_ASP_ALPHACODE (STD, DTSHD),                         // PAF_SOURCE_DTS
    PAF_ASP_ALPHACODE (STD, AAC),                           // PAF_SOURCE_AAC
    PAF_ASP_ALPHACODE (STD, MPG),                           // PAF_SOURCE_MPEG /* unused */
    PAF_ASP_ALPHACODE (STD, DTSHD),                         // PAF_SOURCE_DTS12
    PAF_ASP_ALPHACODE (STD, DTSHD),                         // PAF_SOURCE_DTS13
    PAF_ASP_ALPHACODE (STD, DTSHD),                         // PAF_SOURCE_DTS14
    PAF_ASP_ALPHACODE (STD, DTSHD),                         // PAF_SOURCE_DTS16
    0,                                                      // PAF_SOURCE_WMA9PRO
    0,                                                      // PAF_SOURCE_MP3
    0,                                                      // PAF_SOURCE_DSD1
    0,                                                      // PAF_SOURCE_DSD2
    0,                                                      // PAF_SOURCE_DSD3
    PAF_ASP_ALPHACODE (STD, DDP),                           // PAF_SOURCE_DDP
    PAF_ASP_ALPHACODE (STD, DTSHD),                         // PAF_SOURCE_DTSHD
    PAF_ASP_ALPHACODE (STD, THD),                           // PAF_SOURCE_THD
    PAF_ASP_ALPHACODE (STD, DXP),                           // PAF_SOURCE_DXP
};

const PAF_ASP_AlgKey PAF_ASP_params_encAlgKey =
{
    PAF_SOURCE_N,                                           // length
    /* Relies on the fact that ACP_SERIES_* != 0 here */
    0,                                                      // PAF_SOURCE_UNKNOWN
    0,                                                      // PAF_SOURCE_NONE
    0,                                                      // PAF_SOURCE_PASS
    0,                                                      // PAF_SOURCE_SNG
    0,                                                      // PAF_SOURCE_AUTO
    0,                                                      // PAF_SOURCE_BITSTREAM
    0,                                                      // PAF_SOURCE_DTSALL
    0,                                                      // PAF_SOURCE_PCMAUTO
    PAF_ASP_ALPHACODE (STD, PCE),                           // PAF_SOURCE_PCM
    0,                                                      // PAF_SOURCE_PC8
    0,                                                      // PAF_SOURCE_AC3
    0,                                                      // PAF_SOURCE_DTS
    0,                                                      // PAF_SOURCE_AAC
    0,                                                      // PAF_SOURCE_MPEG
    0,                                                      // PAF_SOURCE_DTS12
    0,                                                      // PAF_SOURCE_DTS13
    0,                                                      // PAF_SOURCE_DTS14
    0,                                                      // PAF_SOURCE_DTS16
    0,                                                      // PAF_SOURCE_WMA9PRO
    PAF_ASP_ALPHACODE (STD, MPE),                           // PAF_SOURCE_MP3
    0,                                                      // PAF_SOURCE_DSD1
    0,                                                      // PAF_SOURCE_DSD2
    0                                                       // PAF_SOURCE_DSD3
};

const PAF_ASP_SioMap PAF_ASP_params_decSioMap =
{
    PAF_SOURCE_N,                                           // length
    PAF_SOURCE_UNKNOWN,                                     // PAF_SOURCE_UNKNOWN -> ...
    PAF_SOURCE_NONE,                                        // PAF_SOURCE_NONE -> ...
    PAF_SOURCE_PASS,                                        // PAF_SOURCE_PASS -> ...
    PAF_SOURCE_PCM,                                         // PAF_SOURCE_SNG -> PCM
    PAF_SOURCE_AUTO,                                        // PAF_SOURCE_AUTO -> ...
    PAF_SOURCE_BITSTREAM,                                   // PAF_SOURCE_BITSTREAM -> ...
    PAF_SOURCE_DTSALL,                                      // PAF_SOURCE_UNUSED1 -> ...
    PAF_SOURCE_PCMAUTO,                                     // PAF_SOURCE_UNUSED2 -> ...
    PAF_SOURCE_PCM,                                         // PAF_SOURCE_PCM -> ...
    PAF_SOURCE_PC8,                                         // PAF_SOURCE_PC8 -> ...
    PAF_SOURCE_AC3,                                         // PAF_SOURCE_AC3 -> ...
    PAF_SOURCE_DTS,                                         // PAF_SOURCE_DTS -> ...
    PAF_SOURCE_AAC,                                         // PAF_SOURCE_AAC -> ...
    PAF_SOURCE_MPEG,                                        // PAF_SOURCE_MPEG -> ...
    PAF_SOURCE_DTS12,                                       // PAF_SOURCE_DTS12 -> ...
    PAF_SOURCE_DTS13,                                       // PAF_SOURCE_DTS13 -> ...
    PAF_SOURCE_DTS14,                                       // PAF_SOURCE_DTS14 -> ...
    PAF_SOURCE_DTS16,                                       // PAF_SOURCE_DTS16 -> ...
    PAF_SOURCE_WMA9PRO,                                     // PAF_SOURCE_WMA9PRO -> ...
    PAF_SOURCE_MP3,                                         // PAF_SOURCE_MP3 -> ...
    PAF_SOURCE_DSD1,                                        // PAF_SOURCE_DSD1 -> ...
    PAF_SOURCE_DSD2,                                        // PAF_SOURCE_DSD2 -> ...
    PAF_SOURCE_DSD3,                                        // PAF_SOURCE_DSD3 -> ...
    PAF_SOURCE_DDP,                                         // PAF_SOURCE_DDP -> ...
    PAF_SOURCE_DTSHD,                                       // PAF_SOURCE_DTSHD -> ...
    PAF_SOURCE_THD,                                         // PAF_SOURCE_THD -> ...
    PAF_SOURCE_DXP,                                         // PAF_SOURCE_DXP -> ...
};

//
// Mapping Declarations -- from *coders to *puts
//
//   External declarations to patched IROM provide standard functionality.
//
const SmInt PAF_AST_streamsFromDecodes_std[DECODEN_MAX] =
{
    0, 1, 2,
};
const SmInt PAF_AST_streamsFromEncodes_std[ENCODEN_MAX] =
{
    0, 1, 2,
};
const SmInt PAF_AST_inputsFromDecodes_std[DECODEN_MAX] =
{
    0, 1, 2,
};
extern const SmInt PAF_AST_outputsFromEncodes_std[ENCODEN_MAX];
asm ("_PAF_AST_outputsFromEncodes_std .set _PAF_AST_inputsFromDecodes_std");

//
// Setting of audio stream order
//

const SmInt PAF_AST_streamOrder[2] =
{
    0,1,
};

//
// Audio Framework Initialization Function Table Declarations
//
//   External declarations to patched IROM provide standard functionality.
//

extern const PAF_AST_Fxns PAF_AST_params_fxnsPA;

//
// Audio Framework Status Initialization Declarations
//
//   External declarations to patched IROM provide standard functionality.
//

const PAF_InpBufStatus PAF_AST_params_inpBufStatus =
{
    sizeof (PAF_InpBufStatus),                              // size
    0,                                                      // mode
    0,                                                      // sioSelect
    0x80,                                                   // lockOverride = none
    0,                                                      // unused
    PAF_SAMPLERATE_UNKNOWN,                                 // sampleRateOverride
    PAF_SAMPLERATE_NONE,                                    // sampleRateData
    PAF_SAMPLERATE_NONE,                                    // sampleRateMeasured
    PAF_SAMPLERATE_UNKNOWN,                                 // sampleRateStatus
    PAF_IEC_AUDIOMODE_UNKNOWN,                              // nonaudio
    PAF_IEC_PREEMPHASIS_UNKNOWN,                            // emphasisData
    0,                                                      // emphasisOverride
    PAF_IEC_PREEMPHASIS_NO,                                 // emphasisStatus
    0,                                                      // lock
    0,                                                      // scanAtHighSampleRateMode
    0,                                                      // zeroRun
    0,                                                      // unused2[1]
    24,                                                     // precisionDefault
    -1,                                                     // precisionDetect
    0,                                                      // precisionOverride
    0,                                                      // precisionInput
    100,                                                    // zeroRunTrigger
    100,                                                    // zeroRunRestart
    2048,                                                   // unknownTimeout
    0,                                                      // lastFrameMask
    0,                                                      // lastFrameFlag
    0,                                                      // reportDTS16AsDTSForLargeSampleRate
    0, 0                                                    // reserved
};

const PAF_DecodeStatus PAF_AST_params_decodeStatus_primary =
{
    sizeof (PAF_DecodeStatus),                              // size
    1,                                                      // mode
    0,                                                      // unused1
    0,                                                      // command.action
    0,                                                      // command.result
    PAF_SAMPLERATE_UNKNOWN,                                 // sampleRate
    PAF_SOURCE_NONE,                                        // sourceSelect
    PAF_SOURCE_UNKNOWN,                                     // sourceProgram
    PAF_SOURCE_UNKNOWN,                                     // sourceDecode
    PAF_SOURCE_DUAL_STEREO,                                 // sourceDual
    4,                                                      // sourceKaraoke: both
    0,                                                      // aspGearControl: unused
    0,                                                      // aspGearStatus: unused
    0, 0, 0, 0,												// Unused
    0, 0, 0, 0,												// Unused
    0, 0, 0, 0,												// Unused
    0, 0, 0, 0,												// Unused
    0, 0, 0, 0,												// Unused
    0,                                                      // frameCount
    0x40,                                                   // karaoka: Vocal 1 Level
    0x40,                                                   // karaoka: Vocal 1 Pan
    0x40,                                                   // karaoka: Vocal 2 Level
    0xc0,                                                   // karaoka: Vocal 2 Pan
    0x40,                                                   // karaoka: Melody Level
    0x00,                                                   // karaoka: Melody Pan
    0,                                                      // decBypass
    0,                                                      // unused
	0,                                 						// frameLength: reset later
    1,                                  					// bufferRatio: unity
    PAF_IEC_PREEMPHASIS_UNKNOWN,        					// emphasis
	0,								    					// bufferDrift
	0, 0,
	PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, PAF_CC_AUX_SURROUND4_UNKNOWN, 0, 0, 0, 0, 0,	
    // channelConfigurationRequest.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,	
    // channelConfigurationProgram.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,	
    // channelConfigurationDecode.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,	
    // channelConfigurationDownmix.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,	
    // channelConfigurationOverride.full 
    -3,                                                     // channelMap.from[0]
    -3,                                                     // channelMap.from[1]
    -3,                                                     // channelMap.from[2]
    -3,                                                     // channelMap.from[3]
    -3,                                                     // channelMap.from[4]
    -3,                                                     // channelMap.from[5]
    -3,                                                     // channelMap.from[6]
    -3,                                                     // channelMap.from[7]
    -3,                                                     // channelMap.from[8]
    -3,                                                     // channelMap.from[9]
    -3,                                                     // channelMap.from[10]
    -3,                                                     // channelMap.from[11]
    -3,                                                     // channelMap.from[12]
    -3,                                                     // channelMap.from[13]
    -3,                                                     // channelMap.from[14]
    -3,                                                     // channelMap.from[15]
    -3,                                                     // channelMap.to[0]
    -3,                                                     // channelMap.to[1]
    -3,                                                     // channelMap.to[2]
    -3,                                                     // channelMap.to[3]
    -3,                                                     // channelMap.to[4]
    -3,                                                     // channelMap.to[5]
    -3,                                                     // channelMap.to[6]
    -3,                                                     // channelMap.to[7]
    -3,                                                     // channelMap.to[8]
    -3,                                                     // channelMap.to[9]
    -3,                                                     // channelMap.to[10]
    -3,                                                     // channelMap.to[11]
    -3,                                                     // channelMap.to[12]
    -3,                                                     // channelMap.to[13]
    -3,                                                     // channelMap.to[14]
    -3,                                                     // channelMap.to[15]
    -3,                                                     // channelMap.from[0]
    -3,                                                     // channelMap.from[1]
    -3,                                                     // channelMap.from[2]
    -3,                                                     // channelMap.from[3]
    -3,                                                     // channelMap.from[4]
    -3,                                                     // channelMap.from[5]
    -3,                                                     // channelMap.from[6]
    -3,                                                     // channelMap.from[7]
    -3,                                                     // channelMap.from[8]
    -3,                                                     // channelMap.from[9]
    -3,                                                     // channelMap.from[10]
    -3,                                                     // channelMap.from[11]
    -3,                                                     // channelMap.from[12]
    -3,                                                     // channelMap.from[13]
    -3,                                                     // channelMap.from[14]
    -3,                                                     // channelMap.from[15]
    -3,                                                     // channelMap.to[0]
    -3,                                                     // channelMap.to[1]
    -3,                                                     // channelMap.to[2]
    -3,                                                     // channelMap.to[3]
    -3,                                                     // channelMap.to[4]
    -3,                                                     // channelMap.to[5]
    -3,                                                     // channelMap.to[6]
    -3,                                                     // channelMap.to[7]
    -3,                                                     // channelMap.to[8]
    -3,                                                     // channelMap.to[9]
    -3,                                                     // channelMap.to[10]
    -3,                                                     // channelMap.to[11]
    -3,                                                     // channelMap.to[12]
    -3,                                                     // channelMap.to[13]
    -3,                                                     // channelMap.to[14]
    -3,                                                     // channelMap.to[15]
    0,                                                      // programFormat.mask
    0,                                                      // programFormat.form
   
};
const PAF_DecodeStatus *const PAF_AST_params_decodeStatus[] =
{
    &PAF_AST_params_decodeStatus_primary,
};
const PAF_OutBufStatus PAF_AST_params_outBufStatus =
{
    sizeof (PAF_OutBufStatus),                              // size
    1,                                                      // mode
    0,                                                      // sioSelect
    PAF_SAMPLERATE_UNKNOWN,                                 // sampleRate
    0,                                                      // audio
    PAF_OB_CLOCK_INTERNAL,                                  // clock
    PAF_OB_FLUSH_ENABLE,                                    // flush
    0,                                                      // unused[2]
    0,                                                      // PAF_OB_MARKER_ENABLED
};

const PAF_EncodeStatus PAF_AST_params_encodeStatus_primary =
{
    sizeof (PAF_EncodeStatus),                              // size
    1,                                                      // mode
    0,                                                      // unused1
    PAF_SAMPLERATE_UNKNOWN,                                 // sampleRate
    0,                                                      // channelCount
    0, 0, 0, 0,												//Unused                                        
    0, 0, 0, 0,												//Unused
    0, 0, 0, 0, 											//Unused
    0, 0, 0, 0,												//Unused
    0, 0, 0, 0, 											//Unused
    0,                                                      // frameLength
    0,                                                      // encBypass
    PAF_SOURCE_PCM,                                         // select
	PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, PAF_CC_AUX_SURROUND4_UNKNOWN, 0,0,0,0,0,
    // channelConfigurationRequest.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,0,0,0,0,
    // channelConfigurationStream.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,0,0,0,0,
    // channelConfigurationEncode.full
    0, 0, 0, 0,	0, 0, 0, 0, 								//Unused
    -3,                                                     // channelMap.from[0]
    -3,                                                     // channelMap.from[1]
    -3,                                                     // channelMap.from[2]
    -3,                                                     // channelMap.from[3]
    -3,                                                     // channelMap.from[4]
    -3,                                                     // channelMap.from[5]
    -3,                                                     // channelMap.from[6]
    -3,                                                     // channelMap.from[7]
    -3,                                                     // channelMap.from[8]
    -3,                                                     // channelMap.from[9]
    -3,                                                     // channelMap.from[10]
    -3,                                                     // channelMap.from[11]
    -3,                                                     // channelMap.from[12]
    -3,                                                     // channelMap.from[13]
    -3,                                                     // channelMap.from[14]
    -3,                                                     // channelMap.from[15]
    -3,                                                     // channelMap.to[0]
    -3,                                                     // channelMap.to[1]
    -3,                                                     // channelMap.to[2]
    -3,                                                     // channelMap.to[3]
    -3,                                                     // channelMap.to[4]
    -3,                                                     // channelMap.to[5]
    -3,                                                     // channelMap.to[6]
    -3,                                                     // channelMap.to[7]
    -3,                                                     // channelMap.to[8]
    -3,                                                     // channelMap.to[9]
    -3,                                                     // channelMap.to[10]
    -3,                                                     // channelMap.to[11]
    -3,                                                     // channelMap.to[12]
    -3,                                                     // channelMap.to[13]
    -3,                                                     // channelMap.to[14]
    -3,                                                     // channelMap.to[15]
    -3,                                                     // channelMap.from[0]
    -3,                                                     // channelMap.from[1]
    -3,                                                     // channelMap.from[2]
    -3,                                                     // channelMap.from[3]
    -3,                                                     // channelMap.from[4]
    -3,                                                     // channelMap.from[5]
    -3,                                                     // channelMap.from[6]
    -3,                                                     // channelMap.from[7]
    -3,                                                     // channelMap.from[8]
    -3,                                                     // channelMap.from[9]
    -3,                                                     // channelMap.from[10]
    -3,                                                     // channelMap.from[11]
    -3,                                                     // channelMap.from[12]
    -3,                                                     // channelMap.from[13]
    -3,                                                     // channelMap.from[14]
    -3,                                                     // channelMap.from[15]
    -3,                                                     // channelMap.to[0]
    -3,                                                     // channelMap.to[1]
    -3,                                                     // channelMap.to[2]
    -3,                                                     // channelMap.to[3]
    -3,                                                     // channelMap.to[4]
    -3,                                                     // channelMap.to[5]
    -3,                                                     // channelMap.to[6]
    -3,                                                     // channelMap.to[7]
    -3,                                                     // channelMap.to[8]
    -3,                                                     // channelMap.to[9]
    -3,                                                     // channelMap.to[10]
    -3,                                                     // channelMap.to[11]
    -3,                                                     // channelMap.to[12]
    -3,                                                     // channelMap.to[13]
    -3,                                                     // channelMap.to[14]
    -3,                                                     // channelMap.to[15]
    0,                                  					// programFormat.mask
    0,                                  					// programFormat.form
    0,                                                      // sampleProcess[0]
};

const PAF_EncodeStatus PAF_AST_params_encodeStatus_secondary =
{
    sizeof (PAF_EncodeStatus),          // size
    1,                                  // mode
    0,                                  // unused1
    PAF_SAMPLERATE_UNKNOWN,             // sampleRate
    0,                                  // channelCount
    0, 0, 0, 0,							//Unused                                        
    0, 0, 0, 0,							//Unused
    0, 0, 0, 0, 						//Unused
    0, 0, 0, 0,							//Unused
    0, 0, 0, 0, 						//Unused
    0,                                  // frameLength
    0,                                  // encBypass
    PAF_SOURCE_PCM,                     // select
    PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, PAF_CC_AUX_STEREO_STEREO, 0,0,0,0,0,
                                        // channelConfigurationRequest.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,0,0,0,0,
                                        // channelConfigurationStream.full
    PAF_CC_SAT_UNKNOWN, PAF_CC_SUB_ZERO, 0, 0,0,0,0,0,
                                        // channelConfigurationEncode.full
    0, 0, 0, 0,	0, 0, 0, 0, 			//Unused
    PAF_LEFT,                           // channelMap.from[0]
    PAF_RGHT,                           // channelMap.from[1]
    -3,                                 // channelMap.from[2]
    -3,                                 // channelMap.from[3]
    -3,                                 // channelMap.from[4]
    -3,                                 // channelMap.from[5]
    -3,                                 // channelMap.from[6]
    -3,                                 // channelMap.from[7]
    -3,                                 // channelMap.from[8]
    -3,                                 // channelMap.from[9]
    -3,                                 // channelMap.from[10]
    -3,                                 // channelMap.from[11]
    -3,                                 // channelMap.from[12]
    -3,                                 // channelMap.from[13]
    -3,                                 // channelMap.from[14]
    -3,                                 // channelMap.from[15]
    -3,                           // channelMap.from[0]
        -3,                           // channelMap.from[1]
        -3,                                 // channelMap.from[2]
        -3,                                 // channelMap.from[3]
        -3,                                 // channelMap.from[4]
        -3,                                 // channelMap.from[5]
        -3,                                 // channelMap.from[6]
        -3,                                 // channelMap.from[7]
        -3,                                 // channelMap.from[8]
        -3,                                 // channelMap.from[9]
        -3,                                 // channelMap.from[10]
        -3,                                 // channelMap.from[11]
        -3,                                 // channelMap.from[12]
        -3,                                 // channelMap.from[13]
        -3,                                 // channelMap.from[14]
        -3,                                 // channelMap.from[15]
    0,                                  // channelMap.to[0]
    1,                                  // channelMap.to[1]
    2,                                  // channelMap.to[2]
    3,                                  // channelMap.to[3]
    4,                                  // channelMap.to[4]
    5,                                  // channelMap.to[5]
    6,                                  // channelMap.to[6]
    7,                                  // channelMap.to[7]
    8,                                  // channelMap.to[8]
    9,                                  // channelMap.to[9]
    10,                                 // channelMap.to[10]
    11,                                 // channelMap.to[11]
    12,                                 // channelMap.to[12]
    13,                                 // channelMap.to[13]
    14,                                 // channelMap.to[14]
    15,                                 // channelMap.to[15]
    -3,                           // channelMap.from[0]
        -3,                           // channelMap.from[1]
        -3,                                 // channelMap.from[2]
        -3,                                 // channelMap.from[3]
        -3,                                 // channelMap.from[4]
        -3,                                 // channelMap.from[5]
        -3,                                 // channelMap.from[6]
        -3,                                 // channelMap.from[7]
        -3,                                 // channelMap.from[8]
        -3,                                 // channelMap.from[9]
        -3,                                 // channelMap.from[10]
        -3,                                 // channelMap.from[11]
        -3,                                 // channelMap.from[12]
        -3,                                 // channelMap.from[13]
        -3,                                 // channelMap.from[14]
        -3,                                 // channelMap.from[15]
        0,                                  // programFormat.mask
        0,                                  // programFormat.form
    0,                                  // sampleProcess[0]
};


const PAF_EncodeStatus *const PAF_AST_params_encodeStatus[] =
{
    &PAF_AST_params_encodeStatus_primary,
    &PAF_AST_params_encodeStatus_secondary,
};

const PAF_VolumeStatus PAF_AST_params_volumeStatus =
{
    sizeof (PAF_VolumeStatus),                              // size
    1,                                                      // mode
    PAF_MAXNUMCHAN,                                         // channelCount
    0x0f,                                                   // implementation
    0,                                                      // unused1
    50,                                                     // rampTime: 50 msec/dB (20 dB/sec)
    0,                                                      // unused2
    0,                                                      // unused3
    -2 * 20, 0, 0, 0,                                       // master
    -2 * 0, 0, 0, 0,                                        // trim
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //  
    -2 * 0, 0, 0, 0,                                        // trim - upper16
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //
    -2 * 0, 0, 0, 0,                                        //

};

//
// Common Space Parameter Declarations and Definitions
//
//   Local definitions in RAM provide non-standard functionality.
//   The NULL pointer provides standard functionality.
//

/* baseline definition - NULL equivalent */
/* May be used for overrides of IALG_MemSpace */

static const IALG_MemSpace params_memspace_PAy[] = {
    PAF_IALG_NONE,                                          // Scratch
    PAF_IALG_NONE,                                          // Persistant
    PAF_IALG_NONE,                                          // Write once
    PAF_IALG_NONE,                                          // Common 1
    PAF_IALG_NONE,                                          // Common 2
    PAF_IALG_NONE,                                          // Common 3
    PAF_IALG_NONE,                                          // Common 4
    PAF_IALG_NONE,                                          // Common 5
    PAF_IALG_NONE,                                          // Common 6
    PAF_IALG_NONE,                                          // Common 7
    IALG_EXTERNAL,                                          // Common 8
    PAF_IALG_NONE,                                          // Common 9
    PAF_IALG_NONE,                                          // Common 10
    PAF_IALG_NONE,                                          // Common 11
    PAF_IALG_NONE,                                          // Common 12
    PAF_IALG_NONE,                                          // Common 13
    PAF_IALG_NONE,                                          // Common 14
    PAF_IALG_NONE,                                          // Common 15
};

//
// Heap Declarations
//

#include <pafhjt.h>

extern int IRAM;
extern int SDRAM;
extern int L3RAM;

// .............................................................................
// DIB memory requirements

// primary input
const IALG_MemRec inpMemTabPrimary[] =
{
    // SDRAM circular buffer
    // 2 buffers * 256 samples/buffer * 8 words/sample * 3 bytes/word
    {
     180 * 1024,                                            // size
     128,                                                   // alignment
     IALG_EXTERNAL,                                         // space
     IALG_PERSIST,                                          // attrs
     NULL,                                                  // base
     },

    // IRAM scratch memory for autodetection and stereo PCM input
    // High watermark needs are set by the latter:
    // double buffer stereo 32bit PCM input 512 max frame size
    // 1 buffers * 512 samples/buffer * 2 words/sample * 4 bytes/word
    {
     8 * 1024,                                              // size
     128,                                                   // alignment
     IALG_SARAM,                                            // space
     IALG_SCRATCH,                                          // attrs
     NULL,                                                  // base
     }
};

const PAF_SIO_IALG_Params inpSioAlgParamsPrimary =
{
    2,
    inpMemTabPrimary
};

const PAF_ASP_LinkInit inpLinkInitPrimary[] =
{
    PAF_ASP_LINKINITPARAMS (STD, IB, TIH, &inpSioAlgParamsPrimary),
    PAF_ASP_LINKNONE
};

const PAF_ASP_LinkInit *const inpLinkInit[] =
{
    inpLinkInitPrimary
};

// .............................................................................
// DOB memory requirements

// primary output
const IALG_MemRec outMemTabPrimary[] =
{
    // SDRAM buffer
    // 2 buffers * 256 samples/buffer * 8 words/sample * 3 bytes/word
    {
     24576 * 4 / 3,                                         // size: note:
     // if writeOBMarkerModeDisable, then 24576 may be used
     // if writeOBMarderModeEnable, then 32768 must be used
     128,                                                   // alignment
     IALG_EXTERNAL,                                           // space
     IALG_PERSIST,                                          // attrs
     NULL,                                                  // base
     }
};


const IALG_MemRec outMemTabSecondary[] =
{
    // IRAM buffer
    //      2 buffers * 256 samples/buffer * 8 words/sample * 3 bytes/word
    {
        4096*2,           // size
        128,            // alignment
        IALG_EXTERNAL,    // space
        IALG_PERSIST,   // attrs
        NULL,           // base
    }
};

const PAF_SIO_IALG_Params outSioAlgParamsPrimary =
{
    1,
    outMemTabPrimary
};

const PAF_SIO_IALG_Params outSioAlgParamsSecondary =
{
    1,
    outMemTabSecondary
};

const PAF_ASP_LinkInit outLinkInitPrimary[] =
{
    PAF_ASP_LINKINITPARAMS (STD, OB, TIH, &outSioAlgParamsPrimary),
    PAF_ASP_LINKNONE
};
const PAF_ASP_LinkInit outLinkInitSecondary[] = 
{
    PAF_ASP_LINKINITPARAMS (STD,OB,TIH,&outSioAlgParamsSecondary),
    PAF_ASP_LINKNONE
};

const PAF_ASP_LinkInit * const outLinkInit[] =
{
    outLinkInitPrimary,
    outLinkInitSecondary
};


// .............................................................................
// sourceProgram mapped to DOB num of buffers
const PAF_ASP_outNumBufMap outPrimaryNumBufMap =
{
    4,                                                      // maxNumBuf
    PAF_SOURCE_N,                                           // length
    0,                                                      // PAF_SOURCE_UNKNOWN
    0,                                                      // PAF_SOURCE_NONE
    2,                                                      // PAF_SOURCE_PASS
    2,                                                      // PAF_SOURCE_SNG
    0,                                                      // PAF_SOURCE_AUTO
    0,                                                      // PAF_SOURCE_BITSTREAM
    0,                                                      // PAF_SOURCE_DTSALL
    0,                                                      // PAF_SOURCE_PCMAUTO
    2,                                                      // PAF_SOURCE_PCM
    0,                                                      // PAF_SOURCE_PC8
    2,                                                      // PAF_SOURCE_AC3
    2,                                                      // PAF_SOURCE_DTS
    2,                                                      // PAF_SOURCE_AAC
    0,                                                      // PAF_SOURCE_MPEG
    2,                                                      // PAF_SOURCE_DTS12
    2,                                                      // PAF_SOURCE_DTS13
    2,                                                      // PAF_SOURCE_DTS14
    2,                                                      // PAF_SOURCE_DTS16
    0,                                                      // PAF_SOURCE_WMA9PRO
    0,                                                      // PAF_SOURCE_MP3
    2,                                                      // PAF_SOURCE_DSD1,
    2,                                                      // PAF_SOURCE_DSD2,
    2,                                                      // PAF_SOURCE_DSD3,
    0,                 // PAF_SOURCE_DDP
    0,                 // PAF_SOURCE_DTSHD
    0,                 // PAF_SOURCE_THD
    0,                 // PAF_SOURCE_DXP

};

const PAF_ASP_outNumBufMap *const outNumBufMap[] =
{
	&outPrimaryNumBufMap,
    &outPrimaryNumBufMap,
};

// .............................................................................
const PAF_MetadataBufStatus PAF_AST_params_MetadataBufStatus[] =
{
	4 * 1024,			// bufSize
	PAF_MAX_NUM_PRIVATE_MD,					// NumBuf
	128,				// alignment
	&IRAM,				// pSpace
};
//
//
// This structure defines the memory allocation of audio frame buffers (channel buffers). Users can customize this structure based
// on their requirement. IALG_SARAM specifies the audio frame buffer is allocated in IRAM and IALG_EXTERNAL
// specifies the audio frame buffer is allocated in SDRAM. By allocating few channel buffers in SDRAM, users can save internal memory
// but trading performance
// This structure defined for PAF_MAXNUMCHAN number of channels. But channel buffers will be allocated  for "PAF_AST_params_numchan" 
// number of channels only.

const PAF_AudioFrameBufStatus	PAF_AST_params_AudioFrameBufStatus[PAF_MAXNUMCHAN] =
{
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_SARAM,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL,
	IALG_EXTERNAL
};
//
// Audio Stream Parameter Definitions
//
//   Global definition in RAM provides standard & non-standard functionality.
//

const PAF_AST_Params params_PAy =
{
    &PAF_AST_params_fxnsPA,                               // fxns
    {                                                       // zone
     0,                                                     // master
     1,                                                     // inputs
     0,                                                     // input1
     1,                                                     // inputN
     1,                                                     // decodes
     0,                                                     // decode1
     1,                                                     // decodeN
     2,                                                     // streams
     0,                                                     // stream1 /* unused */
     2,                                                     // streamN /* unused */
     2,                                                     // encodes
     0,                                                     // encode1
     2,                                                     // encodeN
     2,                                                     // outputs
     0,                                                     // output1
     2,                                                     // outputN
     },
    PAF_AST_inputsFromDecodes_std,
    PAF_AST_outputsFromEncodes_std,
    {                                                       // heap
     &IRAM,                                                 // pIntern
     &SDRAM,                                                // pExtern
     &IRAM,                                                 // pInpbuf
     &IRAM,                                                 // pOutbuf
     &IRAM,                                                 // pFrmbuf
     &L3RAM,                                                // pIntern1
     1,                                                     // clear
     },
    {                                                       // common
     params_memspace_PAy,                                 // space
     },
    NULL,                                                   // z_rx_bufsiz
    NULL,                                                   // z_tx_bufsiz
    PAF_AST_params_numchan,                                 // z_numchan
    256,                                                    // framelength
    &PAF_AST_params_audioFrameFunctions,                    // pAudioFrameFunctions
    &PAF_ASP_chainFxns,                                     // pChainFxns
    &PAF_AST_params_inpBufStatus,                           // pInpBufStatus
    PAF_AST_params_decodeStatus,                            // z_pDecodeStatus
    &PAF_AST_params_outBufStatus,                           // pOutBufStatus
    PAF_AST_params_encodeStatus,                            // z_pEncodeStatus
    &PAF_AST_params_volumeStatus,                           // pVolumeStatus
    &PAF_ASP_params_decAlgKey,                              // pDecAlgKey
    &PAF_ASP_params_encAlgKey,                              // pEncAlgKey
    &PAF_ASP_params_decSioMap,                              // pDecSioMap
    PAF_AST_streamsFromDecodes_std,                         // streamsFromDecodes
    PAF_AST_streamsFromEncodes_std,                         // streamsFromEncodes
    256,                                                    // maxFramelength
    PAF_AST_streamOrder,                                    // streamOrder
    inpLinkInit,                                            // i_inpLinkInit
    outLinkInit,                                            // i_outLinkInit
    outNumBufMap,                                           // outNumBufMap
    PAF_AST_params_MetadataBufStatus,						//metadata buffer status
	PAF_AST_params_AudioFrameBufStatus						//Audio frame buffer status
};

// EOF
