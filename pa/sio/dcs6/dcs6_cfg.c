
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DA8xx SPI/I2C slave control status (DCS6) implementation
//
//

#include <dcs6.h>

const Uint8 UART_STRINGS[]={
    17,'S','1','0','5','0','0','0','0','0','0','C','9','F','C','\r','\n',
    17,'S','1','0','5','0','0','0','0','0','0','C','9','F','C','\r','\n',
    13,'S','C','0','3','8','0','0','1','7','B','\r','\n',
    13,'S','S','0','3','F','0','0','1','0','B','\r','\n',
    13,'S','C','0','3','8','1','0','1','7','A','\r','\n',
    13,'S','S','0','3','F','0','0','1','0','B','\r','\n',
    13,'S','0','0','3','0','0','0','0','F','C','\r','\n',
    13,'S','0','0','3','0','0','0','0','F','C','\r','\n',
    13,'S','9','0','3','0','0','0','0','F','C','\r','\n',
    13,'S','9','0','3','0','0','0','0','F','C','\r','\n',
    0,
    3,'\r','\n',
};

const DCS6_Config_Dmax DCS6_CONFIG_DMAX={
    sizeof(DCS6_Config_Dmax),
    dMAX_INT1,
    dMAX_TCCANY,
    dMAX_TCCANY,
    0,
};

#ifdef BIOS6_NONLEGACY
extern HeapMem_Handle SDRAMHeap;
#else
extern Int SDRAM;
extern Int IRAM;
#endif
const DCS6_Config DCS6_CONFIG={
    sizeof(DCS6_Config),
#ifdef BIOS6_NONLEGACY
    (void*)&SDRAMHeap,
    (void*)&SDRAMHeap,
#else
    (void*)&IRAM,
    (void*)&SDRAM,
#endif
    DCS6_CONFIG_CACHEMODE_ENABLE,
    0,
    128,
    (DCS6_Config_Dmax*)&DCS6_CONFIG_DMAX,
};

extern void DCS6_log(DCS6_Handle,void*,char*);
extern void DCS6_log1(DCS6_Handle,void*,char*,Int32);
extern void* DCS6_semAlloc(DCS6_Handle);
extern Uint32 DCS6_semPend(DCS6_Handle,void*);
extern void DCS6_semPost(DCS6_Handle,void*);
extern void* DCS6_memAlloc(DCS6_Handle,void*,Uint32,Uint32);
extern void DCS6_taskDisable(DCS6_Handle);
extern void DCS6_taskEnable(DCS6_Handle);
extern void DCS6_configureInt(DCS6_Handle);
extern void DCS6_cacheInv(DCS6_Handle,void*,Uint32);
extern void DCS6_cacheWbInv(DCS6_Handle,void*,Uint32);
extern void DCS6_errorRecover(DCS6_Handle);
extern void DCS6_slc(DCS6_Handle);
extern Uint32 DCS6_read(DCS6_Handle,Uint16*,Uint16);
extern Uint32 DCS6_write(DCS6_Handle,Uint16*,Uint16);
extern void DCS6_isr(UArg);
extern void DCS6_configuredMAX(DCS6_Handle);
extern void DCS6_configureSPI(DCS6_Handle);
extern void DCS6_configureI2C(DCS6_Handle);
extern void DCS6_configureUART(DCS6_Handle);
extern void DCS6_configure(DCS6_Handle);
extern void DCS6_resetSPI(DCS6_Handle);
extern void DCS6_resetI2C(DCS6_Handle);
extern void DCS6_resetUART(DCS6_Handle);
extern void DCS6_reset(DCS6_Handle);
extern Uint32 DCS6_resAllocdMAX(DCS6_Handle);
extern Uint32 DCS6_resAllocSPI(DCS6_Handle);
extern Uint32 DCS6_resAllocI2C(DCS6_Handle);
extern Uint32 DCS6_resAllocUART(DCS6_Handle);
extern Uint32 DCS6_resAlloc(DCS6_Handle);
extern DCS6_Handle DCS6_open(const DCS6_Params*,const DCS6_Config*,const DCS6_Fxns_Ptr,dMAX_Handle,ACP_Handle);
const DCS6_Fxns DCS6_FXNS={
    DCS6_log,
    DCS6_log1,
    DCS6_semAlloc,
    DCS6_semPend,
    DCS6_semPost,
    DCS6_memAlloc,
    DCS6_taskDisable,
    DCS6_taskEnable,
    DCS6_configureInt,
    DCS6_cacheInv,
    DCS6_cacheWbInv,
    DCS6_errorInit,
    DCS6_errorAssert,
    DCS6_errorDeassert,
    DCS6_errorRecover,
    DCS6_slc,
    DCS6_read,
    DCS6_write,
    DCS6_isr,
    DCS6_configuredMAX,
    DCS6_configureSPI,
    DCS6_configureI2C,
    DCS6_configureUART,
    DCS6_configure,
    DCS6_resetSPI,
    DCS6_resetI2C,
    DCS6_resetUART,
    DCS6_reset,
    DCS6_resAllocdMAX,
    DCS6_resAllocSPI,
    DCS6_resAllocI2C,
    DCS6_resAllocUART,
    DCS6_resAlloc,
    DCS6_open,
};

