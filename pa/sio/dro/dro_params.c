
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/



#include "dro_params.h"

 /* Reverse Ring IO 16 bit output */
const Dro_Params DRO_RINGIO_16bit = {
	sizeof (Dro_Params),									/* size */
	"/DRO",													/* name */
	0,														/* Channel Number (moduleNum) */
	NULL,													/* pConfig (unused) */
	2,														/* wordSize (in bytes) */
	16,														/* precision  */
	NULL,
	2,														/* number of channel  */
    NULL,													/* RingIO handle, Handle needs to be passed in DSP-DSP usecase */
    {0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3}			/* Channel Mask*/
};

/* Reverse Ring IO 24 bit output */
const Dro_Params DRO_RINGIO_24bit = {
	sizeof (Dro_Params),									/* size */
	"/DRO",													/* name */
	0,														/* Channel Number (moduleNum) */
	NULL,													/* pConfig (unused) */
	3,														/* wordSize (in bytes) */
	24,														/* precision  */
	NULL,
	2,														/* number of channel  */
    NULL,													/* RingIO handle, Handle needs to be passed in DSP-DSP usecase */
    {0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3}			/* Channel Mask*/
};

/* Reverse Ring IO 32 bit output */
const Dro_Params DRO_RINGIO_32bit = {
	sizeof (Dro_Params),									/* size */
	"/DRO",													/* name */
	0,														/* Channel Number (moduleNum) */
	NULL,													/* pConfig (unused) */
	4,														/* wordSize (in bytes) */
	32,														/* precision  */
	NULL,
	2,														/* number of channel  */
	NULL,													/* RingIO handle, Handle needs to be passed in DSP-DSP usecase */
    {0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3}			/* Channel Mask*/
};
