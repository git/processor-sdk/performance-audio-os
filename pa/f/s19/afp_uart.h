
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/



#ifndef _AFP_UART
#define _AFP_UART    1
#ifdef BIOS6_NONLEGACY
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/IHeap.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/ipc/Semaphore.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/heaps/HeapMem.h>
#else
#include <std.h>
#include <hwi.h>
#include <tsk.h>
#include <c64.h>
#include <sem.h>
#include <log.h>
#endif

#include <acp.h>

#include <primus.h>

#define AFPUART_INT_UART0    38
#define AFPUART_INT_UART1    46
#define AFPUART_INT_UART2    69

#define AFPUART_XON    0x11
#define AFPUART_XOFF   0x13

#define AFPUART_DEV_UART0    0
#define AFPUART_DEV_UART1    1 
#define AFPUART_DEV_UART2    2
#define AFPUART_RXFIFTL_1    CSL_UART_FCR_RXFIFTL_CHAR1
#define AFPUART_RXFIFTL_4    CSL_UART_FCR_RXFIFTL_CHAR4
#define AFPUART_RXFIFTL_8    CSL_UART_FCR_RXFIFTL_CHAR8
#define AFPUART_RXFIFTL_14   CSL_UART_FCR_RXFIFTL_CHAR14
#define AFPUART_DIV_B2400    3906
#define AFPUART_DIV_B4800    1953
#define AFPUART_DIV_B9600    977
#define AFPUART_DIV_B19200   488
#define AFPUART_DIV_B38400   244
#define AFPUART_DIV_B56000   167
#define AFPUART_DIV_B128000  73
typedef struct AFPUart_Obj *AFPUart_Handle;
typedef struct AFPUart_Obj{
    Uint32 size;
    Uint8 dev;
    Uint8 rxfiftl;
    Uint16 div;
    Uint16 rxSize;
    Uint16 txSize;
    void *pBufSeg;
    Uint16 arxSize;
    Uint16 atxSize;
    void *apBufSeg;
    void *logObj;
    Uint8 cpuInt;
    Uint8 res[3];
    Uint32 rxfiflen;
    Uint32 rxOvrn;
    Uint32 txUdrn;
    Uint32 hostOvrn;
    void *devHandle;
    void *sem;
    Uint8 *rxBuf;
    Uint8 *txBuf;
    Uint8 *arxBuf;
    Uint8 *atxBuf;
    Uint16 rxRIdx;
    Uint16 rxWIdx;
    Uint16 txRIdx;
    Uint16 txWIdx;
    Uint16 rxLineCnt;
    Uint16 txLineCnt;
    Uint32 intMask;
    Uint32 errcnt;
    Uint32 errflg;
    Uint16 comSize;
    Uint16 resSize;
    void *crBufSeg;
    Uint32 srecLim;
    Uint8* comBuf;
    Uint8* resBuf;
    Uint16 comLen;
    Uint16 payLen;
    Uint16 comRIdx;
    Uint16 comWIdx;
    ACP_Params *acpParams;
    ACP_Handle acp;
}AFPUart_Obj;
extern AFPUart_Obj AFPUART_OBJ;
#endif

