/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  
/*
 *  ======== iir2tap1ch_cN_df2.c ========
 *  IIR filter implementation(DF2) for tap-2 & channels-1, casc-N, SP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 2, CHANNELS 1 and CASCADE N ************************/
/*Single section equations :                                                 */
/*                                                                           */
/* Filter equation  : H(z) =  1  + b1*z~1 + b2*z~2                           */
/*                           ----------------------                          */
/*                            1  - a1*z~1 - a2*z~2                           */
/*                                                                           */
/*   Direct form    : y(n) = x(n) + b1*x(n-1) + a1*y(n-1) + a2*y(n-2)        */
/*                                                                           */
/*   Canonical form : w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                    */
/*                    y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                    */
/*                                                                           */
/*                                                                           */
/* Implementation structure of one cascade section :                         */
/*           g                  w(n)                                         */
/*     x(n)-->---[+]---->--------@-------------[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+    b1     |                            */
/*               [+]----<<----| Z~1 |---->>----[+]                           */
/*                |           +-----+           |                            */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a2    +-----+    b2     |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/* The input gain(b0) of all sections are made into a common input gain.     */
/*****************************************************************************/


/* Header files */
#include "filters.h"
#include "fil_table.h"

/*
 *  ======== Filter_iirT2Ch1_cN_df2() ========
 *  IIR filter implementation(DF2) for tap-2 & channels-1, casc-N, SP. 
 */
/* Memory section for the function code */
Int Filter_iirT2Ch1_cN_df2_mx1( PAF_FilParam *pParam ) 
{
    Void ** pCoef, ** pVar; /* Handle parameters */
    Int i; /* Channel counter */
    Int error = 0, multiple;
    Int casc, cascN;
    Float *coefPtr, *inPtr;
    Double *varPtr;
    
    /* Get handle parameters into temp local variables */
    inPtr   = pParam->pIn[0];    
    pCoef   = pParam->pCoef;
    coefPtr = pCoef[0];
    pVar    = pParam->pVar;
    varPtr  = pVar[0];
    casc    = pParam->use >> 1; /* Get number of cascades */
    
    /* 1-stage "cascade" not supported */
    if(casc  == 1)
        return(1);
    
    multiple = casc/2;
    cascN    = 0;
    
    /* Do the gain part */
    if(multiple)
    {
        error   += Filter_iirT2Ch1_c2_df2_mx1(pParam);
        cascN   += 2;
        pCoef[0] = &coefPtr[1 + 4*cascN];
        pVar[0]  = &varPtr[2*cascN];
        pParam->pIn[0] =  pParam->pOut[0];
        
        multiple--;
    }
    
    /* Do the non-gain and multiple 5 part */
    pParam->pIn[0] =  pParam->pOut[0];
    
    for(i =0; i < multiple; i++)
    {
        error   += Filter_iirT2Ch1_c2_ng_df2_mx1(pParam);
        cascN   += 2;
        pCoef[0] = &coefPtr[1 + 4*cascN];
        pVar[0]  = &varPtr[2*cascN];
    }            

    /* Do the last stage (if any.  applicable for odd casccade calls) */
    if (casc&1) {
        // FIXME: HACK, HACK; HACK, STACK; STACK, STACK; STACK, HACK
        Float dummyCoef[8];
        Double  dummyVar[4];
        dummyCoef[0] = coefPtr[1 + 4*cascN + 0];
        dummyCoef[1] = coefPtr[1 + 4*cascN + 1];
        dummyCoef[2] = coefPtr[1 + 4*cascN + 2];
        dummyCoef[3] = coefPtr[1 + 4*cascN + 3];
        dummyCoef[4] = dummyCoef[5] = 
        dummyCoef[6] = dummyCoef[7] = 0;
        dummyVar[0] = varPtr[2*cascN + 0];
        dummyVar[1] = varPtr[2*cascN + 1];
        dummyVar[2] = 0;
        dummyVar[3] = 0;
        pCoef[0] = dummyCoef;
        pVar[0]  = dummyVar;
        error   += Filter_iirT2Ch1_c2_ng_df2_mx1(pParam);
        varPtr[2*cascN + 0] = dummyVar[0];
        varPtr[2*cascN + 1] = dummyVar[1];
    }      
    /* Put back the handle elements */
    pCoef[0]       = coefPtr;
    pParam->pVar   = pVar;
    pVar[0]        = varPtr;
    pParam->pIn[0] = inPtr;
    
    return(error);     
} /* Int Filter_iirT2Ch1_cN_df2() */
