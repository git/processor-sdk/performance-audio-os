
/*
*  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
*  ALL RIGHTS RESERVED 
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  Internal vendor specific (TIH) interface header for SRC
 *  algorithm. Only the implementation source files include
 *  this header; this header is not shipped as part of the
 *  algorithm.
 *
 *  This header contains declarations that are specific to
 *  this implementation and which do not need to be exposed
 *  in order for an application to use the Synchronous Rate 
 *  Conversion Algorithm.
 */
#ifndef SRC_TIH_PRIV_
#define SRC_TIH_PRIV_

#include <ti/xdais/ialg.h>
#include <xdc/runtime/Log.h>

#include "isrc.h"
#include "com_tii_priv.h"

#include "cpl.h"

#define SRC_TIH_activate NULL

#define SRC_TIH_deactivate NULL

extern Int SRC_TIH_alloc(const IALG_Params *algParams, IALG_Fxns **pf,
                        IALG_MemRec memTab[]);

#define SRC_TIH_free COM_TII_free

#define SRC_TIH_control COM_TII_control

extern Int SRC_TIH_initObj(IALG_Handle handle,
                          const IALG_MemRec memTab[], IALG_Handle parent,
                          const IALG_Params *algParams);
                
#define SRC_TIH_moved COM_TII_moved
                
extern Int SRC_TIH_apply(ISRC_Handle, PAF_AudioFrame *);

extern Int SRC_TIH_reset(ISRC_Handle, PAF_AudioFrame *);

extern Int SRC_TIH_ratio2(ISRC_Handle, PAF_AudioFrame *, Int);
extern Int SRC_TIH_ratio4(ISRC_Handle, PAF_AudioFrame *, Int);
extern Int SRC_TIH_ratioD(ISRC_Handle, PAF_AudioFrame *, Int);
extern Int SRC_TIH_ratioQ(ISRC_Handle, PAF_AudioFrame *, Int);

extern Int SRC_TIH_dsamp(ISRC_Handle, PAF_AudioFrame *, Int, Int);

extern Void SRC_TIH_downsamp2(PAF_AudioData *, PAF_AudioData*, Int kMax, 
                PAF_AudioData *, PAF_AudioData *, Int jMax);                              

extern Void SRC_TIH_downsamp4(PAF_AudioData *, PAF_AudioData*, Int kMax, 
                PAF_AudioData *, PAF_AudioData *, Int jMax);

extern Void SRC_TIH_upsamp2(PAF_AudioData *,  PAF_AudioData *, Int,
                PAF_AudioData *,  PAF_AudioData *,PAF_AudioData *, Uns, Int, Int, Int);
extern Void SRC_TIH_upsamp4(PAF_AudioData *,  PAF_AudioData *, Int,
                PAF_AudioData *,  PAF_AudioData *,PAF_AudioData *, Uns, Int, Int, Int);                            

#endif  /* SRC_TIH_PRIV_ */
