
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) Alpha Code Processor Algorithm IALG implementation
//
//
//

/*
 *  ACP Module IALG implementation - MDS's implementation of the
 *  IALG interface for the ACP Algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <xdc/std.h>

#include <ti/xdais/ialg.h>
#include <iacp.h>
#include <acp_mds.h>
#include <acp_mds_priv.h>

#include <string.h>         /* memcpy() declaration */

/*
 *  ======== ACP_MDS_activate ========
 */
  /* COM_TII_activate */

/*
 *  ======== ACP_MDS_alloc ========
 */
Int ACP_MDS_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IACP_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IACP_PARAMS;  /* set default parameters */
    }

    /* Request memory for ACP object of two possible forms */

    memTab[0].size = (sizeof(ACP_MDS_Obj)+3)/4*4 + (sizeof(ACP_MDS_Status)+3)/4*4;
    memTab[0].alignment = 4;
    memTab[0].space = IALG_SARAM;
    memTab[0].attrs = IALG_PERSIST;

    if (! params->config.pSRecordState)
        return (1);
    else {
        memTab[1].size = (sizeof(IACP_SRecordState)+3)/4*4;
        memTab[1].alignment = 4;
        memTab[1].space = IALG_EXTERNAL;
        memTab[1].attrs = IALG_PERSIST;

        memTab[2].size = 2*(int )params->config.pSRecordState;
        memTab[2].alignment = 2;
        memTab[2].space = IALG_EXTERNAL;
        memTab[2].attrs = IALG_PERSIST;

        memTab[3].size = 2*(int )params->config.pSRecordState;
        memTab[3].alignment = 2;
        memTab[3].space = IALG_EXTERNAL;
        memTab[3].attrs = IALG_PERSIST;

        return (4);
    }
}

/*
 *  ======== ACP_MDS_deactivate ========
 */
  /* COM_TII_deactivate */

/*
 *  ======== ACP_MDS_free ========
 */
  /* COM_TII_free */

/*
 *  ======== ACP_MDS_initObj ========
 */
Int ACP_MDS_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    ACP_MDS_Obj *acp = (Void *)handle;
    const IACP_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IACP_PARAMS;  /* set default parameters */
    }

    acp->pStatus = (ACP_MDS_Status *)((char *)acp + (sizeof(ACP_MDS_Obj)+3)/4*4);

    *acp->pStatus = *params->pStatus;
    acp->config = params->config;

    if (acp->config.pSRecordState) {
        int length = (int )acp->config.pSRecordState;
        acp->config.pSRecordState = memTab[1].base;
        acp->config.pSRecordState->fromSequenceLength = length;
        acp->config.pSRecordState->toSequenceLength = length;
        acp->config.pSRecordState->fromSequence = memTab[2].base;
        acp->config.pSRecordState->toSequence = memTab[3].base;
        acp->config.pSRecordState->fromBytes = -1;
    }

    return (IALG_EOK);
}

/*
 *  ======== ACP_MDS_control ========
 */
Int
ACP_MDS_control (IALG_Handle handle, IALG_Cmd cmd, IALG_Status *pStatus)
{
    ACP_MDS_Obj *obj = (Void *)handle;

    switch (cmd) {
      case IACP_GETBETAPRIMEVALUE:
        *(Int *)pStatus = obj->pStatus->betaPrimeValue;
        return (0);
      case IACP_SETBETAPRIMEVALUE:
        obj->pStatus->betaPrimeValue = *(Int *)pStatus;
        return (0);
      case IACP_GETBETAPRIMEBASE:
        *(Int *)pStatus = obj->pStatus->betaPrimeBase;
        return (0);
      case IACP_GETBETAPRIMEOFFSET:
        *(Int *)pStatus = obj->pStatus->betaPrimeOffset;
        return (0);
      default:
        return COM_TII_control (handle, cmd, pStatus);
    }
}

/*
 *  ======== ACP_MDS_moved ========
 */
  /* COM_TII_moved */

