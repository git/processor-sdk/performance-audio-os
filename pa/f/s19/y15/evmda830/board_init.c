
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Initial version obtained from ver 1.13 of i12/evmda830/board_init.c


#include <da830lib.h>

#pragma CODE_SECTION (board_init, ".text:board_init")
void board_init (void)
{

	CPUARBU = 0x00020010;
	MSTPRI0 = 0x44443333;
	MSTPRI1 = 0x44441110;
	BPRIO   = 0x0;
	
	// Setup PINMUX
	// Created using the Pin Setup Utility (Version: 1.0.426.6)
	PINMUX0 = 0x11112188;
	PINMUX1 = 0x11111111;
	PINMUX2 = 0x11111111;
	PINMUX3 = 0x11111111;
	PINMUX4 = 0x11111111;
	PINMUX5 = 0x11111111;
	PINMUX6 = 0x11111111;
	PINMUX7 = 0x11111111;
	PINMUX8 = 0x21122111;
	PINMUX9 = 0x11011112;
	PINMUX10 = 0x22222221;
	PINMUX11 = 0x11142222;
	PINMUX12 = 0x11111111;
	PINMUX13 = 0x22111111;
	PINMUX14 = 0x88222222;
	PINMUX15 = 0x21888888;
	PINMUX16 = 0x11111112;
	PINMUX17 = 0x00100111;
	PINMUX18 = 0x11111111;
	PINMUX19 = 0x00000001;

	// Enable peripherals through PSC0
	lpscEnable (PSC0, PD0, 0);								// TPCC
	lpscEnable (PSC0, PD0, 1);								// TPTC0
	lpscEnable (PSC0, PD0, 2);								// TPTC1
	lpscEnable (PSC0, PD0, 3);								// EMIFA
	lpscEnable (PSC0, PD0, 4);								// SPI0
	lpscEnable (PSC0, PD0, 5);								// MMC/SD0
	lpscEnable (PSC0, PD0, 6);								// AINTC
//  lpscEnable (PSC0, PD0, 7);                              // ARM RAM / ROM
	lpscEnable (PSC0, PD0, 8);								// Security Controller / Key Manager
	lpscEnable (PSC0, PD0, 9);								// UART0
	lpscEnable (PSC0, PD0, 10);								// SCR0_SS
	lpscEnable (PSC0, PD0, 11);								// SCR1_SS
	lpscEnable (PSC0, PD0, 12);								// SCR2_SS
	lpscEnable (PSC0, PD0, 13);								// dMAX
//  lpscEnable (PSC0, PD0, 14);                             // ARM
//  lpscEnable (PSC0, PD0, 15);                             // GEM

	// Enable peripherals through PSC1
	lpscEnable (PSC1, PD0, 1);								// USB 2.0
	lpscEnable (PSC1, PD0, 2);								// USB 1.1
	lpscEnable (PSC1, PD0, 3);								// GPIO
	lpscEnable (PSC1, PD0, 4);								// UHPI
	lpscEnable (PSC1, PD0, 5);								// CPGMAC
	lpscEnable (PSC1, PD0, 6);								// EMIFB
	lpscEnable (PSC1, PD0, 7);								// McASP0
	lpscEnable (PSC1, PD0, 8);								// McASP1
	lpscEnable (PSC1, PD0, 9);								// McASP2
	lpscEnable (PSC1, PD0, 10);								// SPI1
	lpscEnable (PSC1, PD0, 11);								// I2C1
	lpscEnable (PSC1, PD0, 12);								// UART1
	lpscEnable (PSC1, PD0, 13);								// UART2
	lpscEnable (PSC1, PD0, 16);								// LCDC
	lpscEnable (PSC1, PD0, 17);								// HR/EPWM0/1/2
	lpscEnable (PSC1, PD0, 20);								// ECAP0/1/2
	lpscEnable (PSC1, PD0, 21);								// EQEP0/1
	lpscEnable (PSC1, PD0, 24);								// SCR_P0_SS
	lpscEnable (PSC1, PD0, 25);								// SCR_P1_SS
	lpscEnable (PSC1, PD0, 26);								// CR_P3_SS
	lpscEnable (PSC1, PD0, 31);								// L3_CBA_RAM

    // Route the SPI0 signals to the serial expansion connector for
    // DCS6 SPI communication
    // i2c0 is used to configure the PCF8574APWRG4 on UI card which in turn controls the SPI_MODE signal.

    if(0){
        unsigned int * i2c0_ptr=(unsigned int *)0x01C22000u;
        i2c0_ptr[9]=0; 			// resetting peripheral by writing 0 to i2c mode reg.
        i2c0_ptr[12]=2;			// writing to i2c prescaler reg.
        i2c0_ptr[3]=6;			// writing to i2c clock low-time divider reg.
        i2c0_ptr[4]=6;			// writing to i2c clock high-time divider reg.
        i2c0_ptr[9]=0x00000020;		// enabling i2c peripheral by writing 1 to IRS bit of the mode reg. 
        i2c0_ptr[7]=0x3F;		// populating the slave address 0x3F to i2c slave address reg.
        i2c0_ptr[9]|=0x00000600;	// configured the i2c0 as master and transmitter by writing to i2c mode reg.
        i2c0_ptr[5]=1;			// writing 1 to i2c data count reg.
        while(i2c0_ptr[2]&0x00001000);	// polling i2c interrupt status register to see whether bus is busy.
        while(!(i2c0_ptr[2]&0x00000010)); // checking whether ICXRDY bit is set.
        i2c0_ptr[8]=0x7F;		// writing 0x7F to i2c data transmit reg.
        i2c0_ptr[9]|=0x00002800;	// configured to send START/STOP bit by writing to STT and STP bits of mode reg.
        while(i2c0_ptr[2]&0x00001000);	// polling i2c interrupt status reg to see if the bus is busy!
        while(i2c0_ptr[9]&0x00000400);	// checking whether transfer is completed. wait for MST bit to be 0 in mode reg.    
    }
}
