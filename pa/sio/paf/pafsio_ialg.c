
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// XDAIS IALG wrapper for PAF SIO drivers (implementations)
//
//
//

#include <xdc/std.h> //<std.h>
#include <ti/xdais/ialg.h> //<ialg.h>
//#include  <std.h>
#include <ti/xdais/xdas.h> //<xdas.h>

#include "pafsio_ialg.h"

// -----------------------------------------------------------------------------
// IALG_FXNS for use by initPhaseCommon when requesting common memory requirements
// for logical IO drivers. 

Int PAF_SIO_IALG_alloc (const IALG_Params *algParams, IALG_Fxns **pf, IALG_MemRec memTab[])
{
    PAF_SIO_IALG_Params *pParams = (PAF_SIO_IALG_Params *) algParams;
    int i;

    // Memory for algorithm object. (needed later by PAF_ALG_create_)
    // allocate extra memory to store memTabs since we assume the ones provided
    // maybe in ROM and we need to be able to update the base address as returned
    // by the allocator
    memTab[0].size = (sizeof(PAF_SIO_IALG_Obj)+3)/4*4 + (pParams->numRec*sizeof(IALG_MemRec)+3)/4*4;
    memTab[0].alignment = 4;
    memTab[0].space = IALG_EXTERNAL;
    memTab[0].attrs = IALG_PERSIST;

    // Memory specified via params
    for (i=0; i < pParams->numRec; i++) {
        memTab[i+1].size      = pParams->pMemRec[i].size;
        memTab[i+1].alignment = pParams->pMemRec[i].alignment;
        memTab[i+1].space     = (IALG_MemSpace) pParams->pMemRec[i].space;
        memTab[i+1].attrs     = (IALG_MemAttrs) pParams->pMemRec[i].attrs;
    }
    
    return (1+pParams->numRec);    
} // PAF_SIO_IALG_alloc

// -----------------------------------------------------------------------------

Int PAF_SIO_IALG_initObj (IALG_Handle handle, const IALG_MemRec memTab[], IALG_Handle p, const IALG_Params *algParams)
{
    PAF_SIO_IALG_Obj    *alg = (Void *) handle;
    PAF_SIO_IALG_Params *pParams = (PAF_SIO_IALG_Params *) algParams;
    int i;

    // copy (possible const) parameters into alg context
    alg->config.numRec = pParams->numRec;

    // update parameters with RAM memTab array base address
    alg->config.pMemRec = (IALG_MemRec *) ((char *)memTab[0].base + (sizeof(PAF_SIO_IALG_Obj)+3)/4*4);

    // copy memTab info into alg context for use in initPhaseDevice
    for (i=0; i < pParams->numRec; i++) {
        alg->config.pMemRec[i].size      = memTab[i+1].size;
        alg->config.pMemRec[i].alignment = memTab[i+1].alignment;
        alg->config.pMemRec[i].space     = memTab[i+1].space;
        alg->config.pMemRec[i].attrs     = memTab[i+1].attrs;
        alg->config.pMemRec[i].base      = memTab[i+1].base;
    }

    return (IALG_EOK);    
} // PAF_SIO_IALG_initObj

// -----------------------------------------------------------------------------

#define IALGFXNS                                                        \
    &PAF_SIO_IALG,           /* module ID */                            \
    NULL,                    /* activate */                             \
    PAF_SIO_IALG_alloc,      /* alloc */                                \
    NULL,                    /* control */                              \
    NULL,                    /* deactivate */                           \
    NULL,                    /* free */                                 \
    PAF_SIO_IALG_initObj,    /* init */                                 \
    NULL,                    /* moved */                                \
    NULL                     /* numAlloc (NULL => IALG_DEFMEMRECS) */   \

IALG_Fxns PAF_SIO_IALG = {
    IALGFXNS
};

// -----------------------------------------------------------------------------

// EOF
