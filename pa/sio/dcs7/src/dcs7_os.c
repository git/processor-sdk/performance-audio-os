
/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// OS dependent DCS implementation
//
//

#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/IHeap.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/hal/Cache.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/heaps/HeapMem.h>

#include "dcs7.h"
#include "dcs7_priv.h"


#ifndef BIOS6_NONLEGACY
#define BIOS6_NONLEGACY
#endif 

Uint32 DCS7_msToTicks(DCS7_Handle handle, Uint32 ms)
{
#ifdef BIOS6_NONLEGACY
    return ((ms * 1000)/ Clock_tickPeriod);
#else
    return ((ms * 1000)/ ti_sysbios_knl_Clock_tickPeriod);
#endif
}


void* DCS7_semAlloc(DCS7_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    return Semaphore_create(0, NULL, NULL);
#else
    return SEM_create(0,NULL);
#endif
}

Uint32 DCS7_semPend(DCS7_Handle handle, void *sem)
{
#ifdef BIOS6_NONLEGACY
    return Semaphore_pend(sem, BIOS_WAIT_FOREVER);
#else
    return SEM_pend(sem,SYS_FOREVER);
#endif
}

Uint32 DCS7_semPendTimeout(DCS7_Handle handle, void *sem, UInt32 timeout)
{
#ifdef BIOS6_NONLEGACY
    return Semaphore_pend(sem, timeout);
#else
    return SEM_pend(sem,SYS_FOREVER);
#endif
}
void DCS7_semPost(DCS7_Handle handle, void *sem)
{
#ifdef BIOS6_NONLEGACY
    Semaphore_post(sem);
#else
    SEM_post(sem);
#endif
}
void DCS7_semReset(DCS7_Handle handle, void *sem, Uint32 count)
{
#ifdef BIOS6_NONLEGACY
    Semaphore_reset(sem, count);
#else
    SEM_reset(sem, count);
#endif
}

void* DCS7_memAlloc(DCS7_Handle handle, void *memSeg, Uint32 size, Uint32 align)
{
#ifdef BIOS6_NONLEGACY
    return (void*) Memory_alloc((IHeap_Handle) *(Int32*) memSeg, size,
                                align, NULL);
#else
    return MEM_alloc(*(Int32*)memSeg,size,align);
#endif
}

void DCS7_taskDisable(DCS7_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    Task_disable();
#else
    TSK_disable();
#endif
}

void DCS7_taskEnable(DCS7_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    Task_enable();
#else
    TSK_enable();
#endif
}

void DCS7_configureInt(DCS7_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    
#else
    
#endif
}

void DCS7_cacheInv(DCS7_Handle handle, void *p, Uint32 cnt)
{
#ifdef BIOS6_NONLEGACY
    Uint32 rxSizeCacheAlign = CACHE_ROUND_TO_LINESIZE(L1D, cnt, sizeof(Uint8));
    Cache_inv(p, rxSizeCacheAlign, Cache_Type_L1D | Cache_Type_L2D, TRUE);
#else
    Cache_inv(p,cnt,Cache_Type_L1D|Cache_Type_L2D,TRUE);
#endif
}

void DCS7_cacheWbInv(DCS7_Handle handle, void *p, Uint32 cnt)
{
#ifdef BIOS6_NONLEGACY
    Uint32 rxSizeCacheAlign = CACHE_ROUND_TO_LINESIZE(L1D, cnt, sizeof(Uint8));
    Cache_wbInv(p, rxSizeCacheAlign, Cache_Type_L1D | Cache_Type_L2D, TRUE);
#else
    Cache_wbInv(p,cnt,Cache_Type_L1D|Cache_Type_L2D,TRUE);
#endif
}
