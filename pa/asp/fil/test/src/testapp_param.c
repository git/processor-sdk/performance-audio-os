/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/**
 *  @file   testapp_param.c
 *
 *  @brief  Consist of table for filter function and ther configurations
 *
 *****************************************************************************/

#include "testapp.h"

#ifndef NULL
#define NULL 0
#endif /*NULL*/





tTestTable gTestArray_IIR[]={
//  {funtionname        ,taps ,chan ,unicoef ,inplace ,casc}   //      index
    {Filter_iirT1Ch1    ,1    ,1    ,0       ,0       ,0},     //        0
    {Filter_iirT1Ch2    ,1    ,2    ,0       ,0       ,0},     //        1
    {Filter_iirT1Ch3    ,1    ,3    ,0       ,0       ,0},     //        2
    {Filter_iirT1Ch4    ,1    ,4    ,0       ,0       ,0},     //        3
    {Filter_iirT1Ch5_u  ,1    ,5    ,1       ,0       ,0},     //        4
    {Filter_iirT1Ch6_ui ,1    ,6    ,1       ,1       ,0},     //        5
    {Filter_iirT1Ch7_ui ,1    ,7    ,1       ,1       ,0},     //        6
    {Filter_iirT1ChN    ,1    ,8    ,0       ,0       ,0},     //        7
    {Filter_iirT2Ch1    ,2    ,1    ,0       ,0       ,0},     //        8
    {Filter_iirT2Ch2    ,2    ,2    ,0       ,0       ,0},     //        9
    {Filter_iirT2Ch3    ,2    ,3    ,0       ,0       ,0},     //        10
    {Filter_iirT2Ch4_u  ,2    ,4    ,1       ,0       ,0},     //        11
    {Filter_iirT2Ch4_ui ,2    ,4    ,1       ,1       ,0},     //        12
    {Filter_iirT2ChN    ,2    ,7    ,0       ,0       ,0},     //        13
    {Filter_iirTNChN    ,2    ,8    ,0       ,0       ,0},     //        14
    {NULL               ,0    ,0    ,0       ,0       ,0}      //
};

char iir_outFileName[][MAX_FILE_NAME_SIZE]={
    "Filter_iirT1Ch1",
    "Filter_iirT1Ch2",
    "Filter_iirT1Ch3",
    "Filter_iirT1Ch4",
    "Filter_iirT1Ch5_u",
    "Filter_iirT1Ch6_ui",
    "Filter_iirT1Ch7_ui",
    "Filter_iirT1ChN",
    "Filter_iirT2Ch1",
    "Filter_iirT2Ch2",
    "Filter_iirT2Ch3",
    "Filter_iirT2Ch4_u",
    "Filter_iirT2Ch4_ui",
    "Filter_iirT2ChN",
    "Filter_iirTNChN"
};

tTestTable gTestArray_FIR[]={
//  {funtionname        ,taps ,chan ,unicoef ,inplace ,casc}   //  index
    {Filter_firTNCh1    ,20   ,1    ,0       ,0       ,0},     //   0
    {Filter_firTNChN    ,2    ,8    ,0       ,0       ,0},     //   1
    {NULL               ,0    ,0    ,0       ,0       ,0}
};

char fir_outFileName[][MAX_FILE_NAME_SIZE] = {
    "Filter_firTNCh1",
    "Filter_firTNChN",
};

tTestTable gTestArray_IIR_MX[]={
//  {funtionname                ,taps ,chan ,unicoef ,inplace ,casc}   //  index
    {Filter_iirT1Ch1_mx         ,1    ,1    ,0       ,0       ,0},     //   0
    {Filter_iirT2Ch1_mx         ,2    ,1    ,0       ,0       ,0},     //   1
    {Filter_iirT2Ch1_mx_s       ,2    ,1    ,0       ,0       ,0},     //   2
    {Filter_iirT2Ch1_ui_mx      ,2    ,1    ,1       ,1       ,0},     //   3
    {Filter_iirT2Ch2_mx         ,2    ,2    ,0       ,0       ,0},     //   4
    {Filter_iirT2Ch2_u_mx       ,2    ,2    ,1       ,0       ,0},     //   5
    {Filter_iirT2Ch2_ui_mx      ,2    ,2    ,1       ,1       ,0},     //   6
    {Filter_iirT2Ch2_ui_mx_BM1  ,2    ,2    ,1       ,1       ,0},     //   7
    {Filter_iirT2Ch2_ui_mx_BM2  ,2    ,2    ,1       ,1       ,0},     //   8
    {Filter_iirT2ChN_mx         ,2    ,3    ,0       ,0       ,0},     //   9
    {Filter_iirT4Ch1_mx         ,4    ,1    ,0       ,0       ,0},     //   10
    {Filter_iirT4ChN_mx         ,4    ,3    ,0       ,0       ,0},     //   11
    {NULL                       ,0    ,0    ,0       ,0       ,0}
};

char iir_mx_outFileName[][MAX_FILE_NAME_SIZE] = {
    "Filter_iirT1Ch1_mx",
    "Filter_iirT2Ch1_mx",
    "Filter_iirT2Ch1_mx_s",
    "Filter_iirT2Ch1_ui_mx",
    "Filter_iirT2Ch2_mx",
    "Filter_iirT2Ch2_u_mx",
    "Filter_iirT2Ch2_ui_mx",
    "Filter_iirT2Ch2_ui_mx_BM1",
    "Filter_iirT2Ch2_ui_mx_BM2",
    "Filter_iirT2ChN_mx",
    "Filter_iirT4Ch1_mx",
    "Filter_iirT4ChN_mx"
};

tTestTable gTestArray_IIR_CAS[]={
//  {funtionname                    ,taps ,chan ,unicoef ,inplace ,casc}     // index
    {Filter_iirT2Ch1_c2_df2         ,2    ,1    ,0       ,0       ,2},       //   0
    {Filter_iirT2Ch1_c3_df2         ,2    ,1    ,0       ,0       ,3},       //   1
    {Filter_iirT2Ch1_c4_df2         ,2    ,1    ,0       ,0       ,4},       //   2
    {Filter_iirT2Ch1_c4_ng_df2      ,2    ,1    ,0       ,0       ,4},       //   3
    {Filter_iirT2Ch1_c5_df2         ,2    ,1    ,0       ,0       ,5},       //   4
    {Filter_iirT2Ch1_cN_df2         ,2    ,1    ,0       ,0       ,6},       //   5
    {Filter_iirT2ChN_cN_df2         ,2    ,3    ,0       ,0       ,6},       //   6
    {Filter_iirT2Ch2_c2_i_df2       ,2    ,2    ,0       ,1       ,2},       //   7
    {Filter_iirT2Ch1_c5_ng_df2      ,2    ,1    ,0       ,0       ,5},       //   8
    {NULL                           ,0    ,0    ,0       ,0       ,0}
};

char iir_cas_outFileName[][MAX_FILE_NAME_SIZE] = {
    "Filter_iirT2Ch1_c2_df2",
    "Filter_iirT2Ch1_c3_df2",
    "Filter_iirT2Ch1_c4_df2",
    "Filter_iirT2Ch1_c4_ng_df2",
    "Filter_iirT2Ch1_c5_df2",
    "Filter_iirT2Ch1_cN_df2",
    "Filter_iirT2ChN_cN_df2",
    "Filter_iirT2Ch2_c2_i_df2",
    "Filter_iirT2Ch1_c5_ng_df2"
};


/** @brief Filter table*/
pTestTable testapp_filterTable[] = {
    gTestArray_IIR,       /**< IIR filter      */
    gTestArray_FIR,       /**< FIR filter      */
    gTestArray_IIR_MX,    /**< IIR MX filter   */
    gTestArray_IIR_CAS    /**< IIR CAS filter  */
};

/** @brief Avalaible filter functions*/
char (*testName[])[] = {
    iir_outFileName,
    fir_outFileName,
    iir_mx_outFileName,
    iir_cas_outFileName
};


