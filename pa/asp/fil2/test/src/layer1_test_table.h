/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  
#ifndef LAYER1_TEST_TABLE_
#define LAYER1_TEST_TABLE_

// Test table structure
typedef struct testTable
{
    Uint type; // type field that will call the function, in FIL
    Int  taps; 
    Int  ch;
    Int  uniCoef;
    Int  inplace;
    Int  dPrec;
    Int  cPrec;
    Int  cascade;
	PAF_ChannelMask maskSelect;
    
    int ncalls;
    unsigned max, min;
    double avg;
} testTable; 
#ifdef EXT_CHAN_TEST 
	#define TEST_DF2CASC         184
	#define TEST_FIR         	 110
	#define TEST_IIR_MX1         88	
	#define TEST_MX1_DF1         11	
	#define TEST_IIR         44
	#define TEST_IIR_DF2T         22
#else
	#define TEST_DF2CASC         160
	#define TEST_FIR         	 80
	#define TEST_IIR_MX1         64	
	#define TEST_MX1_DF1         8	
	#define TEST_IIR         32
	#define TEST_IIR_DF2T         16
#endif


#endif
