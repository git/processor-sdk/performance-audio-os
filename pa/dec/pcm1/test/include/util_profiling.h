/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/**
 *  @file   util_profiling.h
 *
 *  @brief  This file contains the common declaration for operations
 *          used for profiling.
 *
 *          DSP profiling is only supported presently.
 *****************************************************************************/

#ifdef ARMCOMPILE
#include "type_defs.h"
#include "timer64_api.h"
UINT32 timerCyclesMsw=0xffffffff;
#define DEC_PROFILE_MAXCNT  0xFFFFFFFF
extern void hwTimer64Reload (UINT32 timerCyclesMsw, UINT32 timerCycles);
extern void hwTimerGetCount (UINT32 *msw, UINT32 *lsw);
#endif

#ifndef __UTIL_PROFILING_H__
#define __UTIL_PROFILING_H__

/** Externs */
extern void _TSC_enable(void);
extern long long _TSC_read(void);

/**
 *  @brief Structure for profiling.
 *
 */
typedef struct
{
	/**< Starting state cycles count */
    long long process_start_cycles;
    /**< End state cycles count  */
    long long process_end_cycles;
    /**< Total cycles consumed in the process */
    long long total_process_cycles;
    /**< Peak process cycles */
    long long peak_process_cycles;
    /**< Process counter */
    unsigned int process_count;
} tPrfl;


/**
 *  @brief     Profiling Initialization
 *
 *  @param[in] profile      Handle to profiling structure
 *
 *  @remark    Initialize/clear the handle parameters
 *
 */
inline void Prfl_Init(tPrfl *profile)
{
    profile->process_start_cycles = 0;
    profile->process_end_cycles = 0;
    profile->total_process_cycles = 0;
    profile->peak_process_cycles = 0;
    profile->process_count = 0;
#ifdef ARMCOMPILE
    hwTimer64Reload (timerCyclesMsw, 0xffffffff);
#endif
}

/**
 *  @brief     Start profiling
 *
 *  @param[in] profile      Handle to profiling structure
 *
 *  @remark    Start the counter to monitor the cycles taken by process.
 *
 */
inline void Prfl_Start(tPrfl *profile)
{
#ifdef ARMCOMPILE
    hwTimerGetCount (&timerCyclesMsw, &profile->process_start_cycles);
#else
    _TSC_enable();
    profile->process_start_cycles = _TSC_read();
#endif
}

/**
 *  @brief     Stop profiling
 *
 *  @param[in] profile      Handle to profiling structure
 *
 *  @remark    Stop the counter. Update the process count,
 *             total cycles for process and peak process cycles.
 *
 */
inline void Prfl_Stop(tPrfl *profile)
{
    long long process_cycles = 0;
#ifdef ARMCOMPILE
	hwTimerGetCount (&timerCyclesMsw, &profile->process_end_cycles);
#else
    profile->process_end_cycles = _TSC_read();
#endif	
#ifdef ARMCOMPILE
    process_cycles = (profile->process_end_cycles -
	                 profile->process_start_cycles)*6;
	if(process_cycles < 0) {
                process_cycles += DEC_PROFILE_MAXCNT;      
            }
#else
    process_cycles = profile->process_end_cycles -
	                 profile->process_start_cycles;
#endif

    if (process_cycles > profile->peak_process_cycles)
    {
        profile->peak_process_cycles = process_cycles;
    }

    profile->total_process_cycles += process_cycles;

    profile->process_count++;

}
#endif  /* __UTIL_PROFILING_H__ */
