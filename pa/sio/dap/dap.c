
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// SIO driver implementation for audio I/O using McASP.
//
//
//

#include <std.h>
#include <clk.h>
#include <dev.h>
#include <hwi.h>
#include <mem.h>
#include <que.h>
#include <sem.h>
#include <sys.h>
#include <tsk.h>

#ifndef DAP_CACHE_SUPPORT
  // if you rebuild dap.c in your project without this defined, 
  // the result is quite hard to find:  Occasional glitches in the sound.
  #define DAP_CACHE_SUPPORT  // typically defined in the project
#endif

#ifdef DAP_CACHE_SUPPORT
#include <ti/sysbios/hal/Cache.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h> //memset

#include "dap.h"
#include "dap_dmax.h"
#ifdef DAP_PORT_MCASP
#include "dap_mcasp.h"
#else
#error "No port defined"
#endif

#include <pafsio.h>

// don't use hidden jump table when using configurable dMAX (for now)
#ifndef dMAX_CFG
#include "pafhjt.h"
#undef free
#endif

// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
// For release, set mask to 1 to make it easier to catch any errors.
#define CURRENT_TRACE_MASK  1   // terse only

#define TRACE_MASK_TERSE    1   // only flag errors
#define TRACE_MASK_GENERAL  2   // log a half dozen lines per loop
#define TRACE_MASK_VERBOSE  4   // trace full operation

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
#else
 #define TRACE_VERBOSE(a)
#endif

// This works to set a breakpoint
#define SW_BREAKPOINT       asm( " SWBP 0" );
/* Software Breakpoint to Code Composer */
// SW_BREAKPOINT;

// .............................................................................

#ifdef Z_TOP
// in the Z topology case, the oTime component takes its cues from here.
extern int g_oTimeDapActive;  // from oTime.c
#endif

// .............................................................................
// notes:
//  . add control function to PORT table
//  . how to handle DMA/PORT specifics in parameter entries
//      can assume numSers = numChans is general and can be applied by DMA
//      same for wordSize?
//  . why are two idle stages needed (seems like 1 is enough)?

// .............................................................................
// only one global variable, not static so that DMA and port functions
// can access. We cant just store the address in devExt since the ISR has
// no context.

DAP_DriverObject dapDrv;

// needed since DAP_watchDog is called before DAP_init 
Int DAP_initialized = 0;

// .............................................................................

//Int  DAP_close(DEV_Handle);
Int  DAP_ctrl(DEV_Handle, Uns, Arg);
Int  DAP_idle(DEV_Handle, Bool);
Int  DAP_issue(DEV_Handle);
Int  DAP_open(DEV_Handle, String);
//Bool DAP_ready(DEV_Handle, SEM_Handle);
Int  DAP_reclaim(DEV_Handle);
Int  DAP_shutdown(DEV_Handle);
Int  DAP_start(DEV_Handle);
Int  DAP_config(DEV_Handle device, const DAP_Params *pParams);
Int  DAP_freeResources(DEV_Handle device);

DAP_Fxns DAP_FXNS =
{
    NULL, //DAP_close, -- remove for IROM since not using
    DAP_ctrl,
    DAP_idle,
    DAP_issue,
    DAP_open,
    NULL, //DAP_ready, -- remove for IROM since not using
    DAP_reclaim,
    DAP_shutdown,
    DAP_start,
    DAP_config,
    DAP_freeResources,
#ifdef DAP_PORT_MCASP
    (DAP_PORT_Fxns *) &DAP_MCASP_FXNS,
#endif
#ifdef DAP_DMA_EDMA
    (DAP_DMA_Fxns *) &DAP_EDMA_FXNS,
#endif
#ifdef DAP_DMA_DMAX
    (DAP_DMA_Fxns *) &DAP_DMAX_FXNS
#endif
};

// -----------------------------------------------------------------------------
// This function is not in the driver function table.
// Must be pointed at in GUI config tool.
//
Void DAP_init (Void)
{
    DEV_Device  *entry;
    DAP_Fxns    *pFxns;

    TRACE_GEN((&TR_MOD, "DAP_init.%d", __LINE__));

    // find function table pointer (used by DAP_XX_FTABLE_init macros)
    DEV_match("/DAP", &entry);
    if (entry == NULL) {
        SYS_error ("DAP", SYS_ENODEV);
        return;
    }
    pFxns = (DAP_Fxns *) entry->fxns;

    DAP_DMA_FTABLE_init ();
    DAP_PORT_FTABLE_init ();

    dapDrv.numDevices = 0;
    DAP_initialized = 1;

  #ifdef Z_TOP
    g_oTimeDapActive = 0;
  #endif

    return;
} // DAP_init

// -----------------------------------------------------------------------------

Int DAP_ctrl (DEV_Handle device, Uns code, Arg arg)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *)device->object;
    const DAP_Params *pParams;
    Int result = SYS_OK;

    //TRACE_GEN((&TR_MOD, "DAP_ctrl.%d (0x%x) code = 0x%x", __LINE__, device, code));

    switch (code) {

/* .......................................................................... */

        case PAF_SIO_CONTROL_MUTE:
        case PAF_SIO_CONTROL_UNMUTE:
            pParams = pDevExt->pParams;
            if (pParams == NULL)
                return SYS_OK;

            if (pParams->sio.control != NULL)
                result = pParams->sio.control (device, (const PAF_SIO_Params *)pParams, code, arg);
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_OPEN:
            if (pDevExt->runState)
                return SYS_EBUSY;

            if (!( pParams = (const DAP_Params *) arg ))
                return SYS_OK;

            if (result = DAP_FTABLE_config (device, pParams))
                return result;

            if (pParams->sio.control && (result = pParams->sio.control (device, (const PAF_SIO_Params *)pParams, code, arg)))
                return result;

            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_CLOSE:
            if (pDevExt->runState)
                return SYS_EBUSY;

            result = DAP_FTABLE_freeResources (device);
            if (result)
                return result;

            if (!(pParams = pDevExt->pParams))
                return SYS_OK;

            if (pParams->sio.control && (result = pParams->sio.control (device, (const PAF_SIO_Params *)pParams, code, arg)))
                return result;

            result = DAP_PORT_FTABLE_close (device);
            if (result)
                return result;

            pDevExt->pParams = NULL;
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_GET_WORDSIZE:
            // defer to DMA processing
            result = DAP_DMA_FTABLE_ctrl (device, code, arg);
            break;

        case PAF_SIO_CONTROL_SET_WORDSIZE:
            // defer to DMA processing
            result = DAP_DMA_FTABLE_ctrl (device, code, arg);
            break;

        case PAF_SIO_CONTROL_GET_PRECISION:
            if (arg == 0)
                return SYS_EINVAL;

            pParams = pDevExt->pParams;
            if (pParams == NULL)
                return( SYS_EINVAL );

            *((int *) arg) = pParams->sio.precision;
            break;

        case PAF_SIO_CONTROL_GET_NUMCHANNELS:
            if (arg == 0)
                return SYS_EINVAL;

            *((int *) arg) = pDevExt->numSlots * pDevExt->numSers;
            break;

        case PAF_SIO_CONTROL_SET_RATEX:
            pParams = pDevExt->pParams;
            if (pParams == NULL)
                return SYS_OK ;

            if (pParams->sio.control == NULL)
                return SYS_EINVAL;

            result = pParams->sio.control( device, (const PAF_SIO_Params *)pParams, PAF_SIO_CONTROL_SET_RATEX, arg);
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_IDLE:
            pParams = pDevExt->pParams;
            if (pParams == NULL)
                return SYS_OK ;

            if (pParams->sio.control == NULL)
                return SYS_EINVAL;

            result = pParams->sio.control( device, (const PAF_SIO_Params *)pParams, PAF_SIO_CONTROL_IDLE, arg);
            break;

        case PAF_SIO_CONTROL_IDLE_WITH_CLOCKS:
            // 1. Here we are intentionally not using SIO_Idle() and
            //    leaving the Tx clock running. We need this to avoid DAC noise,
            //    as well as provide a DIT clock when using digital output.
            if (device->mode != DEV_OUTPUT)
                return SYS_EINVAL;

            pParams = pDevExt->pParams;
            if (pParams == NULL)
                return SYS_OK;

            result = DAP_FTABLE_shutdown (device);
            if (result)
                return result;
            pDevExt->errorState = PAF_SIO_ERROR_IDLE_STAGE1;
            TRACE_GEN((&TR_MOD, "DAP_ctrl.%d: (0x%x) errorState = PAF_SIO_ERROR_IDLE_STAGE1 0x%x.", __LINE__, device, PAF_SIO_ERROR_IDLE_STAGE1));
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_GET_INPUT_STATUS:
            // needs to be attached
            pParams = pDevExt->pParams;
            if (pParams == NULL)
                return SYS_OK;

            if (pParams->sio.control == NULL)
                return SYS_EINVAL;

            result = pParams->sio.control( device, (const PAF_SIO_Params *)pParams, code, arg );
            break;

        case PAF_SIO_CONTROL_WATCHDOG:
            pParams = pDevExt->pParams;
            if (pParams == NULL)
                return SYS_OK;
            if (pParams->sio.control && (result = pParams->sio.control (device, (const PAF_SIO_Params *)pParams, code, arg)))
                return result;
            break;
            
/* .......................................................................... */

        // Timing stats specific to DMA engine
        case PAF_SIO_CONTROL_ENABLE_STATS:
        case PAF_SIO_CONTROL_DISABLE_STATS:
        case PAF_SIO_CONTROL_GET_STATS:
        case PAF_SIO_CONTROL_GET_NUM_EVENTS:
        case PAF_SIO_CONTROL_GET_NUM_REMAINING:
            result = DAP_DMA_FTABLE_ctrl (device, code, arg);
            // TRACE_VERBOSE((&TR_MOD, "DAP_ctrl: (0x%x) code 0x%x.  result 0x%x.", device, code, result));
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_SET_DITSTATUS:
            if(device->mode == DEV_OUTPUT)
            {
                const DAP_Params *pParams = pDevExt->pParams;
                MCASP_Handle hPort = dapMcaspDrv.hPort[pParams->sio.moduleNum];
                volatile Uint32 *base = (volatile Uint32 *)(hPort->baseAddr);
                MCASP_ConfigXmt *pTxConfig = (MCASP_ConfigXmt *)pParams->sio.pConfig;
                int encSelect = *((int *) arg);

                // HACK -- determine DIT need by FXWID
                if (((pTxConfig->afsxctl & _MCASP_AFSXCTL_FXWID_MASK)>> _MCASP_AFSXCTL_FXWID_SHIFT) == MCASP_AFSXCTL_FXWID_BIT)
                {
                    if ( (encSelect == 0x13) ||
                         (encSelect == 0xa)  || 
                         (encSelect == 0x6)) // DTE, DDE, MPE 
                    {
                        base[_MCASP_DITCSRA0_OFFSET] |= 2;
                        base[_MCASP_DITCSRB0_OFFSET] |= 2;
                    }
                    else
                    {
                        base[_MCASP_DITCSRA0_OFFSET] &= 0xfffffffd;
                        base[_MCASP_DITCSRB0_OFFSET] &= 0xfffffffd;
                    }
                }

                pParams = pDevExt->pParams;
                if (pParams == NULL)
                    return SYS_OK;

                if (pParams->sio.control != NULL)
                    result = pParams->sio.control (device, (const PAF_SIO_Params *)pParams, code, arg);
            }
            break;

/* .......................................................................... */

    }

    return result;
} // DAP_ctrl

// -----------------------------------------------------------------------------

Int DAP_idle (DEV_Handle device, Bool flush)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *)device->object;
    Int result = SYS_OK;

    //TRACE_GEN((&TR_MOD, "DAP_idle.%d (0x%x)", __LINE__, device));

   #ifdef Z_TOP
    g_oTimeDapActive = 0;
   #endif

    // do nothing if already idled or unattached
    if ((!pDevExt->runState) || (pDevExt->pParams == NULL))
        return result;

    // disable DMA processing -- stop servicing sync events
    result = DAP_DMA_FTABLE_disable (device);
    if (result)
    {
        TRACE_VERBOSE((&TR_MOD, "%s.%d: DAP_DMA_FTABLE_disable returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    // reset serial port -- stop generating sync events
    result = DAP_PORT_FTABLE_reset (device);
    if (result)
    {
        TRACE_VERBOSE((&TR_MOD, "%s.%d: DAP_PORT_FTABLE_reset returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    pDevExt->shutDown = 0; // force shutdown to run
    result = DAP_FTABLE_shutdown (device);
    if (result)
    {
        TRACE_VERBOSE((&TR_MOD, "%s.%d: DAP_FTABLE_shutdown returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    // signal stopped
    pDevExt->runState = 0;

    // reset errorState
    pDevExt->errorState = PAF_SIO_ERROR_NONE;
    TRACE_VERBOSE((&TR_MOD, "DAP_ctrl.%d: errorState = PAF_SIO_ERROR_NONE 0x%x.", __LINE__, PAF_SIO_ERROR_NONE));

    // place call to physical device
    if ((pDevExt->pParams != NULL) && (pDevExt->pParams->sio.control != NULL))
        result = pDevExt->pParams->sio.control(device, (const PAF_SIO_Params *)pDevExt->pParams, PAF_SIO_CONTROL_IDLE, 0);

    return result;
} // DAP_idle

// -----------------------------------------------------------------------------

Int DAP_start (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *)device->object;
    int result;

    TRACE_GEN((&TR_MOD, "DAP_start.%d (0x%x)", __LINE__, device));

    // signal we have started
    //    we change the state here since we have already moved a frame from the 
    //    todevice queue to the xferQue. If an error occurs during one of the 
    //    following resets/enables then we need to have runState !=0 in order
    //    for DAP_idle to properly cleanup. Moreover, the following resets/enables
    //    do not (and are now required not to) depend on runState being 0.
    pDevExt->runState = 1;

   #ifdef Z_TOP
    g_oTimeDapActive = 0;
   #endif

    result = DAP_PORT_FTABLE_reset (device);
    if (result)
    {
        TRACE_VERBOSE((&TR_MOD, "%s.%d: DAP_PORT_FTABLE_reset returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    // enable DMA processing
    result = DAP_DMA_FTABLE_enable (device);
    if (result)
    {
        TRACE_VERBOSE((&TR_MOD, "%s.%d: DAP_DMA_FTABLE_enable returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    // enable peripheral
    result = DAP_PORT_FTABLE_enable (device);
    if (result)
    {
        TRACE_VERBOSE((&TR_MOD, "%s.%d: DAP_PORT_FTABLE_enable returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    return SYS_OK;
} // DAP_start

// -----------------------------------------------------------------------------

Int DAP_issue (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *)device->object;
    DEV_Frame *pFrame;
    Int result;

    TRACE_GEN((&TR_MOD, "DAP_issue.%d (0x%x)", __LINE__, device));

    if (pDevExt->errorState >= PAF_SIO_ERROR_ERRBUF_XFER)
    {
        TRACE_TERSE((&TR_MOD, "DAP_issue.%d, errorState 0x%x (PAF_SIO_ERROR_ERRBUF_XFER = 0x%x)",
            __LINE__, pDevExt->errorState, PAF_SIO_ERROR_ERRBUF_XFER));
        return SYS_EBADIO;
    }

    // .........................................................................
    // here if running

    // disable device interrupts
    result = DAP_DMA_FTABLE_lock (device);
    if (result)
    {
        TRACE_VERBOSE((&TR_MOD, "%s.%d: DAP_DMA_FTABLE_lock returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    // get frame and parameter table to use; ints off => non-atomic OK
    //     dont need to check for empty queues since were here then todevice
    //     must have a frame placed there by the SIO_issue layer.
    //     paramQue must be valid since it is accessed the same as todevice.
    //     (indirectly -- isr places used items onto paramQue and fromdevice que
    //      at the same time)
    // set misc argument to pParam so get enqueue later
    pFrame = (DEV_Frame *) QUE_dequeue (device->todevice);

    // configure and insert transfer
    result = DAP_DMA_FTABLE_config (device, pFrame);
    if (result)
    {
        TRACE_VERBOSE((&TR_MOD, "%s.%d: DAP_DMA_FTABLE_config returned %d.\n", __FUNCTION__, __LINE__, result));
        DAP_DMA_FTABLE_unlock (device);
        return result;
    }

    // place on holder queue, ints off => non-atomic OK
    QUE_enqueue (&pDevExt->xferQue, pFrame);

    // increment count
    pDevExt->numQueued += 1;

    // special case enables when not yet started
    if (pDevExt->runState == 0) {
        result = DAP_FTABLE_start (device);
        if (result) {
            DAP_DMA_FTABLE_unlock (device);
            return result;
        }
    }
    pDevExt->shutDown = 0;

    if (result = DAP_DMA_FTABLE_unlock (device))
        TRACE_VERBOSE((&TR_MOD, "%s.%d: DAP_DMA_FTABLE_unlock returned %d.\n", __FUNCTION__, __LINE__, result));

    return result;
} // DAP_issue 

// -----------------------------------------------------------------------------

Int DAP_reclaim (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *)(device->object);
    Int result;
#ifdef DAP_CACHE_SUPPORT
    DEV_Frame *pFrame;
#endif

    TRACE_GEN((&TR_MOD, "DAP_reclaim.%d (0x%x)", __LINE__, device));

    // must be running and  error free 
    if (!pDevExt->runState)
    {
        TRACE_GEN((&TR_MOD, "DAP_reclaim.%d, not runState: 0x%x", __LINE__, pDevExt->errorState));
        return SYS_EBADIO;
    }

    if (pDevExt->errorState)
    {
        TRACE_TERSE((&TR_MOD, "DAP_reclaim.%d, errorState: 0x%x", __LINE__, pDevExt->errorState));
        return SYS_EBADIO;
    }

    // wait for ISR to signal block completion
    TRACE_VERBOSE((&TR_MOD, "DAP_reclaim.%d wait for ISR to signal block completion", __LINE__));
    if (!SEM_pend(pDevExt->sync, device->timeout))
    {
        TRACE_VERBOSE((&TR_MOD, "DAP_reclaim.%d, wait for ISR to signal block completion, return SYS_ETIMEOUT", __LINE__));
        return SYS_ETIMEOUT;
    }

    // call DMA reclaim
    TRACE_VERBOSE((&TR_MOD, "DAP_reclaim.%d: calling DAP_DMA_FTABLE_reclaim(device) device = 0x%x DAP_DMA_FTABLE_reclaim = 0x%x",
        __LINE__, device, &pDevExt->pFxns->pDmaFxns->reclaim));
    result = DAP_DMA_FTABLE_reclaim (device);
    if (result)
    {
        TRACE_GEN((&TR_MOD, "DAP_reclaim.%d: DAP_DMA_FTABLE_reclaim returned 0x%x", __LINE__, result));
        return result;
    }

    // return error (owner must idle)
    if (pDevExt->errorState == PAF_SIO_ERROR_FATAL)
    {
        TRACE_TERSE((&TR_MOD, "DAP_reclaim.%d, PAF_SIO_ERROR_FATAL: 0x%x", __LINE__, pDevExt->errorState));
        return PAF_SIO_ERROR_FATAL;
    }

#ifdef DAP_CACHE_SUPPORT
    // invalidate CACHE region if input -- use clean since
    //    Dont clean if was for fill.
    // since pend returned we know that head of fromdevice is valid
    pFrame = QUE_head (device->fromdevice);
    if ((device->mode == DEV_INPUT) && (pFrame->addr != NULL))
        Cache_inv (pFrame->addr, pFrame->size, Cache_Type_ALL, TRUE);
#endif

    TRACE_VERBOSE((&TR_MOD, "DAP_reclaim.%d, exit SYS_OK", __LINE__));
    return SYS_OK;
} // DAP_reclaim

// -----------------------------------------------------------------------------

Int DAP_open (DEV_Handle device, String name)
{
    DAP_DeviceExtension   *pDevExt;
    DEV_Device            *entry;
    Int                    oldMask, result;
    
    TRACE_GEN((&TR_MOD, "DAP_open.%d (0x%x)", __LINE__, device));

    // check SIO mode 
    if ((device->mode != DEV_INPUT) && (device->mode != DEV_OUTPUT))
        return SYS_EMODE;

    // allocate memory for device extension
    device->object = NULL;
    pDevExt = MEM_alloc (device->segid, (sizeof(DAP_DeviceExtension)+3)/4*4, 4);
    if (pDevExt == MEM_ILLEGAL)
    {
        printf("%s.%d:  MEM_alloc failed.\n", __FUNCTION__, __LINE__);
        TRACE_TERSE((&TR_MOD, "%s.%d:  MEM_alloc failed.\n", __FUNCTION__, __LINE__));
        asm( " SWBP 0" );  // SW Breakpoint
        return SYS_EALLOC;
    }
    device->object = (Ptr)pDevExt;

    // inits
    pDevExt->device = device;
    pDevExt->sync = NULL;
    pDevExt->pParams = NULL;
    pDevExt->runState = 0;  // not yet started
    pDevExt->errorState = PAF_SIO_ERROR_NONE;
    pDevExt->shutDown = 1;
    pDevExt->numQueued = 0;

    // use dev match to fetch function table pointer for DAP
    DEV_match("/DAP", &entry);
    if (entry == NULL) {
        SYS_error("DAP", SYS_ENODEV);
        return SYS_ENODEV;
    }
    pDevExt->pFxns = (DAP_Fxns *) entry->fxns;

    // create semaphore for device
    pDevExt->sync = SEM_create (0, NULL);
    if (pDevExt->sync == NULL)
    {
        TRACE_TERSE((&TR_MOD, "%s.%d: create semaphore for device failed.\n", __FUNCTION__, __LINE__));
        return SYS_EALLOC;
    }

    // queue init -- frame bin between todevice and fromdevice
    QUE_new (&pDevExt->xferQue);

    // update driver global (need to protect context)
    if (dapDrv.numDevices >= MAX_DAP_DEVICES)
    {
        TRACE_TERSE((&TR_MOD, "%s.%d: add device failure: no. of devices = %d; need to increase MAX_DAP_DEVICES.\n",
            __FUNCTION__, __LINE__, dapDrv.numDevices));
        SW_BREAKPOINT;
    }
    oldMask = HWI_disable ();
    dapDrv.device[dapDrv.numDevices] = device;
    pDevExt->deviceNum = dapDrv.numDevices++;
    HWI_restore (oldMask);

    // DMA init
    result = DAP_DMA_FTABLE_open (device);
    if (result)
    {
        TRACE_TERSE((&TR_MOD, "%s.%d: DAP_DMA_FTABLE_open returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    // PORT init
    result = DAP_PORT_FTABLE_open (device);
    if (result)
    {
        TRACE_TERSE((&TR_MOD, "%s.%d: DAP_PORT_FTABLE_open returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    return result;
} // DAP_open

// -----------------------------------------------------------------------------

Int DAP_config (DEV_Handle device, const DAP_Params *pParams)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *)device->object;
    Int                  result;

    TRACE_GEN((&TR_MOD, "DAP_config.%d (0x%x)", __LINE__, device));

    // cannot configure if transfer started
    if (pDevExt->runState == 1)
        return SYS_EBADIO;

    // save pointer to config structure in device extension. here so that
    //   forthcoming functions can use/modify config structure.
    pDevExt->pParams = pParams;

    // allocate Port resources.
    //    This must come before DMA configuration
    result = DAP_PORT_FTABLE_alloc (device);
    if (result)
    {
        TRACE_TERSE((&TR_MOD, "%s.%d: DAP_PORT_FTABLE_alloc returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    // DMA resource allocation
    result = DAP_DMA_FTABLE_alloc (device);
    if (result)
    {
        TRACE_TERSE((&TR_MOD, "%s.%d: DAP_DMA_FTABLE_alloc returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    return SYS_OK;
} // DAP_config

// -----------------------------------------------------------------------------

Int DAP_shutdown (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *)(device->object);
    SIO_Handle stream = (SIO_Handle) device;
    DEV_Frame *pFrame;
    Int result;

    TRACE_GEN((&TR_MOD, "DAP_shutdown.%d (0x%x)", __LINE__, device));

    if (pDevExt->shutDown)
        return SYS_EBADIO;

    if (pDevExt->pParams == NULL)
        return SYS_OK;

    result = DAP_DMA_FTABLE_lock (device);
    if (result)
    {
        TRACE_TERSE((&TR_MOD, "%s.%d: DAP_DMA_FTABLE_lock returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    // must call before xferQue and numQueued are modified
    result = DAP_DMA_FTABLE_shutdown (device);
    if (result)
    {
        TRACE_TERSE((&TR_MOD, "%s.%d: DAP_DMA_FTABLE_shutdown returned %d.\n", __FUNCTION__, __LINE__, result));
        DAP_DMA_FTABLE_unlock (device);
        return result;
    }

    // reset queues
    while (!QUE_empty(device->todevice)) {
       // place oustanding requests onto holding queue
       pFrame = (DEV_Frame *) QUE_dequeue (device->todevice);
       QUE_enqueue (&pDevExt->xferQue, (QUE_Elem *) pFrame);
    }

    while (!QUE_empty(&pDevExt->xferQue)) {
        // pull frame from holding queue and place on user queue
        pFrame = (DEV_Frame *) QUE_dequeue (&pDevExt->xferQue);
        QUE_enqueue (device->fromdevice, (QUE_Elem *) pFrame);
    }

    // reset counter
    pDevExt->numQueued = 0;

    // think this is better (from SIO_idle for standard model )
    // refill frame list -- so user needn't call reclaim, which may cause Rx underrun.
    while (!QUE_empty(device->fromdevice)) {
        /* place oustanding requests onto holding queue */
        pFrame = (DEV_Frame *) QUE_dequeue (device->fromdevice);
        QUE_enqueue (&stream->framelist, (QUE_Elem *) pFrame);
    }
    SEM_reset (pDevExt->sync, 0);
    
    pDevExt->shutDown = 1;
    
    result = DAP_DMA_FTABLE_unlock (device);
    if (result)
    {
        TRACE_TERSE((&TR_MOD, "%s.%d: DAP_DMA_FTABLE_unlock returned %d.\n", __FUNCTION__, __LINE__, result));
        return result;
    }

    return SYS_OK;
} // DAP_shutdown

// -----------------------------------------------------------------------------

Void DAP_watchDog (Void)
{
    DEV_Handle device;
    DAP_DeviceExtension *pDevExt;
    int i, oldMask, result;

    //TRACE_GEN((&TR_MOD, "DAP_watchDog.%d (0x%x)", __LINE__, device));

    // do nothing if DAP_init not yet called
    if (!DAP_initialized)
    {
        TRACE_VERBOSE((&TR_MOD, "%s.%d: DAP_init not yet called.\n", __FUNCTION__, __LINE__));
        return;
    }

    // protect context
    TSK_disable (); // needed since we may call SEM_post
    oldMask = HWI_disable ();

    TRACE_VERBOSE((&TR_MOD, "%s.%d: devices loop, numDevices = %d", __FUNCTION__, __LINE__, dapDrv.numDevices));

    for (i=0; i < dapDrv.numDevices; i++) {
        device  = dapDrv.device[i];

        //TRACE_VERBOSE((&TR_MOD, "%s.%d, devices loop start, device = 0x%x", __FUNCTION__, __LINE__, device));

        pDevExt = (DAP_DeviceExtension *) device->object;

        // do nothing if not running
        if (!pDevExt->runState)
            continue;

        // call board specific watchdog
        // TODO: handle return value
        SIO_ctrl (device, PAF_SIO_CONTROL_WATCHDOG, NULL);
        
        // if port layer returns error then must need to clean up
        result = DAP_PORT_FTABLE_watchDog (device);
        if (result) {
            // set errorState which will force owner thread
            //   to clean up via SIO_idle()
            pDevExt->errorState = PAF_SIO_ERROR_FATAL;
            TRACE_TERSE((&TR_MOD, "DAP_watchDog.%d, PAF_SIO_ERROR_FATAL: 0x%x", __LINE__, pDevExt->errorState));

            // if outstanding pend then post to free owner thead
            if (!SEM_pend(pDevExt->sync, 0))
                SEM_post (pDevExt->sync);
        }
    }

    // renable interrupts and task manager.
    // If we posted to the semaphore then the TSK_enable call will lead to
    // an immediate task switch to the associated audio thread.
    HWI_restore (oldMask);
    TSK_enable ();

} // DAP_watchDog

// -----------------------------------------------------------------------------

Int DAP_freeResources (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *)(device->object);

    TRACE_VERBOSE((&TR_MOD, "DAP_freeResources.%d, DAP_DMA_FTABLE_free device (0x%x)", __LINE__, device));
    // free DMA resources
    return (DAP_DMA_FTABLE_free (device));
} //DAP_freeResources

// -----------------------------------------------------------------------------
