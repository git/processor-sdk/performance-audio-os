
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// RingIO Data Driver implementation
//

#include <stdio.h>
#include <string.h>

#include <mem.h>
#include <que.h>
#include <sem.h>
#include <sys.h>
#include <tsk.h>

#include "dri.h"
#include "drierr.h"
#include "dri_ringio.h"
#include "dri_params.h"

#include <clk.h>
#include <log.h>

#include <paftyp.h>
#include <stdasp.h>

Int   DRI_ctrl (DEV_Handle device, Uns code, Arg Arg);
Int   DRI_idle (DEV_Handle device, Bool Flush);
Int   DRI_issue (DEV_Handle device);
Int   DRI_open (DEV_Handle device, String Name);
Int   DRI_reclaim (DEV_Handle device);
Int   DRI_requestFrame (DEV_Handle device, PAF_InpBufConfig *pBufConfig);
Int   DRI_reset (DEV_Handle device, PAF_InpBufConfig *pBufConfig);
Int   DRI_waitForData (DEV_Handle device, PAF_InpBufConfig *pBufConfig, XDAS_UInt32 count);


// Driver function table.
DRI_Fxns DRI_FXNS = {
    NULL,              // close not used in PA/F systems
    DRI_ctrl,
    DRI_idle,
    DRI_issue,
    DRI_open,
    NULL,              // ready not used in PA/F systems
    DRI_reclaim,
    DRI_requestFrame,
    DRI_reset,
    DRI_waitForData
};

// macros assume pDevExt is available and pDevExt->pFxns is valid

#define DRI_FTABLE_requestFrame(_a,_b)        (*pDevExt->pFxns->requestFrame)(_a,_b)
#define DRI_FTABLE_reset(_a,_b)               (*pDevExt->pFxns->reset)(_a,_b)
#define DRI_FTABLE_waitForData(_a,_b,_c)      (*pDevExt->pFxns->waitForData)(_a,_b,_c)
#define DRI_FTABLE_ctrl(_a,_b,_c)             (*pDevExt->pFxns->ctrl)(_a,_b,_c)


// .............................................................................
// syncState

enum
{
    SYNC_NONE,
    SYNC_PCM_FORCED
};

// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
//
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
// For release, set mask to 0.
#define TRACE_MASK_NONE     0
#define TRACE_MASK_TERSE    (1<<0)      // only flag errors
#define TRACE_MASK_GENERAL  (1<<1)      // describe normal behavior
#define TRACE_MASK_VERBOSE  (1<<2)      // detailed trace of operation

#define CURRENT_TRACE_MASK  1

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
#else
 #define TRACE_VERBOSE(a)
#endif

// -----------------------------------------------------------------------------

#if 0
 #include <math.h>
 #define PI      3.1415926f

static void _fillSine (int * p, int size)
{
    static unsigned int j = 0;
    int               srv = 48000;
    float             sri = 1.0f / 48000.0f;
    int              samp = j % srv;
    float            time = samp * sri;
    float            tinc = sri;
    float          omega1 = 2 * PI * 500.0f;
    float          omega2 = 2 * PI * 2000.0f;
    int                 i;

    for (i = 0; i < size/8; i++) {

          *p++ = ((short) (32767 *  sinf (omega1 * time)) << 16) & 0xFFFF0000;
        *p++ = ((short) (32767 *  sinf (omega2 * time)) << 16) & 0xFFFF0000;

        time += tinc;
    }
    j = samp + 256;

    return;
} //_fillSine
#endif


// -----------------------------------------------------------------------------

static Void _DRI_RingIO_reader_notify (RingIO_Handle handle, RingIO_NotifyParam param, RingIO_NotifyMsg msg)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) param;
    (Void) handle;      // To avoid compiler warning


    // sanity check -- param should never be null
    if (!pDevExt)
        return;

    switch (msg) {

        case NOTIFY_DATA_END:
            // Got data transfer end notification from GPP
            pDevExt->freadEnd = TRUE;
            break;

        default:
            break;
    }

    TRACE_VERBOSE((&TR_MOD, "_DRI_RingIO_reader_notify.%d.", __LINE__));
    SEM_post ((SEM_Handle) & (pDevExt->readerSemObj));

    return;
} //_DRI_RingIO_reader_notify

// -----------------------------------------------------------------------------

Int DRI_issue (DEV_Handle device)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) device->object;
    Int                   status = 0;
    DEV_Frame          *srcFrame;
    PAF_InpBufConfig *pBufConfig;

    TRACE_VERBOSE((&TR_MOD, "DRI_issue (0x%x) line %d", device, __LINE__ ));
    // check if queue is empty!
    if (QUE_empty(device->todevice))
    {
        TRACE_TERSE((&TR_MOD, "DRI_issue (0x%x) line %d: todevice queue is empty.", device, __LINE__));
        return SYS_EINVAL;
    }

    srcFrame   = QUE_get (device->todevice);
    TRACE_GEN((&TR_MOD, "DRI_issue (0x%x).%d:  got srcFrame 0x%x.", device, __LINE__, srcFrame));
    pBufConfig = (PAF_InpBufConfig *) srcFrame->addr;
    if (!pBufConfig || !pBufConfig->pBufStatus)
    {
        TRACE_TERSE((&TR_MOD, "DRI_issue.%d: (0x%x)  bad pBufConfig.", device, __LINE__));
        return SYS_EINVAL;
    }

    QUE_put (device->fromdevice, srcFrame);

    // CJP
    switch(srcFrame->arg)
    {   // srcFrame->arg has the sync request.  See DIB.
        case PAF_SIO_REQUEST_AUTO:
            TRACE_GEN((&TR_MOD, "DRI_issue (0x%x) PAF_SIO_REQUEST_AUTO", device));
            break;
        case PAF_SIO_REQUEST_NEWFRAME:
            TRACE_GEN((&TR_MOD, "DRI_issue (0x%x) PAF_SIO_REQUEST_NEWFRAME", device));
            break;
        case PAF_SIO_REQUEST_SYNC:
            TRACE_GEN((&TR_MOD, "DRI_issue (0x%x) PAF_SIO_REQUEST_SYNC", device));
            pDevExt->syncRequested = TRUE;
            break;  // don't return because issue only sets sizes.
        default:
            TRACE_GEN((&TR_MOD, "DRI_issue (0x%x) ?? 0x%x", device, srcFrame->arg));
            break;
    }

    // if first issue after open then init internal buf config view
    if (pDevExt->syncState == SYNC_NONE) 
    {
        TRACE_GEN((&TR_MOD, "DRI_issue.%d: (0x%x) syncState == SYNC_NONE.", __LINE__, device));
        status = DRI_FTABLE_reset (device, pBufConfig);
        if (status)
        {
            TRACE_TERSE((&TR_MOD, "DRI_issue.%d: (0x%x) returns error 0x%x.", __LINE__, device, status));
            return status;
        }
        TRACE_GEN((&TR_MOD, "DRI_issue.%d: (0x%x) syncState = SYNC_PCM_FORCED.", __LINE__, device));
        pDevExt->syncState = SYNC_PCM_FORCED;
    }

    status = DRI_FTABLE_requestFrame (device, &pDevExt->bufConfig);

    TRACE_VERBOSE((&TR_MOD, "DRI_issue (0x%x) line %d returns 0x%x", device, __LINE__, status ));
    return status;
}// DRI_issue

// -----------------------------------------------------------------------------
// This function uses the local definition of frameLength and lengthofData in
// pDevExt to request the next frame of data.

Int DRI_requestFrame (DEV_Handle device, PAF_InpBufConfig * pBufConfig)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) device->object;
    int                   status = 0;
    int xferSize;

    // flush previous frame
    pBufConfig->pntr.pVoid   = pBufConfig->head.pVoid;

    xferSize = pDevExt->pcmFrameLength * pBufConfig->sizeofElement * pDevExt->numChans;
    // TRACE_GEN((&TR_MOD, "DRI_requestFrame: pcmFrameLength %d.  sizeofElement: %d.  chansToTransfer %d.",
    //        pDevExt->pcmFrameLength, pBufConfig->sizeofElement, pDevExt->numChans));
    TRACE_GEN((&TR_MOD, "DRI_requestFrame (0x%x): xferSize: %d", device, xferSize));

    pDevExt->frameLength     = xferSize; // pBufConfig->frameLength is apparently used by DRI_reclaim.
    pDevExt->lengthofData    = pDevExt->pcmFrameLength * pDevExt->numChans;
    pBufConfig->frameLength  = xferSize; // ;
    pBufConfig->lengthofData = pDevExt->pcmFrameLength * pDevExt->numChans; // in elements, used in dwr_inp.c


    return status;
} // DRI_requestFrame

// -----------------------------------------------------------------------------

Int DRI_waitForData (DEV_Handle device, PAF_InpBufConfig * pBufConfig, XDAS_UInt32 count)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) device->object;
    int  status;

    while (1)
    {
        Int validDataLevel = RingIO_getValidSize (pDevExt->RingHandle);
        TRACE_VERBOSE((&TR_MOD, "DRI_waitForData (0x%x): %d valid, wanted %d.", device, validDataLevel, count));


        // check that decoding still requested -- allows for status
        // register to be updated via IOS to cancel autoProcessing
        if (pDevExt->pDecodeStatus) 
        {
            if (pDevExt->pDecodeStatus->sourceSelect == PAF_SOURCE_NONE)
            {
                TRACE_TERSE((&TR_MOD, "DRI_waitForData: return DRIERR_SYNC."));
                return DRIERR_SYNC; 
            }
        }

        //TRACE_VERBOSE((&TR_MOD, "DRI_waitForData: count: %d, pcmFrameLength: %d, sizeofElement: %d.",
        //        count, pDevExt->pcmFrameLength, pBufConfig->sizeofElement));

        status = DRI_RingIO_Reclaim (pDevExt, pBufConfig, count); // (pDevExt->frameLength * pBufConfig->sizeofElement));
        if (status)
        {
            TRACE_TERSE((&TR_MOD, "DRI_waitForData (0x%x).%d: return 0x%x.", device, __LINE__, status));
            // to do:  Support not getting all of the data requested.
            return status;
        }
        else 
        {
            TRACE_GEN((&TR_MOD, "DRI_waitForData (0x%x).%d: reclaimed %d OK.", device, __LINE__, count));
            return 0;
        }
    }
    // return 0;
} // DRI_waitForData

// -----------------------------------------------------------------------------
// Although interface allows for arbitrary BufConfigs we only support 1 -- so
// we can assume the one on the fromdevice is the one we want

Int DRI_reclaim (DEV_Handle device)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) device->object;
    Int                   status = 0;
    DEV_Frame          *dstFrame;
    PAF_InpBufConfig *pBufConfig;

    // TRACE_VERBOSE((&TR_MOD, "DRI_reclaim (0x%x) line %d", device, __LINE__ ));
    dstFrame = (DEV_Frame *) QUE_head (device->fromdevice);

    if (dstFrame == (DEV_Frame *) device->fromdevice)
    {
        TRACE_TERSE((&TR_MOD, "DRI_reclaim (0x%x) line %d.  Null frame.", device, __LINE__ ));
        return DRIERR_UNSPECIFIED;
    }
    if (!dstFrame->addr)
    {
        TRACE_TERSE((&TR_MOD, "DRI_reclaim (0x%x) line %d.  Null address.", device, __LINE__ ));
        return DRIERR_UNSPECIFIED;
    }

    if (pDevExt->syncRequested)
    {
        pDevExt->syncRequested = FALSE;
        pDevExt->sourceProgram = PAF_SOURCE_PCM;  // type is always PCM.
        TRACE_GEN((&TR_MOD, "DRI_reclaim (0x%x).%d: return after syncRequested.", device, __LINE__));
        return 0;
    }


    pBufConfig = (Ptr) dstFrame->addr;
    dstFrame->size = sizeof (PAF_InpBufConfig);

   #if 1    // keeps AS_out running even when SDPIF input is removed.
            // Maybe this is a general approach to making output insensitive to input.
    TRACE_GEN((&TR_MOD, "DRI_reclaim (0x%x) line %d: waiting for data.", device, __LINE__));
    status = DRI_waitForData(device, &pDevExt->bufConfig, pDevExt->bufConfig.frameLength);
        // DRI_FTABLE_waitForData (device, &pDevExt->bufConfig, pDevExt->bufConfig.frameLength);
    TRACE_GEN((&TR_MOD, "DRI_reclaim (0x%x) line %d: status %x.", device, __LINE__, status));

   #else




    // If we haven't reached prefill yet, as indicated by firstRead, then
    // check valid size. If still don't have enough then indicate with sync
    // error and keep sourceProgram to unknown. For cases where the input
    // file is smaller than the prefill threshold we move on since we have
    // all the data we're going to get.
    if (pDevExt->firstRead == TRUE)
    {
        Uint32         validDataLevel = 0;
        Int requiredLevel;

        // Size of the buffer is set in afp_link.c.
        requiredLevel = 4 * pDevExt->pcmFrameLength * pBufConfig->sizeofElement * pDevExt->numChans;
        validDataLevel = RingIO_getValidSize (pDevExt->RingHandle);

        
        if ((validDataLevel < requiredLevel) && (pDevExt->freadEnd != TRUE))
        {
            TRACE_GEN((&TR_MOD, "DRI_reclaim (0x%x): Low level.  Valid: %d. requiredLevel: %d.",
                       device, validDataLevel, requiredLevel));
            status = DRIERR_SYNC;
        }
        else {
            TRACE_GEN((&TR_MOD, "DRI_reclaim (0x%x) line %d: First read.  validDataLevel %d", 
                                device, __LINE__, validDataLevel));
            pDevExt->firstRead     = FALSE;
            pDevExt->sourceProgram = PAF_SOURCE_PCM;
        }
    }

    if (pDevExt->firstRead == FALSE) 
    {
        TRACE_GEN((&TR_MOD, "DRI_reclaim (0x%x) line %d: waiting for data.", device, __LINE__));
        status = DRI_waitForData(device, &pDevExt->bufConfig, pDevExt->bufConfig.frameLength);
            // DRI_FTABLE_waitForData (device, &pDevExt->bufConfig, pDevExt->bufConfig.frameLength);
        TRACE_GEN((&TR_MOD, "DRI_reclaim (0x%x) line %d: status %x.", device, __LINE__, status));
    }
   #endif

    // reflect to caller state of buffer
    *pBufConfig = pDevExt->bufConfig;

    TRACE_GEN((&TR_MOD, "DRI_reclaim (0x%x).%d returns 0x%x", device, __LINE__, status ));
    return status;
} // DRI_reclaim

//--------------------------------------------------------------------------------
Int DRI_initStats (DEV_Handle device, float seed)
{
    DRI_DeviceExtension   *pDevExt = (DRI_DeviceExtension *) device->object;
    int iseed = (unsigned int)(seed+0.1);
    // PAF_SIO_Stats *pStats;
    
    // allocate structure only once to avoid fragmentation
    if (!pDevExt->pStats) {
        pDevExt->pStats = MEM_alloc (device->segid, (sizeof(PAF_SIO_Stats)+3)/4*4, 4);
        if (!pDevExt->pStats)
            return SYS_EALLOC;
    }
    if(iseed == 48000)
            pDevExt->pStats->dpll.dv = 3.637978807091713e-007;
    else if(iseed == 44100)
            pDevExt->pStats->dpll.dv = 3.959703462896842e-007;
    else if(iseed == 32000)
            pDevExt->pStats->dpll.dv = 5.4569682106375695e-007;

    return SYS_OK;
} //DRI_initStats

Int DRI_ctrl (DEV_Handle device, Uns code, Arg arg)
{
    DRI_DeviceExtension   *pDevExt = (DRI_DeviceExtension *) device->object;
    PAF_SIO_InputStatus *tmpStatus = (PAF_SIO_InputStatus *) arg;
    // Int                     status = 0;

    // TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x). code: 0x%x.  arg: 0x%x", device, code, arg));

    switch (code) {

        case PAF_SIO_CONTROL_SET_DECSTATUSADDR:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d PAF_SIO_CONTROL_SET_DECSTATUSADDR", device, __LINE__));
            pDevExt->pDecodeStatus = (PAF_DecodeStatus *) arg;
            return 0;

        case PAF_SIO_CONTROL_SET_PCMFRAMELENGTH:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d PAF_SIO_CONTROL_SET_PCMFRAMELENGTH", device, __LINE__));
            pDevExt->pcmFrameLength = (XDAS_Int32) arg;
            return 0;

        case PAF_SIO_CONTROL_SET_SOURCESELECT:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d PAF_SIO_CONTROL_SET_SOURCESELECT", device, __LINE__));
            pDevExt->sourceSelect = (XDAS_Int8) arg;
            return 0;

        case PAF_SIO_CONTROL_GET_SOURCEPROGRAM:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d PAF_SIO_CONTROL_GET_SOURCEPROGRAM", device, __LINE__));
            if (!arg)
            {
                TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d: return DRIERR_UNSPECIFIED", device, __LINE__));
                return DRIERR_UNSPECIFIED;
            }                
            *((XDAS_Int8 *) arg) = pDevExt->sourceProgram;
            return 0;

        case PAF_SIO_CONTROL_SET_IALGADDR:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d PAF_SIO_CONTROL_SET_IALGADDR", device, __LINE__));
            pDevExt->pSioIalg = (PAF_SIO_IALG_Obj *) arg;
            return 0;

        // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

        case PAF_SIO_CONTROL_OPEN:
        {
            const Dri_Params *pParams;

            pParams = (const Dri_Params *) arg;
            pDevExt->pParams = pParams;

            // extract necessary info from pParams
            if (pParams->pReaderHandle == NULL)
            {   // legacy case
                pDevExt->dri_dro_link = FALSE;
                pDevExt->numChans = 2;
                pDevExt->RingHandle  = PA_DSPLINK_readerHandle;
                TRACE_TERSE((&TR_MOD, "DRI_ctrl (0x%x): PAF_SIO_CONTROL_OPEN for ARM connection.", device));
                printf("DRI configured to connect to connect to ARM.\n");
               #ifdef ENABLE_DSP_DSP_RINGIO
                printf("\n!! DRI can't yet talk to ARM if ENABLE_DSP_DSP_RINGIO is set!! \n");
               #endif
            }
            else
            {
                pDevExt->dri_dro_link = TRUE;
                pDevExt->RingHandle  = (RingIO_Handle)(*pParams->pReaderHandle);
                pDevExt->numChans = pParams->numChans;
                TRACE_TERSE((&TR_MOD, "DRI_ctrl (0x%x): DRI configured to connect %d channels to DRO (0x%x).\n",
                                      device, pDevExt->numChans, pDevExt->RingHandle));
                // printf("DRI (0x%x)-DRO configured to connect %d channels to DRO. (0x%x)\n",
                //       device, pDevExt->numChans, pDevExt->RingHandle);
            }
            // set syncState to none to force reset in first issue call after
            // open which is needed to init internal bufConfig view.
            TRACE_GEN((&TR_MOD, "DRI_ctrl (0x%x).%d: syncState = SYNC_NONE.", device, __LINE__));
            pDevExt->syncState  = SYNC_NONE;
            pDevExt->linkLock   = FALSE;
            // indicate the ring is empty
            pDevExt->freadEnd  = FALSE;
            pDevExt->firstRead = TRUE;
            pDevExt->syncRequested = FALSE;
            return 0;
        }

        case PAF_SIO_CONTROL_GET_NUM_REMAINING:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d PAF_SIO_CONTROL_GET_NUM_REMAINING", device, __LINE__));
            if (!arg)
            {
                TRACE_VERBOSE((&TR_MOD, "DRI_ctrl.%d: return DRIERR_UNSPECIFIED", __LINE__));
                return DRIERR_UNSPECIFIED;
            }
            return 0;

        case PAF_SIO_CONTROL_GET_INPUT_STATUS:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d PAF_SIO_CONTROL_GET_INPUT_STATUS", device, __LINE__));

            // LINK is initialized and ring open in afp_link task return unlocked
            // until this happens to force framework to skip data requests and keep
            // checking.
            if (pDevExt->RingHandle == NULL) {
                TRACE_VERBOSE((&TR_MOD, "DRI_ctrl.%d: PAF_SIO_CONTROL_GET_INPUT_STATUS: not init", __LINE__));
                tmpStatus->lock = 0;
                return 0;
            }

            // if setNotifier has not succeeded, as indicated by local lock status, then
            // call again. If it still hasn't succeeded then return unlocked to force
            // framework to skip data requests and keep trying.
            if (pDevExt->linkLock == FALSE) 
            {

                // we swallow any error reported by this call and indicate success or failure
                // solely through the lock status
                int localStatus = RingIO_setNotifier (pDevExt->RingHandle,
                                                      RINGIO_NOTIFICATION_ONCE,
                                                      READER_WATERMARK_VALUE,
                                                      &_DRI_RingIO_reader_notify,
                                                      (RingIO_NotifyParam) pDevExt);

                pDevExt->linkLock = (localStatus == RINGIO_SUCCESS) ? TRUE : FALSE;
                TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d: PAF_SIO_CONTROL_GET_INPUT_STATUS.  linkLock: %d",
                           device, __LINE__, pDevExt->linkLock));

            }

            tmpStatus->lock = (pDevExt->linkLock == TRUE) ? 1 : 0;
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d: PAF_SIO_CONTROL_GET_INPUT_STATUS. lock: 0x%x", device, __LINE__, tmpStatus->lock));
            return 0;

        case PAF_SIO_CONTROL_ENABLE_STATS:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d PAF_SIO_CONTROL_ENABLE_STATS", device, __LINE__));
            DRI_initStats (device, (float) arg);
            return 0;

        case PAF_SIO_CONTROL_GET_STATS:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d PAF_SIO_CONTROL_GET_STATS", device, __LINE__));
            // pass pointer to local stats structure
            // ???: should this be a ptr to const ?
            *((Ptr *) arg) = pDevExt->pStats;
            return 0;

        case PAF_SIO_CONTROL_DISABLE_STATS:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d PAF_SIO_CONTROL_DISABLE_STATS", device, __LINE__));
            // not handled
            return 0;

        case PAF_SIO_CONTROL_GET_NUM_EVENTS:
        {
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d PAF_SIO_CONTROL_GET_NUM_EVENTS", device, __LINE__));
            return 0;
        }
        case PAF_SIO_CONTROL_TRACE:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d:  PAF_SIO_CONTROL_TRACE is unsupported", device, __LINE__));
            return 0;

        default:
            TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d:  code 0x%x Unsupported", device, __LINE__, code));
            return 0;
    }
    // TRACE_VERBOSE((&TR_MOD, "DRI_ctrl (0x%x).%d: return %x", device, __LINE__, status));
    // return status;  // unreachable
} // DRI_ctrl
// -----------------------------------------------------------------------------

Int DRI_idle (DEV_Handle device, Bool flush)
{
    DRI_DeviceExtension   *pDevExt = (DRI_DeviceExtension *) device->object;
    Int                     status = 0;

    TRACE_VERBOSE((&TR_MOD, "DRI_idle (0x%x), line %d: Reset.", device, __LINE__));

    while (!QUE_empty (device->todevice))
        QUE_enqueue (device->fromdevice, QUE_dequeue (device->todevice));

    while (!QUE_empty (device->fromdevice))
        QUE_enqueue (&((SIO_Handle) device)->framelist, QUE_dequeue (device->fromdevice));

    status = DRI_FTABLE_reset (device, NULL);
    if (status)
        return status;

    return 0;
} // DRI_idle

// -----------------------------------------------------------------------------

Int DRI_open (DEV_Handle device, String name)
{
    DRI_DeviceExtension *pDevExt;
    DEV_Device            *entry;
    // PAF_InpBufConfig *pBufConfig;
    (void)name;  // not used

    TRACE_GEN((&TR_MOD, "DRI_open (0x%x).", device));

    // only one frame interface supported
    if (device->nbufs != 1)
        return SYS_EALLOC;

    if ((pDevExt = MEM_alloc (device->segid, sizeof (DRI_DeviceExtension), 0)) == MEM_ILLEGAL)
    {
        SYS_error ("DRI MEM_alloc", SYS_EALLOC);
        return SYS_EALLOC;
    }
  
    pDevExt->pcmFrameLength = 0;
    pDevExt->sourceSelect   = PAF_SOURCE_NONE;
    pDevExt->pInpBufStatus  = NULL;
    pDevExt->pDecodeStatus  = NULL;
    pDevExt->pStats         = NULL;
    device->object          = (Ptr) pDevExt;
    pDevExt->syncRequested  = FALSE;
    pDevExt->device         = device;

    // pBufConfig = &pDevExt->bufConfig;
    // pBufConfig->allocation is normally zero here, set before check in reset

    // use dev match to fetch function table pointer for DRI
    name = DEV_match ("/DRI", &entry);
    if (entry == NULL) {
        SYS_error ("DRI", SYS_ENODEV);
        return SYS_ENODEV;
    }
    pDevExt->pFxns     = (DRI_Fxns *) entry->fxns;
    pDevExt->syncState = SYNC_NONE;

    SEM_new (&(pDevExt->readerSemObj), 0);

    return 0;
} // DRI_open

// -----------------------------------------------------------------------------
// Although this is void it is still needed since BIOS calls all DEV inits on
// startup.

Void DRI_init (Void)
{
} // DRI_init

// -----------------------------------------------------------------------------

Int DRI_reset (DEV_Handle device, PAF_InpBufConfig * pBufConfig)
{
    DRI_DeviceExtension *pDevExt = (DRI_DeviceExtension *) device->object;

    TRACE_VERBOSE((&TR_MOD, "DRI_reset (0x%x), line %d: Reset.", device, __LINE__));

    if (pBufConfig)
    {
        TRACE_VERBOSE((&TR_MOD, "DRI_reset (0x%x), line %d: pBufConfig. allocation: %d", 
                       device, __LINE__, pBufConfig->allocation));
        pBufConfig->pntr          = pBufConfig->base;
        pBufConfig->head          = pBufConfig->base;
        pBufConfig->futureHead    = pBufConfig->base;
        pBufConfig->lengthofData  = 0;
        pBufConfig->sizeofElement = pDevExt->pParams->sio.wordSize;
        pBufConfig->stride        = pDevExt->pParams->numChans;
        pBufConfig->sizeofBuffer  = pBufConfig->allocation;

        // hack -- save status context for use in close
        pDevExt->pInpBufStatus                = pBufConfig->pBufStatus;
        pBufConfig->pBufStatus->lastFrameFlag = 0;

        // initialize internal view with external buf config
        pDevExt->bufConfig = *pBufConfig;
    }

    if (pBufConfig->allocation == 0)
    {
        TRACE_TERSE((&TR_MOD, "DRI_reset (0x%x):  Warning:  No Allocation.  Check array of PAF_ASP_LinkInit structures in params.", device));
        // this seems to happen occasionally and is not fatal, though at one point it was fatal.
        // Now removing the printf as it seems worse than the occasional error.
        // From what I've seen, the buffer is present when the system is ready to go.
        // printf("DRI_reset (0x%x):  ERROR:  No Allocation.\n", device);
        // return SYS_EALLOC;
    }


    pDevExt->syncState     = SYNC_NONE;
    pDevExt->pcmTimeout    = 0;
    pDevExt->zeroCount     = 0;
    pDevExt->sourceProgram = PAF_SOURCE_UNKNOWN;

    if (pDevExt->pInpBufStatus)
        pDevExt->pInpBufStatus->zeroRun = 0;

    // indicate the ring is empty
    pDevExt->freadEnd  = FALSE;
    pDevExt->firstRead = TRUE;

    SEM_reset (&(pDevExt->readerSemObj), 0);
    return 0;
} //DRI_reset

// -----------------------------------------------------------------------------
