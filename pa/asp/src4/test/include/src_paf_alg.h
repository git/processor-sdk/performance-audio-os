
/*
 *  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
 *  ALL RIGHTS RESERVED 
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Algorithm Creation API declarations.
//
//
//

#ifndef PAF_ALG_
#define PAF_ALG_

#include <src_paf_ialg.h>

typedef struct PAF_ALG_AllocInit {
    IALG_Fxns *ialg_fxns;
    const IALG_Params *ialg_prms;
} PAF_ALG_AllocInit;

struct PAF_ALG_Fxns {
    Int (*alloc)(const PAF_ALG_AllocInit *, Int, IALG_MemRec *); 
    ALG_Handle (*create)(const IALG_Fxns *, IALG_Handle p, const IALG_Params *, const IALG_MemRec *,PAF_IALG_Config *);
    Void (*init)(IALG_MemRec *, Int, const IALG_MemSpace *);
    Int (*allocMemory)(IALG_MemRec *, Int, const IALG_MemRec *,PAF_IALG_Config *);
    Int (*mallocMemory)(IALG_MemRec *, PAF_IALG_Config *);
    Int (*memSpace)(const PAF_IALG_Config *, IALG_MemSpace);
    Void (*setup)(PAF_IALG_Config *, Int, Int,Int,Int);
};

extern struct PAF_ALG_Fxns PAF_ALG_fxns;

inline
Int
PAF_ALG_alloc (
    const PAF_ALG_AllocInit *pInit,
    Int sizeofInit,
    IALG_MemRec *common)
{
    return PAF_ALG_fxns.alloc(pInit,sizeofInit,common);
}

inline
ALG_Handle
PAF_ALG_create (
    const IALG_Fxns *fxns,
    IALG_Handle p,
    const IALG_Params *params,
    const IALG_MemRec *common,
    PAF_IALG_Config *pafConfig)
{
    return PAF_ALG_fxns.create(fxns,p,params,common,pafConfig);
}
  
inline
Void 
PAF_ALG_init (
    IALG_MemRec *memTab,
    Int n,
    const IALG_MemSpace *commonSpace)
{
    PAF_ALG_fxns.init(memTab,n,commonSpace);
}

inline
Int
PAF_ALG_allocMemory (
    IALG_MemRec memTab[],
    Int n,
    const IALG_MemRec common[],
    PAF_IALG_Config *p)
{
    return PAF_ALG_fxns.allocMemory(memTab,n,common,p);
}

inline
Int
PAF_ALG_mallocMemory (
    IALG_MemRec *memTab,
    PAF_IALG_Config *p)
{
    return PAF_ALG_fxns.mallocMemory(memTab,p);
}

inline
Int
PAF_ALG_memSpace (
    const PAF_IALG_Config *p, 
    IALG_MemSpace space)
{
    return PAF_ALG_fxns.memSpace(p,space);
}

inline
Void
PAF_ALG_setup (
    PAF_IALG_Config *p,
    Int internalHeap,
    Int externalHeap,
    Int internalHeap1,
    Int clr)
{
    PAF_ALG_fxns.setup(p,internalHeap,externalHeap,internalHeap1,clr);
}

/*
 *  ======== PAF_ALG_freeMemory ========
 */

extern 
Void 
PAF_ALG_freeMemory (
    IALG_MemRec *, 
    Int);

/*
 *  ======== PAF_ALG_activate ========
 */

extern 
Void 
PAF_ALG_activate(
    ALG_Handle alg);

/*
 *  ======== PAF_ALG_deactivate ========
 */

extern 
Void 
PAF_ALG_deactivate(
    ALG_Handle alg);

/*
 *  ======== PAF_ALG_delete ========
 */

extern 
Void 
PAF_ALG_delete(
    ALG_Handle alg);

/*
 *  ======== PAF_ALG_alloc ========
 */

#define PAF_ALG_ALLOC(X,C) \
    PAF_ALG_alloc ((const PAF_ALG_AllocInit *)(X), sizeof (*(X)), (C))

/*
 *  ======== PAF_ALGERR Definitions ========
 */

#define PAF_ALGERR               (int)0x80010000

#define PAF_ALGERR_UNSPECIFIED   (PAF_ALGERR+0x00) 
#define PAF_ALGERR_MEMTAB_COUNT  (PAF_ALGERR+0x01)
#define PAF_ALGERR_MEMTAB_MALLOC (PAF_ALGERR+0x02)
#define PAF_ALGERR_PERSIST       (PAF_ALGERR+0x03)
#define PAF_ALGERR_COMMON        (PAF_ALGERR+0x04)

#endif /* PAF_ALG_ */
