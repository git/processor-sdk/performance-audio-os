
/*
* Copyright (C) 2004-2017 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Decoder Declarations
//
//
//

#ifndef PAFDEC_
#define PAFDEC_

#include <xdc/std.h>
#include <ti/xdais/xdas.h>
#include <paftyp.h>
#include <inpbuf.h>

typedef struct PAF_DecodeControl {
    Int size;
    PAF_AudioFrame *pAudioFrame;
    XDAS_Int16 frameLength;
    XDAS_Int8 sampleRate;
    XDAS_Int8 emphasis;
    const PAF_InpBufConfig * pInpBufConfig;
} PAF_DecodeControl;

typedef struct PAF_Command {
    XDAS_Int8 action;
    XDAS_Int8 result;
} PAF_Command;

typedef volatile struct PAF_DecodeStatus {
    Int size;
    XDAS_Int8 mode;                                         //4
    XDAS_Int8 command2;                                     //5
    PAF_Command command;//size 2bytes                       //6
    XDAS_Int8 sampleRate;                                   //8
    XDAS_Int8 sourceSelect;                                 //9
    XDAS_Int8 sourceProgram;                                //10
    XDAS_Int8 sourceDecode;                                 //11
    XDAS_Int8 sourceDual;                                   //12
    XDAS_Int8 sourceKaraoke;                                //13
    XDAS_Int8 aspGearControl;                               //14
    XDAS_Int8 aspGearStatus;                                //15
    XDAS_Int8 Unused[4];//can be used                       //16
    XDAS_Int8 Unused2[4];//can be used                      //20
    XDAS_Int8 Unused3[4];//can be used                      //24
    XDAS_Int8 Unused4[4];//can be used                      //28
    XDAS_Int8 Unused5[4];//can be used                      //32
    XDAS_Int32 frameCount;                                  //36
    XDAS_Int8 karaokeCapableUserQ6[6];                      //40
    XDAS_Int8 decBypass;                                    //46
    XDAS_Int8 syncMetadata;                // was Unused6;  //47  CJP:  Moving mdDeFormat to PCM decoder
    XDAS_Int16 frameLength;                                 //48
    XDAS_Int8 bufferRatio;                                  //50
    XDAS_Int8 emphasis;                                     //51
    XDAS_Int16 bufferDrift;                                 //52
    XDAS_Int8 sourceDecode_subType;                         //54
    XDAS_Int8 sourceProgram_subType;                        //55
    PAF_ChannelConfiguration channelConfigurationRequest;   //56
    PAF_ChannelConfiguration channelConfigurationProgram;   //64
    PAF_ChannelConfiguration channelConfigurationDecode;    //72
    PAF_ChannelConfiguration channelConfigurationDownmix;   //80
    PAF_ChannelConfiguration channelConfigurationOverride;  //88
    PAF_ChannelMap_HD channelMap; // not constant size      //96 0x60
    PAF_ProgramFormat programFormat;
} PAF_DecodeStatus;

// Source enumeration:
    // THESE SHOULD BE RENUMBERED TO MATCH IEC+ --Kurt

enum PAF_SOURCE {
    PAF_SOURCE_UNKNOWN,
    PAF_SOURCE_NONE,
    PAF_SOURCE_PASS,
    PAF_SOURCE_SNG,
    PAF_SOURCE_AUTO,
    PAF_SOURCE_BITSTREAM,
    PAF_SOURCE_DTSALL,
    PAF_SOURCE_PCMAUTO,
    PAF_SOURCE_PCM,
    PAF_SOURCE_PC8,
    PAF_SOURCE_AC3,
    PAF_SOURCE_DTS,
    PAF_SOURCE_AAC,
    PAF_SOURCE_MPEG,
    PAF_SOURCE_DTS12,
    PAF_SOURCE_DTS13,
    PAF_SOURCE_DTS14,
    PAF_SOURCE_DTS16,
    PAF_SOURCE_WMA9PRO,
    PAF_SOURCE_MP3,
    PAF_SOURCE_DSD1,
    PAF_SOURCE_DSD2,
    PAF_SOURCE_DSD3,
    PAF_SOURCE_DDP,
    PAF_SOURCE_DTSHD,
    PAF_SOURCE_THD,
    PAF_SOURCE_DXP,
    PAF_SOURCE_WMA,
    PAF_SOURCE_N
};

enum PAF_SOURCE_SUBTYPE {
    PAF_SOURCE_SUBTYPE_UNKNOWN,
    PAF_SOURCE_SUBTYPE_NONE,
    PAF_SOURCE_MATPCM_OBJ,
    PAF_SOURCE_MATPCM_CNL,
    PAF_SOURCE_MATTHD_OBJ,
    PAF_SOURCE_MATTHD_CNL,
    PAF_SOURCE_DDP_OBJ,
    PAF_SOURCE_DDP_CNL,
    PAF_SOURCE_SUBTYPE_N
};

#define PAF_SOURCE_DTS11 PAF_SOURCE_DTS

enum PAF_SOURCE_DUAL {
    PAF_SOURCE_DUAL_STEREO,
    PAF_SOURCE_DUAL_MONO1,
    PAF_SOURCE_DUAL_MONO2,
    PAF_SOURCE_DUAL_MONOMIX,
    PAF_SOURCE_DUAL_N
};

typedef struct PAF_DecodeInStruct {
    XDAS_Int8 outputFlag;
    XDAS_Int8 errorFlag;
    XDAS_Int16 sampleCount;
    PAF_AudioFrame *pAudioFrame;
    PAF_AudioBuffer audioShare;
} PAF_DecodeInStruct;

typedef struct PAF_DecodeOutStruct {
    XDAS_Int8 outputFlag;
    XDAS_Int8 errorFlag;
    PAF_AudioFrame *pAudioFrame;
} PAF_DecodeOutStruct;

#endif  /* PAFDEC_ */
