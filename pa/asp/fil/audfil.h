
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef AUDFIL_
#define AUDFIL_

/* ------------------------------------------------------------------------- */

#include "procsdk_audio_typ.h"
#ifdef ARMCOMPILE
#include <paftyp.h>
#endif
#ifdef _TMS320C6X
#include <paftyp.h>
#define RESTRICT restrict
#else
#define RESTRICT

#ifndef lengthof
#define lengthof(X) (sizeof(X)/sizeof(*(X)))
#endif /* lengthof */

#ifndef NULL
#define NULL 0
#endif

typedef int Int;
typedef short MdInt;
typedef unsigned short MdUns;
#ifndef ARMCOMPILE
typedef void Void;
typedef double PAF_AudioData;
#endif

#endif

/* ......................................................................... */

#define PAF_RETURN_IF_ERROR( x) \
  { \
  const Int err = (x); \
  if( err) \
    return( err); \
  }

/* ------------------------------------------------------------------------- */

#define PAF_FIL_COF \
  MdUns type; \
  MdInt order;

typedef struct {
  PAF_FIL_COF
} PAF_AudioFilterCoefs;

typedef struct {
  PAF_FIL_COF
  float c[1];
} PAF_AudioFilterCoefs_SP;

typedef struct {
  PAF_FIL_COF
  double c[1];
} PAF_AudioFilterCoefs_DP;

typedef float  PAF_AudioFilterState_SP;
typedef double PAF_AudioFilterState_DP;

typedef struct PAF_AudioFilter {
  Int size;
  const PAF_AudioFilterCoefs *pCoefs;
  Void *pState;
} PAF_AudioFilter;

typedef struct PAF_AudioFilter_Fxns {
  Int  (*init)( PAF_AudioFilter *this);
  Int (*check)( const PAF_AudioFilter *this);
  Int (*alloc)( const PAF_AudioFilter *this, Int *sizeState);
  Int (*reset)( const PAF_AudioFilter *this);
  Int (*apply)( const PAF_AudioFilter *this1,const PAF_AudioFilter *this2,
                PAF_AudioData *pData1, PAF_AudioData *pData2, Int num);
} PAF_AudioFilter_Fxns;

extern PAF_AudioFilter_Fxns *PAF_AudioFilter_fxns;

/* ------------------------------------------------------------------------- */

#define PAF_AF_TYPE_FIR (0<<8)

#define PAF_AF_TYPE_IIR4 (2<<8)
#define PAF_AF_TYPE_IIR5 (3<<8)

#define PAF_AF_TYPE_SP (0<<12)	/* single-precision coeffs, data */
#define PAF_AF_TYPE_DP (1<<12)	/* double-precision coeffs, data */

/* ......................................................................... */

#define PAF_AF_ERR_BASE 0x100
#define PAF_AF_ERR_TYPE_UNSUPPORTED (PAF_AF_ERR_BASE+1)

/* ------------------------------------------------------------------------- */

#endif  /* AUDFIL_ */
