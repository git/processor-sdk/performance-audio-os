
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 -- System Stream Function Declarations
//
//
//

#ifndef SS0_
#define SS0_

#include <std.h>

#include <acp.h>
#include <pafsys.h>

#define PAF_SYSTEMSTREAMFXNS_XN 4

struct PAF_SST_Params;
struct PAF_SST_Config;

typedef void PAF_SST_FxnsMain 
    (const struct PAF_SST_Params *, struct PAF_SST_Config *);
typedef Int PAF_SST_FxnsCompute 
    (const struct PAF_SST_Params *, struct PAF_SST_Config *, long long *);
typedef Int PAF_SST_FxnsTransmit 
    (const struct PAF_SST_Params *, struct PAF_SST_Config *, long long *);

typedef struct PAF_SST_Fxns {
    PAF_SST_FxnsMain *main;
    Int count;
    struct {
        PAF_SST_FxnsCompute  *compute;
        PAF_SST_FxnsTransmit *transmit;
    } sub[1];
} PAF_SST_Fxns;

typedef struct PAF_SST_Params {
    SmInt streams;
    SmInt stream1; /* unused */
    SmInt streamN; /* unused */
    SmInt ss;
    const PAF_SST_Fxns *fxns;
} PAF_SST_Params;

typedef struct PAF_SST_Config {
    ACP_Handle acp;
    PAF_SystemStatus *pStatus;
} PAF_SST_Config;

typedef struct PAF_ERR_Config {
    ACP_Handle acp;
    PAF_ErrorStatus *pStatus;
} PAF_ERR_Config;

#endif /* SS0_ */
