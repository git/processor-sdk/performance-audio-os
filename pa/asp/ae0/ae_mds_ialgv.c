
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This file contains the function table definitions for all
 *  interfaces implemented by the AE_MDS module that derive
 *  from IALG
 *
 *  We place these tables in a separate file for two reasons:
 *     1. We want allow one to one to replace these tables
 *        with different definitions.  For example, one may
 *        want to build a system where the AE is activated
 *        once and never deactivated, moved, or freed.
 *
 *     2. Eventually there will be a separate "system build"
 *        tool that builds these tables automatically 
 *        and if it determines that only one implementation
 *        of an API exists, "short circuits" the vtable by
 *        linking calls directly to the algorithm's functions.
 */
#include <xdc/std.h>
#include <ti/xdais/ialg.h>

#include <iae.h>
#include <ae_mds.h>
#include <ae_mds_priv.h>

#define IALGFXNS \
    &AE_MDS_IALG,       /* module ID */                         \
    AE_MDS_activate,    /* activate */                          \
    AE_MDS_alloc,       /* alloc */                             \
    AE_MDS_control,     /* control */                           \
    AE_MDS_deactivate,  /* deactivate */                        \
    AE_MDS_free,        /* free */                              \
    AE_MDS_initObj,     /* init */                              \
    AE_MDS_moved,       /* moved */                             \
    NULL                /* numAlloc (NULL => IALG_MAXMEMRECS) */\

/*
 *  ======== AE_MDS_IAE ========
 *  This structure defines MDS's implementation of the IAE interface
 *  for the AE_MDS module.
 */
const IAE_Fxns AE_MDS_IAE = {       /* module_vendor_interface */
    IALGFXNS,
    AE_MDS_reset,
    AE_MDS_apply,
};

/*
 *  ======== AE_MDS_IALG ========
 *  This structure defines MDS's implementation of the IALG interface
 *  for the AE_MDS module.
 */
#ifdef _TMS320C6X
#ifndef __TI_EABI__
asm("_AE_MDS_IALG .set _AE_MDS_IAE");
#else
asm("AE_MDS_IALG .set AE_MDS_IAE");
#endif
#else

/*
 *  We duplicate the structure here to allow this code to be compiled and
 *  run non-DSP platforms at the expense of unnecessary data space
 *  consumed by the definition below.
 */
IALG_Fxns AE_MDS_IALG = {       /* module_vendor_interface */
    IALGFXNS
};

#endif
