::@ECHO OFF
rem:   launch_ccs.bat
rem:   batch file to launch CCS to build the DCS test app.

rem:  OS_SDK_ROOT is where the open source version of PA resides.
rem:  This directory contains pa, da, etc.
rem:  Use double backslashes so that the path can be used by sed.

rem:  TI_DIR is where different tools reside.
 
rem:  CG_TOOLS is where c6xx compiler reside.


if "%COMPUTERNAME%"=="CPU-158" (
    echo "Launching Code Composer using settings for CPU-158."
    set PAF_ROOT=E:\\K2G_paf\\code\\work\\alpha_cmd\\os_paf_dev_dcs6
    set TI_DIR=C:\\ti
    set DEVICE=66AK2G02
    set PDK_ROOT_PATH=C:\\ti\\pdk_k2g_1_0_1\\packages
    c:
    cd C:\\ti\\ccsv6\\eclipse
    start ccstudio.exe -data E:\\K2G_paf\\code\\work\\alpha_cmd\\workspace_v6_0_dcs7_master
    goto end
)

:end
