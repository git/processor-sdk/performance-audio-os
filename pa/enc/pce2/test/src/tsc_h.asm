*
*
******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************
* ========================================================================= *
*                                                                           *
*   TEXAS INSTRUMENTS, INC.                                                 *
*                                                                           *
*   NAME                                                                    *
*       tsc                                                                 *
*                                                                           *
*   PLATFORM                                                                *
*       C64xPLUS                                                            *
*S                                                                          *
*S  AUTHOR                                                                  *
*S      Oliver Sohm                                                         *
*S                                                                          *
*S  REVISION HISTORY                                                        *
*S      30-Mar-2004 Initial version  . . . . . . . . . . .  Oliver Sohm     *
*                                                                           *
*   USAGE                                                                   *
*       These routines are C callable, and have the following C prototypes: *
*                                                                           *
*           void TSC_enable();                                              *
*           long long TSC_read();                                           *
*                                                                           *
*   DESCRIPTION                                                             *
*       TSC_enable: Enables the Time Stamp Counter                          *
*       TSC_read:   Reads the Time Stamp Counter and returns 64-bit count   *
*                                                                           *
* ========================================================================= *

* ------------------------------------------------------------------------- *
*  TSC_enable                                                               *
* ------------------------------------------------------------------------- *
    .global TSC_enable
    
TSC_enable:

    RETNOP  B3, 4

    MVC     B4, TSCL     ; writing any value enables timer

    
* ------------------------------------------------------------------------- *
*  TSC_read                                                                 *
* ------------------------------------------------------------------------- *
    .global TSC_read

TSC_read:

    RETNOP  B3, 2
    
    DINT               ; don't strictily need DINT/RINT because of branch 
||  MVC TSCL,   B4     ; Read the low half first; high half copied to TSCH

    MVC TSCH,   B5     ; Read the snapshot of the high half
||  RINT
||  MV  B4,  A4
    
    MV  B5,  A5
    
* ========================================================================= *
*   End of file:  tsc                                                       *
* ========================================================================= *
