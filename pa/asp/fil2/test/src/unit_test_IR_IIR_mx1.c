/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  
/* This is meant for functional test of all kinds of filters that come in the IIR category */

// std headers
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <clk.h>
#include <log.h>
#include  <xdc/std.h>
#include <ti/xdais/xdas.h>
#include <paftyp.h>

// fil headers
#include <ifil.h>
#include <fil_tii.h>
#include <fil_tii_priv.h>
#include <filerr.h>
#include <filextern.h>
#include <filtyp.h>
#include <filters.h>
#include <fil_table.h>

#define FIL_TEST_TOTAL         (80  + 8 + 8) // +8 for newly added test cases - Thomas 
                                             // +8 for 16channels cases.
#define FIL_TEST_SAMPLE_COUNT  16
#define FIL_TEST_ITERATIONS    10

// Test table structure
typedef struct 
{
    FIL_filterFnPtr filterFn; // Function ptr that is tested
    Uint type; // type field that will call the function, in FIL
    Int  taps; 
    Int  ch;
    Int  uniCoef;
    Int  inplace;
    Int  dPrec;
    Int  cPrec;
    Int  cascade;
} testTable; 

int generic_IR_filter( PAF_FilParam *, testTable );
int iir_generic( PAF_FilParam * );
int iir_generic1( PAF_FilParam * );

// Constant
PAF_AudioData gInArray[256];
PAF_AudioData gCoeff[256];  

// Ref Output
PAF_AudioData gOutArrayRef[256];
double gVarsRef[256] = {0};

// Test Output 
PAF_AudioData gOutArray[256];
double gVars[256] = {0};

// Allocation for the param pointers 
PAF_AudioData *pIn[PAF_MAXNUMCHAN];
PAF_AudioData *pOut[PAF_MAXNUMCHAN];
double *pVar[PAF_MAXNUMCHAN];
PAF_AudioData *pCoef[PAF_MAXNUMCHAN];

// Param structure
PAF_FilParam gParamTest = {
 (void **)pIn,
 (void **)pOut,
 (void **)pCoef,
 (void **)pVar,
 FIL_TEST_SAMPLE_COUNT, // Int sampleCount
 0,			// Uchar channels
 0			// Uchar use
};




testTable gTestArray[FIL_TEST_TOTAL] = {
//-----------------------------2Tap-Nch---------------------------------------------------
// fiterFn          	type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
{ Filter_iirT2ChN_mx1,   0,      2,     1,      0,         0,      1,        0,       0   }, // 0
{ Filter_iirT2ChN_mx1,   0,      2,     2,      0,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     3,      0,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     4,      0,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     5,      0,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     6,      0,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     7,      0,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     8,      0,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     9,      0,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     10,     0,         0,      1,        0,       0   }, 

// Unicoeff          	type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
{ Filter_iirT2ChN_mx1,   0,      2,     1,      1,         0,      1,        0,       0   }, // 10
{ Filter_iirT2ChN_mx1,   0,      2,     2,      1,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     3,      1,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     4,      1,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     5,      1,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     6,      1,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     7,      1,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     8,      1,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     9,      1,         0,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     10,     1,         0,      1,        0,       0   }, 

// Inplace          	type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
{ Filter_iirT2ChN_mx1,   0,      2,     1,      0,         1,      1,        0,       0   }, // 20
{ Filter_iirT2ChN_mx1,   0,      2,     2,      0,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     3,      0,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     4,      0,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     5,      0,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     6,      0,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     7,      0,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     8,      0,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     9,      0,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     10,     0,         1,      1,        0,       0   }, 

// Unicoeff & Inplace   type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
{ Filter_iirT2ChN_mx1,   0,      2,     1,      1,         1,      1,        0,       0   }, // 30
{ Filter_iirT2ChN_mx1,   0,      2,     2,      1,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     3,      1,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     4,      1,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     5,      1,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     6,      1,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     7,      1,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     8,      1,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     9,      1,         1,      1,        0,       0   }, 
{ Filter_iirT2ChN_mx1,   0,      2,     10,     1,         1,      1,        0,       0   }, 

//-----------------------------4Tap-Nch---------------------------------------------------
{ Filter_iirT4ChN_mx1,   0,      4,     1,      0,         0,      1,        0,       0   }, // 40
{ Filter_iirT4ChN_mx1,   0,      4,     2,      0,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     3,      0,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     4,      0,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     5,      0,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     6,      0,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     7,      0,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     8,      0,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     9,      0,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     10,     0,         0,      1,        0,       0   }, 

// Unicoeff          	type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
{ Filter_iirT4ChN_mx1,   0,      4,     1,      1,         0,      1,        0,       0   }, // 50
{ Filter_iirT4ChN_mx1,   0,      4,     2,      1,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     3,      1,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     4,      1,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     5,      1,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     6,      1,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     7,      1,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     8,      1,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     9,      1,         0,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     10,     1,         0,      1,        0,       0   }, 

// Inplace          	type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
{ Filter_iirT4ChN_mx1,   0,      4,     1,      0,         1,      1,        0,       0   }, // 60
{ Filter_iirT4ChN_mx1,   0,      4,     2,      0,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     3,      0,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     4,      0,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     5,      0,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     6,      0,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     7,      0,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     8,      0,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     9,      0,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     10,     0,         1,      1,        0,       0   }, 

// Unicoeff & Inplace   type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
{ Filter_iirT4ChN_mx1,   0,      4,     1,      1,         1,      1,        0,       0   }, // 70
{ Filter_iirT4ChN_mx1,   0,      4,     2,      1,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     3,      1,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     4,      1,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     5,      1,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     6,      1,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     7,      1,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     8,      1,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     9,      1,         1,      1,        0,       0   }, 
{ Filter_iirT4ChN_mx1,   0,      4,     10,     1,         1,      1,        0,       0   },

// Adding few more test cases for Anthara - Thomas
// fiterFn          	type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
{ Filter_iirT2Ch1_mx1,   0,      2,     1,      0,         0,      1,        0,       0   }, // 80
{ Filter_iirT2Ch1_mx1,   0,      2,     1,      0,         1,      1,        0,       0   }, // 
{ Filter_iirT2Ch1_mx1,   0,      2,     1,      1,         0,      1,        0,       0   }, // 
{ Filter_iirT2Ch1_mx1,   0,      2,     1,      1,         1,      1,        0,       0   }, // 83

// fiterFn          	type    taps    ch    uniCoef   inplace  dPrec    cPrec   cascade
{ Filter_iirT4Ch1_mx1,   0,      4,     1,      0,         0,      1,        0,       0   }, // 84
{ Filter_iirT4Ch1_mx1,   0,      4,     1,      0,         1,      1,        0,       0   }, // 
{ Filter_iirT4Ch1_mx1,   0,      4,     1,      1,         0,      1,        0,       0   }, // 
{ Filter_iirT4Ch1_mx1,   0,      4,     1,      1,         1,      1,        0,       0   }, // 87

// T2ChN (16channel)
{ Filter_iirT2ChN_mx1,   0,      2,     16,      0,         0,      1,        0,       0   }, // 88
{ Filter_iirT2ChN_mx1,   0,      2,     16,      0,         1,      1,        0,       0   }, // 
{ Filter_iirT2ChN_mx1,   0,      2,     16,      1,         0,      1,        0,       0   }, // 
{ Filter_iirT2ChN_mx1,   0,      2,     16,      1,         1,      1,        0,       0   }, // 91

// T4ChN (16channel)
{ Filter_iirT4ChN_mx1,   0,      4,     16,      0,         0,      1,        0,       0   }, // 92
{ Filter_iirT4ChN_mx1,   0,      4,     16,      0,         1,      1,        0,       0   }, // 
{ Filter_iirT4ChN_mx1,   0,      4,     16,      1,         0,      1,        0,       0   }, // 
{ Filter_iirT4ChN_mx1,   0,      4,     16,      1,         1,      1,        0,       0   }, // 95

};


void main( void ) {
    int i, j, k, ret;
    int ch, taps, filType, dPrec, cPrec;
    int testCount;
    FIL_filterFnPtr filFn;
    int testIterations = 10;
    int uniCoef, inplace;
    
    int start, testIndex, testfailure;

    for( i = 0; i < 256; i++ )
        gCoeff[i] = (i+1)*0.00015;
	        
    // Reset vars and output
    for( i = 0; i < 256; i++ ) { 
	gOutArrayRef[i] = 0;
        gVarsRef[i] = 0;
        gOutArray[i] = 0;
        gVars[i] = 0; 
    }
    
	for( start = 0; start < FIL_TEST_TOTAL; start++ ) {
	
		//for( testIndex = 0; testIndex < FIL_TEST_ITERATIONS>>1; testIndex++ ) { 
	    //for( testIndex = 0; testIndex < FIL_TEST_TOTAL; testIndex++ ) { 
		testCount = start ;//+ testIndex;
	        
	        /*if( testCount >= FIL_TEST_TOTAL )
	            testCount = 0;*/
	            
	        goto test_filter;  
	          
            	testfilterback :	            
	            
	} 
	
	goto fil_end;          
    
        
        
test_filter :
	    // Reset vars
	    for( i = 0; i < 256; i++ ) {
	        gOutArrayRef[i] = 0;
	        gVarsRef[i] = 0;
	        gOutArray[i] = 0;
	        gVars[i] = 0;
	    }

		testfailure = 0;

		for( testIterations = 0; testIterations < FIL_TEST_ITERATIONS; testIterations++ ) {
		 // Get a local copy of the fil
		    ch      = gTestArray[testCount].ch;
		    filType = gTestArray[testCount].type;
		    dPrec   = gTestArray[testCount].dPrec;
		    cPrec   = gTestArray[testCount].cPrec;
		    taps    = gTestArray[testCount].taps;
		    uniCoef = gTestArray[testCount].uniCoef;
		    inplace = gTestArray[testCount].inplace;

		    // Get input samples & Get coefficients.
		    for( i = 0; i < 256; i++ ) {
		         gInArray[i] = i*testIterations*0.001;
		         gOutArray[i] = gInArray[i];
		    }

		    		    
		    // Setup the param structure.
		    for( i = 0; i < ch; i++ ) {

		        gParamTest.pOut [i] = gOutArray + i*FIL_TEST_SAMPLE_COUNT;
		        gParamTest.pVar [i] = gVars  + i*taps;
		        
		        if( inplace )
		            gParamTest.pIn [i]  = gOutArray + i*FIL_TEST_SAMPLE_COUNT;
			else
		            gParamTest.pIn  [i] = gInArray  + i*FIL_TEST_SAMPLE_COUNT;
		        
		        if( uniCoef )
		            gParamTest.pCoef[i] = gCoeff;
		        else
		            gParamTest.pCoef[i] = gCoeff + i*( taps*2 + 1 );
		            
		    }  
		    gParamTest.channels = ch;
		    gParamTest.use      = taps;
		          
	            if( gTestArray[testCount].filterFn == 0 )
	               continue;
	            
	            // Do Test filtering.
	            ret = gTestArray[testCount].filterFn( &gParamTest );
	        
	            if( ret == 0 ) { 
			for( i = 0; i < ch; i++ ) { 
				gParamTest.pIn [i] = gInArray + i*FIL_TEST_SAMPLE_COUNT;
			        gParamTest.pOut[i] = gOutArrayRef + i*FIL_TEST_SAMPLE_COUNT;
				gParamTest.pVar[i] = gVarsRef     + i*taps;
			} 
			            
	            	// Do reference filtering.
	            	generic_IR_filter( &gParamTest, gTestArray[testCount] ); 
			
			for( i = 0; i < ch*FIL_TEST_SAMPLE_COUNT; i++ ) { 
				if ( fabs(gOutArrayRef[i] - gOutArray[i] ) > 0.00000005 ){ 
					printf("Test case [%d], Iteration [%d] - failed \n", testCount, testIterations);
			        testfailure = 1;    
			        }
                 /* rvanga Commenting these lines print only when there is a failure....*/   
			   // else
			    //	printf("Test Case of %d for Iteration %d is passed\n", testCount, testIterations);
			}
		    	            
	            }
	        else
	            printf("Error\n");
	            
	    }
         if(testfailure)
           printf("Test case [%d], - failed \n", testCount);
		 else
		   printf("Test case [%d], - passed \n", testCount);

		goto testfilterback;


    fil_end :
            
}


int generic_IR_filter( PAF_FilParam * param, testTable testElement) {
    int i, j, k;
    int ret;

    if( testElement.type )
    ;
    else {
        if( (testElement.dPrec == 1) && (testElement.cPrec == 0) )
            return( iir_generic1( param ) );
        else
            return( iir_generic( param ) );
    }
    
}

int iir_generic( PAF_FilParam * pParam ) {
    Int count;
    Int i, t, taps, ch;
    PAF_AudioData accY, accYtemp, accW;
    PAF_AudioData varW, varW_temp;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtVars, * restrict filtVars0;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    PAF_AudioData filtVarsTemp0, filtVarsTemp1;
    
    count = pParam->sampleCount;
    taps  = pParam->use;
    
    for (ch = 0; ch < pParam->channels; ch++) {
        x = (PAF_AudioData *)pParam->pIn[ch];
        y = (PAF_AudioData *)pParam->pOut[ch];            

        filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;

        filtVars  = (PAF_AudioData *)pParam->pVar[ch];
        
        for (i = 0; i < count; i ++) {
             accY = 0; 
             accW = *x;
             
             for( t = 0; t < taps; t++ ) {
                 accW += filtCfsA[t]   * filtVars[t];  
                 
                 accY += filtCfsB[t+1] * filtVars[t]; 
             }
             
             accY += accW * filtCfsB[0]; 
             
             *y++ = accY;
             x++; 
             
             filtVarsTemp0 = filtVars[0];
             
             for( t = 0; t < taps; t++ ) {
                 filtVarsTemp1 =  filtVars[t + 1];
                 
                 filtVars[t + 1] = filtVarsTemp0;
                 
                 filtVarsTemp0 =  filtVarsTemp1; 
             }   
             filtVars[taps] = filtVarsTemp1;
                       
             
             filtVars[0] = accW;
             
         }
     }
     return( FIL_SUCCESS );      
}
   
int iir_generic1( PAF_FilParam * pParam )
{
    Int count;
    Int i, t, taps, ch;
    double accY, accYtemp, accW;
    double varW, varW_temp;
    double * restrict filtVars, * restrict filtVars0;
    double filtVarsTemp0, filtVarsTemp1;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    count = pParam->sampleCount;
    taps  = pParam->use;
    
    for (ch = 0; ch < pParam->channels; ch++) {
        x = (PAF_AudioData *)pParam->pIn[ch];
        y = (PAF_AudioData *)pParam->pOut[ch];            

        filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;

        filtVars  = (double *)pParam->pVar[ch];
        
        for (i = 0; i < count; i ++) {
             accY = 0; 
             accW = *x * filtCfsB[0];
             
             for( t = 0; t < taps; t++ ) {
                 accW += filtCfsA[t] * filtVars[t];  
                 
                 accY += filtCfsB[t+1] * filtVars[t]; 
             }
             
             accY += accW; 
             
             *y++ = accY;
             x++; 
             
             filtVarsTemp0 = filtVars[0];
             
             for( t = 0; t < taps; t++ ) {
                 filtVarsTemp1 =  filtVars[t + 1];
                 
                 filtVars[t + 1] = filtVarsTemp0;
                 
                 filtVarsTemp0 =  filtVarsTemp1; 
             }   
             filtVars[taps] = filtVarsTemp1;
                       
             
             filtVars[0] = accW;
             
         }
     }
     return( FIL_SUCCESS );      
}
