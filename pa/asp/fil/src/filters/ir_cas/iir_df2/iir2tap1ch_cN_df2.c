/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/*
 *  ======== iir2tap1ch_cN_df2.c ========
 *  IIR filter implementation(DF2) for tap-2 & channels-1, casc-N, SP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 2, CHANNELS 1 and CASCADE N ************************/
/*Single section equations :                                                 */
/*                                                                           */
/* Filter equation  : H(z) =  1  + b1*z~1 + b2*z~2                           */
/*                           ----------------------                          */
/*                            1  - a1*z~1 - a2*z~2                           */
/*                                                                           */
/*   Direct form    : y(n) = x(n) + b1*x(n-1) + a1*y(n-1) + a2*y(n-2)        */
/*                                                                           */
/*   Canonical form : w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                    */
/*                    y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                    */
/*                                                                           */
/*                                                                           */
/* Implementation structure of one cascade section :                         */
/*           g                  w(n)                                         */
/*     x(n)-->---[+]---->--------@-------------[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+    b1     |                            */
/*               [+]----<<----| Z~1 |---->>----[+]                           */
/*                |           +-----+           |                            */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a2    +-----+    b2     |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/* The input gain(b0) of all sections are made into a common input gain.     */
/*****************************************************************************/


/* Header files */
#include "filters.h"
#include "fil_table.h"

#define FIL_IIR_2TAP_1CH_NCASC_CASES 16 /* Number of derived cascaded cases */

typedef struct fil_struct1 
{
    char casc;
    Int (*funcPtr)( PAF_FilParam * );
}fil_struct1;

/* Various cascaded cases, where function pointers are arranged in sequence */
static fil_struct1 const Ch1_Casc2[1] = 
{
    {2, Filter_iirT2Ch1_c2_df2},
};
static fil_struct1 const Ch1_Casc3[1] = 
{
    {3, Filter_iirT2Ch1_c3_df2},
};
static fil_struct1 const Ch1_Casc4[1] = 
{
    {4, Filter_iirT2Ch1_c4_df2},
};
static fil_struct1 const Ch1_Casc5[1] = 
{
    {5, Filter_iirT2Ch1_c5_df2},
};
static fil_struct1 const Ch1_Casc6[2] = 
{
    {2, Filter_iirT2Ch1_c2_df2},
    {4, Filter_iirT2Ch1_c4_ng_df2},
};
static fil_struct1 const Ch1_Casc7[] = 
{
    {3, Filter_iirT2Ch1_c3_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},
};
static fil_struct1 const Ch1_Casc8[] = 
{
    {4, Filter_iirT2Ch1_c4_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},
};
static fil_struct1 const Ch1_Casc9[] = 
{
    {4, Filter_iirT2Ch1_c4_df2,},
    {5, Filter_iirT2Ch1_c5_ng_df2,},
};
static fil_struct1 const Ch1_Casc10[] = 
{
    {5, Filter_iirT2Ch1_c5_df2,},
    {5, Filter_iirT2Ch1_c5_ng_df2,},
};
static fil_struct1 const Ch1_Casc11[] = 
{
    {3, Filter_iirT2Ch1_c3_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},    
};
static fil_struct1 const Ch1_Casc12[] = 
{
    {4, Filter_iirT2Ch1_c4_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},    
};
static fil_struct1 const Ch1_Casc13[] = 
{
    {4, Filter_iirT2Ch1_c4_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},
    {5, Filter_iirT2Ch1_c5_ng_df2,},    
};
static fil_struct1 const Ch1_Casc14[] = 
{
    {4, Filter_iirT2Ch1_c4_df2,},
    {5, Filter_iirT2Ch1_c5_ng_df2,},
    {5, Filter_iirT2Ch1_c5_ng_df2,},    
};
static fil_struct1 const Ch1_Casc15[] = 
{
    {3, Filter_iirT2Ch1_c3_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},        
};
static fil_struct1 const Ch1_Casc16[] = 
{
    {4, Filter_iirT2Ch1_c4_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},
    {4, Filter_iirT2Ch1_c4_ng_df2,},        
};

/* Array of the different cascaded case structure ptrs */
static fil_struct1 * const Ch1_CascN_array[] =
{
    (fil_struct1 *)Ch1_Casc2,
    (fil_struct1 *)Ch1_Casc3,
    (fil_struct1 *)Ch1_Casc4,
    (fil_struct1 *)Ch1_Casc5,
    (fil_struct1 *)Ch1_Casc6,
    (fil_struct1 *)Ch1_Casc7,
    (fil_struct1 *)Ch1_Casc8,
    (fil_struct1 *)Ch1_Casc9,
    (fil_struct1 *)Ch1_Casc10,
    (fil_struct1 *)Ch1_Casc11,
    (fil_struct1 *)Ch1_Casc12,
    (fil_struct1 *)Ch1_Casc13,
    (fil_struct1 *)Ch1_Casc14,
    (fil_struct1 *)Ch1_Casc15,
    (fil_struct1 *)Ch1_Casc16,
};

static Int (* const funcArray[4])( PAF_FilParam * ) = 
{
    Filter_iirT2Ch1_c2_df2,
    Filter_iirT2Ch1_c3_df2,
    Filter_iirT2Ch1_c4_df2,
    Filter_iirT2Ch1_c5_df2,    
};
/*
 *  ======== Filter_iirT2Ch1_cN_df2() ========
 *  IIR filter implementation(DF2) for tap-2 & channels-1, casc-N, SP. 
 */
/* Memory section for the function code */
Int Filter_iirT2Ch1_cN_df2( PAF_FilParam *pParam ) 
{
    Void ** pCoef, ** pVar; /* Handle parameters */
    Int i; /* Channel counter */
    Int error = 0, multiple;
    Int casc, cascN, funcN, rem;
    Int (*fnPtr)( PAF_FilParam * ) ;
    Float *coefPtr, *varPtr, *inPtr;
    
    /* Get handle parameters into temp local variables */
    inPtr   = pParam->pIn[0];    
    pCoef   = pParam->pCoef;
    coefPtr = pCoef[0];
    pVar    = pParam->pVar;
    varPtr  = pVar[0];
    casc    = pParam->use >> 1; /* Get number of cascades */
    
    /* Check whether cascades < 2 */
    if(casc < 2)
        return(1);
    
    if(casc <= FIL_IIR_2TAP_1CH_NCASC_CASES)
    {
         funcN = 0;
         
         for(i = 0; i < casc; funcN++)
         {
             /* Get casc case array */
             cascN = Ch1_CascN_array[casc - 2][funcN].casc;
             fnPtr = Ch1_CascN_array[casc - 2][funcN].funcPtr;
             
             /* Setup coefficients, vars etc */ 
             pCoef[0] = &coefPtr[4*i];
             pVar[0]  = &varPtr[2*i];
             
             /* Call function */
             if(fnPtr)
                 error += fnPtr(pParam);
             else
                 return(FIL_ERROR);
                 
             /* Offset coeff ptr by 1 if gain is present in the initial cascade
                and make further processing inplace */
             if(!i)
             {
                 coefPtr++;
                 pParam->pIn[0] =  pParam->pOut[0];
             }
             
             /* Set counter, and array index */
             i += cascN;
         }
         coefPtr--; /* Correct back the coeff ptr */
    }
    else
    {
        multiple = casc/5;
        rem      = casc - multiple*5;
        cascN    = 0;
        
        /* Do the gain part */
        if(rem == 1)
        {
            error   += Filter_iirT2Ch1_c2_df2(pParam);
            cascN   += 2;
            pCoef[0] = &coefPtr[1 + 4*cascN];
            pVar[0]  = &varPtr[2*cascN];
            pParam->pIn[0] =  pParam->pOut[0];
            
            error   += Filter_iirT2Ch1_c4_ng_df2(pParam);
            cascN   += 4;
            pCoef[0] = &coefPtr[1 + 4*cascN];
            pVar[0]  = &varPtr[2*cascN];
            
            multiple--;
        }
        else
        {        
	        if(rem == 0)
	        {
	            rem = 5;
	            multiple--;
	        }
	        
	        error   += funcArray[rem - 2](pParam);
	        cascN   += rem;
	        pCoef[0] = &coefPtr[1 + 4*cascN];
	        pVar[0]  = &varPtr[2*cascN];
        }
        
        /* Do the non-gain and multiple 5 part */
        pParam->pIn[0] =  pParam->pOut[0];
        
        for(i =0; i < multiple; i++)
        {
            error   += Filter_iirT2Ch1_c5_ng_df2(pParam);
            cascN   += 5;
            pCoef[0] = &coefPtr[1 + 4*cascN];
            pVar[0]  = &varPtr[2*cascN];
        }            
        
    }
    
    /* Put back the handle elements */
    pCoef[0]       = coefPtr;
    pParam->pVar   = pVar;
    pVar[0]        = varPtr;
    pParam->pIn[0] = inPtr;
    
    return(error);     
} /* Int Filter_iirT2Ch1_cN_df2() */
