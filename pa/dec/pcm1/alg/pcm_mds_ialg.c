/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Vendor-specific (MDS) PCM algorithm IALG implementation
//

/*
 *  PCM Module IALG implementation - MDS's implementation of the
 *  IALG interface for the PCM algorithm.
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <xdc/std.h> //<std.h>
#include <ti/xdais/ialg.h> //<ialg.h>
//#include <ti/procsdk_audio/procsdk_audio_typ.h>
#include <procsdk_audio_typ.h>

#include <ipcm.h>
#include <pcm_mds.h>
#include <pcm_mds_priv.h>

#include "paf_ialg.h"


#define NON_CACHE_STATUS


// CJP:  A bit of a hack.  In bypass mode, deformatter can set this.
extern PAF_ChannelConfiguration gBypassConfig;

/*
 *  ======== PCM_MDS_activate ========
 */
  /* COM_TIH_activate */

/*
 *  ======== PCM_MDS_alloc ========
 */
Int PCM_MDS_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IPCM_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IPCM_PARAMS;  /* set default parameters */
    }

    /* Request memory for PCM object */
#ifdef NON_CACHE_STATUS
    memTab[0].size = (sizeof(PCM_MDS_Obj)+3)/4*4 + (sizeof(PCM_MDS_Config)+3)/4*4;
#else
    memTab[0].size = (sizeof(PCM_MDS_Obj)+3)/4*4 + (sizeof(PCM_MDS_Status)+3)/4*4 + (sizeof(PCM_MDS_Config)+3)/4*4;
#endif
    memTab[0].alignment = 4;
    memTab[0].space = IALG_SARAM;
    memTab[0].attrs = IALG_PERSIST;

    memTab[1].size = (sizeof(PCM_MDS_Active)+3)/4*4;
    memTab[1].alignment = 4;
#ifdef NON_CACHE_STATUS
    memTab[1].space = IALG_ENCSDATA;
#else
    memTab[1].space = IALG_SARAM;
#endif
    memTab[1].attrs = IALG_PERSIST;
    
    memTab[2].size = (sizeof(PCM_MDS_Scrach)+3)/4*4;
    memTab[2].alignment = 4;
    memTab[2].space = IALG_SARAM;
    memTab[2].attrs = IALG_SCRATCH;

#ifdef NON_CACHE_STATUS
    memTab[3].size = (sizeof(PCM_MDS_Status)+3)/4*4;
    memTab[3].alignment = 4;
    memTab[3].space = IALG_ENCSDATA;
    memTab[3].attrs = IALG_PERSIST;
#endif

    return (4);
}

/*
 *  ======== PCM_MDS_deactivate ========
 */
  /* COM_TIH_deactivate */

/*
 *  ======== PCM_MDS_free ========
 */
  /* COM_TIH_free */

/*
 *  ======== PCM_MDS_initObj ========
 */
Int PCM_MDS_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    PCM_MDS_Obj *pcm = (Void *)handle;
    const IPCM_Params *params = (Void *)algParams;

    if (params == NULL) {
        params = &IPCM_PARAMS;  /* set default parameters */
    }

#ifdef NON_CACHE_STATUS
    pcm->pStatus = (PCM_MDS_Status *) memTab[3].base;
    pcm->pConfig = (PCM_MDS_Config *)((char *)pcm + (sizeof(PCM_MDS_Obj)+3)/4*4);
#else
    pcm->pStatus = (PCM_MDS_Status *)((char *)pcm + (sizeof(PCM_MDS_Obj)+3)/4*4);
    pcm->pConfig = (PCM_MDS_Config *)((char *)pcm + (sizeof(PCM_MDS_Obj)+3)/4*4 + (sizeof(PCM_MDS_Status)+3)/4*4);
#endif
    pcm->pActive = (PCM_MDS_Active *)memTab[1].base;
    pcm->pScrach = (PCM_MDS_Scrach *)memTab[2].base;


    if (params->pStatus)
        *pcm->pStatus = *params->pStatus;
    else
        pcm->pStatus->size = sizeof(pcm->pStatus);

    if (params->pConfig)
        *pcm->pConfig = *params->pConfig;
    else
        pcm->pConfig->size = sizeof(pcm->pConfig);

    if (params->pActive)
        *pcm->pActive = *params->pActive;
    else
        pcm->pActive->size = sizeof(pcm->pActive);

    if (params->pScrach)
        *pcm->pScrach = *params->pScrach;
    else
        pcm->pScrach->size = sizeof(*pcm->pScrach);

    gBypassConfig.full = 0;

    return (IALG_EOK);
}

/*
 *  ======== PCM_MDS_control ========
 */
Int PCM_MDS_control(IALG_Handle handle, IALG_Cmd cmd, IALG_Status *status)
{
    Int control;
	COM_TII_Obj *obj = (Void *) handle;

	if (cmd == ICOM_GETSTATUSADDRESS2) {
		*(Void **) status = (Void *) obj->pCommon;
		return ((Int) 0);
	}

    control = COM_TII_control(handle, cmd, status);

    if (control < 0) {
        PCM_MDS_Obj *pcm = (Void *)handle;

        switch (cmd)
        {
          case IPCM_MININFO:
            return 384;
          case IPCM_MINSAMGEN:
            return ((Int)pcm->pConfig->minimumFrameLength);
          case IPCM_MAXSAMGEN:
            return ((Int)pcm->pConfig->maximumFrameLength);
          default:
            return ((Int)-1);
        }
    }
    else {
        return (control);
    }
}

/*
 *  ======== PCM_MDS_moved ========
 */
  /* COM_TIH_moved */

