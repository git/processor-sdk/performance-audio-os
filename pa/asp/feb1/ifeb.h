
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the FIL-ASP Example Demonstration algorithm.
 */
#ifndef IFEB_
#define IFEB_

#include <ialg.h>
#include  <std.h>
#include <xdas.h>
#include <alg.h> /* for SIO */

#include "icom.h"
#include "paftyp.h"
#include "fil.h"
#include "fil_tii.h"

/*
 *  ======== IFEB_Obj ========
 *  Every implementation of IFEB *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IFEB_Obj {
    struct IFEB_Fxns *fxns;    /* function list: standard, public, private */
} IFEB_Obj;

/*
 *  ======== IFEB_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IFEB_Obj *IFEB_Handle;

/*
 *  ======== GainDatatype ========
 *  This defines both the precision and datatype of the band gain.
 */
typedef signed char GainDatatype;   

/*
 *  ======== IFEB_Status ========
 *  This Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.  This structure is actually
 *  instantiated and initialized in ieq.c.
 */
typedef volatile struct IFEB_Status {

    Int size;             /* This value must always be here, and must be set to the 
                             total size of this structure in 8-bit bytes, as the 
                             sizeof() operator would do. */
    XDAS_Int8 mode;       /* This is the 8-bit FEB Mode Control Register.  All 
                             Algorithms must have a mode control register.  */
    XDAS_Int8 unused;     /* This 8-bit register is unused, and is not only useful as a 
                             "spare", but also provides 16-bit alignment to the next 
                             value. */
    GainDatatype bandGain[10];  /* This is the array of target band gains */
    Float gainStatus[10];      /* This is the pointer to the array of present band gains */                             

} IFEB_Status;

/*
 *  ======== IFEB_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IFEB_Config {
    Int size; /* Size of IFEB_Config */
} IFEB_Config;

/*
 *  ======== IFEB_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a FEB object.
 *
 *  Every implementation of IFEB *must* declare this structure as
 *  the first member of the implementation's parameter structure.  This structure is actually
 *  instantiated and initialized in ieq.c.
 */
typedef struct IFEB_Params {
    Int size;
    const IFEB_Status *pStatus;
    IFEB_Config *pConfig;
    const IFIL_Params *pFilParams;
} IFEB_Params;

/*
 *  ======== IFEB_PARAMS ========
 *  Default instance creation parameters (defined in ieq.c)
 */
extern const IFEB_Params IFEB_PARAMS;
extern const IFIL_Params IFIL_PARAMS_FEB;

/*
 *  ======== IFEB_Fxns ========
 *  All implementation's of FEB must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is FEB_<vendor>_IFEB, where
 *  <vendor> is the vendor name.
 */
typedef struct IFEB_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IFEB_Handle, PAF_AudioFrame *);
    Int         (*apply)(IFEB_Handle, PAF_AudioFrame *);
    /* private */
    IFIL_Fxns   *filFxns;    
} IFEB_Fxns;

/*
 *  ======== IFEB_Cmd ========
 *  The Cmd enumeration defines the control commands for the FEB
 *  control method.
 */
typedef enum IFEB_Cmd {
    IFEB_NULL                   = ICOM_NULL,
    IFEB_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IFEB_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IFEB_GETSTATUS              = ICOM_GETSTATUS,
    IFEB_SETSTATUS              = ICOM_SETSTATUS
} IFEB_Cmd;

#endif  /* IFEB_ */
