
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common Speaker Location Delay algoritm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the Speaker Location Delay algoritm.
 */
#ifndef IDEL_
#define IDEL_

#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include <ti/xdais/xdas.h>

#include "icom.h"
#include "paftyp.h"

/*
 *  ======== IDEL_Obj ========
 *  Every implementation of IDEL *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IDEL_Obj {
    struct IDEL_Fxns *fxns;    /* function list: standard, public, private */
} IDEL_Obj;

/*
 *  ======== IDEL_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IDEL_Obj *IDEL_Handle;

/*
 *  ======== IDEL_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.
 */
typedef volatile struct IDEL_Status {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 unit;
    XDAS_Int8 numc;
    XDAS_Int8 nums;
    XDAS_UInt16 delay[PAF_MAXNUMCHAN];
    XDAS_UInt16 masterDelay;
    XDAS_Int8 resetFlag;
    XDAS_Int8 unused[3];
} IDEL_Status;

/*
 *  ======== IDEL_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IDEL_Config {
    Int size;
	XDAS_UInt16 sizeofScrach;
	XDAS_UInt8 useDATCopy;
	XDAS_UInt8 delCommonMemNumber;
	XDAS_UInt16 unused;
    union {
        PAF_ChannelMask bits;
        XDAS_Int32 not;
    } mask;
    PAF_DelayState state[1];
} IDEL_Config;

/*
 *  ======== IDEL_Active ========
 *  Active structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IDEL_Active {
    Int size;
    Int flag;
}IDEL_Active;

extern IDEL_Active IDEL_ACTIVE;

/*
 *  ======== IDEL_Scrach ========
 */

typedef struct IDEL_Scrach {
    Int size;
	PAF_AudioData *delScrachPtr;
} IDEL_Scrach;


/*
 *  ======== IDEL_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a DEL object.
 *
 *  Every implementation of IDEL *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */
typedef struct IDEL_Params {
    Int size;
    const IDEL_Status *pStatus;
    const IDEL_Config *pConfig;
    const IDEL_Active *pActive;
    const IDEL_Scrach *pScrach;
} IDEL_Params;

/*
 *  ======== IDEL_PARAMS ========
 *  Default instance creation parameters (defined in idel.c)
 */
extern const IDEL_Params IDEL_PARAMS;
extern const IDEL_Params IDEL_PARAMS_BASIC;
extern const IDEL_Params IDEL_PARAMS_DOLBY;
extern const IDEL_Params IDEL_PARAMS_THX;
extern const IDEL_Params IDEL_PARAMS_C1;

/*
 *  ======== IDEL_Fxns ========
 *  All implementation's of DEL must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is DEL_<vendor>_IDEL, where
 *  <vendor> is the vendor name.
 */
typedef struct IDEL_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IDEL_Handle, PAF_AudioFrame *);
    Int         (*apply)(IDEL_Handle, PAF_AudioFrame *);
} IDEL_Fxns;

/*
 *  ======== IDEL_Cmd ========
 *  The Cmd enumeration defines the control commands for the DEL
 *  control method.
 */
typedef enum IDEL_Cmd {
    IDEL_NULL                   = ICOM_NULL,
    IDEL_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IDEL_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IDEL_GETSTATUS              = ICOM_GETSTATUS,
    IDEL_SETSTATUS              = ICOM_SETSTATUS
} IDEL_Cmd;

#endif  /* IDEL_ */
