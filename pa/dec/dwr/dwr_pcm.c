
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Decode Wrapper functionality for Legacy decoders to work with off-chip input buffer.
//
//
//

#include <std.h>
#include <ialg.h>
#include <dwr_inp.h>
#include <pcm_mds.h>

const IDWR_Params IDWRPCM_PARAMS = {
    sizeof(IDWR_Params),
    (IALG_Fxns *) &PCM_MDS_IPCM,
    NULL,
};


#define IALGFXNS \
    &DWRPCM_TII_IALG,    /* module ID */                         \
    DWR_TII_activate,    /* activate */                          \
    DWR_TII_alloc,       /* alloc */                             \
    DWR_TII_control,     /* control */			         \
    DWR_TII_deactivate,  /* deactivate */                        \
    NULL,                /* free */                              \
    DWR_TII_initObj,     /* init */                              \
    NULL,                /* moved */                             \
    DWRPCM_TII_numAlloc  /* numAlloc (NULL => IALG_MAXMEMRECS) */\

#define IDWRFXNS \
    IALGFXNS, \
    DWR_TII_reset, \
    DWR_TII_info, \
    DWR_TII_decode
    //CPL_setAudioFrame
    

const DWR_TII_Fxns DWRPCM_TII_IDWR_FXNS = {       /* module_vendor_interface */
    IDWRFXNS,
};

#ifdef __TI_EABI__
asm(" .global DWRPCM_TII_IDWRPCM");
asm("DWRPCM_TII_IDWRPCM .set DWRPCM_TII_IDWR_FXNS");    
asm("DWRPCM_TII_IALG .set DWRPCM_TII_IDWR_FXNS");
#else /* _EABI_*/
asm(" .global _DWRPCM_TII_IDWRPCM");
asm("_DWRPCM_TII_IDWRPCM .set _DWRPCM_TII_IDWR_FXNS");    
asm("_DWRPCM_TII_IALG .set _DWRPCM_TII_IDWR_FXNS");
#endif /* _EABI_*/


Int DWRPCM_TII_numAlloc()
{
    IDWR_Params *dwrParams = (IDWR_Params *)&IDWRPCM_PARAMS;
    const IALG_Fxns *decTable = dwrParams->decTable;
    Int n = decTable->algNumAlloc != NULL ? decTable->algNumAlloc() : IALG_DEFMEMRECS;

    return n+3;
}
