
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) Audio Stream Join Algorithm functionality implementation
//
//   Combine multiple audio streams into one in a general manner.
//
//   The only significant difference between an ASJ Algorithm and an ASP
//   Algorithm is that (1) the former references an array of Audio Frames
//   (Audio Frame Array or AFA) while the latter references a single Audio 
//   Frame (Audio Frame Pointer or AFP), and (2) the former combines into
//   the last element of the AFF or "primary" from all elements of the AFF,
//   including both the first N-1 elements or "secondary" as well as the
//   last element or "primary," without altering the first N-1 elements of 
//   the AFF, while the latter modifies the Audio Frame directly via the AFP.
//
//   The "join" implemented here with ASJ Algorithm, Number 1 is controlled
//   for each channel independently as one of the following:
//   * "None" or no mixing,
//   * "Add to" to combine the secondary with the primary by mixing, and
//   * "Into" and "onto" to combine the secondary with the primary without 
//     mixing.
//   For details of the "join" implemented here, see the comments and code
//   of the ASJ_MDS_mixit function below.
//
//   For more information on the implementation of an ASJ Algorithm, see
//   the Custom Audio Stream Join Algorithm, Number 1 which provides a
//   simple example of such implementation.
//
//
//

/*
 *  ASJ Module implementation - MDS implementation of a Audio Stream Join Algorithm.
 */

#include <std.h>
#include <xdas.h>

#include <iasj.h>
#include <asj_mds.h>
#include <asj_mds_priv.h>
#include <asjerr.h>

#include "paftyp.h"
#include "pafmac_a.h"
#include <math.h>
#include <cpl.h>
#include "stdasp.h"

#include "ztop.h"  // hack, to enable 3 input version
#ifdef NUM_AS_OUT_INPUT_STREAMS
 #if NUM_AS_OUT_INPUT_STREAMS  ==  3
  #define THREE_INPUT
 #endif
#endif

#ifndef _TMS320C6X
#include "c67x_cintrins.h"
#endif

// Local symbol definitions

#if PAF_AUDIODATATYPE_FIXED
#define ZERO 0
#else
#define ZERO 0.
#endif

// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// #define ENABLE_TRACE
#ifdef ENABLE_TRACE
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif
// -----------------------------------------------------------------------------


/*
 *  ======== satvol ========
 *  Local implementation of volume saturation operation.
 */

inline Int 
satvol (Int x)
{
    return x < -0x8000 ? -0x8000 : x > 0x7fff ? 0x7fff : x;
}

#ifdef THREE_INPUT

/*
 *  ======== ASJ_MDS_apply ========
 *  MDS's implementation of the apply operation.
 */

Int 
ASJ_MDS_apply(IASJ_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    ASJ_MDS_Obj *asj = (Void *)handle;

    PAF_AudioFrame * restrict pAFO   = &pAudioFrame[0];
    PAF_AudioFrame * restrict pAFI_a = &pAudioFrame[1];
    PAF_AudioFrame * restrict pAFI_b = &pAudioFrame[2];

    // Exit if mode is disabled.
    if (asj->pStatus->mode == 0)
        return 0;

    // Perform mix:
    handle->fxns->mixit (handle, pAFO, pAFI_a, pAFI_b, 1);

#ifdef PAF_PROCESS_ASJ
    PAF_PROCESS_SET (pAudioFrame->sampleProcess, ASJ);
#endif /* PAF_PROCESS_ASJ */

    return 0;
}

/*
 *  ======== ASJ_MDS_reset ========
 *  MDS's implementation of the information operation.
 */
Int 
ASJ_MDS_reset(IASJ_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    ASJ_MDS_Obj *asj = (Void *)handle;

    PAF_AudioFrame * restrict pAFO = &pAudioFrame[0];
    PAF_AudioFrame * restrict pAFI_a = &pAudioFrame[1];
    PAF_AudioFrame * restrict pAFI_b = &pAudioFrame[2];

    // Exit if mode is disabled.
    if (asj->pStatus->mode == 0)
        return 0;

    // Perform mix setup:

    handle->fxns->mixit (handle, pAFO, pAFI_a, pAFI_b, 0);

#ifdef PAF_PROCESS_ASJ
    PAF_PROCESS_SET (pAudioFrame->sampleProcess, ASJ);
#endif /* PAF_PROCESS_ASJ */

    return 0;
}

//
// ASJ_MDS_mixit: perform mix from secondary to primary
//

Int 
ASJ_MDS_mixit(IASJ_Handle handle, PAF_AudioFrame *pAFO, PAF_AudioFrame *pAFI_a, PAF_AudioFrame *pAFI_b, Int apply)
{
    ASJ_MDS_Obj *asj = (Void *)handle;

    Int k;
    Int iChan, oChan;

    Int channelForm;
    Int channelType;
    Int unknownFlag;
    PAF_AudioData chanGainO;
    PAF_AudioData chanGainI;
    PAF_AudioData SigmaGain;

    LgInt iMask_a, iMask_b;
    LgInt oMask;

    Int nChan = pAFO->data.nChannels;    /* == pAFI->data.nChannels */
    Int sampleCount = pAFO->sampleCount; /* == pAFI->sampleCount */ 
        // assert that all sampleCounts are equal.
    PAF_AudioData *samout;
    PAF_AudioData *saminp_a;
    PAF_AudioData *saminp_b;

    if (!pAFI_a->fxns)
    {
    	TRACE((&trace, "ASJ_MDS_mixit.%d: exit on null pAFI_a->fxns,", __LINE__));
    	return 0;
    }
    if (!pAFI_b->fxns)
    {
        TRACE((&trace, "ASJ_MDS_mixit.%d: exit on null pAFI_b->fxns,", __LINE__));
        return 0;
    }
    // determine active channels
    iMask_a = pAFI_a->fxns->channelMask (pAFI_a, pAFI_a->channelConfigurationStream);
    iMask_b = pAFI_b->fxns->channelMask (pAFI_b, pAFI_b->channelConfigurationStream);
    oMask   = pAFO->fxns->channelMask (pAFO, pAFO->channelConfigurationStream);
    TRACE((&trace, "ASJ_MDS_mixit: iMask_a: 0x%x.  iMask_b: 0x%x.  oMask: 0x%x.", iMask_a, iMask_b, oMask));

    //
    // If apply, unconditionally mix A and B into output.
    // If not, copy A into output.
    //

    if (apply) 
    {
    	TRACE((&trace, "ASJ_MDS_mixit.%d: apply", __LINE__));

        // "O" entries are for A channel
        // "I" entries are for B channel

        asj->config.volumeRampO = handle->fxns->volRamp (handle, pAFI_a,
            asj->config.volumeRampO,asj->pStatus->masterGainO,asj->pStatus->rampTimeO);

        asj->config.volumeRampI = handle->fxns->volRamp (handle, pAFI_b,
            asj->config.volumeRampI,asj->pStatus->masterGainI,asj->pStatus->rampTimeI);

        for (oChan=0; oChan < nChan; oChan++) 
        {   // Keep this simple for now.
            // Assume simple stereo channel configuration and 1:1 mix
            
            saminp_a = pAFI_a->data.sample[oChan];
            saminp_b = pAFI_b->data.sample[oChan];
            samout   = pAFO->data.sample[oChan];
            chanGainO = 0.707; // pAFO->fxns->dB2ToLinear(satvol(asj->config.volumeRampO + asj->pStatus->chanGainO[oChan]));
            chanGainI = 0.707; // pAFO->fxns->dB2ToLinear(satvol(asj->config.volumeRampI + asj->pStatus->chanGainI[oChan]));

          #if 1
            for (k=0; k < sampleCount; k++)
                samout[k] = saminp_a[k]*chanGainO + saminp_b[k]*chanGainI;
          #else
            // check parameter order
            CPL_CALL(vecLinear2)(chanGainO, saminp_a,
                                 chanGainI, saminp_b, 
                                 samout, sampleCount);
          #endif
        }
    }
    else 
    {
    	TRACE((&trace, "ASJ_MDS_mixit.%d: !apply", __LINE__));

        for (oChan=0; oChan < nChan; oChan++) 
        {   // Keep this simple for now.
            // Assume simple stereo channel configuration and 1:1 mix

            saminp_a = pAFI_a->data.sample[oChan];
            samout   = pAFO->data.sample[oChan];

          #if 1
            for (k=0; k < sampleCount; k++)
                samout[k] = saminp_a[k];
          #else
            CPL_CALL(ivecCopy)(saminp_a, samout, sampleCount);
          #endif
        }
    }
    return 0;
}
#else  // legacy version

/*
 *  ======== ASJ_MDS_apply ========
 *  MDS's implementation of the apply operation.
 */

Int 
ASJ_MDS_apply(IASJ_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    ASJ_MDS_Obj *asj = (Void *)handle;

    PAF_AudioFrame * restrict pAFI = &pAudioFrame[asj->config.offsetI];
    PAF_AudioFrame * restrict pAFO = &pAudioFrame[asj->config.offsetO];

    if (asj->config.offsetI != 1)
    {
    	asj->config.offsetI = 1;
    	TRACE((&trace, "ASJ_MDS_apply.%d: forcing offsetI to 1,", __LINE__));
    	pAFI = &pAudioFrame[asj->config.offsetI];
    }


    // Exit if mode is disabled.
    if (asj->pStatus->mode == 0)
        return 0;

    // Perform mix:

    handle->fxns->mixit (handle, pAFO, pAFI, 1);

#ifdef PAF_PROCESS_ASJ
    PAF_PROCESS_SET (pAudioFrame->sampleProcess, ASJ);
#endif /* PAF_PROCESS_ASJ */

    return 0;
}

/*
 *  ======== ASJ_MDS_reset ========
 *  MDS's implementation of the information operation.
 */
Int 
ASJ_MDS_reset(IASJ_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    ASJ_MDS_Obj *asj = (Void *)handle;

    PAF_AudioFrame * restrict pAFI = &pAudioFrame[asj->config.offsetI];
    PAF_AudioFrame * restrict pAFO = &pAudioFrame[asj->config.offsetO];

    if (asj->config.offsetI != 1)
    {
    	asj->config.offsetI = 1;
    	TRACE((&trace, "ASJ_MDS_reset.%d: forcing offsetI to 1,", __LINE__));
    	pAFI = &pAudioFrame[asj->config.offsetI];
    }

    // Exit if mode is disabled.
    if (asj->pStatus->mode == 0)
        return 0;

    // Perform mix setup:

    handle->fxns->mixit (handle, pAFO, pAFI, 0);

#ifdef PAF_PROCESS_ASJ
    PAF_PROCESS_SET (pAudioFrame->sampleProcess, ASJ);
#endif /* PAF_PROCESS_ASJ */

    return 0;
}

//
// ASJ_MDS_mixit: perform mix from secondary to primary
//

Int 
ASJ_MDS_mixit(IASJ_Handle handle, PAF_AudioFrame *pAFO, PAF_AudioFrame *pAFI, Int apply)
{
    ASJ_MDS_Obj *asj = (Void *)handle;

    //Int k;
    Int iChan, oChan;

    Int channelForm;
    Int channelType;
    Int unknownFlag;
    PAF_AudioData chanGainO;
    PAF_AudioData chanGainI;
    PAF_AudioData SigmaGain;

    LgInt iMask;
    LgInt oMask;

    Int nChan = pAFO->data.nChannels; /* == pAFI->data.nChannels */
    Int sampleCount = pAFO->sampleCount; /* == pAFI->sampleCount */ 
    PAF_AudioData *samout;
    PAF_AudioData *saminp;

    if (!pAFI->fxns)
    {
    	TRACE((&trace, "ASJ_MDS_mixit.%d: exit on null pAFI->fxns,", __LINE__));
    	return 0;
    }
    iMask = /* Active secondary channels */
        pAFI->fxns->channelMask (pAFI, pAFI->channelConfigurationStream);

    oMask = /* Active primary channels */
        pAFO->fxns->channelMask (pAFO, pAFO->channelConfigurationStream);

    TRACE((&trace, "ASJ_MDS_mixit.%d: enter,", __LINE__));

    //
    // Mix Audio Frame Secondary to Primary (if apply flag is set):
    //
    // Mix for each primary channel I may be, as per given
    // (Form Control Register):
    //
    // (0x0N) "None":   No use of secondary channel N in primary.
    // (0x1N) "Add to": Addition of secondary channel N to primary 
    //                  channel I when both N(2) and I(1) exist.
    // (0x2N) "Into":   Overwriting of secondary chanel N to primary 
    //                  channel I when both N(2) and I(1) exist.
    // (0x3N) "Onto":   Overwriting of secondary chanel N to primary 
    //                  channel I even when I(1) does not exist.
    //
    // The result will be the original primary channel configuration
    // except for mixes of the secondary "onto" the primary, which may 
    // result in Channel Configuration Unknown. Such does zero out
    // channels originally not in the primary not copied from the
    // secondary.
    //
    // Note that the LSN of Form Control Register I acts in a manner
    // similar to a Channel Map To for the operation from the secondary 
    // to the primary, but there is no corresponding Channel Map From,
    // so this isn't really a channel map. Rather, form value I controls
    // the mapping according to the PA/F-standard interpretation of 
    // offset I (PAF_LEFT, PAF_RGHT, PAF_LCTR, PAF_RCTR, etc.).
    //

    if (apply) {
    	TRACE((&trace, "ASJ_MDS_mixit.%d: apply", __LINE__));
 
        asj->config.volumeRampO = handle->fxns->volRamp (handle, pAFO,
            asj->config.volumeRampO,asj->pStatus->masterGainO,asj->pStatus->rampTimeO);

        asj->config.volumeRampI = handle->fxns->volRamp (handle, pAFI,
            asj->config.volumeRampI,asj->pStatus->masterGainI,asj->pStatus->rampTimeI);

      #ifdef ENABLE_TRACE
        channelForm = asj->pStatus->form[0];
        channelType = channelForm >> 4;
        iChan = channelForm & 0x0f;
        saminp = pAFI->data.sample[iChan];
        samout = pAFO->data.sample[0];
        TRACE((&trace, "ASJ_MDS_mixit: [i0]: 0x%x, [o0]: 0x%x", saminp, samout ));
        TRACE((&trace, "ASJ_MDS_mixit[i0] [0]: 0x%x, [16]: 0x%x, [99]: 0x%x", saminp[0], saminp[16], saminp[99] ));
        TRACE((&trace, "ASJ_MDS_mixit[o0] [0]: 0x%x, [16]: 0x%x, [99]: 0x%x", samout[0], samout[16], samout[99] ));

        channelForm = asj->pStatus->form[1];
        channelType = channelForm >> 4;
        iChan = channelForm & 0x0f;
        saminp = pAFI->data.sample[iChan];
        samout = pAFO->data.sample[1];
        TRACE((&trace, "ASJ_MDS_mixit: [i1]: 0x%x, [o1]: 0x%x", saminp, samout ));
        TRACE((&trace, "ASJ_MDS_mixit[i1] [0]: 0x%x, [16]: 0x%x, [99]: 0x%x", saminp[0], saminp[16], saminp[99] ));
        TRACE((&trace, "ASJ_MDS_mixit[o1] [0]: 0x%x, [16]: 0x%x, [99]: 0x%x", samout[0], samout[16], samout[99] ));
      #endif

        // Operate secondary channels to primary as per channel form

        unknownFlag = 0; /* Channel Configuration known */

        for (oChan=0; oChan < nChan; oChan++) {
            channelForm = asj->pStatus->form[oChan];
            channelType = channelForm >> 4;
            iChan = channelForm & 0x0f;
            switch (channelType) {
              case 0: // Type is none from secondary to primary
                break;
              case 1: // Type is add from secondary to primary
                if ((iMask & (1 << iChan)) && (oMask & (1 << oChan))) {
                    saminp = pAFI->data.sample[iChan];
                    samout = pAFO->data.sample[oChan];
                    chanGainO = pAFO->fxns->dB2ToLinear(satvol(asj->config.volumeRampO + asj->pStatus->chanGainO[oChan]));
                    chanGainI = pAFO->fxns->dB2ToLinear(satvol(asj->config.volumeRampI + asj->pStatus->chanGainI[iChan]));

                    if (saminp && samout) {
#if 0
                        for (k=0; k < sampleCount; k++)
                           samout[k] = samout[k]*chanGainO + saminp[k]*chanGainI;
#else
                        CPL_CALL(vecLinear2)(chanGainO, samout,chanGainI,
                            saminp, samout, sampleCount);
#endif
                    }

                    SigmaGain = _fabsf(chanGainO) + _fabsf(chanGainI);
                    if (SigmaGain)
                        pAFO->data.samsiz[oChan] += 2*20*log10(SigmaGain);
                }
                break;
              case 2: // Type is from secondary into primary
                if ((iMask & (1 << iChan)) && (oMask & (1 << oChan))) {
                    saminp = pAFI->data.sample[iChan];
                    samout = pAFO->data.sample[oChan];
                    chanGainI = pAFO->fxns->dB2ToLinear(satvol(asj->config.volumeRampI + asj->pStatus->chanGainI[iChan]));

                    if (saminp && samout) {
#if 0
                        for (k=0; k < sampleCount; k++)
                            samout[k] = saminp[k]*chanGainI;
#else
                        CPL_CALL(vecScale)(chanGainI,saminp,samout,sampleCount);
#endif
                    }

                    SigmaGain = _fabsf(chanGainI);
                    if(SigmaGain)
                        pAFO->data.samsiz[oChan] += 2*20*log10(SigmaGain);
                }
                break;
              case 3: // Type is from secondary onto primary
                if (iMask & (1 << iChan)) {
                    saminp = pAFI->data.sample[iChan];
                    samout = pAFO->data.sample[oChan];
                    chanGainI = pAFO->fxns->dB2ToLinear(satvol(asj->config.volumeRampI + asj->pStatus->chanGainI[iChan]));

                    if (saminp && samout) {
#if 0
                        for (k=0; k < sampleCount; k++)
                            samout[k] = saminp[k]*chanGainI;
#else
                        CPL_CALL(vecScale)(chanGainI,saminp,samout,sampleCount);
#endif
                    }

                    SigmaGain = _fabsf(chanGainI);
                    if(SigmaGain)
                        pAFO->data.samsiz[oChan] += 2*20*log10(SigmaGain);
                }
                else if (oMask & (1 << oChan)) {
                    samout = pAFO->data.sample[oChan];
                    if (samout) {
#if 0
                        for (k=0; k < sampleCount; k++)
                            samout[k] = ZERO;
#else	
                        CPL_CALL(vecSet)(0.0f,samout,sampleCount);
#endif
                    }
                    pAFO->data.samsiz[oChan] = 0x8000;
                }
                if (! (oMask & (1 << oChan))) {
                    unknownFlag = 1; /* Channel Configuration unknown */
                }
                break;
              default:
                /* cases 4..7 reserved for standard joins */
                /* cases 8..15 reserved for non-standard joins */
                break;
            }
        }

        // Clean up if Channel Configuration now unknown

        if (unknownFlag) {
            for (oChan=0; oChan < nChan; oChan++) {
                channelForm = asj->pStatus->form[oChan];
                iChan = channelForm & 0x0f;
                if (! (iMask & (1 << iChan)) && ! (oMask & (1 << oChan))) {
                    if (samout = pAFO->data.sample[oChan]) {
#if 0
                        for (k=0; k < sampleCount; k++)
                            samout[k] = ZERO;
#else	
                        CPL_CALL(vecSet)(0.0f,samout,sampleCount);
#endif
                    }
                    pAFO->data.samsiz[oChan] = 0x8000;
                }
            }
            pAFO->channelConfigurationStream.full = PAF_CC_UNKNOWN;
        }

    }
    else {
 
    	TRACE((&trace, "ASJ_MDS_mixit.%d: !apply", __LINE__));
        // Validate secondary channels to primary as per channel form

        unknownFlag = 0; /* Channel Configuration known */

        for (oChan=0; oChan < nChan; oChan++) {
            channelForm = asj->pStatus->form[oChan];
            channelType = channelForm >> 4;
            iChan = channelForm & 0x0f;
            switch (channelType) {
              case 0: // Type is none from secondary to primary
                break;
              case 1: // Type is add from secondary to primary
                break;
              case 2: // Type is from secondary into primary
                break;
              case 3: // Type is from secondary onto primary
                if ((iMask & (1 << iChan)) && ! (oMask & (1 << oChan))) {
                    unknownFlag = 1; /* Channel Configuration unknown */
                }
                break;
              default:
                /* cases 4..7 reserved for standard joins */
                /* cases 8..15 reserved for non-standard joins */
                break;
            }
        }

        // Set up if Channel Configuration now unknown

        if (unknownFlag) {
            pAFO->channelConfigurationStream.full = PAF_CC_UNKNOWN;
        }

    }

    return 0;
}

#endif // THREE_INPUT
float ASJ_MDS_volRamp(IASJ_Handle handle, PAF_AudioFrame *pAudioFrame, 
                      float volumeRamp,PAF_AudioSize masterGain, XDAS_UInt16 rampTime )
{
    // If requested volume ramp time is non-zero and volume ramp
    // is active (volume master control has changed and current
    // volume level needs to ramp up/down to it), update volume
    // status accordingly:

    if (volumeRamp != masterGain && rampTime > 0) {

        Int rate = pAudioFrame->sampleRate;
        Int cnt = pAudioFrame->sampleCount;

        float frameDbQ1;
        float sampleTime;

        // Calculate audio frame duration in msec
        //   = (sample count / 8) * (8 samples / (x samples/sec)),
        //
        // then convert to required change in 0.5 dB steps (volume is Q1),
        // using given ramp rate, for duration of current audio frame
        //   = 2.0 * frame duration (msec) / ramp rate (msec/dB):
        sampleTime = 1.0f/pAudioFrame->fxns->sampleRateHz(pAudioFrame, rate, PAF_SAMPLERATEHZ_STD);
        frameDbQ1 = ((float )(cnt>>3) * sampleTime * 8000.0f * 2.0f) /
                    (float ) rampTime;

        if (volumeRamp > (float )masterGain) {
            // ramp volume "down," limiting to avoid "overshoot":
            volumeRamp = volumeRamp - (float ) masterGain >= frameDbQ1
                ? volumeRamp -= frameDbQ1
                : masterGain;
        }
        else {
            // ramp volume "up," limiting to avoid "overshoot":
            volumeRamp = (float ) masterGain - volumeRamp >= frameDbQ1
                ? volumeRamp += frameDbQ1
                : masterGain;
        }
    }
    else    // ramp inactive -- use volume master control level:
        volumeRamp = masterGain;

    return volumeRamp;

}
// EOF
