
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX FIFO data structures
//
//
//

#ifndef FIFO_H_ 
#define FIFO_H_ 

#include <fifo.inc>
#include <tistdtypes.h>
#include <dmax_struct.h>

/* FIFO parameter format */
typedef struct fifo_wp{
    Uint32 wp:20;
    Uint32 res:12; 
}fifo_wp;

typedef struct fifo_sz{
    Uint32 size:20;
    Uint32 res0:4;
    Uint32 esize:2;
    Uint32 res1:6;
}fifo_sz;

typedef struct fifo_rp{
    Uint32 rp:20;
    Uint32 res:12; 
}fifo_rp;

typedef struct fifo_fm{
    Uint32 fmark:20;
    Uint32 res0:4;
    Uint32 fmsc:4;
    Uint32 res1:4;
}fifo_fm;

typedef struct fifo_em{
    Uint32 emark:20;
    Uint32 res0:4;
    Uint32 emsc:4;
    Uint32 res1:4;
}fifo_em;

typedef struct fifo_ef{
    Uint32 ef0:1;
    Uint32 res0:7;
    Uint32 ef1:1;
    Uint32 res1:7;
    Uint32 ef2:1;
    Uint32 res2:7;
    Uint32 ff:1;
    Uint32 res3:7;
}fifo_ef;

typedef struct fifo{
    Uint32 base;
    union{
        Uint32 full;
        fifo_wp part;
    }wp;
    union{
        Uint32 full;
        fifo_sz part;
    }sz;
    union{
        Uint32 full;
        fifo_rp part;
    }rp;
    union{
        Uint32 full;
        fifo_fm part;
    }fm;
    union{
        Uint32 full;
        fifo_em part;
    }em;
    union{
        Uint32 full;
        fifo_ef part;
    }ef;
}fifo;

/* FIFO API declarations */
extern Int32 dMAX_fifoAllocFSC(dMAX_Handle handle,Int32 fsc);
extern Uint32 dMAX_fifoChkFSC(dMAX_Handle handle,Uint32 fsc);
extern void dMAX_fifoClrFSC(dMAX_Handle handle,Uint32 fsc);
extern void dMAX_fifoFreeFSC(dMAX_Handle handle,Uint32 fsc);
#endif // FIFO_H_

