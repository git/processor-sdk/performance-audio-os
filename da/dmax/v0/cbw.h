
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX CBW Service Routine data structures
//
//
//

#ifndef CBW_H_ 
#define CBW_H_ 

#include <cbw.inc>
#include <fifo.h>
#include <tistdtypes.h>
#include <dmax_struct.h>

/* CBW macros */
#define CBW_MKEE(QTSL,SYNC,TCC,ATCINT,TCINT,RLOAD,EWM,PTE,PP,ETYPE) \
    ((((QTSL)&CBW_QTSL_MSK)<<CBW_QTSL_SHFT) |       \
     (((SYNC)&CBW_SYNC_MSK)<<CBW_SYNC_SHFT) |       \
     (((PP)>>4&CBW_PP_P3_MSK)<<CBW_PP_P3_SHFT) |    \
     (((TCC)&CBW_TCC_MSK)<<CBW_TCC_SHFT) |          \
     (((ATCINT)&CBW_ATCINT_MSK)<<CBW_ATCINT_SHFT) | \
     (((TCINT)&CBW_TCINT_MSK)<<CBW_TCINT_SHFT) |    \
     (((PP)>>3&CBW_PP_P2_MSK)<<CBW_PP_P2_SHFT) |    \
     (((RLOAD)&CBW_RLOAD_MSK)<<CBW_RLOAD_SHFT) |    \
     (((EWM)&CBW_EWM_MSK)<<CBW_EWM_SHFT) |          \
     (((PTE)>>2&CBW_PTE_MSK)<<CBW_PTE_SHFT) |       \
     (((PP)&CBW_PP_P1_MSK)<<CBW_PP_P1_SHFT) |       \
     (((ETYPE)&CBW_ETYPE_MSK)<<CBW_ETYPE_SHFT))

/* CBW parameter format */
typedef struct cbw_cnt{
    Uint32 cnt0:16;
    Uint32 cnt1:15;
    Uint32 rs:1;
}cbw_cnt;

typedef struct cbw{
    Uint32 src;
    fifo *fd;
    union{
        Uint32 full;
        cbw_cnt part;
    }cnt;
    Int16 src_idx0;
    Uint16 res1;
    Int16 src_idx1;
    Uint16 res2;
    union{
        Uint32 full;
        cbw_cnt part;
    }cnt_ref;
    Uint32 src_rld0;
    Uint32 src_rld1;
    Uint32 dt0;
    Uint32 dt1;
    Uint32 res3;
}cbw;

/* CBW API declarations */
extern Uint32 dMAX_cbwOpen(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                           Uint32 pp,Uint32 tcc,Fxn fxn);
extern void dMAX_cbwClose(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 pp,Uint32 tcc,Uint32 pt);
extern void dMAX_cbwCfgEE(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 ee);
extern void dMAX_cbwCfgPT(dMAX_Handle handle,Uint32 pt,cbw *pcbw);
extern void dMAX_cbwCopy(dMAX_Handle handle,Uint32 evt);
extern void dMAX_cbwNoWait(dMAX_Handle handle,Uint32 tcc);
#endif // CBW_H_

