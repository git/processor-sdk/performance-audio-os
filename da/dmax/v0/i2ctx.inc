/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef I2CTX_
#define I2CTX_

// The I2CTX service routine facilitates I2C transmit transfer. The
// following Parameter Table is used to specify I2CTX transfer. 
//
// 31        24        16        8         0  
//  _______________________________________
// |       RES         | TXPEND  | CTRL    |
// |___________________|_________|_________|
// |                  DXR                  |
// |_______________________________________|
// |                  BUF                  |
// |_______________________________________|
// | TCC     | TCINT   |       LEN         |
// |_________|_________|___________________|
// | PAYBIG  | PAYNUM  |       PAYCNT      |
// |_________|_________|___________________|
// |     FULLLEN       |       RPTR        |
// |_________ _________|___________________|
// |     AVALEN        |       WPTR        |
// |___________________|___________________|
// | RES1    | NEWNUM  |       NEWLEN      |
// |_________|_________|___________________|
// |               EMPTYREG                |
// |_______________________________________|
// |               EMPTYVAL                |
// |_______________________________________|
//

// I2CTX parameter field lengths and offsets
#define I2CTX_CTRL_OFF    0      // I2CTX ctrl offset
#define I2CTX_CTRL_LEN    1      // I2CTX ctrl length
#define I2CTX_TXPEND_OFF  1      // I2CTX txpend offset
#define I2CTX_TXPEND_LEN  1      // I2CTX txpend length 
#define I2CTX_RES_OFF     2      // I2CTX res offset
#define I2CTX_RES_LEN     2      // I2CTX res length
#define I2CTX_DXR_OFF     4      // I2CTX dxr offset
#define I2CTX_DXR_LEN     4      // I2CTX dxr length
#define I2CTX_BUF_OFF     8      // I2CTX buf offset
#define I2CTX_BUF_LEN     4      // I2CTX buf length
#define I2CTX_LEN_OFF     12     // I2CTX len offset
#define I2CTX_LEN_LEN     2      // I2CTX len length
#define I2CTX_TCINT_OFF   14     // I2CTX tcint offset
#define I2CTX_TCINT_LEN   1      // I2CTX tcint length
#define I2CTX_TCC_OFF     15     // I2CTX tcc offset
#define I2CTX_TCC_LEN     1      // I2CTX tcc length
#define I2CTX_PAYCNT_OFF  16     // I2CTX paycnt offset
#define I2CTX_PAYCNT_LEN  2      // I2CTX paycnt length
#define I2CTX_PAYNUM_OFF  18     // I2CTX paynum offset
#define I2CTX_PAYNUM_LEN  1      // I2CTX paynum length
#define I2CTX_PAYBIG_OFF  19     // I2CTX paybig offset
#define I2CTX_PAYBIG_LEN  1      // I2CTX paybig length 
#define I2CTX_RPTR_OFF    20     // I2CTX rptr offset
#define I2CTX_RPTR_LEN    2      // I2CTX rptr length
#define I2CTX_FULLLEN_OFF 22     // I2CTX fulllen offset
#define I2CTX_FULLLEN_LEN 2      // I2CTX fulllen length
#define I2CTX_WPTR_OFF    24     // I2CTX wptr offset
#define I2CTX_WPTR_LEN    2      // I2CTX wptr length
#define I2CTX_AVALEN_OFF  26     // I2CTX avalen offset
#define I2CTX_AVALEN_LEN  2      // I2CTX avalen length
#define I2CTX_NEWLEN_OFF  28     // I2CTX newlen offset
#define I2CTX_NEWLEN_LEN  2      // I2CTX newlen length
#define I2CTX_NEWNUM_OFF  30     // I2CTX newnum offset
#define I2CTX_NEWNUM_LEN  1      // I2CTX newnum length
#define I2CTX_RES1_OFF    31     // I2CTX res1 offset
#define I2CTX_RES1_LEN    1      // I2CTX res1 length
#define I2CTX_EMPTYREG_OFF 32    // I2CTX emptyreg offset
#define I2CTX_EMPTYREG_LEN 4     // I2CTX emptyreg length
#define I2CTX_EMPTYVAL_OFF 36    // I2CTX emptyval offset
#define I2CTX_EMPTYVAL_LEN 4     // I2CTX emptyval length

#endif // I2XTX_
