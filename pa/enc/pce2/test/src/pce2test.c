/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// PCE algoritm functionality implementation
//
//
//

/*
 *  PCE2 Module - implements all functions and defines all constant
 *  structures common to all PCM Encoder algoritm implementations.
 */
#include <stdio.h>
#include <math.h>
#include <xdc/std.h>
#include <alg.h>
#include <util_file_io.h>
#include "util_paf_file_io.h"
#include <paftyp.h>
#include <pafdec.h>
#include <ccm.h>
#include "pce2enc.h"
#define PAF_DEVICE 0xD8000000
#include <pafhjt.h>
#include <pce2test.h>
#include <stdasp.h>
#include "util_c66x_cache.h"


/*
 *  ======== Macros ========
 */
#define OUT_BUF_SIZE 0x18000
#define PROFILER                // profile control

#define TMS320C6X               // platform
//#define TRACE_CHANNEL_MASKS   // trace control
#define VOLUME 0
#define DELAY 1
#define OUTPUT 2
#define FRAME_SIZE 256
//#define MAX_CHANNEL 32
#define RAMPTIME 1
#define PRECISION_16 16
#define PRECISION_24 24
//#define PAF_AUDIODATATYPE PAF_AUDIODATATYPE_FLOAT  // audio data type
#define MDB2_CORRECTION (3.0103 / 3)
#define CFG_STEREO 0            // skeaper config control
// Test case control
// Value   Vol Del Output
//   1 -   1    1    1
//   2 -   1    1    0
//   3 -   0    1    0
#define TEST_CASE 1


#if TEST_CASE == 1
	#define VOL_MODE 1
	#define DEL_MODE 1
	#define OUT_MODE 1
#elif TEST_CASE == 2
	#define VOL_MODE 1
	#define DEL_MODE 1
	#define OUT_MODE 0
#else
	#define VOL_MODE 0
	#define DEL_MODE 1
	#define OUT_MODE 0
#endif


#if CFG_STEREO != 1
	#define INPUT_STREAM_NAME "../../test_vectors/input/Sine_48Khz.wav"
#else
	#define INPUT_STREAM_NAME "../../test_vectors/input/Sine_48Khz_stereo.wav"
#endif


#ifdef PROFILER
#include <util_profiling.h>
#endif

const  PAFHJT_t *pafhjt;
//PAFHJT_t pafhjt_app;


#ifdef TMS320C6X
#include <mathf.h>
#else
#define exp10f(a)	pow(10, a)
#include <math.h>
#endif

/*
 *  ======== trace ========
 */
#ifdef TRACE_CHANNEL_MASKS
   #include "dp.h"
   #define TRACE(a) dp a
#else
  #define TRACE(a)
#endif

extern const IPCE_Params IPCE_PARAMS;
extern const IPCE_Status IPCE_PARAMS_STATUS_BASIC;
extern const IPCE_Config IPCE_PARAMS_CONFIG_BASIC;
extern const IPCE_Active IPCE_PARAMS_ACTIVE_BASIC;
extern const IPCE_Scrach IPCE_PARAMS_SCRACH_BASIC;
extern const PAF_EncodeStatus ENCODESTATUS;
extern const PAF_OutBufConfig OUTBUFCONFIG;
extern const PAF_VolumeStatus PAF_VOLUME_STATUS;



#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif /* PAF_AUDIODATATYPE_FIXED */

pce2TestTable gTestArray[] =
{
  /* numSat numSub vol del output*/
	 0,     0,     0,  0,  0,  //PAF_CC_SAT_UNKNOWN,
	 0,     0,     1,  1,  0,    //PAF_CC_SAT_NONE,
	 1,     0,     1,  1,  0,    //PAF_CC_SAT_MONO,       // center only
	 2,     0,     1,  1,  0,    //PAF_CC_SAT_STEREO,     // L, R
	 3,     0,     1,  1,  0,    //PAF_CC_SAT_PHANTOM1,   // L, R, single surround
	 4,     0,     1,  1,  0,    //PAF_CC_SAT_PHANTOM2,   // L, R, single surround, single back
	 5,     0,     1,  1,  0,    //PAF_CC_SAT_PHANTOM3,   // L, R, L/R surround, single back
	 6,     0,     1,  1,  0,    //PAF_CC_SAT_PHANTOM4,   // L, R, L/R surround, L/R back
	 3,     0,     1,  1,  0,    //PAF_CC_SAT_3STEREO,    // L, R, C
	 4,     0,     1,  1,  0,    //PAF_CC_SAT_SURROUND1,  // L, R, C, single surround
	 5,     0,     1,  1,  0,    //PAF_CC_SAT_SURROUND2,  // L, R, C, single surround, single back
	 6,     0,     1,  1,  0,    //PAF_CC_SAT_SURROUND3,  // L, R, C, L/R surround, single back
	 7,     0,     1,  1,  0,    //PAF_CC_SAT_SURROUND4,  // L, R, C, L/R surround, L/R back
	 0,     0,     1,  1,  0,    //PAF_CC_SAT_N
};
PAF_AudioFunctions afFxns = {
	PAF_ASP_dB2ToLinear,
	PAF_ASP_channelMask,
	PAF_ASP_programFormat,
	PAF_ASP_sampleRateHz
};
/*typedef struct ActivePhaseOutputLocal {
    Int size;
    PAF_EncodeStatus EncodeStatus;
    PAF_OutBufConfig OutBufConfig;
} ActivePhaseOutputLocal;*/

int main()
{
	IPCE_Params pceParams,*pPceParams;
	PAF_AudioFrame audioFrame;
    Int error = 0;
    Int count = 0;
    Int testCount = 0;
	IPCE_Handle handle;
    PAF_EncodeInStruct inStruct;
    PAF_EncodeOutStruct outStruct;
	PAF_EncodeControl Control;
	PAF_EncodeStatus *pEncStatus;
	PAF_IALG_Config ialgConfig;
	XDAS_Int8 sat;
	PAF_ActivePhaseOutput ActivePhaseOutput;
	PAF_ActivePhaseVolume ActivePhaseVolume;
	/* Status structures */
	IPCE_Status status; /* no pointers as elements */
	/* Config structures */
	IPCE_Config config; /* scale uninitialized*/

	//IPCE_ConfigPhase ConfigPhase[IPCE_PHASES]; /* volume delay configuration */
	//PAF_IALG_Common ConfigCommon[IPCE_PHASES]; /* size and flag */
	//IPCE_ConfigPhaseVolume ConfigPhaseVolume; /* not a pointer in interface structure */
	//IPCE_ConfigPhaseDelay  ConfigPhaseDelay;  /* not a pointer in interface structure */
	//IPCE_ConfigPhaseOutput ConfigPhaseOutput;  /* not a pointer in interface structure */

	/* active structures */
	IPCE_Active active;

	//PAF_ActivePhase ActivePhase[IPCE_PHASES];
	//PAF_ActivePhaseVolume ActivePhaseVolume;
	//PAF_ActivePhaseDelay  ActivePhaseDelay; /* only size */
	//PAF_ActivePhaseOutput ActivePhaseOutput;
	//PAF_VolumeStatus VolumeStatus;
	//PAF_EncodeStatus EncodeStatus;
	PAF_OutBufConfig  *pOutBufConfig;//OutBufConfig,

	/* scratch structures */
	IPCE_Scrach scrach;
	//PAF_ScrachPhase ScrachPhase[IPCE_PHASES];
	PAF_ScrachPhaseVolume ScrachPhaseVolume; /* only size */
	PAF_ScrachPhaseDelay  ScrachPhaseDelay; /* only size */
	PAF_ScrachPhaseOutput ScrachPhaseOutput; /* only size */
    FILE *pOutputFile2,*pOutputFile3;
	int chunk_size = 0;
	int subchunk2_size = 0;
    char *pOutFile2 =  "../../test_vectors/output/VolDelOut.tmp";
    char *pOutFile3 =  "../../test_vectors/output/VolDelOut.wav";
    char byte;
#ifdef PROFILER
    tPrfl profiler;
#endif

    // cache configuration
    memarchcfg_cacheEnable();
	/*
	 *  ======== Allocating input and ouput file handle ========
	 */
	tFIOHandle *pFIO = (tFIOHandle*)calloc(1, sizeof(tFIOHandle));
	tFIOHandle *pFIOtmp = (tFIOHandle*)calloc(1, sizeof(tFIOHandle));
	pOutputFile2 = fopen(pOutFile2,"wb");

	memset(&profiler, 0, sizeof(tPrfl));
	memset(&handle, 0, sizeof(IPCE_Handle));
	memset(&inStruct, 0, sizeof(PAF_EncodeInStruct));
	memset(&outStruct, 0, sizeof(PAF_EncodeOutStruct));
	memset(&Control, 0, sizeof(PAF_EncodeControl));
	memset(&ialgConfig, 0, sizeof(PAF_IALG_Config));
	memset((void *)&status, 0, sizeof(IPCE_Status));
	memset(&config, 0, sizeof(IPCE_Config));
	memset(&active, 0, sizeof(IPCE_Active));
	memset(&scrach, 0, sizeof(IPCE_Scrach));
	memset(&audioFrame, 0, sizeof(PAF_AudioFrame));

    inStruct.pAudioFrame= &audioFrame;
    pafhjt = &PAFHJT_RAM;
	//PCE_TII_IPCE_Test = PCE_TII_IPCE;
	pafhjt = &PAFHJT_RAM;

	/*
	 *  audio frame initialization
	 */

	audioFrame.mode = 1;
	audioFrame.sampleRate = PAF_SAMPLERATE_48000HZ;
	audioFrame.sampleCount = FRAME_SIZE;
	audioFrame.data.nChannels = PAF_MAXNUMCHAN_AF;
	audioFrame.data.nSamples = FRAME_SIZE;
    audioFrame.data.samsiz = (PAF_AudioSize *)malloc(sizeof(PAF_AudioSize) * PAF_MAXNUMCHAN_AF);
	audioFrame.fxns = &afFxns;
    audioFrame.pChannelConfigurationMaskTable = &PAF_ASP_stdCCMT;
#if SPK_CFG_STEREO
    // 7.1 speaker config
	audioFrame.channelConfigurationStream.part.sat = sat = PAF_CC_SAT_SURROUND4;
	audioFrame.channelConfigurationStream.part.sub = PAF_CC_SUB_ONE;
#else
	// Stereo speaker config
	audioFrame.channelConfigurationStream.part.sat = sat = PAF_CC_SAT_STEREO;
	audioFrame.channelConfigurationStream.part.sub = PAF_CC_SUB_ZERO;
#endif

	audioFrame.channelConfigurationRequest.part.sat = audioFrame.channelConfigurationStream.part.sat;
	audioFrame.channelConfigurationRequest.part.sub = audioFrame.channelConfigurationStream.part.sub;
    // Call apply() function of ASP, if ASP is Enabled and active then after apply call
    // the channel configurations should be updated to
	//audioFrame.channelConfigurationStream.part.sat = PAF_CC_SAT_SURROUND2;
	//audioFrame.channelConfigurationStream.part.sub = PAF_CC_SUB_ONE;
	//audioFrame.channelConfigurationRequest.part.sat = PAF_CC_SAT_SURROUND2;
	//audioFrame.channelConfigurationRequest.part.sub = PAF_CC_SUB_ONE;

	/*
	 *  registering input output file names to read/write and
	 */
    error = PAF_FIO_Init(pFIO, INPUT_STREAM_NAME, "../../test_vectors/output/VolDel.wav");
    pFIO->inBufSampleCount = FRAME_SIZE;
    pFIO->outBufSampleCount = FRAME_SIZE;

    error |= PAF_FIO_Config(pFIO,&audioFrame, gTestArray[sat].numSat +
    		                                             audioFrame.channelConfigurationStream.part.sub );

    if (error != RETRUN_SUCCESS)
    {
       return 0;
    }
    //pOutBufTmp  = (char*) calloc(FRAME_SIZE * STRIDE, sizeof(int) );
	/*
	 *  assigning default values to all the params. pPceParams can be
	 *  reassigned with pceParams with user defined params.
	 */
    pceParams = IPCE_PARAMS;
    status = IPCE_PARAMS_STATUS_BASIC;
    config = IPCE_PARAMS_CONFIG_BASIC;
    active = IPCE_PARAMS_ACTIVE_BASIC;
    scrach = IPCE_PARAMS_SCRACH_BASIC;
    ActivePhaseOutput.pEncodeStatus =  (PAF_EncodeStatus *)&ENCODESTATUS;
    ActivePhaseOutput.pOutBufConfig = (PAF_OutBufConfig *)&OUTBUFCONFIG;
    ActivePhaseOutput.size = sizeof(PAF_ActivePhaseOutput);
    ActivePhaseVolume.pVolumeStatus = (PAF_VolumeStatus *)&PAF_VOLUME_STATUS;
    ActivePhaseVolume.size =    sizeof(PAF_ActivePhaseVolume);

    (active.pActivePhase[OUTPUT]) = ( PAF_ActivePhase  *)&ActivePhaseOutput;
    (active.pActivePhase[VOLUME]) = ( PAF_ActivePhase  *)&ActivePhaseVolume;
    pPceParams =(IPCE_Params *) &pceParams;

    pPceParams->pStatus = &status;
    pPceParams->pConfig = &config;
    pPceParams->pActive = &active;
    pPceParams->pScrach = &scrach;

    // Enable disable modes here
    status.phase[VOLUME].mode = VOL_MODE;
    status.phase[DELAY].mode = DEL_MODE;
    status.phase[OUTPUT].mode = OUT_MODE;


    if (status.phase[OUTPUT].mode == 1)
    {
	  pOutBufConfig = ((PAF_ActivePhaseOutput *)active.pActivePhase[OUTPUT])->pOutBufConfig;

    }

    pPceParams->pScrach->pScrachPhase[VOLUME]->pScrachPhaseData.pScrachPhaseVolume = &ScrachPhaseVolume;
    pPceParams->pScrach->pScrachPhase[DELAY]->pScrachPhaseData.pScrachPhaseDelay = &ScrachPhaseDelay;
    pPceParams->pScrach->pScrachPhase[OUTPUT]->pScrachPhaseData.pScrachPhaseOutput = &ScrachPhaseOutput;


    config.frameLength = FRAME_SIZE;

    /* VOLUME CONTROL Settings*/

    ((PAF_ActivePhaseVolume *)active.pActivePhase[VOLUME])->pVolumeStatus->master.control = -20;//DB
    ActivePhaseVolume.pVolumeStatus->trim[PAF_LEFT].control = -20; //DB
    ActivePhaseVolume.pVolumeStatus->trim[PAF_RGHT].control = -20;//DB
    ActivePhaseVolume.pVolumeStatus->trim[PAF_CNTR].control = -20; //DB
    ActivePhaseVolume.pVolumeStatus->trim[PAF_LSUR].control = -20;//DB
    ActivePhaseVolume.pVolumeStatus->trim[PAF_RSUR].control = -20; //DB
    ActivePhaseVolume.pVolumeStatus->trim[PAF_LBAK].control = -20;//DB
    ActivePhaseVolume.pVolumeStatus->trim[PAF_RBAK].control = -20; //DB
    ActivePhaseVolume.pVolumeStatus->trim[PAF_SUBW].control = -20;//DB
    audioFrame.data.samsiz[PAF_LEFT] = -20;//DB :
    audioFrame.data.samsiz[PAF_RGHT] = -20;//DB
    audioFrame.data.samsiz[PAF_CNTR] = -20;//DB
    audioFrame.data.samsiz[PAF_LSUR] = -20;//DB
    audioFrame.data.samsiz[PAF_RSUR] = -20;//DB
    audioFrame.data.samsiz[PAF_LBAK] = -20;//DB
    audioFrame.data.samsiz[PAF_RBAK] = -20;//DB
    audioFrame.data.samsiz[PAF_SUBW] = -20;//DB

    /* DELAY CONTROL Settings: Delay module converts float to 24 bit */
    /* fixed point value along with delay functionality              */

    status.del.masterDelay = 0;//32; //number of samples to delay
    status.del.delay[PAF_LEFT] = 0;//64;//number of samples to delay
    status.del.delay[PAF_RGHT] = 0;//64;//number of samples to delay
    status.del.delay[PAF_CNTR] = 0;//64;//number of samples to delay
    status.del.delay[PAF_LSUR] = 5;//64;//number of samples to delay
    status.del.delay[PAF_RSUR] = 5;//64;//number of samples to delay
    status.del.delay[PAF_LBAK] = 5;//64;//number of samples to delay
    status.del.delay[PAF_RBAK] = 5;//64;//number of samples to delay
    status.del.delay[PAF_SUBW] = 5;//64;//number of samples to delay
    status.del.unit = 0; // time unit selection

    // active.bitstreamMask.bits = // this will be updated in delay module
	config.pConfigPhase[DELAY]->pConfigPhaseData.pConfigPhaseDelay.mask.bits =
    		audioFrame.fxns->channelMask (&audioFrame,
    				audioFrame.channelConfigurationStream);

	/*
	 *  creating asp module
	 */
    ialgConfig.clr=0;
	handle = (IPCE_Handle)PCE_create((IPCE_Fxns *)&PCE_TII_IPCE,
			                          pPceParams, &ialgConfig);
	if (handle == NULL)
	{
		return 0;
	}

    if (status.phase[OUTPUT].mode == 1)
    {
      pOutBufConfig->base.pVoid       = &pFIO->outBuf[0];
      pOutBufConfig->pntr.pVoid       = &pFIO->outBuf[0];
      pOutBufConfig->head.pVoid       = &pFIO->outBuf[0];
      pOutBufConfig->allocation       = OUT_BUF_SIZE;
      pOutBufConfig->sizeofElement    = 3;
      pOutBufConfig->precision        = PRECISION_24;
      pOutBufConfig->stride        = STRIDE;
      pOutBufConfig->putCount      = 0;
    }

    Control.pAudioFrame = (PAF_AudioFrame *)inStruct.pAudioFrame;

    Control.size = sizeof(PAF_EncodeControl);
    Control.pVolumeStatus = ActivePhaseVolume.pVolumeStatus;
    Control.pOutBufConfig = ActivePhaseOutput.pOutBufConfig;
    Control.frameLength = config.frameLength;
    Control.encActive = 1; // not used now

    pEncStatus = ActivePhaseOutput.pEncodeStatus;
    pEncStatus->channelConfigurationRequest.part.sat = audioFrame.channelConfigurationRequest.part.sat ;
    pEncStatus->channelConfigurationRequest.part.sub = audioFrame.channelConfigurationRequest.part.sub ;
    /*pEncStatus->channelConfigurationEncode.part.sat = audioFrame.channelConfigurationRequest.part.sat ;
    pEncStatus->channelConfigurationEncode.part.sub = audioFrame.channelConfigurationRequest.part.sub ;
    pEncStatus->channelConfigurationStream.part.sat = audioFrame.channelConfigurationRequest.part.sat ;
    pEncStatus->channelConfigurationStream.part.sub = audioFrame.channelConfigurationRequest.part.sub ;
    */

    /* PAF_CC_SAT_SURROUND4 with PAF_SUBW */
    pEncStatus->channelMap.from[0] = PAF_LEFT;
    pEncStatus->channelMap.from[1] = PAF_RGHT;
    pEncStatus->channelMap.from[2] = PAF_CNTR;
    pEncStatus->channelMap.from[3] = PAF_LSUR;
    pEncStatus->channelMap.from[4] = PAF_RSUR;
    pEncStatus->channelMap.from[5] = PAF_LBAK;
    pEncStatus->channelMap.from[6] = PAF_RBAK;
    pEncStatus->channelMap.from[7] = PAF_SUBW;

    pEncStatus->channelMap.to[0] = 0;
    pEncStatus->channelMap.to[1] = 1;
    pEncStatus->channelMap.to[2] = 2;
    pEncStatus->channelMap.to[3] = 3;
    pEncStatus->channelMap.to[4] = 4;
    pEncStatus->channelMap.to[5] = 5;
    pEncStatus->channelMap.to[6] = 6;
    pEncStatus->channelMap.to[7] = 7;


    //pEncStatus->sampleProcess = ;
    PCE_Reset(handle,
                Control, pEncStatus);
    PCE_Info(handle,
                Control, pEncStatus);

	/*
	 *  Configured frame size is read per loop
	 */
    if (status.phase[OUTPUT].mode == 1)
    {
    	*(pFIOtmp) = *(pFIO);
    	chunk_size = 36;
    	subchunk2_size = 0;
    }
	while (PAF_FIO_Read(pFIO, &audioFrame) == RETRUN_SUCCESS)
    {
        if (status.phase[OUTPUT].mode == 1)
        {
	        ((PAF_ActivePhaseOutput *)pPceParams->pActive->pActivePhase[OUTPUT])->pOutBufConfig->pntr.pFloat\
	        		                                                       =(float *)&pFIO->outBuf[0];
	        ((PAF_ActivePhaseOutput *)pPceParams->pActive->pActivePhase[OUTPUT])->pOutBufConfig->base.pFloat\
			                                                               =(float *)&pFIO->outBuf[0];
        }
#ifdef PROFILER
         Prfl_Start(&profiler);
#endif

        PCE_encodeFrame(handle,
                        &inStruct,
 					    &outStruct); /* pass some data */
#ifdef PROFILER
        Prfl_Stop(&profiler);
#endif

        if (status.phase[OUTPUT].mode == 1)
        {

    	    char *pData =(char *) ((PAF_ActivePhaseOutput *)pPceParams->pActive->pActivePhase[OUTPUT])->pOutBufConfig->base.pSmInt;
        	chunk_size += STRIDE*3*FRAME_SIZE;
        	subchunk2_size += STRIDE*3*FRAME_SIZE;
        	fwrite(pData, 1, 3 * STRIDE * FRAME_SIZE, pOutputFile2);
        }
        else
        {
            PAF_FIO_Write(pFIO,&audioFrame);
        }
        /*increament process count*/
        count++;
        printf("Test case %d, frame %d\n", testCount, count);
   }
#ifdef PROFILER
            printf("Total Process Cycles %llu, Peak Process Cycles %llu Process Blocks size %d Process Count %u Input sample rate %d \n****************\n",
                    profiler.total_process_cycles, profiler.peak_process_cycles, FRAME_SIZE, profiler.process_count, ((subchunk_data *)&pFIO->subchunk_data)->sample_rate );
            {   // write porfiling info in file
                FILE *pStdout_profile = NULL;
                pStdout_profile = fopen("PCE_profile.txt", "a");
                fprintf(pStdout_profile, "TestCase Count %d: Total Process Cycles %llu, Peak Process Cycles %llu Process Blocks size %d Process Count %u Input sample rate %d",
                		testCount, profiler.total_process_cycles, profiler.peak_process_cycles, FRAME_SIZE, profiler.process_count, ((subchunk_data *)&pFIO->subchunk_data)->sample_rate);
                fprintf(pStdout_profile, "\n****************\n");
                fclose(pStdout_profile);
            }

#endif
   PAF_FIO_DeInit(pFIO, &audioFrame);
   fclose(pOutputFile2);
   if (status.phase[OUTPUT].mode == 1)
   {
   /* writing otput wave file header*/
     pOutputFile2 = fopen(pOutFile2,"rb");
     pOutputFile3 = fopen(pOutFile3,"wb");

     pFIO->header_1_data.chunk_size = chunk_size;
     pFIO->header_2_data.subchunk2_size = subchunk2_size;
   ((subchunk_data *)&pFIO->subchunk_data)->num_channels = STRIDE;
     ((subchunk_data *)&pFIO->subchunk_data)->bits_per_sample = 24;
     fwrite((void*)&pFIO->header_1_data, 1, sizeof(header_1), pOutputFile3);
     fwrite((void*)&pFIO->subchunk_data, 1, pFIO->header_1_data.subchunk1_size, pOutputFile3);
     fwrite((void*)&pFIO->header_2_data, 1, sizeof(header_2), pOutputFile3);

     /* copying audio data from tmp file to wav file*/
     printf("\n Dumplin Wave file ..... ");
     while (!feof(pOutputFile2))
     {
         fread(&byte, sizeof(char), 1, pOutputFile2);
         fwrite(&byte, sizeof(char), 1, pOutputFile3);
     }
     fclose(pOutputFile2);
     fclose(pOutputFile3);
   }

   PCE_delete(handle);
   printf("\n completed");
   return(0);
}

/*
 *  memcpy
 */

Uint32 DAT_cacheop_and_copy (void *src, void *dst, Uint16 cnt)
{
#ifdef EDMA_DAT
#if ((PAF_DEVICE&0xFF000000) == 0xD8000000)
	return edma_DAT_copy( src,dst, cnt);
#else
	return (Uint32) memcpy (dst, src, cnt);
#endif
#else
	return (Uint32) memcpy (dst, src, cnt);
#endif
}

/*
 * DMA dummy wait
 */
void DAT_wait_dummy (Uint32 eventid)
{
#ifdef EDMA_DAT
#if ((PAF_DEVICE&0xFF000000) == 0xD8000000)
	edma_DAT_wait(eventid);
#endif
#endif
}
