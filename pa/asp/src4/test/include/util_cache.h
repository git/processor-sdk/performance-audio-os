/*
 *  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
 *  ALL RIGHTS RESERVED 
 */


#ifndef __UTIL_CACHE_H__
#define __UTIL_CACHE_H__

volatile unsigned int *L1PINV = (unsigned int *) 0x1845028;

/* writeback invalidates to avoid any missbehaviour such as call stack getting cleaned*/
volatile unsigned int *L1DWBINV = (unsigned int *) 0x1845044; 

volatile unsigned int *L2WBINV = (unsigned int *) 0x1845004;

/* writing a one into these registers invalidates */

unsigned int L1PINVAL = 0x1; 

unsigned int L1DINVAL = 0x1;

unsigned int L2INVAL = 0x1;

#define L2_SIZE_32KB  1
#define L2_SIZE_64KB  2
#define L2_SIZE_128KB 3
#define L2_SIZE_256KB 4
#define L2_SIZE_512KB 5
#define L2_MAX_CACHE  7 /* max supported by chip */

typedef struct cacheConfig_s {
    unsigned short int L1PflushFlag;
    unsigned short int L1DflushFlag;
    unsigned short int L2flushFlag;
    unsigned short int L2asCacheFlag;
    unsigned short int L2sizeCfg;
} cacheConfig_t;

cacheConfig_t cacheConfig = {
    1,              // L1PflushFlag 
    1,              // L1DflushFlag
    1,              // L2flushFlag
    1,              // L2asCacheFlag
    //L2_SIZE_512KB // L2sizeCfg
    L2_SIZE_256KB   // L2sizeCfg
    //L2_SIZE_128KB
};

static inline void L1D_init(int on)
{

    volatile unsigned int *l1dcfg = (volatile unsigned int *)0x1840040;
    int newval = on ? 7 : 0;

    *l1dcfg &= ~0x7;
    *l1dcfg |= newval;
    while ((*l1dcfg & 0x7) != newval);
  
}

static inline void L1P_init(int on)
{
    volatile unsigned int *l1pcfg = (volatile unsigned int *)0x1840020;

    int newval = on ? 7 : 0; /* 32K or 0K */

    *l1pcfg &= ~0x7;
    *l1pcfg |= newval;
    while ((*l1pcfg & 0x7) != newval);
  
}
#if 1
static inline void L2_init(int on, unsigned short int L2sizeCfg) 
{
    volatile unsigned int *l2ccfg = (volatile unsigned int *)0x1840000;
    volatile unsigned int *mar12  = (volatile unsigned int *)0x1848030; /* 0x0C000000 - 0x0CFFFFFF */
    volatile unsigned int *mar192 = (volatile unsigned int *)0x1848300; /* 0xC0000000 - 0xC0FFFFFF */
    volatile unsigned int *mar193 = (volatile unsigned int *)0x1848304; /* 0xC1000000 - 0xC1FFFFFF */
    volatile unsigned int *mar194 = (volatile unsigned int *)0x1848308; /* 0xC2000000 - 0xC2FFFFFF */
    volatile unsigned int *mar195 = (volatile unsigned int *)0x184830c; /* 0xC3000000 - 0xC3FFFFFF */
    volatile unsigned int *mar128 = (volatile unsigned int *)0x1848200; /* 0x80000000 - 0x80FFFFFF */
    volatile unsigned int *mar129 = (volatile unsigned int *)0x1848204; /* 0x81000000 - 0x81FFFFFF */
    volatile unsigned int *mar130 = (volatile unsigned int *)0x1848208; /* 0x82000000 - 0x82FFFFFF */
    int newval = on ? L2sizeCfg : 0;
    
    (void)*mar192;
    (void)*mar193;
    (void)*mar194;
    (void)*mar195;

    // *mar192 = on ? 1 : 0;
    // *mar193 = on ? 1 : 0;
    // *mar194 = on ? 1 : 0;
    // *mar195 = on ? 1 : 0;
    *mar12  = on ? 1 : 0;
    *mar128 = on ? 1 : 0;
    *mar129 = on ? 1 : 0;
    *mar130 = on ? 1 : 0;

    *l2ccfg &= ~0x7;
    *l2ccfg |= newval;
    while ((*l2ccfg & 0x7) != newval);
}
#endif

#if 0
static inline void L2_init(int on, unsigned short int L2sizeCfg) 
{
    volatile unsigned int *l2ccfg = (volatile unsigned int *)0x1840000;
    volatile unsigned int *mar224 = (volatile unsigned int *)0x1848380; /* 0xE0000000 - 0xE0FFFFFF */
    volatile unsigned int *mar128 = (volatile unsigned int *)0x1848200; /* 0x80000000 - 0x80FFFFFF */
    int newval = on ? L2sizeCfg : 0;

    *mar224 = on ? 1 : 0;
    *mar128 = on ? 1 : 0;

    *l2ccfg &= ~0x7;
    *l2ccfg |= newval;
    while ((*l2ccfg & 0x7) != newval);
}
#endif

/* Initialize L1P and L1D to be 32 KB cache, L2 as (default) SRAM */
static inline void memarchcfg_cacheEnable(void)
{
    L1D_init(1);
    L1P_init(1);
    L2_init(cacheConfig.L2asCacheFlag, cacheConfig.L2sizeCfg);
}

static inline void memarchcfg_cacheFlush(void)
{
    if (cacheConfig.L1PflushFlag) 
    {
        *L1PINV = L1PINVAL;

        while (((*L1PINV) & 1) != 0)
        {
          asm ( " NOP "); 
          asm ( " NOP ");
          asm ( " NOP ");
        }
    }

    if (cacheConfig.L1DflushFlag)
    {
        *L1DWBINV = L1DINVAL;

        while (((*L1DWBINV) & 1) != 0)
        {
          asm ( " NOP "); 
          asm ( " NOP ");
          asm ( " NOP ");
        }
    }

    if (cacheConfig.L2flushFlag)
    {
        *L2WBINV = L2INVAL ;

        while (((*L2WBINV) & 1) != 0)
        {
          asm ( " NOP "); 
          asm ( " NOP ");
          asm ( " NOP ");
        }
    }
} /* memarchcfg_cacheFlush */

#endif
