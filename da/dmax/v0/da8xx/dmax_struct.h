
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX Data structures definitions
//
//
//

#ifndef dMAX_STRUCT_H_
#define dMAX_STRUCT_H_

#include <std.h>
#include <sem.h>

#include <tistdtypes.h>

#include <dmax.h>
#include "cslr_dmaxpdsp.h" 

/* dMAX Data Ram organization constants */
#define dMAX_EET_BASE    0       // EET base
#define dMAX_EET_SIZE    132     // EET size 
#define dMAX_PTT_BASE    (dMAX_EET_BASE+dMAX_EET_SIZE) // PTT base
#define dMAX_PTT_SIZE    32      // PTT size
#define dMAX_PT_BASE     (dMAX_PTT_BASE+dMAX_PTT_SIZE) // PT base
#define dMAX_PT_SIZE     (dMAX_MAX_DSIZE-dMAX_PT_BASE) // PT size 

/* dMAX pending priority level constants */
#define dMAX_NUMPP       32
#define dMAX_PPANY       -1
#define dMAX_PPNONE      -2
#define dMAX_PP(N)       (N>=0&&N<dMAX_NUMPP?N:dMAX_PPANY)

/* dMAX Fxn Table */
typedef struct dMAX_Obj *dMAX_Handle;
typedef struct dMAX_Fxns{
    void (*gioRst)(dMAX_Handle handle);
    void (*gioRstRel)(dMAX_Handle handle);
    void (*evtPol)(dMAX_Handle handle,Uint32 evt,Uint32 pol);
    void (*enaEvt)(dMAX_Handle handle,Uint32 evt);
    void (*disEvt)(dMAX_Handle handle,Uint32 evt);
    void (*disEvtAll)(dMAX_Handle handle);
    void (*evtPri)(dMAX_Handle handle,Uint32 maxid,Uint32 evt);
    void (*clrEvtFlg)(dMAX_Handle handle,Uint32 evt);
    void (*clrEvtFlgAll)(dMAX_Handle handle);
    void (*dirInpAll)(dMAX_Handle handle);
    void (*rst)(dMAX_Handle handle,Uint32 maxid);
    void (*rstRel)(dMAX_Handle handle,Uint32 maxid);
    void (*initIRAM)(dMAX_Handle handle,Uint32 maxid);
    void (*initDRAM)(dMAX_Handle handle,Uint32 maxid);
    void (*updateEET)(dMAX_Handle handle,Uint32 maxid,Uint32 evt,Uint32 ee);
    void (*updatePTT)(dMAX_Handle handle,Uint32 maxid,Uint32 pp,Uint32 evt);
    Uint32 * (*allocPT)(dMAX_Handle handle,Uint32 maxid,Uint32 size);
    void (*cfgPT)(dMAX_Handle handle,Uint32 *pt,Uint32 *pCfg,Uint32 size);
    void (*freePT)(dMAX_Handle handle,Uint32 maxid,Uint32 *pt,Uint32 size);
    Int32 (*allocEvt)(dMAX_Handle handle,Int32 evt);
    void (*freeEvt)(dMAX_Handle handle,Uint32 evt);
    Int32 (*allocInt)(dMAX_Handle handle,Int32 intnum);
    Int32 (*mapInt)(dMAX_Handle handle,Uint32 intnum);
    void (*freeInt)(dMAX_Handle handle,Uint32 intnum);
    Int32 (*allocTCC)(dMAX_Handle handle,Int32 tcc, Uint32 intNum);
    Uint32 (*chkTCC)(dMAX_Handle handle,Uint32 tcc);
    void (*setTCC)(dMAX_Handle handle,Uint32 tcc); 
    void (*clrTCC)(dMAX_Handle handle,Uint32 tcc);
    void (*freeTCC)(dMAX_Handle handle,Uint32 tcc);
    Int32 (*allocFSC)(dMAX_Handle handle,Int32 fsc);
    Uint32 (*chkFSC)(dMAX_Handle handle,Uint32 fsc);
    void (*setFSC)(dMAX_Handle handle,Uint32 fsc); 
    void (*clrFSC)(dMAX_Handle handle,Uint32 fsc);
    void (*freeFSC)(dMAX_Handle handle,Uint32 fsc);
    Int32 (*allocPP)(dMAX_Handle handle,Uint32 maxid,Int32 pp);
    void (*freePP)(dMAX_Handle handle,Uint32 maxid,Uint32 pp); 
    void (*setCPUEvt)(Uint32 evt);
    Int32 (*evt2CPUOUT)(Int32 evt);
    void (*isr)(SEM_Handle sem);
}dMAX_Fxns;

/* dMAX Object and dMAX Handle Definitions */
typedef struct MAX_Obj {
    volatile Uint32 *ibase;      // IRAM base 
    const Uint32 *code;          // micro code base
    Uint16 length;               // micro code length
    Uint8  maxid;                // MAX id
    Uint8  unused;               // unused
    Uint32 ppalloc;              // pending priority allocation table 
    Uint32 *dbase;               // DRAM base
    Uint32 *eet;                 // EET base
    Uint8  *ptt;                 // PTT base
    Uint32 alloc[(dMAX_MAX_DSIZE>>2)/32]; // allocation table
} MAX_Obj, *MAX_Handle;

typedef struct dMAX_Obj {
    dMAX_Fxns *fxns;             // Fxn table
    volatile Uint32 *gio;        // GIO base
    Uint32 evtalloc;             // event allocation
    Uint32 intalloc;             // interrupt allocation table
    Uint32 tccalloc;             // tcc allocation table
    Uint32 fscalloc;             // fsc allocation table
    MAX_Handle max[DMAXPDSP_PER_CNT]; // MAX Object
} dMAX_Obj;

/* dMAX Fxns and Object declarations */
extern dMAX_Fxns dMAX_FXNS;
extern void dMAX_initCustom(dMAX_Handle handle,const Uint32* max0_code,
                            const Uint32* max1_code);
extern void dMAX_init(dMAX_Handle handle);
extern dMAX_Handle dMAX_HANDLE;
#endif // dMAX_STRUCT_H_

