
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX GIO constants and definitions
//
//
//

#ifndef dMAX_GIO_
#define dMAX_GIO_

/* GIO Device Identifiers */
#define GIO_DEV0         0

/* GIO Base address definitions */
#define GIO0_BASE        0x60000000

/* GIO register definitions - in unit of word offset */
#define GIOGCR0          0x00
#define GIOPWDN          0x01
#define GIOPOL           0x02
#define GIOENASET        0x03
#define GIOENACLR        0x04
#define GIOLVLSET        0x05
#define GIOLVLCLR        0x06
#define GIOFLG           0x07
#define GIOOFFA          0x08
#define GIOOFFB          0x09
#define GIOEMUA          0x0a
#define GIOEMUB          0x0b
#define GIODIR0          0x0c
#define GIODIN0          0x0d
#define GIODOUT0         0x0e
#define GIODSET0         0x0f
#define GIODCLR0         0x10
#define GIODPDR0         0x11
#define GIOPE0           0x12
#define RSVD0            0x13
#define GIODIR1          0x14
#define GIODIN1          0x15
#define GIODOUT1         0x16
#define GIODSET1         0x17
#define GIODCLR1         0x18
#define GIODPDR1         0x19
#define GIOPE1           0x1a
#define RSVD1            0x1b
#define GIODIR2          0x1c
#define GIODIN2          0x1d
#define GIODOUT2         0x1e
#define GIODSET2         0x1f
#define GIODCLR2         0x20
#define GIODPDR2         0x21
#define GIOPE2           0x22
#define RSVD2            0x23
#define GIODIR3          0x24
#define GIODIN3          0x25
#define GIODOUT3         0x26
#define GIODSET3         0x27
#define GIODCLR3         0x28
#define GIODPDR3         0x29
#define GIOPE3           0x2a
#define RSVD3            0x2b
#define GIODIR4          0x2c
#define GIODIN4          0x2d
#define GIODOUT4         0x2e
#define GIODSET4         0x2f
#define GIODCLR4         0x30
#define GIODPDR4         0x31
#define GIOPE4           0x32
#define RSVD4            0x33
#define GIODIR5          0x34
#define GIODIN5          0x35
#define GIODOUT5         0x36
#define GIODSET5         0x37
#define GIODCLR5         0x38
#define GIODPDR5         0x39
#define GIOPE5           0x3a
#define RSVD5            0x3b
#define GIODIR6          0x3c
#define GIODIN6          0x3d
#define GIODOUT6         0x3e
#define GIODSET6         0x3f
#define GIODCLR6         0x40
#define GIODPDR6         0x41
#define GIOPE6           0x42
#define RSVD6            0x43
#define GIODIR7          0x44
#define GIODIN7          0x45
#define GIODOUT7         0x46
#define GIODSET7         0x47
#define GIODCLR7         0x48
#define GIODPDR7         0x49
#define GIOPE7           0x4a
#define RSVD7            0x4b

/* GIO port definitions - in unit of word offset */
#define GIONUMPORTS      8
#define GIOPORT_BASE     GIODIR0
#define GIOPORT_OFF      8
#define GIOPORT_DIROFF   0
#define GIOPORT_DINOFF   1 
#define GIOPORT_DOUTOFF  2 
#define GIOPORT_DSETOFF  3 
#define GIOPORT_DCLROFF  4 
#define GIOPORT_DPDROFF  5 
#define GIOPORT_DPEOFF   6 
#define GIOPORT_RSVDOFF  7

/* GIO TCC port definitions - in unit of word offset */
#define GIOTCCNUMPORTS   2 
#define GIOTCCPORT_BASE  GIODIR2

/* GIO FSC port definitions - in unit of word offset */
#define GIOFSCNUMPORTS   2 
#define GIOFSCPORT_BASE  GIODIR0

/* GIO register fields */
/* GIOGCR0 fields */
#define GIOGCR0_RST      0       // GIOGCR0 reset
#define GIOGCR0_NORST    1       // GIOGCR0 no reset
#define GIOGCR0_RST_MSK  1       // GIOGCR0 reset mask
#define GIOGCR0_RST_SHFT 0       // GIOGCR0 reset shift

/* GIOPOL fields */
#define GIOPOL_FALL      0       // GIOPOL falling edge
#define GIOPOL_RISE      1       // GIOPOL rising edge

/* GIOENASET fields */
#define GIOENASET_ENA    1       // GIOENASET enable event
#define GIOENASET_ENA_ALL 0xFFFFFFFF // GIOENASET enable all events

/* GIOENACLR fields */
#define GIOENACLR_DIS    1       // GIOENASET disable event
#define GIOENACLR_DIS_ALL 0xFFFFFFFF // GIOENACLR disable all events

/* GIOLVLSET fields */
#define GIOLVLSET_HGH    1       // GIOLVLSET high priority 
#define GIOLVLSET_HGH_ALL 0xFFFFFFFF // GIOLVLSET high priority all events

/* GIOLVLCLR fields */
#define GIOLVLCLR_LOW    1       // GIOLVLCLR low priority 
#define GIOLVLCLR_LOW_ALL 0xFFFFFFFF // GIOLVLCLR low priority all events

/* GIOFLG fields */
#define GIOFLG_CLR       1       // GIOFLG clear
#define GIOFLG_CLR_ALL   0xFFFFFFFF // GIOFLG clear all

/* GIODIR fields */
#define GIODIR_IN        0       // GIODIR input pin 
#define GIODIR_IN_ALL    0       // GIODIR input all pins
#define GIODIR_OUT       1       // GIODIR output pin 

#endif // dMAX_GIO_
