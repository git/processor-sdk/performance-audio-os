
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// IDL function for determining/displaying CPU Load info.
// Currently also flushes LoggerBuf for overall system trace log
//
//
//

// ............................................................................

#include  <std.h>
#include <xdas.h>

#include <ti/bios/include/log.h>

#include <ti/sysbios/utils/Load.h>
#include <ti/sysbios/knl/Task.h>

#include <xdc/runtime/LoggerBuf.h>
#include <xdc/runtime/System.h>

extern LOG_Obj trace;

// ............................................................................

Void PAF_load (Void)
{
    Load_Stat stat;
    UInt32 totalLoad;

    Load_getTaskLoad( Task_getIdleTask(), &stat);
    totalLoad = 100 - Load_calculateLoad (&stat);

    LOG_printf( &trace, "CPU Load using Task Idle Time = %d%%", totalLoad );
    LOG_printf( &trace, "CPU Load = %d%%", Load_getCPULoad() );

    LoggerBuf_flushAll();
    System_flush(); // currently required (for BIOS in ROM), owing to SDSCM00026589
}
