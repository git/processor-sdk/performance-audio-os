
/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Common PCM Encoder algoritm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the PCM Encoder algoritm.
 */
#ifndef IPCE_
#define IPCE_

#include <alg.h>
#include <ialg.h>
#include  <std.h>
#include <xdas.h>

#include "icom.h"
#include "pafenc.h"

/*
 *  ======== IPCE_Obj ========
 *  Every implementation of IPCE *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IPCE_Obj {
    struct IPCE_Fxns *fxns;    /* function list: standard, public, private */
} IPCE_Obj;

/*
 *  ======== IPCE_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IPCE_Obj *IPCE_Handle;

#define IPCE_PHASES 4

/*
 *  ======== IPCE_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.
 */
typedef volatile struct IPCE_StatusPhase {
    XDAS_Int8 mode;
    XDAS_Int8 type;
} IPCE_StatusPhase;

typedef volatile struct IPCE_Status {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 type; /* unused */
    IPCE_StatusPhase phase[IPCE_PHASES];
} IPCE_Status;

/*
 *  ======== IPCE_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IPCE_ConfigPhase {
    Int size; /* size != sizeof (IPCE_ConfigPhase) */
    Int data; /* ... */
} IPCE_ConfigPhase;

typedef struct IPCE_Config {
    Int size;
    XDAS_Int16 frameLength;
    XDAS_Int16 unused;
    float volumeRamp;
    /* IPCE_ConfigPhase * */ Int pConfigPhase[IPCE_PHASES];
    PAF_AudioData scale[PAF_MAXNUMCHAN];
} IPCE_Config;

/*
 *  ======== IPCE_Active ========
 *  Active structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm but which must maintain value
 *  only while the algorithm is running.
 */
typedef struct IPCE_Active {
    Int size;
    Int flag;
    PAF_EncodeStatus *pEncodeStatus;
    PAF_VolumeStatus *pVolumeStatus;
    PAF_OutBufConfig *pOutBufConfig;
} IPCE_Active;

/*
 *  ======== IPCE_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a PCE object.
 *
 *  Every implementation of IPCE *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */
typedef struct IPCE_Params {
    Int size;
    const IPCE_Status *pStatus;
    const IPCE_Config *pConfig;
    const IPCE_Active *pActive;
} IPCE_Params;

/*
 *  ======== IPCE_PARAMS ========
 *  Default instance creation parameters (defined in ipce.c)
 */
extern const IPCE_Params IPCE_PARAMS;

/*
 *  ======== IPCE_Fxns ========
 *  All implementation's of PCE must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is PCE_<vendor>_IPCE, where
 *  <vendor> is the vendor name.
 */
typedef struct IPCE_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IPCE_Handle, ALG_Handle, PAF_EncodeControl *, PAF_EncodeStatus *);
    Int         (*info)(IPCE_Handle, ALG_Handle, PAF_EncodeControl *, PAF_EncodeStatus *);
    Int         (*encode)(IPCE_Handle, ALG_Handle, PAF_EncodeInStruct *, PAF_EncodeOutStruct *);
    /* private */
    Int         (*phase[IPCE_PHASES])(IPCE_Handle, PAF_EncodeInStruct *, PAF_EncodeOutStruct *, IPCE_ConfigPhase *, IPCE_StatusPhase *, PAF_ChannelMask, PAF_ChannelMask);
    Int         (*outputCheck)(IPCE_Handle, int, int);
    Int         (*outputAudio)(IPCE_Handle, PAF_AudioData *, PAF_AudioData, int);
    Int         (*outputFinal)(IPCE_Handle, int, int);
} IPCE_Fxns;

/*
 *  ======== IPCE_Cmd ========
 *  The Cmd enumeration defines the control commands for the PCE
 *  control method.
 */
typedef enum IPCE_Cmd {
    IPCE_NULL                   = ICOM_NULL,
    IPCE_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IPCE_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IPCE_GETSTATUS              = ICOM_GETSTATUS,
    IPCE_SETSTATUS              = ICOM_SETSTATUS,
    IPCE_MINSAMGEN,
    IPCE_MININFO,
    IPCE_MAXSAMGEN
} IPCE_Cmd;

#endif  /* IPCE_ */
