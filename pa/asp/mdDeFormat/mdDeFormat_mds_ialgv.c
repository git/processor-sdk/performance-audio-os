/*
* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This file contains the function table definitions for all
 *  interfaces implemented by the MDDEFORMAT_MDS module that derive
 *  from IALG
 *
 *  If you added private functions to IMDDEFORMAT_Fxns in imdDeFormat.h,
 *  then you would need to initialize those in MDDEFORMAT_MDS_IMDDEFORMAT 
 *  below. 
 *
 */

#include <std.h>
#include <ialg.h>

#include <imdDeFormat.h>
#include <mdDeFormat_mds.h>
#include <mdDeFormat_mds_priv.h>

#define IALGFXNS \
    &MDDEFORMAT_MDS_IALG,       /* module ID */                         \
    MDDEFORMAT_MDS_activate,    /* activate */                          \
    MDDEFORMAT_MDS_alloc,       /* alloc */                             \
    MDDEFORMAT_MDS_control,     /* control */                           \
    MDDEFORMAT_MDS_deactivate,  /* deactivate */                        \
    MDDEFORMAT_MDS_free,        /* free */                              \
    MDDEFORMAT_MDS_initObj,     /* init */                              \
    MDDEFORMAT_MDS_moved,       /* moved */                             \
    MDDEFORMAT_MDS_numAlloc     /*numAlloc*/                            \


/*
 *  ======== MDDEFORMAT_MDS_IMDDEFORMAT ========
 *  This structure defines TII's implementation of the IMDDEFORMAT interface
 *  for the MDDEFORMAT_MDS module.
 */
const IMDDEFORMAT_Fxns MDDEFORMAT_MDS_IMDDEFORMAT = {       /* module_vendor_interface */
    IALGFXNS,
    MDDEFORMAT_MDS_reset,
    MDDEFORMAT_MDS_apply,

    // private
};

/*
 *  ======== MDDEFORMAT_MDS_IALG ========
 *  This structure defines TII's implementation of the IALG interface
 *  for the MDDEFORMAT_MDS module.
 */
#ifdef _TMS320C6X

asm("_MDDEFORMAT_MDS_IALG .set _MDDEFORMAT_MDS_IMDDEFORMAT");

#else

/*
 *  We duplicate the structure here to allow this code to be compiled and
 *  run non-DSP platforms at the expense of unnecessary data space
 *  consumed by the definition below.
 */
IALG_Fxns MDDEFORMAT_MDS_IALG = {       /* module_vendor_interface */
    IALGFXNS
};

#endif
