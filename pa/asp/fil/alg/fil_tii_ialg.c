/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/**
 *  @file   fil_tii_ialg.c
 *
 *  @brief  FIL Module IALG implementation - TII's implementation of the
 *          IALG interface for the FIL Generic filter function .
 *
 *          This file contains an implementation of the IALG interface
 *          required by XDAS.
 */
/*
 *  ======== fil_tii_ialg.c ========
 *  FIL Module IALG implementation - TII's implementation of the
 *  IALG interface for the FIL Generic filter function .
 *
 *  This file contains an implementation of the IALG interface
 *  required by XDAS.
 */

#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include <ifil.h>
#include <fil_tii.h>
#include <fil_tii_priv.h>

/*
 *  ======== FIL_TII_activate ========
 */
  /* COM_TIH_activate */

/*
 *  ======== FIL_TII_alloc ========
 */
Int FIL_TII_alloc(const IALG_Params *algParams,
                 IALG_Fxns **pf, IALG_MemRec memTab[])
{
    const IFIL_Params *params = (Void *)algParams;
    
    if (params == NULL) {
        params = &IFIL_PARAMS;  /* set default parameters */
    }
    /* Request memory for FIL object */
    memTab[0].size = ( sizeof(FIL_TII_Obj) + 3 ) /4*4; // + other objs which are as poiinters.
    memTab[0].alignment = 4;
    memTab[0].space = IALG_SARAM;
    memTab[0].attrs = IALG_PERSIST;

    memTab[1].size = ( sizeof(FIL_TII_Status) + 3 ) /4*4; // + other objs which are as poiinters.
    memTab[1].alignment = 4;
    memTab[1].space = IALG_SARAM;
    memTab[1].attrs = IALG_PERSIST;
    
    memTab[2].size = ( sizeof(FIL_TII_Config) + 3 ) /4*4; // + other objs which are as poiinters.
    memTab[2].alignment = 4;
    memTab[2].space = IALG_SARAM;
    memTab[2].attrs = IALG_PERSIST; 
    
    memTab[3].size = ( sizeof(PAF_FilParam) + 3 ) /4*4; // + other objs which are as poiinters.
    memTab[3].alignment = 4;
    memTab[3].space = IALG_SARAM;
    memTab[3].attrs = IALG_PERSIST; 
    
    memTab[4].size = ( 16*PAF_MAXNUMCHAN + 3 ) /4*4; 
    memTab[4].alignment = 4;
    memTab[4].space = IALG_SARAM;
    memTab[4].attrs = IALG_SCRATCH;      
       
    FIL_enquire( (IALG_Params *) params, &memTab[5]);
    memTab[5].space = IALG_SARAM;
    memTab[5].attrs = IALG_PERSIST;      

    return (6);
}

/*
 *  ======== FIL_TII_deactivate ========
 */
  /* COM_TIH_deactivate */

/*
 *  ======== FIL_TII_free ========
 */
Int FIL_TII_free(IALG_Handle handle, IALG_MemRec memTab[])
{

    /* Request memory for FIL object */
    memTab[0].size = ( sizeof(FIL_TII_Obj) + 3 ) /4*4; // + other objs which are as poiinters.
    memTab[0].alignment = 4;
    memTab[0].space = IALG_EXTERNAL;
    memTab[0].attrs = IALG_PERSIST;

    memTab[1].size = ( sizeof(FIL_TII_Status) + 3 ) /4*4; // + other objs which are as poiinters.
    memTab[1].alignment = 4;
    memTab[1].space = IALG_EXTERNAL;
    memTab[1].attrs = IALG_PERSIST;
    memTab[1].base  = (Void *)((FIL_TII_Obj *)handle)->pStatus;
    
    memTab[2].size = ( sizeof(FIL_TII_Config) + 3 ) /4*4; // + other objs which are as poiinters.
    memTab[2].alignment = 4;
    memTab[2].space = IALG_EXTERNAL;
    memTab[2].attrs = IALG_PERSIST; 
    memTab[2].base  = ((FIL_TII_Obj *)handle)->pConfig;
    
    memTab[3].size = ( sizeof(PAF_FilParam) + 3 ) /4*4; // + other objs which are as poiinters.
    memTab[3].alignment = 4;
    memTab[3].space = IALG_EXTERNAL;
    memTab[3].attrs = IALG_PERSIST; 
    memTab[3].base  = ((FIL_TII_Obj *)handle)->pConfig->pParams;
    
    memTab[4].size = ( 16*PAF_MAXNUMCHAN + 3 ) /4*4; 
    memTab[4].alignment = 4;
    memTab[4].space = IALG_SARAM;
    memTab[4].attrs = IALG_SCRATCH;      
    memTab[4].base  = ((FIL_TII_Obj *)handle)->pConfig->pParams->pIn;
       
    FIL_enquire( (IALG_Params *) handle, &memTab[5]);
    memTab[5].space = IALG_EXTERNAL;
    memTab[5].attrs = IALG_PERSIST;      
    memTab[5].base  = ((FIL_TII_Obj *)handle)->pConfig->pVars;
    
    return (6);
}

/*
 *  ======== FIL_TII_initObj ========
 */
Int FIL_TII_initObj(IALG_Handle handle,
                const IALG_MemRec memTab[], IALG_Handle p,
                const IALG_Params *algParams)
{
    const IFIL_Params *params = (Void *)algParams;
    
    if ( (params == NULL) || (params->pConfig->pCoefs == NULL) 
         || (params->pStatus->mode == NULL) 
       )
        params = &IFIL_PARAMS;  /* set default parameters */

    
    ((FIL_TII_Obj *)handle)->pStatus = (FIL_TII_Status *)memTab[1].base;
    ((FIL_TII_Obj *)handle)->pConfig = (FIL_TII_Config *)memTab[2].base;
    
    ((FIL_TII_Obj *)handle)->pConfig->pCoefs  = params->pConfig->pCoefs;
    ((FIL_TII_Obj *)handle)->pConfig->pParams = (PAF_FilParam *)memTab[3].base;
    ((FIL_TII_Obj *)handle)->pConfig->pVars   = (Void *)memTab[5].base;
    
    ((FIL_TII_Obj *)handle)->pStatus->maskSelect = params->pStatus->maskSelect;
    ((FIL_TII_Obj *)handle)->pStatus->maskStatus = params->pStatus->maskStatus;
    ((FIL_TII_Obj *)handle)->pStatus->mode = params->pStatus->mode;
    ((FIL_TII_Obj *)handle)->pStatus->size = params->pStatus->size;
    ((FIL_TII_Obj *)handle)->pStatus->use  = params->pStatus->use;
     
    ((FIL_TII_Obj *)handle)->pConfig->pIIR = 
        (IFIL_FunRec (*)[FIL_IR_IIRTAPGEN][FIL_IR_IIRCHGEN])FIL_IIRFilterRec;
        
    ((FIL_TII_Obj *)handle)->pConfig->pFIR = 
        (IFIL_FunRec (*)[FIL_IR_FIRTAPGEN][FIL_IR_FIRCHGEN])FIL_FIRFilterRec;
        
    ((FIL_TII_Obj *)handle)->pConfig->pSOS_DF2 = 
        (IFIL_FunRec (*)[FIL_CASC_IR_SOS_DF2_CH_GEN][FIL_CASC_IR_SOS_DF2_CASC_GEN])FIL_IIR_SOS_DF2_FilterRec;

    ((FIL_TII_Obj *)handle)->pConfig->pParams->pIn   = (Void **)memTab[4].base;
    ((FIL_TII_Obj *)handle)->pConfig->pParams->pOut  = (Void **)memTab[4].base + PAF_MAXNUMCHAN;    
    ((FIL_TII_Obj *)handle)->pConfig->pParams->pCoef = (Void **)memTab[4].base + PAF_MAXNUMCHAN*2;   
    ((FIL_TII_Obj *)handle)->pConfig->pParams->pVar  = (Void **)memTab[4].base + PAF_MAXNUMCHAN*3;
    ((FIL_TII_Obj *)handle)->pConfig->pParams->use   = 0;
     
    ((FIL_Handle)handle)->fxns->memReset( (Void *)((FIL_TII_Obj *)handle)->pConfig->pVars, (Int)memTab[5].size );
    
    return (IALG_EOK);
}

/*
 *  ======== FIL_TII_control ========
 */
  /* COM_TIH_control */

/*
 *  ======== FIL_TII_moved ========
 */
  /* COM_TIH_moved */

Int FIL_TII_numAlloc()
{
    return(6);
}
