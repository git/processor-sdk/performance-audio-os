
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX MEM 1D Generic Service Routine data structures
//
//
//

#ifndef MEM1DG_H_ 
#define MEM1DG_H_ 

#include <mem1dg.inc>
#include <tistdtypes.h>
#include <dmax_struct.h>

/* MEM1DG macros */
#define MEM1DG_MKCTRL(ESIZE,SYNC,LINK,TCINT)              \
    ((((ESIZE)&MEM1DG_ESIZE_MSK)<<MEM1DG_ESIZE_SHFT) |    \
     (((SYNC)&MEM1DG_SYNC_MSK)<<MEM1DG_SYNC_SHFT) |       \
     (((LINK)&MEM1DG_LINK_MSK)<<MEM1DG_LINK_SHFT) |       \
     (((TCINT)&MEM1DG_TCINT_MSK<<MEM1DG_TCINT_SHFT)))

#define MEM1DG_MKEE(PTB,PT)                               \
    ((((PTB)&0x000001FF)<<8) |                            \
     (PT))

/* MEM1DG parameter format */
typedef struct mem1dg {
    Uint8  ctrl;
    Uint8  tcint;
    Uint8  tcc;
    Uint8  pend;
    Uint32 src;
    Uint32 dst;
    Uint16 cnt;
    Uint8  blen;
    Uint8  res;
    Int16  src_idx;
    Int16  dst_idx;
    Uint32 link;
} mem1dg;

/* MEM1DG API declarations */
extern Uint32 dMAX_mem1dgOpen(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                              Uint32 pp,Uint32 tcc,Int32 tcint,Fxn fxn);
extern void dMAX_mem1dgClose(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                             Uint32 pp,Uint32 tcc,Int32 tcint,Uint32 pt);
extern void dMAX_mem1dgCfgEE(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                             Uint32 ee);
extern void dMAX_mem1dgCfgPT(dMAX_Handle handle,Uint32 pt,mem1dg *pmem1dg);
extern void dMAX_mem1dgCopy(dMAX_Handle handle,Uint32 evt);
extern void dMAX_mem1dgWait(dMAX_Handle handle,Uint32 tcc);
extern void dMAX_mem1dgNoWait(dMAX_Handle handle,Uint32 tcc);
extern Uint32 dMAX_mem1dgBusy(dMAX_Handle handle,Uint32 tcc);
extern Uint32 dMAX_mem1dgLnkOpen(dMAX_Handle handle,Uint32 maxid,Int32 tcc,
                                 Int32 tcint,Int32 pt,Fxn fxn);
extern void dMAX_mem1dgLnkClose(dMAX_Handle handle,Uint32 maxid,Int32 tcc,
                                Int32 tcint,Int32 pt);
extern Uint32 dMAX_mem1dgLnkAdd(dMAX_Handle handle,Uint32 maxid,Uint32 cpt);

#endif // MEM1DG_H_

