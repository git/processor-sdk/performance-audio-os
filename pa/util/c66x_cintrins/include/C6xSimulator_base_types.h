/****************************************************************************/
/*  C6xSimulator_base_types.h                                               */
/****************************************************************************/

/* 
 *
 * Copyright (C) 1997-2005 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 *  This header file defines the set of base type abstractions used in
 *  C6xSimulator and its tests. 
 */

/****************************************************************************/
/* compiler directive to ensure only one copy of .h gets                    */
/* included                                                                 */
#ifndef C6XSIMULATOR_BASE_TYPES_H
#define C6XSIMULATOR_BASE_TYPES_H

/***************************************************************************/
/* the type defines                                                        */

typedef char                    int8;
typedef short                  int16;
typedef int                    int32;

typedef unsigned char          uint8;
typedef unsigned short        uint16;
typedef unsigned int          uint32;

typedef float                float32;  // Presume float is 32-bits everywhere

#ifndef _TMS320C6X

#ifdef _MSC_VER       // handle Microsoft types
typedef __int64                int40;
typedef __int64                int64_d;
typedef __int64                int64_ll;

typedef unsigned __int64       uint40;
typedef unsigned __int64       uint64_d;
typedef unsigned __int64       uint64_ll;
typedef double                 double64;
#else                 // other targets we've used support long long >= 64 bits
typedef long long              int40;
typedef long long              int64_d;
typedef long long              int64_ll;

typedef unsigned long long     uint40;
typedef unsigned long long     uint64_d;
typedef unsigned long long     uint64_ll;
typedef double                 double64;
#endif                // MSC_VER

/***************************************************************************/
/* The ordering of word0 ... word3 in this structure does not matter from  */
/* a correctness perspective.  It is written in this little endian (LE)    */
/* order because the common case is: C66x uses LE, and the host (usually   */
/* x86) also uses LE.  In that case, this ordering means the image of the  */
/* __x128_t in memory is a byte for byte match.  Nice, but not necessary.  */
/***************************************************************************/
typedef struct {
    uint32 word0;
    uint32 word1;
    uint32 word2;
    uint32 word3;
} __x128_t;

typedef struct {
    uint32 word0;
    uint32 word1;
} __float2_t;

#else 
/* TI compiler data type map */

/***************************************************************************/
/* Starting with v7.0 tools, the compiler supports EABI.  And under EABI   */
/* "long" is 32-bits, not 40.  So, rather than presume long is always 40   */
/* bits, copy the 40-bit wide type from stdint.h.                          */
/***************************************************************************/
#include <stdint.h>
typedef int40_t                int40;
typedef uint40_t               uint40;

typedef double                 int64_d;
typedef long long              int64_ll; 
typedef double                 uint64_d;
typedef unsigned long long     uint64_ll;
typedef double                 double64;

#include <c6x.h>

#endif                // #ifndef _TMS320C6X

#endif                // C6XSIMULATOR_BASE_TYPES_H

/*
 *  Automated Revision Information
 *  Changed: $Date: 2011-02-02 09:47:45 -0600 (Wed, 02 Feb 2011) $
 *  Revision: $Revision: 9975 $
 */

