
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// SPI constants and definitions
//
//
//

#ifndef SPI_
#define SPI_

/* SPI Device Identifiers */
#define SPI_DEV0          0
#define SPI_DEV1          1

/* SPI Base address definitions */
#define SPI0_BASE         0x47000000
#define SPI1_BASE         0x48000000

/* SPI register definitions */
#define SPIGCR0_OFF       0x00
#define SPIGCR1_OFF       0x01
#define SPIINT0_OFF       0x02
#define SPILVL_OFF        0x03
#define SPIFLG_OFF        0x04
#define SPIPC0_OFF        0x05
#define SPIPC1_OFF        0x06
#define SPIPC2_OFF        0x07
#define SPIPC3_OFF        0x08
#define SPIPC4_OFF        0x09
#define SPIPC5_OFF        0x0A
#define SPIPC6_OFF        0x0B
#define SPIPC7_OFF        0x0C
#define SPIPC8_OFF        0x0D
#define SPIDAT0_OFF       0x0E
#define SPIDAT1_OFF       0x0F
#define SPIBUF_OFF        0x10
#define SPIEMU_OFF        0x11
#define SPIDELAY_OFF      0x12
#define SPIDEF_OFF        0x13
#define SPIFMT0_OFF       0x14
#define SPIFMT1_OFF       0x15
#define SPIFMT2_OFF       0x16
#define SPIFMT3_OFF       0x17
#define TGINTVECT0_OFF    0x18
#define TGINTVECT1_OFF    0x19
#define MIBSPIE_OFF       0x1A
#define TGITENST_OFF      0x1B
#define TGITENCR_OFF      0x1C
#define TGITLVST_OFF      0x1D
#define TGITLVCR_OFF      0x1E
#define TGINTFLG_OFF      0x1F
#define TICKCNT_OFF       0x20
#define LTGPEND_OFF       0x21
#define TGCTRL_OFF        0x22
#define DMACTRL_OFF       0x23
#define TXBUF_OFF         0x24
#define RXBUF_OFF         0x25

/* SPI Pin mappings within SPIPCn registers */
#define SPI_SCS            0
#define SPI_ENA            8
#define SPI_CLK            9
#define SPI_SIMO          10
#define SPI_SOMI          11

/* Character length */
#define SPI_CHARLEN8     8       // SPI 8 bit
#define SPI_CHARLEN16    16      // SPI 16 bit

/* Number of pins */
typedef enum spi_np {
    SPI_3,
    SPI_4_SCS,
    SPI_4_RDY,
    SPI_5,
    SPI_5_SRDY
}spi_np;


#endif /* SPI_ */
