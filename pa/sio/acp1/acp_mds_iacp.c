/*
* Copyright (C) 2004-2017 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) Alpha Code Processor Algorithm functionality implementation
//
//
//

/*
 *  ACP Module implementation - MDS implementation of a ACP Algorithm.
 */

 #include <xdc/runtime/Log.h>
 
#include <stdio.h>
#include <xdc/std.h>
//#include <ti/procsdk_audio/procsdk_audio_typ.h>
#include <procsdk_audio_typ.h>
//#include <mem.h> /* for MEM_alloc */

#include <iacp.h>
#include <acp_mds.h>
#include <acp_mds_priv.h>
#include <acperr.h>

#include "acptype.h"
#include "statusOp_common.h"

#include <logp.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
//#include "pafhjt.h"

//#define ENABLE_ALPHA_ERROR_TRACE
#ifdef ENABLE_ALPHA_ERROR_TRACE
 #define AE_TRACE(a) LOG_printf a
#else
 #define AE_TRACE(a)
#endif

// Enable ACP_DEBUG for setting CCS software breakpoint on 0xdead response
//#define ACP_DEBUG
#ifdef ACP_DEBUG
    #define SW_BREAKPOINT asm( " SWBP 0" );
#endif

/*
 *  ======== ACP_MDS_attach ========
 *  MDS's implementation of the attach operation.
 */

Int
ACP_MDS_attach(IACP_Handle handle, Int series, Int beta, IALG_Status *pStatus)
{
    ACP_MDS_Obj *acp = (Void *)handle;

    if (((Uns )series > ACP_SERIES_CUS) || (!acp->config.betaTable[series]))
    {
        AE_TRACE((&trace, "ACP_MDS_attach.%d: ACPERR_ATTACH_NOSERIES error", __LINE__));
        return ACPERR_ATTACH_NOSERIES;
    }
    else if ((Uns )beta >= (acp->config.betaTable[series]->size / sizeof (Ptr)))
    {
        AE_TRACE((&trace, "ACP_MDS_attach.%d: ACPERR_ATTACH_NOBETA error", __LINE__));
        return ACPERR_ATTACH_NOBETA;
    }
    if (acp->config.betaTable[series]->pStatus[beta])
    {
        // Old status present
        if (pStatus)
        {
            // New status not null -- error
            AE_TRACE((&trace, "ACP_MDS_attach.%d: ACPERR_ATTACH_NONULL error", __LINE__));
            return ACPERR_ATTACH_NONULL;
        }
        else
        {
            // New status null -- reset
            acp->config.betaTable[series]->pStatus[beta] = NULL;
            return 0;
        }
    }
    else
    {
        // Old status not present
        acp->config.betaTable[series]->pStatus[beta] = pStatus;
        
        return 0;
    }
}

/*
 *  ======== ACP_MDS_apply ========
 *  MDS's implementation of the apply operation.
 */

Int
ACP_MDS_apply(IACP_Handle handle, const ACP_Unit *from, ACP_Unit *to)
{
    ACP_MDS_Obj *acp = (Void *)handle;

    ACP_Union *x = (ACP_Union *)from;
    ACP_Union *y = (ACP_Union *)to;

    Uns write, series, type;
    Int subtype;

    Int toNeeded;

    Int beta, gamma, rho, sigma;
    Uns phi, psi;
    Int delta, temp = 0;

    if ((x[0].byte.hi & 0xc0) != 0xc0)
        return ((IACP_Obj *)acp)->fxns->legacy(handle, from, to);
    else if (x[0].word == 0xffff)
        return 0; // non-zero fill

    write = x[0].byte.hi & 0x08;
    series = (x[0].byte.hi >> 4) & 0x03;
    type = x[0].byte.hi & 0x07;
    subtype = -1;
    #if 0 // Enable for debugging only
        AE_TRACE((&trace, "ACP_MDS_apply: write = 0x%x, series = 0x%x, type = 0x%x", write, series, type));
    #endif

    if (series >= 4 || ! acp->config.betaTable[series])
    {
        AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOSERIES error for series = %d", __LINE__, series));
        return ACPERR_APPLY_NOSERIES;
    }

    toNeeded = ! write;

    beta = gamma = -1;
    switch (type)
    {
      case 0:
        if (write)
        {
            phi = x[0].byte.lo;
            psi = x[1].byte.hi;
            delta = (SmInt )x[1].byte.lo;
        }
        else
        {
            beta = x[0].byte.lo;
            gamma = x[1].byte.hi;
            rho = x[1].byte.lo;
            if (rho & 0x80)
                toNeeded = 0;
        }
        break;
      case 2:
      case 6:
        beta = x[0].byte.lo;
        gamma = x[1].byte.hi;
        break;
      case 3:
      case 4:
        beta = x[0].byte.lo;
        gamma = x[1].word;
        break;
      case 1:
        if (! write)
        {
            sigma = x[0].byte.lo;
            toNeeded = 0;
        }
        break;
      case 5:
        subtype = x[0].byte.lo;
        switch (subtype) {
          case 0:
            type = subtype;
            if (write)
            {
                phi = x[1].word;
                psi = x[2].word;
                delta = (MdInt )x[3].word;
            }
            else
            {
                beta = x[1].word;
                gamma = x[2].word;
                rho = x[3].word;
                if (rho & 0x80)
                    toNeeded = 0;
            }
            break;
          case 1:
          case 12:
            type = subtype;
            if (! write)
            {
                sigma = x[1].word;
                toNeeded = 0;    // correction for MID #378
            }
            break;
          case 6:
          case 10:
            type = subtype;
            beta = x[1].word;
            gamma = x[2].word;
            break;
          case 8:
            if (write)
            {
                AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOSUBTYPE error", __LINE__));
                return ACPERR_APPLY_NOSUBTYPE;
            }
            else
            {
                type = subtype;
                beta = x[1].word;
            }
            break;
          case 9:
            if (write)
            {
                switch (x[1].byte.hi)
                {
                  case 4:
                    acp->pStatus->betaPrimeValue = x[1].byte.lo;
                    break;
                  case 5:
                    acp->pStatus->betaPrimeBase = x[1].byte.lo;
                    break;
                  case 6:
                    acp->pStatus->betaPrimeOffset =
                        (acp->pStatus->betaPrimeOffset & 0xff00) + x[1].byte.lo;
                    break;
                  case 7:
                    acp->pStatus->betaPrimeOffset =
                        (acp->pStatus->betaPrimeOffset & 0x00ff)
                        + ((MdInt )x[1].byte.lo << 8);
                    break;
                  default:
                    AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NO59 error", __LINE__));
                    return ACPERR_APPLY_NO59;
                }
            }
            else
            {
                SmUns a;
                switch (x[1].byte.hi)
                {
                  case 4:
                    a = acp->pStatus->betaPrimeValue;
                    break;
                  case 5:
                    a = acp->pStatus->betaPrimeBase;
                    break;
                  case 6:
                    a = acp->pStatus->betaPrimeOffset;
                    break;
                  case 7:
                    a = acp->pStatus->betaPrimeOffset >> 8;
                    break;
                  default:
                    AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NO59 error", __LINE__));
                    return ACPERR_APPLY_NO59;
                }
                if (to) {
                    y[0].word = 0xcd09 + (series << 12);
                    y[1].word = ((MdInt )x[1].byte.hi << 8) + a;
                }
                else {
                    acp->pStatus->args[x[1].byte.lo] = a;
                }
            }
            return 0;
          case 11:
            type = subtype;
            if (x[1].word < 5)
            {
                AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_BAD511 error", __LINE__));
                return ACPERR_APPLY_BAD511;
            }
            break;
          case 0xf0:
          case 0xf1:
          case 0xf2:
          case 0xf3:
            if (! write)
            {
                XDAS_Int32 a = acp->pStatus->args[subtype&3];
                if (to)
                {
                    y[0].word = a & 0xffff;
                    y[1].word = a >> 16;
                }
                return 0;
            }
            /* ... fall through to other cases ... */
          case 0xf4:
          case 0xf5:
          case 0xf6:
          case 0xf7:
          case 0xf8:
          case 0xf9:
          case 0xfa:
          case 0xfb:
          case 0xfc:
          case 0xfd:
          case 0xfe:
          case 0xff:
            if (! write)
            {
                AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOSUBTYPE error", __LINE__));
                return ACPERR_APPLY_NOSUBTYPE;
            }
            else
            {
                volatile XDAS_Int32 *pA = &acp->pStatus->args[subtype&3];
                XDAS_Int32 a;
                XDAS_Int16 z = x[1].word;
                if ((subtype & 0x0c) == 0)
                    a = (XDAS_Int32 )z;
                else if ((subtype & 0x0c) == 0x04)
                    a = (XDAS_Int32 )z & 0xffff;
                else if ((subtype & 0x0c) == 0x08)
                    a = ((XDAS_Int32 )z << 16) + (*pA & 0xffff);
                else
                    a = ((XDAS_Int32 )z << 16);
                *pA = a;
                if (to)
                {
                    y[0].word = a & 0xffff;
                    y[1].word = a >> 16;
                }
            }
            return 0;
          default:
            AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOSUBTYPE error", __LINE__));
            return ACPERR_APPLY_NOSUBTYPE;
        }
        break;
      case 7:
        break;
      default:
        AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOTYPE error", __LINE__));
        return ACPERR_APPLY_NOTYPE;
    }

    if (toNeeded && to == NULL)
    {
        AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NODEST error", __LINE__));
        return ACPERR_APPLY_NODEST;
    }

    // Check & relocate beta, if utilized.
    if (beta >= 0)
    {
        if (beta >= acp->pStatus->betaPrimeBase)
            beta += acp->pStatus->betaPrimeValue*acp->pStatus->betaPrimeOffset;
        if (beta >= acp->config.betaTable[series]->size / sizeof (Ptr)
            || ! acp->config.betaTable[series]->pStatus[beta])
        {
            // If enabled, this trace fills print log with too many errors due to system stream,
            // for Bass Management or Deemphasis not included in ASP chain.
            if (! acp->config.betaTable[series]->pStatus[beta])
                AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOBETA error for beta = 0x%x", __LINE__, beta));
                //Log_info1("ACP_MDS_apply(): ACPERR_APPLY_NOBETA error, beta = 0x%x", beta);

            return ACPERR_APPLY_NOBETA;
        }
        if (acp->config.betaTable[series]->pStatus[beta]->size < 0)
            return ((IACP_Obj *)acp)->fxns->escape(handle, from, to);
    }

    // Check gamma, if utilized.
    if (gamma >= 0)
    {
        if (gamma >= acp->config.betaTable[series]->pStatus[beta]->size)
        {
            AE_TRACE((&trace, "ACP_MDS_apply: ACPERR_APPLY_NOGAMMA error for gamma = 0x%x", gamma));
            //Log_info1("ACP_MDS_apply(): ACPERR_APPLY_NOGAMMA error, gamma = 0x%x", gamma);
            return ACPERR_APPLY_NOGAMMA;
        }
    }

#ifdef _TMS320C6X
    switch (type)
    {
      case 0:
        if (write)
        {
            LgInt args[4];
            ACP_Dublin d;
            if (phi >= acp->config.phiTable[series]->size / sizeof (Ptr)
                || ! acp->config.phiTable[series]->pFunction[phi])
            {
                AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOPHI error for phi = 0x%x", __LINE__, phi));
                return ACPERR_APPLY_NOPHI;
            }
            args[0] = acp->pStatus->args[0];
            args[1] = acp->pStatus->args[1];
            args[2] = acp->pStatus->args[2];
            args[3] = acp->pStatus->args[3];
            if (psi & 0x08)
            {
                if (! (psi & 0x04))
                {
                    if (subtype >= 0)
                        delta &= 0xffff;
                    else
                        delta &= 0xff;
                }
                args[psi & 0x03] = delta;
            }
            d.full = acp->config.phiTable[series]->pFunction[phi](args[0],
                args[1], args[2], args[3]);
            acp->pStatus->args[(psi >> 4) & 0x03] = d.full;
            if (to)
            {
                y[0] = d.part.lo;
                y[1] = d.part.hi;
            }
        }
        else
        {
            ACP_Dublin d;
            if (! (rho & 0x80))
            {
                switch (rho)
                {
                  case 0x00:
                    break;
                  case 0x11:
                  case 0x19:
                    //y[0].byte.lo = ((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma];
                    statusOp_read_beta(&y[0].byte.lo,
                                      &((SmInt *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                      sizeof(SmUns), beta);
                    break;
                  case 0x21:
                    //y[0].word = (MdUns )((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma];
                    statusOp_read_beta(&temp,
                                      &((SmInt *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                      sizeof(SmUns), beta);
                    y[0].word = (MdUns )temp;
                    break;
                  case 0x22:
                  case 0x2a:
                    //_amem2(&y[0].word) = _amem2(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]);
                    statusOp_read_beta(&y[0].word,
                                      &((SmInt *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                      sizeof(y[0].word), beta);
                    break;
                  case 0x40:
                    d.full = (LgUns )&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma];
                    y[0] = d.part.lo;
                    y[1] = d.part.hi;
                    break;
                  case 0x41:
                    //d.full = (LgUns )((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma];
                    statusOp_read_beta(&temp,
                                      &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                      sizeof(SmUns), beta);
                    d.full = (LgUns )temp;
                    y[0] = d.part.lo;
                    y[1] = d.part.hi;
                    break;
                  case 0x42:
                    //_amem2(&d.part.lo.word) = _amem2(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]);
                    statusOp_read_beta(&d.part.lo.word,
                                      &((SmInt *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                      sizeof(d.part.lo.word), beta);
                    d.full = (LgUns )d.part.lo.word;
                    y[0] = d.part.lo;
                    y[1] = d.part.hi;
                    break;
                  case 0x44:
                  case 0x4c:
                    //_amem4(&d.full) = _amem4(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]);
                    statusOp_read_beta(&d.full,
                                      &((SmInt *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                      sizeof(d.full), beta);
                    y[0] = d.part.lo;
                    y[1] = d.part.hi;
                    break;
                  case 0x29:
                    //y[0].word = (MdInt )((SmInt *)acp->config.betaTable[series]->pStatus[beta])[gamma];
                    statusOp_read_beta(&temp,
                                      &((SmInt *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                      sizeof(SmInt), beta);
                    y[0].word = (MdInt )temp;
                    break;
                  case 0x49:
                    //d.full = (LgInt )((SmInt *)acp->config.betaTable[series]->pStatus[beta])[gamma];
                    statusOp_read_beta(&temp,
                                      &((SmInt *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                      sizeof(SmInt), beta);
                    d.full = (LgInt )temp;
                    y[0] = d.part.lo;
                    y[1] = d.part.hi;
                    break;
                  case 0x4a:
                    //_amem2(&d.part.lo.word) = _amem2(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]);
                    statusOp_read_beta(&d.part.lo.word,
                                      &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                      sizeof(d.part.lo.word), beta);
                    d.full = (LgInt )d.part.lo.word;
                    y[0] = d.part.lo;
                    y[1] = d.part.hi;
                    break;
                  default:
                    AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NORHO error", __LINE__));
                    return ACPERR_APPLY_NORHO;
                }
            }
            else if (! (rho & 0x40))
            {
                switch (rho & 0x0f)
                {
                  case 0x00:  // Address of quantity returned as 32-bit value.
                    // TODO: Verify. Can the address change? if yes, then we need to use the common status API.
                    d.full = (LgUns )&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma];
                    break;
                  case 0x01:
                    //d.full = (LgUns )((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma];
                    statusOp_read_beta(&temp,
                                       &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                       sizeof(SmUns), beta);
                    d.full = (LgUns)temp;
                    break;
                  case 0x02:
                    //_amem2(&d.part.lo.word) = _amem2(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]);
                    statusOp_read_beta(&d.part.lo.word,
								       &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
								       sizeof(d.part.lo.word), beta);
                	d.full = (LgUns )d.part.lo.word;
                    break;
                  case 0x04:
                  case 0x0c:
                    //_amem4(&d.full) = _amem4(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]);
                    statusOp_read_beta(&d.full,
								       &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
								       sizeof(d.full), beta);
                    break;
                  case 0x09:
                    //d.full = (LgInt )((SmInt *)acp->config.betaTable[series]->pStatus[beta])[gamma];
				    statusOp_read_beta(&temp,
				                       &((SmInt *)acp->config.betaTable[series]->pStatus[beta])[gamma],
				                       sizeof(SmInt), beta);
				    d.full = (LgInt)temp;
                    break;
                  case 0x0a:
                    //_amem2(&d.part.lo.word) = _amem2(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]);
                	statusOp_read_beta(&d.part.lo.word,
                	                   &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                	                   sizeof(d.part.lo.word), beta);
                	d.full = (LgInt )d.part.lo.word;
                    break;
                }
                acp->pStatus->args[(rho >> 4) & 0x03] = d.full;
                if (to)
                {
                    y[0] = d.part.lo;
                    y[1] = d.part.hi;
                }
            }
            else
            {
                d.full = acp->pStatus->args[(rho >> 4) & 0x03];
                switch (rho & 0x0f)
                {
                  case 0x00:
                    break;
                  case 0x01:
                    //((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma] = d.part.lo.byte.lo;
                	  statusOp_write_beta(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                			              &d.part.lo.byte.lo, sizeof(d.part.lo.byte.lo), beta);
                	break;
                  case 0x02:
                    //_amem2(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]) = _amem2(&d.part.lo.word);
                	  statusOp_write_beta(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                			              &d.part.lo.word, sizeof(d.part.lo.word), beta);
                	break;
                  case 0x04:
                    //_amem4(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]) = _amem4(&d.full);
                	  statusOp_write_beta(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                			              &d.full, sizeof(d.full), beta);
                    break;
                }
                if (to)
                {
                    y[0] = d.part.lo;
                    y[1] = d.part.hi;
                }
            }
        }
        return 0;
      case 12:
      case 1:
        if (! write)
        {
            if (sigma >= acp->config.sigmaTable[series]->size / sizeof (Ptr))
            {
            	//printf("sigma: 0x%x >= tablesize 0x%x.\n",
            	//		sigma, acp->config.sigmaTable[series]->size / sizeof (Ptr) );
                AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOSIGMA (out of table range) error for sigma = %d",
                    __LINE__, sigma));
                return ACPERR_APPLY_NOSIGMA;
            }
            if (!acp->config.sigmaTable[series]->pSequence[sigma])
            {
                if ((sigma != 1 /* Default */) && (sigma != 2 /* AtBoot */) && (sigma != 4 /* AtTime */))
                {
                    AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOSIGMA (null address) error for sigma = %d",
                        __LINE__, sigma));
                }
                return ACPERR_APPLY_NOSIGMA;
            }
            from = acp->config.sigmaTable[series]->pSequence[sigma];
        }
        {
            Int errno = ((IACP_Obj *)acp)->fxns->sequence(handle, from, NULL);
            if (to)
            {
                y[0].word = acp->pStatus->args[0] & 0xffff;
                if (subtype >= 0)
                    y[1].word = acp->pStatus->args[0] >> 16;
            }
            // AE_TRACE for sequence error is covered in sequence function below.
            return errno;
        }
        /* unreachable */
      case 2:   // 8-bit Read/Write
        if (write)
        {
           //((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma] = x[1].byte.lo;
           /*-->ok*/ statusOp_write_beta(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                &x[1].byte.lo, sizeof(x[1].byte.lo), beta);
        }
        else
        {
            y[0].word = x[0].word ^ 0x0800;
            y[1].byte.hi = x[1].byte.hi;
            //y[1].byte.lo = ((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma];
            statusOp_read_beta(&y[1].byte.lo, &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                               sizeof(y[1].byte.lo), beta);
        }
        return 0;
      case 3:   // 16-bit Read/Write
        if (write)
        {
            //*(MdUns *)&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma] = x[2].word;
        	/*-->ok*/statusOp_write_beta(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
        	                    &x[2].word, sizeof(x[2].word), beta);
        }
        else
        {
            y[0].word = x[0].word ^ 0x0800;
            y[1].word = x[1].word;
            //y[2].word = *(MdUns *)&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma];
            statusOp_read_beta(&y[2].word, &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                               sizeof(MdUns), beta);
        }
        return 0;
      case 4:   // 32-bit Read/Write
        if (write)
        {
            ACP_Dublin d;
            d.part.lo = x[2];
            d.part.hi = x[3];
            //_amem4(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]) = _amem4(&d.full);
            statusOp_write_beta(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                &d.full, sizeof(d.full), beta);
        }
        else
        {
            ACP_Dublin d;
            y[0].word = x[0].word ^ 0x0800;
            y[1].word = x[1].word;
            //_amem4(&d.full) = _amem4(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]);
            statusOp_read_beta(&d.full, &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                               sizeof(d.full), beta);
            y[2] = d.part.lo;
            y[3] = d.part.hi;
        }
        return 0;
      case 6:   // Variable-Length Read/Write
        if (subtype < 0)
        {
            Uns kappa = x[1].byte.lo;
            if (write)
            { //memcpy (&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma], &x[2], kappa);
                statusOp_write_beta(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                    &x[2], kappa, beta);
            }
            else
            {
                y[0].word = x[0].word ^ 0x0800;
                y[1].word = x[1].word;
                //memcpy (&y[2], &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma], kappa);
                statusOp_read_beta(&y[2],
                                   &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                   kappa, beta);
            }
        }
        else    // From Type 5 alpha command (Extended Alpha Code Type)
        {
            Uns kappa = x[3].word;
            if (write)
            {
               //memcpy (&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma], &x[4], kappa);
            	statusOp_write_beta(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
            	                    &x[4], kappa, beta);
            }
            else
            {
                y[0].word = x[0].word ^ 0x0800;
                y[1].word = x[1].word;
                y[2].word = x[2].word;
                y[3].word = x[3].word;
                //memcpy (&y[4], &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma], kappa);
                statusOp_read_beta(&y[4],
                                   &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                                   kappa, beta);
            }
        }
        return 0;
      case 7:   // Physical Read/Write
        {
            Int zeta = x[0].byte.lo;
            Int kappa = x[1].word;
            ACP_Dublin d;
            d.part.lo = x[2];
            d.part.hi = x[3];
            switch (zeta)
            {
              case 0:
                // FOR NOW --Kurt
              case 1:
                {
                    far SmInt *pi = (SmInt *)d.full;
                    if (write)
                    {
                        memcpy (pi, &x[4], kappa);
                    }
                    else
                    {
                        y[0].word = x[0].word ^ 0x0800;
                        y[1].word = x[1].word;
                        y[2].word = x[2].word;
                        y[3].word = x[3].word;
                        memcpy (&y[4], pi, kappa);
                    }
                }
                break;
              case 2:
               {
                    Int i;
                    far MdInt *pi = (MdInt *)d.full;
                    if ((kappa & 1) || ((Int )pi & 1))
                    {
                        AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOZETA error", __LINE__));
                        return ACPERR_APPLY_NOZETA;
                    }
                    else if (write)
                    {
                        for (i=0; i < kappa/2; i++)
                            _amem2(&pi[i]) = _amem2(&x[4+i].word);
                    }
                    else
                    {
                        y[0].word = x[0].word ^ 0x0800;
                        y[1].word = x[1].word;
                        y[2].word = x[2].word;
                        y[3].word = x[3].word;
                        for (i=0; i < kappa/2; i++)
                            _amem2(&y[4+i].word) = _amem2(&pi[i]);
                    }
                }
                break;
              case 4:
                {
                    Int i;
                    far LgInt *pi = (LgInt *)d.full;
                    if ((kappa & 3) || ((Int )pi & 3))
                    {
                        AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOZETA error", __LINE__));
                        return ACPERR_APPLY_NOZETA;
                    }
                    else if (write)
                    {
                        for (i=0; i < kappa/4; i++)
                        {
                            d.part.lo = x[4+2*i];
                            d.part.hi = x[5+2*i];
                            _amem4(&pi[i]) = _amem4(&d.full);
                        }
                    }
                    else
                    {
                        y[0].word = x[0].word ^ 0x0800;
                        y[1].word = x[1].word;
                        y[2].word = x[2].word;
                        y[3].word = x[3].word;
                        for (i=0; i < kappa/4; i++)
                        {
                            _amem4(&d.full) = _amem4(&pi[i]);
                            y[4+2*i] = d.part.lo;
                            y[5+2*i] = d.part.hi;
                        }
                    }
                }
                break;
              default:
                AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOZETA error", __LINE__));
                return ACPERR_APPLY_NOZETA;
            }
        }
        return 0;
      case 8:
        {
            Int htgnel = ((IACP_Obj *)acp)->fxns->htgnel(handle, from);
            if (write)
            {
                AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_INTERNAL error", __LINE__));
                return ACPERR_APPLY_INTERNAL;
            }
            else if (htgnel == -2)
            {
                return ((IACP_Obj *)acp)->fxns->escape(handle, from, to);
            }
            else if (htgnel < 2)
            {
                #ifdef ACP_DEBUG
                    SW_BREAKPOINT;
                #endif
                y[0].word = 0xdead;
                AE_TRACE((&trace, "ACP_MDS_apply.%d:  0xdead: case 8 alpha error", __LINE__));
            }
            else
            {
                IALG_Status *pStatus = acp->config.betaTable[series]->pStatus[beta];
                Uns kappa = 2 * (htgnel - 2);
                if (beta < 256 && kappa < 256)
                {
                    y[0].word = 0xce00 + (series << 12) + beta;
                    y[1].word = kappa;
                   //memcpy (&y[2], pStatus, kappa);
                    statusOp_read_beta(&y[2], pStatus, kappa, beta);
                }
                else
                {
                    y[0].word = 0xcd06 + (series << 12);
                    y[1].word = beta;
                    y[2].word = 0;
                    y[3].word = kappa;
                    //memcpy (&y[4], pStatus, kappa);
                    statusOp_read_beta(&y[4], pStatus, kappa, beta);
                }
            }
        }
        return 0;
      case 10:
        {   // variable-length indirect read/write
            Uns kappa = x[3].word;
            far SmUns *pi;
            //_amem4(&pi) = _amem4(&((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma]);
            statusOp_read_beta(&pi,
                               &((SmUns *)acp->config.betaTable[series]->pStatus[beta])[gamma],
                               sizeof(LgUns), beta);
            if (write)
            {
                memcpy (pi, &x[4], kappa);
                //statusOp_write_beta(pi, &x[4], kappa, beta);
            }
            else
            {
                y[0].word = x[0].word ^ 0x0800;
                y[1].word = x[1].word;
                y[2].word = x[2].word;
                y[3].word = x[3].word;
                memcpy (&y[4], pi, kappa);
                //statusOp_read_beta(&y[4], pi, kappa, beta);
            }
        }
        return 0;
      case 11:
        {
            MdUns lambda = x[1].word; // known >= 5
            Uns xi = x[2].byte.hi;
            Uns phi = x[2].byte.lo;
            MdUns kappa = x[3].word;
            MdUns tau = x[4].word;
            LgUns delta = (LgUns )x[5].word + ((LgUns )x[6].word << 16);
            LgInt (*func)(LgInt) = (LgInt (*)(LgInt) )acp->config.phiTable[series]->pFunction[phi];
            if (lambda == 5)
            {
                for (;;)
                {
                    Int errno;
                    if (kappa-- == 0)
                    {
                        break;
                    }
                    else if (errno = (*func)(tau))
                    {
                        AE_TRACE((&trace, "ACP_MDS_apply.%d: tau error", __LINE__));
                        return errno;
                    }
                }
                if (to)
                {
                    y[0].word = x[5].word;
                    y[1].word = x[6].word;
                }
            }
            else
            {
                for (;;)
                {
                    Int errno;
                    if (errno = ((IACP_Obj *)acp)->fxns->apply(handle, from+7, to))
                        return errno;
                    else if (xi == 0)
                         break;
                    else if (xi == 1)
                         ;
                    else if (xi == 2 && acp->pStatus->args[0] == delta)
                         break;
                    else if (xi == 3 && acp->pStatus->args[0] != delta)
                         break;
                    else if (xi == 4 && acp->pStatus->args[0] >= delta)
                         break;
                    else if (xi == 5 && acp->pStatus->args[0] >  delta)
                         break;
                    else if (xi == 6 && acp->pStatus->args[0] <= delta)
                         break;
                    else if (xi == 7 && acp->pStatus->args[0] <  delta)
                         break;
                    else if (xi == 8 && (acp->pStatus->args[0] & delta))
                         break;
                    else if (xi == 9 && ~(acp->pStatus->args[0] | delta))
                         break;
                    if (kappa-- == 0)
                    {
                        AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_TIMEOUT error", __LINE__));
                        return ACPERR_APPLY_TIMEOUT;
                    }
                    else if (errno = (*func)(tau))
                    {
                        AE_TRACE((&trace, "ACP_MDS_apply.%d: tau error", __LINE__));
                        return errno;
                    }
                }
                if (to)
                {
                    y[0].word = acp->pStatus->args[0] & 0xffff;
                    y[1].word = acp->pStatus->args[0] >> 16;
                }
            }
        }
        return 0;
      default:
        AE_TRACE((&trace, "ACP_MDS_apply.%d: ACPERR_APPLY_NOTYPE error", __LINE__));
        return ACPERR_APPLY_NOTYPE;
    }
#else
    return ACPERR_APPLY_UNSPECIFIED;
#endif

    /* unreachable */
}

/*
 *  ======== ACP_MDS_escape ========
 *  MDS's implementation of the escape operation.
 */

Int
ACP_MDS_escape(IACP_Handle handle, const ACP_Unit *from, ACP_Unit *to)
{
    // none known
    AE_TRACE((&trace, "ACP_MDS_escape.%d: ACPERR_ESCAPE_UNSPECIFIED error", __LINE__));
    return ACPERR_ESCAPE_UNSPECIFIED;
}

/*
 *  ======== ACP_MDS_legacy ========
 *  MDS's implementation of the legacy operation.
 */

Int
ACP_MDS_legacy(IACP_Handle handle, const ACP_Unit *from, ACP_Unit *to)
{
    if ((from[0] & 0xc000) == 0x4000)
    {

        // DA/M

        AE_TRACE((&trace, "ACP_MDS_legacy.%d: ACPERR_LEGACY_UNSPECIFIED error", __LINE__));
        return ACPERR_LEGACY_UNSPECIFIED;
    }
    else
    {

        // IA/M

        if (to)
        {
            Int i;
            to[0] = 0xbad0;
            for (i=1; i < 16; i++)
                to[i] = 0;
        }
        return 0;
    }
}

/*
 *  ======== ACP_MDS_length ========
 *  MDS's implementation of the length operation.
 */

Int
ACP_MDS_length(IACP_Handle handle, const ACP_Unit *from)
{
    ACP_Union *x = (ACP_Union *)from;

    SmInt len;

    static const SmInt length[16] =
    {
        2, // type 0 read
        1, // type 1 read
        2, // type 2 read
        2, // type 3 read
        2, // type 4 read
        -1, // type 5 read
        2, // type 6 read
        4, // type 7 read
        2, // type 0 write
        -4, // type 1 write
        2, // type 2 write
        3, // type 3 write
        4, // type 4 write
        -1, // type 5 write
        -2, // type 6 write
        -3, // type 7 write
    };

    if ((x[0].byte.hi & 0xc0) == 0x40)
        return -1; // DA/M
    else if ((x[0].byte.hi & 0xc0) != 0xc0)
        return (x[1].word & 0x7fff) + 14; // IA/M
    else if (x[0].word == 0xffff)
        return 1; // non-zero fill

    len = length[x[0].byte.hi & 0x0f];

    if (len >= 0)
    {
        return len;
    }
    else if (len == -1)
    {
        MdUns alpha = x[0].word & 0xcfff;
        if (alpha == 0xc500)
            /* type 5-0 read  */ return 4;
        else if (alpha == 0xcd00)
            /* type 5-0 write */ return 4;
        else if (alpha == 0xc501)
            /* type 5-1 read  */ return 2;
        else if (alpha == 0xcd01)
            /* type 5-1 write */ return x[1].word + 2;
        else if (alpha == 0xc506)
            /* type 5-6 read  */ return 4;
        else if (alpha == 0xcd06)
            /* type 5-6 write */ return 4+(x[3].word+1)/2;
        else if (alpha == 0xc508)
            /* type 5-8 read  */ return 2;
        else if (alpha == 0xc509)
            /* type 5-9 read  */ return 2;
        else if (alpha == 0xcd09)
            /* type 5-9 write */ return 2;
        else if (alpha == 0xc50a)
            /* type 5-10 read  */ return 4;
        else if (alpha == 0xcd0a)
            /* type 5-10 write */ return 4+(x[3].word+1)/2;
        else if (alpha == 0xc50b)
            /* type 5-11 read  */ return x[1].word + 2;
        else if (alpha == 0xcd0b)
            /* type 5-11 write */ return x[1].word + 2;
        else if (alpha == 0xc50c)
            /* type 5-12 read  */ return 2;
        else if (0xc5f0 <= alpha && alpha <= 0xc5f3)
            /* type 5-24X read  */ return 2;
        else if (0xcdf0 <= alpha && alpha <= 0xcdff)
            /* type 5-24X write */ return 2;
        return -3; // Unknown ?
    }
    else if (len == -2)
    {
        SmUns kappa = (SmUns )x[1].byte.lo;
        return 2+(kappa+1)/2;
    }
    else if (len == -3)
    {
        Uns kappa = (MdUns )x[1].word;
        return 4+(kappa+1)/2;
    }
    else if (len == -4)
    {
        Uns lambda = (SmUns )x[0].byte.lo;
        return 1+lambda;
    }
    else
    {
        return -3; // Unknown ?
    }
}

/*
 *  ======== ACP_MDS_htgnel ========
 *  MDS's implementation of the htgnel operation.
 */

Int
ACP_MDS_htgnel(IACP_Handle handle, const ACP_Unit *from)
{
    ACP_MDS_Obj *acp = (Void *)handle;

    ACP_Union *x = (ACP_Union *)from;

    SmInt len;

    static const SmInt htgnel[16] = {
        2, // type 0 read
        1, // type 1 read
        2, // type 2 read
        3, // type 3 read
        4, // type 4 read
        -1, // type 5 read
        -2, // type 6 read
        -3, // type 7 read
        2, // type 0 write
        1, // type 1 write
        0, // type 2 write
        0, // type 3 write
        0, // type 4 write
        -1, // type 5 write
        0, // type 6 write
        0, // type 7 write
    };

    if ((x[0].byte.hi & 0xc0) == 0x40)
        return -1; // DA/M
    else if ((x[0].byte.hi & 0xc0) != 0xc0)
        return 14; // IA/M
    else if (x[0].word == 0xffff)
        return 0; // non-zero fill

    len = htgnel[x[0].byte.hi & 0x0f];

    if (len >= 0)
        return len;
    else if (len == -1) {
        MdUns alpha = x[0].word & 0xcfff;
        if (alpha == 0xc500)
            /* type 5-0 read  */ return 2;
        else if (alpha == 0xcd00)
            /* type 5-0 write */ return 2;
        else if (alpha == 0xc501)
            /* type 5-1 read  */ return 2;
        else if (alpha == 0xcd01)
            /* type 5-1 write */ return 2;
        else if (alpha == 0xc506)
            /* type 5-6 read  */ return 4+(x[3].word+1)/2;
        else if (alpha == 0xcd06)
            /* type 5-6 write */ return 0;
        else if (alpha == 0xc508) {
            /* type 5-8 read  */
            Int beta = x[1].word;
            Int series = (x[0].byte.hi & 0x30) >> 4;
            Int size;
            if (beta >= acp->pStatus->betaPrimeBase)
                beta += acp->pStatus->betaPrimeValue*acp->pStatus->betaPrimeOffset;
            if (beta >= acp->config.betaTable[series]->size / sizeof (Ptr)
                || ! acp->config.betaTable[series]->pStatus[beta])
                return 1;
            size = acp->config.betaTable[series]->pStatus[beta]->size;
            if (size < 0)
                return -2;
            else
                return 2+(size+1)/2;
        }
        else if (alpha == 0xc509)
            /* type 5-9 read  */ return 2;
        else if (alpha == 0xcd09)
            /* type 5-9 write */ return 0;
        else if (alpha == 0xc50a)
            /* type 5-10 read */ return 4+(x[3].word+1)/2;
        else if (alpha == 0xcd0a)
            /* type 5-10 write */ return 0;
        else if (alpha == 0xc50b)
            /* type 5-11 read  */ return 2;
        else if (alpha == 0xcd0b)
            /* type 5-11 write */ return 0;
        else if (alpha == 0xc50c)
            /* type 5-12 read  */ return 0;
        else if (0xc5f0 <= alpha && alpha <= 0xc5f3)
            /* type 5-X read  */ return 2;
        else if (0xcdf0 <= alpha && alpha <= 0xcdff)
            /* type 5-X write */ return 0;
        return -3; // Unknown ?
    }
    else if (len == -2) {
        SmUns kappa = (SmUns )x[1].byte.lo;
        return 2+(kappa+1)/2;
    }
    else if (len == -3) {
        Uns kappa = (MdUns )x[1].word;
        return 4+(kappa+1)/2;
    }
    else
        return -3; // Unknown ?
}

/*
 *  ======== ACP_MDS_sequence ========
 *  MDS's implementation of the sequence operation.
 */

Int
ACP_MDS_sequence(IACP_Handle handle, const ACP_Unit *from, ACP_Unit *to)
{
    // Needs check on size of to. --Kurt

    ACP_MDS_Obj *acp = (Void *)handle;

    Int n, n0, nF, nT, m, errno;

    const ACP_Unit *pF;
    ACP_Unit *pT = to + 1;

    ACP_Unit f0 = from[0];

    if ((f0 & 0xc000) == 0x0000 || (f0 & 0xc000) == 0x8000)
        return ((IACP_Obj *)acp)->fxns->legacy(handle, from, to); // IA/M

    if ((f0 & 0xcf00) == 0xc900) {
        n0 = f0 & 0x00ff;
        pF = from + 1;
    }
    else if ((f0 & 0xcfff) == 0xcd01) {
        n0 = from[1];
        pF = from + 2;
    }
    else
    {
        AE_TRACE((&trace, "ACP_MDS_sequence.%d: ACPERR_SEQUENCE_NOTYPE error", __LINE__));
        return ACPERR_SEQUENCE_NOTYPE;
    }

    for (n=n0, m=0; n > 0; n-=nF, pF+=nF, pT+=nT, m+=nT) {
        if (! *pF)
            nF = 1, nT = 0;
        else if ((nF = ((IACP_Obj *)acp)->fxns->length(handle, pF)) <= 0)
        {
            AE_TRACE((&trace, "ACP_MDS_sequence.%d: ACPERR_SEQUENCE_NOFROMLENGTH error", __LINE__));
            return ACPERR_SEQUENCE_NOFROMLENGTH;
        }
        else if ((nT = ((IACP_Obj *)acp)->fxns->htgnel(handle, pF)) < 0)
        {
            AE_TRACE((&trace, "ACP_MDS_sequence.%d: ACPERR_SEQUENCE_NODESTLENGTH error", __LINE__));
            return ACPERR_SEQUENCE_NODESTLENGTH;
        }
        else if (errno = ((IACP_Obj *)acp)->fxns->apply(handle, pF, to?pT:to))
        {
            // If enabled, this trace fills print log with too many errors due to system stream,
            // for Bass Management or Deemphasis not included in ASP chain.
            //AE_TRACE((&trace, "ACP_MDS_sequence.%d: apply() returned error = %d", __LINE__, errno));
            return errno;
        }
    }

    if (to) {
        if (! (f0 & 0x8000))
            *to = m;
        else if (m < 256)
            *to = 0xc900 + (f0 & 0x3000) + m;
        else {
            for (n=m; n>0; n--)
              to[n+1] = to[n];
            to[1] = m;
            to[0] = 0xcd01 + (f0 & 0x3000);
        }
    }

    if (errno = (n0 == 0 ? 0 : n == 0 ? 0 : ACPERR_SEQUENCE_NOMATCHLENGTH))
        AE_TRACE((&trace, "ACP_MDS_sequence.%d: exiting with ACPERR_SEQUENCE_NOMATCHLENGTH error", __LINE__));
    return errno;
}
