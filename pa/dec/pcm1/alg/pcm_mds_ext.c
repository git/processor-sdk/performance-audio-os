/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
//
//
// Vendor-specific (MDS) PCM algorithm instantiation implementation
//

#include <xdc/std.h> //<std.h>
#include <ti/xdais/ialg.h> //<ialg.h>
//#include <ti/procsdk_audio/procsdk_audio_typ.h>
#include <procsdk_audio_typ.h>

#include <alg.h>
#include <ti/xdais/ialg.h>
#include <paf_alg.h>

#include <pcm.h>
#include <ipcm.h>
#include <pcm_mds.h>
#include <pcm_mds_priv.h>

/*
 *  ======== PCM_MDS_create ========
 */
PCM_MDS_Handle PCM_MDS_create(const PCM_Params *params)
{
    return ((Void *)PAF_ALG_create(&PCM_MDS_IALG, NULL, (IALG_Params *)params,NULL,NULL));
}

/*
 *  ======== PCM_MDS_delete ========
 */
  /* COM_TIH_delete */

/*
 *  ======== PCM_MDS_exit ========
 */
  /* COM_TIH_exit */

/*
 *  ======== PCM_MDS_init ========
 */
  /* COM_TIH_init */

