/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX implementations for DAP_DMA
//
//
//

#include <stdio.h>	// for printf on error
#include <std.h>
#include <c64.h> //for IER
#include <hwi.h>
#include <gbl.h>
#include <sio.h>
#include  <std.h>
#include <xdas.h>

#ifndef DAP_CACHE_SUPPORT
  #define DAP_CACHE_SUPPORT  // typically defined in the project
#endif

#ifdef DAP_CACHE_SUPPORT
#include <ti/sysbios/hal/Cache.h>
#endif

#include "dap.h"
#include "dap_dmax.h"

#include <cslr_dmaxintc.h>
#include <cslr_dmaxpdsp.h>
#include <da830lib.h>
#include <dmax_params.h>
#include <mcasp.pdsp>

#include <math.h>

#include <dpll.h>

// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */

#include <logp.h>
#include <dp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
// For release, set mask to 1 to make it easier to catch any errors.
#define CURRENT_TRACE_MASK  1   // terse only

#define TRACE_MASK_TERSE    1   // only flag errors
#define TRACE_MASK_GENERAL  2   // log a half dozen lines per loop
#define TRACE_MASK_VERBOSE  4   // trace full operation

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
#else
 #define TRACE_VERBOSE(a)
#endif

// This works to set a breakpoint
#define SW_BREAKPOINT       asm( " SWBP 0" );
/* Software Breakpoint to Code Composer */
// SW_BREAKPOINT;



// .............................................................................
// Resource Access Control
//
// There are two resources which must be protected in this layer:
//        (i)  driver global (dapDmaxDrv)
//        (ii) device specific (pDmaExt)
//
// For (i) there are at least two choices for appropriate access control.
//        (a) HWI disable/enable macros
//            This is effective but inefficient since it blocks all
//            processing regardless of its resource usage.
//
//        (b) Raise the TSK priority > max priority of all tasks using DAP.
//            This assumes that no tasks using DAP run at TSK_MAXPRI.
//
// (a) is chosen since it provides better localization of the access control
// method. i.e. we only need to consider code in this layer. Also, the primary
// member of (i) being written to is a QUE_OBJ. The QUE component provides
// atomic APIs (put and get) which effectively use HWI_disable/enable blocks.
// Therefore the use of (a) provides consistency and allows the use of the QUE
// APIs without additional resource access control where appropriate. Moreover
// the possible inefficiency of (a) is mitigated by the fact that the uses of
// (i) other than that of the QUE object are very limited. These being one time
// initialization done before switching is enabled (DAP_DMAX_init) and
// the allocation of the shared interrupt (DAP_DMAX_alloc). The allocation
// will be performed only once by the driver. Hence it has negligible impact
// on code efficiency. And the check (whether the interrupt is yet allocate)
// is very quick and only runs once per device; when it is opened.
//
// For (ii) we need to protect the context from ISR switching but not from
// task switching. Ideally we could just disable interrupt servicing for a
// particular device. However all DAP_DMAX devices share a common interrupt
// and in order to protect a specific device's context the interrupt processing
// for all devices must be disabled. This is unfortunate considering this was
// not necessary on Mahlar. The EDMA had the ability to disable interrupt
// generation for specfific completion codes and generate an interrupt pulse
// when reenabling if the completion code had been latched while disabled.
// This functionality is not provided with the current dMAX implementation.
// Consequently all routines must exit with the interrupt enabled.

// The device context will only be accessed in the ISR once the device is
// running. Functions which run before the device is enabled
// (e.g. DAP_DMAX_config) need not worry about disabling interrupts.
//
// Note that resources defined elsewhere are assumed to be properly protected
// via the APIs used for accessing said resources (e.g. DMAX_FTABLE_xx).
//
// .............................................................................
// global context for DMA layer
static DAP_DMAX_DriverObject dapDmaxDrv = {0};
// Int8       intTab[3]; 
// Uint8      lockCnt;
// QUE_Handle hParamQue;

// symbols for DAP_DMAX_DriverObject.intab usage
// Note that on DA8xx, unlike DA7xx, the DMX mapping is not used since the INTC
// does this automagically.
#define _INTTAB_API        0   // GEM event number (see table 6-8 of sprs483.pdf)
#define _INTTAB_CPU        1   // GEM interrupt number (one of 12 configurable interrupts)

// Local ECAP defines since not using a CSL
#define _ECAP0_BASE             0x01F06000
#define _ECAP1_BASE             0x01F07000
#define _ECAP2_BASE             0x01F08000
#define _ECAP_TSCTR_OFFSET      0
#define _ECAP_ECCTL1_OFFSET     20
#define _ECAP_ECCTL2_OFFSET     21
#define _ECAP_ECEINT_OFFSET     22
#define _ECAP_ECCLR_OFFSET      24

// .............................................................................
// Function table

Int                   DAP_DMAX_alloc             (DEV_Handle);
Int                   DAP_DMAX_config            (DEV_Handle, DEV_Frame *);
Int                   DAP_DMAX_ctrl              (DEV_Handle device, Uns code, Arg arg);
Int                   DAP_DMAX_disable           (DEV_Handle);
Int                   DAP_DMAX_enable            (DEV_Handle);
Int                   DAP_DMAX_free              (DEV_Handle);
Void                  DAP_DMAX_init              (Void);
Void                  DAP_DMAX_isr               (Void);
Int                   DAP_DMAX_lock              (DEV_Handle);
Int                   DAP_DMAX_open              (DEV_Handle);
Int                   DAP_DMAX_reclaim           (DEV_Handle);
Int                   DAP_DMAX_shutdown          (DEV_Handle);
Int                   DAP_DMAX_unlock            (DEV_Handle);
DAP_DMAX_ParamHandle  DAP_DMAX_allocParam        (DEV_Handle);
Int                   DAP_DMAX_freeParam         (DEV_Handle,DAP_DMAX_ParamHandle);
Int                   DAP_DMAX_initErrorParam    (DEV_Handle,DAP_DMAX_ParamHandle);
Int                   DAP_DMAX_initStats         (DEV_Handle device, float seed);
Int                   DAP_DMAX_numRemain         (DEV_Handle device, Int * pNumRem);

DAP_DMAX_Fxns DAP_DMAX_FXNS =
{
    DAP_DMAX_alloc,
    DAP_DMAX_config,
    DAP_DMAX_ctrl,
    DAP_DMAX_disable,
    DAP_DMAX_enable,
    DAP_DMAX_free,
    DAP_DMAX_init,
    DAP_DMAX_isr,
    DAP_DMAX_lock,
    DAP_DMAX_open,
    DAP_DMAX_reclaim,
    DAP_DMAX_shutdown,
    DAP_DMAX_unlock,
    DAP_DMAX_allocParam,
    DAP_DMAX_freeParam,
    DAP_DMAX_initErrorParam,
    DAP_DMAX_initStats,
    DAP_DMAX_numRemain
};

int dapDmaxDataBase [] =
{
    DMAX_DRAM0_BASEADDR,
    DMAX_DRAM1_BASEADDR
};

// TODO: should this be done in the McASP section?
//       i.e. how to make this general if needed
static const char mcaspEvents[6] =
{
    McASP0_TX_EVENT,
    McASP0_RX_EVENT,
    McASP1_TX_EVENT,
    McASP1_RX_EVENT,
    McASP2_TX_EVENT,
    McASP2_RX_EVENT
};

// overwrite legacy version of this macro defined in ../dap_dmax.h
#undef DMAX_FTABLE_allocateTCC
#define DMAX_FTABLE_allocateTCC(_a,_b,_c)         pDmax->fxns->allocTCC(_a,_b,_c)

#define INV_TCC ((char) -1)
#define INV_INT (-1)


// -----------------------------------------------------------------------------
// This function is called before main and hence no devices yet running.
// Therefore we need not worry about context switching.

Void DAP_DMAX_init (Void)
{
    // allocate global parameter que. If it fails we we cause an
    // exception and hence later code can assume it is valid without checking.
    dapDmaxDrv.hParamQue = QUE_create (NULL);
    if (!dapDmaxDrv.hParamQue) {
        SYS_error ("DAP DMAX", SYS_EALLOC);
        SW_BREAKPOINT;
        return;
    }
    dapDmaxDrv.intTab[_INTTAB_API] = INV_INT;
    dapDmaxDrv.lockCnt = 0;

} //DAP_DMAX_init

// -----------------------------------------------------------------------------
// This function is called when the device is not running and so need not
// protect the device context.

Int DAP_DMAX_alloc (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
    dMAX_Handle pDmax = pDmaExt->pDmax;
    const DAP_Params *pParams = pDevExt->pParams;
    Int   oldMask, result;
    Int32 intApi, intCpu;


    // one time allocation of DMA interrupt
    // protect dapDmaxDrv since this thread could be interrupted by another task
    // running DAP (e.g. in the case of time-slicing)
    oldMask = HWI_disable ();
    intApi = dapDmaxDrv.intTab[_INTTAB_API];
    if (intApi == INV_INT) {
        intApi = DMAX_FTABLE_allocateInt (pDmax,dMAX_INTANY);
        if (intApi == INV_INT) {
            HWI_restore (oldMask);
            SW_BREAKPOINT;
            return SYS_EALLOC;
        }

        intCpu = DMAX_FTABLE_mapInt (pDmax, intApi);
        if (intCpu == INV_INT) {
            HWI_restore (oldMask);
            SW_BREAKPOINT;
            return SYS_EALLOC;
        }

        dapDmaxDrv.intTab[_INTTAB_API] = intApi;
        dapDmaxDrv.intTab[_INTTAB_CPU] = intCpu;

        HWI_eventMap     (intCpu, intApi);
        HWI_dispatchPlug (intCpu, (Fxn)(pDevExt->pFxns->pDmaFxns->isr),-1,NULL);
        C64_enableIER    ((1<<intCpu));
    }
    HWI_restore (oldMask);

    pDmaExt->event = mcaspEvents[1-device->mode + 2*pDevExt->pParams->sio.moduleNum];

    // save a local copy of const so that it can change
    pDmaExt->wordSize = pParams->sio.wordSize;

    // allocate primary TCC
    pDmaExt->tcc = DMAX_FTABLE_allocateTCC (pDmax, dMAX_TCCANY, intApi);
    if (pDmaExt->tcc == INV_TCC)
    {
    	SW_BREAKPOINT;
        return SYS_EALLOC;
    }

    // allocate and configure nominal error xfer
    pDmaExt->pDmaxErrorParam1 = DAP_DMAX_FTABLE_allocParam (device);
    if (!pDmaExt->pDmaxErrorParam1)
    {
    	SW_BREAKPOINT;
        return SYS_EALLOC;
    }
    TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_alloc.%d (0x%x) calling DAP_DMAX_FTABLE_initErrorParam 1", __LINE__, device));
    result = DAP_DMAX_FTABLE_initErrorParam (device,pDmaExt->pDmaxErrorParam1);
    if (result)
    {
    	SW_BREAKPOINT;
        return result;
    }
    pDmaExt->linkToggleOffset = pDmaExt->pDmaxErrorParam1->offset;

    return SYS_OK;
} //DAP_DMAX_alloc

// -----------------------------------------------------------------------------

Int DAP_DMAX_disable (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
    dMAX_Handle pDmax = pDmaExt->pDmax;

    TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_disable(0x%x).", device));

    // disable DMA channel
    DMAX_FTABLE_disableEvent (pDmax,pDmaExt->event);
    DMAX_FTABLE_clearEventFlag (pDmax,pDmaExt->event);

    return SYS_OK;
} //DAP_DMAX_disable

// -----------------------------------------------------------------------------

Int DAP_DMAX_enable (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
    dMAX_Handle pDmax = pDmaExt->pDmax;
    int wordSize = pDmaExt->wordSize;
    volatile int * pEntry;
    short ptype;
    int i;

    TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_enable(0x%x).", device));

    // configure event entry
    //    - set ptype
    // for 16 bit input only stereo is supported (ptype = 16)
    // 32bit inputs use common routine (ptype = 17)
    if (device->mode == DEV_INPUT) {
        if (wordSize == 2){
            if (pDevExt->numSers == 1)
                ptype = PT_McASP0;
            else if (pDevExt->numSers == 4)
                ptype = PT_McASP6;
        }
        else if (wordSize == 4)
            ptype = PT_McASP1; //17
   }
    else {
        if ((wordSize == 4) && (pDevExt->numSers > 16))
            return SYS_EINVAL;

        if ((wordSize == 3) && (pDevExt->numSers == 4))
            ptype = PT_McASP2; //18
        else if ((wordSize == 3) && (pDevExt->numSers == 1))
            ptype = PT_McASP3; //19
        else if ((wordSize == 4) && (pDevExt->numSers == 1))
            ptype = PT_McASP4; //20
        else
            ptype = PT_McASP5; //21
    }

    // clear any previous TCC and flag
    if (pDmaExt->tcc != INV_TCC)
        DMAX_FTABLE_clrTCC (pDmax, pDmaExt->tcc);
    for (i=0; i < pDmaExt->maxExtraTcc; i++) {
        if (pDmaExt->tccExtra[i] != INV_TCC)
            DMAX_FTABLE_clrTCC (pDmax, pDmaExt->tccExtra[i]);
    }
    DMAX_FTABLE_clearEventFlag (pDmax,pDmaExt->event);

    // set ptype, priority (high) then enable event
    pEntry = (volatile int *) (dapDmaxDataBase[pDmaExt->maxID] + pDmaExt->event*4);
    *pEntry &= ~(mEntry_pType << fEntry_pType);
    *pEntry |= (ptype << fEntry_pType);

    // finally enable event processing
    DMAX_FTABLE_eventPriorityHigh (pDmax, pDmaExt->event);
    DMAX_FTABLE_enableEvent (pDmax,pDmaExt->event);

    return SYS_OK;
} //DAP_DMAX_enable

// -----------------------------------------------------------------------------
// Assumes shutdown is called elsewhere to free parameter entries

Int DAP_DMAX_free (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
    dMAX_Handle pDmax = pDmaExt->pDmax;
    Int i, result;


    // disable -- this disables the DMA servicing and so interrupts will not occur
    // which means we need not worry about context preservation after this point
    result = DAP_DMA_FTABLE_disable (device);
    if (result)
    {
    	SW_BREAKPOINT;
        return result;
    }

    // free TCCs
    if (pDmaExt->tcc != INV_TCC) {
        DMAX_FTABLE_freeTCC (pDmax,pDmaExt->tcc);
        pDmaExt->tcc = INV_TCC;
    }
    for (i=0; i < pDmaExt->maxExtraTcc; i++) {
        if (pDmaExt->tccExtra[i] != INV_TCC) {
            DMAX_FTABLE_freeTCC (pDmax,pDmaExt->tccExtra[i]);
            pDmaExt->tccExtra[i] = INV_TCC;
        }
    }

    // free error params
    DAP_DMAX_FTABLE_freeParam (device, pDmaExt->pDmaxErrorParam1);
    DAP_DMAX_FTABLE_freeParam (device, pDmaExt->pDmaxErrorParam2);
    pDmaExt->pDmaxErrorParam1 = NULL;
    pDmaExt->pDmaxErrorParam2 = NULL;

    return SYS_OK;
} //DAP_DMAX_free

// -----------------------------------------------------------------------------

Int DAP_DMAX_lock (DEV_Handle device)
{
    Int oldMask;

    oldMask = HWI_disable ();
    C64_disableIER ((1 << dapDmaxDrv.intTab[_INTTAB_CPU]));
    dapDmaxDrv.lockCnt++;
    HWI_restore (oldMask);

    return SYS_OK;
} //DAP_DMAX_lock

// -----------------------------------------------------------------------------
// This function cannot be called within SWI/HWI/TSK disable context since it
// calls MEM_alloc.

Int DAP_DMAX_open (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt;
    int i;

    pDmaExt = (DAP_DMAX_DeviceExtension *) MEM_alloc (device->segid, (sizeof(DAP_DMAX_DeviceExtension)+3)/4*4, 4);
    if (pDmaExt == MEM_ILLEGAL)
    {
        printf("%s.%d:  MEM_alloc failed.\n", __FUNCTION__, __LINE__);
        SW_BREAKPOINT;
        return SYS_EALLOC;
    }
    pDevExt->pDmaExt = (Void *) pDmaExt;

    pDmaExt->pDmax = dMAX_HANDLE;
    pDmaExt->maxID = dMAX_MAX0_ID;
    pDmaExt->tcc = INV_TCC;
    pDmaExt->maxExtraTcc = MAX_EXTRA_TCC;
    for (i=0; i < pDmaExt->maxExtraTcc; i++)
        pDmaExt->tccExtra[i] = INV_TCC;
    pDmaExt->pDmaxErrorParam1 = NULL;
    pDmaExt->pDmaxErrorParam2 = NULL;
    pDmaExt->linkToggleOffset = 0;
    pDmaExt->pStats = NULL;

    return SYS_OK;
} //DAP_DMAX_open

// -----------------------------------------------------------------------------
// TODO: call lock explicitly or assume it is locked on entry?

// TODO: investigate boundary values
#define NOMINAL_XFER_SIZE 40 /* in synchronization events */

Int DAP_DMAX_config (
    DEV_Handle device,
    DEV_Frame *pFrame)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
    PAF_SIO_Stats *pStats = pDmaExt->pStats;
    volatile DMAX_MCASP_Param *pParam;
    int translate, cnt, tcc, i;
    DAP_DMAX_Param *pDmaxParam;
    dMAX_Handle pDmax = pDmaExt->pDmax;

    TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_config.%d (0x%x) enter", __LINE__, device));

    // if not running then free extra TCCs
    if ((pDevExt->runState == 0) || (pDevExt->errorState == PAF_SIO_ERROR_IDLE_STAGE1)) {
        int i;
        for (i=0; i < pDmaExt->maxExtraTcc; i++) {
            if (pDmaExt->tccExtra[i] != INV_TCC) {
                DMAX_FTABLE_freeTCC (pDmax,pDmaExt->tccExtra[i]);
                pDmaExt->tccExtra[i] = INV_TCC;
            }
        }
    }

    // fetch parameter table memory
    pDmaxParam = DAP_DMAX_FTABLE_allocParam (device);
    if (!pDmaxParam)
        return SYS_EALLOC;
    pFrame->misc = (Arg) pDmaxParam;
    pParam = (volatile DMAX_MCASP_Param *) (dapDmaxDataBase[pDmaExt->maxID] + (int)pDmaxParam->offset);
    translate = (int) pFrame->addr;
    cnt =  pFrame->size/pDmaExt->wordSize/pDevExt->numSers;

    // need to qualify for input since output starts with small
    // xfer size but is intended to enter error state immediately
    // so it doesn't need extra TCCs
    tcc = INV_TCC;
    if ((device->mode == DEV_INPUT) && (cnt <= NOMINAL_XFER_SIZE)) {
        for (i=0; i < pDmaExt->maxExtraTcc; i++) {
            if (pDmaExt->tccExtra[i] == INV_TCC) {
                tcc = pDmaExt->tccExtra[i] = DMAX_FTABLE_allocateTCC (pDmax, dMAX_TCCANY, dapDmaxDrv.intTab[_INTTAB_API]);
                if (tcc == INV_TCC)
                    return SYS_EALLOC;
            }
        }
    }
    else
        tcc = pDmaExt->tcc;
    if (tcc == INV_TCC)
        return SYS_EALLOC;

    TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_config.%d (0x%x) setting ->stat to zero", __LINE__, device));
    pParam->stat = 0;
    pParam->cnt  = cnt;
    pParam->xfer = translate;
    pParam->tcc  = tcc;

    // slotParity field is used to flag odd / even event - used at DMAX to identify the polarity of the occurred event
    // & reorder data accordingly in multichannel audio Rx processing. This field is set to 0 at every  block start to
    // signal even event as the first event. Later this field is toggled on every Rx event in DMAX code.
    // RSTAT <.RTDMSLOT>is no more used as this will not be the correct indication of polarity
    // in FIFO enabled case. Data depth in FIFO can be more than 1 in which RSTAT will signal the stats of the last
    // data in the FIFO and not the first one being read.
    pParam->slotParity = 0;

    if ((pStats && pStats->state == PAF_SIO_STATS_ENABLED) &&
        ((pDevExt->runState == 0) || (pDevExt->errorState == PAF_SIO_ERROR_IDLE_STAGE1))) 
    {   // status used to measure sample rate and compute ratio between input and 
        //  output rate for ASRC module ARC.
        volatile unsigned short * baseEcap;
        // ECAP0 is used for input and ECAP1 for output
        // CFGCHIP1 controls input mux for ECAP sources.(See TRM, table 10-46 in sprug84b)
        if (device->mode == DEV_INPUT) {
            baseEcap = (volatile unsigned short *) _ECAP0_BASE;
            CFGCHIP1 &= ~0x3e0000;
            CFGCHIP1 |= ((2+2*pDevExt->pParams->sio.moduleNum) << 17);
        }
        else {
            baseEcap = (volatile unsigned short *) _ECAP1_BASE;
            CFGCHIP1 &= ~0x7c00000;
            CFGCHIP1 |= ((1+2*pDevExt->pParams->sio.moduleNum) << 22);
        }

        // Disable all interrupts (not using)
        baseEcap[_ECAP_ECEINT_OFFSET] = 0;

        // Stop ECAP counter
        baseEcap[_ECAP_ECCTL2_OFFSET] = 0;

        // Set start of time to 0
        pStats->lastClkCnt = 0;
        baseEcap[_ECAP_TSCTR_OFFSET] = 0;
        baseEcap[_ECAP_TSCTR_OFFSET + 1] = 0;

        // Counter unaffected by emulation suspend (run-free)
        // Event prescale by 1 (i.e. capture every event)
        // Do not reset counter on Capture Event 1 (absolute time stamp)
        // Capture Event 1 triggered on a rising edge (RE)
        // Enable CAP1-4 register loads at capture event time
        baseEcap[_ECAP_ECCTL1_OFFSET] = (2 << 14) | ( 1 << 8);

        // ECAP set to continuous capture mode
        // Disable sync in and out
        // Start counter
        // Stop wrap = 0 (i.e. only use first capture register)
        // Continuous mode
        baseEcap[_ECAP_ECCTL2_OFFSET] = (2 << 6) | (1 << 4);
    }

    //link to error xfer
    pParam->link = pDmaExt->linkToggleOffset;

    // flush output data cache to SDRAM if needed
#ifdef DAP_CACHE_SUPPORT
    if (pFrame->addr) {
        if (device->mode == DEV_INPUT)
            Cache_inv (pFrame->addr, pFrame->size, Cache_Type_ALL, TRUE);
        else
            Cache_wbInv (pFrame->addr, pFrame->size, Cache_Type_ALL, TRUE);
    }
#endif

    // if not running (could check?) and so configure the event entry directly
    // else attach (link) xfer to end of chain
    if (pDevExt->runState == 0) {
        volatile int * pEntry = (volatile int *) (dapDmaxDataBase[pDmaExt->maxID] + pDmaExt->event*4);
        int result;
        *pEntry &= ~(mEntry_param << fEntry_param);
        *pEntry &= ~(mEntry_burst << fEntry_burst);
        *pEntry &= ~(mEntry_wrdSz << fEntry_wrdSz);
        *pEntry |=  (pDmaxParam->offset << fEntry_param);
        *pEntry |=  ((pDmaExt->wordSize*pDevExt->numSers) << fEntry_burst);
        *pEntry |=  (pDmaExt->wordSize << fEntry_wrdSz);
        TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_config.%d (0x%x) calling DAP_DMAX_FTABLE_initErrorParam 1", __LINE__, device));
        result = DAP_DMAX_FTABLE_initErrorParam (device,pDmaExt->pDmaxErrorParam1);
        if (result)
            return result;
        if (pDmaExt->pDmaxErrorParam2) {
            TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_config.%d (0x%x) calling DAP_DMAX_FTABLE_initErrorParam 2", __LINE__, device));
            result = DAP_DMAX_FTABLE_initErrorParam (device,pDmaExt->pDmaxErrorParam2);
            if (result)
                return result;
        }
    }
    else {
        int offset = pDmaExt->pDmaxLastParam->offset;
        int result;
        volatile short *pLink;
        if (pDevExt->errorState == PAF_SIO_ERROR_IDLE_STAGE1) {
            offset = pDmaExt->linkToggleOffset;
            pDevExt->errorState = PAF_SIO_ERROR_NONE;
            // toggle error link
            if (!pDmaExt->pDmaxErrorParam2) {
                pDmaExt->pDmaxErrorParam2 = DAP_DMAX_FTABLE_allocParam (device);
                if (!pDmaExt->pDmaxErrorParam2)
                    return SYS_EALLOC;
            }
            if (pDmaExt->linkToggleOffset == pDmaExt->pDmaxErrorParam1->offset) {
                TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_config.%d (0x%x) calling DAP_DMAX_FTABLE_initErrorParam 1", __LINE__, device));
                result = DAP_DMAX_FTABLE_initErrorParam (device,pDmaExt->pDmaxErrorParam2);
                if (result)
                    return result;
                pDmaExt->linkToggleOffset = pDmaExt->pDmaxErrorParam2->offset;
            }
            else {
                TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_config.%d (0x%x) calling DAP_DMAX_FTABLE_initErrorParam 1", __LINE__, device));
                result = DAP_DMAX_FTABLE_initErrorParam (device,pDmaExt->pDmaxErrorParam1);
                if (result)
                    return result;
                pDmaExt->linkToggleOffset = pDmaExt->pDmaxErrorParam1->offset;
            }
        }
        // write directly to link address since PDSP is using global address to arbitrate
        pLink = (volatile short *) ((int)pDmax->max[pDmaExt->maxID]->dbase + (int)(offset+2));
        *pLink = pDmaxParam->offset;
    }
    pDmaExt->pDmaxLastParam = pDmaxParam;

    return SYS_OK;
} //DAP_DMAX_config

// -----------------------------------------------------------------------------
#ifdef Z_TOP
 extern SEM_Handle g_oTimeSem;  // from oTime.c
 extern int g_oTimeDapActive;
#endif

Void DAP_DMAX_isr (Void)
{
    DEV_Handle                 device;
    DAP_DeviceExtension       *pDevExt;
    DAP_DMAX_DeviceExtension  *pDmaExt;
    dMAX_Handle                pDmax;
    DEV_Frame                 *pFrame;
    int                        i,j,tcc;

    TRACE_VERBOSE((&TR_MOD, "DAP ISR.%d enter", __LINE__));

    // loop over devices determining the caller, service all
    while (1) {

        //TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_isr.%d, devices loop start, numDevices = %d", __LINE__, dapDrv.numDevices));

        for (i=0; i < dapDrv.numDevices; i++) {
            device = dapDrv.device[i];
            pDevExt = (DAP_DeviceExtension *)(device->object);
            pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
            pDmax = pDmaExt->pDmax;

            tcc = INV_TCC;
            if ((pDmaExt->tcc != INV_TCC) && DMAX_FTABLE_checkTCC(pDmax,pDmaExt->tcc)) {
                TRACE_VERBOSE((&TR_MOD, "DAP ISR.%d, update TCC 0x%x", __LINE__, device));
                tcc = pDmaExt->tcc;
                break;
            }
            for (j=0; j < pDmaExt->maxExtraTcc; j++) {
                if ((pDmaExt->tccExtra[j] != INV_TCC) && DMAX_FTABLE_checkTCC(pDmax,pDmaExt->tccExtra[j])) {
                    tcc = pDmaExt->tccExtra[j];
                    // free this extra tcc since no longer needed
                    TRACE_VERBOSE((&TR_MOD, "DAP ISR.%d, Free TCC 0x%x", __LINE__, device));
                    DMAX_FTABLE_freeTCC (pDmax,tcc);
                    pDmaExt->tccExtra[j] = INV_TCC;
                    break;
                }
            }
            if (tcc != INV_TCC) {
                TRACE_VERBOSE((&TR_MOD, "DAP ISR.%d, tcc != INV_TCC 0x%x", __LINE__, device));
                break;
            }
        }

        TRACE_VERBOSE((&TR_MOD, "DAP ISR.%d, 0x%x", __LINE__, device));
        // if no more serviceable devices then exit
        if (tcc == INV_TCC)
            break;

        if ((pDevExt->runState == 1) && !pDevExt->errorState) {
            volatile int * pEntry = (volatile int *) (dapDmaxDataBase[pDmaExt->maxID] + pDmaExt->event*4);
            int oActive = (*pEntry & (mEntry_param << fEntry_param)) >> fEntry_param;
            volatile DMAX_MCASP_Param *pActive = (volatile DMAX_MCASP_Param *) (dapDmaxDataBase[pDmaExt->maxID] + oActive);
            // here if kickstarted

            // if here then an interrupt occured due to errorEdma or valid
            //   transfer, we assume the xfer is long enough so it will not complete
            //   before we are finished here.

            // if last transfer was valid then complete it
            if (!QUE_empty (&pDevExt->xferQue)) 
            {
                // pull frame from holding queue
                pFrame = (DEV_Frame *) QUE_dequeue (&pDevExt->xferQue);

                TRACE_VERBOSE((&TR_MOD, "DAP ISR.%d, 0x%x, dequeued 0x%x", __LINE__, device, pFrame));
                if (pFrame->misc) 
                {
                    PAF_SIO_Stats *pStats = pDmaExt->pStats;

                    // copy capture value from dMAX memory if needed
                    // dMAX blockEnd routine fills in xfer (int offset 1) with
                    // buffer time stamp
                    if (pStats && pStats->state == PAF_SIO_STATS_ENABLED)
                    {
                        volatile unsigned int * baseEcap;
                        pFrame->cmd = ((volatile unsigned int *) (dapDmaxDataBase[pDmaExt->maxID] + ((DAP_DMAX_ParamHandle) pFrame->misc)->offset))[1];
                        TRACE_GEN((&TR_MOD, "DAP ISR, 0x%x, maxID, %d,  offset: %d", 
                                   device, pDmaExt->maxID, ((DAP_DMAX_ParamHandle) pFrame->misc)->offset));

                        if (device->mode == DEV_OUTPUT) {
                            baseEcap = (volatile unsigned int *) _ECAP1_BASE;
                            TRACE_GEN((&TR_MOD, "DAP ISR, 0x%x, output. captured, %u, now %u", device, pFrame->cmd, *baseEcap));
                            // dp(&TR_MOD, "DAP ISR, 0x%x, output. captured, %u at 0x%x, now %u\n", device, pFrame->cmd, &(pFrame->cmd), *baseEcap);
                            (void)baseEcap;  // clear warning.
                        }
                        else
                        {  // ecap0 is used for all inputs
                            baseEcap = (volatile unsigned int *) _ECAP0_BASE;
                            TRACE_GEN((&TR_MOD, "DAP ISR, 0x%x, input. captured, %u, now %u", device, pFrame->cmd, *baseEcap));
                            // dp(&TR_MOD, "DAP ISR, 0x%x, input. captured, %u at 0x%x, now %u\n", device, pFrame->cmd, &(pFrame->cmd), *baseEcap);
                            (void)baseEcap;  // clear warning.
                        }
                    }
                    else
                    {
                         if (device->mode == DEV_OUTPUT) {
                             TRACE_GEN((&TR_MOD, "DAP ISR, 0x%x, output, no stats", device));
                         }
                         else
                         {
                            switch (pDevExt->pParams->sio.moduleNum)
                            {
                                case 0:
                                    TRACE_GEN((&TR_MOD, "DAP ISR, 0x%x, input 0, no stats", device));
                                    break;
                                case 1:
                                    TRACE_GEN((&TR_MOD, "DAP ISR, 0x%x, input 1, no stats", device));
                                    break;
                            }
                        }
                    }

                    // return parameter to queue
                    TRACE_GEN((&TR_MOD, "DAP ISR, 0x%x, calling DAP_DMAX_FTABLE_freeParam() with 0x%x", device, pFrame->misc));
                    DAP_DMAX_FTABLE_freeParam (device, (DAP_DMAX_ParamHandle) pFrame->misc);
                }

                TRACE_VERBOSE((&TR_MOD, "DAP ISR.%d, 0x%x: Signal.", __LINE__, device));
                // place frame onto user queue and signal user thread
                // use non-atomic version since we are at ISR level
                QUE_enqueue (device->fromdevice, (Ptr) pFrame);

                // decrement count
                pDevExt->numQueued -= 1;

                // signal user thread
                SEM_ipost (pDevExt->sync);

                #ifdef Z_TOP
                // signal oTime module
                if ((device->mode == DEV_OUTPUT) && g_oTimeSem)
                {
                    // TRACE_VERBOSE((&TR_MOD, "DAP_isr posting g_oTimeSem"));
                    g_oTimeDapActive = 1;
                    SEM_ipost (g_oTimeSem);
                }
                #endif
            }
            else
            {
                TRACE_VERBOSE((&TR_MOD, "DAP ISR.%d, 0x%x, last transfer not valid", __LINE__, device));
            }
            // signal error condition if currently transferring buffer is valid
            if (pActive->stat)
            {
                TRACE_TERSE((&TR_MOD, "DAP ISR, 0x%x, pActive->stat is 0x%x, setting errorState = PAF_SIO_ERROR_ERRBUF_XFER (0x%x)", 
                                               device, pActive->stat, PAF_SIO_ERROR_ERRBUF_XFER));
                TRACE_TERSE((&TR_MOD, "DAP ISR, pActive: 0x%x, &pActive->stat 0x%x", 
                                               pActive, &pActive->stat));

                pDevExt->errorState = PAF_SIO_ERROR_ERRBUF_XFER;
            }

        } // runState

        // clear TCC (TODO: what if no pDmax);
        if (pDmax)
        {
            TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_isr.%d, no pDmax, so clearing TCC", __LINE__));
            DMAX_FTABLE_clrTCC (pDmax, tcc);
        }

        //TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_isr.%d, devices loop end", __LINE__));

    } //while (1)
    TRACE_VERBOSE((&TR_MOD, "DAP ISR.%d, 0x%x:  return.", __LINE__, device));
    return;

} // DAP_DMAX_isr

// -----------------------------------------------------------------------------

Int DAP_DMAX_ctrl (DEV_Handle device, Uns code, Arg arg)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
    Int result = SYS_OK;

    switch (code) {

// .............................................................................


        // normalize by dividing by numslots so it is invariant wrt to TDM mode
        // return error if in errorstate so that drift is calculated using last
        // known functioning values.
        case PAF_SIO_CONTROL_GET_NUM_EVENTS:
            if (pDevExt->errorState)
            {
                // TRACE_VERBOSE((&TR_MOD, "PAF_SIO_CONTROL_GET_NUM_EVENTS:  errorState 0x%x.  return 0x%x.", pDevExt->errorState, SYS_EBADIO));
                return SYS_EBADIO;
            }

            result = DAP_DMAX_FTABLE_numRemain (device, (Int *) arg);
            *((Int *)arg) /= pDevExt->numSlots;
            // TRACE_VERBOSE((&TR_MOD, "PAF_SIO_CONTROL_GET_NUM_EVENTS.%d: result 0x%x.  computed %d.", __LINE__, result, *((Int *)arg)));
            break;

        case PAF_SIO_CONTROL_GET_NUM_REMAINING:
            result = DAP_DMAX_FTABLE_numRemain (device, (Int *) arg);
            break;

        case PAF_SIO_CONTROL_GET_WORDSIZE:
            if (arg == 0)
                return SYS_EINVAL;
            *((int *) arg) = pDmaExt->wordSize;
            break;

        case PAF_SIO_CONTROL_SET_WORDSIZE:
            // currently only supported for input
            if (device->mode != DEV_INPUT)
                return SYS_EINVAL;

            // can't be running
            if (pDevExt->runState)
                return SYS_EBUSY;

            // must be 2 or 4 bytes
            if ((arg != 2) && (arg != 4))
                return SYS_EINVAL;

            // return success for unconfigured devices?
            if (pDevExt->pParams == NULL)
                return SYS_OK;

            pDmaExt->wordSize = arg;
            break;

// .............................................................................

        case PAF_SIO_CONTROL_ENABLE_STATS:
            result = DAP_DMAX_FTABLE_initStats (device, (float) arg);
            break;

        case PAF_SIO_CONTROL_DISABLE_STATS:
            if (pDmaExt->pStats)
                pDmaExt->pStats->state = PAF_SIO_STATS_DISABLED;
            break;

        case PAF_SIO_CONTROL_GET_STATS:
            // TRACE_DAP()(&trace, "dap_dmax (0x%x): PAF_SIO_CONTROL_GET_STATS returns 0x%x", device, pDmaExt->pStats));
            // pass pointer to local stats structure
            *((Ptr *) arg) = pDmaExt->pStats;
            break;

// .............................................................................

        default:
            // return error for all unprocessed codes
            result = SYS_EINVAL;
            break;
    }
    return result;
} //DAP_DMAX_ctrl

// -----------------------------------------------------------------------------
// Portions of this function cannot be called within SWI/HWI/TSK disable context
// due to the direct or indirect (QUE_create) calls to MEM_alloc.

DAP_DMAX_ParamHandle DAP_DMAX_allocParam (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
    dMAX_Handle pDmax = pDmaExt->pDmax;
    DAP_DMAX_ParamHandle pDmaxParam = NULL;
    Int oldMask;

    // pull from local que unless its empty. We lock context since there could be
    // a race condition between checking the empty condition and actually pulling
    // an item off the queue. And due to the lock we can use the non-atomic
    // dequeue.
    oldMask = HWI_disable ();
    {
        if ((*(int*)dapDmaxDrv.hParamQue) == 0)
        {
            TRACE_TERSE((&trace, " << DAP_DMAX_allocParam, queue is zero!"));
            SW_BREAKPOINT;
        }

        if (!QUE_empty (dapDmaxDrv.hParamQue))
        {
            // QUE_dequeue is inline, protected by HWI_disable.
            pDmaxParam = (DAP_DMAX_ParamHandle) QUE_dequeue (dapDmaxDrv.hParamQue);
            if (pDmaxParam)
            {
                TRACE_VERBOSE((&trace, " << DAP_DMAX_allocParam, queue not empty, got 0x%x, offset %d", pDmaxParam, pDmaxParam->offset));
            }
            else
            {
                TRACE_VERBOSE((&trace, " << DAP_DMAX_allocParam, queue not empty, got %d. (offset)", pDmaxParam));
            }
        }
        else
        {
            TRACE_VERBOSE((&trace, " << DAP_DMAX_allocParam, queue empty. (offset)"));
        }
    }
    HWI_restore (oldMask);

    if (!pDmaxParam) {
        pDmaxParam = MEM_alloc (device->segid, (sizeof(DAP_DMAX_Param)+3)/4*4, 4);
        TRACE_GEN((&trace, "DAP_DMAX_allocParam, MEM_alloc'ed pDmaxParam 0x%x", pDmaxParam));
        if (!pDmaxParam)
        {
            printf("%s.%d:  MEM_alloc failed.\n", __FUNCTION__, __LINE__);
            SW_BREAKPOINT;
            return NULL;
        }

        pDmaxParam->offset = (XDAS_UInt16) ((int) DMAX_FTABLE_allocateTable (pDmax, pDmaExt->maxID, sizeof(DMAX_MCASP_Param)) & mEntry_param);
        TRACE_GEN((&trace, "DAP_DMAX_allocParam, DMAX_FTABLE_allocateTable'ed offset %d", pDmaxParam->offset));
        if (!pDmaxParam->offset)
        {
            printf("%s.%d:  MEM_alloc failed.\n", __FUNCTION__, __LINE__);
            SW_BREAKPOINT;
            return NULL;
    }
    }

    return pDmaxParam;
} //DAP_DMAX_allocParam

// -----------------------------------------------------------------------------
// by construction numQueued = # of parameter entries used
// and it also is the # of elements on the xferQue
// TODO: call lock directly or assume it is locked on entry?

Int DAP_DMAX_shutdown (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DEV_Frame *pFrame;
    int i;

    if (pDevExt->numQueued) {
        pFrame = QUE_head (&pDevExt->xferQue);
        DAP_DMAX_FTABLE_freeParam (device, (DAP_DMAX_ParamHandle) pFrame->misc);
        for (i=0; i < pDevExt->numQueued-1; i++) {
            pFrame = QUE_next (pFrame);
            DAP_DMAX_FTABLE_freeParam (device, (DAP_DMAX_ParamHandle) pFrame->misc);
        }
    }

    return SYS_OK;
} //DAP_DMAX_shutdown

// -----------------------------------------------------------------------------
Int DAP_DMAX_freeParam (DEV_Handle device, DAP_DMAX_ParamHandle pDmaxParam)
{
    // use atomic API since we share this queue with the isr and other threads
    if (pDmaxParam)
    {
        Int oldMask = HWI_disable ();

        QUE_put (dapDmaxDrv.hParamQue, pDmaxParam);
        TRACE_VERBOSE((&trace, " >> DAP_DMAX_freeParam.%d, put 0x%x, offset %d in queue", __LINE__, pDmaxParam, pDmaxParam->offset));
        if ((*(int*)dapDmaxDrv.hParamQue) == 0)
        {
        	TRACE_TERSE((&trace, "DAP_DMAX_freeParam.%d, queue is zero!", __LINE__));
        	SW_BREAKPOINT;
        }
        HWI_restore (oldMask);
    }
    else
    {
        TRACE_VERBOSE((&trace, "DAP_DMAX_freeParam, No pDmaxParam, called with null"));
    }

    return SYS_OK;
} //DAP_DMAX_freeParam

// -----------------------------------------------------------------------------

Int DAP_DMAX_initErrorParam (DEV_Handle device, DAP_DMAX_ParamHandle pErrorParam)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
    volatile DMAX_MCASP_Param *pError = (volatile DMAX_MCASP_Param *) (dapDmaxDataBase[pDmaExt->maxID] + (int)pErrorParam->offset);

    TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_initErrorParam, 0x%x, setting stat to 0x%x", device, pDevExt->numSlots));
    TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_initErrorParam, pError: 0x%x, pDevExt: 0x%x", pError, pDevExt));

    pError->tcc     = 0; //dont generate an interrupt on completion
    pError->slotParity  = 0; //dont care since tcc=0
    pError->intNum  = 0; //dont care since tcc=0
    pError->stat    = pDevExt->numSlots; //!=0 ==> error state and doubles as cnt reload
    pError->cnt     = pDevExt->numSlots; //# of TDM slots to maintain channel alignment
    pError->xfer    = 0;                 //addr NULL ==> transmit zeros
    pError->link    = pErrorParam->offset; //circular link to itself

    return SYS_OK;
} //DAP_DMAX_initErrorParam

// -----------------------------------------------------------------------------
// This function cannot be called within SWI/HWI/TSK disable context since it
// calls MEM_alloc.
// Assumes pStats is valid

Int DAP_DMAX_initStats (DEV_Handle device, float seed)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
    PAF_SIO_Stats *pStats;

    double fcpu = GBL_getFrequency () * 1000 * 2;
    // the DPLL wants 2x the frequency.

    // allocate structure only once to avoid fragmentation
    if (!pDmaExt->pStats) {
        pDmaExt->pStats = MEM_alloc (device->segid, (sizeof(PAF_SIO_Stats)+3)/4*4, 4);
        if (!pDmaExt->pStats)
        {
            printf("%s.%d:  MEM_alloc failed.\n", __FUNCTION__, __LINE__);
            SW_BREAKPOINT;
            return SYS_EALLOC;
    }
    }
    if ((pDevExt->runState == 0) || (pDevExt->errorState == PAF_SIO_ERROR_IDLE_STAGE1)) {
        pStats = pDmaExt->pStats;
        pStats->clkTotal = 0.0;
        pStats->state = PAF_SIO_STATS_ENABLED;
        pDmaExt->pStats->freqSeed = seed;

        // init pll, assumes freqSeed initialized via SIO_ctrl
        return DPLL_Init (&pStats->dpll, (double) pStats->freqSeed * 2, fcpu);
    }

    TRACE_TERSE((&TR_MOD, "DAP_DMAX_initStats"));
    return SYS_OK;
} //DAP_DMAX_initStats

// -----------------------------------------------------------------------------
Int DAP_DMAX_reclaim (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
    PAF_SIO_Stats *pStats = pDmaExt->pStats;
    DEV_Frame *pFrame = QUE_head (device->fromdevice);

    // update dpll if necessary
    if (pStats && pStats->state == PAF_SIO_STATS_ENABLED) 
    {
        double clocks, delta;
        int cnt =  (2 * pFrame->size) / (pDmaExt->wordSize * pDevExt->numSers * pDevExt->numSlots);

        // dp(NULL, "DAP_DMAX_reclaim(0x%x): cnt: %d. frameSize: %d. wordSize: %d, numSers: %d. numSlots: %d.\n", device, cnt, pFrame->size, pDmaExt->wordSize, pDevExt->numSers,pDevExt->numSlots);
        TRACE_TERSE((&TR_MOD, "DAP_DMAX_reclaim(0x%x) pFrame: 0x%x.  fromDevice: 0x%x:", device, pFrame, device->fromdevice));
        delta = (double)pFrame->cmd - (double)pStats->lastClkCnt;
        if (delta < 0)
            delta += (double) 0xFFFFFFFF;
        pStats->clkTotal += delta;
        pStats->lastClkCnt = (unsigned int) pFrame->cmd;
        // TRACE_VERBOSE((&TR_MOD, "DAP_DMAX_reclaim.%d. Update PLL 0x%x with %d", __LINE__, pStats->dpll, cnt));

#define BITSTIM 32    // mod clock value to maintain accuracy (floating point numbers have greater accuracy for smaller numbers)
        clocks = fmod (pStats->clkTotal, ldexp( 1., BITSTIM));
        DPLL_Update (&pStats->dpll, clocks, cnt);
    }
    return SYS_OK;
} //DAP_DMAX_reclaim

// -----------------------------------------------------------------------------

Int DAP_DMAX_unlock (DEV_Handle device)
{
    Int oldMask = HWI_disable ();
    dapDmaxDrv.lockCnt--;
    if (!dapDmaxDrv.lockCnt)
        C64_enableIER ((1 << dapDmaxDrv.intTab[_INTTAB_CPU]));
    HWI_restore (oldMask);

    return SYS_OK;
} //DAP_DMAX_unlock

// -----------------------------------------------------------------------------

Int DAP_DMAX_numRemain (DEV_Handle device, Int *pNumRem)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    DAP_DMAX_DeviceExtension *pDmaExt = (DAP_DMAX_DeviceExtension *) pDevExt->pDmaExt;
    volatile int * pEntry = (volatile int *) (dapDmaxDataBase[pDmaExt->maxID] + pDmaExt->event*4);
    volatile DMAX_MCASP_Param *pParam;
    Int i, link, oldMask, offset1, offset2, run[2], totRuns;


    // latch offsets here and check if in use (i.e. params are non-null)
    // if not then set to 0, which is a value link can never take, which lets
    // us keep the simple comparison login in the while statement below
    offset1 = pDmaExt->pDmaxErrorParam1 ? pDmaExt->pDmaxErrorParam1->offset : 0;
    offset2 = pDmaExt->pDmaxErrorParam2 ? pDmaExt->pDmaxErrorParam2->offset : 0;

    if (!pNumRem)
        return 1;
    *pNumRem = 0;

    // if not running then return
    if (!pDevExt->runState)
        return 0;

    oldMask = HWI_disable();

    totRuns = 0;
    do {
        for (i=0; i < 2; i++) {
            run[i] = 0;

            // set link = adress of active parameter ram
            link = (*pEntry & (mEntry_param << fEntry_param)) >> fEntry_param;

            // add cnts of all parameter rams until the end of list which is the error transfer
            while ((link != offset1) && (link != offset2)) {
                pParam = (volatile DMAX_MCASP_Param *) (dapDmaxDataBase[pDmaExt->maxID] + link);
                run[i] += pParam->cnt;
                // set link to address of next parameter ram in linked list
                link = pParam->link;
            }
        }
        totRuns += 1;
    } while (abs(run[1]-run[0]) > 5 && totRuns < 10);

    *pNumRem = run[1];
    TRACE_TERSE((&TR_MOD, "DAP_DMAX_numRemain(0x%x) %d:", device, *pNumRem));
    HWI_restore (oldMask);

    return 0;
} //DAP_DMAX_numRemain



// -----------------------------------------------------------------------------
