
/*
* Copyright (C) 2004-2017 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) MIPS Load Demonstration algorithm
// interface declarations
//
//
//

/*
 *  Vendor specific (MDS) interface header for
 *  MIPS Load Demonstration algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular ML implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef ML_MDS_
#define ML_MDS_

#include <ti/xdais/ialg.h>

#include <iml.h>
#include <icom.h>

/*
 *  ======== ML_MDS_exit ========
 *  Required module finalization function
 */
#define ML_MDS_exit COM_TII_exit

/*
 *  ======== ML_MDS_init ========
 *  Required module initialization function
 */
#define ML_MDS_init COM_TII_init

/*
 *  ======== ML_MDS_IALG ========
 *  MDS's implementation of ML's IALG interface
 */
extern IALG_Fxns ML_MDS_IALG; 

/*
 *  ======== ML_MDS_IML ========
 *  MDS's implementation of ML's IML interface
 */
extern const IML_Fxns ML_MDS_IML; 


/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of MDS's
 *  MIPS Load Demonstration algorithm. However, other
 *  implementation specific operations can also be added.
 */

/*
 *  ======== ML_MDS_Handle ========
 */
typedef struct ML_MDS_Obj *ML_MDS_Handle;

/*
 *  ======== ML_MDS_Params ========
 *  We don't add any new parameters to the standard ones defined by IML.
 */
typedef IML_Params ML_MDS_Params;

/*
 *  ======== ML_MDS_Status ========
 *  We don't add any new status to the standard one defined by IML.
 */
typedef IML_Status ML_MDS_Status;

/*
 *  ======== ML_MDS_Config ========
 *  We don't add any new config to the standard one defined by IML.
 */
typedef IML_Config ML_MDS_Config;

/*
 *  ======== ML_MDS_PARAMS ========
 *  Define our default parameters.
 */
#define ML_MDS_PARAMS   IML_PARAMS

/*
 *  ======== ML_MDS_create ========
 *  Create a ML_MDS instance object.
 */
extern ML_MDS_Handle ML_MDS_create(const ML_MDS_Params *params);

/*
 *  ======== ML_MDS_delete ========
 *  Delete a ML_MDS instance object.
 */
#define ML_MDS_delete COM_TII_delete

#endif  /* ML_MDS_ */
