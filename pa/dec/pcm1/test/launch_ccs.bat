@ECHO OFF
rem:   launch_ccs.bat
rem:   batch file to launch CCS to build the SRC test app.

rem:  OS_SDK_ROOT is where the open source version of PA resides.
rem:  This directory contains pa, da, etc.
rem:  Use double backslashes so that the path can be used by sed.

rem:  TI_DIR is where different tools reside.
 
rem:  CG_TOOLS is where c6xx compiler reside.

rem:  Now launch code composer with these environment variables set.
rem:  c:
rem:  cd "C:\ti\ccsv5\eclipse"
rem:  "C:ti\ccsv5\eclipse\ccstudio.exe"
rem:  ccstudio

if "%COMPUTERNAME%"=="CPU-178" (
    echo "Launching Code Composer using settings for CPU-178."
    set OS_SDK_ROOT=C:\\pa\\10x_sys\\paf
    set TI_BASE=C:\\PA_Tools\\10x
    set CG_TOOLS=C:\\PA_Tools\\10x\ti-cgt-c6000_8.1.0
    c:
    cd "C:\\ti\\ccsv6\\eclipse"
    rem "C:\PA_Tools\CCSV5_3_0\ccsv5\eclipse\ccstudio.exe"
    start ccstudio -data C:\\Users\\niranjan.u\\workspace_v6_0
    goto end
)

if "%COMPUTERNAME%"=="LTA0322553" (
    echo "Launching Code Composer using settings for LTA0322553."
    set OS_SDK_ROOT=C:\\ti\\processor_sdk_audio_1_02_02_00\\pasrc
    set TI_BASE=C:\\ti
    set CG_TOOLS=C:\\ti\\ccsv6\\tools\\compiler\\ti-cgt-c6000_8.1.0
    c:
    cd "C:\\ti\\ccsv6\\eclipse"
    rem "C:\PA_Tools\CCSV5_3_0\ccsv5\eclipse\ccstudio.exe"
    start ccstudio -data C:\\Users\\a0322553\\workspace_v6_1_pcmTest
    goto end
)

:end
