
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX SPI and I2C Service routines data structures 
//
//
//

#ifndef SPI_I2C_H_
#define SPI_I2C_H_

#include <spi_i2c.inc>
#include <spi.h>
#include <i2c.h>
#include <tistdtypes.h>
#include <dmax_struct.h>

/* SPI_I2C macros */
#define SPI_I2C_MKEE(PTB, PTYPE)                 \
    ((((PTB) & 0x000001FF) << 8) |               \
     (PTYPE))

/* SPI_I2C API declarations */
extern Uint32 dMAX_spiOpen(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                           Uint32 pp,Uint32 tcc,Uint32 tcint,Fxn fxn);
extern void dMAX_spiClose(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                           Uint32 pp,Uint32 tcc,Uint32 tcint,Uint32 pt);
extern void dMAX_spiCfgEE(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 ee);
extern void dMAX_spiCfgPT(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 pp,Uint32 tcc,Uint32 tcint,Uint32 pt,
                          Uint32 sp,Uint32 np,Uint32 rbuf,Uint32 rlen,
                          Uint32 hwm,Uint32 lwm,Uint32 xbuf,Uint32 xlen);
extern void dMAX_spiStart(dMAX_Handle handle,Uint32 evt);
extern Uint32 dMAX_i2cOpen(dMAX_Handle handle,Uint32 revt,Uint32 xevt,
                           Uint32 maxid,Uint32 rpp,Uint32 xpp,Uint32 tcc,
                           Uint32 tcint,Fxn fxn);
extern void dMAX_i2cClose(dMAX_Handle handle,Uint32 revt,Uint32 xevt,
                          Uint32 maxid,Uint32 rpp,Uint32 xpp,Uint32 tcc,
                          Uint32 tcint,Uint32 pt);
extern void dMAX_i2cCfgEE(dMAX_Handle handle,Uint32 revt,Uint32 xevt,
                          Uint32 maxid,Uint32 ee);
extern void dMAX_i2cCfgPT(dMAX_Handle handle,Uint32 revt,Uint32 xevt,
                          Uint32 maxid,Uint32 rpp,Uint32 xpp,Uint32 tcc,
                          Uint32 tcint,Uint32 pt,Uint32 rbuf,Uint32 rlen,
                          Uint32 hwm,Uint32 lwm,Uint32 xbuf,Uint32 xlen);
extern void dMAX_i2cStart( dMAX_Handle handle,Uint32 revt,Uint32 xevt); 
extern Uint32 dMAX_spi_i2cRead(dMAX_Handle handle,Uint32 pt,Uint32 rbuf,
                               Uint32 rlen,Uint32 abuf,Uint32 alen);
extern Uint32 dMAX_spi_i2cWrite(dMAX_Handle handle,Uint32 pt,Uint32 xbuf,
                                Uint32 xlen,Uint32 abuf,Uint32 alen);
extern void dMAX_spi_i2cNoWait(dMAX_Handle handle,Uint32 tcc);
#endif // SPI_I2C_H_

