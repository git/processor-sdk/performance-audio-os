/* 
 *
 * Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* This code is auto-generated.  Any manual modifications will be lost! */
/* Most of the C66 intrinsics functions are declared here, but not all. */
#ifndef C66_INSTRINSICS_H
#define C66_INSTRINSICS_H
/* Instruction function declarations */

#ifdef TMS320C66X
__x128_t _ccmatmpy(int64_ll arg_xdwop1, __x128_t arg_qwop2);
int64_ll _ccmatmpyr1(int64_ll arg_xdwop1, __x128_t arg_qwop2);
int64_ll _ccmpy32r1(int64_ll arg_dwop1, int64_ll arg_xdwop2);
__x128_t _cmatmpy(int64_ll arg_xdwop1, __x128_t arg_qwop2);
int64_ll _cmatmpyr1(int64_ll arg_xdwop1, __x128_t arg_qwop2);
int64_ll _cmpy32r1(int64_ll arg_dwop1, int64_ll arg_xdwop2);
__x128_t _cmpysp(__float2_t arg_dwop1, __float2_t arg_xdwop2);
int32 _crot270(int32 arg_xop);
int32 _crot90(int32 arg_xop);
int64_ll _dadd(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dadd2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dadd_c(int32 arg_scst5, int64_ll arg_xdwop2);
__float2_t _daddsp(__float2_t arg_dwop1, __float2_t arg_xdwop2);
int64_ll _dapys2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _davg2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _davgnr2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _davgnru4(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _davgu4(int64_ll arg_dwop1, int64_ll arg_xdwop2);
__x128_t _dccmpy(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dccmpyr1(int64_ll arg_dwop1, int64_ll arg_xdwop2);
uint32 _dcmpeq2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
uint32 _dcmpeq4(int64_ll arg_dwop1, int64_ll arg_xdwop2);
uint32 _dcmpgt2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
uint32 _dcmpgtu4(int64_ll arg_dwop1, int64_ll arg_xdwop2);
__x128_t _dcmpy(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dcmpyr1(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dcrot270(int64_ll arg_xdwop);
int64_ll _dcrot90(int64_ll arg_xdwop);
int64_ll _ddotp4h(__x128_t arg_qwop1, __x128_t arg_qwop2);
int64_ll _ddotpsu4h(__x128_t arg_qwop1, __x128_t arg_qwop2);
__float2_t _dinthsp(uint32 arg_xop);
__float2_t _dinthspu(uint32 arg_xop);
__float2_t _dintsp(int64_ll arg_xdwop);
__float2_t _dintspu(int64_ll arg_xdwop);
int64_ll _dmax2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dmaxu4(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dmin2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dminu4(int64_ll arg_dwop1, int64_ll arg_xdwop2);
__x128_t _dmpy2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
__float2_t _dmpysp(__float2_t arg_dwop1, __float2_t arg_xdwop2);
__x128_t _dmpysu4(int64_ll arg_dwop1, int64_ll arg_xdwop2);
__x128_t _dmpyu2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
__x128_t _dmpyu4(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dmvd(int32 arg_op1, int32 arg_xop2);
int32 _dotp4h(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dotp4hll(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int32 _dotpsu4h(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dotpsu4hll(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dpackh2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dpackh4(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dpackhl2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dpackl2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dpackl4(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dpacklh2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dpacklh4(uint32 arg_op1, uint32 arg_xop2);
int64_ll _dsadd(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dsadd2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dshl(int64_ll arg_xdwop1, uint32 arg_op2);
int64_ll _dshl2(int64_ll arg_xdwop1, uint32 arg_op2);
int64_ll _dshr(int64_ll arg_xdwop1, uint32 arg_op2);
int64_ll _dshr2(int64_ll arg_xdwop1, uint32 arg_op2);
int64_ll _dshru(int64_ll arg_xdwop1, uint32 arg_op2);
int64_ll _dshru2(int64_ll arg_xdwop1, uint32 arg_op2);
__x128_t _dsmpy2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dspacku4(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dspint(__float2_t arg_xdwop);
uint32 _dspinth(__float2_t arg_xdwop);
int64_ll _dssub(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dssub2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dsub(int64_ll arg_dwop1, int64_ll arg_xdwop2);
int64_ll _dsub2(int64_ll arg_dwop1, int64_ll arg_xdwop2);
__float2_t _dsubsp(__float2_t arg_dwop1, __float2_t arg_xdwop2);
int64_ll _dxpnd2(uint32 arg_xop);
int64_ll _dxpnd4(uint32 arg_xop);
int32 _land(int32 arg_op1, int32 arg_xop2);
int32 _landn(int32 arg_op1, int32 arg_xop2);
int32 _lor(int32 arg_op1, int32 arg_xop2);
void _mfence(void);
int64_ll _mpyu2(uint32 arg_op1, uint32 arg_xop2);
__x128_t _qmpy32(__x128_t arg_qwop1, __x128_t arg_qwop2);
__x128_t _qmpysp(__x128_t arg_qwop1, __x128_t arg_qwop2);
__x128_t _qsmpy32r1(__x128_t arg_qwop1, __x128_t arg_qwop2);
uint32 _shl2(uint32 arg_xop1, uint32 arg_op2);
int64_ll _unpkbu4(uint32 arg_xop);
int64_ll _unpkh2(uint32 arg_xop);
int64_ll _unpkhu2(uint32 arg_xop);
int64_ll _xorll_c(int32 arg_scst5, int64_ll arg_xdwop2);
#endif /* #ifdef TMS320C66X */

#ifdef TMS320C67X
int32 _dpint(double64 arg_dwop);
int64_ll _mpyidll(int32 arg_op1, int32 arg_xop2);
double64 _mpysp2dp(float32 arg_op1, float32 arg_xop2);
double64 _mpyspdp(float32 arg_op1, double64 arg_xdwop2);
double64 _rcpdp(double64 arg_dwop);
float32 _rcpsp(float32 arg_xop);
double64 _rsqrdp(double64 arg_dwop);
float32 _rsqrsp(float32 arg_xop);
int32 _spint(float32 arg_xop);
#endif /* #ifdef TMS320C67X */

#endif // #ifndef C66_INSTRINSICS_H

