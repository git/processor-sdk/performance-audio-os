/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  
// Unit test for IR_IIR_mx1 Layer-1 Filter implementation

// std headers
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <math.h>
#include <clk.h>
#include <log.h>
#include  <xdc/std.h>
#include <ti/xdais/xdas.h>
#include <paftyp.h>

// fil headers
#include "fil.h"
#include "fil_tii.h"
#include "fil_tii_priv.h"
#include <filerr.h>
#include <filextern.h>
#include <filtyp.h>
#include <filters.h>
#include <fil_table.h>
#include <fil_macros.h>

#include<stdasp.h>

PAF_AudioFunctions audioFrameFunctions = {
    PAF_ASP_dB2ToLinear,
    PAF_ASP_channelMask,
	PAF_ASP_programFormat, 
	PAF_ASP_sampleRateHz,
    PAF_ASP_delay
};

#pragma DATA_SECTION(varBuf,".varBuf")
unsigned char varBuf[( 16*PAF_MAXNUMCHAN + 3 ) /4*4];

#define MAX_NUM_CHAN 16
#define MAX_NUM_SAMPLES 1024

PAF_AudioFrame pAudioFrame;

float Audio_data[MAX_NUM_CHAN][MAX_NUM_SAMPLES];
float *pAudio[MAX_NUM_CHAN];


#define FIL_TEST_TOTAL         64
#define FIL_TEST_SAMPLE_COUNT  16
#define FIL_TEST_ITERATIONS    10

#define FIL_create(a, b)  (FIL_Handle)ALG_create((IALG_Fxns *)a , NULL, (IALG_Params*)b)
#define FIL_delete(a)	ALG_delete((ALG_Handle)a)


// Test table structure
typedef struct 
{
    Uint type; // type field that will call the function, in FIL
    Int  taps; 
    Int  ch;
    Int  uniCoef;
    Int  inplace;
    Int  dPrec;
    Int  cPrec;
    Int  cascade;
	PAF_ChannelMask maskSelect;
} testTable; 

extern PAF_AudioData IIRFilterCoeff0[9];
extern PAF_AudioData IIRFilterCoeff1[9];
extern PAF_AudioData IIRFilterCoeff2[9];
extern PAF_AudioData IIRFilterCoeff3[9];
extern PAF_AudioData IIRFilterCoeff4[9];
extern PAF_AudioData IIRFilterCoeff5[9];
extern PAF_AudioData IIRFilterCoeff6[9];
extern PAF_AudioData IIRFilterCoeff7[9];

extern PAF_AudioData IIRFilterCoeff_32[9];
extern PAF_AudioData IIRFilterCoeff_44[9];
extern PAF_AudioData IIRFilterCoeff_48[9];
extern PAF_AudioData IIRFilterCoeff_64[9];
extern PAF_AudioData IIRFilterCoeff_88[9];
extern PAF_AudioData IIRFilterCoeff_96[9];
extern PAF_AudioData IIRFilterCoeff_128[9];
extern PAF_AudioData IIRFilterCoeff_192[9];


PAF_AudioData gInArray[256];
PAF_AudioData gOutArrayRef[256];
double gVarsRef[256] = {0};

// Allocation for the param pointers 
PAF_AudioData *pIn[PAF_MAXNUMCHAN];
PAF_AudioData *pOut[PAF_MAXNUMCHAN];
double *pVar[PAF_MAXNUMCHAN];
PAF_AudioData *pCoef[PAF_MAXNUMCHAN];

// Param structure
PAF_FilParam gParamTest = {
 (void **)pIn,
 (void **)pOut,
 (void **)pCoef,
 (void **)pVar,
 FIL_TEST_SAMPLE_COUNT, // Int sampleCount
 0,			// Uchar channels
 0			// Uchar use
};

  

struct IIRFilterStructType{
 LgUns type;
 LgUns sampRate;
 PAF_AudioData *cPtr[8];
}IIRFilterStruct = {
 0x00048042, 
 0x00000010, 
 {
  IIRFilterCoeff0,
  IIRFilterCoeff1,
  IIRFilterCoeff2,
  IIRFilterCoeff3,
  IIRFilterCoeff4,
  IIRFilterCoeff5,
  IIRFilterCoeff6,
  IIRFilterCoeff7
 }
};

IFIL_Status IIIR_PARAMS_STATUS = {
	sizeof(IFIL_Status), /* size */
	0x1f07,              /* mode */ /* ch-0,1,2,10 & 11 */
	0x1,                 /* use */ /* ON */                
	0x0001,              /* mask select */ /* ch-0,1,10 & 11 */
	0x0                  /* mask status */ 
};

IFIL_Config IIIR_PARAMS_CONFIG = {
	sizeof(IFIL_Config), /* size */
	(const PAF_FilCoef *)&IIRFilterStruct /* IIR filter Coefficients */
};

const IFIL_Params IIIR_PARAMS = {
	sizeof(IFIL_Params), /* size */
	0x1, /* Unused */	                  
	&IIIR_PARAMS_STATUS,
	&IIIR_PARAMS_CONFIG
};

testTable gTestArray[FIL_TEST_TOTAL] = {
//-----------------------------2Tap-Nch---------------------------------------------------
//type    		taps    ch    uniCoef   inplace  dPrec    cPrec   cascade	maskSelect

{ 0x00008041,      2,     1,      0,         0,      1,        0,       0,    0x0001	}, // 0
{ 0x00008042,      2,     2,      0,         0,      1,        0,       0,    0x0003	}, 
{ 0x00008043,      2,     3,      0,         0,      1,        0,       0,    0x0007	}, 
{ 0x00008044,      2,     4,      0,         0,      1,        0,       0,    0x0107	}, 
{ 0x00008045,      2,     5,      0,         0,      1,        0,       0,    0x0307	}, 
{ 0x00008046,      2,     6,      0,         0,      1,        0,       0,    0x0707	}, 
{ 0x00008047,      2,     7,      0,         0,      1,        0,       0,    0x0f07	}, 
{ 0x00008048,      2,     8,      0,         0,      1,        0,       0,    0x1f07	}, 

{ 0x00048041,      2,     1,      1,         0,      1,        0,       0,    0x0001	}, // 8
{ 0x00048042,      2,     2,      1,         0,      1,        0,       0,    0x0003	}, 
{ 0x00048043,      2,     3,      1,         0,      1,        0,       0,    0x0007	}, 
{ 0x00048044,      2,     4,      1,         0,      1,        0,       0,    0x0107	}, 
{ 0x00048045,      2,     5,      1,         0,      1,        0,       0,    0x0307	},
{ 0x00048046,      2,     6,      1,         0,      1,        0,       0,    0x0707	},
{ 0x00048047,      2,     7,      1,         0,      1,        0,       0,    0x0f07	},
{ 0x00048048,      2,     8,      1,         0,      1,        0,       0,    0x1f07	},

{ 0x00008041,      2,     1,      0,         1,      1,        0,       0,    0x0001	},// 16
{ 0x00008042,      2,     2,      0,         1,      1,        0,       0,    0x0003	},
{ 0x00008043,      2,     3,      0,         1,      1,        0,       0,    0x0007	},
{ 0x00008044,      2,     4,      0,         1,      1,        0,       0,    0x0107	},
{ 0x00008045,      2,     5,      0,         1,      1,        0,       0,    0x0307	},
{ 0x00008046,      2,     6,      0,         1,      1,        0,       0,    0x0707	},
{ 0x00008047,      2,     7,      0,         1,      1,        0,       0,    0x0f07	},
{ 0x00008048,      2,     8,      0,         1,      1,        0,       0,    0x1f07	},

{ 0x00008081,      4,     1,      0,         0,      1,        0,       0,    0x0001	}, // 24
{ 0x00008082,      4,     2,      0,         0,      1,        0,       0,    0x0003	}, 
{ 0x00008083,      4,     3,      0,         0,      1,        0,       0,    0x0007	}, 
{ 0x00008084,      4,     4,      0,         0,      1,        0,       0,    0x0107	}, 
{ 0x00008085,      4,     5,      0,         0,      1,        0,       0,    0x0307	}, 
{ 0x00008086,      4,     6,      0,         0,      1,        0,       0,    0x0707	}, 
{ 0x00008087,      4,     7,      0,         0,      1,        0,       0,    0x0f07	}, 
{ 0x00008088,      4,     8,      0,         0,      1,        0,       0,    0x1f07	}, 

{ 0x00048081,      4,     1,      1,         0,      1,        0,       0,    0x0001	}, // 32
{ 0x00048082,      4,     2,      1,         0,      1,        0,       0,    0x0003	}, 
{ 0x00048083,      4,     3,      1,         0,      1,        0,       0,    0x0007	}, 
{ 0x00048084,      4,     4,      1,         0,      1,        0,       0,    0x0107	}, 
{ 0x00048085,      4,     5,      1,         0,      1,        0,       0,    0x0307	},
{ 0x00048086,      4,     6,      1,         0,      1,        0,       0,    0x0707	},
{ 0x00048087,      4,     7,      1,         0,      1,        0,       0,    0x0f07	},
{ 0x00048088,      4,     8,      1,         0,      1,        0,       0,    0x1f07	},

{ 0x00008081,      4,     1,      0,         0,      1,        0,       0,    0x0001	}, // 40
{ 0x00008082,      4,     2,      0,         0,      1,        0,       0,    0x0003	}, 
{ 0x00008083,      4,     3,      0,         0,      1,        0,       0,    0x0007	}, 
{ 0x00008084,      4,     4,      0,         0,      1,        0,       0,    0x0107	}, 
{ 0x00008085,      4,     5,      0,         0,      1,        0,       0,    0x0307	}, 
{ 0x00008086,      4,     6,      0,         0,      1,        0,       0,    0x0707	}, 
{ 0x00008087,      4,     7,      0,         0,      1,        0,       0,    0x0f07	}, 
{ 0x00008088,      4,     8,      0,         0,      1,        0,       0,    0x1f07	}, 

{ 0x00048081,      4,     1,      1,         0,      1,        0,       0,    0x0001	},// 48
{ 0x00048082,      4,     2,      1,         0,      1,        0,       0,    0x0003	},
{ 0x00048083,      4,     3,      1,         0,      1,        0,       0,    0x0007	},
{ 0x00048084,      4,     4,      1,         0,      1,        0,       0,    0x0107	},
{ 0x00048085,      4,     5,      1,         0,      1,        0,       0,    0x0307	},
{ 0x00048086,      4,     6,      1,         0,      1,        0,       0,    0x0707	},
{ 0x00048087,      4,     7,      1,         0,      1,        0,       0,    0x0f07	},
{ 0x00048088,      4,     8,      1,         0,      1,        0,       0,    0x1f07	},

{ 0x80048081,      4,     1,      1,         0,      1,        0,       0,    0x0001	}, // 56
{ 0x80048082,      4,     2,      1,         0,      1,        0,       0,    0x0003	}, 
{ 0x80048083,      4,     3,      1,         0,      1,        0,       0,    0x0007	}, 
{ 0x80048084,      4,     4,      1,         0,      1,        0,       0,    0x0107	}, 
{ 0x80048085,      4,     5,      1,         0,      1,        0,       0,    0x0307	}, 
{ 0x80048086,      4,     6,      1,         0,      1,        0,       0,    0x0707	}, 
{ 0x80048087,      4,     7,      1,         0,      1,        0,       0,    0x0f07	}, 
{ 0x80048088,      4,     8,      1,         0,      1,        0,       0,    0x1f07	}, 

};

void Audioframe_init(PAF_AudioFrame *pAudioFrame)
{
  int i;
  

  for(i=0; i < MAX_NUM_CHAN; i++ ) 		
    {
	 	pAudio[i] = Audio_data[i];
	 }
  pAudioFrame->sampleCount = FIL_TEST_SAMPLE_COUNT;  	
  pAudioFrame->data.sample = pAudio;
  pAudioFrame->sampleRate  =  PAF_SAMPLERATE_48000HZ;	
  pAudioFrame->channelConfigurationStream.part.sat = PAF_CC_SAT_SURROUND4;
  pAudioFrame->channelConfigurationStream.part.sub = PAF_CC_SUB_ONE;
  pAudioFrame->pChannelConfigurationMaskTable = &PAF_ASP_stdCCMT;
  pAudioFrame->fxns = &audioFrameFunctions;

}

void PAF_ALG_delete(ALG_Handle handle)
{
}

void PAF_ALG_activate(ALG_Handle handle)
{
}

void PAF_ALG_deactivate(ALG_Handle handle)
{
}

struct PAF_ALG_Fxns PAF_ALG_fxns =
{
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};
static Int varsPerCh( Uint type )
{
    Int coeffPrec, bytesPerVar;
    Int i, count, adjPower2;

    coeffPrec = FilMacro_GetPrecField(type); /* Get coefficient precision */
    
    /* Identify the coeff prec and find the bytes/var-memory element */
    if( coeffPrec == PAF_AUDIODATATYPE_DOUBLE )
        bytesPerVar = 8;
    else if( coeffPrec == PAF_AUDIODATATYPE_FLOAT )
        bytesPerVar = 4;
    else if( coeffPrec == PAF_AUDIODATATYPE_INT )
        bytesPerVar = 4;
    else if( coeffPrec == PAF_AUDIODATATYPE_INT16 )
        bytesPerVar = 2;
    
    /* Switch to the given filter group */           
    switch(FilMacro_GetGroupField(type))
    {
        /* Impulse Response Group */
        case FIL_IR :
        {
            Int taps, subGroup;
            subGroup = FilMacro_GetSubGroupField(type);
            taps = FilMacro_GetIRTap(type) ;

            /* IIR Sub-group */
            if (subGroup == FIL_IR_IIR_DF1) {
#ifdef FIL2_BUILD
                if(FilMacro_GetIR_IIR_ProcField(type) == FIL_IR_IIR_PROC_MX1) {
                    bytesPerVar = 8; /* Internal processing is Double precision */
					if (taps == 2) return 24;		// MX1 T2 is DF1 implementation;
				}
#endif // FIL2_BUILD                    
 
                return( taps * bytesPerVar );
            }
			else if (subGroup == FIL_IR_IIR_DF2) {
				if (taps == 2) return 24;
				else return ( bytesPerVar * taps);	
			}
            /* FIR Sub-group */
            else
            {
            /* State mem is made ^2, for easy circularing */
                /* count=bits from MSB bit to left most '1' */                
                FilMacro_Left_1(taps, 16, count, i); 
                                
                adjPower2 = 0x8000 >> count;/* Get the ^2 just below 'taps' */
                /* If taps is not ^2 */
                if( taps & (~adjPower2) )
                    adjPower2 <<= 1; /* Shift, to get next ^2 no, above taps */
                                  
                return( adjPower2 * bytesPerVar );
            }
        } /* case FIL_IR */
               
        case FIL_CASCADE_IR :
        {
            Int taps, subGroup;
            subGroup = FilMacro_GetSubGroupField(type);

#ifdef FIL2_BUILD
            if(FilMacro_GetIR_IIR_ProcField(type) == FIL_IR_IIR_PROC_MX1)
                bytesPerVar = 8; /* Internal processing is Double precision */
#endif // FIL2_BUILD

            if( subGroup == FIL_IR_IIR_CASC_SOS_DF2 )
            {
                // Get the no. of cascades 
                taps = FilMacro_GetIRTap(type) ;
                                
                // Return 2*cascades
                return( taps * bytesPerVar );
            }
            else
                return(FIL_ERROR);
        }
               
        default :  
            return(0);
            
    } /* switch(FilMacro_GetGroupField(type)) */

} 

void error(char *fmt, ...)
{
    va_list ap;
    fprintf(stderr, "error: ");
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
    exit(2);
}

void main(void)
{
    FIL_Handle iirHandle;
    int i,j,k, testfailure = 0, testIterations,ch,testCount,taps, varsperCh;
	void ** coeffsPtr; int coeffPtrPerCh = 0;
	Uint mask = 0x1;
	PAF_ChannelMask appCh, chTemplate;
	int dSampleRate, sampIdx = 0, cSampRate;
	void * pVars;void *varsPtr;

//    FIL_init();
//	for( i = 0; i < 256; i++ ) 
//		IIRFilterCoeff[i] = (i+1)*0.00015;
	
	Audioframe_init(&pAudioFrame);

	pVars = varBuf;
	for( testCount = 0; testCount < FIL_TEST_TOTAL; testCount++ ) {

		IIRFilterStruct.type = gTestArray[testCount].type;
		IIIR_PARAMS_STATUS.maskSelect = gTestArray[testCount].maskSelect;

		for( i = 0; i < 256; i++ ) {
			gOutArrayRef[i] = 0;
	    	gVarsRef[i] = 0;
		}

		varsperCh = varsPerCh(gTestArray[testCount].type);

		testfailure = 0;
		
		sampIdx = 0;
		if( gTestArray[testCount].type & FIL_BIT31 ) {
        
        	IIRFilterStruct.sampRate = 0x000003fc;
			IIRFilterStruct.cPtr[0] = IIRFilterCoeff_32;
			IIRFilterStruct.cPtr[1] = IIRFilterCoeff_44;
			IIRFilterStruct.cPtr[2] = IIRFilterCoeff_48;
			IIRFilterStruct.cPtr[3] = IIRFilterCoeff_64;
			IIRFilterStruct.cPtr[4] = IIRFilterCoeff_88;
			IIRFilterStruct.cPtr[5] = IIRFilterCoeff_96;
			IIRFilterStruct.cPtr[6] = IIRFilterCoeff_128;
			IIRFilterStruct.cPtr[7] = IIRFilterCoeff_192;

			dSampleRate = pAudioFrame.sampleRate; 
        
        	/* Coefficient sample rate bit field */
        	cSampRate    = IIRFilterStruct.sampRate;
        	    
        	/* Check whether Coeff structure contains sampRate matching the data */
        	if( !( cSampRate & ( 0x1 << dSampleRate ) ) )
        	    printf("Sample Rate Not supported.");
        	    
        	dSampleRate -= 2; /* Skip the 'none' and 'unknown' sample rates */
        	cSampRate  >>= 2; /* Do the same in coeff bit field also */
        
        	/* Find the no. of sample rates(1's) before the requested one(bit) */
        	FilMacro_1sToRight(cSampRate, dSampleRate, sampIdx, i);
        
		}
		for(testIterations = 0; testIterations < FIL_TEST_ITERATIONS ; ++testIterations) {
			
    		Char *charPtr = (Char *)pVars;
			coeffPtrPerCh = 0;
			mask = 0x1;
			for( i = 0; i < 256; i++ ) {
				gOutArrayRef[i] = 0;
	    		gVarsRef[i] = 0;
			}
   
		    /* Reset the memory, byte-wise */
    		for( i=0; i < 8 * varsperCh; i++ )
        		*charPtr++ = 0;
    		for(j = 0; j < PAF_MAXNUMCHAN; ++j){
	    		for( i = 0; i < 256; i++ ) 
					pAudioFrame.data.sample[j][i] = i* (testIterations+1)* 0.001;
			}
    	
    		for( i = 0; i < 256; i++ )
				gInArray[i] = pAudioFrame.data.sample[0][i];

			ch   = gTestArray[testCount].ch;
			taps = gTestArray[testCount].taps;
    	
    		gParamTest.channels = ch;
			gParamTest.use      = taps;	
			gParamTest.sampleCount = 16;	

    		varsPtr     = (void *)pVars;
    		coeffsPtr = (void *)&IIRFilterStruct.cPtr[0];
			coeffsPtr  += sampIdx;
			if (!gTestArray[testCount].uniCoef)
				coeffPtrPerCh = 1;

    		appCh = gTestArray[testCount].maskSelect;
    		chTemplate = IIIR_PARAMS_STATUS.mode;
    		for( i = 0, j = 0; i < PAF_MAXNUMCHAN; i++ )
		    {
        		/* If the channel bit is 1(select),fill its corresponding ptrs */
        		if(appCh & mask)
        		{
            		gParamTest.pIn[j] = gInArray;
					gParamTest.pVar[j] = varsPtr;
					gParamTest.pOut[j] = gOutArrayRef + j*FIL_TEST_SAMPLE_COUNT;
					gParamTest.pCoef[j] = *coeffsPtr;
            		j++;
        		}
        		/* If channel template has the next ch, offset the pointers */
        		if(chTemplate & mask)
        		{
        		    varsPtr   = (char *)varsPtr + varsperCh;
        		    coeffsPtr += coeffPtrPerCh;
        		}
        		/* Update for next iteration */
        		mask <<= 1;
    }
    		if((iirHandle = FIL_create((IALG_Fxns *)&FIL_TII_IFIL, (FIL_Params *)&IIIR_PARAMS)) != 0)
    		{
     			if (FIL_TII_apply_patch(iirHandle, &pAudioFrame))
     				printf("Filter function call Returned Error");

    		} else {
				error("Memory Allocation failure");
			}

   			generic_IR_filter( &gParamTest, gTestArray[testCount] ); 
    
			for (i = 0,k=0; i < PAF_MAXNUMCHAN ; ++i) {
				
				if ((gTestArray[testCount].maskSelect >> i ) & 1u) {
					for (j = 0; j < FIL_TEST_SAMPLE_COUNT; ++j) {
						if ( fabs(gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j] - pAudioFrame.data.sample[i][j] ) > 0.00000005 )
							testfailure = 1;
					}
					k++;
				}
			}
		
			if (testfailure)
				printf("Test Failed at iteration %d for testCount %d, Channels tested = %d \n",testIterations,testCount, k) ; 
    		FIL_TII_reset(iirHandle, &pAudioFrame);  //To clear the FilterStates
    		FIL_delete(iirHandle);
//    		FIL_exit();
		}
		if ( !testfailure)
			printf("Test Passed for TestCount %d\n", testCount);
	}
}



int generic_IR_filter( PAF_FilParam * param, testTable testElement) {

	if( (testElement.dPrec == 1) && (testElement.cPrec == 0) )
        return( iir_generic1( param ) );    
    else    
        return( iir_generic( param ) );    
    
}

int iir_generic( PAF_FilParam * pParam ) {
    Int count;
    Int i, t, taps, ch;
    PAF_AudioData accY,  accW;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtVars;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    PAF_AudioData filtVarsTemp0, filtVarsTemp1;
    
    count = pParam->sampleCount;
    taps  = pParam->use;
    
    for (ch = 0; ch < pParam->channels; ch++) {
        x = (PAF_AudioData *)pParam->pIn[ch];
        y = (PAF_AudioData *)pParam->pOut[ch];            

        filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;

        filtVars  = (PAF_AudioData *)pParam->pVar[ch];
        
        for (i = 0; i < count; i ++) {
             accY = 0; 
             accW = *x;
             
             for( t = 0; t < taps; t++ ) {
                 accW += filtCfsA[t]   * filtVars[t];  
                 
                 accY += filtCfsB[t+1] * filtVars[t]; 
             }
             
             accY += accW * filtCfsB[0]; 
             
             *y++ = accY;
             x++; 
             
             filtVarsTemp0 = filtVars[0];
             
             for( t = 0; t < taps; t++ ) {
                 filtVarsTemp1 =  filtVars[t + 1];
                 
                 filtVars[t + 1] = filtVarsTemp0;
                 
                 filtVarsTemp0 =  filtVarsTemp1; 
             }   
             filtVars[taps] = filtVarsTemp1;
                       
             
             filtVars[0] = accW;
             
         }
     }
     return( FIL_SUCCESS );      
}
   
int iir_generic1( PAF_FilParam * pParam )
{
    Int count;
    Int i, t, taps, ch;
    double accY, accW;
    double * restrict filtVars;
    double filtVarsTemp0, filtVarsTemp1;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    count = pParam->sampleCount;
    taps  = pParam->use;
    
    for (ch = 0; ch < pParam->channels; ch++) {
        x = (PAF_AudioData *)pParam->pIn[ch];
        y = (PAF_AudioData *)pParam->pOut[ch];            

        filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;

        filtVars  = (double *)pParam->pVar[ch];
        
        for (i = 0; i < count; i ++) {
             accY = 0; 
             accW = *x * filtCfsB[0];
             
             for( t = 0; t < taps; t++ ) {
                 accW += filtCfsA[t] * filtVars[t];  
                 
                 accY += filtCfsB[t+1] * filtVars[t]; 
             }
             
             accY += accW; 
             
             *y++ = accY;
             x++; 
             
             filtVarsTemp0 = filtVars[0];
             
             for( t = 0; t < taps; t++ ) {
                 filtVarsTemp1 =  filtVars[t + 1];
                 
                 filtVars[t + 1] = filtVarsTemp0;
                 
                 filtVarsTemp0 =  filtVarsTemp1; 
             }   
             filtVars[taps] = filtVarsTemp1;
                       
             
             filtVars[0] = accW;
             
         }
     }
     return( FIL_SUCCESS );      
}
