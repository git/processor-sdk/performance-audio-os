/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//

#ifndef MCASP_
#define MCASP_ 1

// MCASP Event Entry constants 
#define MCASP_PT_MSK    0xFF     // PT Mask
#define MCASP_PT_SHFT   0        // PT Shift

#define MCASP_PTB_MSK   0x1FF    // PTB Mask
#define MCASP_PTB_SHFT  8        // PTB Shift

#define MCASP_WDSZ1     1        // WDSZ - 1
#define MCASP_WDSZ2     2        // WDSZ - 2
#define MCASP_WDSZ3     3        // WDSZ - 3
#define MCASP_WDSZ4     4        // WDSZ - 4
#define MCASP_WDSZ_MSK  0x7      // ETYPE Mask
#define MCASP_WDSZ_SHFT 17       // ETYPE Shift

#define MCASP_BLEN_MSK  0xFF     // ETYPE Mask
#define MCASP_BLEN_SHFT 24       // ETYPE Shift

// MCASP parameter field lengths and offsets
#define MCASP_CNT_OFF    0       // MCASP cnt offset
#define MCASP_CNT_LEN    2       // MCASP cnt length
#define MCASP_LINK_OFF   2       // MCASP link offset
#define MCASP_LINK_LEN   2       // MCASP link length
#define MCASP_XFER_OFF   4       // MCASP xfer offset
#define MCASP_XFER_LEN   4       // MCASP xfer length
#define MCASP_TCC_OFF    8       // MCASP tcc offset
#define MCASP_TCC_LEN    1       // MCASP tcc length
#define MCASP_TCCREG_OFF 9       // MCASP tccreg offset
#define MCASP_TCCREG_LEN 1       // MCASP tccreg length
#define MCASP_INTNUM_OFF 10      // MCASP intNum offset
#define MCASP_INTNUM_LEN 1       // MCASP intNum length
#define MCASP_STAT_OFF   11      // MCASP stat offset
#define MCASP_STAT_LEN   1       // MCASP stat length
#define MCASP_RTI_OFF    12      // MCASP rti offset
#define MCASP_RTI_LEN    4       // MCASP rti length

#endif // MCASP_
