/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/**
 *  @file   util_paf_file_io.c
 *
 *  @brief
 *
 *****************************************************************************/

#include "util_file_io.h"
#include "util_paf_file_io.h"
#include <math.h>

static float *pAudio[32];

/**
 *  @brief     Initialization
 *
 */
int PAF_FIO_Init(tFIOHandle *file, char *inFile, char *outFile)
{
	return FIO_Init(file, inFile,  outFile);
}

/**
 *  @brief     Configuration call
 *
 */
int PAF_FIO_Config(tFIOHandle *file, PAF_AudioFrame *pAudioFrame, int bufChannelCount)
{
	int chIndex;
	int maxAFChannels = pAudioFrame->data.nChannels;
	int maxAFSamples = pAudioFrame->data.nSamples;

	pAudioFrame->data.sample = pAudio;

	// also allocate memory for channels and assign pointers in audio frame
	for (chIndex = 0; chIndex < maxAFChannels; chIndex++)
	{
		pAudioFrame->data.sample[chIndex] = (PAF_AudioData*)calloc(maxAFSamples, sizeof(PAF_AudioData));
	}

	return FIO_Config(file, bufChannelCount);
}

/**
 *  @brief     Data read from input buffer into PAF structure
 *
 *  @todo      Testing required
 */
int PAF_FIO_Read(tFIOHandle *file, PAF_AudioFrame *pAudioFrame)
{
	int i;
	int error = RETRUN_ERROR;
	int numChannels = file->bufChannelCount;     // number of channels present
	int inSampleCount = file->inBufSampleCount;

	// get inBuf from read operation
	if (FIO_Read(file) != RETRUN_SUCCESS)
		return error;

	// copy the input buffer to mapped Audio Frame channel
	for (i = 0; i < numChannels; i++)
	{
		char chan = AFChanPtrMap[numChannels][i];

		// check for valid mapping
		if (chan != -1)
			memcpy(&pAudioFrame->data.sample[chan][0], &file->inBuf[i*inSampleCount], sizeof(float)*inSampleCount);

		error = RETRUN_SUCCESS;
	}

    return error;
}

/**
 *  @brief     Data write to output file
 *
 */
int PAF_FIO_Write(tFIOHandle *file, PAF_AudioFrame *pAudioFrame)
{
	int i;
	int numChannels = file->bufChannelCount;     // number of channels present
	int outSampleCount = file->outBufSampleCount;

	// copy the input buffer to mapped Audio Frame channel
	for (i = 0; i < numChannels; i++)
	{
		char chan = AFChanPtrMap[numChannels][i];

		// check for valid mapping
		if (chan != -1)
			memcpy(&file->outBuf[i*outSampleCount], &pAudioFrame->data.sample[chan][0], sizeof(float)*outSampleCount);

	}

	return FIO_Write(file);
}

/**
 *  @brief     Deinitialization
 *
 */
int PAF_FIO_DeInit(tFIOHandle *file)
{
	return FIO_DeInit(file);
}


