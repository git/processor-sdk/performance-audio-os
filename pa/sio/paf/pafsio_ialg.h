
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// XDAIS IALG wrapper for PAF SIO drivers (declarations)
//
//
//

#ifndef PAFSIO_IALG_
#define PAFSIO_IALG_

#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include <ti/xdais/xdas.h>
#include <com_tii_priv.h>

typedef struct PAF_SIO_IALG_Params {
	XDAS_Int8			 numRec;
    const IALG_MemRec   *pMemRec;
} PAF_SIO_IALG_Params;

typedef struct PAF_SIO_IALG_Config {
	XDAS_Int8			 numRec;
    IALG_MemRec         *pMemRec;
} PAF_SIO_IALG_Config;

typedef struct PAF_SIO_IALG_Obj {
    IALG_Obj             alg;          
    PAF_SIO_IALG_Config  config;
} PAF_SIO_IALG_Obj;

extern IALG_Fxns PAF_SIO_IALG;

// no current customizations for IB or OB
#define IB_TIH_init COM_TII_init
#define IB_TIH_IIB  PAF_SIO_IALG

#define OB_TIH_init COM_TII_init
#define OB_TIH_IOB  PAF_SIO_IALG

#endif // PAFSIO_IALG_
