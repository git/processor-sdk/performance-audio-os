# 
#  Copyright (C) 2004-2018 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 
#
# Common Tool definitions for PA.
#
#
# -----------------------------------------------------------------------------

# Architecture specific settings of tools and tool options
TI_DIR = c:/ti
#PASDK_DIR=C:/ti

# Tools
ifeq (${ARCH},c66x)
CG_TOOLS_${ARCH} ?= ${TI_DIR}/ti-cgt-c6000_8.2.2
CC = "${CG_TOOLS_${ARCH}}/bin/cl6x.exe"
LD = "${CG_TOOLS_${ARCH}}/bin/lnk6x.exe"
AR = "${CG_TOOLS_${ARCH}}/bin/ar6x.exe"
NM = "${CG_TOOLS_${ARCH}}/bin/nm6x.exe"
endif

ifeq (${ARCH},a15)
CG_TOOLS_${ARCH} ?= ${TI_DIR}/gcc-arm-none-eabi-6-2017-q1-update
CC  = "${CG_TOOLS_${ARCH}}/bin/arm-none-eabi-gcc"
AR  = "${CG_TOOLS_${ARCH}}/bin/arm-none-eabi-ar"
LNK = "${CG_TOOLS_${ARCH}}/bin/arm-none-eabi-ld"
endif

# Component versions
BIOSVER=bios_6_52_00_12
XDCVER=xdctools_3_50_03_33_core
IPCVER=ipc_3_47_01_00
PDKVER=pdk_k2g_1_0_9
EDMA3LLDVER=edma3_lld_2_12_05_30C
XDAISVER=xdais_7_24_00_04
DSPLIBVER=dsplib_c66x_3_4_0_0
#PASDKVER=processor_sdk_audio_1_03_00_00

# Installation directories
BIOSROOT            ?= ${TI_DIR}/${BIOSVER}
XDCROOT             ?= ${TI_DIR}/${XDCVER}
IPCROOT             ?= ${TI_DIR}/${IPCVER}
PDKROOT             ?= ${TI_DIR}/${PDKVER}
EDMA3LLDROOT        ?= ${TI_DIR}/${EDMA3LLDVER}
XDAISROOT           ?= ${TI_DIR}/${XDAISVER}
DSPLIBROOT          ?= ${TI_DIR}/${DSPLIBVER}
#PASDKROOT           ?= ${PASDK_DIR}/${PASDKVER}

# PASDK custom IPC version
#IPCROOT            ?= ${PASDKROOT}/psdk_cust/${IPCVER}
# PASDK custom PDK version
#PDKROOT            ?= ${PASDKROOT}/psdk_cust/${PDKVER}

# Packages directories
BIOS_PCKGS          = ${BIOSROOT}/packages
XDC_PCKGS           = ${XDCROOT}/packages
IPC_PCKGS           = ${IPCROOT}/packages
PDK_PCKGS           = ${PDKROOT}/packages
EDMA3LLD_PCKGS      = ${EDMA3LLDROOT}/packages
XDAIS_PCKGS         = ${XDAISROOT}/packages
DSPLIB_PCKGS        = ${DSPLIBROOT}/packages
#PASDK_PCKGS         = ${PASDKROOT}/packages


# BIOS and XDC
XDC_PLAT            = ti.platforms.generic:plat
XS                  = export XDCROOT="${XDCROOT}"; export XDCPATH="${BIOS_PCKGS}"; "${XDCROOT}/xs"

# Common includes
ifeq ($(ARCH),c66x)
INCLUDES += -I"$(CG_TOOLS_$(ARCH))/include"
INCLUDES += -I"$(XDC_PCKGS)"
INCLUDES += -I"$(BIOS_PCKGS)"
INCLUDES += -I"$(XDAIS_PCKGS)"
INCLUDES += -I"$(DSPLIB_PCKGS)"
INCLUDES += -I"$(EDMA3LLD_PCKGS)"
INCLUDES += -I"$(IPC_PCKGS)"
#INCLUDES += -I"$(PASDK_PCKGS)"
endif

ifeq ($(ARCH),a15)
INCLUDES += -I"$(CG_TOOLS_$(ARCH))/include"
INCLUDES += -I"$(CG_TOOLS_$(ARCH))/include/newlib-nano"
INCLUDES += -I"$(XDC_PCKGS)"
INCLUDES += -I"$(BIOS_PCKGS)"
INCLUDES += -I"$(XDAIS_PCKGS)"
INCLUDES += -I"$(IPC_PCKGS)"
#INCLUDES += -I"$(PASDK_PCKGS)"
endif

# PA includes
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/asp/com`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/asp/std`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/dec/com`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/f/include`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/f/alpha`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/f/s3`
INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/sio/acp1`
INCLUDES += -I`cygpath -w ${ROOTDIR}/da/psp/dat`
#INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/chip/primus/csl`
#INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/chip/primus/csl/c674x`
#INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/boards/drivers`

# FIL includes
FIL_INCLUDES  = -I`cygpath -w ${ROOTDIR}/pa/asp/fil`
FIL_INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/asp/fil/alg`
FIL_INCLUDES += -I`cygpath -w ${ROOTDIR}/pa/asp/fil/src`

# -pdsw225 generates a compiler warning on implicitly defined functions
# -mv6740, among other things, defines _674_ which is needed for things like hwi.h
#-pdsr1037 # TODO: what is id 1037?
#-mo # Place each function in a separate subsection
#--mem_model:data=far # Const access model
#--mem_model:const=far # Data access model
#-k # Keep the generated .asm file

ifeq ($(ARCH),c66x)
#DEFINES  += -d"PAF_DEVICE=0xD8000000"
DEFINES  += -d"PAF_DEVICE=0xDA000000"
CFLAGS   = -fr"." $(INCLUDES) $(DEFINES)
CFLAGS  += -pdsr1037
CFLAGS  += -pdse225
CFLAGS  += -mo
CFLAGS  += -mv6600
CFLAGS  += --mem_model:data=far
CFLAGS  += --mem_model:const=far
CFLAGS  += -k
CFLAGS  += --abi=eabi
CFLAGS  += --strip_coff_underscore
CFLAGS  += --visibility=hidden
#CFLAGS  += --abi=coffabi
#CFLAGS  += --symdebug:none
#CFLAGS  += --c99
ARFLAGS= rq
endif

ifeq ($(ARCH),a15)
DEFINES = -Dxdc_target_types__=ti/targets/arm/elf/std.h -D__TMS470__ -DPAF_DEVICE=0xD8000000  -Dfar= -DARMCOMPILE
CFLAGS = -mcpu=cortex-a15 -mfpu=neon-vfpv4 -mtune=cortex-a15 -mthumb-interwork -mthumb -fPIC -funroll-loops -ftree-vectorize -static-libgcc -c -Wall -Wextra -Wstrict-prototypes -Wmissing-prototypes -pedantic -std=gnu99 -Wdeclaration-after-statement -Wvla -mfloat-abi=hard  $(INCLUDES)
ARFLAGS= cr
endif

ifeq (${CONFIG},debug)
ifeq (${ARCH},c66x)
CFLAGS += -g
endif
ifeq (${ARCH},a15)
CFLAGS += -O0 -g
endif
endif
ifeq (${CONFIG},release)
ifeq (${ARCH},c66x)
CFLAGS += -o3
endif
ifeq (${ARCH},a15)
CFLAGS += -O3
endif
endif

# -q = quiet
ARFLAGS= rq 
#DEFINES = -"Dxdc_target_types__=ti/targets/std.h"
#DEFINES += -D"xdc_target_name__=ti.targets.elf.C66"

#export CG_TOOLS
