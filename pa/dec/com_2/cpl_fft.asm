
*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*

*

* ========================================================================= *
*                                                                           *
*     TEXAS INSTRUMENTS, INC.                                               *
*                                                                           *
*     NAME                                                                  *
*           fftSPxSP- Single precision floating point FFT for complex input *
*                                                                           *
*     AUTHOR                                                                *
*           TII-PA                                                          *
*                                                                           *
*      USAGE                                                                * 
*           This routine is C-callable and can be called as:                * 
*                                                                           * 
*           void fft_SPXSP(                                                 * 
*               int N, float * ptr_x, float * ptr_w, float * ptr_y,         * 
*               unsigned char * brev, int n_min, int offset, int n_max);    * 
*                                                                           * 
*           N = length of fft in complex samples, power of 2 such that      * 
*           N>=8 and N <= 16384.                                            * 
*           ptr_x = pointer to complex data input                           * 
*           ptr_w = pointer to complex twiddle factor (see below)           * 
*           *************************************************************   * 
*           NOTE:   Both ptr_x and ptr_w  Should be double word aligned..   * 
*           *************************************************************   * 
*           ptr_y = pointer to complex output data                          * 
*           brev = pointer to bit reverse table containing 64 entries       * 
*           n_min = smallest fft butterfly used in computation              * 
*                   used for decomposing fft into subffts, see notes        * 
*           *************************************************************   * 
*           n_min should be 4 if N can be represented as Power of 4         * 
*                   else, n_min should be 2                                 * 
*           *************************************************************   * 
*           offset = index in complex samples of sub-fft from start of      * 
*                    main fft                                               * 
*           n_max = size of main fft in complex samples                     * 
*                                                                           * 
*     DESCRIPTION                                                           * 
*          The benchmark performs a mixed radix forwards fft using          * 
*          a special sequence of coefficients generated in the following    * 
*          way:                                                             * 
*                                                                           * 
*            /* generate vector of twiddle factors for optimized            * 
*           algorithm */                                                    * 
*           void tw_gen(float * w, int N)                                   * 
*           {                                                               * 
*             int j, k;                                                     * 
*             double x_t, y_t, theta1, theta2, theta3;                      * 
*             const double PI = 3.141592654;                                * 
*                                                                           * 
*             for (j=1, k=0; j <= N>>2; j = j<<2)                           * 
*             {                                                             * 
*                 for (i=0; i < N>>2; i+=j)                                 * 
*                 {                                                         * 
*                     theta1 = 2*PI*i/N;                                    * 
*                     x_t = cos(theta1);                                    * 
*                     y_t = sin(theta1);                                    * 
*                     w[k]   =  (float)x_t;                                 * 
*                     w[k+1] =  (float)y_t;                                 * 
*                                                                           * 
*                     theta2 = 4*PI*i/N;                                    * 
*                     x_t = cos(theta2);                                    * 
*                     y_t = sin(theta2);                                    * 
*                     w[k+2] =  (float)x_t;                                 * 
*                     w[k+3] =  (float)y_t;                                 * 
*                                                                           * 
*                     theta3 = 6*PI*i/N;                                    * 
*                     x_t = cos(theta3);                                    * 
*                     y_t = sin(theta3);                                    * 
*                     w[k+4] =  (float)x_t;                                 * 
*                     w[k+5] =  (float)y_t;                                 * 
*                     k+=6;                                                 * 
*                 }                                                         * 
*             }                                                             * 
*           }                                                               * 
*         This redundant set of twiddle factors is size 2*N float samples.  * 
*         The function is accurate to about 130dB of signal to noise ratio  * 
*         to the DFT function below:                                        * 
*                                                                           * 
*           void dft(int N, float x[], float y[])                           * 
*           {                                                               * 
*              int k,i, index;                                              * 
*              const float PI = 3.14159654;                                 * 
*              float * p_x;                                                 * 
*              float arg, fx_0, fx_1, fy_0, fy_1, co, si;                   * 
*                                                                           * 
*              for(k = 0; k<N; k++)                                         * 
*              {                                                            * 
*                p_x = x;                                                   * 
*                fy_0 = 0;                                                  * 
*                fy_1 = 0;                                                  * 
*                for(i=0; i<N; i++)                                         * 
*                {                                                          * 
*                  fx_0 = p_x[0];                                           * 
*                  fx_1 = p_x[1];                                           * 
*                  p_x += 2;                                                * 
*                  index = (i*k) % N;                                       * 
*                  arg = 2*PI*index/N;                                      * 
*                  co = cos(arg);                                           * 
*                  si = -sin(arg);                                          * 
*                  fy_0 += ((fx_0 * co) - (fx_1 * si));                     * 
*                  fy_1 += ((fx_1 * co) + (fx_0 * si));                     * 
*                }                                                          * 
*                y[2*k] = fy_0;                                             * 
*                y[2*k+1] = fy_1;                                           * 
*              }                                                            * 
*           }                                                               * 
*                                                                           * 
*          The function takes the table and input data and calculates       * 
*          the fft producing the frequency domain data in the Y array.      * 
*                                                                           * 
*          *************************************************************    * 
*          *************************************************************    * 
*          The following Notes doesnot effect Implementation of this FFT    * 
*          on Antara, as Memory structure of Antara is Completely           * 
*          different from Malhar, but to maintain a common interface for    * 
*          both, the following usage is also still valid.                   * 
*          *************************************************************    * 
*          *************************************************************    * 
*                                                                           * 
*          As the fft allows every input point to effect every output       * 
*          point in a cache based system such as the c6711, this causes     * 
*          cache thrashing. This is mitigated by allowing the main fft      * 
*          of size N to be divided into several steps, allowing as much     * 
*          data reuse as possible.                                          * 
*                                                                           * 
*          For example the following function:                              * 
*                                                                           * 
*          fft_SPXSP(1024, &x[0],&w[0],y,brev,4,  0,1024);                  * 
*                                                                           * 
*          is equvalent to:                                                 * 
*                                                                           * 
*          fft_SPXSP(1024,&x[2*0],  &w[0] ,   y,brev,256,  0,1024)          * 
*          fft_SPXSP(256, &x[2*0],  &w[2*768],y,brev,4,    0,1024)          * 
*          fft_SPXSP(256, &x[2*256],&w[2*768],y,brev,4,  256,1024)          * 
*          fft_SPXSP(256, &x[2*512],&w[2*768],y,brev,4,  512,1024)          * 
*          fft_SPXSP(256, &x[2*768],&w[2*768],y,brev,4,  768,1024)          * 
*                                                                           * 
*          Notice how the 1st fft function is called on the entire 1K       * 
*          data set it covers the 1st pass of the fft until the butterfly   * 
*          size is 256. The following 4 ffts do 256 pt ffts 25% of the      * 
*          size. These continue down to the end when the buttly is of size  * 
*          4. They use an index to the main twiddle factor array of         * 
*          0.75*2*N.This is because the twiddle factor array is composed    * 
*          of successively decimated versions of the main array.            * 
*                                                                           * 
*          N not equal to a power of 4 can be used, i.e. 512. In this case  * 
*          to decompose the fft the following would be needed :             * 
*                                                                           * 
*          fft_SPXSP(512, &x[0],&w[0],y,brev,2,  0,512);                    * 
*                                                                           * 
*          is equvalent to:                                                 * 
*                                                                           * 
*          fft_SPXSP(512, &x[2*0],  &w[0] ,   y,brev,128,  0,512)           * 
*          fft_SPXSP(128, &x[2*0],  &w[2*384],y,brev,4,    0,512)           * 
*          fft_SPXSP(128, &x[2*128],&w[2*384],y,brev,4,  128,512)           * 
*          fft_SPXSP(128, &x[2*256],&w[2*384],y,brev,4,  256,512)           * 
*          fft_SPXSP(128, &x[2*384],&w[2*384],y,brev,4,  384,512)           * 
*                                                                           * 
*          The twiddle factor array is composed of log4(N) sets of twiddle  * 
*          factors, (3/4)*N, (3/16)*N, (3/64)*N, etc.  The index into this  * 
*          array for each stage of the fft is calculated by summing these   * 
*          indices up appropriately.                                        * 
*          For multiple ffts they can share the same table by calling the   * 
*          small ffts from further down in the twiddle factor array. In     * 
*          the same way as the decomposition works for more data reuse.     * 
*                                                                           * 
*          Thus, the above decomposition can be summarized for a general    * 
*          N, radix "rad" as follows:                                       * 
*          fft_SPXSP(N,  &x[0],      &w[0],      y,brev,N/4,0,   N)         * 
*          fft_SPXSP(N/4,&x[0],      &w[2*3*N/4], y,brev,rad,0,   N)        * 
*          fft_SPXSP(N/4,&x[2*N/4],  &w[2*3*N/4], y,brev,rad,N/4, N)        * 
*          fft_SPXSP(N/4,&x[2*N/2],  &w[2*3*N/4], y,brev,rad,N/2, N)        * 
*          fft_SPXSP(N/4,&x[2*3*N/4],&w[2*3*N/4], y,brev,rad,3*N/4, N)      * 
*                                                                           * 
*          As discussed previously, N can be either a power of 4 or 2.      * 
*          If N is a power of 4, then rad = 4, and if N is a power of 2     * 
*          and not a power of 4, then rad = 2. "rad" is used to control     * 
*          how many stages of decomposition are performed. It is also       * 
*          used to determine whether a radix-4 or radix-2 decomposition     * 
*          should be performed at the last stage. Hence when "rad" is set   * 
*          to "N/4" the first stage of the transform alone is performed     * 
*          and the code exits. To complete the FFT, four other calls are    * 
*          required to perform N/4 size FFTs.In fact, the ordering of       * 
*          these 4 FFTs amongst themselves does not matter and hence from   * 
*          a cache perspective, it helps to go through the remaining 4      * 
*          FFTs in exactly the opposite order to the first. This is         * 
*          illustrated as follows:                                          * 
*                                                                           * 
*          fft_SPXSP(N,  &x[0],      &w[0],      y,brev,N/4,0,   N)         * 
*          fft_SPXSP(N/4,&x[2*3*N/4],&w[2*3*N/4],y,brev,rad,3*N/4, N)       * 
*          fft_SPXSP(N/4,&x[2*N/2],  &w[2*3*N/4],y,brev,rad,N/2, N)         * 
*          fft_SPXSP(N/4,&x[2*N/4],  &w[2*3*N/4],y,brev,rad,N/4, N)         * 
*          fft_SPXSP(N/4,&x[0],      &w[2*3*N/4],y,brev,rad,0,   N)         * 
*                                                                           * 
*          In addition this function can be used to minimize call           * 
*          overhead, by completing the FFT with one function call           * 
*          invocation as shown below:                                       * 
*          fft_SPXSP(N,  &x[0],      &w[0],      y, brev, rad, 0,N)         * 
*                                                                           * 
*                                                                           * 
*     TECHNIQUES                                                            * 
*                                                                           * 
*          1. A special sequence of coeffs. used as generated above         * 
*          produces the fft. This collapses the inner 2 loops in the        * 
*          traditional Burrus and Parks implementation Fortran Code.        * 
*                                                                           * 
*          2. The revised FFT uses a redundant sequence of twiddle factors  * 
*          to allow a linear access through the data. This linear access    * 
*          enables data and instruction level parallelism.                  * 
*                                                                           * 
*          3.The data produced by the fft_SPxSP fft is in normal form, the  * 
*          whole data array is written into a new output buffer.            * 
*                                                                           * 
*          4. The fft_SPxSP butterfly is bit reversed, i.e. the inner 2     * 
*          points of the butterfly are crossed over,this has the effect of  * 
*          making the data come out in bit reversed rather than fft_SPxSP   * 
*          digit reversed order. This simplifies the last pass of the loop  * 
*          A simple table is used to do the bit reversal  out of place.     * 
*                                                                           * 
*              unsigned char brev[64] = {                                   * 
*                    0x0, 0x20, 0x10, 0x30, 0x8, 0x28, 0x18, 0x38,          * 
*                    0x4, 0x24, 0x14, 0x34, 0xc, 0x2c, 0x1c, 0x3c,          * 
*                    0x2, 0x22, 0x12, 0x32, 0xa, 0x2a, 0x1a, 0x3a,          * 
*                    0x6, 0x26, 0x16, 0x36, 0xe, 0x2e, 0x1e, 0x3e,          * 
*                    0x1, 0x21, 0x11, 0x31, 0x9, 0x29, 0x19, 0x39,          * 
*                    0x5, 0x25, 0x15, 0x35, 0xd, 0x2d, 0x1d, 0x3d,          * 
*                    0x3, 0x23, 0x13, 0x33, 0xb, 0x2b, 0x1b, 0x3b,          * 
*                    0x7, 0x27, 0x17, 0x37, 0xf, 0x2f, 0x1f, 0x3f           * 
*              };                                                           * 
*                                                                           * 
*     ASSUMPTIONS                                                           * 
*          1. N must be a power of 2 and N >= 8  N <= 16384 points.         * 
*                                                                           * 
*          2. Complex time data x and twiddle facotrs w are aligned on      * 
*          double word boundares. Real values are stored in even word       * 
*          positions and imaginary values in odd positions.                 * 
*                                                                           * 
*          3. All data is in single precision floating point format. The    * 
*          complex frequency data will be returned in linear order.         * 
*                                                                           * 
*                                                                           * 
*     C CODE                                                                * 
*           This is the C equivalent of the assembly code without           * 
*           restrictions: Note that the assembly code is hand optimized     * 
*           and restrictions may apply.                                     * 
*                                                                           * 
*                                                                           * 
*    void fft_SPXSP(int N, float *ptr_x, float *ptr_w, float *ptr_y,        * 
*       unsigned char *brev, int n_min, int offset, int n_max)              * 
*    {                                                                      * 
*            int  i, j, k, l1, l2, h2, predj;                               * 
*            int  tw_offset, stride, fft_jmp;                               * 
*                                                                           * 
*            float x0, x1, x2, x3,x4,x5,x6,x7;                              * 
*            float xt0, yt0, xt1, yt1, xt2, yt2, yt3;                       * 
*            float yt4, yt5, yt6, yt7;                                      * 
*            float si1,si2,si3,co1,co2,co3;                                 * 
*            float xh0,xh1,xh20,xh21,xl0,xl1,xl20,xl21;                     * 
*            float x_0, x_1, x_l1, x_l1p1, x_h2 , x_h2p1, x_l2, x_l2p1;     * 
*            float xl0_0, xl1_0, xl0_1, xl1_1;                              * 
*            float xh0_0, xh1_0, xh0_1, xh1_1;                              * 
*            float *x,*w;                                                   * 
*            int   k0, k1, j0, j1, l0, radix;                               * 
*            float * y0, * ptr_x0, * ptr_x2;                                * 
*                                                                           * 
*            radix = n_min;                                                 * 
*                                                                           * 
*            stride = N; /* N is the number of complex samples */           * 
*            tw_offset = 0;                                                 * 
*            while (stride > radix)                                         * 
*            {                                                              * 
*                j = 0;                                                     * 
*                fft_jmp = stride + (stride>>1);                            * 
*                h2 = stride>>1;                                            * 
*                l1 = stride;                                               * 
*                l2 = stride + (stride>>1);                                 * 
*                x = ptr_x;                                                 * 
*                w = ptr_w + tw_offset;                                     * 
*                                                                           * 
*                for (i = 0; i < N; i += 4)                                 * 
*                {                                                          * 
*                    co1 = w[j];                                            * 
*                    si1 = w[j+1];                                          * 
*                    co2 = w[j+2];                                          * 
*                    si2 = w[j+3];                                          * 
*                    co3 = w[j+4];                                          * 
*                    si3 = w[j+5];                                          * 
*                                                                           * 
*                    x_0    = x[0];                                         * 
*                    x_1    = x[1];                                         * 
*                    x_h2   = x[h2];                                        * 
*                    x_h2p1 = x[h2+1];                                      * 
*                    x_l1   = x[l1];                                        * 
*                    x_l1p1 = x[l1+1];                                      * 
*                    x_l2   = x[l2];                                        * 
*                    x_l2p1 = x[l2+1];                                      * 
*                                                                           * 
*                    xh0  = x_0    + x_l1;                                  * 
*                    xh1  = x_1    + x_l1p1;                                * 
*                    xl0  = x_0    - x_l1;                                  * 
*                    xl1  = x_1    - x_l1p1;                                * 
*                                                                           * 
*                    xh20 = x_h2   + x_l2;                                  * 
*                    xh21 = x_h2p1 + x_l2p1;                                * 
*                    xl20 = x_h2   - x_l2;                                  * 
*                    xl21 = x_h2p1 - x_l2p1;                                * 
*                                                                           * 
*                    ptr_x0 = x;                                            * 
*                    ptr_x0[0] = xh0 + xh20;                                * 
*                    ptr_x0[1] = xh1 + xh21;                                * 
*                                                                           * 
*                    ptr_x2 = ptr_x0;                                       * 
*                    x += 2;                                                * 
*                    j += 6;                                                * 
*                    predj = (j - fft_jmp);                                 * 
*                    if (!predj) x += fft_jmp;                              * 
*                    if (!predj) j = 0;                                     * 
*                                                                           * 
*                    xt0 = xh0 - xh20;                                      * 
*                    yt0 = xh1 - xh21;                                      * 
*                    xt1 = xl0 + xl21;                                      * 
*                    yt2 = xl1 + xl20;                                      * 
*                    xt2 = xl0 - xl21;                                      * 
*                    yt1 = xl1 - xl20;                                      * 
*                                                                           * 
*                    ptr_x2[l1  ] = xt1 * co1 + yt1 * si1;                  * 
*                    ptr_x2[l1+1] = yt1 * co1 - xt1 * si1;                  * 
*                    ptr_x2[h2  ] = xt0 * co2 + yt0 * si2;                  * 
*                    ptr_x2[h2+1] = yt0 * co2 - xt0 * si2;                  * 
*                    ptr_x2[l2  ] = xt2 * co3 + yt2 * si3;                  * 
*                    ptr_x2[l2+1] = yt2 * co3 - xt2 * si3;                  * 
*                }                                                          * 
*                tw_offset += fft_jmp;                                      * 
*                stride = stride>>2;                                        * 
*            }/* end while */                                               * 
*                                                                           * 
*            j = offset>>2;                                                 * 
*                                                                           * 
*            ptr_x0 = ptr_x;                                                * 
*            y0 = ptr_y;                                                    * 
*            /*l0 = _norm(n_max) - 17;    get size of fft */                * 
*            l0=0;                                                          * 
*            for(k=30;k>=0;k--)                                             * 
*                if( (n_max & (1 << k)) == 0 )                              * 
*                  l0++;                                                    * 
*                else                                                       * 
*                   break;                                                  * 
*            l0=l0-17;                                                      * 
*            if (radix <= 4) for (i = 0; i < N; i += 4)                     * 
*            {                                                              * 
*                    /* reversal computation */                             * 
*                                                                           * 
*                    j0 = (j     ) & 0x3F;                                  * 
*                    j1 = (j >> 6);                                         * 
*                    k0 = brev[j0];                                         * 
*                    k1 = brev[j1];                                         * 
*                    k = (k0 << 6) +  k1;                                   * 
*                    k = k >> l0;                                           * 
*                    j++;        /* multiple of 4 index */                  * 
*                                                                           * 
*                    x0   = ptr_x0[0];  x1 = ptr_x0[1];                     * 
*                    x2   = ptr_x0[2];  x3 = ptr_x0[3];                     * 
*                    x4   = ptr_x0[4];  x5 = ptr_x0[5];                     * 
*                    x6   = ptr_x0[6];  x7 = ptr_x0[7];                     * 
*                    ptr_x0 += 8;                                           * 
*                                                                           * 
*                    xh0_0  = x0 + x4;                                      * 
*                    xh1_0  = x1 + x5;                                      * 
*                    xh0_1  = x2 + x6;                                      * 
*                    xh1_1  = x3 + x7;                                      * 
*                                                                           * 
*                    if (radix == 2) {                                      * 
*                      xh0_0 = x0;                                          * 
*                      xh1_0 = x1;                                          * 
*                      xh0_1 = x2;                                          * 
*                      xh1_1 = x3;                                          * 
*                    }                                                      * 
*                                                                           * 
*                    yt0  = xh0_0 + xh0_1;                                  * 
*                    yt1  = xh1_0 + xh1_1;                                  * 
*                    yt4  = xh0_0 - xh0_1;                                  * 
*                    yt5  = xh1_0 - xh1_1;                                  * 
*                                                                           * 
*                    xl0_0  = x0 - x4;                                      * 
*                    xl1_0  = x1 - x5;                                      * 
*                    xl0_1  = x2 - x6;                                      * 
*                    xl1_1  = x3 - x7;                                      * 
*                                                                           * 
*                    if (radix == 2) {                                      * 
*                      xl0_0 = x4;                                          * 
*                      xl1_0 = x5;                                          * 
*                      xl1_1 = x6;                                          * 
*                      xl0_1 = x7;                                          * 
*                    }                                                      * 
*                                                                           * 
*                    yt2  = xl0_0 + xl1_1;                                  * 
*                    yt3  = xl1_0 - xl0_1;                                  * 
*                    yt6  = xl0_0 - xl1_1;                                  * 
*                    yt7  = xl1_0 + xl0_1;                                  * 
*                                                                           * 
*                    if (radix == 2) {                                      * 
*                      yt7  = xl1_0 - xl0_1;                                * 
*                      yt3  = xl1_0 + xl0_1;                                * 
*                    }                                                      * 
*                                                                           * 
*                    y0[k] = yt0; y0[k+1] = yt1;                            * 
*                    k += n_max>>1;                                         * 
*                    y0[k] = yt2; y0[k+1] = yt3;                            * 
*                    k += n_max>>1;                                         * 
*                    y0[k] = yt4; y0[k+1] = yt5;                            * 
*                    k += n_max>>1;                                         * 
*                    y0[k] = yt6; y0[k+1] = yt7;                            * 
*            }                                                              * 
*   }                                                                       * 
*     NOTES                                                                 * 
*                                                                           * 
*      1. Endian: Configuration is LITTLE ENDIAN.                           * 
*                                                                           * 
*      2. Interruptibility: The Code is Interrupt tolerant but not          * 
*         Interruptible.                                                    * 
*                                                                           * 
*     CYCLES                                                                * 
*                                                                           * 
*          cycles= [2.5*N+21]*ceil(log4(N)-1) + 2*N + 45                    * 
*          e.g. N = 256,   cycles = 2540                                    * 
*          e.g. N = 128,   cycles = 1324                                    * 
*                                                                           * 
*     CODESIZE                                                              * 
*          1600 bytes                                                       * 
* ========================================================================= *


		   .asg	A15, FP
		   .asg	B14, DP
		   .asg	B15, SP

           .global  _CPL_fft_asm
           .sect ".text:CPL_fft_asm"
_CPL_fft_asm:
        
	.map	A_xt2/A23
	.map	A_xh20/A24
	.map	A_xh21/A28
	.map	A_yt0/A18
	.map	A_yt0$1/A6
	.map	A_yt0$2/A4
	.map	A_yt0$3/A20
	.map	A_yt0$4/A3
	.map	A_yt1/A10
	.map	A_yt1'/A16
	.map	A_yt1''/A9
	.map	A_yt2/A9
	.map	A_yt2$1/A5
	.map	A_yt2$2/A31
	.map	A_yt2$3/A17
	.map	B_si1/B29
	.map	A_yt4/A20
	.map	A_yt4'/A3
	.map	A_yt4''/A8
	.map	B_si2/B25
	.map	B_si3/B21
	.map	A_yt6/A19
	.map	A_yt6$1/A4
	.map	A_yt6$2/A3
	.map	A_yt6$3/A8
	.map	A_h22/A15
	.map	A_ptr_w/A6
	.map	A_ptr_w'/B6
	.map	B_sum1/B5
	.map	B_sum1'/B9
	.map	B_sum1''/B16
	.map	A_xl20/A6
	.map	A_xl20'/A5
	.map	A_xl20''/A9
	.map	B_sum2/B12
	.map	B_sum2'/B8
	.map	B_sum2''/B9
	.map	A_xl21/A9
	.map	A_xl21'/A17
	.map	A_xl21''/A6
	.map	B_fft_jmp_1/B29
	.map	B_sum3/B16
	.map	B_sum3'/B4
	.map	B_sum3''/B5
	.map	B_sum4/B16
	.map	B_sum4'/B4
	.map	B_sum4''/B5
	.map	A_i/A3
	.map	A_i'/A1
	.map	B_sum5/B19
	.map	B_sum6/B18
	.map	A_j/A28
	.map	A_j'/A14
	.map	A_k/A17
	.map	A_k$1/A18
	.map	A_k$2/A9
	.map	A_k$3/A3
	.map	B_sum7/B16
	.map	B_sum7'/B4
	.map	B_sum7''/B5
	.map	B_xh0/B9
	.map	B_xh0'/B30
	.map	B_xh0''/B6
	.map	B_temp/B6
	.map	B_sum8/B6
	.map	B_sum8$1/B4
	.map	B_sum8$2/B8
	.map	B_sum8$3/B9
	.map	B_xh1/B17
	.map	B_xh1'/B16
	.map	B_xl0/B22
	.map	B_x_h2p/B23
	.map	B_xl1/B11
	.map	B_xl1'/B9
	.map	B_xl1''/B17
	.map	B_i/B1
	.map	B_i'/B4
	.map	B_j/B5
	.map	B_j'/B10
	.map	B_k/B27
	.map	B_k'/B5
	.map	A_w/A3
	.map	A_x/A8
	.map	A_y/A30
	.map	A_brev/A27
	.map	A_brev'/A8
	.map	B_n/B13
	.map	B_n$1/A4
	.map	B_n$2/B4
	.map	B_n$3/A3
	.map	A_p_x0/A26
	.map	A_p_y0/A25
	.map	B_x_l1p/B27
	.map	B_p_y/B26
	.map	B_xt0/B17
	.map	B_xt0'/B4
	.map	B_xt0''/B5
	.map	B_x_l2p/B25
	.map	B_w/B19
	.map	B_xt1/B16
	.map	B_xt1'/B5
	.map	B_xt1''/B6
	.map	B_x/B4
	.map	B_x'/B28
	.map	B_xt2/B6
	.map	B_xt2'/B17
	.map	B_xt2''/B4
	.map	B_y/B7
	.map	A_x_h2p/A17
	.map	B_yt0/B27
	.map	B_fft_jmp/B3
	.map	B_yt1/B21
	.map	B_yt1'/B27
	.map	B_yt1''/B4
	.map	B_yt2/B12
	.map	B_yt2'/B8
	.map	B_yt2''/B5
	.map	B_yt3/B21
	.map	B_yt5/B7
	.map	B_yt5'/B5
	.map	B_yt7/B4
	.map	B_tw_offset/B4
	.map	B_tw_offset'/B5
	.map	A_x_l1p/A23
	.map	B_h22/B2
	.map	A_x_l2p/A21
	.map	A_radix/A3
	.map	A_radix'/A26
	.map	A_x_h2/A16
	.map	B_prod10/B19
	.map	B_prod11/B19
	.map	B_prod12/B16
	.map	B_prod12'/B4
	.map	B_prod12''/B1
	.map	A_x_l1/A22
	.map	A_x_l2/A20
	.map	A_3h22/A13
	.map	A_3h22'/A4
	.map	A_h2/A12
	.map	A_j0/A9
	.map	A_k0/A8
	.map	A_l0/A24
	.map	B_xh20/B6
	.map	B_xh20'/B8
	.map	B_xh20''/B9
	.map	B_xh21/B23
	.map	B_xl20/B5
	.map	B_xl20'/B16
	.map	B_xl20''/B11
	.map	B_h2/B31
	.map	A_prod10/A19
	.map	B_xl21/B26
	.map	A_temp1/A3
	.map	A_temp1$1/A2
	.map	A_temp1$2/A20
	.map	A_temp1$3/A0
	.map	A_prod11/A18
	.map	B_j1/B4
	.map	A_prod12/A3
	.map	A_co1/A24
	.map	B_k1/B20
	.map	A_co2/A18
	.map	B_l0/B6
	.map	B_l0'/B4
	.map	A_x0/A6
	.map	A_x0'/A26
	.map	A_co3/A28
	.map	B_prod1/B6
	.map	B_prod1'/B9
	.map	B_prod1''/B30
	.map	A_x1/A7
	.map	A_x1'/A27
	.map	B_prod2/B9
	.map	B_prod2'/B5
	.map	B_prod3/B19
	.map	A_x4/A4
	.map	B_prod4/B5
	.map	B_prod4'/B16
	.map	A_x5/A5
	.map	B_prod5/B17
	.map	B_prod5'/B4
	.map	A_x0c/A23
	.map	B_prod6/B16
	.map	B_prod6'/B5
	.map	B_prod6''/B6
	.map	B_prod7/B9
	.map	B_prod7'/B22
	.map	B_prod7''/B8
	.map	A_x1c/A22
	.map	B_prod8/B19
	.map	A_fft_jmp/A11
	.map	B_prod9/B18
	.map	B_outjmp/B19
	.map	A_sum1/A7
	.map	B_brev/B18
	.map	B_brev'/A8
	.map	A_sum2/A0
	.map	A_sum2'/A3
	.map	A_sum2''/A10
	.map	A_sum3/A3
	.map	A_sum4/A3
	.map	A_sum5/A22
	.map	B_p_x0/B17
	.map	A_sum6/A6
	.map	A_sum6'/A3
	.map	A_sum6''/A9
	.map	B_predj/B0
	.map	A_sum7/A3
	.map	A_sum7'/A5
	.map	B_p_y0/B16
	.map	A_sum8/A3
	.map	A_sum8'/A6
	.map	A_sum8''/A5
	.map	A_prod1/A23
	.map	A_prod2/A22
	.map	A_prod3/A21
	.map	B_w0/B30
	.map	A_prod4/A3
	.map	A_prod5/A9
	.map	A_prod5'/A3
	.map	B_x0/B18
	.map	A_prod6/A22
	.map	B_x1/B19
	.map	B_x2/B24
	.map	A_prod7/A6
	.map	A_prod7'/A5
	.map	B_x3/B25
	.map	A_prod8/A16
	.map	A_prod9/A3
	.map	A_prod9'/A4
	.map	A_prod9''/A5
	.map	B_x6/B22
	.map	B_x7/B23
	.map	B_radix2/B0
	.map	B_radix2'/B4
	.map	A_predj/A1
	.map	B_co1/B28
	.map	B_co2/B24
	.map	A_outjmp/A21
	.map	B_co3/B20
	.map	B_2h2/B8
	.map	A_si1/A25
	.map	A_si2/A19
	.map	B_x_h2/B22
	.map	A_si3/A29
	.map	B_x2c/B9
	.map	B_x_l1/B26
	.map	B_x3c/B8
	.map	B_x_l2/B24
	.map	A_xh0/A7
	.map	A_xh0'/A4
	.map	A_xh1/A5
	.map	A_xh1'/A6
	.map	B_stride/B13
	.map	A_xl0/A4
	.map	A_xl0'/A8
	.map	A_xl1/A20
	.map	B_while/B0
	.map	B_n_max/B10
	.map	B_n_max'/B4
	.map	A_p_y/A16
	.map	B_ptr_x/B4
	.map	B_ptr_x'/B17
	.map	B_ptr_x''/A3
	.map	A_xt0/A31
	.map	A_xt0'/A5
	.map	A_xt0''/A3
	.map	B_n_min/B8
	.map	B_n_min'/B5
	.map	B_ptr_y/B6
	.map	B_ptr_y'/A25
	.map	A_xt1/A3
	.map	A_xt1'/A4
	.map	A_xt1''/A2

;** --------------------------------------------------------------------------*
;               .include "fft_SPXSP_p.h67"
;
;       fft_SPXSP_sa(N,&ptr_x[0],&ptr_w[0],ptr_y_sa,brev,rad,0,N);
;fft_SPXSP_sa   .cproc  N, ptr_x, ptr_w, ptr_y, brev, n_min, offset, n_max
; _fft_SPXSP_sa   .cproc  A_n, B_ptr_x, A_ptr_w, B_ptr_y, A_brev, B_n_min, A_offset, B_n_max
;           .no_mdep
;           .reg      B_w0
;           .reg      A_j, A_w, A_x, A_y, A_h2, A_3h22,A_h22, A_fft_jmp, A_predj, B_fft_jmp_1
;           .reg      B_j, B_w, B_x, B_y, B_h2, B_2h2, B_h22, B_fft_jmp, B_predj, B_n
;           .reg      A_si1,A_co1, A_si2,A_co2, A_si3,A_co3
;           .reg      B_si1,B_co1, B_si2,B_co2, B_si3,B_co3
;           .reg      A_x1,A_x0, A_x_h2p,A_x_h2, A_x_l1p,A_x_l1, A_x_l2p,A_x_l2
;           .reg      A_xh0,A_xh1,A_xl0,A_xl1, A_xh20,A_xh21,A_xl20,A_xl21
;           .reg      B_x1,B_x0, B_x_h2p,B_x_h2, B_x_l1p,B_x_l1, B_x_l2p,B_x_l2
;           .reg      B_xh0,B_xh1,B_xl0,B_xl1, B_xh20,B_xh21,B_xl20,B_xl21
;           .reg      A_sum1, A_sum2
;           .reg      A_sum3, A_sum4, A_xt1, A_yt1
;           .reg      A_prod1,A_prod2,A_prod3,A_prod4
;           .reg      A_sum5, A_sum6, A_xt0, A_yt0
;           .reg      A_prod5,A_prod6,A_prod7,A_prod8
;           .reg      A_sum7, A_sum8, A_xt2, A_yt2
;           .reg      A_prod9,A_prod10,A_prod11,A_prod12
;           .reg      B_sum1, B_sum2
;           .reg      B_sum3, B_sum4,  B_xt1,   B_yt1
;           .reg      B_prod1,B_prod2, B_prod3, B_prod4
;           .reg      B_sum5, B_sum6,  B_xt0,   B_yt0
;           .reg      B_prod5,B_prod6, B_prod7, B_prod8
;           .reg      B_sum7, B_sum8,  B_xt2,   B_yt2
;           .reg      B_prod9,B_prod10,B_prod11,B_prod12
;           .reg      A_radix,  A_i 
;           .reg      B_radix2, B_stride, B_tw_offset, B_i, B_while, B_temp
;           .reg      A_tw_offset, A_r2, A_j0, A_k0, A_temp1, A_k, A_l0
;           .reg      B_brev, B_l0, B_j1, B_k1, B_k
;           .reg      A_p_x0, A_p_y0, A_p_y, A_outjmp
;           .reg      B_p_x0, B_p_y0, B_p_y, B_outjmp
;           .reg      B_x2, B_x3, B_x6, B_x7, B_yt3, B_yt5, B_yt7
;           .reg      A_x4, A_x5,                    A_yt4, A_yt6
;           .reg      A_xh0_0, A_xh1_0, A_xl0_0, A_xl1_0
;           .reg      B_xl0_1, B_xl1_1, B_xh0_1, B_xh1_1
;           .reg      A_x0c, A_x1c
;           .reg      B_x2c, B_x3c

           STW     .D2T1   A11,*SP--(88)     ; |431| 
||         MV      .L1X    SP,A31            ; |431| 

           STW     .D2T2   B13,*+SP(76)      ; |431| 
||         STW     .D1T1   A_ptr_w,*-A31(76)  ; |431| 

           STW     .D2T2   B_ptr_x,*+SP(8)   ; |431|
||         STW     .D1T1   A_brev',*-A31(64)  ; |431| 
||         MVK     .S1     0x4,A_radix       ; |474| 

           STW     .D2T2   B11,*+SP(68)      ; |431| 
||         STW     .D1T1   A_radix,*-A31(52) ; |477| 

           STW     .D2T2   B12,*+SP(72)      ; |431| 
           STW     .D2T1   B_n$1,*+SP(4)     ; |431| 

           STW     .D1T1   A13,*-A31(36)     ; |431| 
||         STW     .D2T2   B_ptr_y,*+SP(16)  ; |431| 

           STW     .D1T1   A14,*-A31(32)     ; |431| 
||         MV      .L2X    B_n$1,B_n         ; |431| 
||         STW     .D2T2   B_n_min,*+SP(28)  ; |431| 

           STW     .D1T1   A15,*-A31(28)     ; |431| 
||         NORM    .L2     B_n,B_radix2'     ; |476| 
||         STW     .D2T2   B_n_max,*+SP(32)  ; |431| 

           STW     .D1T1   A10,*-A31(4)      ; |431| 
||         ZERO    .L2     B_tw_offset       ; |483| 
||         AND     .S2     0x1,B_radix2',B_radix2 ; |477| 
||         STW     .D2T2   B3,*+SP(80)       ; |431| 

           STW     .D2T2   B_tw_offset,*+SP(40) ; |478| 
|| [ B_radix2] MVK .S1     0x2,A_radix       ; |478| 
||         STW     .D1T1   A12,*-A31(40)     ; |431| 

   [ B_radix2] STW .D1T1   A_radix,*-A31(52) ; |478| 
||         STW     .D2T2   B10,*+SP(64)      ; |431| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP $C$L1
;** --------------------------------------------------------------------------*
$C$L1:    
$C$DW$L$_fft_SPXSP_sa$2$B:
; ILOOP: .trip 8
           LDW     .D2T1   *+SP(8),B_ptr_x'' ; |496| 
           SHRU    .S2     B_stride,0x2,B_h2 ; |492| 
           MV      .L1X    B_h2,A_h2         ; |493| 
           LDW     .D2T2   *+SP(4),B_n$2     ; |493| 
           LDW     .D2T2   *+SP(40),B_tw_offset' ; |496| 
           ADD     .L1     0xfffffff0,B_ptr_x'',A_x ; |500| 
           LDDW    .D1T1   *++A_x(16),A_x1':A_x0' ; |524| (P) <0,0> Load x1, x0
           LDDW    .D1T1   *++A_x[A_h2],A_x_h2p:A_x_h2 ; |525| (P) <0,1> Load x_h2p, x_h2  

           LDDW    .D1T1   *++A_x[A_h2],A_x_l1p:A_x_l1 ; |526| (P) <0,3> Load x_l1p, x_l1  
||         MPYSU   .M2     0x6,B_stride,B_fft_jmp ; |489| 

           ADD     .L2X    0xfffffff0,B_ptr_x'',B_x' ; |500| 
||         LDW     .D2T2   *+SP(12),A_ptr_w' ; |496| 

           SHRU    .S1X    B_fft_jmp,0x3,A_fft_jmp ; |490| 
||         LDDW    .D2T2   *++B_x'(24),B_x1:B_x0 ; |529| (P) <0,7> Load x1, x0  
||         MVC     .S2     CSR,B30
||         ZERO    .L1     A_j'              ; |487| 

           LDDW    .D1T1   *+A_x[A_h2],A_x_l2p:A_x_l2 ; |527| (P) <0,9> Load x_l2p, x_l2          
||         LDDW    .D2T2   *++B_x'[B_h2],B_x_h2p:B_x_h2 ; |530| (P) <0,9> Load x_h2p, x_h2  
||         SHL     .S1     A_h2,0x1,A_h22    ; |495| 
||         ADD     .L1     0x6,A_j',A_j'     ; |555| (P) <0,2> j += 6
||         SHL     .S2     B_h2,0x4,B_2h2    ; |552| (P) <0,1> 

           SUB     .L1X    A_x,B_2h2,A_x     ; |553| (P) <0,15> 
||         LDDW    .D2T2   *++B_x'[B_h2],B_x_l1p:B_x_l1 ; |531| (P) <0,10> Load x_l1p, x_l1  
||         ADD     .D1     0xfffffff0,B_ptr_x'',A_y ; |501| 
||         SHL     .S1     A_h22,0x1,A_3h22' ; |497| 
||         SHRU    .S2     B_n$2,0x3,B_i'    ; |509| 

           ADDSP   .S1     A_x1',A_x_l1p,A_xh1 ; |535| (P) <0,12> xh1 = x1 + x_l1p
||         ADDSP   .L1     A_x0',A_x_l1,A_xh0 ; |534| (P) <0,16> xh0 = x0 + x_l1
||         AND     .L2     -2,B30,B4
||         SUB     .D2     B_i',4,B_i
||         SUB     .D1     A_j',A_fft_jmp,A_predj ; |556| (P) <0,14> predj = j - fft_jmp
||         SHRU    .S2     B_stride,0x2,B_stride ; |507| 

   [!A_predj] ADD  .S1X    A_x,B_fft_jmp,A_x ; |557| (P) <0,16> *x = *x + fft_jmp
||         SUBSP   .L1     A_x0',A_x_l1,A_xl0 ; |536| (P) <0,14> xl0 = x0 - x_l1
||         LDDW    .D2T2   *+B_x'[B_h2],B_x_l2p:B_x_l2 ; |532| (P) <0,16> Load x_l2p, x_l2
||         MVC     .S2     B4,CSR            ; interrupts off
||         ADD     .D1     A_h22,A_3h22',A_3h22 ; |498| 
||         ZERO    .L2     B_j'              ; |488| 

$C$DW$L$_fft_SPXSP_sa$2$E:
$C$L2:    ; PIPED LOOP PROLOG

           STW     .D2T2   B30,*+SP(44)
||         SHRU    .S2     B_fft_jmp,0x1,B_fft_jmp_1 ; |505| 
|| [!A_predj] ZERO .S1     A_j'              ; |558| (P) <0,17> j=0
||         LDDW    .D1T1   *++A_x(16),A_x1':A_x0' ; |524| (P) <1,0> Load x1, x0
||         MV      .L2X    A_x,B_x           ; |523| (P) <1,0> x_copy =x
||         SUBSP   .L1     A_x1',A_x_l1p,A_xl1 ; |537| (P) <0,17> xl1 = x1 - x_l1p

           ADDAH   .D2     A_ptr_w',B_tw_offset',B_w0 ; |503| 
||         ADD     .L2     B_tw_offset',B_fft_jmp_1,B_tw_offset' ; |506| 
||         ADDSP   .S1     A_x_h2,A_x_l2,A_xh20 ; |538| (P) <0,18> xh20 = x_h2 + x_l2
||         SHL     .S2     B_h2,0x4,B_2h2    ; |552| (P) <1,1> 
||         LDDW    .D1T1   *++A_x[A_h2],A_x_h2p:A_x_h2 ; |525| (P) <1,1> Load x_h2p, x_h2  
||         ADDSP   .L1     A_x_h2p,A_x_l2p,A_xh21 ; |539| (P) <0,18> xh21 = x_h2p + x_l2p

           SUBSP   .L1     A_x_h2,A_x_l2,A_xl20 ; |540| (P) <0,19> xl20 = x_h2 - x_l2
||         ADDAD   .D2     B_w0,B_j',B_w     ; |513| (P) <0,19>  ^ 
||         ADDSP   .S2     B_x1,B_x_l1p,B_xh1 ; |544| (P) <0,19> xh1 = x1 + x_l1p
||         SUBSP   .L2     B_x1,B_x_l1p,B_xl1 ; |546| (P) <0,19> xl1 = x1 - x_l1p
||         ADD     .D1     0x6,A_j',A_j'     ; |555| (P) <1,2> j += 6
||         SUBSP   .S1     A_x_h2p,A_x_l2p,A_xl21 ; |541| (P) <0,19> xl21 = x_h2p - x_l2p

           STW     .D2T2   B_tw_offset',*+SP(40) ; |498| 
||         ADD     .S2     0x6,B_j',B_j'     ; |645| (P) <0,20> 
||         MV      .S1X    B_w,A_w           ; |514| (P) <0,20>  ^ 
||         LDDW    .D1T1   *++A_x[A_h2],A_x_l1p:A_x_l1 ; |526| (P) <1,3> Load x_l1p, x_l1  
||         SUBSP   .L2     B_x0,B_x_l1,B_xl0 ; |545| (P) <0,20> xl0 = x0 - x_l1

           ADDSP   .S2     B_x_h2p,B_x_l2p,B_xh21 ; |548| (P) <0,21> xh21 = x_h2p + x_l2p
||         LDDW    .D1T1   *A_w,A_si1:A_co1  ; |516| (P) <0,21> Load si1, co1
||         LDDW    .D2T2   *+B_w(40),B_si3:B_co3 ; |521| (P) <0,21> Load si3, co3

           ADDSP   .L1     A_xh0,A_xh20,A_sum1 ; |563| (P) <0,22> temp1 = xh0 + xh20
||         SUBSP   .S1     A_xh1,A_xh21,A_yt0$1 ; |569| (P) <0,22> yt0 = xh1 - xh21
||         LDDW    .D1T1   *+A_w(16),A_si3:A_co3 ; |518| (P) <0,22> Load si3, co3
||         ADDSP   .S2     B_x_h2,B_x_l2,B_xh20 ; |547| (P) <0,22> xh20 = x_h2 + x_l2
||         ADDSP   .L2     B_x0,B_x_l1,B_xh0 ; |543| (P) <0,22> xh0 = x0 + x_l1
||         LDDW    .D2T2   *+B_w(24),B_si1:B_co1 ; |519| (P) <0,22> Load si1, co1

           SUBSP   .L2     B_x_h2,B_x_l2,B_xl20 ; |549| (P) <0,23> xl20 = x_h2 - x_l2
||         SUBSP   .S2     B_x_h2p,B_x_l2p,B_xl21 ; |550| (P) <0,23> xl21 = x_h2p - x_l2p
||         LDDW    .D2T2   *+B_w(32),B_si2:B_co2 ; |520| (P) <0,23> Load si2, co2
||         LDDW    .D1T1   *+A_w(8),A_si2:A_co2 ; |517| (P) <0,23>  ^ Load si2, co2
||         SUBSP   .S1     A_xl1,A_xl20,A_yt1 ; |581| (P) <0,23> yt1 = xl1 - xl20

           LDDW    .D2T2   *++B_x(24),B_x1:B_x0 ; |529| (P) <1,7> Load x1, x0  
||         ADDSP   .S1     A_xl1,A_xl20,A_yt2$1 ; |593| (P) <0,24> yt2 = xl1 + xl20

           SUBSP   .S1     A_xh0,A_xh20,A_xt0 ; |568| (P) <0,25> xt0 = xh0 - xh20
||         SUBSP   .S2     B_xh1,B_xh21,B_yt0 ; |609| (P) <0,25> yt0 = xh1 - xh21

           LDDW    .D1T1   *+A_x[A_h2],A_x_l2p:A_x_l2 ; |527| (P) <1,9> Load x_l2p, x_l2          
||         LDDW    .D2T2   *++B_x[B_h2],B_x_h2p:B_x_h2 ; |530| (P) <1,9> Load x_h2p, x_h2  
||         ADDSP   .L1     A_xh1,A_xh21,A_sum2 ; |564| (P) <0,26> temp2 = xh1 + xh21

           LDDW    .D2T2   *++B_x[B_h2],B_x_l1p:B_x_l1 ; |531| (P) <1,10> Load x_l1p, x_l1  
||         SUBSP   .S2     B_xh0,B_xh20,B_xt0 ; |608| (P) <0,27> xt0 = xh0 - xh20

           ADDSP   .L1     A_xl0,A_xl21,A_xt1 ; |580| (P) <0,28> xt1 = xl0 + xl21
||         MPYSP   .M1     A_yt0$1,A_co2,A_prod7 ; |572| (P) <0,28> prod3 = yt0 * co2 
||         ADDSP   .S2     B_xl0,B_xl21,B_xt1 ; |619| (P) <0,28> xt1 = xl0 + xl21

           SHL     .S2     B_h2,0x1,B_h22    ; |496| 
||         MPYSP   .M1     A_yt2$1,A_co3,A_prod11 ; |596| (P) <0,29> prod3 = yt2 * co3 
||         ADDSP   .L2     B_xh1,B_xh21,B_sum2 ; |604| (P) <0,29> temp2 = xh1 + xh21
||         ADDSP   .S1     A_x1',A_x_l1p,A_xh1 ; |535| (P) <1,12> xh1 = x1 + x_l1p
||         MPYSP   .M2     B_yt0,B_co2,B_prod7 ; |612| (P) <0,29> prod3 = yt0 * co2 

           MPYSP   .M1     A_yt0$1,A_si2,A_prod6 ; |571| (P) <0,30>  ^ prod2 = yt0 * si2
||         SUBSP   .S2     B_xl0,B_xl21,B_xt2 ; |631| (P) <0,30> xt2 = xl0 - xl21
||         MPYSP   .M2     B_yt0,B_si2,B_prod6 ; |611| (P) <0,30> prod2 = yt0 * si2
||         SHL     .S1     A_3h22,0x2,A_temp1$1 ; |642| (P) <0,30> 

           MPYSP   .M1     A_xt0,A_co2,A_prod5 ; |570| (P) <0,31>  ^ prod1 = xt0 * co2 
||         SUBSP   .L1     A_x0',A_x_l1,A_xl0 ; |536| (P) <1,14> xl0 = x0 - x_l1
||         SUB     .S1     A_j',A_fft_jmp,A_predj ; |556| (P) <1,14> predj = j - fft_jmp
||         ADDSP   .L2     B_xh0,B_xh20,B_sum1 ; |603| (P) <0,31> temp1 = xh0 + xh20

           SUBSP   .S1     A_xl0,A_xl21,A_xt2 ; |592| (P) <0,32> xt2 = xl0 - xl21
||         STW     .D1T1   A_sum1,*++A_y(16) ; |565| (P) <0,32> ptr_x0[0] = temp1
||         MPYSP   .M2     B_xt1,B_si1,B_prod4 ; |624| (P) <0,32> prod4 = xt1 * si1
||         MV      .S2X    A_y,B_y           ; |561| (P) <0,32> 
||         SUB     .L1X    A_x,B_2h2,A_x     ; |553| (P) <1,15> 
||         MPYSP   .M1     A_yt2$1,A_si3,A_prod10 ; |595| (P) <0,32> prod2 = yt2 * si3

           MPYSP   .M1     A_xt0,A_si2,A_prod8 ; |573| (P) <0,33> prod4 = xt0 * si2
||         MPYSP   .M2     B_xt1,B_co1,B_prod1 ; |621| (P) <0,33> prod1 = xt1 * co1
||         SUBSP   .S2     B_xl1,B_xl20,B_yt1' ; |620| (P) <0,33> yt1 = xl1 - xl20
||         SUB     .L2X    B_j',A_fft_jmp,B_predj ; |646| (P) <0,33>  ^ 
||         ADDSP   .L1     A_x0',A_x_l1,A_xh0 ; |534| (P) <1,16> xh0 = x0 + x_l1
|| [!A_predj] ADD  .S1X    A_x,B_fft_jmp,A_x ; |557| (P) <1,16> *x = *x + fft_jmp
||         LDDW    .D2T2   *+B_x[B_h2],B_x_l2p:B_x_l2 ; |532| (P) <1,16> Load x_l2p, x_l2
||         STW     .D1T1   A_sum2,*+A_y(4)   ; |566| (P) <0,33> ptr_x0[1] = temp2

           MPYSP   .M1     A_yt1,A_co1,A_prod3 ; |584| (P) <0,34> prod3 = yt1 * co1 
||         ADDSP   .L2     B_xl1,B_xl20,B_yt2 ; |632| (P) <0,34> yt2 = xl1 + xl20
||         SUBSP   .L1     A_x1',A_x_l1p,A_xl1 ; |537| (P) <1,17> xl1 = x1 - x_l1p
|| [!A_predj] ZERO .S1     A_j'              ; |558| (P) <1,17> j=0
||         LDDW    .D1T1   *++A_x(16),A_x1':A_x0' ; |524| (P) <2,0> Load x1, x0
||         MV      .S2X    A_x,B_x           ; |523| (P) <2,0> x_copy =x
||         MPYSP   .M2     B_xt0,B_co2,B_prod5 ; |610| (P) <0,34> prod1 = xt0 * co2 

;** --------------------------------------------------------------------------*
$C$L3:    ; PIPED LOOP KERNEL
$C$DW$L$_fft_SPXSP_sa$4$B:

           MPYSP   .M1     A_yt1,A_si1,A_prod2 ; |583| <0,35> prod2 = yt1 * si1
||         MPYSP   .M2     B_xt2,B_si3,B_prod12 ; |636| <0,35> prod4 = xt2 * si3
|| [!B_predj] ZERO .L2     B_j'              ; |648| <0,35>  ^ 
||         STW     .D2T2   B_sum1,*++B_y(24) ; |605| <0,35> ptr_x0[0] = temp1
||         ADDSP   .S1     A_x_h2,A_x_l2,A_xh20 ; |538| <1,18> xh20 = x_h2 + x_l2
||         ADDSP   .L1     A_x_h2p,A_x_l2p,A_xh21 ; |539| <1,18> xh21 = x_h2p + x_l2p
||         SHL     .S2     B_h2,0x4,B_2h2    ; |552| <2,1> 
||         LDDW    .D1T1   *++A_x[A_h2],A_x_h2p:A_x_h2 ; |525| <2,1> Load x_h2p, x_h2  

           MPYSP   .M1     A_xt1,A_co1,A_prod1 ; |582| <0,36> prod1 = xt1 * co1
||         MPYSP   .M2     B_xt2,B_co3,B_prod9 ; |633| <0,36> prod1 = xt2 * co3 
||         SUBSP   .S1     A_x_h2p,A_x_l2p,A_xl21 ; |541| <1,19> xl21 = x_h2p - x_l2p
||         SUBSP   .L1     A_x_h2,A_x_l2,A_xl20 ; |540| <1,19> xl20 = x_h2 - x_l2
||         ADDAD   .D2     B_w0,B_j',B_w     ; |513| <1,19>  ^ 
||         ADDSP   .S2     B_x1,B_x_l1p,B_xh1 ; |544| <1,19> xh1 = x1 + x_l1p
||         SUBSP   .L2     B_x1,B_x_l1p,B_xl1 ; |546| <1,19> xl1 = x1 - x_l1p
||         ADD     .D1     0x6,A_j',A_j'     ; |555| <2,2> j += 6

           MPYSP   .M1     A_xt1,A_si1,A_prod4 ; |585| <0,37> prod4 = xt1 * si1
||         MPYSP   .M2     B_xt0,B_si2,B_prod8 ; |613| <0,37> prod4 = xt0 * si2
||         ADDSP   .L1     A_prod5,A_prod6,A_sum5 ; |574| <0,37>  ^ sum1 = prod1 + prod2
||         STW     .D2T2   B_sum2,*+B_y(4)   ; |606| <0,37> ptr_x0[1] = temp2
||         ADD     .S2     0x6,B_j',B_j'     ; |645| <1,20> 
||         SUBSP   .L2     B_x0,B_x_l1,B_xl0 ; |545| <1,20> xl0 = x0 - x_l1
||         MV      .S1X    B_w,A_w           ; |514| <1,20>  ^ 
||         LDDW    .D1T1   *++A_x[A_h2],A_x_l1p:A_x_l1 ; |526| <2,3> Load x_l1p, x_l1  

           MPYSP   .M1     A_xt2,A_co3,A_prod9 ; |594| <0,38> prod1 = xt2 * co3 
||         MPYSP   .M2     B_yt1',B_si1,B_prod2 ; |622| <0,38> prod2 = yt1 * si1
||         ADDSP   .L2     B_prod5,B_prod6,B_sum5 ; |614| <0,38> sum1 = prod1 + prod2
||         SUBSP   .S1     A_prod7,A_prod8,A_sum6 ; |575| <0,38> sum2 = prod3 - prod4
||         ADDSP   .S2     B_x_h2p,B_x_l2p,B_xh21 ; |548| <1,21> xh21 = x_h2p + x_l2p
||         LDDW    .D2T2   *+B_w(40),B_si3:B_co3 ; |521| <1,21> Load si3, co3
||         LDDW    .D1T1   *A_w,A_si1:A_co1  ; |516| <1,21> Load si1, co1

           MPYSP   .M1     A_xt2,A_si3,A_prod12 ; |597| <0,39> prod4 = xt2 * si3
||         MPYSP   .M2     B_yt1',B_co1,B_prod3 ; |623| <0,39> prod3 = yt1 * co1 
||         ADDSP   .L1     A_xh0,A_xh20,A_sum1 ; |563| <1,22> temp1 = xh0 + xh20
||         LDDW    .D2T2   *+B_w(24),B_si1:B_co1 ; |519| <1,22> Load si1, co1
||         SUBSP   .S1     A_xh1,A_xh21,A_yt0$1 ; |569| <1,22> yt0 = xh1 - xh21
||         LDDW    .D1T1   *+A_w(16),A_si3:A_co3 ; |518| <1,22> Load si3, co3
||         ADDSP   .S2     B_x_h2,B_x_l2,B_xh20 ; |547| <1,22> xh20 = x_h2 + x_l2
||         ADDSP   .L2     B_x0,B_x_l1,B_xh0 ; |543| <1,22> xh0 = x0 + x_l1

           ADDSP   .L1     A_prod1,A_prod2,A_sum3 ; |586| <0,40> sum1 = prod1 + prod2
||         MPYSP   .M2     B_yt2,B_si3,B_prod10 ; |634| <0,40> prod2 = yt2 * si3
||         SUBSP   .S1     A_xl1,A_xl20,A_yt1 ; |581| <1,23> yt1 = xl1 - xl20
||         SUBSP   .L2     B_x_h2,B_x_l2,B_xl20 ; |549| <1,23> xl20 = x_h2 - x_l2
||         SUBSP   .S2     B_x_h2p,B_x_l2p,B_xl21 ; |550| <1,23> xl21 = x_h2p - x_l2p
||         LDDW    .D2T2   *+B_w(32),B_si2:B_co2 ; |520| <1,23> Load si2, co2
||         LDDW    .D1T1   *+A_w(8),A_si2:A_co2 ; |517| <1,23>  ^ Load si2, co2

           SUBSP   .L1     A_prod3,A_prod4,A_sum4 ; |587| <0,41> sum2 = prod3 - prod4
||         MPYSP   .M2     B_yt2,B_co3,B_prod11 ; |635| <0,41> prod3 = yt2 * co3 
||         SUBSP   .L2     B_prod7,B_prod8,B_sum6 ; |615| <0,41> sum2 = prod3 - prod4
||         STW     .D1T1   A_sum5,*++A_y[A_h22] ; |576| <0,41> ptr_x2[l1] = sum1 
||         ADDSP   .S1     A_xl1,A_xl20,A_yt2$1 ; |593| <1,24> yt2 = xl1 + xl20
||         LDDW    .D2T2   *++B_x(24),B_x1:B_x0 ; |529| <2,7> Load x1, x0  

           ADDSP   .L1     A_prod9,A_prod10,A_sum7 ; |598| <0,42> sum1 = prod1 + prod2
||         ADDSP   .L2     B_prod1,B_prod2,B_sum3 ; |625| <0,42> sum1 = prod1 + prod2
||         STW     .D2T2   B_sum5,*++B_y[B_h22] ; |616| <0,42> ptr_x2[l1] = sum1 
||         STW     .D1T1   A_sum6,*+A_y(4)   ; |577| <0,42> ptr_x2[l1+1] = sum2 
||         SUBSP   .S1     A_xh0,A_xh20,A_xt0 ; |568| <1,25> xt0 = xh0 - xh20
||         SUBSP   .S2     B_xh1,B_xh21,B_yt0 ; |609| <1,25> yt0 = xh1 - xh21

           SUBSP   .S1     A_prod11,A_prod12,A_sum8 ; |599| <0,43> sum2 = prod3 - prod4
||         SUBSP   .L2     B_prod3,B_prod4,B_sum4 ; |626| <0,43> sum2 = prod3 - prod4
||         ADDSP   .L1     A_xh1,A_xh21,A_sum2 ; |564| <1,26> temp2 = xh1 + xh21
||         LDDW    .D1T1   *+A_x[A_h2],A_x_l2p:A_x_l2 ; |527| <2,9> Load x_l2p, x_l2          
||         LDDW    .D2T2   *++B_x[B_h2],B_x_h2p:B_x_h2 ; |530| <2,9> Load x_h2p, x_h2  

           STW     .D1T1   A_sum3,*++A_y[A_h22] ; |588| <0,44> ptr_x2[l1] = sum1 
||         ADDSP   .L2     B_prod9,B_prod10,B_sum7 ; |637| <0,44> sum1 = prod1 + prod2
||         SUBSP   .S2     B_xh0,B_xh20,B_xt0 ; |608| <1,27> xt0 = xh0 - xh20
||         LDDW    .D2T2   *++B_x[B_h2],B_x_l1p:B_x_l1 ; |531| <2,10> Load x_l1p, x_l1  

           STW     .D1T1   A_sum4,*+A_y(4)   ; |589| <0,45> ptr_x2[l1+1] = sum2 
||         SUBSP   .L2     B_prod11,B_prod12,B_sum8 ; |638| <0,45> sum2 = prod3 - prod4
||         STW     .D2T2   B_sum6,*+B_y(4)   ; |617| <0,45> ptr_x2[l1+1] = sum2 
||         ADDSP   .L1     A_xl0,A_xl21,A_xt1 ; |580| <1,28> xt1 = xl0 + xl21
||         ADDSP   .S2     B_xl0,B_xl21,B_xt1 ; |619| <1,28> xt1 = xl0 + xl21
||         MPYSP   .M1     A_yt0$1,A_co2,A_prod7 ; |572| <1,28> prod3 = yt0 * co2 

           STW     .D1T1   A_sum7,*++A_y[A_h22] ; |600| <0,46> ptr_x2[l1] = sum1 
|| [ B_i]  B       .S2     $C$L3             ; |653| <0,46> Branch to innerloop
||         STW     .D2T2   B_sum3,*++B_y[B_h22] ; |627| <0,46> ptr_x2[l1] = sum1 
||         MPYSP   .M1     A_yt2$1,A_co3,A_prod11 ; |596| <1,29> prod3 = yt2 * co3 
||         MPYSP   .M2     B_yt0,B_co2,B_prod7 ; |612| <1,29> prod3 = yt0 * co2 
||         ADDSP   .L2     B_xh1,B_xh21,B_sum2 ; |604| <1,29> temp2 = xh1 + xh21
||         ADDSP   .S1     A_x1',A_x_l1p,A_xh1 ; |535| <2,12> xh1 = x1 + x_l1p

           SUB     .L1     A_y,A_temp1$1,A_y ; |643| <0,47> 
||         STW     .D1T1   A_sum8,*+A_y(4)   ; |601| <0,47> ptr_x2[l1+1] = sum2 
||         STW     .D2T2   B_sum4,*+B_y(4)   ; |628| <0,47> ptr_x2[l1+1] = sum2 
||         SHL     .S1     A_3h22,0x2,A_temp1$1 ; |642| <1,30> 
||         MPYSP   .M1     A_yt0$1,A_si2,A_prod6 ; |571| <1,30>  ^ prod2 = yt0 * si2
||         SUBSP   .S2     B_xl0,B_xl21,B_xt2 ; |631| <1,30> xt2 = xl0 - xl21
||         MPYSP   .M2     B_yt0,B_si2,B_prod6 ; |611| <1,30> prod2 = yt0 * si2

   [!B_predj] ADD  .S1X    A_y,B_fft_jmp,A_y ; |647| <0,48> 
||         STW     .D2T2   B_sum7,*++B_y[B_h22] ; |639| <0,48> ptr_x2[l1] = sum1 
||         MPYSP   .M1     A_xt0,A_co2,A_prod5 ; |570| <1,31>  ^ prod1 = xt0 * co2 
||         ADDSP   .L2     B_xh0,B_xh20,B_sum1 ; |603| <1,31> temp1 = xh0 + xh20
||         SUBSP   .L1     A_x0',A_x_l1,A_xl0 ; |536| <2,14> xl0 = x0 - x_l1
||         SUB     .D1     A_j',A_fft_jmp,A_predj ; |556| <2,14> predj = j - fft_jmp

           STW     .D2T2   B_sum8,*+B_y(4)   ; |640| <0,49> ptr_x2[l1+1] = sum2 
||         SUBSP   .S1     A_xl0,A_xl21,A_xt2 ; |592| <1,32> xt2 = xl0 - xl21
||         MPYSP   .M1     A_yt2$1,A_si3,A_prod10 ; |595| <1,32> prod2 = yt2 * si3
||         STW     .D1T1   A_sum1,*++A_y(16) ; |565| <1,32> ptr_x0[0] = temp1
||         MPYSP   .M2     B_xt1,B_si1,B_prod4 ; |624| <1,32> prod4 = xt1 * si1
||         MV      .S2X    A_y,B_y           ; |561| <1,32> 
||         SUB     .L1X    A_x,B_2h2,A_x     ; |553| <2,15> 

           MPYSP   .M1     A_xt0,A_si2,A_prod8 ; |573| <1,33> prod4 = xt0 * si2
||         STW     .D1T1   A_sum2,*+A_y(4)   ; |566| <1,33> ptr_x0[1] = temp2
||         MPYSP   .M2     B_xt1,B_co1,B_prod1 ; |621| <1,33> prod1 = xt1 * co1
||         SUBSP   .S2     B_xl1,B_xl20,B_yt1' ; |620| <1,33> yt1 = xl1 - xl20
||         SUB     .L2X    B_j',A_fft_jmp,B_predj ; |646| <1,33>  ^ 
||         ADDSP   .L1     A_x0',A_x_l1,A_xh0 ; |534| <2,16> xh0 = x0 + x_l1
|| [!A_predj] ADD  .S1X    A_x,B_fft_jmp,A_x ; |557| <2,16> *x = *x + fft_jmp
||         LDDW    .D2T2   *+B_x[B_h2],B_x_l2p:B_x_l2 ; |532| <2,16> Load x_l2p, x_l2

   [ B_i]  ADD     .D2     0xffffffff,B_i,B_i ; |652| <1,34> Decrement loop counter by 4
||         MPYSP   .M1     A_yt1,A_co1,A_prod3 ; |584| <1,34> prod3 = yt1 * co1 
||         MPYSP   .M2     B_xt0,B_co2,B_prod5 ; |610| <1,34> prod1 = xt0 * co2 
||         ADDSP   .L2     B_xl1,B_xl20,B_yt2 ; |632| <1,34> yt2 = xl1 + xl20
||         SUBSP   .L1     A_x1',A_x_l1p,A_xl1 ; |537| <2,17> xl1 = x1 - x_l1p
|| [!A_predj] ZERO .S1     A_j'              ; |558| <2,17> j=0
||         LDDW    .D1T1   *++A_x(16),A_x1':A_x0' ; |524| <3,0> Load x1, x0
||         MV      .S2X    A_x,B_x           ; |523| <3,0> x_copy =x

$C$DW$L$_fft_SPXSP_sa$4$E:
;** --------------------------------------------------------------------------*
$C$L4:    ; PIPED LOOP EPILOG

           MPYSP   .M2     B_xt2,B_si3,B_prod12'' ; |636| (E) <1,35> prod4 = xt2 * si3
|| [!B_predj] ZERO .L2     B_j'              ; |648| (E) <1,35>  ^ 
||         STW     .D2T2   B_sum1,*++B_y(24) ; |605| (E) <1,35> ptr_x0[0] = temp1
||         ADDSP   .S1     A_x_h2,A_x_l2,A_xh20 ; |538| (E) <2,18> xh20 = x_h2 + x_l2
||         ADDSP   .L1     A_x_h2p,A_x_l2p,A_xh21 ; |539| (E) <2,18> xh21 = x_h2p + x_l2p
||         SHL     .S2     B_h2,0x4,B_2h2    ; |552| (E) <3,1> 
||         LDDW    .D1T1   *++A_x[A_h2],A_x_h2p:A_x_h2 ; |525| (E) <3,1> Load x_h2p, x_h2  
||         MPYSP   .M1     A_yt1,A_si1,A_prod2 ; |583| (E) <1,35> prod2 = yt1 * si1

           MPYSP   .M2     B_xt2,B_co3,B_prod9 ; |633| (E) <1,36> prod1 = xt2 * co3 
||         SUBSP   .S1     A_x_h2p,A_x_l2p,A_xl21'' ; |541| (E) <2,19> xl21 = x_h2p - x_l2p
||         SUBSP   .L1     A_x_h2,A_x_l2,A_xl20'' ; |540| (E) <2,19> xl20 = x_h2 - x_l2
||         ADDAD   .D2     B_w0,B_j',B_w     ; |513| (E) <2,19>  ^ 
||         ADDSP   .S2     B_x1,B_x_l1p,B_xh1' ; |544| (E) <2,19> xh1 = x1 + x_l1p
||         SUBSP   .L2     B_x1,B_x_l1p,B_xl1'' ; |546| (E) <2,19> xl1 = x1 - x_l1p
||         ADD     .D1     0x6,A_j',A_j'     ; |555| (E) <3,2> j += 6
||         MPYSP   .M1     A_xt1,A_co1,A_prod1 ; |582| (E) <1,36> prod1 = xt1 * co1

           MPYSP   .M1     A_xt1,A_si1,A_prod4 ; |585| (E) <1,37> prod4 = xt1 * si1
||         ADDSP   .L1     A_prod5,A_prod6,A_sum5 ; |574| (E) <1,37>  ^ sum1 = prod1 + prod2
||         STW     .D2T2   B_sum2,*+B_y(4)   ; |606| (E) <1,37> ptr_x0[1] = temp2
||         ADD     .S2     0x6,B_j',B_j'     ; |645| (E) <2,20> 
||         SUBSP   .L2     B_x0,B_x_l1,B_xl0 ; |545| (E) <2,20> xl0 = x0 - x_l1
||         MV      .S1X    B_w,A_w           ; |514| (E) <2,20>  ^ 
||         LDDW    .D1T1   *++A_x[A_h2],A_x_l1p:A_x_l1 ; |526| (E) <3,3> Load x_l1p, x_l1  
||         MPYSP   .M2     B_xt0,B_si2,B_prod8 ; |613| (E) <1,37> prod4 = xt0 * si2

           MPYSP   .M2     B_yt1',B_si1,B_prod2 ; |622| (E) <1,38> prod2 = yt1 * si1
||         ADDSP   .L2     B_prod5,B_prod6,B_sum5 ; |614| (E) <1,38> sum1 = prod1 + prod2
||         SUBSP   .S1     A_prod7,A_prod8,A_sum6'' ; |575| (E) <1,38> sum2 = prod3 - prod4
||         ADDSP   .S2     B_x_h2p,B_x_l2p,B_xh21 ; |548| (E) <2,21> xh21 = x_h2p + x_l2p
||         LDDW    .D2T2   *+B_w(40),B_si3:B_co3 ; |521| (E) <2,21> Load si3, co3
||         LDDW    .D1T1   *A_w,A_si1:A_co1  ; |516| (E) <2,21> Load si1, co1
||         MPYSP   .M1     A_xt2,A_co3,A_prod9 ; |594| (E) <1,38> prod1 = xt2 * co3 

           MPYSP   .M2     B_yt1',B_co1,B_prod3 ; |623| (E) <1,39> prod3 = yt1 * co1 
||         ADDSP   .L1     A_xh0,A_xh20,A_sum1 ; |563| (E) <2,22> temp1 = xh0 + xh20
||         LDDW    .D2T2   *+B_w(24),B_si1:B_co1 ; |519| (E) <2,22> Load si1, co1
||         SUBSP   .S1     A_xh1,A_xh21,A_yt0$3 ; |569| (E) <2,22> yt0 = xh1 - xh21
||         LDDW    .D1T1   *+A_w(16),A_si3:A_co3 ; |518| (E) <2,22> Load si3, co3
||         ADDSP   .S2     B_x_h2,B_x_l2,B_xh20'' ; |547| (E) <2,22> xh20 = x_h2 + x_l2
||         ADDSP   .L2     B_x0,B_x_l1,B_xh0'' ; |543| (E) <2,22> xh0 = x0 + x_l1
||         MPYSP   .M1     A_xt2,A_si3,A_prod12 ; |597| (E) <1,39> prod4 = xt2 * si3

           MPYSP   .M2     B_yt2,B_si3,B_prod10 ; |634| (E) <1,40> prod2 = yt2 * si3
||         SUBSP   .S1     A_xl1,A_xl20'',A_yt1'' ; |581| (E) <2,23> yt1 = xl1 - xl20
||         SUBSP   .L2     B_x_h2,B_x_l2,B_xl20'' ; |549| (E) <2,23> xl20 = x_h2 - x_l2
||         SUBSP   .S2     B_x_h2p,B_x_l2p,B_xl21 ; |550| (E) <2,23> xl21 = x_h2p - x_l2p
||         LDDW    .D2T2   *+B_w(32),B_si2:B_co2 ; |520| (E) <2,23> Load si2, co2
||         LDDW    .D1T1   *+A_w(8),A_si2:A_co2 ; |517| (E) <2,23>  ^ Load si2, co2
||         ADDSP   .L1     A_prod1,A_prod2,A_sum3 ; |586| (E) <1,40> sum1 = prod1 + prod2

           SUBSP   .L1     A_prod3,A_prod4,A_sum4 ; |587| (E) <1,41> sum2 = prod3 - prod4
||         SUBSP   .L2     B_prod7,B_prod8,B_sum6 ; |615| (E) <1,41> sum2 = prod3 - prod4
||         STW     .D1T1   A_sum5,*++A_y[A_h22] ; |576| (E) <1,41> ptr_x2[l1] = sum1 
||         ADDSP   .S1     A_xl1,A_xl20'',A_yt2$2 ; |593| (E) <2,24> yt2 = xl1 + xl20
||         LDDW    .D2T2   *++B_x(24),B_x1:B_x0 ; |529| (E) <3,7> Load x1, x0  
||         MPYSP   .M2     B_yt2,B_co3,B_prod11 ; |635| (E) <1,41> prod3 = yt2 * co3 

           ADDSP   .L2     B_prod1,B_prod2,B_sum3'' ; |625| (E) <1,42> sum1 = prod1 + prod2
||         STW     .D2T2   B_sum5,*++B_y[B_h22] ; |616| (E) <1,42> ptr_x2[l1] = sum1 
||         STW     .D1T1   A_sum6'',*+A_y(4) ; |577| (E) <1,42> ptr_x2[l1+1] = sum2 
||         SUBSP   .S1     A_xh0,A_xh20,A_xt0'' ; |568| (E) <2,25> xt0 = xh0 - xh20
||         SUBSP   .S2     B_xh1',B_xh21,B_yt0 ; |609| (E) <2,25> yt0 = xh1 - xh21
||         ADDSP   .L1     A_prod9,A_prod10,A_sum7' ; |598| (E) <1,42> sum1 = prod1 + prod2

           SUBSP   .S1     A_prod11,A_prod12,A_sum8'' ; |599| (E) <1,43> sum2 = prod3 - prod4
||         ADDSP   .L1     A_xh1,A_xh21,A_sum2'' ; |564| (E) <2,26> temp2 = xh1 + xh21
||         LDDW    .D1T1   *+A_x[A_h2],A_x_l2p:A_x_l2 ; |527| (E) <3,9> Load x_l2p, x_l2          
||         LDDW    .D2T2   *++B_x[B_h2],B_x_h2p:B_x_h2 ; |530| (E) <3,9> Load x_h2p, x_h2  
||         SUBSP   .L2     B_prod3,B_prod4,B_sum4'' ; |626| (E) <1,43> sum2 = prod3 - prod4

           STW     .D1T1   A_sum3,*++A_y[A_h22] ; |588| (E) <1,44> ptr_x2[l1] = sum1 
||         SUBSP   .S2     B_xh0'',B_xh20'',B_xt0'' ; |608| (E) <2,27> xt0 = xh0 - xh20
||         LDDW    .D2T2   *++B_x[B_h2],B_x_l1p:B_x_l1 ; |531| (E) <3,10> Load x_l1p, x_l1  
||         ADDSP   .L2     B_prod9,B_prod10,B_sum7 ; |637| (E) <1,44> sum1 = prod1 + prod2

           STW     .D1T1   A_sum4,*+A_y(4)   ; |589| (E) <1,45> ptr_x2[l1+1] = sum2 
||         STW     .D2T2   B_sum6,*+B_y(4)   ; |617| (E) <1,45> ptr_x2[l1+1] = sum2 
||         ADDSP   .L1     A_xl0,A_xl21'',A_xt1'' ; |580| (E) <2,28> xt1 = xl0 + xl21
||         ADDSP   .S2     B_xl0,B_xl21,B_xt1'' ; |619| (E) <2,28> xt1 = xl0 + xl21
||         MPYSP   .M1     A_yt0$3,A_co2,A_prod7' ; |572| (E) <2,28> prod3 = yt0 * co2 
||         SUBSP   .L2     B_prod11,B_prod12'',B_sum8$3 ; |638| (E) <1,45> sum2 = prod3 - prod4

           STW     .D1T1   A_sum7',*++A_y[A_h22] ; |600| (E) <1,46> ptr_x2[l1] = sum1 
||         MPYSP   .M1     A_yt2$2,A_co3,A_prod11 ; |596| (E) <2,29> prod3 = yt2 * co3 
||         MPYSP   .M2     B_yt0,B_co2,B_prod7'' ; |612| (E) <2,29> prod3 = yt0 * co2 
||         ADDSP   .L2     B_xh1',B_xh21,B_sum2'' ; |604| (E) <2,29> temp2 = xh1 + xh21
||         ADDSP   .S1     A_x1',A_x_l1p,A_xh1' ; |535| (E) <3,12> xh1 = x1 + x_l1p
||         STW     .D2T2   B_sum3'',*++B_y[B_h22] ; |627| (E) <1,46> ptr_x2[l1] = sum1 

           SUB     .L1     A_y,A_temp1$1,A_y ; |643| (E) <1,47> 
||         STW     .D2T2   B_sum4'',*+B_y(4) ; |628| (E) <1,47> ptr_x2[l1+1] = sum2 
||         SHL     .S1     A_3h22,0x2,A_temp1$3 ; |642| (E) <2,30> 
||         MPYSP   .M1     A_yt0$3,A_si2,A_prod6 ; |571| (E) <2,30>  ^ prod2 = yt0 * si2
||         SUBSP   .S2     B_xl0,B_xl21,B_xt2'' ; |631| (E) <2,30> xt2 = xl0 - xl21
||         MPYSP   .M2     B_yt0,B_si2,B_prod6'' ; |611| (E) <2,30> prod2 = yt0 * si2
||         STW     .D1T1   A_sum8'',*+A_y(4) ; |601| (E) <1,47> ptr_x2[l1+1] = sum2 

   [!B_predj] ADD  .S1X    A_y,B_fft_jmp,A_y ; |647| (E) <1,48> 
||         MPYSP   .M1     A_xt0'',A_co2,A_prod5' ; |570| (E) <2,31>  ^ prod1 = xt0 * co2 
||         ADDSP   .L2     B_xh0'',B_xh20'',B_sum1'' ; |603| (E) <2,31> temp1 = xh0 + xh20
||         SUBSP   .L1     A_x0',A_x_l1,A_xl0' ; |536| (E) <3,14> xl0 = x0 - x_l1
||         SUB     .D1     A_j',A_fft_jmp,A_predj ; |556| (E) <3,14> predj = j - fft_jmp
||         STW     .D2T2   B_sum7,*++B_y[B_h22] ; |639| (E) <1,48> ptr_x2[l1] = sum1 

           SUBSP   .S1     A_xl0,A_xl21'',A_xt2 ; |592| (E) <2,32> xt2 = xl0 - xl21
||         MPYSP   .M1     A_yt2$2,A_si3,A_prod10 ; |595| (E) <2,32> prod2 = yt2 * si3
||         STW     .D1T1   A_sum1,*++A_y(16) ; |565| (E) <2,32> ptr_x0[0] = temp1
||         MPYSP   .M2     B_xt1'',B_si1,B_prod4' ; |624| (E) <2,32> prod4 = xt1 * si1
||         MV      .S2X    A_y,B_y           ; |561| (E) <2,32> 
||         SUB     .L1X    A_x,B_2h2,A_x     ; |553| (E) <3,15> 
||         STW     .D2T2   B_sum8$3,*+B_y(4) ; |640| (E) <1,49> ptr_x2[l1+1] = sum2 

           STW     .D1T1   A_sum2'',*+A_y(4) ; |566| (E) <2,33> ptr_x0[1] = temp2
||         MPYSP   .M2     B_xt1'',B_co1,B_prod1'' ; |621| (E) <2,33> prod1 = xt1 * co1
||         SUBSP   .S2     B_xl1'',B_xl20'',B_yt1' ; |620| (E) <2,33> yt1 = xl1 - xl20
||         SUB     .L2X    B_j',A_fft_jmp,B_predj ; |646| (E) <2,33>  ^ 
||         ADDSP   .L1     A_x0',A_x_l1,A_xh0' ; |534| (E) <3,16> xh0 = x0 + x_l1
|| [!A_predj] ADD  .S1X    A_x,B_fft_jmp,A_x ; |557| (E) <3,16> *x = *x + fft_jmp
||         LDDW    .D2T2   *+B_x[B_h2],B_x_l2p:B_x_l2 ; |532| (E) <3,16> Load x_l2p, x_l2
||         MPYSP   .M1     A_xt0'',A_si2,A_prod8 ; |573| (E) <2,33> prod4 = xt0 * si2

           LDW     .D2T1   *+SP(36),A_radix'
||         MPYSP   .M1     A_yt1'',A_co1,A_prod3 ; |584| (E) <2,34> prod3 = yt1 * co1 
||         ADDSP   .L2     B_xl1'',B_xl20'',B_yt2'' ; |632| (E) <2,34> yt2 = xl1 + xl20
||         SUBSP   .L1     A_x1',A_x_l1p,A_xl1 ; |537| (E) <3,17> xl1 = x1 - x_l1p
|| [!A_predj] ZERO .S1     A_j'              ; |558| (E) <3,17> j=0
||         MPYSP   .M2     B_xt0'',B_co2,B_prod5' ; |610| (E) <2,34> prod1 = xt0 * co2 

           MPYSP   .M2     B_xt2'',B_si3,B_prod12' ; |636| (E) <2,35> prod4 = xt2 * si3
|| [!B_predj] ZERO .L2     B_j'              ; |648| (E) <2,35>  ^ 
||         STW     .D2T2   B_sum1'',*++B_y(24) ; |605| (E) <2,35> ptr_x0[0] = temp1
||         ADDSP   .S1     A_x_h2,A_x_l2,A_xh20 ; |538| (E) <3,18> xh20 = x_h2 + x_l2
||         ADDSP   .L1     A_x_h2p,A_x_l2p,A_xh21 ; |539| (E) <3,18> xh21 = x_h2p + x_l2p
||         MPYSP   .M1     A_yt1'',A_si1,A_prod2 ; |583| (E) <2,35> prod2 = yt1 * si1

           MPYSP   .M2     B_xt2'',B_co3,B_prod9 ; |633| (E) <2,36> prod1 = xt2 * co3 
||         SUBSP   .S1     A_x_h2p,A_x_l2p,A_xl21' ; |541| (E) <3,19> xl21 = x_h2p - x_l2p
||         SUBSP   .L1     A_x_h2,A_x_l2,A_xl20' ; |540| (E) <3,19> xl20 = x_h2 - x_l2
||         ADDAD   .D2     B_w0,B_j',B_w     ; |513| (E) <3,19>  ^ 
||         ADDSP   .S2     B_x1,B_x_l1p,B_xh1 ; |544| (E) <3,19> xh1 = x1 + x_l1p
||         SUBSP   .L2     B_x1,B_x_l1p,B_xl1' ; |546| (E) <3,19> xl1 = x1 - x_l1p
||         MPYSP   .M1     A_xt1'',A_co1,A_prod1 ; |582| (E) <2,36> prod1 = xt1 * co1

           MPYSP   .M1     A_xt1'',A_si1,A_prod4 ; |585| (E) <2,37> prod4 = xt1 * si1
||         ADDSP   .L1     A_prod5',A_prod6,A_sum5 ; |574| (E) <2,37>  ^ sum1 = prod1 + prod2
||         STW     .D2T2   B_sum2'',*+B_y(4) ; |606| (E) <2,37> ptr_x0[1] = temp2
||         ADD     .S2     0x6,B_j',B_j'     ; |645| (E) <3,20> 
||         SUBSP   .L2     B_x0,B_x_l1,B_xl0 ; |545| (E) <3,20> xl0 = x0 - x_l1
||         MV      .S1X    B_w,A_w           ; |514| (E) <3,20>  ^ 
||         MPYSP   .M2     B_xt0'',B_si2,B_prod8 ; |613| (E) <2,37> prod4 = xt0 * si2

           MPYSP   .M1     A_xt2,A_co3,A_prod9'' ; |594| (E) <2,38> prod1 = xt2 * co3 
||         ADDSP   .L2     B_prod5',B_prod6'',B_sum5 ; |614| (E) <2,38> sum1 = prod1 + prod2
||         SUBSP   .S1     A_prod7',A_prod8,A_sum6' ; |575| (E) <2,38> sum2 = prod3 - prod4
||         ADDSP   .S2     B_x_h2p,B_x_l2p,B_xh21 ; |548| (E) <3,21> xh21 = x_h2p + x_l2p
||         LDDW    .D2T2   *+B_w(40),B_si3:B_co3 ; |521| (E) <3,21> Load si3, co3
||         LDDW    .D1T1   *A_w,A_si1:A_co1  ; |516| (E) <3,21> Load si1, co1
||         MPYSP   .M2     B_yt1',B_si1,B_prod2' ; |622| (E) <2,38> prod2 = yt1 * si1

           MPYSP   .M2     B_yt1',B_co1,B_prod3 ; |623| (E) <2,39> prod3 = yt1 * co1 
||         ADDSP   .L1     A_xh0',A_xh20,A_sum1 ; |563| (E) <3,22> temp1 = xh0 + xh20
||         LDDW    .D2T2   *+B_w(24),B_si1:B_co1 ; |519| (E) <3,22> Load si1, co1
||         SUBSP   .S1     A_xh1',A_xh21,A_yt0$2 ; |569| (E) <3,22> yt0 = xh1 - xh21
||         LDDW    .D1T1   *+A_w(16),A_si3:A_co3 ; |518| (E) <3,22> Load si3, co3
||         ADDSP   .S2     B_x_h2,B_x_l2,B_xh20' ; |547| (E) <3,22> xh20 = x_h2 + x_l2
||         ADDSP   .L2     B_x0,B_x_l1,B_xh0' ; |543| (E) <3,22> xh0 = x0 + x_l1
||         MPYSP   .M1     A_xt2,A_si3,A_prod12 ; |597| (E) <2,39> prod4 = xt2 * si3

           ADDSP   .L1     A_prod1,A_prod2,A_sum3 ; |586| (E) <2,40> sum1 = prod1 + prod2
||         SUBSP   .S1     A_xl1,A_xl20',A_yt1' ; |581| (E) <3,23> yt1 = xl1 - xl20
||         SUBSP   .L2     B_x_h2,B_x_l2,B_xl20' ; |549| (E) <3,23> xl20 = x_h2 - x_l2
||         SUBSP   .S2     B_x_h2p,B_x_l2p,B_xl21 ; |550| (E) <3,23> xl21 = x_h2p - x_l2p
||         LDDW    .D2T2   *+B_w(32),B_si2:B_co2 ; |520| (E) <3,23> Load si2, co2
||         LDDW    .D1T1   *+A_w(8),A_si2:A_co2 ; |517| (E) <3,23>  ^ Load si2, co2
||         MPYSP   .M2     B_yt2'',B_si3,B_prod10 ; |634| (E) <2,40> prod2 = yt2 * si3

           LDW     .D2T2   *+SP(44),B6
||         SUBSP   .L1     A_prod3,A_prod4,A_sum4 ; |587| (E) <2,41> sum2 = prod3 - prod4
||         SUBSP   .L2     B_prod7'',B_prod8,B_sum6 ; |615| (E) <2,41> sum2 = prod3 - prod4
||         STW     .D1T1   A_sum5,*++A_y[A_h22] ; |576| (E) <2,41> ptr_x2[l1] = sum1 
||         ADDSP   .S1     A_xl1,A_xl20',A_yt2 ; |593| (E) <3,24> yt2 = xl1 + xl20
||         MPYSP   .M2     B_yt2'',B_co3,B_prod11 ; |635| (E) <2,41> prod3 = yt2 * co3 

           ADDSP   .L2     B_prod1'',B_prod2',B_sum3' ; |625| (E) <2,42> sum1 = prod1 + prod2
||         STW     .D2T2   B_sum5,*++B_y[B_h22] ; |616| (E) <2,42> ptr_x2[l1] = sum1 
||         STW     .D1T1   A_sum6',*+A_y(4)  ; |577| (E) <2,42> ptr_x2[l1+1] = sum2 
||         SUBSP   .S1     A_xh0',A_xh20,A_xt0' ; |568| (E) <3,25> xt0 = xh0 - xh20
||         SUBSP   .S2     B_xh1,B_xh21,B_yt0 ; |609| (E) <3,25> yt0 = xh1 - xh21
||         ADDSP   .L1     A_prod9'',A_prod10,A_sum7 ; |598| (E) <2,42> sum1 = prod1 + prod2

           SUBSP   .S1     A_prod11,A_prod12,A_sum8' ; |599| (E) <2,43> sum2 = prod3 - prod4
||         ADDSP   .L1     A_xh1',A_xh21,A_sum2' ; |564| (E) <3,26> temp2 = xh1 + xh21
||         SUBSP   .L2     B_prod3,B_prod4',B_sum4' ; |626| (E) <2,43> sum2 = prod3 - prod4

           STW     .D1T1   A_sum3,*++A_y[A_h22] ; |588| (E) <2,44> ptr_x2[l1] = sum1 
||         SUBSP   .S2     B_xh0',B_xh20',B_xt0' ; |608| (E) <3,27> xt0 = xh0 - xh20
||         ADDSP   .L2     B_prod9,B_prod10,B_sum7'' ; |637| (E) <2,44> sum1 = prod1 + prod2

           SUBSP   .L2     B_prod11,B_prod12',B_sum8$2 ; |638| (E) <2,45> sum2 = prod3 - prod4
||         STW     .D2T2   B_sum6,*+B_y(4)   ; |617| (E) <2,45> ptr_x2[l1+1] = sum2 
||         ADDSP   .L1     A_xl0',A_xl21',A_xt1' ; |580| (E) <3,28> xt1 = xl0 + xl21
||         ADDSP   .S2     B_xl0,B_xl21,B_xt1' ; |619| (E) <3,28> xt1 = xl0 + xl21
||         MPYSP   .M1     A_yt0$2,A_co2,A_prod7 ; |572| (E) <3,28> prod3 = yt0 * co2 
||         STW     .D1T1   A_sum4,*+A_y(4)   ; |589| (E) <2,45> ptr_x2[l1+1] = sum2 

           STW     .D1T1   A_sum7,*++A_y[A_h22] ; |600| (E) <2,46> ptr_x2[l1] = sum1 
||         MPYSP   .M1     A_yt2,A_co3,A_prod11 ; |596| (E) <3,29> prod3 = yt2 * co3 
||         MPYSP   .M2     B_yt0,B_co2,B_prod7' ; |612| (E) <3,29> prod3 = yt0 * co2 
||         ADDSP   .L2     B_xh1,B_xh21,B_sum2' ; |604| (E) <3,29> temp2 = xh1 + xh21
||         STW     .D2T2   B_sum3',*++B_y[B_h22] ; |627| (E) <2,46> ptr_x2[l1] = sum1 

           SUB     .L1     A_y,A_temp1$3,A_y ; |643| (E) <2,47> 
||         STW     .D2T2   B_sum4',*+B_y(4)  ; |628| (E) <2,47> ptr_x2[l1+1] = sum2 
||         SHL     .S1     A_3h22,0x2,A_temp1$2 ; |642| (E) <3,30> 
||         MPYSP   .M1     A_yt0$2,A_si2,A_prod6 ; |571| (E) <3,30>  ^ prod2 = yt0 * si2
||         SUBSP   .S2     B_xl0,B_xl21,B_xt2' ; |631| (E) <3,30> xt2 = xl0 - xl21
||         MPYSP   .M2     B_yt0,B_si2,B_prod6' ; |611| (E) <3,30> prod2 = yt0 * si2
||         STW     .D1T1   A_sum8',*+A_y(4)  ; |601| (E) <2,47> ptr_x2[l1+1] = sum2 

   [!B_predj] ADD  .S1X    A_y,B_fft_jmp,A_y ; |647| (E) <2,48> 
||         MPYSP   .M1     A_xt0',A_co2,A_prod5' ; |570| (E) <3,31>  ^ prod1 = xt0 * co2 
||         ADDSP   .L2     B_xh0',B_xh20',B_sum1' ; |603| (E) <3,31> temp1 = xh0 + xh20
||         STW     .D2T2   B_sum7'',*++B_y[B_h22] ; |639| (E) <2,48> ptr_x2[l1] = sum1 

           STW     .D2T2   B_sum8$2,*+B_y(4) ; |640| (E) <2,49> ptr_x2[l1+1] = sum2 
||         MPYSP   .M1     A_yt2,A_si3,A_prod10 ; |595| (E) <3,32> prod2 = yt2 * si3
||         STW     .D1T1   A_sum1,*++A_y(16) ; |565| (E) <3,32> ptr_x0[0] = temp1
||         MPYSP   .M2     B_xt1',B_si1,B_prod4' ; |624| (E) <3,32> prod4 = xt1 * si1
||         MV      .S2X    A_y,B_y           ; |561| (E) <3,32> 
||         SUBSP   .S1     A_xl0',A_xl21',A_xt2 ; |592| (E) <3,32> xt2 = xl0 - xl21

           MPYSP   .M1     A_xt0',A_si2,A_prod8 ; |573| (E) <3,33> prod4 = xt0 * si2
||         MPYSP   .M2     B_xt1',B_co1,B_prod1' ; |621| (E) <3,33> prod1 = xt1 * co1
||         SUBSP   .S2     B_xl1',B_xl20',B_yt1' ; |620| (E) <3,33> yt1 = xl1 - xl20
||         SUB     .L2X    B_j',A_fft_jmp,B_predj ; |646| (E) <3,33>  ^ 
||         STW     .D1T1   A_sum2',*+A_y(4)  ; |566| (E) <3,33> ptr_x0[1] = temp2

           MPYSP   .M2     B_xt0',B_co2,B_prod5' ; |610| (E) <3,34> prod1 = xt0 * co2 
||         ADDSP   .L2     B_xl1',B_xl20',B_yt2' ; |632| (E) <3,34> yt2 = xl1 + xl20
||         MPYSP   .M1     A_yt1',A_co1,A_prod3 ; |584| (E) <3,34> prod3 = yt1 * co1 

           CMPGTU  .L2X    B_stride,A_radix',B_while ; |655| 
||         MPYSP   .M2     B_xt2',B_si3,B_prod12' ; |636| (E) <3,35> prod4 = xt2 * si3
|| [!B_predj] ZERO .S2     B_j'              ; |648| (E) <3,35>  ^ 
||         STW     .D2T2   B_sum1',*++B_y(24) ; |605| (E) <3,35> ptr_x0[0] = temp1
||         MPYSP   .M1     A_yt1',A_si1,A_prod2 ; |583| (E) <3,35> prod2 = yt1 * si1

           MPYSP   .M1     A_xt1',A_co1,A_prod1 ; |582| (E) <3,36> prod1 = xt1 * co1
||         MPYSP   .M2     B_xt2',B_co3,B_prod9 ; |633| (E) <3,36> prod1 = xt2 * co3 

           MPYSP   .M2     B_xt0',B_si2,B_prod8 ; |613| (E) <3,37> prod4 = xt0 * si2
||         ADDSP   .L1     A_prod5',A_prod6,A_sum5 ; |574| (E) <3,37>  ^ sum1 = prod1 + prod2
||         STW     .D2T2   B_sum2',*+B_y(4)  ; |606| (E) <3,37> ptr_x0[1] = temp2
||         MPYSP   .M1     A_xt1',A_si1,A_prod4 ; |585| (E) <3,37> prod4 = xt1 * si1

           MPYSP   .M1     A_xt2,A_co3,A_prod9' ; |594| (E) <3,38> prod1 = xt2 * co3 
||         ADDSP   .L2     B_prod5',B_prod6',B_sum5 ; |614| (E) <3,38> sum1 = prod1 + prod2
||         SUBSP   .S1     A_prod7,A_prod8,A_sum6' ; |575| (E) <3,38> sum2 = prod3 - prod4
||         MPYSP   .M2     B_yt1',B_si1,B_prod2' ; |622| (E) <3,38> prod2 = yt1 * si1

           MPYSP   .M2     B_yt1',B_co1,B_prod3 ; |623| (E) <3,39> prod3 = yt1 * co1 
||         MPYSP   .M1     A_xt2,A_si3,A_prod12 ; |597| (E) <3,39> prod4 = xt2 * si3

           ADDSP   .L1     A_prod1,A_prod2,A_sum3 ; |586| (E) <3,40> sum1 = prod1 + prod2
||         MPYSP   .M2     B_yt2',B_si3,B_prod10 ; |634| (E) <3,40> prod2 = yt2 * si3

           MPYSP   .M2     B_yt2',B_co3,B_prod11 ; |635| (E) <3,41> prod3 = yt2 * co3 
||         SUBSP   .L2     B_prod7',B_prod8,B_sum6 ; |615| (E) <3,41> sum2 = prod3 - prod4
||         STW     .D1T1   A_sum5,*++A_y[A_h22] ; |576| (E) <3,41> ptr_x2[l1] = sum1 
||         SUBSP   .L1     A_prod3,A_prod4,A_sum4 ; |587| (E) <3,41> sum2 = prod3 - prod4

           ADDSP   .L1     A_prod9',A_prod10,A_sum7 ; |598| (E) <3,42> sum1 = prod1 + prod2
||         STW     .D2T2   B_sum5,*++B_y[B_h22] ; |616| (E) <3,42> ptr_x2[l1] = sum1 
||         STW     .D1T1   A_sum6',*+A_y(4)  ; |577| (E) <3,42> ptr_x2[l1+1] = sum2 
||         ADDSP   .L2     B_prod1',B_prod2',B_sum3' ; |625| (E) <3,42> sum1 = prod1 + prod2

           SUBSP   .L2     B_prod3,B_prod4',B_sum4' ; |626| (E) <3,43> sum2 = prod3 - prod4
||         SUBSP   .S1     A_prod11,A_prod12,A_sum8 ; |599| (E) <3,43> sum2 = prod3 - prod4

           STW     .D1T1   A_sum3,*++A_y[A_h22] ; |588| (E) <3,44> ptr_x2[l1] = sum1 
||         ADDSP   .L2     B_prod9,B_prod10,B_sum7' ; |637| (E) <3,44> sum1 = prod1 + prod2
|| [ B_while] B    .S2     $C$L1             ; |656| 

           SUBSP   .L2     B_prod11,B_prod12',B_sum8$1 ; |638| (E) <3,45> sum2 = prod3 - prod4
||         STW     .D2T2   B_sum6,*+B_y(4)   ; |617| (E) <3,45> ptr_x2[l1+1] = sum2 
||         STW     .D1T1   A_sum4,*+A_y(4)   ; |589| (E) <3,45> ptr_x2[l1+1] = sum2 

           STW     .D2T2   B_sum3',*++B_y[B_h22] ; |627| (E) <3,46> ptr_x2[l1] = sum1 
||         STW     .D1T1   A_sum7,*++A_y[A_h22] ; |600| (E) <3,46> ptr_x2[l1] = sum1 

           SUB     .L1     A_y,A_temp1$2,A_y ; |643| (E) <3,47> 
||         STW     .D1T1   A_sum8,*+A_y(4)   ; |601| (E) <3,47> ptr_x2[l1+1] = sum2 
||         STW     .D2T2   B_sum4',*+B_y(4)  ; |628| (E) <3,47> ptr_x2[l1+1] = sum2 
||[!B_while]MV     .S1X    SP,A31            ; 

           MVC     .S2     B6,CSR            ; interrupts on
||         STW     .D2T2   B_sum7',*++B_y[B_h22] ; |639| (E) <3,48> ptr_x2[l1] = sum1 
||[!B_while]LDW    .D1T1   *+A31(8),A24      ; recover B_ptr_x 
||[!B_while]ZERO   .S1     A0                ; init A_j

           STW     .D2T2   B_sum8$1,*+B_y(4) ; |640| (E) <3,49> ptr_x2[l1+1] = sum2 
||[!B_while]LDW    .D1T1   *+A31(32),A26     ; recover B_nmax
||[!B_while]MV     .S2X    A0,         B3    ;temp j

           ; BRANCHCC OCCURS {$C$L1}         ; |656| 
;** --------------------------------------------------------------------------*
; LOOP: .trip 8

        LDW     .D2T1   *+SP(24),A8       ; A_brev
||      MVC     .S2     CSR,B28

        LDW     .D2T2   *+SP(28),B8       ; recover B_nmin 

        LDW     .D2T1   *+SP(4),A4        ; recover N 
||      AND     .L2     -2,B28,B31
||      MVC     .S2     B31,CSR           ; interrupts off

        LDW     .D2T2   *+SP(16),B6       ; B_ptr_y 
||      MV      .S2     A24, B31           ; A_p_x0

        MV      .S1     A24, A31           ; B_p_x0
||      ADDAW   .D2     B31,2,B31         ;Temp y1
||      NORM    .L2     A26,B0            ; l0=NORM(n_max)
||      MV      .S2     A26,B10
            
;***********************************************************************;
;************************   PROLOG BEGINS HERE    **********************;
;***********************************************************************;
;----------------------------------------------------------------------;
        LDDW    .D1     *A31++[02],         A17:A16         ;Load x[1] & x[0]
 ||     LDDW    .D2     *B31++[02],         B17:B16         ;Load x[3] & x[2]
 ||     ZERO    .S1     A1                                  ;oo- r2flag=0
 ||     SHR     .S2     B10,        1,          B10         ;nmax >>=1
 
        LDDW    .D1     *A31++[02],         A19:A18         ;Load x[5] & x[4]
 ||     LDDW    .D2     *B31++[02],         B19:B18         ;Load x[7] & x[6]
 ||     CMPEQ   .L1X    B8,         2,          A1          ;Check for radix 2  
        
        EXTU    .S1     A0,     26,     26,     A11         ;j0=j&3F
 ||     SHR     .S2     B3,         6,          B11         ;j1=j>>6
 ||     MV      .D1     A4,         A2                      ;Set the Loop Counter
 ||     SUB     .D2     B0,17,B0          ; l0 -= 17 
 
        MV      .S2     B0,         B5                      ;l0  
 ||     MV      .L1X    B6,         A29
 ||     ADD     .S1     A0,         1,          A0          ;j+=1
 
        ADD     .L2     B6,         4,          B29         ;ptr_y          
 ||     MV      .S1X    B10,        A10                     ;nmax 
        
        MV      .S1     A8, A30           ; A_brev
||      MV      .S2     A8, B30           ; B_brev
        
        LDBU    .D1     *A30[A11],              B12         ;k0
 ||[!A1]ADDSP   .L1     A16,        A18,        A16         ;x0+x4
 ||[!A1]ADDSP   .L2     B16,        B18,        B16         ;x2+x6
 ||     ADD     .S2     B3,         1,          B3          ;j+=1
        
   [!A1]ADDSP   .L1     A17,        A19,        A17         ;x1+x5
 ||[!A1]ADDSP   .L2     B17,        B19,        B17         ;x3+x7
 ||     LDBU    .D2     *B30[B11],              B13         ;k1             

;----------------------------------------------------------------------;
        LDDW    .D1     *A31++[02],         A17:A16         ;@Load x[1] & x[0]
 ||     LDDW    .D2     *B31++[02],         B17:B16         ;@Load x[3] & x[2]
 ||[!A1]SUBSP   .L1     A16,        A18,        A18         ;x0-x4
 ||[!A1]SUBSP   .L2     B17,        B19,        B18         ;x3-x7
 ||[A2] SUB     .S1     A2,         4,          A2          ;Decrement Loop ctr     
  
        LDDW    .D1     *A31++[02],         A19:A18         ;@Load x[5] & x[4]
 ||     LDDW    .D2     *B31++[02],         B19:B18         ;@Load x[7] & x[6]
 ||[!A1]SUBSP   .L1     A17,        A19,        A19         ;x1-x5
 ||[!A1]SUBSP   .L2     B16,        B18,        B19         ;x2-x6                                                                
        
        EXTU    .S1     A0,     26,     26,     A11         ;@j0=j&3F
 ||     SHR     .S2     B3,         6,          B11         ;@j1=j>>6
 ||     ADDSP   .L1X    A16,        B16,        A20         ;yt0
 ||     SUBSP   .L2X    A16,        B16,        B21         ;yt4
        
        ADD     .S1     A0,         1,          A0          ;@j+=1
 ||     ADDSP   .L2X    A17,        B17,        B20         ;yt1
 ||     SUBSP   .L1X    A17,        B17,        A21         ;yt5
 ||     SHL     .S2     B12,        6,          B24         ;k0<<6
        
        ADDSP   .L1X    A18,        B18,        A22         ;yt2
 ||     SUBSP   .L2X    A18,        B18,        B23         ;yt6
 ||     ADD     .S2     B24,        B13,        B12         ;k=temp+k1
        
        ADDSP   .L1X    A19,        B19,        A23         ;yt7
 ||     SUBSP   .L2X    A19,        B19,        B22         ;yt3
 ||     SHRU    .S2     B12,        B5,         B12         ;k=k>>l0

;***********************************************************************;
;********************   BEGIN OF PIPELOOPED KERNEL   *******************;
;***********************************************************************;
                    
LOOP:                   
        LDBU    .D1     *A30[A11],              B12         ;@k0
 ||[!A1]ADDSP   .L1     A16,        A18,        A16         ;@x0+x4
 ||[!A1]ADDSP   .L2     B16,        B18,        B16         ;@x2+x6
 ||     ADD     .S2     B3,         1,          B3          ;@j+=1
 ||     ADDAW   .D2     B29,        B12,        B28,        ;y1
 ||     MV      .S1X    B12,        A12                        
        
   [!A1]ADDSP   .L1     A17,        A19,        A17         ;@x1+x5
 ||[!A1]ADDSP   .L2     B17,        B19,        B17         ;@x3+x7
 ||     LDBU    .D2     *B30[B11],              B13         ;@k1                
 ||     ADDAW   .D1     A29,        A12,        A28,        ;y1

        LDDW    .D1     *A31++[02],         A17:A16         ;@@Load x[1] & x[0]
 ||     LDDW    .D2     *B31++[02],         B17:B16         ;@@Load x[3] & x[2]
 ||[!A1]SUBSP   .L1     A16,        A18,        A18         ;@x0-x4
 ||[!A1]SUBSP   .L2     B17,        B19,        B18         ;@x3-x7
 ||[A2] SUB     .S1     A2,         4,          A2          ;@Decrement Loop ctr        
 ||[A2] B       .S2     LOOP        
  
        LDDW    .D1     *A31++[02],         A19:A18         ;@@Load x[5] & x[4]
 ||     LDDW    .D2     *B31++[02],         B19:B18         ;@@Load x[7] & x[6]
 ||[!A1]SUBSP   .L1     A17,        A19,        A19         ;@x1-x5
 ||[!A1]SUBSP   .L2     B16,        B18,        B19         ;@x2-x6                                                               
 ||[A1] MV      .S1X    B22,        A23                     ;yt7=yt3
 ||[A1] MV      .S2X    A23,        B22                     ;yt3=yt7   
        
        EXTU    .S1     A0,     26,     26,     A11         ;@@j0=j&3F
 ||     SHR     .S2     B3,         6,          B11         ;@@j1=j>>6
 ||     ADDSP   .L1X    A16,        B16,        A20         ;@yt0
 ||     SUBSP   .L2X    A16,        B16,        B21         ;@yt4
 ||     STW     .D1     A20,        *A28++[A10]             ;Store yt0
 ||     STW     .D2     B20,        *B28++[B10]             ;Store yt1
        
        ADD     .S1     A0,         1,          A0          ;@@j+=1
 ||     ADDSP   .L2X    A17,        B17,        B20         ;@yt1
 ||     SUBSP   .L1X    A17,        B17,        A21         ;@yt5
 ||     SHL     .S2     B12,        6,          B24         ;@k0<<6
 ||     STW     .D1     A22,        *A28++[A10]             ;Store yt2
 ||     STW     .D2     B22,        *B28++[B10]             ;Store yt3      
 
        ADDSP   .L1X    A18,        B18,        A22         ;@yt2
 ||     SUBSP   .L2X    A18,        B18,        B23         ;@yt6
 ||     ADD     .S2     B24,        B13,        B12         ;@k=temp+k1
 ||     STW     .D1     B21,        *A28++[A10]             ;Store yt4
 ||     STW     .D2     A21,        *B28++[B10]             ;Store yt5
        
        ADDSP   .L1X    A19,        B19,        A23         ;@yt7
 ||     SUBSP   .L2X    A19,        B19,        B22         ;@yt3
 ||     SHRU    .S2     B12,        B5,         B12         ;@k=k>>l0
 ||     STW     .D1     B23,        *A28++[A10]             ;Store yt6
 ||     STW     .D2     A23,        *B28++[B10]             ;Store yt7                      

;***********************************************************************;
;********************   END OF PIPELOOPED KERNEL  **********************;
;***********************************************************************;
ENDFUNTION:         
           MV      .S1X    SP,A31            ; 
||         LDW     .D2T2   *+SP(80),B3       ; Restore return address 

           LDDW    .D1T1   *+A31(56),A15:A14 ; Poping A15:A14 
||         LDW     .D2T2   *+SP(44),B28      ; Restore CSR


           LDDW    .D2T2   *+SP(64),B11:B10  ; Poping B11:B10
||         LDW     .D1T1   *+A31(84),A10     ; Poping A10

           LDDW    .D2T2   *+SP(72),B13:B12  ; Poping B13:B12 
||         LDDW    .D1T1   *+A31(48),A13:A12 ; Poping A13:A12

           NOP

           RET     .S2     B3                ; Return 
           MVC     .S2    B28, CSR          ; interrupts on
           LDW    .D2T1   *++SP(88),A11     ; Poping A11
           NOP            2
        
	     .end

* ======================================================================== *
*  End of file: fft_SPXSP_h.asm                                            *
* ======================================================================== *
