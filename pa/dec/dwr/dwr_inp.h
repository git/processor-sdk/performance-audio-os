
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Decode Wrapper functionality for Legacy decoders to work with off-chip input buffer.
//
//
//

#ifndef DWR_TII_PRIV_
#define DWR_TII_PRIV_

#include <alg.h>
#include <ti/xdais/ialg.h>
//#include <log.h>
#include "icom.h"

#include "pafdec.h"
#include "stdbeta.h"

typedef struct IDWR_Obj {
    struct IDWR_Fxns *fxns;    /* function list: standard, public, private */
} IDWR_Obj;

typedef struct IDWR_Obj *IDWR_Handle;

typedef struct IDWR_Active {
    Int size;
	Int flag;
    const PAF_InpBufConfig *pInpBufConfig;
	// Needed by decoder
} IDWR_Active;

typedef IDWR_Active DWR_TII_Active;

typedef struct IDWR_Params {
    Int size;
    const IALG_Fxns * decTable;
    const IALG_Params *decParam;
} IDWR_Params;

extern const IDWR_Params IDWRAAC_PARAMS;
extern const IDWR_Params IDWRAC3_PARAMS;
extern const IDWR_Params IDWRDTS_PARAMS;
extern const IDWR_Params IDWRPCM_PARAMS;

typedef struct IDWR_FXNS
{
   /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IDWR_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);
    Int         (*info)(IDWR_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);
    Int         (*decode)(IDWR_Handle, ALG_Handle, PAF_DecodeInStruct *, PAF_DecodeOutStruct *);
} IDWR_Fxns, *IDWR_FxnsPtr;

extern const IDWR_Fxns DWRAAC_TII_IDWRAAC; 
extern const IDWR_Fxns DWRAC3_TII_IDWRAC3;
extern const IDWR_Fxns DWRDTS_TII_IDWRDTS; 
extern const IDWR_Fxns DWRPCM_TII_IDWRPCM;

#define DWRAAC_TII_init COM_TII_init
#define DWRAC3_TII_init COM_TII_init
#define DWRDTS_TII_init COM_TII_init
#define DWRPCM_TII_init COM_TII_init

#define STD_BETA_DWRAAC STD_BETA_AAC
#define STD_BETA_DWRAC3 STD_BETA_AC3
#define STD_BETA_DWRDTS STD_BETA_DTS
#define STD_BETA_DWRPCM STD_BETA_PCM

extern IALG_Fxns DWRAAC_TII_IALG; 
extern IALG_Fxns DWRAC3_TII_IALG; 
extern IALG_Fxns DWRDTS_TII_IALG; 
extern IALG_Fxns DWRPCM_TII_IALG; 

typedef struct DWR_TII_CommonInp {
    PAF_UnionPointer base;
    PAF_UnionPointer pntr;
    XDAS_UInt32 sizeofBuffer;
	XDAS_UInt32 unused;
} DWR_TII_CommonInp;


typedef struct DWR_TII_Obj {
    IALG_Obj           alg;          /* MUST be first field of all XDAS algs */
	IALG_Handle        decHandle;
    DWR_TII_Active    *pActive;      /* private interface (stream-shared) */
    PAF_IALG_Common   *pCommonLock;
	DWR_TII_CommonInp  commonInp;
} DWR_TII_Obj;

extern Void DWR_TII_activate(IALG_Handle handle);
extern Void DWR_TII_deactivate(IALG_Handle handle);
extern Int DWR_TII_alloc(const IALG_Params *algParams, IALG_Fxns **pf,
                        IALG_MemRec memTab[]);
extern Int DWR_TII_control(IALG_Handle, IALG_Cmd, IALG_Status *);
extern Int DWR_TII_initObj(IALG_Handle handle,
                          const IALG_MemRec memTab[], IALG_Handle parent,
                          const IALG_Params *algParams);                
extern Int DWRAAC_TII_numAlloc();
extern Int DWRAC3_TII_numAlloc();
extern Int DWRDTS_TII_numAlloc();
extern Int DWRPCM_TII_numAlloc();


extern Int DWR_TII_decode(IDWR_Handle, ALG_Handle, PAF_DecodeInStruct *, PAF_DecodeOutStruct *);
extern Int DWR_TII_info(IDWR_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);
extern Int DWR_TII_reset(IDWR_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);

typedef struct DWR_TII_Fxns {
    IALG_Fxns   ialg;
    Int         (*reset)(IDWR_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);
    Int         (*info)(IDWR_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);
    Int         (*decode)(IDWR_Handle, ALG_Handle, PAF_DecodeInStruct *, PAF_DecodeOutStruct *);
	//Int         (*setAudioFrame)(IALG_Handle, PAF_AudioFrame *, Int, PAF_DecodeStatus *);
}DWR_TII_Fxns;
#endif  /* DWR_TII_PRIV_ */
