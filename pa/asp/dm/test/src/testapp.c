/******************************************************************************
 * Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

/**
 *  @file  testapp.c
 *
 *  @brief Down Mix test application Testapp file.
 *
 *          This is a Down Mix test application.
 *          The application test the various implementation of Down Mix.
 *
 *          It expects the input configuration from a wave file and generate
 *          the output configuration wave file with downmix conversion.
 *
 *          The Down Mix parameters are recieved from external tables. 
 *          The test application
 *
 *          will configure the neccessary parameters and allocate memory
 *          for Down Mix input/output buffer and Down Mix memory states.
 ******************************************************************************/

/************************************************************
 Include files
 *************************************************************/
#include "testapp.h"
#include "dm_tii.h"
#include "dm.h"
#include "util_file_io.h"
#include "util_paf_file_io.h"
#include "util_c66x_cache.h"

#define PROFILER

#ifdef PROFILER
#include "util_profiling.h"
#endif
/************************************************************
 Function Definitions
 *************************************************************/
/************************************************************
 Macro and Extern Definitions
 *************************************************************/
//#define ARMCOMPILE
#define SAMPLING_RATE 48000
#define MAX_CHANNELS 32
#ifdef ARMCOMPILE
#define FRAME_SIZE 256
#else
#define FRAME_SIZE 256
#endif
#define Stream_Sat_CH PAF_CC_SAT_PHANTOM3
#define Stream_Sub_CH PAF_CC_SUB_ONE
#define Req_Sat_CH PAF_CC_SAT_STEREO
#define Req_Sub_CH PAF_CC_SUB_ZERO
#define N_Input_CH 6
#define N_Output_CH 2
#define LOG printf

extern const IDM_Params IDM_PARAMS;
extern const test_case dm_test_cases[];
/************************************************************
 global variables
 *************************************************************/
const PAFHJT_t *pafhjt;
static short samsiz[MAX_CHANNELS]; //local memory for samsiz 
#define MAX(a,b) (((a)>(b))?(a):(b))
/************************************************************
 Calling main() Function
 *************************************************************/
#ifdef ARMCOMPILE
void idle_func() 
{
    printf("In idle_func\n");
}
#endif
int main() 
{
    const IDM_Params *pDmParams;
    PAF_AudioFrame PAfrm;
    IDM_Handle DmHandle;
    tFIOHandle *ppAF = (tFIOHandle*) calloc(1, sizeof(tFIOHandle));
    DM_Status *pStatus;
    char inFileName[MAX_CHAR];
    char outFileName[MAX_CHAR];

    int test_case_I = 0,Frame_size =256;
#ifdef PROFILER
    int use_cache = 1;                          // to enable chache
#endif
#ifdef ARMCOMPILE
    use_cache = 0;
#endif
    if (use_cache)
        memarchcfg_cacheEnable();
    int error;
    PAF_AudioFunctions audioFrameFunctions = { PAF_ASP_dB2ToLinear,
            PAF_ASP_channelMask,
            NULL,
            NULL,
            NULL };

#ifdef PROFILER
    tPrfl profiler;
#endif
    /* Interface function calls*/
    pafhjt = &PAFHJT_RAM;
for (Frame_size = 256; Frame_size < 1536; )
{
    for (test_case_I = 0; test_case_I < 18; test_case_I++)
    {
        unsigned int framecnt = 0;
        test_case testCase = dm_test_cases[test_case_I];
        strcpy(inFileName, testCase.charInFileName);
        strcpy(outFileName, testCase.charOutFileName);

        printf("Executing test case[%d]: %s\n", test_case_I,
                testCase.charTestCaseName);
        /************************************************************
         Initilizing tFIOHandle variables
         *************************************************************/

        ppAF->inBufChannelCount = testCase.numOfInChannels;
        ppAF->outBufChannelCount = testCase.numOfOutChannels;
        ppAF->inBufSampleCount = Frame_size;
        ppAF->outBufSampleCount = Frame_size;
        ppAF->outputSampleRate = SAMPLING_RATE;
        ppAF->samsizemode = testCase.samsize_enable;
        ppAF->inBuf = (float*) calloc(
                ppAF->inBufSampleCount * ppAF->inBufChannelCount, 4);
        ppAF->outBuf = (float*) calloc(
                ppAF->outBufSampleCount * ppAF->outBufChannelCount, 4);
        ppAF->tempBuf = (unsigned char*) calloc(
                MAX(ppAF->inBufSampleCount, ppAF->outBufSampleCount)
                        * ppAF->inBufChannelCount, 4);

        /************************************************************
         Initilizing PAFAudioframe variables
         *************************************************************/
        PAfrm.fxns = &audioFrameFunctions;
        PAfrm.mode = 1;
        PAfrm.data.nChannels = MAX_CHANNELS;
        PAfrm.data.nSamples = Frame_size;
        PAfrm.channelConfigurationRequest.full = 0;
#if 0
        PAfrm.channelConfigurationRequest.part.sat =
                testCase.channelConfigurationRequest.part.sat;
        PAfrm.channelConfigurationRequest.part.sub =
                testCase.channelConfigurationRequest.part.sub;
        PAfrm.channelConfigurationRequest.part.aux =
                testCase.channelConfigurationRequest.part.aux;
        PAfrm.channelConfigurationRequest.part.extMask =
                testCase.channelConfigurationRequest.part.extMask;
        PAfrm.channelConfigurationRequest.part.extMask2 =
                testCase.channelConfigurationRequest.part.extMask2;
        PAfrm.channelConfigurationRequest.part.extMask3 =
                testCase.channelConfigurationRequest.part.extMask3;
        PAfrm.channelConfigurationRequest.part.reserved1 =
                testCase.channelConfigurationRequest.part.reserved1;
        PAfrm.channelConfigurationRequest.part.reserved2 =
                testCase.channelConfigurationRequest.part.reserved2;
        PAfrm.channelConfigurationStream.part.sat =
                testCase.channelConfigurationStream.part.sat;
        PAfrm.channelConfigurationStream.part.sub =
                testCase.channelConfigurationStream.part.sub;
        PAfrm.channelConfigurationStream.part.aux =
                testCase.channelConfigurationStream.part.aux;
        PAfrm.channelConfigurationStream.part.extMask =
                testCase.channelConfigurationStream.part.extMask;
        PAfrm.channelConfigurationStream.part.extMask2 =
                testCase.channelConfigurationStream.part.extMask2;
        PAfrm.channelConfigurationStream.part.extMask3 =
                testCase.channelConfigurationStream.part.extMask3;
        PAfrm.channelConfigurationStream.part.reserved1 =
                testCase.channelConfigurationStream.part.reserved1;
        PAfrm.channelConfigurationStream.part.reserved2 =
                testCase.channelConfigurationStream.part.reserved2;
#else
		PAfrm.channelConfigurationRequest.full = testCase.channelConfigurationRequest.full;
		PAfrm.channelConfigurationStream.full = testCase.channelConfigurationStream.full;
#endif

        PAfrm.pChannelConfigurationMaskTable = &PAF_ASP_stdCCMT;
        PAfrm.sampleCount = Frame_size;
        memset(samsiz, 0, sizeof(short) * MAX_CHANNELS);
        PAfrm.data.samsiz = &samsiz[0];
        /************************************************************
         Initilizing IDM_Params variables
         *************************************************************/

        pDmParams = &IDM_PARAMS;

        /************************************************************
         DmHandle Creatrion
         *************************************************************/

        DmHandle = (IDM_Handle) DM_create((const IDM_Fxns *) &DM_TII_IDM,
                (const IDM_Params *) pDmParams);
        if (DmHandle == NULL) 
        {
            LOG("DmHandle Handle cration failed \n");
            return 0;
        }

        /************************************************************
         DmHandle pStatus Creatrion
         *************************************************************/

        error = DmHandle->fxns->ialg.algControl((Void *) DmHandle,
                IDM_GETSTATUSADDRESS1, (void *) &pStatus);

        if (error == RETRUN_ERROR) 
        {
            LOG("Failed while DmHandle pStatus Creatrion \n");
            return 0;
        }

        /************************************************************
         Initilizing pStatus variables
         *************************************************************/

        pStatus->size = pDmParams->pStatus->size;
        pStatus->mode = pDmParams->pStatus->mode;
        pStatus->LFEDownmixVolume = pDmParams->pStatus->LFEDownmixVolume;
        pStatus->LFEDownmixInclude = pDmParams->pStatus->LFEDownmixInclude;
        pStatus->unused = pDmParams->pStatus->unused;
        pStatus->CntrMixLev = pDmParams->pStatus->CntrMixLev;
        pStatus->SurrMixLev = pDmParams->pStatus->SurrMixLev;
#if 0
        pStatus->request.part.sat =
                testCase.channelConfigurationRequest.part.sat;
        pStatus->request.part.sub =
                testCase.channelConfigurationRequest.part.sub;
        pStatus->request.part.aux =
                testCase.channelConfigurationRequest.part.aux;
        pStatus->request.part.extMask =
                testCase.channelConfigurationRequest.part.extMask;
        pStatus->request.part.extMask2 =
                testCase.channelConfigurationRequest.part.extMask2;
        pStatus->request.part.extMask3 =
                testCase.channelConfigurationRequest.part.extMask3;
        pStatus->request.part.reserved1 =
                testCase.channelConfigurationRequest.part.reserved1;
        pStatus->request.part.reserved2 =
                testCase.channelConfigurationRequest.part.reserved2;
#else
        pStatus->request.full = testCase.channelConfigurationRequest.full;
#endif
        /************************************************************
         Reset DmHandle
         *************************************************************/

        DM_reset(DmHandle, &PAfrm);
        if (error == RETRUN_ERROR) 
        {
            LOG("Failed while Reset DmHandle \n");
            return 0;
        }

#ifdef PROFILER
/* Init and clear profiling variables */
        Prfl_Init(&profiler);
#endif

        /************************************************************
         Initialize and configure the audio file
         module as per test specification
         *************************************************************/
        LOG(" \n Input file : %s \n output file name : %s\n", inFileName,
                outFileName);
        if (inFileName != NULL && outFileName != NULL) 
        {
            error = PAF_FIO_Init(ppAF, inFileName, outFileName);
            if (error != RETRUN_SUCCESS) 
            {
                LOG("Failed at opening ip_file %s \n op_file = %s", inFileName,
                        outFileName);
                return 0;
            }

        }
        error = PAF_FIO_Config(ppAF, &PAfrm, ppAF->inBufChannelCount);
        if (error != RETRUN_SUCCESS) 
        {
            LOG("Failed at Extracting basic Info From the I/P file");
            return 0;
        }

        PAfrm.sampleRate = PAF_SAMPLERATE_48000HZ;
        PAfrm.sampleCount = ppAF->inBufSampleCount;

        /************************************************************
         DM_apply call
         *************************************************************/

        while (PAF_FIO_Read(ppAF, &PAfrm) == RETRUN_SUCCESS) 
        {

#ifdef PROFILER
            Prfl_Start(&profiler);
#endif

            //Actual Processing call for Sample rate conversion
            DM_apply(DmHandle, &PAfrm);

#ifdef PROFILER
            Prfl_Stop(&profiler);
#endif
            LOG("Framecnt = %d \n", framecnt++);
            PAF_FIO_Write(ppAF, &PAfrm);
        }

#ifdef PROFILER

        LOG("Total Process Cycles %llu, Peak Process Cycles %llu "
                "Process Blocks size %d "
                "Process Count %u "
                "Input sample rate %d "
                "\n****************\n", profiler.total_process_cycles,
                profiler.peak_process_cycles,
                Frame_size, profiler.process_count,
                SAMPLING_RATE);

        {   // write porfiling info in file
            FILE *pStdout_profile = NULL;
#ifdef ARMCOMPILE
       pStdout_profile = fopen("$path$//dm_profile.txt","a");
#else
       pStdout_profile = fopen("../../test_vectors/output/dm_profile.txt", "a");
#endif
            fprintf(pStdout_profile,
                    "DM Test Case %s: \nTotal Process Cycles %llu, "
                            "\nPeak Process Cycles %llu "
                            "\nProcess Blocks size %d "
                            "\nProcess Count %u "
                            "\ninput channel count = %d"
                            "\noutput channel count = %d"
                            "\nInput sample rate %d", testCase.charTestCaseName,
                    profiler.total_process_cycles, profiler.peak_process_cycles,
                    Frame_size, profiler.process_count,
                    testCase.numOfInChannels, testCase.numOfOutChannels,
                    SAMPLING_RATE);
            fprintf(pStdout_profile, "\n****************\n");
            fclose(pStdout_profile);
        }

#endif
        free(&PAfrm.data.sample[0][0]);
        FIO_DeInit(ppAF);
    }Frame_size=Frame_size+256;
}
    PAF_FIO_DeInit(ppAF, &PAfrm);
    if (use_cache)
        memarchcfg_cacheFlush();
    DM_delete(DmHandle);
    return 0;
}
/***************************End of file***************************************/
