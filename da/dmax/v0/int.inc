/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef INT_
#define INT_

// The INT service routine facilitates interrupt generation to
// the DSP. Parameter Table:
//
// 31        24        16        8         0  
//  _______________________________________
// | TCC     | TCINT   | BIT     | CTRL    |
// |_________|_________|___ _____|_________|
// |                 ADD                   |
// |_______________________________________|
//
// where CTRL is a bitmap:
//
// 8     7                    2     1      0 
//  _______________________________________
// | LVL | RES                | POL | SOFT |
// |_____|____________________|_____|______| 
//
#define INT_SOFT_DIS    0        // INT Soft interrupt disable  
#define INT_SOFT_ENA    1        // INT Soft interrupt enable  
#define INT_SOFT_MSK    1        // INT Soft interrupt mask  
#define INT_SOFT_SHFT   0        // INT Soft interrupt shift

#define INT_EDGE_RIS    0        // INT Soft interrupt Edge rising 
#define INT_EDGE_FAL    1        // INT Soft interrupt Edge falling  
#define INT_EDGE_MSK    1        // INT Soft interrupt Edge mask  
#define INT_EDGE_SHFT   1        // INT Soft interrupt Edge shift

#define INT_LVL_LOW     0        // INT Soft interrupt Level low 
#define INT_LVL_HGH     1        // INT Soft interrupt Level high  
#define INT_LVL_MSK     1        // INT Soft interrupt Level mask  
#define INT_LVL_SHFT    7        // INT Soft interrupt Level shift

// INT parameter field lengths and offsets
#define INT_CTRL_OFF    0        // INT ctrl offset
#define INT_CTRL_LEN    1        // INT ctrl length
#define INT_BIT_OFF     1        // INT bit offset
#define INT_BIT_LEN     1        // INT bit length
#define INT_TCINT_OFF   2        // INT tcint offset
#define INT_TCINT_LEN   1        // INT tcint length
#define INT_TCC_OFF     3        // INT tcc offset
#define INT_TCC_LEN     1        // INT tcc length
#define INT_ADD_OFF     4        // INT add offset
#define INT_ADD_LEN     4        // INT add length

#endif // INT_
