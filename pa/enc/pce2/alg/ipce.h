
/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Common PCM Encoder algoritm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the PCM Encoder algoritm.
 */
#ifndef IPCE_
#define IPCE_

#include <ti/xdais/ialg.h> //<ialg.h>
#include <ti/xdais/xdas.h>
//#include <ti/procsdk_audio/procsdk_audio_typ.h>
#include <procsdk_audio_typ.h>
#include <alg.h>
#include "icom.h"
#include "pafenc.h"

#define PCE_ALIGN_128    1
/*
 *  ======== IPCE_Obj ========
 *  Every implementation of IPCE *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IPCE_Obj {
    struct IPCE_Fxns *fxns;    /* function list: standard, public, private */
} IPCE_Obj;

/*
 *  ======== IPCE_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IPCE_Obj *IPCE_Handle;

#define IPCE_PHASES 6

/*
 *  ======== IPCE_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.
 */
typedef volatile struct IPCE_StatusPhase {
    XDAS_Int8 mode;
    XDAS_Int8 type;
} IPCE_StatusPhase;

typedef volatile struct IPCE_StatusDelay {
    XDAS_Int8 unused;
    XDAS_Int8 unit;
    XDAS_Int8 numc;
    XDAS_Int8 nums;
    XDAS_UInt16 delay[PAF_MAXNUMCHAN_AF];
    XDAS_UInt16 masterDelay;
} IPCE_StatusDelay;

typedef volatile struct IPCE_Status {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 type; /* unused */
    IPCE_StatusPhase phase[IPCE_PHASES];
    IPCE_StatusDelay del;
    XDAS_Int8 pceExceptionDetect; //Floating Point Exception  SPRU733A
    XDAS_Int8 pceExceptionFlag;   //val=-1s*2^e-127* 1.f, 0<e<255
    XDAS_Int8 pceExceptionMute;   //Cases where val=+Inf,-Inf,NaN,QnaN,SnaN & e=255 is flagged as exception.
    XDAS_Int8 pceClipDetect;      //Added elements should make 16bit alignment
    XDAS_Int8 bsMetadata_type;
    XDAS_Int8 mdInsert;
    XDAS_Int8 maxNumChMd;
} IPCE_Status;

/*
 *  ======== IPCE_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IPCE_ConfigPhaseVolume {
    Int size;
    float volumeRamp;
} IPCE_ConfigPhaseVolume;

typedef struct IPCE_ConfigPhaseDelay {
    Int size;
    union {
        PAF_ChannelMask_HD bits;
        XDAS_Int32 not;
    } mask;
    Int sizeofElement;
    PAF_DelayState * state;
} IPCE_ConfigPhaseDelay;

typedef struct IPCE_ConfigPhaseOutput {
    Int size;
} IPCE_ConfigPhaseOutput;

typedef struct IPCE_ConfigPhase {
    union {
        Int size;
        IPCE_ConfigPhaseVolume pConfigPhaseVolume;
        IPCE_ConfigPhaseDelay  pConfigPhaseDelay;
        IPCE_ConfigPhaseOutput pConfigPhaseOutput;
    } pConfigPhaseData;
} IPCE_ConfigPhase;

typedef struct IPCE_Config {
    Int size;
    XDAS_Int16 frameLength;
    XDAS_Int16 unused;
    IPCE_ConfigPhase * pConfigPhase[IPCE_PHASES];
    PAF_IALG_Common * pConfigCommon[IPCE_PHASES];
    PAF_AudioData scale[PAF_MAXNUMCHAN_AF];
} IPCE_Config;

/*
 *  ======== IPCE_Active ========
 *  Active structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm but which must maintain value
 *  only while the algorithm is running.
 */
typedef struct PAF_ActivePhaseVolume {
    Int size;
    PAF_VolumeStatus *pVolumeStatus;
} PAF_ActivePhaseVolume;

typedef struct PAF_ActivePhaseDelay {
    Int size;
} PAF_ActivePhaseDelay;

typedef struct PAF_ActivePhaseOutput {
    Int size;
    PAF_EncodeStatus *pEncodeStatus;
    PAF_OutBufConfig *pOutBufConfig;
} PAF_ActivePhaseOutput;

typedef struct PAF_ActivePhase {
    union {
        Int size;
        PAF_ActivePhaseVolume *pActivePhaseVolume;
        PAF_ActivePhaseDelay  *pActivePhaseDelay;
        PAF_ActivePhaseOutput *pActivePhaseOutput;
    } pActivePhaseData;
} PAF_ActivePhase;

typedef struct IPCE_Active {
    Int size;
    Int flag;
    PAF_ActivePhase  *pActivePhase[IPCE_PHASES];
    union {
        PAF_ChannelMask_HD bits;
        XDAS_Int32 not;
    } bitstreamMask;
} IPCE_Active;

/*
 *  ======== IPCE_Scrach ========
 *  Scrach structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm and which must maintain value
 *  while the algorithm is running during a single invocation.
 */
typedef struct PAF_ScrachPhaseVolume {
    Int size;
} PAF_ScrachPhaseVolume;

typedef struct PAF_ScrachPhaseDelay {
    Int size;
} PAF_ScrachPhaseDelay;

typedef struct PAF_ScrachPhaseOutput {
    Int size;
} PAF_ScrachPhaseOutput;

typedef struct PAF_ScrachPhase {
    union {
        Int size;
        PAF_ScrachPhaseVolume *pScrachPhaseVolume;
        PAF_ScrachPhaseDelay  *pScrachPhaseDelay;
        PAF_ScrachPhaseOutput *pScrachPhaseOutput;
    } pScrachPhaseData;
} PAF_ScrachPhase;

typedef struct IPCE_Scrach {
    Int size;
    PAF_ScrachPhase *pScrachPhase[IPCE_PHASES];
} IPCE_Scrach;

/*
 *  ======== IPCE_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a PCE object.
 *
 *  Every implementation of IPCE *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */
typedef struct IPCE_Params {
    Int size;
    const IPCE_Status *pStatus;
    const IPCE_Config *pConfig;
    const IPCE_Active *pActive;
    const IPCE_Scrach *pScrach;
} IPCE_Params;

/*
 *  ======== IPCE_PARAMS ========
 *  Default instance creation parameters (defined in ipce.c)
 */
extern const IPCE_Params IPCE_PARAMS;

/*
 *  ======== IPCE_Fxns ========
 *  All implementation's of PCE must declare and statically
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is PCE_<vendor>_IPCE, where
 *  <vendor> is the vendor name.
 */
typedef struct IPCE_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IPCE_Handle, ALG_Handle, PAF_EncodeControl *, PAF_EncodeStatus *);
    Int         (*info)(IPCE_Handle, ALG_Handle, PAF_EncodeControl *, PAF_EncodeStatus *);
    Int         (*encode)(IPCE_Handle, ALG_Handle, PAF_EncodeInStruct *, PAF_EncodeOutStruct *);
    /* private */
    Int         (*phaseInit[IPCE_PHASES])(IPCE_Handle, IPCE_StatusPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *,PAF_ActivePhase *, PAF_ScrachPhase *);
    Int         (*phaseReset[IPCE_PHASES])(IPCE_Handle, ALG_Handle, PAF_EncodeControl *, PAF_EncodeStatus *, IPCE_StatusPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *);
    Int         (*phaseInfo[IPCE_PHASES])(IPCE_Handle, PAF_EncodeControl *, PAF_EncodeStatus *, PAF_ActivePhase *, PAF_ScrachPhase *);
    Int         (*phase[IPCE_PHASES])(IPCE_Handle, PAF_EncodeInStruct *, PAF_EncodeOutStruct *, IPCE_StatusPhase *, IPCE_ConfigPhase *, PAF_IALG_Common *, PAF_ActivePhase *, PAF_ScrachPhase *, PAF_ChannelMask_HD);
//    Int         (*activatePhase)(IALG_Handle, PAF_IALG_Common *);
//    Void        (*deactivatePhase)(IALG_Handle, PAF_IALG_Common *);
    Int         (*outputCheck)(IPCE_Handle, PAF_ActivePhase *, int, int);
    Int         (*outputAudio)(IPCE_Handle, PAF_ActivePhase *, PAF_AudioData *, PAF_AudioData, int);
    Int         (*outputCopy)(IPCE_Handle, PAF_ActivePhase *, float *, int);
    Int         (*outputFinal)(IPCE_Handle, PAF_ActivePhase *, int, int);
    Int         (*delay)(PAF_AudioFrame *, PAF_DelayState *, Int, Int, Int, PAF_AudioData, Int);
    Int         (*delayDAT)(PAF_AudioFrame *, PAF_DelayState *, SmInt *, Int, Int, Int, PAF_AudioData, Int);
    Int         (*outputMdInsert)(IPCE_Handle, PAF_ActivePhase *, PAF_AudioFrame *, PAF_ChannelMask_HD);
} IPCE_Fxns;

/*
 *  ======== IPCE_Cmd ========
 *  The Cmd enumeration defines the control commands for the PCE
 *  control method.
 */
typedef enum IPCE_Cmd {
    IPCE_NULL                   = ICOM_NULL,
    IPCE_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IPCE_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IPCE_GETSTATUS              = ICOM_GETSTATUS,
    IPCE_SETSTATUS              = ICOM_SETSTATUS,
    IPCE_MINSAMGEN,
    IPCE_MININFO,
    IPCE_MAXSAMGEN
} IPCE_Cmd;

#endif  /* IPCE_ */
