[in, fs, nbit] = wavread("../input/input.wav");
fs
nbit


#************************************************************
#                      1 TAP IIR Filter
#************************************************************
Rp=.18;
Rs=60;
Fp=3000/(fs/2) ;             
[b,a]=ellip(1, Rp ,Rs, Fp, 'low');

out_1tap_fc3k_lp = filter(b,a,in);
#************************************************************
Rp=.2;
Rs=30;
Fp=3000/(fs/2);         
[b,a]=ellip(1, Rp , Rs, Fp, 'high');

out_1tap_fc3k_hp = filter(b,a,in);
#************************************************************
Rp2=1;
Rs2=30;
Fp2=800/(fs/2);         
[b2,a2]=ellip(1, Rp2 , Rs2, Fp2, 'high');

out_1tap_fc800_hp = filter(b2,a2,in);
#************************************************************

wavwrite(out_1tap_fc3k_lp,
         48000, 
         "../reference/Filter_iirT1Ch1.wav");
wavwrite([out_1tap_fc3k_lp, \
          out_1tap_fc3k_hp], 
          48000, 
          "../reference/Filter_iirT1Ch2.wav");
wavwrite([out_1tap_fc3k_hp,  \
          out_1tap_fc800_hp, \
          out_1tap_fc3k_lp], 
          48000, 
          "../reference/Filter_iirT1Ch3.wav");
          
wavwrite([out_1tap_fc3k_lp,  \
          out_1tap_fc3k_hp,  \
          out_1tap_fc800_hp, \
          out_1tap_fc3k_lp], 
          48000, 
          "../reference/Filter_iirT1Ch4.wav");

wavwrite([out_1tap_fc3k_hp, \
          out_1tap_fc3k_hp, \
          out_1tap_fc3k_hp, \
          out_1tap_fc3k_hp, \
          out_1tap_fc3k_hp], 
          48000, 
          "../reference/Filter_iirT1Ch5_u.wav");

wavwrite([out_1tap_fc3k_hp, \
          out_1tap_fc3k_hp, \
          out_1tap_fc3k_hp, \
          out_1tap_fc3k_hp, \
          out_1tap_fc3k_hp, \
          out_1tap_fc3k_hp], 
          48000, 
          "../reference/Filter_iirT1Ch6_ui.wav");
          
wavwrite([out_1tap_fc3k_hp,  \
          out_1tap_fc3k_hp,  \
          out_1tap_fc3k_hp,  \
          out_1tap_fc3k_hp,  \
          out_1tap_fc3k_hp,  \
          out_1tap_fc3k_hp,  \
          out_1tap_fc3k_hp], 
          48000, 
          "../reference/Filter_iirT1Ch7_ui.wav");  
  
wavwrite([out_1tap_fc3k_hp,  \
          out_1tap_fc3k_hp,  \
          out_1tap_fc3k_lp,  \
          out_1tap_fc3k_lp,  \
          out_1tap_fc3k_lp,  \
          out_1tap_fc800_hp, \
          out_1tap_fc800_hp, \
          out_1tap_fc800_hp], 
          48000, 
          "../reference/Filter_iirT1ChN.wav");   

#************************************************************
#                      2 TAP IIR Filter
#************************************************************

Rp=.5;
Fp=2000/(fs/2) ;                  
[b,a]=cheby1(2, Rp, Fp, 'low'); 

out_2tap_fc2k_lp = filter(b,a,in);
#************************************************************
Rp=.009;
Fp=3000/(fs/2);                   
[b,a]=cheby1(2, Rp, Fp, 'low');

out_2tap_fc3k_lp = filter(b,a,in);
#************************************************************
Rp=6;
Rs=50;
Fp=10000/(fs/2) ;             
[b,a]=ellip(2, Rp , Rs, Fp, 'high');

out_2tap_fc10k_hp = filter(b,a,in);
#************************************************************
Rp=.009; 
Fp1=3000/(fs/2) ;    
Fp2=5000/(fs/2) ;        
[b,a]=cheby1(1, Rp, [Fp1,Fp2] );
 
out_2tap_fl3k_fh5k_bdpass = filter(b,a,in);
#************************************************************
Rp=6;
Rs=60;
Fp1=2000/(fs/2) ;    
Fp2=7000/(fs/2)  ;       
[b,a]=ellip(1, Rp , Rs, [Fp1,Fp2], 'stop');

out_2tap_fl2k_fh7k_bdstop = filter(b,a,in);
#************************************************************

wavwrite(out_2tap_fc2k_lp,
         48000, 
         "../reference/Filter_iirT2Ch1.wav");
wavwrite([out_2tap_fc2k_lp, \
          out_2tap_fc3k_lp], 
          48000, 
          "../reference/Filter_iirT2Ch2.wav");
wavwrite([out_2tap_fl3k_fh5k_bdpass,  \
          out_2tap_fc2k_lp,           \
          out_2tap_fl3k_fh5k_bdpass], 
          48000, 
          "../reference/Filter_iirT2Ch3.wav");
          
wavwrite([out_2tap_fl2k_fh7k_bdstop,  \
          out_2tap_fl2k_fh7k_bdstop,  \
          out_2tap_fl2k_fh7k_bdstop,  \
          out_2tap_fl2k_fh7k_bdstop], 
          48000, 
          "../reference/Filter_iirT2Ch4_u.wav");
          
wavwrite([out_2tap_fl2k_fh7k_bdstop,  \
          out_2tap_fl2k_fh7k_bdstop,  \
          out_2tap_fl2k_fh7k_bdstop,  \
          out_2tap_fl2k_fh7k_bdstop], 
          48000, 
          "../reference/Filter_iirT2Ch4_ui.wav");

wavwrite([out_2tap_fc3k_lp,  \
          out_2tap_fc3k_lp,  \
          out_2tap_fc10k_hp, \
          out_2tap_fc2k_lp,  \
          out_2tap_fc2k_lp,  \
          out_2tap_fc2k_lp,  \
          out_2tap_fc10k_hp], 
          48000, 
          "../reference/Filter_iirT2ChN.wav");

wavwrite([out_2tap_fl2k_fh7k_bdstop, \
          out_2tap_fl2k_fh7k_bdstop, \
          out_2tap_fc2k_lp,          \
          out_2tap_fc2k_lp,          \
          out_2tap_fl3k_fh5k_bdpass, \
          out_2tap_fl3k_fh5k_bdpass, \
          out_2tap_fl3k_fh5k_bdpass, \
          out_2tap_fl3k_fh5k_bdpass], 
          48000, 
          "../reference/Filter_iirTNChN.wav");

#************************************************************
#                      2 TAP FIR Filter
#************************************************************

Fp=3000/(fs/2);
[b] = fir1(2, Fp);
a = 1;

out_2tap_fir_fc3k_lp = filter(b,a,in);
#************************************************************

wavwrite([out_2tap_fir_fc3k_lp, \
          out_2tap_fir_fc3k_lp, \
          out_2tap_fir_fc3k_lp, \
          out_2tap_fir_fc3k_lp, \
          out_2tap_fir_fc3k_lp, \
          out_2tap_fir_fc3k_lp, \
          out_2tap_fir_fc3k_lp, \
          out_2tap_fir_fc3k_lp], 
          48000, 
          "../reference/Filter_firTNChN.wav");
#************************************************************
#                      20 TAP FIR Filter
#************************************************************

Fp=3000/(fs/2);
[b] = fir1(20, Fp);
a = 1;

out_20tap_fir_fc3k_hp = filter(b,a,in);
#************************************************************

wavwrite(out_20tap_fir_fc3k_hp,
         48000, 
         "../reference/Filter_firTNCh1.wav");
#************************************************************
#                      1 TAP IIR MX Filter
#************************************************************          

wavwrite([out_1tap_fc3k_hp], 
          48000, 
          "../reference/Filter_iirT1Ch1_mx.wav");
#************************************************************
#                      2 TAP IIR MX Filter
#************************************************************          

wavwrite([out_2tap_fc10k_hp], 
          48000, 
          "../reference/Filter_iirT2Ch1_mx.wav");
          
wavwrite([out_2tap_fc3k_lp], 
          48000, 
          "../reference/Filter_iirT2Ch1_mx_s.wav");
          
wavwrite([out_2tap_fl3k_fh5k_bdpass], 
          48000, 
          "../reference/Filter_iirT2Ch1_ui_mx.wav");
          
wavwrite([out_2tap_fc3k_lp,  \
          out_2tap_fc10k_hp], 
          48000, 
          "../reference/Filter_iirT2Ch2_mx.wav");
          
wavwrite([out_2tap_fc10k_hp, \
          out_2tap_fc10k_hp], 
          48000, 
          "../reference/Filter_iirT2Ch2_u_mx.wav");
          
wavwrite([out_2tap_fc3k_lp, \
          out_2tap_fc3k_lp], 
          48000, 
          "../reference/Filter_iirT2Ch2_ui_mx.wav");
          
wavwrite([out_2tap_fl3k_fh5k_bdpass, \
          out_2tap_fc3k_lp,          \
          out_2tap_fc10k_hp], 
          48000, 
          "../reference/Filter_iirT2ChN_mx.wav");
#************************************************************
#                      4 TAP IIR MX Filter
#************************************************************          
          
Rp1=3;
Rs1=60;
Fp1=2000/(fs/2) ;    
Fp2=5000/(fs/2)  ;       
[b,a]=ellip(2, Rp1 ,Rs1, [Fp1,Fp2], 'pass');   

out_4tap_fl2k_fh5k_bdpass = filter(b,a,in);      
#************************************************************
Rp=.01;
Rs=60;
Fp=2000/(fs/2) ;
[b,a]=ellip(4, Rp ,Rs, Fp, 'low');

out_4tap_fc2k_lp = filter(b,a,in);      
#************************************************************
Rp1=3;
Rs1=60;
Fp1=2000/(fs/2) ;    
Fp2=5000/(fs/2)  ;       
[b,a]=ellip(2, Rp1 ,Rs1, [Fp1,Fp2], 'stop');

out_4tap_fl2k_fh5k_bdstop = filter(b,a,in);      
#************************************************************
          
wavwrite([out_4tap_fl2k_fh5k_bdpass], 
          48000, 
          "../reference/Filter_iirT4Ch1_mx.wav");
          
wavwrite([out_4tap_fl2k_fh5k_bdstop, \
          out_4tap_fc2k_lp,          \
          out_4tap_fl2k_fh5k_bdpass], 
          48000, 
          "../reference/Filter_iirT4ChN_mx.wav");          
