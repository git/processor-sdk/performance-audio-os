
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Standard Alpha Code Processor Algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the ACP Algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the ACP
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef ACP_
#define ACP_

#include <ti/xdais/ialg.h>
#include <alg.h>
#include <iacp.h>
#include <paf_alg.h>

#include "acptype.h"
#include "acpbeta.h"

/*
 *  ======== ACP_Handle ========
 *  ACP Algorithm instance handle
 */
typedef struct IACP_Obj *ACP_Handle;

/*
 *  ======== ACP_Params ========
 *  ACP Algorithm instance creation parameters
 */
typedef IACP_Params ACP_Params;

/*
 *  ======== ACP_PARAMS ========
 *  Default instance parameters
 */
#define ACP_PARAMS IACP_PARAMS
#define ACP_PARAMS_SRECORD_SUPPORT IACP_PARAMS_SRECORD_SUPPORT

/*
 *  ======== ACP_Status ========
 *  Status structure for getting ACP instance attributes
 */
typedef volatile struct IACP_Status ACP_Status;

/*
 *  ======== ACP_Cmd ========
 *  This typedef defines the control commands ACP objects
 */
typedef IACP_Cmd   ACP_Cmd;

/*
 * ===== control method commands =====
 */
#define ACP_NULL IACP_NULL
#define ACP_GETSTATUSADDRESS1 IACP_GETSTATUSADDRESS1
#define ACP_GETSTATUSADDRESS2 IACP_GETSTATUSADDRESS2
#define ACP_GETSTATUS IACP_GETSTATUS
#define ACP_SETSTATUS IACP_SETSTATUS
#define ACP_GETBETAPRIMEVALUE IACP_GETBETAPRIMEVALUE
#define ACP_SETBETAPRIMEVALUE IACP_SETBETAPRIMEVALUE
#define ACP_GETBETAPRIMEBASE IACP_GETBETAPRIMEBASE
#define ACP_GETBETAPRIMEOFFSET IACP_GETBETAPRIMEOFFSET

/*
 *  ======== ACP_create ========
 *  Create an instance of a ACP object.
 */
static inline ACP_Handle ACP_create(const IACP_Fxns *fxns, const ACP_Params *prms)
{
    return ((ACP_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== ACP_attach ========
 */
extern Int ACP_attach(ACP_Handle, Int, Int, IALG_Status *);

/*
 *  ======== ACP_apply ========
 */
extern Int ACP_apply(ACP_Handle, const ACP_Unit *, ACP_Unit *);

/*
 *  ======== ACP_sequence ========
 */
extern Int ACP_sequence(ACP_Handle, const ACP_Unit *, ACP_Unit *);

/*
 *  ======== ACP_srecord ========
 */
extern Int ACP_srecord(ACP_Handle, char *, char *);

/*
 *  ======== ACP_escape ========
 */
extern Int ACP_escape(ACP_Handle, const ACP_Unit *, ACP_Unit *);

/*
 *  ======== ACP_legacy ========
 */
extern Int ACP_legacy(ACP_Handle, const ACP_Unit *, ACP_Unit *);

/*
 *  ======== ACP_length ========
 */
extern Int ACP_length(ACP_Handle, const ACP_Unit *);

/*
 *  ======== ACP_htgnel ========
 */
extern Int ACP_htgnel(ACP_Handle, const ACP_Unit *);

/*
 *  ======== ACP_delete ========
 *  Delete a ACP instance object
 */
static inline Void ACP_delete(ACP_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== ACP_exit ========
 *  Module finalization
 */
extern Void ACP_exit(Void);

/*
 *  ======== ACP_init ========
 *  Module initialization
 */
extern Void ACP_init(Void);

#endif  /* ACP_ */
