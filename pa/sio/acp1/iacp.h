
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common Alpha Code Processor Algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the ACP Algorithm.
 */
#ifndef IACP_
#define IACP_

#include <ti/xdais/ialg.h>
#include <icom.h>
#include "acptype.h"

// Fixed compiler bug "improper structure initialization"
// #define CINITBUGFIXED /* Not! */
// Patched compiler bug "improper structure initialization"
// #define CINITBUGPATCHED

/*
 *  ======== IACP_Obj ========
 *  Every implementation of IACP *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IACP_Obj {
    struct IACP_Fxns *fxns;    /* function list: standard, public, private */
} IACP_Obj;

/*
 *  ======== IACP_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IACP_Obj *IACP_Handle;

/*
 *  ======== IACP_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.
 */
typedef volatile struct IACP_Status {
    Int size;
    XDAS_UInt8  betaPrimeValue;
    XDAS_UInt8  betaPrimeBase;
    XDAS_UInt16 betaPrimeOffset;
    XDAS_Int32 unused[2];
    XDAS_Int32 args[4];
} IACP_Status;

/*
 *  ======== IACP_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef volatile struct IACP_BetaTable {
    Int size;
    IALG_Status *pStatus[1];
} IACP_BetaTable;
typedef XDAS_Int32 IACP_PhiFunction(XDAS_Int32, XDAS_Int32, XDAS_Int32, XDAS_Int32);
typedef volatile struct IACP_PhiTable {
    Int size;
    IACP_PhiFunction *pFunction[1];
} IACP_PhiTable;
typedef volatile struct IACP_SigmaTable {
    Int size;
    const ACP_Unit *pSequence[1];
} IACP_SigmaTable;
typedef struct IACP_SRecordState {
    XDAS_Int16 fromSequenceLength;
    XDAS_Int16 toSequenceLength;
    ACP_Unit *fromSequence;
    ACP_Unit *toSequence;
    XDAS_Int16 fromBytes; /* initialize to -1 */
    XDAS_Int16 fromSequenceCount;
    XDAS_Int16 toPlace;
    XDAS_Int16 toCount;
    XDAS_Int16 toStart;
    char *from;
} IACP_SRecordState;
typedef struct IACP_Config {
    IACP_BetaTable **betaTable;
    IACP_PhiTable **phiTable;
    IACP_SigmaTable **sigmaTable;
    IACP_SRecordState *pSRecordState;
} IACP_Config;

/*
 *  ======== IACP_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a ACP object.
 *
 *  Every implementation of IACP *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */
typedef struct IACP_Params {
    Int size;
    const IACP_Status *pStatus;
    IACP_Config config;
} IACP_Params;

/*
 *  ======== IACP_PARAMS ========
 *  Default instance creation parameters (defined in iacp.c)
 */
extern const IACP_Params IACP_PARAMS;
extern const IACP_Params IACP_PARAMS_SRECORD_SUPPORT;

/*
 *  ======== IACP_Fxns ========
 *  All implementation's of ACP must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is ACP_<vendor>_IACP, where
 *  <vendor> is the vendor name.
 */
typedef struct IACP_Fxns {
    /* standard */
    IALG_Fxns   ialg;
    /* public */
    Int         (*attach)(IACP_Handle, Int, Int, IALG_Status *);
    Int         (*apply)(IACP_Handle, const ACP_Unit *, ACP_Unit *);
    Int         (*length)(IACP_Handle, const ACP_Unit *);
    Int         (*htgnel)(IACP_Handle, const ACP_Unit *);
    Int         (*sequence)(IACP_Handle, const ACP_Unit *, ACP_Unit *);
    Int         (*srecord)(IACP_Handle, char *, char *);
    /* private */
    Int         (*escape)(IACP_Handle, const ACP_Unit *, ACP_Unit *);
    Int         (*legacy)(IACP_Handle, const ACP_Unit *, ACP_Unit *);
//#ifdef IROM
    /* IROM specific pointers */
    Void (*sprintf1)(IACP_Handle, char *p, int arg1, int arg2); 
    Void (*sprintf2)(IACP_Handle, char *p, int arg1, int arg2);
    Void (*sprintf3)(IACP_Handle, char *p, int arg1);
//#endif
} IACP_Fxns;

/*
 *  ======== IACP_Cmd ========
 *  The Cmd enumeration defines the control commands for the ACP
 *  control method.
 */
typedef enum IACP_Cmd {
    IACP_NULL                   = ICOM_NULL,
    IACP_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    IACP_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    IACP_GETSTATUS              = ICOM_GETSTATUS,
    IACP_SETSTATUS              = ICOM_SETSTATUS,
    IACP_GETBETAPRIMEVALUE,
    IACP_SETBETAPRIMEVALUE,
    IACP_GETBETAPRIMEBASE,
    IACP_GETBETAPRIMEOFFSET
} IACP_Cmd;

#endif  /* IACP_ */
