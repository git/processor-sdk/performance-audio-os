
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*   ==========================================================================
 *   Copyright (c) Texas Instruments Inc , 2004
 *
 *   Use of this software is controlled by the terms and conditions found
 *   in the license agreement under which this software has been supplied
 *   provided
 *   ==========================================================================
*/

/* ---- File: <csl_resId.h> ---- */
/* for the ARM-side of Primus */
#ifndef _CSL_RESID_H_
#define _CSL_RESID_H_

/* ---- Total number of modues : CSL Abstraction ---- */
/* Module ID 0 is reserved. */
#define CSL_CHIP_NUM_MODULES   	  (25)

#define CSL_KEYMGR_ID              (1)
#define CSL_UART_ID                (2)
#define CSL_SPI_ID                 (3)
#define CSL_DMAX_ID                (4)
#define CSL_RTC_ID          	   (5)
#define CSL_I2C_ID                 (6)
#define CSL_GPIO_ID                (7)
#define CSL_EMIF_ID                (8)
#define CSL_PLLC_ID                (9)
#define CSL_AEMIF_ID               (10)
#define CSL_TMR_ID                 (11) 
#define CSL_UHPI_ID                (12) 
#define CSL_MCASP_ID               (13)
#define CSL_EDMA_ID                (14)
#define CSL_USB_ID                 (15)
#define CSL_INTC_ID                (16)
#define CSL_LCDC_ID                (17)
#define CSL_MPU_ID                 (18)
#define CSL_IOPU_ID                (19)
#define CSL_EMAC_ID                (20)

#define CSL_HRPWM_ID               (21)
#define CSL_ECAP_ID                (22)
#define CSL_EQEP_ID                (23)
#define CSL_EPWM_ID                (24)
#define CSL_MMCSD_ID                (25)


/* ---- Total Number of Resource, one-to-one correspondence with H/W resource */
/* Resource ID - 0 is resvd. */
#define CSL_CHIP_NUM_RESOURCES  (83)

#define CSL_KEYMGR_0_UID              (1)

#define CSL_HRPWM_0_UID              (2)
#define CSL_HRPWM_1_UID              (3)
#define CSL_HRPWM_2_UID              (4)

#define CSL_ECAP_0_UID              (5)
#define CSL_ECAP_1_UID              (6)
#define CSL_ECAP_2_UID              (7)

#define CSL_EQEP_0_UID              (8)
#define CSL_EQEP_1_UID              (9)


#define CSL_UART_0_UID             (10)
#define CSL_UART_1_UID             (12)
#define CSL_UART_2_UID             (13)

#define CSL_SPI_0_UID              (14)
#define CSL_SPI_1_UID              (15)

#define CSL_LCDC_0_UID              (16)

#define CSL_RTC_0_UID              (17)

#define CSL_I2C_0_UID              (18)
#define CSL_I2C_1_UID              (19)

#define CSL_GPIO_UID               (20)	

#define CSL_EMIF_0_UID             (21)

#define CSL_PLLC_0_UID             (22)

#define CSL_AEMIF_0_UID              (23)

#define CSL_TMR_0_UID              (24)
#define CSL_TMR_1_UID              (25)
#define CSL_TMR_2_UID              (26)

#define CSL_UHPI_0_UID             (27)

#define CSL_MCASP_0_UID            (28)
#define CSL_MCASP_1_UID            (29)
#define CSL_MCASP_2_UID            (30)


#define CSL_USB_0_UID              (31)
#define CSL_USB_1_UID              (32)

#define CSL_INTC_UID               (33)

#define CSL_IOPU_0_UID               (34)
#define CSL_IOPU_1_UID               (35)

#define CSL_MPU_0_UID             (36)
#define CSL_MPU_1_UID             (37)

#define CSL_EMAC_0_UID             (38)

#define CSL_DMAX_0_UID             (39)

#define CSL_EPWM_0_UID              (40)
#define CSL_EPWM_1_UID              (41)
#define CSL_EPWM_2_UID              (42)

#define CSL_EDMA_CHA0_UID          (43)
#define CSL_EDMA_CHA1_UID          (44)
#define CSL_EDMA_CHA2_UID          (45)
#define CSL_EDMA_CHA3_UID          (46)
#define CSL_EDMA_CHA4_UID          (47)
#define CSL_EDMA_CHA5_UID          (48)
#define CSL_EDMA_CHA6_UID          (49)
#define CSL_EDMA_CHA7_UID          (50)
#define CSL_EDMA_CHA8_UID          (51)
#define CSL_EDMA_CHA9_UID          (52)
#define CSL_EDMA_CHA10_UID         (53)
#define CSL_EDMA_CHA11_UID         (54)
#define CSL_EDMA_CHA12_UID         (55)
#define CSL_EDMA_CHA13_UID         (56)
#define CSL_EDMA_CHA14_UID         (57)
#define CSL_EDMA_CHA15_UID         (58)
#define CSL_EDMA_CHA16_UID         (59)
#define CSL_EDMA_CHA17_UID         (60)
#define CSL_EDMA_CHA18_UID         (61)
#define CSL_EDMA_CHA19_UID         (62)
#define CSL_EDMA_CHA20_UID         (63)
#define CSL_EDMA_CHA21_UID         (64)
#define CSL_EDMA_CHA22_UID         (65)
#define CSL_EDMA_CHA23_UID         (66)
#define CSL_EDMA_CHA24_UID         (67)
#define CSL_EDMA_CHA25_UID         (68)
#define CSL_EDMA_CHA26_UID         (69)
#define CSL_EDMA_CHA27_UID         (70)
#define CSL_EDMA_CHA28_UID         (71)
#define CSL_EDMA_CHA29_UID         (72)
#define CSL_EDMA_CHA30_UID         (73)
#define CSL_EDMA_CHA31_UID         (74)

#define     CSL_EDMA_QCHA0_UID 		   		      (75)/**< QDMA Channel 0*/
#define     CSL_EDMA_QCHA1_UID 		   		      (76)/**< QDMA Channel 1*/
#define     CSL_EDMA_QCHA2_UID     	   		      (77)/**< QDMA Channel 2*/
#define     CSL_EDMA_QCHA3_UID 		   		      (78)/**< QDMA Channel 3*/
#define     CSL_EDMA_QCHA4_UID 		   		      (79)/**< QDMA Channel 4*/
#define     CSL_EDMA_QCHA5_UID 		   		      (80)/**< QDMA Channel 5*/
#define     CSL_EDMA_QCHA6_UID 		   		      (81)/**< QDMA Channel 6*/
#define     CSL_EDMA_QCHA7_UID 		   		      (82)/**< QDMA Channel 7*/

#define CSL_MMCSD_0_UID              (83)

/*----  XIO masks for peripheral instances ---- */

#define CSL_KEYMGR_0_XIO              (0x00000000)

#define CSL_EPWM_0_XIO              (0x00000000)
#define CSL_EPWM_1_XIO              (0x00000000)
#define CSL_EPWM_2_XIO              (0x00000000)

#define CSL_HRPWM_0_XIO              (0x00000000)
#define CSL_HRPWM_1_XIO              (0x00000000)
#define CSL_HRPWM_2_XIO              (0x00000000)

#define CSL_ECAP_0_XIO              (0x00000000)
#define CSL_ECAP_1_XIO              (0x00000000)
#define CSL_ECAP_2_XIO              (0x00000000)

#define CSL_EQEP_0_XIO              (0x00000000)
#define CSL_EQEP_1_XIO              (0x00000000)


#define CSL_UART_0_XIO             (0x00000000)
#define CSL_UART_1_XIO             (0x00000000)
#define CSL_UART_2_XIO             (0x00000000)

#define CSL_SPI_0_XIO              (0x00000000)
#define CSL_SPI_1_XIO              (0x00000000)

#define CSL_LCDC_0_XIO              (0x00000000)

#define CSL_RTC_0_XIO            (0x00000000)

#define CSL_I2C_0_XIO              (0x00000000)
#define CSL_I2C_1_XIO              (0x00000000)

#define CSL_GPIO_XIO               (0x00000000)	

#define CSL_EMIF_0_XIO             (0x00000000)

#define CSL_PLLC_0_XIO             (0x00000000)

#define CSL_AEMIF_0_XIO              (0x00000000)

#define CSL_TMR_0_XIO              (0x00000000)
#define CSL_TMR_1_XIO              (0x00000000)
#define CSL_TMR_2_XIO              (0x00000000)

#define CSL_UHPI_0_XIO             (0x00000000)

#define CSL_MCASP_0_XIO            (0x00000000)
#define CSL_MCASP_1_XIO            (0x00000000)
#define CSL_MCASP_2_XIO            (0x00000000)


#define CSL_USB_0_XIO              (0x00000000)
#define CSL_USB_1_XIO              (0x00000000)

#define CSL_INTC_XIO               (0x00000000)

#define CSL_IOPU_0_XIO               (0x00000000)
#define CSL_IOPU_0_XIO               (0x00000000)

#define CSL_MPU_0_XIO             (0x00000000)
#define CSL_MPU_1_XIO             (0x00000000)

#define CSL_EMAC_0_XIO             (0x00000000)

#define CSL_DMAX_0_XIO             (0x00000000)

#define CSL_MMCSD_0_XIO             (0x00000000)

#define CSL_EDMA_CHA0_XIO          (0x00000000)
#define CSL_EDMA_CHA1_XIO          (0x00000000)
#define CSL_EDMA_CHA2_XIO          (0x00000000)
#define CSL_EDMA_CHA3_XIO          (0x00000000)
#define CSL_EDMA_CHA4_XIO          (0x00000000)
#define CSL_EDMA_CHA5_XIO          (0x00000000)
#define CSL_EDMA_CHA6_XIO          (0x00000000)
#define CSL_EDMA_CHA7_XIO          (0x00000000)
#define CSL_EDMA_CHA8_XIO          (0x00000000)
#define CSL_EDMA_CHA9_XIO          (0x00000000)
#define CSL_EDMA_CHA10_XIO         (0x00000000)
#define CSL_EDMA_CHA11_XIO         (0x00000000)
#define CSL_EDMA_CHA12_XIO         (0x00000000)
#define CSL_EDMA_CHA13_XIO         (0x00000000)
#define CSL_EDMA_CHA14_XIO         (0x00000000)
#define CSL_EDMA_CHA15_XIO         (0x00000000)
#define CSL_EDMA_CHA16_XIO         (0x00000000)
#define CSL_EDMA_CHA17_XIO         (0x00000000)
#define CSL_EDMA_CHA18_XIO         (0x00000000)
#define CSL_EDMA_CHA19_XIO         (0x00000000)
#define CSL_EDMA_CHA20_XIO         (0x00000000)
#define CSL_EDMA_CHA21_XIO         (0x00000000)
#define CSL_EDMA_CHA22_XIO         (0x00000000)
#define CSL_EDMA_CHA23_XIO         (0x00000000)
#define CSL_EDMA_CHA24_XIO         (0x00000000)
#define CSL_EDMA_CHA25_XIO         (0x00000000)
#define CSL_EDMA_CHA26_XIO         (0x00000000)
#define CSL_EDMA_CHA27_XIO         (0x00000000)
#define CSL_EDMA_CHA28_XIO         (0x00000000)
#define CSL_EDMA_CHA29_XIO         (0x00000000)
#define CSL_EDMA_CHA30_XIO         (0x00000000)
#define CSL_EDMA_CHA31_XIO         (0x00000000)

#define     CSL_EDMA_QCHA0_XIO 		   		      (0x00000000)/**< QDMA Channel 0*/
#define     CSL_EDMA_QCHA1_XIO 		   		      (0x00000000)/**< QDMA Channel 1*/
#define     CSL_EDMA_QCHA2_XIO     	   		      (0x00000000)/**< QDMA Channel 2*/
#define     CSL_EDMA_QCHA3_XIO 		   		      (0x00000000)/**< QDMA Channel 3*/
#define     CSL_EDMA_QCHA4_XIO 		   		      (0x00000000)/**< QDMA Channel 4*/
#define     CSL_EDMA_QCHA5_XIO 		   		      (0x00000000)/**< QDMA Channel 5*/
#define     CSL_EDMA_QCHA6_XIO 		   		      (0x00000000)/**< QDMA Channel 6*/
#define     CSL_EDMA_QCHA7_XIO 		   		      (0x00000000)/**< QDMA Channel 7*/

#endif /* _CSL_RESOURCE_H_ */

/* Rev.No.   Date/Time               ECN No.          Modifier      */
/* -------   ---------               -------          --------      */

/* 1          Jun 17                      				Vishwa      */
/*                                                                  */
/*                                */
/********************************************************************/ 
