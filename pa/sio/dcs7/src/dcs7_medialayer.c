/******************************************************************************
* Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// DCS data translation layer
//
//

#include <string.h>
//#include <ti/drv/uart/src/UART_ascii_utils.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>
/* UART LLD Header files */
#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/UART_stdio.h>
#include <ti/drv/uart/soc/UART_soc.h>

#include <ti/drv/uart/src/v0/UART_v0.h> 

/* SPI LLD Header files */
#include <ti/drv/spi/SPI.h>
#include <ti/drv/spi/src/v0/SPI_v0.h>
#include <ti/drv/spi/soc/SPI_soc.h>
/* I2C LLD Header files */
#include <ti/drv/i2c/I2C.h>

#include <ti/drv/i2c/soc/I2C_v0.h>    

#include <ti/drv/i2c/soc/I2C_soc.h>

#include "dcs7.h"
#include "dcs7_priv.h"
#include "dcs7_medialayer.h"

/******************************************************************************
 * UART defines and enums
 *****************************************************************************/

// miscellaneous size in Bytes
#define MSC_CRLF_SIZE       2
#define MSC_CHARPAIR_SIZE   2

// srecord_size sizes in Bytes/characters
#define ASCII_SRECORD_TYP_SIZE       2
#define ASCII_SRECORD_CNT_SIZE       2        // count
#define ASCII_SRECORD_CSM_SIZE       2        // Checksum
#define ASCII_SRECORD_S1_ADD_SIZE    4        // Address
#define ASCII_SRECORD_S0_S9_MAX_SIZE 12


// Srecord field offset
#define READ_S1_CNT_LOC  (MSC_CRLF_SIZE \
                         + ASCII_SRECORD_START_SIZE \
                         + ASCII_SRECORD_TYP_SIZE)
#define READ_S1_ADD_LOC  (MSC_CRLF_SIZE \
                         + ASCII_SRECORD_START_SIZE \
                         + ASCII_SRECORD_TYP_SIZE \
                         + ASCII_SRECORD_CNT_SIZE)

// hex_srecord_size sizes in Bytes/hex values
#define HEX_SRECORD_TYP_SIZE           1         // Type size
#define HEX_SRECORD_CNT_SIZE           1         // Count size
#define HEX_SRECORD_CSM_SIZE           1         // Checksum
#define HEX_SRECORD_S1_ADD_SIZE        2         // S1 address size
#define HEX_SRECORD_S1_DATA_MAX_SIZE   30
#define HEX_SRECORD_S1_MAX_SIZE        35

#define NULL_RESPONSE_SIZE             2


// srecord_string index
#define SRECORD_START_STRING  0 // S0
#define SRECORD_STOP_STRING   1 // S9
#define SRECORD_DATA_STRING   2 // S1
#define SRECORD_CRLF_STRING   3 // CRLF
#define SRECORD_NUM_STRING    4

#define CLOCK_INIT_TIMEOUT    11

static const Uint8
srecord_strings[SRECORD_NUM_STRING][ASCII_SRECORD_S0_S9_MAX_SIZE]=
{
    { 'S', '0', '0', '3', '0', '0', '0', '0', 'F', 'C', '\r', '\n' },  // SRECORD_START_STRING
    { 'S', '9', '0', '3', '0', '0', '0', '0', 'F', 'C', '\r', '\n' },  // SRECORD_STOP_STRING
    { 'S', '1' },                                                      // SRECORD_DATA_STRING
    { '\r', '\n' }                                                     // SRECORD_CRLF_STRING
};


#ifdef UARTCALLBACK
// Remark: Limitation of LLD callback fn. to pass user argument and return value.
//         Use global parameter.
static DCS7_callbackObj uartTransferObj = {NULL, 0, NULL, 0};
#endif
/******************************************************************************
 * SPI defines and enums
 *****************************************************************************/
typedef union DCS7_uint16
{
    struct
    {
        Uint8 low_byte;
        Uint8 high_byte;
    } part;
    Uint16 word_2byte;
} DCS7_uint16;

// spi sizes in Bytes
#define SPI_ACK_SIZE          2         // spiSlaveAck size
#define SPI_REC_COUNT_SIZE    2         // receive Count size
#define SPI_RES_COUNT_SIZE    2         // response Count size

#define SPI_SLAVE_ACK 0xF101
// Set count value as per dataSize, (x) in bytes
#define NUM_TRANSFER_ELEMENT(x) (spiParam->dataSize == 8 ? (x):(x/2))


/******************************************************************************
 * Common defines and enums
 *****************************************************************************/
void DCS7_swap(DCS7_uint16 *dst, DCS7_uint16 *src, Uint32 size);


/******************************************************************************
 * UART Functions definition
 *****************************************************************************/

#ifdef UARTCALLBACK
// UART CALLBACK funcions
void UART_transferCallback(UART_Handle handle, void *buf, size_t count);
void UART_transferCallback(UART_Handle handle, void *buf, size_t count)
{
    uartTransferObj.transferSize = count;  // return byte count
    // Do not post sem if data is not transfered,
    // The callback is also called at UART LLD rx/tx cancel function
    // with opMode = UART_CALLBACK_CANCEL.
    if (uartTransferObj.opMode != UART_CALLBACK_CANCEL)
    {
        // opMode indicates the UART operation that invoked the
        // Callback function.
        // For write - Post semaphore
        // For Read  - Post SWI
        if (uartTransferObj.opMode == UART_CALLBACK_WRITE)
        {
            Semaphore_post(uartTransferObj.globalSem);
        }
        else
        {
            Swi_post(uartTransferObj.swi);
        }
    }
}
#endif

/**
 *  @brief     Calculate checksum for provided bytes
 *
 *  @param[in] src         input buffer
 *
 *  @param[in] size        Number of bytes in buffer
 *
 *  @retval    checksum value
 *
 */
static Uint8 DCS7_Util_checksum(Uint8 *src, Uint32 size)
{
    Uint32 index;
    Uint8 sum = 0;

    for (index = 0; index < size; index++)
    {
        sum += src[index];
    }
    sum = ~sum;
    return (sum);
}


/**
 *  @brief     Convert Srecord ASCII to HEX
 *
 *  @param[out] dst        Destination buffer (HEX)
 *
 *  @param[in] src        Source buffer      (ASCII)
 *
 *  @param[in] size       Number of bytes in input buffer
 *
 *  @retval    converted Hex count
 *
 */
static Uint32 DCS7_Util_asciiPairToHex(Uint8 *dst, Uint8 *src, Uint32 size)
{
    Uint32 pairCnt, byteCnt;
    Uint32 retVal = 0;
    Uint8 src2byte[MSC_CHARPAIR_SIZE] = {48, 48};  // '0'

    for (byteCnt = 0; byteCnt < size; byteCnt += MSC_CHARPAIR_SIZE)
    {
        // ASCII pair = 1 Byte Hex
        for (pairCnt = 0; pairCnt < MSC_CHARPAIR_SIZE; pairCnt++)
        {
            /* For numbers from 0x0 to 0x9. */
            if ((*src >= 48) && (*src <= 57))
            {
                src2byte[pairCnt] = *src - 48; /* 48 = '0' */
            }
            /* For alphabets from A to F.*/
            if ((*src >= ((Uint8) ('A'))) && (*src <= ((Uint8) ('F'))))
            {
                src2byte[pairCnt] = *src - 55; /* 55 = ('0' + ('A' - '9' - 1)) */
            }
            /* For alphabets from a to f.*/
            if ((*src >= ((Uint8) ('a'))) && (*src <= ((Uint8) ('f'))))
            {
                src2byte[pairCnt] = *src - 87; /* 87 = ('0' + ('a' - '9' - 1)) */
            }
            /*else
            {
                return 0;
            }*/
            src++;
        }
                // lower nibble        // upper nibble
        *dst = (src2byte[1] & 0x0F) | ((src2byte[0] & 0x0F)<<4);
        dst++;
        retVal++;
    }
    return retVal;
}

/**
 *  @brief     Convert HEX to Srecord ASCII
 *
 *  @param[out] dst        Destination buffer (ASCII)
 *
 *  @param[in] src        Source buffer      (HEX)
 *
 *  @param[in] size       Number of bytes in input buffer
 *
 *  @retval    converted ASCII count
 *
 */
static Uint32 DCS7_Util_hexToAsciiPair(Uint8 *dst, Uint8 *src, Uint32 size)
{
    Uint32 pairCnt, byteCnt;
    Uint32 retVal = 0;
    Int8 srcNibble;

    for (byteCnt = 0; byteCnt < size; byteCnt++)
    {
        // ASCII pair = 1 Byte Hex
        for (pairCnt = 0; pairCnt < MSC_CHARPAIR_SIZE; pairCnt++)
        {
            // process nibble
            srcNibble = ((*src) >> (pairCnt == 0 ? 4 : 0)) & 0x0F;
            /* For numbers from 0x0 to 0x9. */
            if ((srcNibble >= 0) && (srcNibble <= 9))
            {
                *dst = srcNibble + '0';
            }
            /* For alphabets from A to F.*/
            if ((srcNibble >= 0xA) && (srcNibble <= 0xF))
            {
                *dst = (srcNibble - 0xA) + 'A';
            }
            /*else
            {
                return 0;
            }*/
            dst++;
            retVal++;
        }
        src++;
    }
    return retVal;
}

/**
 *  @brief     Return ms timeout required for transfer
 *
 *  @param[in] bytes           Number of bytes to transfer
 *
 *  @param[in] baudrate        Baudrate of transmission
 *
 *  @retval    ms timeout
 *
 */
static inline Uint32 DCS7_Uart_timeout(Uint32 bytes, Uint32 baudrate)
{
    // Total bytes *
    // bits (8 data bits + 1 Stop Bit + 1 Start Bit) *
    // 1000 ms /
    // transfer baudrate
    // + software latency
    return (((bytes * 10 * 1000) / baudrate) + 5);
}


// Copy string to dst till LF is found,
// return the size of string in dst
// For S1 record only copy      /--cnt--add--data--cs-/
// For S0,S9 record copy  /--type--cnt--add--data--cs--crlf-/
/**
 *  @brief     Extract string from Srecord
 *
 *  @param[out] dst       destination buffer
 *
 *  @param[in] src        source buffer
 *
 *  @param[in] size       number of bytes in input buffer
 *
 *  @param[in] isS1record   flag to indicate is Srecord is of type "S1"
 *
 *  @retval    size of string in dst
 *
 */
static Uint32 DCS7_SRecord_stringParser(Uint8 *dst, Uint8 *src,
                                        Uint32 size, Uint8 isS1record)
{
    int dstIndex = 0;

    if (isS1record)
    {
        if (*(src+1) != '1')
            return 0;   // not S1 record
        // skip copy of Record type for S1-record
        src += ASCII_SRECORD_TYP_SIZE;
    }

    while (dstIndex < size)
    {
        if (*src != '\n')
        {
            *dst = *src;
            src++;
            dst++;
            dstIndex++;
        }
        else
        {
            *dst = '\n';
            break;
        }
    }

    if (isS1record)  // reduce count for crlf for S1
    {
        dstIndex -= MSC_CRLF_SIZE;
    }

    return (dstIndex + 1);
}

// except pointer to size arrary. Supporting multiple S1 records
/**
 *  @brief     Form the Srecord string for transfer
 *
 *  @param[out] dst       destination buffer
 *
 *  @param[in] src        source buffer
 *
 *  @param[in] size       array, indicating number of bytes in various S1 records present in src
 *
 *  @retval    size of string in dst
 *
 */
static Uint32 DCS7_SRecord_pack(Uint8 *dst, Uint8 *src, Uint32 *size)
{
    Uint32 dstIndex = 0;
    Uint32 s1_count;
    Uint32 srcIndex = 0;

    /* Set Initial CRLF */
    memcpy((void*)(dst + dstIndex),
           &srecord_strings[SRECORD_CRLF_STRING][0],
           MSC_CRLF_SIZE);
    dstIndex += MSC_CRLF_SIZE;
    /* Set S0 record (start) */
    memcpy((void*)(dst + dstIndex),
           &srecord_strings[SRECORD_START_STRING][0],
           ASCII_SRECORD_START_SIZE);
    dstIndex += ASCII_SRECORD_START_SIZE;

    /* Set S1 record (start) */

    for (s1_count = 0; s1_count < SRECORD_RES_MAX_S1_CNT; s1_count++)
    {
        if (size[s1_count] > 0)
        {
            // Set S1 type
            memcpy((void*)(dst + dstIndex),
                   &srecord_strings[SRECORD_DATA_STRING][0],
                   ASCII_SRECORD_TYP_SIZE);
            dstIndex += ASCII_SRECORD_TYP_SIZE;
            // Set /--cnt--add--data--cs-/
            memcpy((void*)(dst + dstIndex),
                   src + srcIndex,
                   size[s1_count]);
            dstIndex += size[s1_count];
            srcIndex += size[s1_count];
            // Set CRLF
            memcpy((void*)(dst + dstIndex),
                   &srecord_strings[SRECORD_CRLF_STRING][0],
                   MSC_CRLF_SIZE);
            dstIndex += MSC_CRLF_SIZE;
        }
        else
        {
            break;
        }
    }

    /* Set S9 record (start) */
    memcpy((void*)(dst + dstIndex),
           &srecord_strings[SRECORD_STOP_STRING][0],
           ASCII_SRECORD_STOP_SIZE);

    dstIndex += ASCII_SRECORD_STOP_SIZE;

    return dstIndex;

}

// Will extract and validate data from S record.
// retrun size of data record
/**
 *  @brief     Extract Data from S1 records
 *
 *  @param[out] dst       destination buffer
 *
 *  @param[in] src        source buffer
 *
 *  @param[in] size       number of bytes in src
 *
 *  @retval    size of string in dst
 *
 */
static Uint32 DCS7_SRecord_unpack(Uint8 *dst, Uint8 *src, Uint32 size)
{
    Uint8 tempSrecord[ASCII_SRECORD_S1_MAX_SIZE];
    Uint16 returnDataSize = 0, tempS1DataSize;
    Uint32 tempSize = 0;
    Uint32 srcIndex = 0;
    Uint32 s1_count;

    /* Get S0 record (start) */
    tempSize = DCS7_SRecord_stringParser(tempSrecord, src,
                                         ASCII_SRECORD_START_SIZE
                                         + MSC_CRLF_SIZE, 0);
    srcIndex += tempSize;
    // parsing CRLF if present at starting
    if (tempSize != ASCII_SRECORD_START_SIZE)
    {
        tempSize = DCS7_SRecord_stringParser(tempSrecord,
                                             (Uint8*) (src + srcIndex),
                                             ASCII_SRECORD_START_SIZE
                                             + MSC_CRLF_SIZE, 0);
        srcIndex += tempSize;
    }
    // validate S0 record
    if ((tempSize != ASCII_SRECORD_START_SIZE) ||
        (memcmp(tempSrecord,
                &srecord_strings[SRECORD_START_STRING][0],
                ASCII_SRECORD_START_SIZE) != 0))
    {  // error, S0 record not found
        return 0;
    }

    /* Get S1 record (data) */
    for (s1_count = 0; s1_count < SRECORD_RES_MAX_S1_CNT; s1_count++)
    {
        tempS1DataSize = DCS7_SRecord_stringParser(dst,
                                                   (Uint8*) (src + srcIndex),
                                                   size - srcIndex, 1);
        if (tempS1DataSize == 0)
            break;  // Not S1 record

        // size for /--type--cnt--add--data--cs--crlf-/
        srcIndex += tempS1DataSize + ASCII_SRECORD_TYP_SIZE
                                   + MSC_CRLF_SIZE;

        // validate S1 record
        /*if ((memcmp(&tempS1record[s1_count][0],
                    &srecord_strings[SRECORD_DATA_STRING][0],
                    ASCII_SRECORD_TYP_SIZE) != 0))
        {  // error, S1 record not found
            return 0;
        }*/

        dst += tempS1DataSize; // pointer update
        // dst = /--cnt--add--data--cs-/

        returnDataSize += tempS1DataSize;
    }

    /* Get S9 record (stop) */
    tempSize = DCS7_SRecord_stringParser(tempSrecord,
                                         (Uint8*) (src + srcIndex),
                                         size - srcIndex, 0);

    // validate S9 record
    if ((tempSize != ASCII_SRECORD_STOP_SIZE) ||
        (memcmp(tempSrecord,
                &srecord_strings[SRECORD_STOP_STRING][0],
                ASCII_SRECORD_STOP_SIZE) != 0))
    {  // error, S9 record not found
        return 0;
    }

    return returnDataSize;
}

#ifdef UARTCALLBACK

/**
 *  @brief     Clock Event function, used by UART SWI
 *             as a timeout functionality. The clock instance
 *             is configured in one shot mode.
 *
 *             If this function is invoked, that will mean
 *             the transfer was not complete within a certain
 *             timeout duration, hence it an error. Under
 *             sunch condition set transfer count as 0 and
 *             post semaphore to wakeup the task.
 *
 *  @param[in] arg0       arg0 used to pass DCS7_Handle
 *
 */
void UartclkFxn(UArg arg0)
{
    DCS7_Handle handle = (DCS7_Handle)arg0;
    Clock_Handle clk = handle->params->pDev->pDevData.pUart->hSwi->clk;
    uartTransferObj.transferSize = 0;
    // Error. Timeout happed. release semaphore
    Semaphore_post(handle->sem);
    Clock_stop(clk);
}


/**
 *  @brief     SWI implementation for UART. It is posted by UART
 *             callback function. The SWI is used to reconfigure
 *             UART transfer interface for intermediate data
 *             operations, until the complete message is received.
 *
 *  @param[in] arg0       arg0 used to pass DCS7_Handle
 *
 *  @param[in] arg1       arg1
 *
 */
void UartSwiFxn(UArg arg0, UArg arg1)
{
    DCS7_Handle handle = (DCS7_Handle)arg0;
    Uint8 type;
    static Uint32 tempReadBufIndex = 0;
    Uint8 *tempReadBuf = (Uint8*)handle->mediaLayer.rxBuf;  // read buffer
    DCS7_Params_Dev_Uart *pUart = handle->params->pDev->pDevData.pUart;
    UART_Handle uart = pUart->hUart;
    Clock_Handle clk = pUart->hSwi->clk;
    Uint8 *s1Cnt = &pUart->hSwi->rxS1Size;
    Uint32 readBytes = 0;
    Uint32 len = 0;

    // stop the timeout clk
    Clock_stop(clk);

    /* Parse the received data */
    if (pUart->hSwi->s1_count <= SRECORD_REC_MAX_S1_CNT)
    {
        len = uartTransferObj.transferSize;  // updated from callback
        /* parse received data*/
        pUart->hSwi->readBufIndex += len;
        if (pUart->hSwi->swiBreakFlag == 1)
        {
            /* all data received successfully */
            // Post Semaphore to wakeup the task.
            uartTransferObj.transferSize = pUart->hSwi->readBufIndex;
            // reset SWI params
            pUart->hSwi->readBufIndex = 0;
            pUart->hSwi->s1_count = 0;
            pUart->hSwi->swiBreakFlag = 0;
            /* All data is received. release semaphore */
            Semaphore_post(uartTransferObj.globalSem);
            return;
        }
        // get record type ('0', '1' or '9') of next read
        type = tempReadBuf[pUart->hSwi->readBufIndex
                           - ASCII_SRECORD_CNT_SIZE
                           - 1];

        // get count
        DCS7_Util_asciiPairToHex((Uint8*) &s1Cnt[pUart->hSwi->s1_count],
                             (Uint8*) &tempReadBuf[pUart->hSwi->readBufIndex -
                                                   ASCII_SRECORD_TYP_SIZE],
                             ASCII_SRECORD_CNT_SIZE);

        if ((type == '0') || (type == '1'))
        {
            readBytes = (s1Cnt[pUart->hSwi->s1_count] * 2) /* byte count*/
                        + ASCII_SRECORD_TYP_SIZE
                        + ASCII_SRECORD_CNT_SIZE
                        + MSC_CRLF_SIZE;
        }
        else if (type == '9')  // Final read, S9
        {
            readBytes = ASCII_SRECORD_STOP_SIZE
                        - ASCII_SRECORD_TYP_SIZE
                        - ASCII_SRECORD_CNT_SIZE;
            s1Cnt[pUart->hSwi->s1_count] = 0;
            pUart->hSwi->swiBreakFlag = 1;  // exit loop
        }
        else  // Not a correct type
        {
            // Error during reception of data.
            // Post semaphore to wakeup the task.
            TRACE_TERSE0("DCS7: Invalid S-Record type");
            // reset SWI params
            uartTransferObj.transferSize = 0;
            pUart->hSwi->readBufIndex = 0;
            pUart->hSwi->s1_count = 0;
            pUart->hSwi->swiBreakFlag = 0;
            pUart->hSwi->rxS1Size[0] = 0;
            Semaphore_post(uartTransferObj.globalSem);
            return;
        }

        // increament the the received S1 records count
        pUart->hSwi->s1_count++;

        if (handle->config->cacheMode == DCS7_CONFIG_CACHEMODE_ENABLE)
        {
            handle->fxns->cacheWbInv(handle,
                             (void*) (&tempReadBuf[pUart->hSwi->readBufIndex]),
                             readBytes);
        }

        /* Re-configure UART transfer for further operation*/
        uartTransferObj.transferSize = 0;
        uartTransferObj.opMode = UART_CALLBACK_READ;
        UART_read(uart,
                 (void *) &tempReadBuf[pUart->hSwi->readBufIndex],
                 readBytes);

        // tempReadBuf = /------------add--data--cs--crlf-/ S1 or S9
        //               /--typ--cnt--/ S1 or S9

        /* Configure timeout for intermediate reads  */
        // Do not wait forever.
        // For debug purpose: COMMENT it for no timeout implementation
        {
            // Clock_setTimeout for one shot clock
            Clock_setTimeout(clk,
                            handle->fxns->msToTicks(handle,
                                                    DCS7_Uart_timeout(readBytes,
                                                             pUart->baudRate)));
            Clock_start(clk);
        }
    }
}
#endif

/**
 *  @brief     Low level DCS UART read
 *
 *  @param[in] handle      DCS handle
 *
 *  @param[out] aBuf       destination buffer
 *
 *  @param[in] aSize       Number of bytes to read
 *
 *  @retval    size of string in dst
 *
 */
Uint32 DCS7_uartReadIssue(DCS7_Handle handle, Uint16 *aBuf, Uint16 aSize)
{
    Uint32 len, i, returnLen = 0;
    Uint32 s1_count;
    Uint8 *tempReadBuf = (Uint8*)handle->mediaLayer.rxBuf;  // read buffer
    Uint32 tempReadBufIndex = 0, aBufIndex = 0;
    DCS7_CommonParam *p = &handle->common;
    Uint8 s1Cnt[SRECORD_REC_MAX_S1_CNT];    // array for /--cnt--/ HEX S1
    Uint8 type = 0, breakFlag = 0;
    Uint32 readBytes = 0;
    DCS7_Params_Dev_Uart *pUart = handle->params->pDev->pDevData.pUart;
    UART_Handle uart = pUart->hUart;
    UART_Params *uartParams = &(pUart->uartParams);

    (void)type;

    if (!uart)
        return 0;

    // TODO: Closing and opening the UART will clear the stale data
    //       on rx FIFO.
    //       Can be replaced with UART_readCancel() once it is fixed.
    UART_close(uart);
    uart = UART_open(BOARD_UART_INSTANCE, uartParams);

    // To read variable length alpha sequence.
    // From first read, get correct number of byte to read
    // in second read operation.

    /* read 1 */
    readBytes = MSC_CRLF_SIZE
                + ASCII_SRECORD_START_SIZE
                + ASCII_SRECORD_TYP_SIZE
                + ASCII_SRECORD_CNT_SIZE;

    if (handle->config->cacheMode == DCS7_CONFIG_CACHEMODE_ENABLE)
    {
        handle->fxns->cacheWbInv(handle,
                                 (void*) (tempReadBuf),
                                 readBytes);
    }

#ifdef UARTCALLBACK
    uartTransferObj.transferSize = 0;
    uartTransferObj.opMode = UART_CALLBACK_READ;

    UART_read(uart, (void *) tempReadBuf, readBytes);
    // tempReadBuf = /--typ--cnt--add--data--cs--crlf-/ S0
    //               /--typ--cnt--/ S1
    //

    // Wait forever
    // Posted from wither SWI or clk instance in case of timeout
    handle->fxns->semPend(handle, handle->sem);

    len = uartTransferObj.transferSize;  // updated from SWI or clk function
    if (len == 0)
    {
        TRACE_TERSE0("DCS7: data receive failed");
        return 0;
    }

    // Copy the size array (updated from SWI) into local array
    for (i = 0; i < SRECORD_REC_MAX_S1_CNT; i++)
    {
        s1Cnt[i] = pUart->hSwi->rxS1Size[i];
    }
#else
    len = UART_read(uart, (void *) tempReadBuf, readBytes);
    // tempReadBuf = /--typ--cnt--add--data--cs--crlf-/ S0
    //               /--typ--cnt--/ S1
    for (s1_count = 0;
         (s1_count < SRECORD_REC_MAX_S1_CNT) && !breakFlag;
         s1_count++)
    {
        tempReadBufIndex = len;
        /* read 2 */
        // get record type ('0', '1' or '9') of next read
        type = tempReadBuf[tempReadBufIndex - ASCII_SRECORD_CNT_SIZE - 1];

        // get count
        DCS7_Util_asciiPairToHex((Uint8*) &s1Cnt[s1_count],
                                 (Uint8*) &tempReadBuf[tempReadBufIndex -
                                                       ASCII_SRECORD_TYP_SIZE],
                                 ASCII_SRECORD_CNT_SIZE);

        if ((type == '0') || (type == '1'))
        {
            readBytes = (s1Cnt[s1_count] * 2) /* byte count*/
                        + ASCII_SRECORD_TYP_SIZE
                        + ASCII_SRECORD_CNT_SIZE
                        + MSC_CRLF_SIZE;
        }
        else if (type == '9')  // Final read, S9
        {
            readBytes = ASCII_SRECORD_STOP_SIZE
                        - ASCII_SRECORD_TYP_SIZE
                        - ASCII_SRECORD_CNT_SIZE;
            s1Cnt[s1_count] = 0;
            breakFlag = 1;  // exit loop
        }
        else  // Not a correct type
        {
            TRACE_TERSE0("DCS7: Invalid S-Record type");
            return 0;
        }

        if (handle->config->cacheMode == DCS7_CONFIG_CACHEMODE_ENABLE)
        {
            handle->fxns->cacheWbInv(handle,
                                     (void*) (&tempReadBuf[tempReadBufIndex]),
                                     readBytes);
        }

        len += UART_read(uart,
                         (void *) &tempReadBuf[tempReadBufIndex],
                         readBytes);
    }
#endif

    //// In UART callback mode:
    //// len has to be in handle and updated at swi then used in below func

    // tempReadBuf = /--typ--cnt--add--data--cs--crlf-/ S0
    //               /--typ--cnt--add--data--cs--crlf-/ S1
    //               /--typ--cnt--add--data--cs--crlf-/ S1
    //               ..................................
    //               /--typ--cnt--add--data--cs--crlf-/ S9

    /* data translation */
    // S-Record parsing
    len = DCS7_SRecord_unpack((Uint8*) tempReadBuf, (Uint8*) tempReadBuf, len);
    if (len == 0)
    {
        TRACE_TERSE0("DCS7: S-Record unpack failed");
        return 0;
    }
    // tempReadBuf = /--cnt--add--data--cs-/ ASCII S1
    //               /--cnt--add--data--cs-/ ASCII S1
    //               ................................
    //               /--cnt--add--data--cs-/ ASCII S1

    // ascii to hex conversion
    len = DCS7_Util_asciiPairToHex((Uint8*) tempReadBuf,
                                   (Uint8*) tempReadBuf,
                                   len);
    // tempReadBuf = /--cnt--add--data--cs-/ HEX S1
    //               /--cnt--add--data--cs-/ HEX S1
    //               ................................
    //               /--cnt--add--data--cs-/ HEX S1

    // REMARK: Checksum validate here, if required

    tempReadBufIndex = 0;
    breakFlag = 0;

    // copy data into destination
    for (s1_count = 0;
         (s1_count < SRECORD_REC_MAX_S1_CNT) && !breakFlag;
         s1_count++)
    {
        if (s1Cnt[s1_count] == 0)
            break;
        s1Cnt[s1_count] -= (HEX_SRECORD_S1_ADD_SIZE
                            + HEX_SRECORD_CSM_SIZE);

        // retreive data field
        memcpy((void*) (((Uint8*)aBuf) + aBufIndex),
                (((Uint8*) &tempReadBuf[tempReadBufIndex])
                + HEX_SRECORD_CNT_SIZE
                + HEX_SRECORD_S1_ADD_SIZE),
                s1Cnt[s1_count]);
        // aBuf = /--data-/ HEX S1

        // update index and size
        tempReadBufIndex += (s1Cnt[s1_count]
                            + HEX_SRECORD_CNT_SIZE
                            + HEX_SRECORD_S1_ADD_SIZE
                            + HEX_SRECORD_CSM_SIZE);
        aBufIndex += s1Cnt[s1_count];

        returnLen += s1Cnt[s1_count];
    }  // copy data into destination

    // Remove Encapsulation
    if ((aBuf[0] & SHORT_HIGH_BYTE_MASK) == ALPHA_CMD_ENCAPSULATION)  // if not slc command
       aBuf[0] &= SHORT_LOW_BYTE_MASK;  // Clear Encapsulation

    p->rbuf_off = returnLen;  // update offset

    return returnLen;
}

/**
 *  @brief     Low level DCS UART read
 *
 *  @param[in] handle      DCS handle
 *
 *  @param[out] aBuf       data to send as response, data feild of S1 record
 *
 *  @param[in] aSize       number of bytes in aBuf
 *
 *  @retval    Number of byte written
 *
 */
Uint32 DCS7_uartWriteIssue(DCS7_Handle handle, Uint16 *aBuf, Uint16 aSize)
{
    DCS7_Params_Dev_Uart *pUart = handle->params->pDev->pDevData.pUart;
    UART_Handle uart = pUart->hUart;
    Uint16 len = 0;
    Uint16 *tempWriteBuf = handle->mediaLayer.txBuf;
    Uint8 *asciiBuf = handle->params->pDev->pDevData.pUart->scratchBuf;
    Uint8 hexBuf[HEX_SRECORD_S1_MAX_SIZE];
    Uint32 bufSize[SRECORD_RES_MAX_S1_CNT];
    Uint32 hexBufIndex;
    Uint32 asciiBufIndex = 0;
    Uint32 retVal = 0;

    if (!uart)
        return 0;
    /* data translation here */
    // aBuf = /--data-/ HEX
    // Add Ecapsulation
    if ((aSize != NULL_RESPONSE_SIZE) &&                     // not a null response
        ((aBuf[0] & SL_MASK) != SLS_MASK))   // not a sls command
    {
        // add encapsulation except for
        // null response and sls
        aBuf[0] = (aBuf[0] & SHORT_LOW_BYTE_MASK) | ALPHA_CMD_ENCAPSULATION;
    }
    
    // Prepare S1 record.
    {
        Uint32 s1_count, aBuf_offset;
        Uint32 prepareDone = 0;
        Uint32 remainingSize = aSize;

        // splitting data into multiple S1 records if required,
        // Cnt and address addition
        for (s1_count = 0; s1_count < SRECORD_RES_MAX_S1_CNT; s1_count++)
        {
            hexBufIndex = 0;
            if (remainingSize > HEX_SRECORD_S1_DATA_MAX_SIZE)
            {
                bufSize[s1_count] = HEX_SRECORD_S1_DATA_MAX_SIZE;
            }
            else if (remainingSize > 0)
            {
                bufSize[s1_count] = remainingSize;
            }
            else
            {   // no data left to prepare S1
                bufSize[s1_count] = 0;
                // break if S1 record is prepared,
                // and there is no remaining data.
                if (prepareDone != 0)
                    break;
            }
            aBuf_offset = aSize - remainingSize;
            // cnt addition
            hexBuf[hexBufIndex++] = bufSize[s1_count]
                                      + HEX_SRECORD_S1_ADD_SIZE
                                      + HEX_SRECORD_CSM_SIZE;
            // 2 hex byte address addition
            hexBuf[hexBufIndex++] = 0;
            hexBuf[hexBufIndex++] = aBuf_offset >> 1;  // Character pair count

            // add data
            if ((aSize != NULL_RESPONSE_SIZE) ||  // do not process null response here
                (aBuf[0] & SL_MASK == SLS_MASK))  // process sls and other alpha response here
            {
                memcpy((void*)&hexBuf[hexBufIndex],
                        (((Uint8*) aBuf) + aBuf_offset),
                        bufSize[s1_count]);
                remainingSize -= bufSize[s1_count];
                hexBufIndex += bufSize[s1_count];
            }
            else  // Null response, set data as 0xC900 (encapsulation)
            {
                hexBuf[hexBufIndex++] = ALPHA_CMD_ENCAP_LOW_BYTE;
                hexBuf[hexBufIndex++] = ALPHA_CMD_ENCAP_HIGH_BYTE;
                remainingSize -= bufSize[s1_count];
            }
            
            // hexBuf = /--cnt--add--data-/ HEX

            // Checksum addition for S1 record
            hexBuf[hexBufIndex++] = DCS7_Util_checksum(&hexBuf[0],
                                                        hexBufIndex);
            // Update total size value
            bufSize[s1_count] = hexBufIndex;  // Hex count

            // hexBuf = /--cnt--add--data--cs-/ HEX
            // binary to ascii conversion
            len = DCS7_Util_hexToAsciiPair(asciiBuf + asciiBufIndex,
                                           &hexBuf[0],
                                           bufSize[s1_count]);
            bufSize[s1_count] = len;          // Character counts
            asciiBufIndex += bufSize[s1_count];
            prepareDone = 1;
        }
    }

    // asciiBuf = /--cnt--add--data--cs-/ ASCII
    // Encasulate SRecord
    len = DCS7_SRecord_pack((Uint8*) tempWriteBuf, asciiBuf, &bufSize[0]);

    // tempWriteBuf = /--S0--S1--S9--/ ASCII

    // Cache handling
    if (handle->config->cacheMode == DCS7_CONFIG_CACHEMODE_ENABLE)
    {
        handle->fxns->cacheWbInv(handle, (void*) (tempWriteBuf), len);
    }

#ifdef UARTCALLBACK
    uartTransferObj.transferSize = 0;
    uartTransferObj.opMode = UART_CALLBACK_WRITE;
    
    UART_write(uart, (void *) tempWriteBuf, len);  // UART LLD Write
    retVal = handle->fxns->semPendTimeout(handle, handle->sem,
                                          handle->fxns->msToTicks(handle,
                                                  DCS7_Uart_timeout(len,
                                                          pUart->baudRate)));
    if (retVal == Semaphore_PendState_TIMEOUT)
    {
        TRACE_TERSE0("DCS7: UART write timeout");
    }
    
    len = uartTransferObj.transferSize;  // updated from callback
#else
    len = UART_write(uart, (void *) tempWriteBuf, len);  // UART LLD Write
#endif

    return len;
}


/******************************************************************************
 * SPI Functions definition
 *****************************************************************************/

#ifdef SPICALLBACK
void SPI_transferCallback(SPI_Handle handle, SPI_Transaction *transaction)
{
    DCS7_Handle dcsHandle = (DCS7_Handle) transaction->arg;

    dcsHandle->fxns->semPost(dcsHandle, dcsHandle->sem);
}
#endif

/**
 *  @brief     Generic SPI transfer 
 *
 *  @param[in] handle      DCS handle
 *
 *  @param[in] size        number of byte to transfer
 *
 *  @retval    Number of byte written
 *
 */
static inline void DCS7_spiTransfer(DCS7_Handle handle, Uint16 size)
{
    Uint32 xferEnable;
    DCS7_Params_Dev_Spi *pSpi = handle->params->pDev->pDevData.pSpi;
    SPI_Params *spiParam = pSpi->spiParams;
    SPI_Transaction *transaction = &(pSpi->transaction);

    transaction->count = NUM_TRANSFER_ELEMENT(size);

    // Enable transfer
    xferEnable = 1;
    SPI_control(pSpi->hSpi, SPI_V0_CMD_XFER_ACTIVATE, (void *) &xferEnable);

    // SPI LLD transfer
    SPI_transfer(pSpi->hSpi, transaction);
}

/**
 *  @brief     Low level DCS SPI read
 *
 *  @param[in] handle      DCS handle
 *
 *  @param[in] aBuf        destination buffer
 *
 *  @param[in] aSize       number of bytes in aBuf
 *
 *  @retval    Number of byte read
 *
 */
Uint32 DCS7_spiReadIssue(DCS7_Handle handle, Uint16 *aBuf, Uint16 aSize)
{
    SPI_Params *spiParam = handle->params->pDev->pDevData.pSpi->spiParams;
    Uint16 *tempWriteBuf = handle->mediaLayer.txBuf;
    Uint16 *tempReadBuf = handle->mediaLayer.rxBuf;
    Uint32 payloadSize = 0;
    Uint32 retVal = 0;


    /* Send Acknowledge (0xf101) to master to sync*/
    memset((void*)tempWriteBuf, 0, SPI_ACK_SIZE);
    memset((void*)tempReadBuf, 0, SPI_ACK_SIZE);

    // Check for master wait
    DCS7_spiTransfer(handle, SPI_ACK_SIZE);

#ifdef SPICALLBACK
    handle->fxns->semPend(handle, handle->sem);  // wait forever
#endif

    // if Master is waiting for ACK
    if (tempReadBuf[0] == 0)
    {
        memset((void*)tempWriteBuf, 0, SPI_ACK_SIZE);
        memset((void*)tempReadBuf, 0, SPI_ACK_SIZE);

        tempWriteBuf[0] = SPI_SLAVE_ACK;

        // Send ACK to master
        DCS7_spiTransfer(handle, SPI_ACK_SIZE);

#ifdef SPICALLBACK
        retVal = handle->fxns->semPendTimeout(handle, handle->sem,
                                         handle->fxns->msToTicks(handle, 300));
        if (retVal == Semaphore_PendState_TIMEOUT)
        {
            TRACE_TERSE0("DCS7: SPI Read timeout");
            return 0;
        }
#endif
    }

    /* Read count byte */
    {
        memset((void*)tempWriteBuf, 0, SPI_REC_COUNT_SIZE);
        memset((void*)tempReadBuf, 0, SPI_REC_COUNT_SIZE);

        DCS7_spiTransfer(handle, SPI_REC_COUNT_SIZE);

#ifdef SPICALLBACK
        retVal = handle->fxns->semPendTimeout(handle, handle->sem,
                                          handle->fxns->msToTicks(handle, 300));
        if (retVal == Semaphore_PendState_TIMEOUT)
        {
            TRACE_TERSE0("DCS7: SPI Read timeout");
            return 0;
        }
#endif

        payloadSize = tempReadBuf[0];

        // TODO: complete the logic
        if (spiParam->dataSize == 8)
        {
            if ((((payloadSize & SHORT_HIGH_BYTE_MASK) >> 8) % 2) == 0)  // if LSB is even or not
            {
                DCS7_swap((DCS7_uint16*)&payloadSize,
                          (DCS7_uint16*)payloadSize, 1);
            }

            aBuf[0] = payloadSize/2; // 16-bit element count
            if (payloadSize > 254)
            {
                payloadSize -= 1; // remove 1 byte for extended count field
            }
        }
        else  // data size is 16  // TODO: to test
        {
            aBuf[0] = payloadSize; // 16-bit element count
            payloadSize *= 2;      // Convert to Byte count
        }
        aBuf++;
    }

    /* Read payload data */
    {
        memset((void*)tempWriteBuf, 0, payloadSize);
        memset((void*)tempReadBuf, 0, payloadSize);

        DCS7_spiTransfer(handle, payloadSize);

#ifdef SPICALLBACK
        retVal = handle->fxns->semPendTimeout(handle, handle->sem,
                                         handle->fxns->msToTicks(handle, 500));
        if (retVal == Semaphore_PendState_TIMEOUT)
        {
            TRACE_TERSE0("DCS7: SPI Read timeout");
            return 0;
        }
#endif
        if ((tempReadBuf[0] & SL_MASK) == SLC_MASK)  // SLC command
        {
            aBuf--;
        }
        memcpy((void*)aBuf, (void*)tempReadBuf, payloadSize);
    }

    return (payloadSize + 1);
}

/**
 *  @brief     Low level DCS SPI write
 *
 *  @param[in] handle      DCS handle
 *
 *  @param[out] aBuf        Data to transfer
 *
 *  @param[in] aSize       number of bytes in aBuf
 *
 *  @retval    Number of byte written
 *
 */
Uint32 DCS7_spiWriteIssue(DCS7_Handle handle, Uint16 *aBuf, Uint16 aSize)
{
    Uint32 txBytes = 2;  // For null response
    Uint32 retVal = 0;
    Uint8 *tempWriteBuf = (Uint8 *) handle->mediaLayer.txBuf;
    Uint8 *tempReadBuf = (Uint8 *) handle->mediaLayer.rxBuf;
    SPI_Params *spiParam = handle->params->pDev->pDevData.pSpi->spiParams;

    memset((void*)tempWriteBuf, 0, SPI_RES_COUNT_SIZE);
    memset((void*)tempReadBuf, 0, SPI_RES_COUNT_SIZE);

    /* Send response Count */

    if (aSize == 2)  // Null response & SLC
    {                                              // 8 Bit : 16 Bit count
        tempWriteBuf[0] = spiParam->dataSize == 8 ? 0 : (SPI_RES_COUNT_SIZE/2);
        tempWriteBuf[1] = spiParam->dataSize == 8 ? SPI_RES_COUNT_SIZE : 0;
    }
    else
    {
        if (spiParam->dataSize == 8)
        {
            Uint32 ByteCnt = aBuf[0] * 2;  // Byte count
            // Check the count for > 256
            if (ByteCnt < 256)
            {
                DCS7_swap((DCS7_uint16 *)tempWriteBuf,
                          (DCS7_uint16 *)&ByteCnt, 1);
            }
            else
            {
                tempWriteBuf[0] = (Uint8)((ByteCnt & SHORT_LOW_BYTE_MASK) +
                                           1 /* 1 Byte for extra count byte*/);
                tempWriteBuf[1] = (Uint8)((ByteCnt & SHORT_HIGH_BYTE_MASK)>>8);
            }
        }
        else
        {
            tempWriteBuf[0] = (Uint8)(aBuf[0] & SHORT_LOW_BYTE_MASK);
            tempWriteBuf[1] = (Uint8)((aBuf[0] & SHORT_HIGH_BYTE_MASK) >> 8);
        }
    }

    DCS7_spiTransfer(handle, SPI_RES_COUNT_SIZE);

#ifdef SPICALLBACK
    retVal = handle->fxns->semPendTimeout(handle, handle->sem,
                                      handle->fxns->msToTicks(handle, 1000));
    if (retVal == Semaphore_PendState_TIMEOUT)
    {
        TRACE_TERSE0("DCS7: SPI Write timeout");
        return 0;
    }
#endif

    if (aSize != NULL_RESPONSE_SIZE)  // not Null response
    {
        txBytes = aSize - 2;
        aBuf++;
    }

    /* Send response payload */
    memset((void*)tempWriteBuf, 0, txBytes);
    memset((void*)tempReadBuf, 0, txBytes);

    if ((aSize != NULL_RESPONSE_SIZE) || ((aBuf[0] & SL_MASK) == SLS_MASK))  // not Null response
    {                                                       // or is a SLS response
        memcpy((void*)tempWriteBuf, (void*)aBuf, txBytes);
    }

    DCS7_spiTransfer(handle, txBytes);

#ifdef SPICALLBACK
    retVal = handle->fxns->semPendTimeout(handle, handle->sem,
                                          handle->fxns->msToTicks(handle, 500));
    if (retVal == Semaphore_PendState_TIMEOUT)
    {
        TRACE_TERSE0("DCS7: SPI Read timeout");
        return 0;
    }
#endif

    return ((aSize == NULL_RESPONSE_SIZE) ? txBytes : (txBytes + 2));
}

/******************************************************************************
 * I2C Functions definition
 *****************************************************************************/
 /**
 *  @brief     Low level DCS I2C read
 *
 *  @param[in] handle      DCS handle
 *
 *  @param[in] aBuf        Destination buffer
 *
 *  @param[in] aSize       number of bytes in aBuf
 *
 *  @retval    Number of byte read
 *
 */
Uint32 DCS7_i2cReadIssue(DCS7_Handle handle, Uint16 *aBuf, Uint16 aSize)
{
    DCS7_Params_Dev_I2c *pI2c = handle->params->pDev->pDevData.pI2c;
    I2C_Transaction *transaction = &(pI2c->transaction);
    Uint32 aBufIndex = 0, readBytes = 0;

    /* read count */
    transaction->writeCount = 0;
    transaction->readCount = 2;

    I2C_transfer(pI2c->hI2c, transaction);

    readBytes = (*((Uint16*)transaction->readBuf));
    aBuf[aBufIndex] = readBytes/2; // 16 bit word count

    /* read alpha command */
    transaction->writeCount = 0;

    // remove extra count byte for data size > 254
    transaction->readCount = (readBytes < 254) ?
                              readBytes : (readBytes - 1);

    I2C_transfer(pI2c->hI2c, transaction);

    if ((*((Uint16*)transaction->readBuf) & SL_MASK) != SLC_MASK)  // not SLC command
        aBufIndex++;

    memcpy((void*)&aBuf[aBufIndex],
           (void*)transaction->readBuf,
           transaction->readCount);

    return (transaction->readCount + 1);
}

/**
 *  @brief     Low level DCS I2C write
 *
 *  @param[in] handle      DCS handle
 *
 *  @param[out] aBuf        Data to transfer
 *
 *  @param[in] aSize       number of bytes in aBuf
 *
 *  @retval    Number of byte written
 *
 */
Uint32 DCS7_i2cWriteIssue(DCS7_Handle handle, Uint16 *aBuf, Uint16 aSize)
{
    DCS7_Params_Dev_I2c *pI2c = handle->params->pDev->pDevData.pI2c;
    I2C_Transaction *transaction = &(pI2c->transaction);
    Uint32 txBytes = 2;  // For null response

    /* Write count */
    if (aSize != NULL_RESPONSE_SIZE)  // not Null response
    {
        txBytes = aSize - 2;
    }

    *((Uint16*) transaction->writeBuf)
                            = (txBytes) < 254 ?
                                    (Uint16)txBytes : (Uint16)(txBytes + 1);
    transaction->writeCount = 2;
    transaction->readCount = 0;
    //*((Uint8*) transaction->writeBuf) = (Uint8) txBytes;

    I2C_transfer(pI2c->hI2c, transaction);

    /* Write alpha command */
    transaction->writeCount =  txBytes;
    transaction->readCount = 0;

    if ((aSize != NULL_RESPONSE_SIZE) || ((aBuf[0] & SL_MASK) == SLS_MASK))  // not Null response
    {
        memcpy(transaction->writeBuf,
               (((aBuf[0] & SL_MASK) == SLS_MASK) ? &aBuf[0] : &aBuf[1]),
               transaction->writeCount);
    }
    else             // Null response
    {
        memset(transaction->writeBuf, 0, transaction->writeCount);
    }

    I2C_transfer(pI2c->hI2c, transaction);

    return ((aSize == NULL_RESPONSE_SIZE) ? txBytes : (txBytes + 2));
}

/******************************************************************************
 * Common Functions definition
 *****************************************************************************/

// support inplace swap
void DCS7_swap(DCS7_uint16 *dst, DCS7_uint16 *src, Uint32 size)
{
    Uint32 i;
    Uint8 temp = 0;

    for (i = 0; i < size; i++)
    {
        temp = src[i].part.high_byte;
        dst[i].part.high_byte = src[i].part.low_byte;
        dst[i].part.low_byte = temp;
    }
}

/**
 *  @brief     Generic DCS read
 *
 *  @param[in] handle      DCS handle
 *
 *  @param[in] aBuf        Data to transfer
 *
 *  @param[in] aSize       number of bytes in aBuf
 *
 *  @retval    Number of byte read
 *
 */
Uint32 DCS7_ReadIssue(DCS7_Handle handle, Uint16 *aBuf, Uint16 aSize)
{
    Uint32 len = 0;
    if (handle->params->dev == DCS7_PARAMS_DEV_SPI0 ||
        handle->params->dev == DCS7_PARAMS_DEV_SPI1)
        len = DCS7_spiReadIssue(handle, aBuf, aSize);
    else if (handle->params->dev == DCS7_PARAMS_DEV_I2C0 ||
             handle->params->dev == DCS7_PARAMS_DEV_I2C1)
        len = DCS7_i2cReadIssue(handle, aBuf, aSize);
    else if (handle->params->dev == DCS7_PARAMS_DEV_UART0)
        len = DCS7_uartReadIssue(handle, aBuf, aSize);

    return len;
}

/**
 *  @brief     Generic DCS write
 *
 *  @param[in] handle      DCS handle
 *
 *  @param[out] aBuf        Data to transfer
 *
 *  @param[in] aSize       number of bytes in aBuf
 *
 *  @retval    Number of byte write
 *
 */
Uint32 DCS7_WriteIssue(DCS7_Handle handle, Uint16 *aBuf, Uint16 aSize)
{
    Uint32 len = 0;
    if (handle->params->dev == DCS7_PARAMS_DEV_SPI0 ||
            handle->params->dev == DCS7_PARAMS_DEV_SPI1)
        len = DCS7_spiWriteIssue(handle, aBuf, aSize);
    else if (handle->params->dev == DCS7_PARAMS_DEV_I2C0 ||
             handle->params->dev == DCS7_PARAMS_DEV_I2C1)
        len = DCS7_i2cWriteIssue(handle, aBuf, aSize);
    else if (handle->params->dev == DCS7_PARAMS_DEV_UART0)
        len = DCS7_uartWriteIssue(handle, aBuf, aSize);

    return len;
}

/**
 *  @brief     Allocate resource for Medialayer
 *
 *  @param[in] handle      DCS handle
 *
 *  @retval    RETURN_SUCCESS or RETURN_ERROR1
 *
 */
Uint32 DCS7_resAllocMediaLayer(
        DCS7_Handle handle)
{
    handle->fxns->taskDisable(handle);

    // Allocate TX and RX buffer used for Low level transfer operation
    handle->mediaLayer.rxBuf = (Uint16*) handle->fxns->memAlloc(handle,
            handle->config->bufMemSeg, handle->config->mediaLayerRxSize,
            handle->config->cacheMode == DCS7_CONFIG_CACHEMODE_ENABLE ?
                    handle->config->cacheBufAlign : 4);
    handle->mediaLayer.txBuf = (Uint16*) handle->fxns->memAlloc(handle,
            handle->config->bufMemSeg, handle->config->mediaLayerTxSize,
            handle->config->cacheMode == DCS7_CONFIG_CACHEMODE_ENABLE ?
                    handle->config->cacheBufAlign : 4);


    if ((handle->mediaLayer.rxBuf == NULL) ||
        (handle->mediaLayer.txBuf == NULL))
        return RETURN_ERROR1;

    handle->mediaLayer.rxSize = handle->config->mediaLayerRxSize;
    handle->mediaLayer.txSize = handle->config->mediaLayerTxSize;


#ifdef UARTCALLBACK
    if (handle->params->dev == DCS7_PARAMS_DEV_UART0)
    {
        DCS7_Params_Dev_Uart *pUart = handle->params->pDev->pDevData.pUart;
        Clock_Params clkParams;
        Swi_Params swiParams;
        pUart->hSwi = (DCS7_swiObj*) handle->fxns->memAlloc(handle,
                                                    handle->config->objMemSeg,
                                                    sizeof(DCS7_swiObj),
                                                    4);
        pUart->hCallback = &uartTransferObj;
        uartTransferObj.globalSem = handle->sem;

        /* Clear UART SWI params */
        pUart->hSwi->clk = NULL;
        pUart->hSwi->swi = NULL;
        pUart->hSwi->readBufIndex = 0;
        pUart->hSwi->s1_count = 0;
        pUart->hSwi->swiBreakFlag = 0;
        memset(pUart->hSwi->rxS1Size, 0, SRECORD_REC_MAX_S1_CNT);

        /* Configure UART SWI */
        Swi_Params_init(&swiParams);
        swiParams.arg0 = (xdc_UArg) handle;
        swiParams.arg1 = 0;
        swiParams.priority = 2;  // swi priority level 2
        swiParams.trigger = 0;

        /* Configure clock module, to be used as timeout for SWI */
        // Create an one-shot Clock Instance with timeout = 11 system time units
        Clock_Params_init(&clkParams);
        clkParams.period = 0;
        clkParams.startFlag = FALSE;
        clkParams.arg = (xdc_UArg) handle;

        pUart->hSwi->clk = Clock_create(UartclkFxn, CLOCK_INIT_TIMEOUT,
                                        &clkParams, NULL);
        pUart->hSwi->swi = Swi_create(UartSwiFxn, &swiParams, NULL);
        uartTransferObj.swi = pUart->hSwi->swi;

        if ((pUart->hSwi->clk == NULL) ||
            (pUart->hSwi->swi == NULL))
        {
            return RETURN_ERROR2;
        }

    }
#endif

    handle->fxns->taskEnable(handle);

    return RETURN_SUCCESS;
}
