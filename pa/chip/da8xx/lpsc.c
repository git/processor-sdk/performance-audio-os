
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DA8xx LPSC management library function
//
//
//

#define WA
#if defined(WA)
#include <primus.h>
#else /* WA */

#include <ti/psp/cslr/cslr_psc_DA830.h>
#include <ti/psp/cslr/soc_DA830.h>

#endif /* WA */

void lpscEnable (int pscNum, int pdNum, int lpscNum)
{
	CSL_PscRegs *REGS;

	if (pscNum == 1)
		REGS = (CSL_PscRegs *) CSL_PSC_1_REGS;
	else
		REGS = (CSL_PscRegs *) CSL_PSC_0_REGS;

	// NEXT = 0x03 to enable LPSC Module N
	CSL_FINS (REGS->MDCTL[lpscNum], PSC_MDCTL_NEXT, 0x03);
	// Program goctl to start transition sequence for LPSCs
	REGS->PTCMD = (1 << pdNum);
	// Wait for GOSTAT = NO TRANSITION from LPSC
	while (!(((REGS->PTSTAT >> pdNum) & 0x00000001) == 0));
	// Wait for MODSTAT = ENABLE from LPSC
	while (!((REGS->MDSTAT[lpscNum] & 0x0000001F) == 0x3));
}

void lpscDisable (int pscNum, int pdNum, int lpscNum)
{
	CSL_PscRegs *REGS;

	if (pscNum == 1)
		REGS = (CSL_PscRegs *) CSL_PSC_1_REGS;
	else
		REGS = (CSL_PscRegs *) CSL_PSC_0_REGS;

	// NEXT = 0x00 to SwRstDisable LPSC Module N
	CSL_FINS (REGS->MDCTL[lpscNum], PSC_MDCTL_NEXT, 0x00);
	// Program goctl to start transition sequence for LPSCs
	REGS->PTCMD = (1 << pdNum);
	// Wait for GOSTAT = NO TRANSITION from LPSC
	while (!(((REGS->PTSTAT >> pdNum) & 0x00000001) == 0));
	// Wait for MODSTAT = ENABLE from LPSC
	while (!((REGS->MDSTAT[lpscNum] & 0x0000001F) == 0x00));
}
