// See: pa/f/s3/paf_alg_priv.h
// Performance Audio Algorithm Creation private API declarations.
//

#ifndef _PSDKAF_ALG_PRIV_
#define _PSDKAF_ALG_PRIV_

#include <xdc/std.h>
#include <ti/sysbios/heaps/HeapMem.h>


/*
 *  ======== PAF_ALG_alloc_ ========
 */

extern
Int
PAF_ALG_alloc_ (
    const PAF_ALG_AllocInit *pInit,
    Int sizeofInit,
    IALG_MemRec *common);

/*
 *  ======== PAF_ALG_create_ ========
 */

extern
ALG_Handle
PAF_ALG_create_ (
    const IALG_Fxns *fxns,
    IALG_Handle p,
    const IALG_Params *params,
    const IALG_MemRec *common,
    PAF_IALG_Config *pafConfig);

/*
 *  ======== PAF_ALG_init_ ========
 */

extern
Void 
PAF_ALG_init_ (
    IALG_MemRec *memTab,
    Int n,
    const IALG_MemSpace *commonSpace);

/*
 *  ======== PAF_ALG_allocMemory_ ========
 */

extern
Int
PAF_ALG_allocMemory_ (
    IALG_MemRec *memTab,
    Int n,
    const IALG_MemRec *common,
    PAF_IALG_Config *p);

/*
 *  ======== PAF_ALG_mallocMemory_ ========
 */

extern
Int
PAF_ALG_mallocMemory_ (
    IALG_MemRec *memTab,
    PAF_IALG_Config *p);

/*
 *  ======== PAF_ALG_memSpace_ ========
 */

extern
HeapMem_Handle
PAF_ALG_memSpaceToHeap_ (
    const PAF_IALG_Config *p, 
    IALG_MemSpace space);

/*
 *  ======== PAF_ALG_memSpace_ ========
 */

extern
Int
PAF_ALG_memSpaceToHeapId_ (
    const PAF_IALG_Config *p, 
    IALG_MemSpace space);

/*
 *  ======== PAF_ALG_setup_ ========
 */

extern
Void
PAF_ALG_setup_ (
    PAF_IALG_Config *p, 
    Int iHeapId, 
    HeapMem_Handle hIHeap, 
    Int lHeapId,
    HeapMem_Handle hLHeap,
    Int eHeapId,
    HeapMem_Handle hEHeap,
    Int lHeapIdShm,
    HeapMem_Handle hLHeapShm,
    Int eHeapIdShm,
    HeapMem_Handle hEHeapShm,
    Int eHeapIdNCShm,
    HeapMem_Handle hEHeapNCShm,
    Int clr
);

#endif /* _PSDKAF_ALG_PRIV_ */
