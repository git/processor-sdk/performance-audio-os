
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  Vendor specific (MDS) interface header for
 *  ASP Example Demonstration algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular AE implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef AE_MDS_
#define AE_MDS_

#include <ti/xdais/ialg.h>

#include <iae.h>
#include <icom.h>

/*
 *  ======== AE_MDS_exit ========
 *  Required module finalization function
 */
#define AE_MDS_exit COM_TII_exit

/*
 *  ======== AE_MDS_init ========
 *  Required module initialization function
 */
#define AE_MDS_init COM_TII_init

/*
 *  ======== AE_MDS_IALG ========
 *  MDS's implementation of AE's IALG interface
 */
extern IALG_Fxns AE_MDS_IALG; 

/*
 *  ======== AE_MDS_IAE ========
 *  MDS's implementation of AE's IAE interface
 */
extern const IAE_Fxns AE_MDS_IAE; 


/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of MDS's
 *  ASP Example Demonstration algorithm. However, other
 *  implementation specific operations can also be added.
 */

/*
 *  ======== AE_MDS_Handle ========
 */
typedef struct AE_MDS_Obj *AE_MDS_Handle;

/*
 *  ======== AE_MDS_Params ========
 *  We don't add any new parameters to the standard ones defined by IAE.
 */
typedef IAE_Params AE_MDS_Params;

/*
 *  ======== AE_MDS_Status ========
 *  We don't add any new status to the standard one defined by IAE.
 */
typedef IAE_Status AE_MDS_Status;

/*
 *  ======== AE_MDS_Config ========
 *  We don't add any new config to the standard one defined by IAE.
 */
typedef IAE_Config AE_MDS_Config;

/*
 *  ======== AE_MDS_PARAMS ========
 *  Define our default parameters.
 */
#define AE_MDS_PARAMS   IAE_PARAMS

/*
 *  ======== AE_MDS_create ========
 *  Create a AE_MDS instance object.
 */
extern AE_MDS_Handle AE_MDS_create(const AE_MDS_Params *params);

/*
 *  ======== AE_MDS_delete ========
 *  Delete a AE_MDS instance object.
 */
#define AE_MDS_delete COM_TII_delete

#endif  /* AE_MDS_ */
