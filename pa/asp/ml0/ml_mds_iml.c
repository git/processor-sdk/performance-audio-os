
/*
* Copyright (C) 2004-2017 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) MIPS Load Demonstration algorithm
// functionality implementation
//
//
//

/*
 *  ML Module implementation - MDS implementation of an
 *  MIPS Load Demonstration algorithm.
 */

#include <xdc/std.h>
#include <ti/xdais/xdas.h>

#include <iml.h>
#include <ml_mds.h>
#include <ml_mds_priv.h>
#include <mlerr.h>

#include "paftyp.h"

#if PAF_AUDIODATATYPE_FIXED
#error fixed point audio data type not supported by this implementation
#endif /* PAF_AUDIODATATYPE_FIXED */

/*
 *  ======== ML_MDS_apply ========
 *  MDS's implementation of the apply operation.
 *
 */

Int 
ML_MDS_apply(IML_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    // Implementation of a MIPS Load function is defined here as
    // adding MIPS demand (CPU load) as a factor of audio data samples.
    // Audio data samples of effectively one channel are "processed"
    // to maintain consistent processing (MIPS) load independent of
    // channel configuration. Processing does not read nor modify
    // audio sample data.
    //
    // Processing is a null routine within nested 'for' loops.
    // The null routine is anchored against being "optimized away"
    // during compiler optimization by insertion of an assembly comment.
    //
    // Input "count" variable is used as a multiplier (outer loop)
    // of inner loop iterations. Number of inner loop iterations
    // are number of samples (sampleCount). An input count of
    // zero disables processing.

    ML_MDS_Obj *ml = (Void *)handle;

    XDAS_Int16 i, j;

    Int sampleCount = pAudioFrame->sampleCount;	// Number of samples in the
						// audio frame per channel

    // Check "mode" and "count" to see if this ASP is enabled:

    if (ml->pStatus->mode == 0 || ml->pStatus->count == 0)
        return 0;	// return if disabled

    // For count*sampleCount iterations, loop doing nothing:

    for (i=0; i < ml->pStatus->count; i++) {
        for (j=0; j < sampleCount; j++) {
            asm("* Comment to maintain loops through compiler optimization");
        }
    }

    return 0;
}

/*
 *  ======== ML_MDS_reset ========
 *  MDS's implementation of the information operation.
 */

Int 
ML_MDS_reset(IML_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    // No reset required.

    return 0;
}
