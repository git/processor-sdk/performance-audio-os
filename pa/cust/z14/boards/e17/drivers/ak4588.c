
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// AK4588 Utility Functions
//
//
//

#include <std.h>
#include <mem.h>

//#include <pafcsl.h>
//#include <csl_gpio.h>
#include <pafsio.h>
#include <paftyp_a.h>

#include <dap_csl_mcasphal.h>  // mcasp definitions

#include <ak4588.h>

#define ADDR_SHIFT 14
#define RW_SHIFT   13
#define REG_SHIFT   8
#define DATA_SHIFT  0
#define READ        0
#define WRITE       1

#define LET_DSP_DOIT


// -----------------------------------------------------------------------------
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// Define to enable local tracing features
// #define AK_DEBUG
#ifdef AK_DEBUG
 #define TRACE(a) LOG_printf a
#else
 #define TRACE(a)
#endif
// .............................................................................


// -----------------------------------------------------------------------------
AK4588_Handle AK4588_create (AK4588_Attrs *pAttrs)
{
    AK4588_Handle handle;


    // SPI only for now
    if ((pAttrs->controlMode != AK4588_MODE_SPI0) &&
        (pAttrs->controlMode != AK4588_MODE_SPI1))
        return NULL;

    handle = MEM_alloc (pAttrs->segid, (sizeof(AK4588_Obj)+3)/4*4, 4);
    if (!handle)
        return handle;

#ifdef LET_DSP_DOIT
    handle->controlMode  = pAttrs->controlMode;
    handle->anaAddr      = pAttrs->anaAddr;
    handle->digAddr      = pAttrs->digAddr;

    // init spi module as master
    if (pAttrs->controlMode == AK4588_MODE_SPI0)
        handle->hPort = (volatile int *) _SPI_BASE_PORT0;
    else
        handle->hPort = (volatile int *) _SPI_BASE_PORT1;
    handle->hPort[SPIGCR0] = 0x0; // assert reset
    handle->hPort[SPIGCR0] = 0x1; // deassert reset
    handle->hPort[SPIGCR1] = 0x3; // CLKMOD - internal, Master
    handle->hPort[SPIPC0] = 0x0E00;   // SOMI/SIMO/CLK/SCS0 as SPI 
    handle->hPort[SPIDAT1] = 0x00;    // CSNR <- 0, DFSEL<- 0
    handle->hPort[SPIFMT0] = 0x00021D10; // 16 bits char len
    handle->hPort[SPIGCR1] |= 0x01000000; // enable SPI
#endif

    return handle;
} //AK4588_write

// -----------------------------------------------------------------------------

Int AK4588_writeDig (AK4588_Handle handle, int reg, int data)
{
#ifdef LET_DSP_DOIT
    int cmd;
    volatile Uint32 *mcasp0 = (volatile Uint32 *) _MCASP_BASE_PORT0;

    // enable SPI CS
    mcasp0[_MCASP_PDOUT_OFFSET] &= ~0x400;

    cmd = (handle->digAddr << ADDR_SHIFT);
    cmd |= (WRITE << RW_SHIFT);
    cmd |= ((reg & 0xF) << REG_SHIFT);
    cmd |= ((data & 0xFF) << DATA_SHIFT);

    handle->hPort[SPIDAT1] = cmd;

    // Check to make sure a word is received and copied
    // into the buffer register (SPIBUF) - this is indicated
    // by the RXINTFLAG being set.
    while(!(handle->hPort[SPIFLG] & 0x00000100));

    // Read SPIBUF even if the data is discarded to force a
    // clear of the RXINT flag.
    cmd = handle->hPort[SPIBUF];

    // disable SPI CS
    mcasp0[_MCASP_PDOUT_OFFSET] |= 0x400;

#endif

    return 0;
} //AK4588_writeDig

// -----------------------------------------------------------------------------

Int AK4588_writeAna (AK4588_Handle handle, int reg, int data)
{
#ifdef LET_DSP_DOIT
    int cmd;
    volatile Uint32 *mcasp0 = (volatile Uint32 *) _MCASP_BASE_PORT0;

    // enable SPI CS
    mcasp0[_MCASP_PDOUT_OFFSET] &= ~0x400;

    cmd = (handle->anaAddr << ADDR_SHIFT);
    cmd |= (WRITE << RW_SHIFT);
    cmd |= ((reg & 0xF) << REG_SHIFT);
    cmd |= ((data & 0xFF) << DATA_SHIFT);

    handle->hPort[SPIDAT1] = cmd;

    // Check to make sure a word is received and copied
    // into the buffer register (SPIBUF) - this is indicated
    // by the RXINTFLAG being set.
    while(!(handle->hPort[SPIFLG] & 0x00000100));

    // Read SPIBUF even if the data is discarded to force a
    // clear of the RXINT flag.
    cmd = handle->hPort[SPIBUF];

    // disable SPI CS
    mcasp0[_MCASP_PDOUT_OFFSET] |= 0x400;

#endif
    
    return 0;
} //AK4588_writeAna

// -----------------------------------------------------------------------------

Int AK4588_readDig (AK4588_Handle handle, int reg, int *pData)
{
#ifdef LET_DSP_DOIT
    int cmd;
    volatile Uint32 *mcasp0 = (volatile Uint32 *) _MCASP_BASE_PORT0;

    // enable SPI CS
    mcasp0[_MCASP_PDOUT_OFFSET] &= ~0x400;

    cmd = (handle->digAddr << ADDR_SHIFT);
    cmd |= (READ << RW_SHIFT);
    cmd |= ((reg & 0xF) << REG_SHIFT);

    handle->hPort[SPIDAT1] = cmd;

    // Check to make sure a word is received and copied
    // into the buffer register (SPIBUF) - this is indicated
    // by the RXINTFLAG being set.
    while(!(handle->hPort[SPIFLG] & 0x00000100));

    *pData = (int) (handle->hPort[SPIBUF] & 0xFF);

        // disable SPI CS
    mcasp0[_MCASP_PDOUT_OFFSET] |= 0x400;

#endif    
    return 0;
} //AK4588_readDig

// -----------------------------------------------------------------------------
// indexed by FS bit value 

static const SmUns dirRateTable[16] = 
{
    PAF_SAMPLERATE_44100HZ,  // 0000
    PAF_SAMPLERATE_UNKNOWN,  // 0001
    PAF_SAMPLERATE_48000HZ,  // 0010
    PAF_SAMPLERATE_32000HZ,  // 0011
    PAF_SAMPLERATE_UNKNOWN,  // 0100
    PAF_SAMPLERATE_UNKNOWN,  // 0101
    PAF_SAMPLERATE_UNKNOWN,  // 0110
    PAF_SAMPLERATE_UNKNOWN,  // 0111
    PAF_SAMPLERATE_88200HZ,  // 1000
    PAF_SAMPLERATE_UNKNOWN,  // 1001
    PAF_SAMPLERATE_96000HZ,  // 1010
    PAF_SAMPLERATE_UNKNOWN,  // 1011
    PAF_SAMPLERATE_176400HZ, // 1100
    PAF_SAMPLERATE_UNKNOWN,  // 1101
    PAF_SAMPLERATE_192000HZ, // 1110
    PAF_SAMPLERATE_UNKNOWN,  // 1111
};

Int AK4588_readDIRStatus (AK4588_Handle handle, PAF_SIO_InputStatus *pStatus)
{

#ifdef LET_DSP_DOIT
    int status0, status1, fs; 
    // int nonAudio;  // not used.
    volatile Uint32 *mcasp0 = (volatile Uint32 *) _MCASP_BASE_PORT0;
    // volatile Uint32 *mcasp0 = (volatile Uint32 *) 0x01d00000;

    // don't think this extra raise is required, but it was here before...    
    mcasp0[_MCASP_PDOUT_OFFSET] |= 0x400;   // raise SPI CS
    AK4588_readDig (handle, 6, &status0);
    AK4588_readDig (handle, 7, &status1);

    fs = ((status1 & 0xF0) >> 4);
    pStatus->lock = !((status0 & 0x10) >> 4);
    // nonAudio = (status0 & 0x2) >> 1;

    if (pStatus->lock) {
        static int lastLocked;
        pStatus->nonaudio = (status0 & 0x2) ? PAF_IEC_AUDIOMODE_NONAUDIO : PAF_IEC_AUDIOMODE_AUDIO;
        pStatus->emphasis = (status0 & 0x4) ? PAF_IEC_PREEMPHASIS_YES : PAF_IEC_PREEMPHASIS_NO;
        pStatus->sampleRateMeasured = dirRateTable[fs];
        if (pStatus->sampleRateMeasured != lastLocked)
        {
            TRACE((&trace, "AK4588 read locked to %d (0x%x)", fs, status0));
            lastLocked = pStatus->sampleRateMeasured; 
        }
    }
    else {
        pStatus->nonaudio = PAF_IEC_AUDIOMODE_UNKNOWN; 
        pStatus->emphasis = PAF_IEC_PREEMPHASIS_UNKNOWN;                    
        pStatus->sampleRateMeasured = PAF_SAMPLERATE_UNKNOWN; 
        TRACE((&trace, "AK4588 read unlocked (0x%x)", status0));
    }
#else
    pStatus->lock = 1;
    pStatus->nonaudio = PAF_IEC_AUDIOMODE_AUDIO;
    pStatus->emphasis = PAF_IEC_PREEMPHASIS_NO;
    pStatus->sampleRateMeasured = PAF_SAMPLERATE_48000HZ;
#endif

    pStatus->sampleRateData = pStatus->sampleRateMeasured;
    return 0;
} //AK4588_readDIRStatus 

// -----------------------------------------------------------------------------
