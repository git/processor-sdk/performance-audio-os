
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Reverse Ring IO driver declarations
//
//
//

#ifndef DRO_H
#define DRO_H

#include <std.h>

#include <dev.h>
#include <sio.h>
#include <xdas.h>

#include <outbuf.h>
#include <pafenc.h>
#include <pafsio.h>
#include <pafsio_ialg.h>
#include <paftyp.h>
#include <ringio.h>

#include "dro_params.h"

// .............................................................................

#define RRINGIO_DATABUF_SIZE     24576         /* Defined in afp_link.h */
#define RRING_IO_NAME     "RRINGIO_ARM_DSP_01"
#define NOTIFY_DATA_END         4u


// .............................................................................
//Function table defs

typedef Int (*DRO_Treset)(DEV_Handle, PAF_OutBufConfig *);
typedef Int (*DRO_Tflush)(DEV_Handle);

typedef struct DRO_Fxns {
    //common (must be same as DEV_Fxns)
    DEV_Tclose      close;
    DEV_Tctrl       ctrl;
    DEV_Tidle       idle;
    DEV_Tissue      issue;
    DEV_Topen       open;
    DEV_Tready      ready;
    DEV_Treclaim    reclaim;

    //DRO specific
    DRO_Treset              reset;
    DRO_Tflush              flush;

} DRO_Fxns;

extern DRO_Fxns DRO_FXNS;

extern Void DRO_init (Void);

// .............................................................................

typedef struct DRO_DeviceExtension {

    DEV_Handle           device;
    DRO_Fxns            *pFxns;

    // second buffer info
    XDAS_Int32          validEmptyLevel;
    XDAS_Int32          lengthofFrame;//number of samples in each frame
    XDAS_Int32          lengthofData;//samples/channel * number of channel(stride)
    XDAS_Int8           syncState;
    const Dro_Params    *pParams;
    XDAS_Int32          secondary_out;
    //hack
    PAF_OutBufStatus    *pBufStatus;
    PAF_EncodeStatus    *pEncStatus;

    //PAF_SIO_IALG_Obj  *pSioIalg;
    PAF_OutBufConfig    bufConfig;
    
    XDAS_Int8           state;
    // RingIO specifics
    SEM_Obj             writerSemObj;   // Writer SEM object
    RingIO_Handle       RRingHandle;    // DSP Side RRing IO Writer-Handle
    XDAS_Int8           dri_dro_link;   // true if the name includes "/DDLink"
    XDAS_Int8           numChans;       // from Dro_Params
    XDAS_Int8           syncRequested;   // Set by DRI_issue(PAF_SIO_REQUEST_SYNC), cleared by subsequent reclaim.
    
    PAF_SIO_Stats       *pStats;

} DRO_DeviceExtension;

// .............................................................................

#endif /* DRO_H */
