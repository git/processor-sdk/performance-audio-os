
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _PRIMUSEDMA_H_
#define _PRIMUSEDMA_H_

/**************************************************************************\
* Primus EDMA file
\**************************************************************************/

/**************************************************************************\
*  Primus Configuration
*
\**************************************************************************/
#define CSL_EDMA_NUM_DMACH            32
#define CSL_EDMA_NUM_QDMACH            8
#define CSL_EDMA_NUM_PARAMENTRY      128
#define CSL_EDMA_NUM_EVQUE             2
#define CSL_EDMA_CHMAPEXIST            0
#define CSL_EDMA_NUMINTCH	          32
#define CSL_EDMA_NUM_REGIONS	       4
#define CSL_EDMA_MEMPROTECT            0

/** @brief Number of Generic Channel instances */
#define CSL_EDMA_CHA_CNT              32





/** @brief Enumerations for EDMA channels
*
*  There are 72 EDMA channels - 64 EDMA Channels and 8 QDMA Channels.
*  The enum values indicate the number of the channel.
*  This is passed as a parameter in @a CSL_dmaOpen() to indicate the
*  channel instance that the user wants to acquire.  The CSL_DMA_CHA_ANY
*  indicates the user is willing to use any available generic channel.
*
*
*/

/* From Primus Event Mapping spread sheet */

#define     CSL_EDMA_AREVT0                   0  /**< Channel 0 */
#define     CSL_EDMA_AXEVT0                   1  /**< Channel 1 */
#define     CSL_EDMA_AREVT1                   2  /**< Channel 2 */
#define     CSL_EDMA_AXEVT1                   3  /**< Channel 3 */
#define     CSL_EDMA_AREVT2                   4  /**< Channel 4 */
#define     CSL_EDMA_AXEVT2                   5  /**< Channel 5 */
#define     CSL_EDMA_GPINT0		   		      6  /**< Channel 6 */
#define     CSL_EDMA_GPINT1		   		      7  /**< Channel 7 */
#define     CSL_EDMA_URXEVT0		 	      8  /**< Channel 8 */
#define     CSL_EDMA_UTXEVT0		   	      9  /**< Channel 9 */
#define     CSL_EDMA_TINT0_12   		      10 /**< Channel 10*/
#define     CSL_EDMA_TINT0_34   		      11 /**< Channel 11*/
#define     CSL_EDMA_URXEVT1		 	      12 /**< Channel 12*/
#define     CSL_EDMA_UTXEVT1		   	      13 /**< Channel 13*/
#define     CSL_EDMA_SPIREVT0                 14 /**< Channel 14*/
#define     CSL_EDMA_SPIXEVT0		 	      15 /**< Channel 15*/
#define     CSL_EDMA_MMCRXEVT	   		      16 /**< Channel 16*/
#define     CSL_EDMA_MMCTXEVT	   		      17 /**< Channel 17*/
#define     CSL_EDMA_SPIREVT1                 18 /**< Channel 18*/
#define     CSL_EDMA_SPIXEVT1		 	      19 /**< Channel 19*/
#define     CSL_EDMA_dMAXEVT6                 20 /**< Channel 20*/
#define     CSL_EDMA_dMAXEVT7		 	      21 /**< Channel 21*/
#define     CSL_EDMA_GPINT2		   		      22 /**< Channel 22*/
#define     CSL_EDMA_GPINT3		   		      23 /**< Channel 23*/
#define     CSL_EDMA_ICREVT0		   		  24 /**< Channel 24*/
#define     CSL_EDMA_ICXEVT0		   		  25 /**< Channel 25*/
#define     CSL_EDMA_ICREVT1		   		  26 /**< Channel 26*/
#define     CSL_EDMA_ICXEVT1		   		  27 /**< Channel 27*/
#define     CSL_EDMA_GPINT4		   		      28 /**< Channel 28*/
#define     CSL_EDMA_GPINT5		   		      29 /**< Channel 29*/
#define     CSL_EDMA_URXEVT2		 	      30 /**< Channel 30*/
#define     CSL_EDMA_UTXEVT2		   	      31 /**< Channel 31*/



#define    CSL_EDMA_CHA0		             0  /**< Channel 0 */
#define    CSL_EDMA_CHA1		   		      1  /**< Channel 1 */
#define    CSL_EDMA_CHA2		   		      2  /**< Channel 2 */
#define    CSL_EDMA_CHA3		   		      3  /**< Channel 3 */
#define    CSL_EDMA_CHA4		   		      4  /**< Channel 4 */
#define    CSL_EDMA_CHA5		   		      5  /**< Channel 5 */
#define    CSL_EDMA_CHA6		   		      6  /**< Channel 6 */
#define    CSL_EDMA_CHA7		   		      7  /**< Channel 7 */
#define    CSL_EDMA_CHA8		   		      8  /**< Channel 8 */
#define    CSL_EDMA_CHA9		   		      9  /**< Channel 9 */
#define    CSL_EDMA_CHA10		   		      10 /**< Channel 10*/
#define    CSL_EDMA_CHA11		   		      11 /**< Channel 11*/
#define    CSL_EDMA_CHA12		   		      12 /**< Channel 12*/
#define    CSL_EDMA_CHA13		   		      13 /**< Channel 13*/
#define    CSL_EDMA_CHA14		   		      14 /**< Channel 14*/
#define    CSL_EDMA_CHA15		   		      15 /**< Channel 15*/
#define    CSL_EDMA_CHA16		   		      16 /**< Channel 16*/
#define    CSL_EDMA_CHA17		   		      17 /**< Channel 17*/
#define    CSL_EDMA_CHA18		   		      18 /**< Channel 18*/
#define    CSL_EDMA_CHA19		   		      19 /**< Channel 19*/
#define    CSL_EDMA_CHA20		   		      20 /**< Channel 20*/
#define    CSL_EDMA_CHA21		   		      21 /**< Channel 21*/
#define    CSL_EDMA_CHA22		   		      22 /**< Channel 22*/
#define    CSL_EDMA_CHA23		   		      23 /**< Channel 23*/
#define    CSL_EDMA_CHA24		   		      24 /**< Channel 24*/
#define    CSL_EDMA_CHA25		   		      25 /**< Channel 25*/
#define    CSL_EDMA_CHA26		   		      26 /**< Channel 26*/
#define    CSL_EDMA_CHA27		   		      27 /**< Channel 27*/
#define    CSL_EDMA_CHA28		   		      28 /**< Channel 28*/
#define    CSL_EDMA_CHA29		   		      29 /**< Channel 29*/
#define    CSL_EDMA_CHA30		   		      30 /**< Channel 30*/
#define    CSL_EDMA_CHA31		   		      31 /**< Channel 31*/
#define    CSL_EDMA_CHA32		   		      32 /**< Channel 32*/
#define     CSL_EDMA_QCHA0		   		      32/**< QDMA Channel 0*/
#define     CSL_EDMA_QCHA1		   		      33/**< QDMA Channel 1*/
#define     CSL_EDMA_QCHA2    	   		      34/**< QDMA Channel 2*/
#define     CSL_EDMA_QCHA3		   		      35/**< QDMA Channel 3*/
#define     CSL_EDMA_QCHA4		   		      36/**< QDMA Channel 4*/
#define     CSL_EDMA_QCHA5		   		      37/**< QDMA Channel 5*/
#define     CSL_EDMA_QCHA6		   		      38/**< QDMA Channel 6*/
#define     CSL_EDMA_QCHA7		   		      39/**< QDMA Channel 7*/


/** @brief Enumerations for EDMA Event Queues
*
*  2 Event Queues
*
*/
typedef enum {
	CSL_EDMA_EVT_QUE_DEFAULT   = 			   0, /**< Queue 0 is default */
	CSL_EDMA_EVT_QUE0 		   = 			   0, /**< Queue 0 */
	CSL_EDMA_EVT_QUE1 		   = 			   1 /**< Queue 1 */
} CSL_EdmaEventQueue;
/** @brief Enumerations for EDMA Transfer Controllers
*
*  4 Transfer Controllers. One to one mapping exists
*  between Event Queues and Transfer Controllers.
*
*/
typedef enum {
	CSL_EDMA_TC0 		   = 			   0, /**< TC 0 */
	CSL_EDMA_TC1 		   = 			   1 /**< TC 1 */
} CSL_EdmaTc;

/** @brief Enumeration for EDMA Regions
*
*  4 Regions.
*
*/

#define 	CSL_EDMA_REGION_GLOBAL  -1   /**< Global Region */
#define 	CSL_EDMA_REGION_0  0         /**< EDMA Region 0 : Shadow Region for ARM */
#define 	CSL_EDMA_REGION_1  1         /**< EDMA Region 1 : Shadow Region for GEM */
#define 	CSL_EDMA_REGION_2  2         /**< EDMA Region 2 : Shadow Region for HD-ImComp0 */
#define 	CSL_EDMA_REGION_3  3         /**< EDMA Region 3 : Shadow Region for HD-ImComp0 */

#endif  /* _PRIMUSEDMA_H_ */

/* Rev.No.   Date/Time               ECN No.          Modifier      */
/* -------   ---------               -------          --------      */

/* 1        14 June 2007 14:57:40                     TaiNguyen     */
/*                                                                  */
/* Uploaded the CSL1.0 June 2007 Release and built the library for ARM and DSP*/
/********************************************************************/
