
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

////
//
// Initial version obtained from ver 1.19 of i12/evmda830/io.c
//

#include <acptype.h>
#include <acpbeta.h>
#include <pafstd_a.h>
#include <pafdec_a.h>
#include <pafenc_a.h>
#include <pafsio.h>

#include <alpha/pcm_a.h>

#include <inpbuf_a.h>
#include <outbuf_a.h>

#include <alpha\pa_y15_evmda830_io_a.h>
#include <dap_e17.h>
#include <dri_params.h>

#define  rb32DECSourceDecode 0xc024,0x0b81
#define  ob32DECSourceDecodeNone 0x0001,0x0000

#define  rb32IBSioSelect 0xc022,0x0581
#define  ob32IBSioSelect(X) (X)|0x0080,0x0000

#define  ob32OBSioSelect(X) (X)|0x0080,0x0000

#define  rb32DECSourceSelect_3 0xc024,0x09b1
#define  wb32DECSourceSelect_3 0xc024,0x09f1

#define writePA3Await(RB32,WB32) 0xcd0b,5+2,0x0204,200,10,WB32,RB32
#define stream1 0xcd09,0x0400 /* Primary output */
#define stream2 0xcd09,0x0401 /* Secondary output */

// -----------------------------------------------------------------------------
//
// Input device configurations & shortcut definitions
//

const struct
{
    Int n;
    const PAF_SIO_Params *x[DEVINP_N];
} patchs_devinp[1] =
{
    DEVINP_N,
        // These values reflect the definitions DEVINP_* in pa*io_a.h:
        NULL,                                               // InNone
        (const PAF_SIO_Params *) &DAP_E17_RX_DIR,           // InDigital
        (const PAF_SIO_Params *) &DAP_E17_RX_ADC_48000HZ,   // InAnalog
        (const PAF_SIO_Params *) &DAP_E17_RX_ADC_STEREO_48000HZ,    // InAnalogStereo
        (const PAF_SIO_Params *) &DAP_E17_RX_1394_STEREO,   // In1394Stereo
        (const PAF_SIO_Params *) &DAP_E17_RX_1394,          // In1394
        (const PAF_SIO_Params *) &DRI_RINGIO,                // InRingIO
		(const PAF_SIO_Params *) &DAP_E17_RX_HDMI,   		// InHDMI
        (const PAF_SIO_Params *) &DAP_E17_RX_HDMI_STEREO,   // InHDMIStereo
        (const PAF_SIO_Params *) &DAP_E17_RX_ADC_96000HZ,   // InAnalog
        (const PAF_SIO_Params *) &DAP_E17_RX_ADC_STEREO_96000HZ,    // InAnalogStereo
        
};

// .............................................................................
// execPAYInNone
#define CUS_SIGMA32_S \
    writeDECSourceSelectNone, \
    writeIBSioSelectN(DEVINP_NULL), \
    0xcdf0,execPAYInNone

#pragma DATA_SECTION(cus_sigma32_s0, ".none")
const ACP_Unit cus_sigma32_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA32_S,
};

const ACP_Unit cus_sigma32_s[] = {
    0xc900 + sizeof (cus_sigma32_s0) / 2 - 1,
    CUS_SIGMA32_S,
};

// execPAYInDigital
#define CUS_SIGMA33_S \
  writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeIBUnknownTimeoutN(2*2048), \
    writeIBScanAtHighSampleRateModeDisable, \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideDisable, \
    writeIBPrecisionDefaultOriginal, \
    writeIBPrecisionOverrideDetect, \
    writeIBSampleRateOverrideStandard, \
    writeIBSioSelectN(DEVINP_DIR), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectAuto, \
    0xcdf0,execPAYInDigital

#pragma DATA_SECTION(cus_sigma33_s0, ".none")
const ACP_Unit cus_sigma33_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA33_S,
};

const ACP_Unit cus_sigma33_s[] = {
    0xc900 + sizeof (cus_sigma33_s0) / 2 - 1,
    CUS_SIGMA33_S,
};

// execPAYInAnalog
#define CUS_SIGMA34_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_ADC1), \
    writeDECSourceSelectPCM,        \
    0xcdf0,execPAYInAnalog

#pragma DATA_SECTION(cus_sigma34_s0, ".none")
const ACP_Unit cus_sigma34_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA34_S,
};

const ACP_Unit cus_sigma34_s[] = {
    0xc900 + sizeof (cus_sigma34_s0) / 2 - 1,
    CUS_SIGMA34_S,
};

// execPAYInAnalog_32ch
#define CUS_SIGMA36_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom32(0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_ADC1), \
    writeDECSourceSelectPCM,        \
    0xcdf0,execPAYInAnalog_32ch

#pragma DATA_SECTION(cus_sigma36_s0, ".none")
const ACP_Unit cus_sigma36_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA36_S,
};

const ACP_Unit cus_sigma36_s[] = {
    0xc900 + sizeof (cus_sigma36_s0) / 2 - 1,
    CUS_SIGMA36_S,
};

// execPAYInAnalogStereo
#define CUS_SIGMA35_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_ADC_STEREO), \
    writeDECSourceSelectPCM,                 \
    0xcdf0,execPAYInAnalogStereo

#pragma DATA_SECTION(cus_sigma35_s0, ".none")
const ACP_Unit cus_sigma35_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA35_S,
};

const ACP_Unit cus_sigma35_s[] = {
    0xc900 + sizeof (cus_sigma35_s0) / 2 - 1,
    CUS_SIGMA35_S,
};

// execPAYInSing
#define CUS_SIGMA38_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_ADC1), \
    writeDECSourceSelectSing, \
    0xcdf0,execPAYInSing

#pragma DATA_SECTION(cus_sigma38_s0, ".none")
const ACP_Unit cus_sigma38_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA38_S,
};

const ACP_Unit cus_sigma38_s[] = {
    0xc900 + sizeof (cus_sigma38_s0) / 2 - 1,
    CUS_SIGMA38_S,
};

// execPAYInHDMI
#define CUS_SIGMA40_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBUnknownTimeoutN(15*1024), \
    writeIBScanAtHighSampleRateModeEnable, \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverrideStandard, \
    writeIBSioSelectN(DEVINP_HDMI), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectAuto, \
    0xcdf0,execPAYInHDMI

#pragma DATA_SECTION(cus_sigma40_s0, ".none")
const ACP_Unit cus_sigma40_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA40_S,
};

const ACP_Unit cus_sigma40_s[] = {
    0xc900 + sizeof (cus_sigma40_s0) / 2 - 1,
    CUS_SIGMA40_S,
};

// execPAYInHDMIStereo
#define CUS_SIGMA41_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(0), \
	writeDECASPGearControlNil, \
    writeDECChannelMapFrom2(0,1), \
    writeIBUnknownTimeoutN(8*1024), \
    writeIBScanAtHighSampleRateModeEnable, \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverrideStandard, \
    writeIBSioSelectN(DEVINP_HDMI_STEREO), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectAuto, \
    0xcdf0,execPAYInHDMIStereo

#pragma DATA_SECTION(cus_sigma41_s0, ".none")
const ACP_Unit cus_sigma41_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA41_S,
};

const ACP_Unit cus_sigma41_s[] = {
    0xc900 + sizeof (cus_sigma41_s0) / 2 - 1,
    CUS_SIGMA41_S,
};

// .............................................................................
// execPAYIn1394Stereo
#define CUS_SIGMA45_S \
     writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom2(0,1), \
    writeIBUnknownTimeoutN(8*1024), \
    writeIBScanAtHighSampleRateModeEnable, \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverrideStandard, \
    writeIBSioSelectN(DEVINP_1394_STEREO), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectAuto, \
    0xcdf0,execPAYIn1394Stereo

#pragma DATA_SECTION(cus_sigma45_s0, ".none")
const ACP_Unit cus_sigma45_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA45_S,
};

const ACP_Unit cus_sigma45_s[] = {
    0xc900 + sizeof (cus_sigma45_s0) / 2 - 1,
    CUS_SIGMA45_S,
};

// execPAYIn1394
#define CUS_SIGMA46_S \
   writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBUnknownTimeoutN(15*1024), \
    writeIBScanAtHighSampleRateModeEnable, \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverrideStandard, \
    writeIBSioSelectN(DEVINP_1394), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectAuto, \
    0xcdf0,execPAYIn1394

#pragma DATA_SECTION(cus_sigma46_s0, ".none")
const ACP_Unit cus_sigma46_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA46_S,
};

const ACP_Unit cus_sigma46_s[] = {
    0xc900 + sizeof (cus_sigma46_s0) / 2 - 1,
    CUS_SIGMA46_S,
};

// execPAYInRingIO
#define CUS_SIGMA47_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(0), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride16, \
    writeIBSampleRateOverride48000Hz, \
    writeIBSioSelectN(DEVINP_RIO), \
    wroteDECSourceProgramUnknown, \
    writeDECSourceSelectPCM, \
    0xcdf0,execPAYInRingIO

#pragma DATA_SECTION(cus_sigma47_s0, ".none")
const ACP_Unit cus_sigma47_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA47_S,
};

const ACP_Unit cus_sigma47_s[] = {
    0xc900 + sizeof (cus_sigma47_s0) / 2 - 1,
    CUS_SIGMA47_S,
};

// execPAYInAnalog96
#define CUS_SIGMA42_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramSurround4_1, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom16(0,4,1,5,2,6,3,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride96000Hz, \
    writeIBSioSelectN(DEVINP_ADC1_96), \
    writeDECSourceSelectPCM,        \
    0xcdf0,execPAYInAnalog96

#pragma DATA_SECTION(cus_sigma42_s0, ".none")
const ACP_Unit cus_sigma42_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA42_S,
};

const ACP_Unit cus_sigma42_s[] = {
    0xc900 + sizeof (cus_sigma42_s0) / 2 - 1,
    CUS_SIGMA42_S,
};

// execPAYInAnalogStereo96
#define CUS_SIGMA43_S \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writePCMChannelConfigurationProgramStereoUnknown, \
    writePCMScaleVolumeN(2*6), \
    writeDECChannelMapFrom16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    writeIBEmphasisOverrideNo, \
    writeIBPrecisionOverride24, \
    writeIBSampleRateOverride96000Hz, \
    writeIBSioSelectN(DEVINP_ADC_STEREO_96), \
    writeDECSourceSelectPCM,                 \
    0xcdf0,execPAYInAnalogStereo96

#pragma DATA_SECTION(cus_sigma43_s0, ".none")
const ACP_Unit cus_sigma43_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA43_S,
};

const ACP_Unit cus_sigma43_s[] = {
    0xc900 + sizeof (cus_sigma43_s0) / 2 - 1,
    CUS_SIGMA43_S,
};

// -----------------------------------------------------------------------------
//
// Output device configurations & shortcut definitions
//

const struct
{
    Int n;
    const PAF_SIO_Params *x[DEVOUT_N];
} patchs_devout[1] =
{
    DEVOUT_N,
        // These values reflect the definitions DEVOUT_* in pa*io_a.h:
        NULL,                                               // OutNone
        (const PAF_SIO_Params *) &DAP_E17_TX_DAC,           // OutAnalog
        (const PAF_SIO_Params *) &DAP_E17_TX_DIT,           // OutDigital
        (const PAF_SIO_Params *) &DAP_E17_TX_DAC_SLAVE,     // OutAnalogSlave
        (const PAF_SIO_Params *) &DAP_E17_TX_STEREO_DAC_SLAVE,  // OutAnalogSlaveStereo
        (const PAF_SIO_Params *) &DAP_E17_TX_2STEREO_DAC_SLAVE, // OutAnalogSlave2Stereo
};

// .............................................................................
// execPAYOutPrimaryNone
#define CUS_SIGMA48_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_NULL), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAYOutPrimaryNone

#pragma DATA_SECTION(cus_sigma48_s0, ".none")
const ACP_Unit cus_sigma48_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA48_S,
};

const ACP_Unit cus_sigma48_s[] = {
    0xc900 + sizeof (cus_sigma48_s0) / 2 - 1,
    CUS_SIGMA48_S,
};

// .............................................................................
// execPAYOutPrimaryAnalog
#define CUS_SIGMA49_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(1), \
    writeENCChannelMapTo16(3,7,2,6,1,5,0,4,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAYOutPrimaryAnalog

#pragma DATA_SECTION(cus_sigma49_s0, ".none")
const ACP_Unit cus_sigma49_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA49_S,
};

const ACP_Unit cus_sigma49_s[] = {
    0xc900 + sizeof (cus_sigma49_s0) / 2 - 1,
    CUS_SIGMA49_S,
};

// .............................................................................
// execPAYOutPrimaryAnalog_32ch
#define CUS_SIGMA53_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(1), \
    writeENCChannelMapTo32(15,31,14,30,13,29,12,28,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAYOutPrimaryAnalog_32ch

#pragma DATA_SECTION(cus_sigma53_s0, ".none")
const ACP_Unit cus_sigma53_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA53_S,
};

const ACP_Unit cus_sigma53_s[] = {
    0xc900 + sizeof (cus_sigma53_s0) / 2 - 1,
    CUS_SIGMA53_S,
};
// .............................................................................
// execPAYOutPrimaryDigital
#define CUS_SIGMA50_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DIT), \
    writeENCChannelMapTo16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAYOutPrimaryDigital

#pragma DATA_SECTION(cus_sigma50_s0, ".none")
const ACP_Unit cus_sigma50_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA50_S,
};

const ACP_Unit cus_sigma50_s[] = {
    0xc900 + sizeof (cus_sigma50_s0) / 2 - 1,
    CUS_SIGMA50_S,
};

// .............................................................................
// execPAYOutPrimaryAnalogSlave
#define CUS_SIGMA56_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DAC_SLAVE), \
    writeENCChannelMapTo16(3,7,2,6,1,5,0,4,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAYOutPrimaryAnalogSlave

#pragma DATA_SECTION(cus_sigma56_s0, ".none")
const ACP_Unit cus_sigma56_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA56_S,
};

const ACP_Unit cus_sigma56_s[] = {
    0xc900 + sizeof (cus_sigma56_s0) / 2 - 1,
    CUS_SIGMA56_S,
};

// execPAYOutPrimaryAnalogSlaveStereo
#define CUS_SIGMA58_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DAC_STEREO), \
    writeENCChannelMapTo16(0,1,2,3,4,5,6,7,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAYOutPrimaryAnalogSlaveStereo

#pragma DATA_SECTION(cus_sigma58_s0, ".none")
const ACP_Unit cus_sigma58_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA58_S,
};

const ACP_Unit cus_sigma58_s[] = {
    0xc900 + sizeof (cus_sigma58_s0) / 2 - 1,
    CUS_SIGMA58_S,
};

// execPAYOutPrimaryAnalogSlave2Stereo
#define CUS_SIGMA59_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DAC_2STEREO), \
    writeENCChannelMapTo16(1,3,0,2,5,7,4,6,-3,-3,-3,-3,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAYOutPrimaryAnalogSlave2Stereo

#pragma DATA_SECTION(cus_sigma59_s0, ".none")
const ACP_Unit cus_sigma59_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA59_S,
};

const ACP_Unit cus_sigma59_s[] = {
    0xc900 + sizeof (cus_sigma59_s0) / 2 - 1,
    CUS_SIGMA59_S,
};

// .............................................................................
// execPAYOutPrimaryAnalogExt
#define CUS_SIGMA51_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DAC_EXT), \
    writeENCChannelMapTo16(3,9,2,8,1,7,0,6,4,10,5,11,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAYOutPrimaryAnalogExt

#pragma DATA_SECTION(cus_sigma51_s0, ".none")
const ACP_Unit cus_sigma51_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA51_S,
};

const ACP_Unit cus_sigma51_s[] = {
    0xc900 + sizeof (cus_sigma51_s0) / 2 - 1,
    CUS_SIGMA51_S,
};


// .............................................................................
// execPAYOutPrimaryAnalogSlaveExt
#define CUS_SIGMA52_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    writeOBSioSelectN(DEVOUT_DAC_SLAVE_EXT), \
    writeENCChannelMapTo16(3,9,2,8,1,7,0,6,4,10,5,11,-3,-3,-3,-3), \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAYOutPrimaryAnalogSlaveExt

#pragma DATA_SECTION(cus_sigma52_s0, ".none")
const ACP_Unit cus_sigma52_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA52_S,
};

const ACP_Unit cus_sigma52_s[] = {
    0xc900 + sizeof (cus_sigma52_s0) / 2 - 1,
    CUS_SIGMA52_S,
};

// -----------------------------------------------------------------------------

// .............................................................................
// execPAYOutSecondaryNone
#define CUS_SIGMA60_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    stream2, \
    writeOBSioSelectN(DEVOUT_NULL), \
    writePA3Await(rb32OBSioSelect,ob32OBSioSelect(DEVOUT_NULL)), \
    stream1, \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAYOutSecondaryNone

#pragma DATA_SECTION(cus_sigma60_s0, ".none")
const ACP_Unit cus_sigma60_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA60_S,
};

const ACP_Unit cus_sigma60_s[] = {
    0xc900 + sizeof(cus_sigma60_s0)/2 - 1,
    CUS_SIGMA60_S,
};
// .............................................................................
// execPAYOutSecondaryDigital
#define CUS_SIGMA61_S \
    rb32DECSourceSelect_3, \
    writeDECSourceSelectNone, \
    writePA3Await(rb32DECSourceDecode,ob32DECSourceDecodeNone), \
    stream2, \
    writeOBSioSelectN(DEVOUT_DIT), \
    writePA3Await(rb32OBSioSelect,ob32OBSioSelect(DEVOUT_DIT)), \
    writeENCChannelMapTo16(0,1,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3), \
    stream1, \
    wb32DECSourceSelect_3, \
    0xcdf0,execPAYOutSecondaryDigital

#pragma DATA_SECTION(cus_sigma61_s0, ".none")
const ACP_Unit cus_sigma61_s0[] = {
    0xc900 + 0 - 1,
    CUS_SIGMA61_S,
};

const ACP_Unit cus_sigma61_s[] = {
    0xc900 + sizeof(cus_sigma61_s0)/2 - 1,
    CUS_SIGMA61_S,
};


// EOF
