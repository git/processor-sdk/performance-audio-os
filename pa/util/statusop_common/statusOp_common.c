// Common, status structure operations


#include <string.h> 
#include <xdc/std.h>
#include <xdc/runtime/Log.h>
#include <procsdk_audio_typ.h>
#include <ti/sysbios/hal/Cache.h>
#include <ti/sysbios/knl/Task.h>

#include <ti/ipc/GateMP.h>
#include <ti/ipc/SharedRegion.h>


#include "statusOp_common.h"

#define NUM_BETA GATEMP_INDEX_MAX

//#define STATUS_GATE_REGION_ID  0
//#define STATUS_GATE_REGION_ID  3

//#define STATUS_GATE_SR_NAME     "SR_0"
#define STATUS_GATE_SR_NAME     "COMMON2_DDR3"

/* Shared Deocoder status structure */
Int sharedBetas[NUM_BETA] =
{
    STD_BETA_DECODE,       // GATEMP_INDEX_DEC
    STD_BETA_DDP,          // GATEMP_INDEX_DDP
    STD_BETA_THD,          // GATEMP_INDEX_THD
    STD_BETA_PCM,           // GATEMP_INDEX_PCM
    STD_BETA_DTSUHDA,       // GATEMP_INDEX_DTS
	STD_BETA_DSD,            // GATEMP_INDEX_DSD
    STD_BETA_AAC,           // GATEMP_INDEX_AAC
};

/* Handle for gateMP for various decoders */
GateMP_Handle gateMP_handles[GATEMP_INDEX_MAX];

/* GateMP names */
String gateMP_name[GATEMP_INDEX_MAX] =
{
        "DEC",
        "DDP",
        "THD",
        "PCM",
        "DTS",
        "DSD",
        "AAC",
};

/**
 *  @brief     Common Initialization parameter for shared status
 *             structures. Will Initialize gatesMP on ARM
 *             and Open on DSP.
 *
 *  @param[in] gateIdx   gateMP Index for shared status.
 *
 *  @return    STATUSOP_INIT_FAIL or STATUSOP_INIT_SUCCESS
 */
Int statusOp_Init(Int gateIdx)
{
    GateMP_Params gateParams;
    GateMP_Handle gateHandle;
    Int status;
    UInt16  regionId;
    

    if (gateIdx >= GATEMP_INDEX_MAX)
    {
        return STATUSOP_INIT_FAIL;
    }
    
#ifdef ARMCOMPILE
    regionId = SharedRegion_getIdByName(STATUS_GATE_SR_NAME);
    
    // On ARM
    GateMP_Params_init(&gateParams);
    // Original Remark: not required a local protect, hence GateMP_LocalProtect_NONE.
    // Update: GateMP_LocalProtect_THREAD is necessary when EITHER local or remote host has multi-threaded app.
	// Hence modified below to reflect the newer choice.
    gateParams.localProtect = GateMP_LocalProtect_THREAD;
    //gateParams.localProtect = GateMP_LocalProtect_NONE;
    gateParams.remoteProtect = GateMP_RemoteProtect_SYSTEM;
    gateParams.name = gateMP_name[gateIdx];
    gateParams.regionId = regionId; //STATUS_GATE_REGION_ID;  // Non_cacheable memory region
    gateHandle = GateMP_create(&gateParams);
    if (gateHandle != NULL)
    {
        gateMP_handles[gateIdx] = gateHandle;
    }
    else
    {
        gateMP_handles[gateIdx] = NULL;
        return STATUSOP_INIT_FAIL;
    }
#else
    // On DSP
    do {
        status = GateMP_open(gateMP_name[gateIdx], &gateHandle);
    } while (status == GateMP_E_NOTFOUND);

    if (status == GateMP_S_SUCCESS)
    {
        gateMP_handles[gateIdx] = gateHandle;
    }
    else
    {
        gateMP_handles[gateIdx] = NULL;
        return STATUSOP_INIT_FAIL;
    }
#endif

    return STATUSOP_INIT_SUCCESS;
}


/**
 *  @brief     Common write implementation for parameters in status structure.
 *             GateMP protection mechanism will be used for only shared
 *             status structures.
 *
 *  @param[out] dstAddr      Destination buffer to write
 *
 *  @param[in] sourceAddr    Source buffer
 *
 *  @param[in] copySize      Number of bytes to write
 *
 *  @param[in] beta          Beta number of component.
 *
 */
Void statusOp_write_beta(void* dstAddr, void* sourceAddr, Int copySize, Int beta)
{
    Int gateIdx = NUM_BETA;
    IArg key;
    Int pri = Task_getPri(Task_self( ));

    // skip gateMP operation for idle task.
    // Else error occurs: "A_badPriority: An invalid task priority was used."
    if (pri > 0)
    {
        // Check if shared status
        for (gateIdx = 0; gateIdx < NUM_BETA; gateIdx++)
        {
            if (sharedBetas[gateIdx] == beta)
                break;
        }
    }

    if (gateIdx < NUM_BETA)
    {
        // Enter gate
        key = GateMP_enter(gateMP_handles[gateIdx]);
    }
	memcpy(dstAddr, sourceAddr, copySize);
	if (gateIdx < NUM_BETA)
    {
	    // Leave the gate
        GateMP_leave(gateMP_handles[gateIdx], key);
    }
}

/**
 *  @brief     Common read implementation for parameters in status structure.
 *             GateMP protection mechanism will be used for only shared
 *             status structures.
 *
 *  @param[out] dstAddr      Destination buffer, containing the read data
 *
 *  @param[in] sourceAddr    Source buffer
 *
 *  @param[in] copySize      Number of bytes to read
 *
 *  @param[in] beta          Beta number of component.
 *
 *
 */
Void statusOp_read_beta(void* dstAddr, void* sourceAddr, Int copySize, Int beta)
{
    Int gateIdx = NUM_BETA;
    IArg key;
    Int pri = Task_getPri(Task_self( ));

    // skip gateMP operation for idle task.
    // Else error occurs: "A_badPriority: An invalid task priority was used."
    if (pri > 0)
    {
        // Check if shared status
        for (gateIdx = 0; gateIdx < NUM_BETA; gateIdx++)
        {
            if (sharedBetas[gateIdx] == beta)
                break;
        }
    }
    if (gateIdx < NUM_BETA)
    {
        // Enter gate
        key = GateMP_enter(gateMP_handles[gateIdx]);
    }
	memcpy(dstAddr, sourceAddr, copySize);
	if (gateIdx < NUM_BETA)
    {
	    // Leave the gate
        GateMP_leave(gateMP_handles[gateIdx], key);
    }
}


/**
 *  @brief     Common write implementation for parameters in status structure.
 *             GateMP protection mechanism will be used for the shared
 *             status structures that is associated to gateIdx.
 *
 *  @param[out] dstAddr      Destination buffer to write
 *
 *  @param[in] sourceAddr    Source buffer
 *
 *  @param[in] copySize      Number of bytes to write
 *
 *  @param[in] gateIdx       GateMP index
 */
Void statusOp_write(void* dstAddr, void* sourceAddr, Int copySize, Int gateIdx)
{
    IArg key;
    if (gateIdx < NUM_BETA)
    {
        // Enter gate
        key = GateMP_enter(gateMP_handles[gateIdx]);
    }
    memcpy(dstAddr, sourceAddr, copySize);
    if (gateIdx < NUM_BETA)
    {
        // Leave the gate
        GateMP_leave(gateMP_handles[gateIdx], key);
    }
}

/**
 *  @brief     Common read implementation for parameters in status structure.
 *             GateMP protection mechanism will be used for the shared
 *             status structures that is associated to gateIdx.
 *
 *  @param[out] dstAddr      Destination buffer, containing the read data
 *
 *  @param[in] sourceAddr    Source buffer
 *
 *  @param[in] copySize      Number of bytes to read
 *
 *  @param[in] beta          Beta number of component.
 *
 *  @param[in] gateIdx       GateMP index
 */
Void statusOp_read(void* dstAddr, void* sourceAddr, Int copySize, Int gateIdx)
{
    IArg key;
    if (gateIdx < NUM_BETA)
    {
        // Enter gate
        key = GateMP_enter(gateMP_handles[gateIdx]);
    }
    memcpy(dstAddr, sourceAddr, copySize);
    if (gateIdx < NUM_BETA)
    {
        // Leave the gate
        GateMP_leave(gateMP_handles[gateIdx], key);
    }
}
