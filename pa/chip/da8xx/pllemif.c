
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DA8xx PLL & EMIF configuration library function
//
//
//

#include <primus.h>
#include <da830lib.h>
#include <math.h>
// Accurate n = ((t us * f MHz) - 5) / 1.65
void delay (volatile int n)
{
//  asm ("      SUB     B15, 8, B15     ");                 // Done by compiler
//  asm ("      STW     A4, *+B15[1]    ");                 // Done by compiler
	asm ("      STW     B0, *+B15[2]    ");
	asm ("      SUB     A4, 24, A4      ");					// Total cycles taken by this function, with n = 0, including clocks taken to jump to this function
	asm ("      CMPGT   A4, 0, B0       ");
	asm ("delay_loop:                   ");
	asm (" [B0] B       delay_loop      ");
	asm (" [B0] SUB     A4, 6, A4       ");					// Cycles taken by loop
	asm ("      CMPGT   A4, 0, B0       ");
	asm ("      NOP     3               ");
	asm ("      LDW     *+B15[2], B0    ");
//  asm ("      B       B3              ");                 // Done by compiler
//  asm ("      ADD     B15, 8, B15     ");                 // Done by compiler
//  asm ("      NOP     4               ");                 // Done by compiler
}

void pllConfigure (unsigned int CLKMODE, unsigned int PLLM, unsigned int POSTDIV, unsigned int PLLDIV3, unsigned int PLLDIV5,
				   unsigned int PLLDIV7, unsigned int DIV45_EN)
{
	CSL_PllcRegs *pll = (CSL_PllcRegs *) CSL_PLLC_0_REGS;
	unsigned int PREDIV = 1;
	unsigned int PLL_LOCK_TIME_CNT;

	PLL_LOCK_TIME_CNT = 2000 * PREDIV / sqrt (PLLM) * 1.05 + 1;

	// Execute

	// Reset PLL and put in bypass mode
	// Set PLLEN=0 and PLLRST=0
	pll->PLLCTL &= 0xFFFFFFFE;

	// wait for 4 cycles to allow PLLEN mux switches properly to bypass clock
	delay (4);

	// Select the Clock Mode bit 8 as External Clock or On Chip Oscilator
	pll->PLLCTL &= 0xFFFFFEFF;
	pll->PLLCTL |= (CLKMODE << 8);

	/* Set PLLENSRC '0',bit 5, PLL Enable(PLLEN) selection is controlled through MMR */
	pll->PLLCTL &= 0xFFFFFFDF;

	/* PLLCTL.EXTCLKSRC bit 9 should be left at 0 for Primus */
	pll->PLLCTL &= 0xFFFFFDFF;

	/* Clear PLLRST bit to 0 -Reset the PLL */
	pll->PLLCTL &= 0xFFFFFFF7;

	/* Disable the PLL output */
	pll->PLLCTL |= 0x10;

	/* PLL initialization sequence */

	/* Power up the PLL- PWRDN bit set to 0 to bring the PLL out of power down bit */
	pll->PLLCTL &= 0xFFFFFFFD;

	/* Enable the PLL from Disable Mode PLLDIS bit to 0 - This is step is not required for Primus */
	pll->PLLCTL &= 0xFFFFFFEF;

	/* Program the required multiplier value in PLLM */
	pll->PLLM = PLLM;

	/* If desired to scale all the SYSCLK frequencies of a given PLLC, program the POSTDIV ratio */
	pll->POSTDIV = 0x8000 | POSTDIV;

	/* Check for the GOSTAT bit in PLLSTAT to clear to 0 to indicate that no GO operation is currently in progress */
	while (pll->PLLSTAT & 0x1 == 1);

	/* Program the RATIO field in PLLDIVx with the desired divide factors. In addition, make sure in this step you leave the PLLDIVx.DxEN bits set so clocks are still enabled (default). */
	pll->PLLDIV3 = 0x8000 | PLLDIV3;
	pll->PLLDIV5 = 0x8000 | PLLDIV5;
	pll->PLLDIV7 = 0x8000 | PLLDIV7;

	/* Set the GOSET bit in PLLCMD to 1 to initiate a new divider transition. */
	pll->PLLCMD |= 0x1;

	/* Wait for the GOSTAT bit in PLLSTAT to clear to 0 (completion of phase alignment). */
	while (pll->PLLSTAT & 0x1 == 1);

	/* Set the PLLRST bit in PLLCTL to 1 to bring the PLL out of reset */
	pll->PLLCTL |= 0x8;

	/* Wait for PLL to lock. See PLL spec for PLL lock time */
	delay (PLL_LOCK_TIME_CNT);

	/* Set the PLLEN bit in PLLCTL to 1 to remove the PLL from bypass mode */
	pll->PLLCTL |= 0x1;

	CFGCHIP3 &= 0xFFFFFFFC;		// Switch EMIF clocks to PLLDIV first
	CFGCHIP3 &= 0xFFFFFFF8;		// Disable 4.5 divider
    if (DIV45_EN) {
		CFGCHIP3 |= 0x04;       // Enable 4.5 divider PLL
		CFGCHIP3 |= 0x01;       // Select 4.5 divider for EMIFB clock source only (not EMIFA)
	}
}

void emifbConfigure (unsigned int SDCFG, unsigned int SDTIM1, unsigned int SDTIM2, unsigned int SDRFC)
{
	CSL_EmifRegs *emif3c = (CSL_EmifRegs *) CSL_EMIF_0_REGS;

	// Configure
	emif3c->SDCR = SDCFG;
	emif3c->SDTIMR = SDTIM1;
	emif3c->SDTIMR2 = SDTIM2;
	emif3c->SDRCR = SDRFC;
}

