
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common Audio Stream Join Algorithm interface implementation
//
//
//

/*
 *  IASJ default instance creation parameters
 */
#include <std.h>

#include "iasj.h"
#include "paftyp.h"

/*
 *  ======== IASJ_PARAMS ========
 *  This static initialization defines the default parameters used to
 *  create instances of ASJ objects.
 */
const IASJ_Status IASJ_PARAMS_STATUS = {
    sizeof(IASJ_Status),
    1,
    0, 0, 0,
#if PAF_MAXNUMCHAN == 16 //form
    0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f,
    0x0f, 0x0f, 0x30, 0x31, 0x0f, 0x0f, 0x0f, 0x0f,
#endif /* PAF_MAXNUMCHAN == 16 */
    0, //masterGainO
    0, //masterGainI
#if PAF_MAXNUMCHAN == 16 //chanGainO
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
#endif /* PAF_MAXNUMCHAN == 16 */

#if PAF_MAXNUMCHAN == 16 //chanGainI
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
#endif /* PAF_MAXNUMCHAN == 16 */
    50, // rampTimeO: 50 msec/dB (20 dB/sec)
    50  // rampTimeI: 50 msec/dB (20 dB/sec)
};
const IASJ_Params IASJ_PARAMS = {
    sizeof(IASJ_Params),
    &IASJ_PARAMS_STATUS,
    -1, 0,
    0, 0,
    -2*40,      /* volumeRampO */
    -2*40,      /* volumeRampI */
};

