
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DA8xx GPIO management library function
//
//
//

#include <primus.h>

unsigned int gpioInitRead (int gpioNum, int gpioPin)
{
	unsigned int bank, bit, mask;

	bank = gpioNum / 2;
	bit = gpioPin;
	if (gpioNum & 0x01)
		bit += 16;
	mask = 1 << bit;

	CSL_GPIO_REGS->BANK[bank].DIR |= mask;

	if (CSL_GPIO_REGS->BANK[bank].IN_DATA & mask)
		return 1;
	else
		return 0;
}

unsigned int gpioRead (int gpioNum, int gpioPin)
{
	unsigned int bank, bit, mask;

	bank = gpioNum / 2;
	bit = gpioPin;
	if (gpioNum & 0x01)
		bit += 16;
	mask = 1 << bit;

	if (CSL_GPIO_REGS->BANK[bank].IN_DATA & mask)
		return 1;
	else
		return 0;
}

void gpioInitWrite (int gpioNum, int gpioPin, int gpioValue)
{
	unsigned int bank, bit, mask;

	bank = gpioNum / 2;
	bit = gpioPin;
	if (gpioNum & 0x01)
		bit += 16;
	mask = 1 << bit;

	if (gpioValue)
		CSL_GPIO_REGS->BANK[bank].SET_DATA = mask;
	else
		CSL_GPIO_REGS->BANK[bank].CLR_DATA = mask;

	CSL_GPIO_REGS->BANK[bank].DIR &= ~mask;
}

void gpioWrite (int gpioNum, int gpioPin, int gpioValue)
{
	unsigned int bank, bit, mask;

	bank = gpioNum / 2;
	bit = gpioPin;
	if (gpioNum & 0x01)
		bit += 16;
	mask = 1 << bit;

	if (gpioValue)
		CSL_GPIO_REGS->BANK[bank].SET_DATA = mask;
	else
		CSL_GPIO_REGS->BANK[bank].CLR_DATA = mask;
}
