
# 
#  Copyright (C) 2004-2018 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 

#
#
# Common portions of Master Makefiles
#
#
# -----------------------------------------------------------------------------

# .............................................................................
# set quietness

ifdef QUIET
SILENCE := @
endif

# .............................................................................

# replace DIRS variable with only the directories which exist. A := must be
# used to avoid endless recursive expansion
DIRS_C66X  := $(wildcard ${DIRS_C66X})

DIRCLEAN_C66X   = $(addsuffix .clean_c66x,$(DIRS_C66X))
DIRINSTALL_C66X = $(addsuffix .install_c66x,$(DIRS_C66X))

# replace DIRS variable with only the directories which exist. A := must be
# used to avoid endless recursive expansion
DIRS_A15  := $(wildcard ${DIRS_A15})

DIRCLEAN_A15    = $(addsuffix .clean_a15,$(DIRS_A15))
DIRINSTALL_A15 = $(addsuffix .install_a15,$(DIRS_A15))


# .............................................................................
# override MAKE to omit directory navigation
# use := so that current MAKE variable is evaluated before assignment

export MAKE:=$(MAKE) --no-print-directory

# .............................................................................

.PHONY: all clean install $(DIRS_C66X) $(DIRCLEAN_C66X) $(DIRINSTALL_C66X) $(DIRS_A15) $(DIRCLEAN_A15) $(DIRINSTALL_A15)

all: $(DIRS_C66X) $(DIRS_A15)

clean: $(DIRCLEAN_C66X) $(DIRCLEAN_A15)

install: $(DIRINSTALL_C66X) $(DIRINSTALL_A15)

# .............................................................................

$(DIRS_C66X)::
	$(SILENCE)$(MAKE) -C $@ ARCH=c66x

$(DIRCLEAN_C66X): %.clean_c66x:
	$(SILENCE)$(MAKE) -C $* clean ARCH=c66x

$(DIRINSTALL_C66X): %.install_c66x:
	$(SILENCE)$(MAKE) -C $* install ARCH=c66x

$(DIRS_A15)::
	$(SILENCE)$(MAKE) -C $@ ARCH=a15

$(DIRCLEAN_A15): %.clean_a15:
	$(SILENCE)$(MAKE) -C $* clean ARCH=a15

$(DIRINSTALL_A15): %.install_a15:
	$(SILENCE)$(MAKE) -C $* install ARCH=a15
    
    
# special rule to print any makefile variable. Try, e.g., "make print-DIRS_C66X"
print-%:
	@echo $* = $($*)

# .............................................................................
#	@echo $@
