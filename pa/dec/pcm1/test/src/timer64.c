/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/**************************************************************************************
 * FILE PURPOSE: The timer64 driver
 **************************************************************************************
 * FILE NAME: timer64.c
 *
 * DESCRIPTION: The driver for timer64. The timer can only be configured in 64 bit
 *				mode, one shot. Only one timer is supported.
 *
 **************************************************************************************/
#include "type_defs.h"
//#include "device.h"
#include "timer64loc.h"
#include "timer64_api.h"
//#include "hw.h"
//#include "bootTrace.h"

//#define DEVICE_TIMER64_BASE         0x02240000    /* Timer 4 */
#define DEVICE_TIMER64_BASE         0x02250000    /* Timer 5 */


#define DEVICE_REG32_W(x,y)   *(volatile unsigned int *)(x)=((unsigned int)y)
#define DEVICE_REG32_R(x)    (*(volatile unsigned int *)(x))

#define DEVICE_REG8_W(x,y)    *(volatile unsigned char *)(x)=((unsigned char)y)
#define DEVICE_REG8_R(x)     (*(volatile unsigned char *)(x))

/**************************************************************************************
 * FUNCTION PURPOSE: Disable the timer
 **************************************************************************************
 * DESCRIPTION: Sets the reset bits in tgcr, disables the timer in tcr.
 **************************************************************************************/
void NEAR hwTimer64Disable (void)
{
  ////bootTrace (0);

  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TCR, TIMER64_REG_VAL_TCR_DISABLE);
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TGCR, TIMER64_REG_VAL_TGCR_DISABLE);

  /* Set the timer counters to 0 */
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TIM12, 0);
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TIM34, 0);

  /* Set the timer period to 0 */
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_PRD34, 0);
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_PRD12, 0);


} /* hwTimer64Disable */

/**************************************************************************************
 * FUNCTION PURPOSE: Initialize the timer
 **************************************************************************************
 * DESCRIPTION: The timer is disabled configured in 64 bit mode. The 32 LSBs of the
 *              period register are setup.
 **************************************************************************************/
void NEAR hw_tim64_setup (UINT32 timerCyclesMsw, UINT32 timerCycles)
{
  //bootTrace (timerCyclesMsw, timerCycles);

  /* Put the timer into reset */
  hwTimer64Disable ();

  /* Remove reset, leave the TCR so the timer is disabled */
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TGCR, TIMER64_REG_VAL_TGCR_64);


  /* Set the timer counters to 0, even though reset should do this */
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TIM12, 0);
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TIM34, 0);

  /* Write the timer period */
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_PRD34, timerCyclesMsw);
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_PRD12, timerCycles);


} /* hw_tim64_setup */


/**************************************************************************************
 * FUNCTION PURPOSE: Configure the timer as a one shot 64 bit timer.
 **************************************************************************************
 * DESCRIPTION: The timer runs in one shot mode. 64 bit mode is used even though the
 *              cpu cycle count is a 32 bit value.
 **************************************************************************************/
void hwTimer64OneShot (UINT32 timerCyclesMsw, UINT32 timerCycles)
{
 
  //bootTrace (timerCyclesMsw, timerCycles);

  hw_tim64_setup (timerCyclesMsw, timerCycles);

  /* Enable the timer */
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TCR, TIMER64_REG_VAL_TCR_1SHOT);

} /* hwTimer64OneShot */

/**************************************************************************************
 * FUNCTION PURPOSE: Configure the timer as a reload 64 bit timer
 **************************************************************************************
 * DESCRIPTION: The timer runs in reload mode
 **************************************************************************************/
void hwTimer64Reload (UINT32 timerCyclesMsw, UINT32 timerCycles)
{

  //bootTrace (timerCyclesMsw, timerCycles);

  hw_tim64_setup (timerCyclesMsw, timerCycles);

  /* Enable the timer */
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TCR, TIMER64_REG_VAL_TCR_RELOAD);

} /* hwTimer64Reload */

/***************************************************************************************
 * FUNCTION PURPOSE: Return true if the timer has expired
 ***************************************************************************************
 * DESCRIPTION: This function returns true if the timer value is 0
 ***************************************************************************************/
bool hwTimer64Expired (void)
{
  UINT32 tim12;
  UINT32 tim34;


  tim12 = DEVICE_REG32_R (DEVICE_TIMER64_BASE + TIMER64_REG_TIM12);
  tim34 = DEVICE_REG32_R (DEVICE_TIMER64_BASE + TIMER64_REG_TIM34);

  if ((tim12 == 0) && (tim34 == 0))
    return (TRUE);

  return (FALSE);

}

/****************************************************************************************
 * FUNCTION PURPOSE: Stop the timer, resets the count, restarts the timer
 ****************************************************************************************
 * DESCRIPTION: The timer is restarted in one shot mode. The current count is returned.
 ****************************************************************************************/
void hwTimer64Reset (UINT32 *msw, UINT32 *lsw)
{

  //bootTrace ((UINT32)msw, lsw);

  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TCR, TIMER64_REG_VAL_TCR_DISABLE);
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TGCR, TIMER64_REG_VAL_TGCR_DISABLE);

  *msw = DEVICE_REG32_R (DEVICE_TIMER64_BASE + TIMER64_REG_TIM12);  
  *lsw = DEVICE_REG32_R (DEVICE_TIMER64_BASE + TIMER64_REG_TIM34);  

  /* Set the timer counters to 0 */
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TIM12, 0);
  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TIM34, 0);

  DEVICE_REG32_W (DEVICE_TIMER64_BASE + TIMER64_REG_TCR, TIMER64_REG_VAL_TCR_1SHOT);

} /* hwTimer64Reset */


/****************************************************************************************
 * FUNCTION PURPOSE: Return the current timer count
 ****************************************************************************************
 * DESCRIPTION: The current timer value is read
 ****************************************************************************************/
void hwTimerGetCount (UINT32 *msw, UINT32 *lsw)
{
  //bootTrace ((UINT32)msw, lsw);

  *lsw = DEVICE_REG32_R (DEVICE_TIMER64_BASE + TIMER64_REG_TIM12);  
  *msw = DEVICE_REG32_R (DEVICE_TIMER64_BASE + TIMER64_REG_TIM34);  

} /* hwTimerGetCount */
