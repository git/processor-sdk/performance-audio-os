
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library -- Vector Operations
//
//
//

#include "cpl.h"
#include "paftyp.h"


void CPL_vecAdd_(float * restrict pIn1, float * restrict pIn2, float * restrict pOut, int n)
{
    int i;
    for (i = 0; i < n; ++i)
        pOut[i] = pIn1[i] + pIn2[i];
}
/*
void CPL_vecClip(float * restrict pIn, float max, float min, float * restrict pOut, int n)
{
    int i;
    for (i = 0; i < n; ++i) {
        if (pIn[i] > max)
            pOut[i] = max;
        if (pIn[i] < min)
            pOut[i] = min;
    }
    }

float CPL_vecClip1(float temp)
{       	
	if(temp  > (float)1.0)
		temp = (float)0.999999999999999;
	if(temp < (float)(-1.0))
		temp = (float)(-1.0);
	return(temp);    
}
*/
void CPL_vecLinear2_(float c1, float * restrict pIn1, float c2, float * restrict pIn2, float * restrict pOut, 
                    int nSamples)
{
    int i;
    for (i = 0; i < nSamples; ++i)
        pOut[i] = c1*pIn1[i] + c2*pIn2[i];
}

void CPL_vecLinear2Common_(float c, float * restrict pIn1, float * restrict pIn2, float * restrict pOut, int nSamples)
{
    int i;
    for (i = 0; i < nSamples; ++i)
        pOut[i] = (pIn1[i] + pIn2[i])*c;
}

void CPL_vecLinear2Incr_(float c1, float Incr, float * restrict pIn1, float c2, float * restrict pIn2, float * restrict pOut, int nSamples)
{
    int i;
    for (i = 0; i < nSamples; ++i)
        {
          pOut[i] = c1*pIn1[i] + c2*pIn2[i];
          c1 -= Incr;
          c2 += Incr;         
        }  
}

void CPL_vecScale_(float c, float * restrict pIn, float * restrict pOut, int n)
{
    int i;
    for (i = 0; i < n; ++i)
        pOut[i] = c*pIn[i];
}

void CPL_vecScaleAcc_(float c, float * restrict pIn, float * restrict pOut, int n)
{
    int i;
    for (i = 0; i < n; ++i)
        pOut[i] += c*pIn[i];
}

void CPL_vecScaleIncr_(float c, float Incr, float * restrict pIn, float * restrict pOut, int n)
{
    int i;
    for (i = 0; i < n; ++i)
      {
         pOut[i] = c*pIn[i];
         c += Incr;
      }  
}
void CPL_vecSet_(float val, float * restrict pOut, int n)
{
    int i;
    for (i = 0; i < n; ++i)
        pOut[i] = val;
}
void CPL_vecSetArr_(float val, float ** restrict pOut, int n, int a)
{
    int i,ArrCount;
	for(ArrCount=0;ArrCount<a;ArrCount++)
    for (i = 0; i < n; i++)
        pOut[ArrCount][i] = val;
}
void CPL_ivecCopy_(int * restrict pIn, int * restrict pOut, int n)
{
    int i;
    for (i = 0; i < n; ++i)
        pOut[i] = pIn[i];
}
void CPL_svecSet_(int val, short * restrict pOut, int n)
{
    int i;
    for (i = 0; i < n; ++i)
        pOut[i] = val;
}

void CPL_ivecSet_(int val, int * restrict pOut, int n)
{
    int i;
    for (i = 0; i < n; ++i)
        pOut[i] = val;
}
void CPL_imaskScale_(int mask, float scale, int * restrict pIn, 
        float * restrict pOut, int stride, int n)
{
    int i;
    for (i = 0; n--; i += stride)
        *pOut++ = (mask & pIn[i]) * scale;
}
void CPL_smaskScale_(int mask, float scale, short * restrict pIn, 
        float * restrict pOut, int stride, int n)
{
    int i;
    for (i = 0; n--; i += stride)
        *pOut++ = (mask & pIn[i]) * scale;
}
void CPL_vecStrideScale_(float scale, float * restrict pIn, 
        float * restrict pOut, int stride, int n)
{
    int i;
    for (i = 0; n--; i += stride)
        *pOut++ = pIn[i] * scale;
}

void CPL_sumDiff_(float * restrict p1, float * restrict p2, int n)
{
	int i;
	float a,b;
	for(i=0;i<n;i++)
	{
		a = p1[i];
		b = p2[i];
		p1[i] = a+b;
		p2[i] = a-b;
	}
}

//---------------------------SFX COMMON FUNCTIONS--------------------------//

/*
 * Return sampling rate in Hz.
 */
float CPL_GetPAFSampleRate_(int sampRateEnum)
{
	switch (sampRateEnum) {
		default:
		case PAF_SAMPLERATE_UNKNOWN:
		case PAF_SAMPLERATE_NONE:
			return 48000;
		case PAF_SAMPLERATE_32000HZ:
			return 32000;
		case PAF_SAMPLERATE_44100HZ:
			return 44100;
		case PAF_SAMPLERATE_48000HZ:
			return 48000;
		case PAF_SAMPLERATE_88200HZ:
			return 88200;
		case PAF_SAMPLERATE_96000HZ:
			return 96000;
		case PAF_SAMPLERATE_192000HZ:
			return 192000;
		case PAF_SAMPLERATE_64000HZ:
			return 64000;
		case PAF_SAMPLERATE_128000HZ:
			return 128000;
		case PAF_SAMPLERATE_176400HZ:
			return 176400;
		case PAF_SAMPLERATE_8000HZ:
			return 8000;
		case PAF_SAMPLERATE_11025HZ:
			return 11025;
		case PAF_SAMPLERATE_12000HZ:
			return 12000;
		case PAF_SAMPLERATE_16000HZ:
			return 16000;
		case PAF_SAMPLERATE_22050HZ:
			return 22050;
		case PAF_SAMPLERATE_24000HZ:
			return 24000;
	}
}

//Get number of channels. If number of channels set to 0
//  the satellite format not supported.
int CPL_GetNumSat_(int config)
{
   int numSat;
   switch (config) {
      case PAF_CC_SAT_MONO:
         numSat = 1;
         break;
      case PAF_CC_SAT_STEREO:
         numSat = 2;
         break;
      case PAF_CC_SAT_PHANTOM1:
         numSat = 3;
         break;
      case PAF_CC_SAT_PHANTOM2:
         numSat = 4;
         break;
      case PAF_CC_SAT_PHANTOM3:
         numSat = 5;
         break;
      case PAF_CC_SAT_PHANTOM4:
         numSat = 6;
         break;
      case PAF_CC_SAT_3STEREO:
         numSat = 3;
         break;
      case PAF_CC_SAT_SURROUND1:
         numSat = 4;
         break;
      case PAF_CC_SAT_SURROUND2:
         numSat = 5;
         break;
      case PAF_CC_SAT_SURROUND3:
         numSat = 6;
         break;
      case PAF_CC_SAT_SURROUND4:
         numSat = 7;
         break;
      default:
         numSat = 0;
         break;
   }
   return numSat;
}


/**************************************************************************
 *
 * Non-interpolated Delay Funtions, that are used by Integer(16 bit)
 *                                                 & Float(32 bit) lines
 *
 **************************************************************************/



/*
 * Process a block of samples. Note that we don't call block processing
 * functions with n = 0. Uses Integer(16 bit) Delay line
 */
#define BUFSCALE (12000.0f) 
 
void CPL_intDelayProc_(WaDelay *d, float *input, float *output, int n)
{
   int j;
   short int *buf = (short int *)d->buf;
   int addr = d->addr;
   int nbuf = d->nbuf;
   int delay = d->delay;
   int addr_mask = d->addr_mask;
	   
   for (j = 0; j < n; j++) {
	  buf[addr++] = input[j] * BUFSCALE ;
	  if (addr >= nbuf) addr = 0;

	  output[j]= (1/BUFSCALE) *
				 buf[(addr - delay - 1) & addr_mask];
   }
   d->addr = addr;
}


/*
 * Process a block of samples. Note that we don't call block processing
 * functions with n = 0. Uses Float(32 bit) Delay line
 */
void CPL_floatDelayProc_(WaDelay *d, float *input, float *output, int n)
{
   int j;
   float *buf = (float *)d->buf;
   int addr = d->addr;
   
   for (j = 0; j < n; j++) {
	buf[addr++] = input[j];
	if (addr >= d->nbuf) addr = 0;
	  
	output[j] = buf[(addr - d->delay - 1) & d->addr_mask];
   }
   d->addr = addr;
}


/*
 * Set the delay without any interpolation.
 */
void CPL_DelaySet_(WaDelay *d, int delay)
{
	
	d->delay = (delay < 0) ? 0 : (delay > d->nbuf-1 ? d->nbuf-1: delay);
	
}

#ifndef _TMS320C6X
#define	_lmbd		lmbd_c
unsigned int lmbd_c(unsigned int src1,unsigned int src2);
#endif
/*
 * Return smallest power of two that is greater than or equal to a.
 */
#if ARMCOMPILE
static int delay_next_power2(int a)
{
	int b = 1;

	while (b < a) {
		b <<= 1;
	}
	return b;
}
#else
#define delay_next_power2(x)   (1 << (32-_lmbd(1,x-1)))
#endif

/*
 * Initialize a delay control structure.
 */

void CPL_DelayInit_(
    WaDelay *d, 		/* ptr to delay structure */
	int type,			/* delay type/flags */
	int max_delay,		/* maximum delay */
	int delay)	    	/* initial delay */
{ 
   float *buf_float;    
	
   if (max_delay < 1) max_delay = 1;

   d->nbuf = delay_next_power2(max_delay);
   if (type == DELAY_TYPE_FLOAT)
   {
	  d->proc = CPL_floatDelayProc;
	  
	  buf_float = (float *)d->buf;
	  /*Clear the float buffer*/
	  CPL_vecSet(0.0f, buf_float, max_delay);
	}
   else
   {
	  d->proc = CPL_intDelayProc;
	  /*Clear the float buffer*/
	  CPL_ivecSet(0, d->buf, max_delay>>1);
   }

   d->addr_mask = d->nbuf - 1;
   d->addr = 0;
   CPL_DelaySet(d, delay);
}

