To build the libraries from Linux (NOTE: this is the default, so these steps are only necessary if build has been configured for windows or cygwin):
1. In pa/build
	- copy rules_a15_linux.mk to rules_a15.mk.
	- copy rules_linux.mk to rules.mk
	- copy target_linux.mk to target.mk
	- copy tools_linux.mk to tools.mk.
2. In pa/build/tools.mk
	- update the variable 'TI_DIR' to reflect the CCS installation path
	- update the variable 'CG_TOOLS' to reflect the Compiler version and path
	- update the version numbers of BIOS, XDC, IPC, Link and XDAIS
3. In pa/build/target.mk
	- update the variable 'ROOTDIR' to reflect PAF installation path
4. Ensure python, sed and make packages are installed on Linux host
5. From pa/build directory issue the following commands
	- make clean
	- make install

To build the libraries from Windows command prompt:
1. In pa\build
	- copy rules_a15_windows.mk to rules_a15.mk.
	- copy rules_windows.mk to rules.mk
	- copy target_windows.mk to target.mk
	- copy tools_window.mk to tools.mk.
2. In pa\build\tools.mk
	- update the variable 'TI_DIR' to reflect the CCS installation path
	- update the variable 'CG_TOOLS' to reflect the Compiler version and path
	- update the version numbers of BIOS, XDC, IPC, Link and XDAIS
3. In pa\build\target.mk, 
	- update the variable 'ROOTDIR' to reflect PAF installation path
4. Install sed version 4.2.1 from http://gnuwin32.sourceforge.net/packages/sed.htm and update Windows path variable so that sed is in the execution path
5. Install python version 2.7.6 from http://www.python.org/download
6. If Cygwin is already installed on the machine, ensure Cygwin binaries are not present in locations specified in the Windows path variable
7. From pa\build directory issue the following commands
	- <CCS installation path>\ccsv6\utils\bin\gmake clean
	- <CCS installation path>\ccsv6\utils\bin\gmake install

To build the libraries from Cygwin:
1. In pa/build
	- copy rules_a15_cygwin_linux.mk to rules_a15.mk.
	- copy rules_cygwin_linux.mk to rules.mk
	- copy target_cygwin_linux.mk to target.mk
	- copy tools_cygwin.mk to tools.mk.
2. In pa/build/tools.mk
	- update the variable 'TI_DIR' to reflect the CCS installation path
	- update the variable 'CG_TOOLS' to reflect the Compiler version and path
	- update the version numbers of BIOS, XDC, IPC, Link and XDAIS
3. In pa/build/target.mk
	- update the variable 'ROOTDIR' to reflect PAF installation path
4. Ensure python, sed and make packages are installed on Cygwin
5. From pa/build directory issue the following commands
	- make clean
	- make install


