//
// 
// 
//
// DMA supporting C Modules
//
// Copyright 2016 by Texas Instruments Incorporated.  All rights reserved.
//
// $Log: SimulateDMA modules $
// 
// Initial version.
//
//

#include <string.h>
#include <stdio.h> // for printf()
#include "simulate_dma.h"

static const char *gpDmaTransSrc = NULL;
static char *gpDmaTransDst = NULL;
static int gnDmaTransLength = 0;
static int gnDmaTransIndex  = -1;

#if defined(ARMCOMPILE)
#define assert(x)  \
{ \
    if (!(x)) { \
        printf("\n!!\n!!  Assertion failed at  %s, line %d.  !!\n!!\n", __FILE__, __LINE__); \
        while(1); \
    }  \
}
#elif defined (_TMS320C6X)
#include <assert.h>
#endif

int SimulateKickDma(void *handle, void *pSrc, void *pDst, int length)
{
	gpDmaTransSrc = pSrc;
	gpDmaTransDst = pDst;
	gnDmaTransLength = length;
	gnDmaTransIndex  = gnDmaTransIndex + 1;

	return gnDmaTransIndex;
}
void SimulateCompDma(void *handle, int id)
{
	if (gnDmaTransIndex == id) {
		assert(gpDmaTransSrc != NULL);
		assert(gpDmaTransDst != NULL);
		memcpy(gpDmaTransDst, gpDmaTransSrc, gnDmaTransLength);
		gpDmaTransSrc = NULL;
		gpDmaTransDst = NULL;
	}
	else {
		assert(!"bug in DMA control");
	}
}
