/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/


/**
 *  @file   fil_tii_ialgv.c
 *
 *  @brief  This file contains the function table definitions for all
 *          interfaces implemented by the FIL_TII module that derive
 *          from IALG
 *
 *  We place these tables in a separate file for two reasons:
 *     1. We want allow one to one to replace these tables
 *        with different definitions.  For example, one may
 *        want to build a system where the FIL is activated
 *        once and never deactivated, moved, or freed.
 *
 *     2. Eventually there will be a separate "system build"
 *        tool that builds these tables automatically 
 *        and if it determines that only one implementation
 *        of an API exists, "short circuits" the vtable by
 *        linking calls directly to the algorithm's functions.
 */
/*
 *  ======== fil_tii_ialgv.c ========
 *  This file contains the function table definitions for all
 *  interfaces implemented by the FIL_TII module that derive
 *  from IALG
 *
 *  We place these tables in a separate file for two reasons:
 *     1. We want allow one to one to replace these tables
 *        with different definitions.  For example, one may
 *        want to build a system where the FIL is activated
 *        once and never deactivated, moved, or freed.
 *
 *     2. Eventually there will be a separate "system build"
 *        tool that builds these tables automatically 
 *        and if it determines that only one implementation
 *        of an API exists, "short circuits" the vtable by
 *        linking calls directly to the algorithm's functions.
 */

#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include <ifil.h>
#include <fil_tii.h>
#include <fil_tii_priv.h>

#define IALGFXNS \
    &FIL_TII_IALG,       /* module ID */                         \
    FIL_TII_activate,    /* activate */                          \
    FIL_TII_alloc,       /* alloc */                             \
    FIL_TII_control,     /* control */                           \
    FIL_TII_deactivate,  /* deactivate */                        \
    FIL_TII_free,        /* free */                              \
    FIL_TII_initObj,     /* init */                              \
    FIL_TII_moved,       /* moved */                             \
    FIL_TII_numAlloc     /* numAlloc*/                           \

/*
 *  ======== FIL_TII_IFIL ========
 *  This structure defines TII's implementation of the IFIL interface
 *  for the FIL_TII module.
 */
const IFIL_Fxns FIL_TII_IFIL = {       /* module_vendor_interface */
    (void *)IALGFXNS,
    FIL_TII_reset,
    FIL_TII_apply,
    FIL_varsPerCh,
    FIL_enquire,
    FIL_memReset,
    FIL_TII_initfilter,
    FIL_TII_resetfilter,
    FIL_setup,
    FIL_varReset,
	FIL_TII_applyIR
};

/*
 *  ======== FIL_TII_IALG ========
 *  This structure defines TII's implementation of the IALG interface
 *  for the FIL_TII module.
 */
/*
 *  We duplicate the structure here to allow this code to be compiled and
 *  run non-DSP platforms at the expense of unnecessary data space
 *  consumed by the definition below.
 */
const IALG_Fxns FIL_TII_IALG = {       /* module_vendor_interface */
    (void *)IALGFXNS
};



