
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// 
// One-to-Many Audio Mixer Algorithm
// 
// 
// Ideal Mixer Requirements
// 1)  Good enough for many customers to use without modification.
// 2)  Supports 1 to many mixing:  One input channel can be routed to multiple output channels.
// 3)  Must processes samsiz to match gains applied
// 4)  Any supported gain configuration can be reached by run time alpha commands
// 5)  Include alpha commands to reach common configurations easily.
// 6)  Support per stream mute
// 7)  Respect channel masks
// 8)  Support a mode switch to mix with stream 0 or over stream 0.
// 9) Comment liberally to make it suitable as a basis for customization.
//
//
//   Combine multiple audio streams into one in a general manner.
//
//   The only significant difference between an AMIX Algorithm and an ASP
//   Algorithm is that (1) the former references an array of Audio Frames
//   (Audio Frame Array or AFA) while the latter references a single Audio 
//   Frame (Audio Frame Pointer or AFP), and (2) the former combines into
//   the last element of the AFF or "primary" from all elements of the AFF,
//   including both the first N-1 elements or "secondary" as well as the
//   last element or "primary," without altering the first N-1 elements of 
//   the AFF, while the latter modifies the Audio Frame directly via the AFP.
//
//
//
//

/*
 *  AMIX Module implementation - MDS implementation of a One-to-Many Audio Mixer Algorithm.
 */

#include <stdio.h>
#include <assert.h>
#include <std.h>
#include <xdas.h>

#include <amix.h>
#include <iamix.h>
#include <amix_mds.h>
#include <amix_mds_priv.h>
#include <amixerr.h>

#include "paftyp.h"
#include "paftyp_a.h"
#include "pafmac_a.h"
#include <math.h>
#include <cpl.h>
#include "stdasp.h"

#ifndef _TMS320C6X
#include "c67x_cintrins.h"
#endif

// Local symbol definitions

#if PAF_AUDIODATATYPE_FIXED
#define ZERO 0
#else
#define ZERO 0.
#endif

// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
//
#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#if PAF_DEVICE_VERSION == 0xE000
#define _DEBUG // This is to enable log_printfs
#endif /* PAF_DEVICE_VERSION */
#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
// For release, set mask to 0.
#define CURRENT_TRACE_MASK  1   // terse only

#define TRACE_MASK_TERSE    1   // only flag errors
#define TRACE_MASK_GENERAL  2   // describe normal behavior
#define TRACE_MASK_VERBOSE  4   // detailed trace of operation

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
#else
 #define TRACE_VERBOSE(a)
#endif


// -----------------------------------------------------------------------------


/*
 *  ======== satvol ========
 *  Local implementation of volume saturation operation.
 */

inline Int 
satvol (Int x)
{
    return x < -0x8000 ? -0x8000 : x > 0x7fff ? 0x7fff : x;
}


/*
 *  ======== AMIX_MDS_apply ========
 *  MDS's implementation of the apply operation.
 */

Int 
AMIX_MDS_apply(IAMIX_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    AMIX_MDS_Obj *amix = (Void *)handle;

    PAF_AudioFrame * restrict pAFO   = &pAudioFrame[0];
    PAF_AudioFrame * restrict pAFI_a = &pAudioFrame[1];
    PAF_AudioFrame * restrict pAFI_b = &pAudioFrame[2];

    // Exit if mode is disabled.
    if (amix->pStatus->mode == 0)
        return 0;

    // Perform mix:
    handle->fxns->mixit (handle, pAFO, pAFI_a, pAFI_b, 1);

#ifdef PAF_PROCESS_AMIX
    PAF_PROCESS_SET (pAudioFrame->sampleProcess, AMIX);
#endif /* PAF_PROCESS_AMIX */

    return 0;
}

/*
 *  ======== AMIX_MDS_reset ========
 *  MDS's implementation of the information operation.
 */
Int 
AMIX_MDS_reset(IAMIX_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    AMIX_MDS_Obj *amix = (Void *)handle;

    PAF_AudioFrame * restrict pAFO = &pAudioFrame[0];
    PAF_AudioFrame * restrict pAFI_a = &pAudioFrame[1];
    PAF_AudioFrame * restrict pAFI_b = &pAudioFrame[2];

    // Exit if mode is disabled.
    if (amix->pStatus->mode == 0)
        return 0;

    // Perform mix setup:
    handle->fxns->mixit (handle, pAFO, pAFI_a, pAFI_b, 0);

#ifdef PAF_PROCESS_AMIX
    PAF_PROCESS_SET (pAudioFrame->sampleProcess, AMIX);
#endif /* PAF_PROCESS_AMIX */

    return 0;
}

/*
**  fixupOutputStreamChannelConfiguration updates the channelConfigurationStream of the
**  audio frame to be the minimal representable superset of channels in the passed 
**  bitmask 'oMask'.
**  If the bitmask is not exactly representable, channels are 'added' (and possibly 
**  cleared, though not currently).
*/
#define LEGACY_SAT_CHANNELS_MAP \
	((1<<PAF_LEFT)|(1<<PAF_RGHT)|(1<<PAF_LSUR)|(1<<PAF_RSUR)\
        |(1<<PAF_CNTR)|(1<<PAF_LBAK)|(1<<PAF_RBAK))
#define LEGACY_SUB_CHANNELS_MAP \
	((1<<PAF_LSUB)|(1<<PAF_RSUB))
#define EXTENDED_CHANNELS_MAP \
	(~(LEGACY_SAT_CHANNELS_MAP | LEGACY_SUB_CHANNELS_MAP))

static void fixupOutputStreamChannelConfiguration (PAF_AudioFrame *pOutAF, 
                                                   PAF_ChannelMask_HD oMask)
{
    PAF_ChannelMask_HD satChannelMask;
    PAF_ChannelMask_HD subChannelMask;
    PAF_ChannelMask_HD extChannelMask;
    PAF_ChannelMask_HD extChannelMaskRemaining;
    PAF_ChannelMask_HD candidateChannelMask;
    PAF_ChannelConfiguration pCC;
    PAF_ChannelConfiguration pCCTemp;

    pCC.full = 0;
    satChannelMask = oMask & LEGACY_SAT_CHANNELS_MAP;
    subChannelMask = oMask & LEGACY_SUB_CHANNELS_MAP;
    extChannelMask = oMask & EXTENDED_CHANNELS_MAP;
    //  Resolve the legacy channels and the extended channels separately
    //  First, resolve the legacy satellites.
    pCCTemp.full = 0;
    for (pCCTemp.part.sat = PAF_CC_SAT_NONE; 
         pCCTemp.part.sat < PAF_CC_SAT_N; 
         pCCTemp.part.sat++)
    {
        candidateChannelMask = pOutAF->fxns->channelMask (pOutAF, pCCTemp);
        // if all of the legacy satellite channels in oMask are 'covered', 
        // use this part.sat
        if (0 == (satChannelMask & ~candidateChannelMask))
        {
            pCC = pCCTemp;
            break;
        }
    }
    // there should be no way to reach here without pCC having been set nonzero.
    // now the legacy subwoofers
    // note that the channelMask() routine returns (-1) if the part.sat is 
    // zero (i.e., 'unknown') - so start with 'NONE'.
    pCCTemp.part.sat = PAF_CC_NONE;
    for (pCCTemp.part.sub = PAF_CC_SUB_ZERO; 
         pCCTemp.part.sub < PAF_CC_SUB_N; 
         pCCTemp.part.sub++)
    {
        candidateChannelMask = pOutAF->fxns->channelMask (pOutAF, pCCTemp);
        // if all of the legacy subwoofer channels in oMask are 'covered', 
        // use this part.sub
        if (0 == (subChannelMask & ~candidateChannelMask))
        {
            pCC.part.sub = pCCTemp.part.sub;
            break;
        }
    }
    // There should be no way to reach here without pCC.part.sub having been set 
    // appropriately.
    // Now resolve the extended channels
    pCCTemp.full = 0;
    extChannelMaskRemaining = extChannelMask;
    for (pCCTemp.part.extMask = PAF_CC_EXTMASK_LwRw;
         pCCTemp.part.extMask && extChannelMaskRemaining;
         pCCTemp.part.extMask <<= 1)
    {
        candidateChannelMask = pOutAF->fxns->channelMask (pOutAF, pCCTemp);
        //  If there's any overlap, the channel configuration needs to 
        //  include this extMask bit
        if (extChannelMaskRemaining & candidateChannelMask)
        {
            pCC.part.extMask |= pCCTemp.part.extMask;
            extChannelMaskRemaining &= ~candidateChannelMask;
        }
    }
    // There IS a way to get here with extChannelMaskRemaining still nonzero:  
    // oMask has channels which cannot be represented in any channel configuration.
    // If this happens, PCE won't output them, so we just forget about them.

    // There may be channels in the final pCC that aren't present in oMask 
    // (e.g., something got inserted into Rs but nothing into Ls) - we might need 
    // to clear the content in those channels, but oTime might have zero'd that, 
    // so let's not, for the time being.
    //
    //  candidateChannelMask = pOutAF->fxns->channelMask (pOutAF, pCC );
    //  if (candidateChannelMask & ~oMask)
    //  {
    //      clear the content in each channel in (candidateChannelMask & ~oMask)
    //  }
    pOutAF->channelConfigurationStream = pCC;
}

//
// AMIX_MDS_mixit: perform mix from secondary to primary
//

Int 
AMIX_MDS_mixit(IAMIX_Handle handle, 
               PAF_AudioFrame *pAFO, 
               PAF_AudioFrame *pAFI_a, 
               PAF_AudioFrame *pAFI_b, 
               Int apply)
{
    AMIX_MDS_Obj *amix = (Void *)handle;
    IAMIX_Status *status = amix->pStatus;
    IAMIX_Config *config = &amix->config;

    Int inChanIdx, outChanIdx;
    Int iGain, iGainPart;
    PAF_ChannelMask_HD iMask_a, iMask_b, iMaskBit, oMask, oMaskBit;

    Int sampleCount;
    Int i, j, k;
        
    if (!apply)
    {
        // we might argue it would be good to copy 1->0 and 2->1 if AMIX_MODE_MIX_OVER_CHANNEL_0
        return 0;  // nothing to do.
    }

    sampleCount = pAFO->sampleCount;     /* == pAFI->sampleCount */ 

    // check whether equal gain update requested for channel A
    if (status->stream_A_EqualBalance_ack) 
    {   // A channel gain has been updated to a constant
        int localGainInt = status->stream_A_EqualBalance_set;
        // clip the value
        if (localGainInt > MAX_GAIN) localGainInt = MAX_GAIN;
        if (localGainInt < MIN_GAIN) localGainInt = MIN_GAIN;

        for (i=0; i<PAF_MAXNUMCHAN; i++) 
        {
            for (j=0; j<PAF_MAXNUMCHAN; j++)  
            {
                if (i==j)
                    status->gainMatrix_A[i][j]  = localGainInt;
                else 
                    status->gainMatrix_A[i][j]  = MIN_GAIN;
            }
        }
        status->stream_A_EqualBalance_set = localGainInt;
        status->stream_A_EqualBalance_ack = 0;
        TRACE_GEN((&TR_MOD, "AMIX_mixit: set stream A to equal balance %d", localGainInt)); 
    }
    // check whether equal gain update requested for channel A
    if (status->stream_B_EqualBalance_ack)
    {   // B channel gain has been updated to a constant
        int localGainInt = status->stream_B_EqualBalance_set;
        // clip the value
        if (localGainInt > MAX_GAIN) localGainInt = MAX_GAIN;
        if (localGainInt < MIN_GAIN) localGainInt = MIN_GAIN;

        for (i=0; i<PAF_MAXNUMCHAN; i++) 
        {
            for (j=0; j<PAF_MAXNUMCHAN; j++)  {
                if (i==j)
                    status->gainMatrix_B[i][j]  = localGainInt;
                else 
                    status->gainMatrix_B[i][j]  = MIN_GAIN;
            }
        }
        status->stream_B_EqualBalance_set = localGainInt;
        status->stream_B_EqualBalance_ack = 0;
        TRACE_GEN((&TR_MOD, "AMIX_mixit: set stream B to equal balance %d", localGainInt));
    }


    if (status->mode == AMIX_MODE_MIX_WITH_CHANNEL_0)
    {
        TRACE_TERSE((&TR_MOD, "AMIX_mixit: Mix With not yet supported. Use ASJ."));

        assert(sampleCount == pAFI_a->sampleCount);
        assert(pAFO->fxns);
        assert(pAFI_a->fxns);

        return 0;  

    }

    // mode is AMIX_MODE_MIX_OVER_CHANNEL_0

    if (sampleCount != pAFI_a->sampleCount)
    {
    	TRACE_TERSE((&TR_MOD, "AMIX_mixit: Channel A sample count (%d) does not match output channel (%d).",
    			pAFI_a->sampleCount, sampleCount));
    }
    // assert(sampleCount == pAFI_a->sampleCount);
    if (sampleCount != pAFI_b->sampleCount)
    {
    	TRACE_TERSE((&TR_MOD, "AMIX_mixit: Channel B sample count (%d) does not match output channel (%d).",
    			pAFI_b->sampleCount, sampleCount));
    }
    // assert(sampleCount == pAFI_b->sampleCount);
    assert(pAFO->fxns);
    assert(pAFI_a->fxns);
    assert(pAFI_b->fxns);



    // determine active channels (i.e., channels present in the input)
    iMask_a = pAFI_a->fxns->channelMask (pAFI_a, pAFI_a->channelConfigurationStream);
    iMask_b = pAFI_b->fxns->channelMask (pAFI_b, pAFI_b->channelConfigurationStream);
    // it's premature to limit the output to that requested, since that could result
    // in LFE being discarded if there's no subwoofer
    TRACE_VERBOSE((&TR_MOD, "\nAMIX_mixit: iMask_a: 0x%x.  iMask_b: 0x%x.",
           iMask_a, iMask_b));

    // Compute the floating point gain matrix.
    // Do this every time so that any updates are taken from the status registers
    //  and so that a unified set of gains are available.
    //
    //  On first glance, the rationale for a separate table of floating-point gains is
    //  unclear.  The floating-point gains are not just a floating-point version of the
    //  status->gainMatrix - the floating-point gains are also modified as a function of
    //  the input stream's channel configuration.  This allows the final tight loops
    //  to run without requiring logical tests inside the loops.
    for (outChanIdx=0; outChanIdx < PAF_MAXNUMCHAN; outChanIdx++)
    {
        for (inChanIdx=0, iMaskBit = 1; 
             inChanIdx < PAF_MAXNUMCHAN; 
             inChanIdx++, iMaskBit <<= 1)
        {
            if (iMaskBit & iMask_a)
            {
                iGainPart = status->gainMatrix_A[outChanIdx][inChanIdx];
                if (iGainPart > MIN_GAIN)
                {
                    iGain = satvol (iGainPart + status->stream_A_OutputGain_set);
                    config->fGainMatrix_A[outChanIdx][inChanIdx] 
                        = pAFO->fxns->dB2ToLinear(iGain);
                }
                else
                {
                    config->fGainMatrix_A[outChanIdx][inChanIdx] = 0.0;
                }
            }
            else
            {
                config->fGainMatrix_A[outChanIdx][inChanIdx] = 0.0;
            }
            if (iMaskBit & iMask_b)
            {
                iGainPart = status->gainMatrix_B[outChanIdx][inChanIdx];
                if (iGainPart > MIN_GAIN)
                {
                    iGain = satvol (iGainPart + status->stream_B_OutputGain_set);
                    config->fGainMatrix_B[outChanIdx][inChanIdx] 
                        = pAFO->fxns->dB2ToLinear(iGain);
                }
                else
                {
                    config->fGainMatrix_B[outChanIdx][inChanIdx] = 0.0;
                }
            }
            else
            {
                config->fGainMatrix_B[outChanIdx][inChanIdx] = 0.0;
            }
        }
    }

    // update samsiz here.
    // No samsiz yet.

    oMask = 0;
    // sum the data.  Assume that output buffer starts with zero.
    for (outChanIdx=0, oMaskBit = 1; 
         outChanIdx < PAF_MAXNUMCHAN; 
         outChanIdx++, oMaskBit <<= 1)
    {   
        for (inChanIdx=0; inChanIdx < PAF_MAXNUMCHAN; inChanIdx++)
        {   
            // if input was not enabled by mask, then fGainMatrix entries are zero.
            float gain_A, gain_B;
            float *restrict samout;
            float *restrict saminp_a;
            float *restrict saminp_b;

            gain_A = config->fGainMatrix_A[outChanIdx][inChanIdx];
            gain_B = config->fGainMatrix_B[outChanIdx][inChanIdx];

            if ((gain_A == 0.0) && (gain_B == 0.0))
            {
                TRACE_VERBOSE((&TR_MOD, "AMIX_mixit: skip mixing [%d][%d] both gains zero.", outChanIdx, inChanIdx));
                continue;  // nothing into this channel
            }
            oMask |= oMaskBit;
            saminp_a = pAFI_a->data.sample[outChanIdx];
            saminp_b = pAFI_b->data.sample[outChanIdx];
            samout   = pAFO->data.sample[outChanIdx];

            if ((gain_A == 0.0) && (gain_B != 0.0)) 
            {
                TRACE_VERBOSE((&TR_MOD, "AMIX_mixit: mixing [%d][%d] with only B.", outChanIdx, inChanIdx));
                for (k=0; k < sampleCount; k++)
                    samout[k] = gain_B * saminp_b[k];
                continue;
            }

            if ((gain_A != 0.0) && (gain_B == 0.0)) 
            {
                TRACE_VERBOSE((&TR_MOD, "AMIX_mixit: mixing [%d][%d] with only A.", outChanIdx, inChanIdx));
                for (k=0; k < sampleCount; k++)
                    samout[k] = gain_A * saminp_a[k];
                continue;
            }

            TRACE_VERBOSE((&TR_MOD, "AMIX_mixit: mixing [%d][%d] with both.", outChanIdx, inChanIdx));
            for (k=0; k<sampleCount; k++)
            {
                samout[k] = saminp_a[k]*gain_A + saminp_b[k]*gain_B;
            }   

        }   // end input loop
    }   // end output loop
    // fixup the output stream's channel configuration as per oMask
    fixupOutputStreamChannelConfiguration(pAFO,oMask);
    return 0;
}


float AMIX_MDS_volRamp(IAMIX_Handle handle, PAF_AudioFrame *pAudioFrame, 
                      float volumeRamp,PAF_AudioSize masterGain, XDAS_UInt16 rampTime )
{
    // If requested volume ramp time is non-zero and volume ramp
    // is active (volume master control has changed and current
    // volume level needs to ramp up/down to it), update volume
    // status accordingly:

    if (volumeRamp != masterGain && rampTime > 0) {

        Int rate = pAudioFrame->sampleRate;
        Int cnt = pAudioFrame->sampleCount;

        float frameDbQ1;
        float sampleTime;

        // Calculate audio frame duration in msec
        //   = (sample count / 8) * (8 samples / (x samples/sec)),
        //
        // then convert to required change in 0.5 dB steps (volume is Q1),
        // using given ramp rate, for duration of current audio frame
        //   = 2.0 * frame duration (msec) / ramp rate (msec/dB):
        sampleTime = 1.0f/pAudioFrame->fxns->sampleRateHz(pAudioFrame, rate, PAF_SAMPLERATEHZ_STD);
        frameDbQ1 = ((float )(cnt>>3) * sampleTime * 8000.0f * 2.0f) /
                    (float ) rampTime;

        if (volumeRamp > (float )masterGain) {
            // ramp volume "down," limiting to avoid "overshoot":
            volumeRamp = volumeRamp - (float ) masterGain >= frameDbQ1
                ? volumeRamp -= frameDbQ1
                : masterGain;
        }
        else {
            // ramp volume "up," limiting to avoid "overshoot":
            volumeRamp = (float ) masterGain - volumeRamp >= frameDbQ1
                ? volumeRamp += frameDbQ1
                : masterGain;
        }
    }
    else    // ramp inactive -- use volume master control level:
        volumeRamp = masterGain;

    return volumeRamp;

}
// EOF
