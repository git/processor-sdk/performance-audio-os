/*
* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This header defines all types, constants, and functions used by
 *  applications that use the algorithm. In other words,
 *  all public types, variable and function declarations go here.
 *
 *  Given the well defined nature of the PA/F ASP interface, there is usually
 *  no need to edit or add anything in this header file.
 */
#ifndef MDDEFORMAT_
#define MDDEFORMAT_

#include <alg.h>
#include <ialg.h>
#include <paf_alg.h>
#include <imdDeFormat.h>

#include "paftyp.h"


/*
 *  ======== MDDEFORMAT_Handle ========
 *  ASP Example Demonstration algorithm instance handle
 */
typedef struct IMDDEFORMAT_Obj *MDDEFORMAT_Handle;

/*
 *  ======== MDDEFORMAT_Params ========
 *  ASP Example Demonstration algorithm instance creation parameters
 */
typedef struct IMDDEFORMAT_Params MDDEFORMAT_Params;

/*
 *  ======== MDDEFORMAT_PARAMS ========
 *  Default instance parameters
 */
#define MDDEFORMAT_PARAMS IMDDEFORMAT_PARAMS

/*
 *  ======== MDDEFORMAT_Status ========
 *  Status structure for getting MDDEFORMAT instance attributes
 */
typedef volatile struct IMDDEFORMAT_Status MDDEFORMAT_Status;

/*
 *  ======== MDDEFORMAT_Cmd ========
 *  This typedef defines the control commands MDDEFORMAT objects
 */
typedef IMDDEFORMAT_Cmd   MDDEFORMAT_Cmd;

/*
 * ===== control method commands =====
 */
#define MDDEFORMAT_NULL IMDDEFORMAT_NULL
#define MDDEFORMAT_GETSTATUSADDRESS1 IMDDEFORMAT_GETSTATUSADDRESS1
#define MDDEFORMAT_GETSTATUSADDRESS2 IMDDEFORMAT_GETSTATUSADDRESS2
#define MDDEFORMAT_GETSTATUS IMDDEFORMAT_GETSTATUS
#define MDDEFORMAT_SETSTATUS IMDDEFORMAT_SETSTATUS

#endif  /* MDDEFORMAT_ */
