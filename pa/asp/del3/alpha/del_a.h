
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Speaker Location Delay alpha codes
//
//
//

#ifndef _DEL_A
#define _DEL_A

#include <acpbeta.h>
#include <paftyp_a.h>

#define  readDELMode 0xc200+STD_BETA_DEL,0x0400
#define writeDELModeDisable 0xca00+STD_BETA_DEL,0x0400
#define writeDELModeEnable 0xca00+STD_BETA_DEL,0x0401

#define  readDELUnit 0xc200+STD_BETA_DEL,0x0500
#define writeDELUnitTimeSamples 0xca00+STD_BETA_DEL,0x0500
#define writeDELUnitTimeMillisecondsQ0 0xca00+STD_BETA_DEL,0x0501
#define writeDELUnitTimeMillisecondsQ1 0xca00+STD_BETA_DEL,0x0502
#define writeDELUnitTimeCentimeters 0xca00+STD_BETA_DEL,0x0503
#define writeDELUnitTimeFeet 0xca00+STD_BETA_DEL,0x0504
#define writeDELUnitTimeYards 0xca00+STD_BETA_DEL,0x0505
#define writeDELUnitTimeMeters 0xca00+STD_BETA_DEL,0x0506
#define writeDELUnitTimeDecimilliseconds 0xca00+STD_BETA_DEL,0x0507
#define writeDELUnitLocationSamples 0xca00+STD_BETA_DEL,0x0580
#define writeDELUnitLocationMillisecondsQ0 0xca00+STD_BETA_DEL,0x0581
#define writeDELUnitLocationMillisecondsQ1 0xca00+STD_BETA_DEL,0x0582
#define writeDELUnitLocationCentimeters 0xca00+STD_BETA_DEL,0x0583
#define writeDELUnitLocationFeet 0xca00+STD_BETA_DEL,0x0584
#define writeDELUnitLocationYards 0xca00+STD_BETA_DEL,0x0585
#define writeDELUnitLocationMeters 0xca00+STD_BETA_DEL,0x0586
#define writeDELUnitLocationDecimilliseconds 0xca00+STD_BETA_DEL,0x0587

#define writeDELUnitTimeMilliseconds writeDELUnitTimeMillisecondsQ0
#define writeDELUnitTimeMilliseconds2 writeDELUnitTimeMillisecondsQ1
#define writeDELUnitLocationMilliseconds writeDELUnitLocationMillisecondsQ0
#define writeDELUnitLocationMilliseconds2 writeDELUnitLocationMillisecondsQ1

#define  readDELNumb 0xc200+STD_BETA_DEL,0x0600
#define writeDELNumbN(NN) 0xca00+STD_BETA_DEL,0x0600+((NN)&0xff)

#define  readDELUnused 0xc200+STD_BETA_DEL,0x0700

#if 0 < PAF_MAXNUMCHAN
#define  readDELDelayLeft 0xc300+STD_BETA_DEL,0x0008
#define writeDELDelayLeftN(NN) 0xcb00+STD_BETA_DEL,0x0008,NN
#define  readDELDelayRght 0xc300+STD_BETA_DEL,0x000a
#define writeDELDelayRghtN(NN) 0xcb00+STD_BETA_DEL,0x000a,NN

#endif /* 0 < PAF_MAXNUMCHAN */
#if PAF_MAXNUMCHAN == 4
#define  readDELDelayCntr 0xc300+STD_BETA_DEL,0x000c
#define writeDELDelayCntrN(NN) 0xcb00+STD_BETA_DEL,0x000c,NN
#define  readDELDelaySurr 0xc300+STD_BETA_DEL,0x000e
#define writeDELDelaySurrN(NN) 0xcb00+STD_BETA_DEL,0x000e,NN

#define  readDELDelayMaster 0xc300+STD_BETA_DEL,0x0010
#define writeDELDelayMasterN(NN) 0xcb00+STD_BETA_DEL,0x0010,NN

#elif PAF_MAXNUMCHAN == 6
#define  readDELDelayCntr 0xc300+STD_BETA_DEL,0x000c
#define writeDELDelayCntrN(NN) 0xcb00+STD_BETA_DEL,0x000c,NN
#define  readDELDelaySurr 0xc300+STD_BETA_DEL,0x000e
#define writeDELDelaySurrN(NN) 0xcb00+STD_BETA_DEL,0x000e,NN
#define  readDELDelayLSur 0xc300+STD_BETA_DEL,0x000e
#define writeDELDelayLSurN(NN) 0xcb00+STD_BETA_DEL,0x000e,NN
#define  readDELDelayRSur 0xc300+STD_BETA_DEL,0x0010
#define writeDELDelayRSurN(NN) 0xcb00+STD_BETA_DEL,0x0010,NN
#define  readDELDelaySubw 0xc300+STD_BETA_DEL,0x0012
#define writeDELDelaySubwN(NN) 0xcb00+STD_BETA_DEL,0x0012,NN

#define  readDELDelayMaster 0xc300+STD_BETA_DEL,0x0014
#define writeDELDelayMasterN(NN) 0xcb00+STD_BETA_DEL,0x0014,NN

#elif PAF_MAXNUMCHAN == 8
#define  readDELDelayCntr 0xc300+STD_BETA_DEL,0x000c
#define writeDELDelayCntrN(NN) 0xcb00+STD_BETA_DEL,0x000c,NN
#define  readDELDelaySurr 0xc300+STD_BETA_DEL,0x000e
#define writeDELDelaySurrN(NN) 0xcb00+STD_BETA_DEL,0x000e,NN
#define  readDELDelayLSur 0xc300+STD_BETA_DEL,0x000e
#define writeDELDelayLSurN(NN) 0xcb00+STD_BETA_DEL,0x000e,NN
#define  readDELDelayRSur 0xc300+STD_BETA_DEL,0x0010
#define writeDELDelayRSurN(NN) 0xcb00+STD_BETA_DEL,0x0010,NN
#define  readDELDelaySubw 0xc300+STD_BETA_DEL,0x0012
#define writeDELDelaySubwN(NN) 0xcb00+STD_BETA_DEL,0x0012,NN
#define  readDELDelayBack 0xc300+STD_BETA_DEL,0x0014
#define writeDELDelayBackN(NN) 0xcb00+STD_BETA_DEL,0x0014,NN
#define  readDELDelayLBak 0xc300+STD_BETA_DEL,0x0014
#define writeDELDelayLBakN(NN) 0xcb00+STD_BETA_DEL,0x0014,NN
#define  readDELDelayRBak 0xc300+STD_BETA_DEL,0x0016
#define writeDELDelayRBakN(NN) 0xcb00+STD_BETA_DEL,0x0016,NN

#define  readDELDelayMaster 0xc300+STD_BETA_DEL,0x0018
#define writeDELDelayMasterN(NN) 0xcb00+STD_BETA_DEL,0x0018,NN

#elif PAF_MAXNUMCHAN == 16
#define  readDELDelayCntr 0xc300+STD_BETA_DEL,0x000c
#define writeDELDelayCntrN(NN) 0xcb00+STD_BETA_DEL,0x000c,NN
#define  readDELDelayLCtr 0xc300+STD_BETA_DEL,0x000c
#define writeDELDelayLCtrN(NN) 0xcb00+STD_BETA_DEL,0x000c,NN
#define  readDELDelayRCtr 0xc300+STD_BETA_DEL,0x000e
#define writeDELDelayRCtrN(NN) 0xcb00+STD_BETA_DEL,0x000e,NN
#define  readDELDelayWide 0xc300+STD_BETA_DEL,0x0010
#define writeDELDelayWideN(NN) 0xcb00+STD_BETA_DEL,0x0010,NN
#define  readDELDelayLWid 0xc300+STD_BETA_DEL,0x0010
#define writeDELDelayLWidN(NN) 0xcb00+STD_BETA_DEL,0x0010,NN
#define  readDELDelayRWid 0xc300+STD_BETA_DEL,0x0012
#define writeDELDelayRWidN(NN) 0xcb00+STD_BETA_DEL,0x0012,NN
#define  readDELDelayOver 0xc300+STD_BETA_DEL,0x0014
#define writeDELDelayOverN(NN) 0xcb00+STD_BETA_DEL,0x0014,NN
#define  readDELDelayLOvr 0xc300+STD_BETA_DEL,0x0014
#define writeDELDelayLOvrN(NN) 0xcb00+STD_BETA_DEL,0x0014,NN
#define  readDELDelayROvr 0xc300+STD_BETA_DEL,0x0016
#define writeDELDelayROvrN(NN) 0xcb00+STD_BETA_DEL,0x0016,NN
#define  readDELDelayLTrr 0xc300+STD_BETA_DEL,0x0014
#define writeDELDelayLTrrN(NN) 0xcb00+STD_BETA_DEL,0x0014,NN
#define  readDELDelayRTrr 0xc300+STD_BETA_DEL,0x0016
#define writeDELDelayRTrrN(NN) 0xcb00+STD_BETA_DEL,0x0016,NN
#define  readDELDelayLTrh 0xc300+STD_BETA_DEL,0x0014
#define writeDELDelayLTrhN(NN) 0xcb00+STD_BETA_DEL,0x0014,NN
#define  readDELDelayRTrh 0xc300+STD_BETA_DEL,0x0016
#define writeDELDelayRTrhN(NN) 0xcb00+STD_BETA_DEL,0x0016,NN
#define  readDELDelaySurr 0xc300+STD_BETA_DEL,0x0018
#define writeDELDelaySurrN(NN) 0xcb00+STD_BETA_DEL,0x0018,NN
#define  readDELDelayLSur 0xc300+STD_BETA_DEL,0x0018
#define writeDELDelayLSurN(NN) 0xcb00+STD_BETA_DEL,0x0018,NN
#define  readDELDelayRSur 0xc300+STD_BETA_DEL,0x001a
#define writeDELDelayRSurN(NN) 0xcb00+STD_BETA_DEL,0x001a,NN
#define  readDELDelayBack 0xc300+STD_BETA_DEL,0x001c
#define writeDELDelayBackN(NN) 0xcb00+STD_BETA_DEL,0x001c,NN
#define  readDELDelayLBak 0xc300+STD_BETA_DEL,0x001c
#define writeDELDelayLBakN(NN) 0xcb00+STD_BETA_DEL,0x001c,NN
#define  readDELDelayRBak 0xc300+STD_BETA_DEL,0x001e
#define writeDELDelayRBakN(NN) 0xcb00+STD_BETA_DEL,0x001e,NN
#define  readDELDelaySubw 0xc300+STD_BETA_DEL,0x0020
#define writeDELDelaySubwN(NN) 0xcb00+STD_BETA_DEL,0x0020,NN
#define  readDELDelayLSub 0xc300+STD_BETA_DEL,0x0020
#define writeDELDelayLSubN(NN) 0xcb00+STD_BETA_DEL,0x0020,NN
#define  readDELDelayRSub 0xc300+STD_BETA_DEL,0x0022
#define writeDELDelayRSubN(NN) 0xcb00+STD_BETA_DEL,0x0022,NN
#define  readDELDelayLHed 0xc300+STD_BETA_DEL,0x0024
#define writeDELDelayLHedN(NN) 0xcb00+STD_BETA_DEL,0x0024,NN
#define  readDELDelayRHed 0xc300+STD_BETA_DEL,0x0026
#define writeDELDelayRHedN(NN) 0xcb00+STD_BETA_DEL,0x0026,NN
#define  readDELDelayLTmd 0xc300+STD_BETA_DEL,0x0024
#define writeDELDelayLTmdN(NN) 0xcb00+STD_BETA_DEL,0x0024,NN
#define  readDELDelayRTmd 0xc300+STD_BETA_DEL,0x0026
#define writeDELDelayRTmdN(NN) 0xcb00+STD_BETA_DEL,0x0026,NN
#define  readDELDelayLTft 0xc300+STD_BETA_DEL,0x0024
#define writeDELDelayLTftN(NN) 0xcb00+STD_BETA_DEL,0x0024,NN
#define  readDELDelayRTft 0xc300+STD_BETA_DEL,0x0026
#define writeDELDelayRTftN(NN) 0xcb00+STD_BETA_DEL,0x0026,NN
#define  readDELDelayLTfh 0xc300+STD_BETA_DEL,0x0024
#define writeDELDelayLTfhN(NN) 0xcb00+STD_BETA_DEL,0x0024,NN
#define  readDELDelayRTfh 0xc300+STD_BETA_DEL,0x0026
#define writeDELDelayRTfhN(NN) 0xcb00+STD_BETA_DEL,0x0026,NN

#define  readDELDelayMaster 0xc300+STD_BETA_DEL,0x0028
#define writeDELDelayMasterN(NN) 0xcb00+STD_BETA_DEL,0x0028,NN

#else /* PAF_MAXNUMCHAN */
#error unsupported option
#endif /* PAF_MAXNUMCHAN */

#define  readDELStatus 0xc508,STD_BETA_DEL
#define  readDELControl \
         readDELMode, \
         readDELUnit, \
         readDELNumb, \
         readDELDelayLeft, \
         readDELDelayRght, \
         readDELDelayCntr, \
         readDELDelayLCtr, \
         readDELDelayRCtr, \
         readDELDelayWide, \
         readDELDelayLWid, \
         readDELDelayRWid, \
         readDELDelayOver, \
         readDELDelayLOvr, \
         readDELDelayROvr, \
		 readDELDelayLTrr, \
		 readDELDelayRTrr, \
		 readDELDelayLTrh, \
		 readDELDelayRTrh, \
         readDELDelaySurr, \
         readDELDelayLSur, \
         readDELDelayRSur, \
         readDELDelayBack, \
         readDELDelayLBak, \
         readDELDelayRBak, \
         readDELDelaySubw, \
         readDELDelayLSub, \
         readDELDelayRSub, \
         readDELDelayLHed, \
         readDELDelayRHed, \
		 readDELDelayLTmd, \
		 readDELDelayRTmd, \
		 readDELDelayLTft, \
		 readDELDelayRTft, \
		 readDELDelayLTfh, \
		 readDELDelayRTfh, \

// XX symbolic definitions are obsolete; please replace use. For backards compatibility:
#define writeDELNumbXX(N) writeDELNumbN(0x##N)

/* in support of inverse compilation only */
#define writeDELNumbXX__10__ writeDELNumbXX(10)
#define wroteDELDelayLeft 0x0800+readDELDelayLeft
#define wroteDELDelayRght 0x0800+readDELDelayRght
#define wroteDELDelayCtr  0x0800+readDELDelayCntr
#define wroteDELDelayLCtr 0x0800+readDELDelayLCtr
#define wroteDELDelayRCtr 0x0800+readDELDelayRCtr
#define wroteDELDelayWide 0x0800+readDELDelayWide
#define wroteDELDelayLWid 0x0800+readDELDelayLWid
#define wroteDELDelayRWid 0x0800+readDELDelayRWid
#define wroteDELDelayOver 0x0800+readDELDelayOver
#define wroteDELDelayLOvr 0x0800+readDELDelayLOvr
#define wroteDELDelayROvr 0x0800+readDELDelayROvr
#define wroteDELDelayLTrr 0x0800+readDELDelayLTrr
#define wroteDELDelayRTrr 0x0800+readDELDelayRTrr
#define wroteDELDelayLTrh 0x0800+readDELDelayLTrh
#define wroteDELDelayRTrh 0x0800+readDELDelayRTrh
#define wroteDELDelaySurr 0x0800+readDELDelaySurr
#define wroteDELDelayLSur 0x0800+readDELDelayLSur
#define wroteDELDelayRSur 0x0800+readDELDelayRSur
#define wroteDELDelayBack 0x0800+readDELDelayBack
#define wroteDELDelayLBak 0x0800+readDELDelayLBak
#define wroteDELDelayRBak 0x0800+readDELDelayRBak
#define wroteDELDelaySubw 0x0800+readDELDelaySubw
#define wroteDELDelayLSub 0x0800+readDELDelayLSub
#define wroteDELDelayRSub 0x0800+readDELDelayRSub
#define wroteDELDelayLHed 0x0800+readDELDelayLHed
#define wroteDELDelayRHed 0x0800+readDELDelayRHed
#define wroteDELDelayLTmd 0x0800+readDELDelayLTmd
#define wroteDELDelayRTmd 0x0800+readDELDelayRTmd
#define wroteDELDelayLTft 0x0800+readDELDelayLTft
#define wroteDELDelayRTft 0x0800+readDELDelayRTft
#define wroteDELDelayLTfh 0x0800+readDELDelayLTfh
#define wroteDELDelayRTfh 0x0800+readDELDelayRTfh

#endif /* _DEL_A */
