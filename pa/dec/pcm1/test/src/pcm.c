/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include "ipcm.h"
#include "pcm.h"


/** 
********************************************************************************
*  @fn     pcm.c
*  @brief  Creates an PCM instance object (using parameters specified 
*          by prms)
*          
*  @param[in] fxns    : Function pointer
*  @param[in] prms    : Parameters used for the object creation
*
*  @return   IALG_EFAIL or IALG_EOK
********************************************************************************
*/
PCM_Handle PCM_create(
    const IPCM_Fxns *fxns,
    const PCM_Params *params,
    PAF_IALG_Config *pafConfig)
{
  return ((PCM_Handle) PAF_ALG_create_((IALG_Fxns *) fxns, NULL, 
      (IALG_Params*) params, NULL, (PAF_IALG_Config*) pafConfig));
}

/**
********************************************************************************
*  @fn     PCM_delete
*  @brief  Deletes PCM instance object 
*          
*  @param[in] handle  : PCM Decoder Hanlde 
*
*  @return None
********************************************************************************
*/
Void PCM_delete(PCM_Handle handle)
{
  PAF_ALG_delete((IALG_Handle) handle);
}

/**
********************************************************************************
*  @fn     PCM_init
*  @brief  PCM init
*          
*  @return None
********************************************************************************
*/
Void PCM_init(Void)
{
}

/**
********************************************************************************
*  @fn     PCM_exit
*  @brief  PCM Exit
*          
*  @return None
********************************************************************************
*/
Void PCM_exit(Void)
{
}

/**
********************************************************************************
*  @fn     PCM_control
*  @brief  PCM control
*
*  @param[in] handle      : PCM Decoder Hanlde 
*  @param[in] cmd         : Command 
*  @param[in/out] params  : Parameters 
*  @param[in/out] status  : Status
*          
********************************************************************************
*/
XDAS_Int32 PCM_control(PCM_Handle handle,
                            PCM_Cmd cmd,
                            PCM_Status * status
                           )
{
    XDAS_Int32  error = 0;

    error = handle->fxns->ialg.algControl((IALG_Handle) handle,
                                         (IALG_Cmd)cmd,
                                         (IALG_Status *) status
                                        );

  return error;
}

/**
********************************************************************************
*  @fn     PCM_decodeFrame
*  @brief  This function decodes one frame
*
*  @param[in] handle     : PCM Decoder Hanlde 
*  @param[in] inBufs     : input buffer
*  @param[in] outBufs    : output buffer 
*  @param[in] inargs     : input arguments
*  @param[in] outargs    : output Arguments
*          
********************************************************************************
*/
XDAS_Int32 PCM_decoodeFrame(IPCM_Handle PCMHandle,  
                           PAF_EncodeInStruct * inStruct,
 						   PAF_EncodeOutStruct * outStruct)
{
  int error;

  error = PCMHandle->fxns->decode(PCMHandle, NULL, inStruct, outStruct);

  return (error);
}

/*
 *  ======== PCM_info ========
 * 
 */
XDAS_Int32 PCM_Info(IPCM_Handle PCM_Handle, 
                    PAF_EncodeControl * Control, PAF_EncodeStatus * Status)
{
	UInt error;

    error = PCM_Handle->fxns->info(PCM_Handle, NULL, Control ,Status);
    return (error);
}
/*
 *  ======== PCM_reset ========
 * 
 */
XDAS_Int32 PCM_Reset(IPCM_Handle PCM_Handle,
                     PAF_EncodeControl * Control, PAF_EncodeStatus * Status)
{
	UInt error;
	error = PCM_Handle->fxns->reset(PCM_Handle, NULL, Control ,Status);
	return (error);
}
