//
// 
// 
//
// DMA supporting C Header File
//
// Copyright 2016 by Texas Instruments Incorporated.  All rights reserved.
//
// $Log: SimulateDMA modules $
// 
// Initial version.
//
//

int SimulateKickDma(void *handle, void *pSrc, void *pDst, int length);

void SimulateCompDma(void *handle, int id);