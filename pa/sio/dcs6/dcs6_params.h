
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DA8xx SPI/I2C slave control status (DCS6) implementation
//
//
//

#ifndef _DCS6_PARAMS
#define _DCS6_PARAMS    1

#ifdef BIOS6_NONLEGACY
#include <xdc/std.h>
#else
#include <std.h>
#endif

/* Parameters declaration */
#define DCS6_PARAMS_DEV_SPI_CLEN_8     8
#define DCS6_PARAMS_DEV_SPI_CLEN_16    16
#define DCS6_PARAMS_DEV_SPI_NP_3       0
#define DCS6_PARAMS_DEV_SPI_NP_4_SCS   1
#define DCS6_PARAMS_DEV_SPI_NP_4_ENA   2
#define DCS6_PARAMS_DEV_SPI_NP_5       3
#define DCS6_PARAMS_DEV_SPI_NP_5_SRDY  4
#define DCS6_PARAMS_DEV_SPI_POL_LOW    0
#define DCS6_PARAMS_DEV_SPI_POL_HIGH   1
#define DCS6_PARAMS_DEV_SPI_PHA_IN     0
#define DCS6_PARAMS_DEV_SPI_PHA_OUT    1
#define DCS6_PARAMS_DEV_SPI_ENAHIZ_DIS 0 
#define DCS6_PARAMS_DEV_SPI_ENAHIZ_ENA 1 
#define DCS6_PARAMS_DEV_SPI_EMASK_OVRN 0x00000040 
#define DCS6_PARAMS_DEV_SPI_EMASK_BITERR 0x00000010 
#define DCS6_PARAMS_DEV_SPI_EMASK_DLENERR 0x00000001 
typedef struct DCS6_Params_Dev_Spi{
    Uint32 size;    /* size of the structure */
    Uint8 clen;     /* character length */
    Uint8 np;       /* number of pins */
    Uint8 pol;      /* clock polarity */
    Uint8 pha;      /* clock phase */
    Uint8 enahiz;   /* SPIENA pin mode */
    Uint8 res[3];   /* reserved */
    Uint32 emask;   /* error mask */
}DCS6_Params_Dev_Spi;

#define DCS6_PARAMS_DEV_I2C0_OA    0x28
#define DCS6_PARAMS_DEV_I2C1_OA    0x29
#define DCS6_PARAMS_DEV_I2C_EMASK_OVRN 0x00000800 
typedef struct DCS6_Params_Dev_I2c{
    Uint32 size;    /* size of the structure */
    Uint32 icoar;   /* slave address */
    Uint32 icpsc;   /* prescale */
    Uint32 icclkl;  /* clock low */
    Uint32 icclkh;  /* clock high */
    Uint32 emask;   /* error mask */
}DCS6_Params_Dev_I2c;

/* The UART divisor values are calculated using UART input clock of 150 MHz.
       For different UART input clock values these divisor values need to be
       recomputed. Refer the UART user guide for determing dow to compute the
       divisor values.
   The UART divisor values below represent the commonly used baud rates. For
       other baud rates the divisor values need to be computed. Refer the UART
       user guide for determing dow to compute the divisor values. */
#define DCS6_PARAMS_DEV_UART_DIV_16X_B2400     3906
#define DCS6_PARAMS_DEV_UART_DIV_16X_B4800     1953
#define DCS6_PARAMS_DEV_UART_DIV_16X_B9600     977
#define DCS6_PARAMS_DEV_UART_DIV_16X_B19200    488
#define DCS6_PARAMS_DEV_UART_DIV_16X_B38400    244 
#define DCS6_PARAMS_DEV_UART_DIV_16X_B57600    162
#define DCS6_PARAMS_DEV_UART_DIV_16X_B115200   81 
#define DCS6_PARAMS_DEV_UART_DIV_13X_B2400     4808 
#define DCS6_PARAMS_DEV_UART_DIV_13X_B4800     2404 
#define DCS6_PARAMS_DEV_UART_DIV_13X_B9600     1202 
#define DCS6_PARAMS_DEV_UART_DIV_13X_B19200    601 
#define DCS6_PARAMS_DEV_UART_DIV_13X_B38400    300 
#define DCS6_PARAMS_DEV_UART_DIV_13X_B57600    200 
#define DCS6_PARAMS_DEV_UART_DIV_13X_B115200   100 
#define DCS6_PARAMS_DEV_UART_MDR_OSMSEL_16X    0
#define DCS6_PARAMS_DEV_UART_MDR_OSMSEL_13X    1
#define DCS6_PARAMS_DEV_UART_FL_4              1
#define DCS6_PARAMS_DEV_UART_FL_8              2
#define DCS6_PARAMS_DEV_UART_FL_14             3
#define DCS6_PARAMS_DEV_UART_EMASK_OVRN        0x00000002
typedef struct DCS6_Params_Dev_Uart{
    Uint32 size;    /* size of the structure */
    Uint32 dll;     /* divisor lsb latch */
    Uint32 dlh;     /* divisor msb latch */
    Uint32 mdr;     /* mode definition */
    Uint32 fl;      /* fifo level */
    Uint16 xrxSize; /* ascii receive buffer size */
    Uint16 xrxHwm;  /* ascii receive buffer high watermark */
    Uint16 xrxLwm;  /* ascii receive buffer low watermark */
    Uint16 xtxSize; /* ascii transmit buffer size */
    Uint32 emask;   /* error mask */
}DCS6_Params_Dev_Uart;

typedef struct DCS6_Params_Dev{
    union{
        Uint32 size;
        DCS6_Params_Dev_Spi *pSpi;
        DCS6_Params_Dev_I2c *pI2c;
        DCS6_Params_Dev_Uart *pUart;
    }pDevData; 
}DCS6_Params_Dev;

#define DCS6_PARAMS_DEV_SPI0    0
#define DCS6_PARAMS_DEV_SPI1    1 
#define DCS6_PARAMS_DEV_I2C0    2
#define DCS6_PARAMS_DEV_I2C1    3
#define DCS6_PARAMS_DEV_UART0   4
#define DCS6_PARAMS_DEV_UART1   5
#define DCS6_PARAMS_DEV_UART2   6
#define DCS6_PARAMS_PSFIL_ENA   1 
#define DCS6_PARAMS_PSLC_DIS    0
#define DCS6_PARAMS_PSLC_ENA    1 
#define DCS6_PARAMS_PR_DIS      0
#define DCS6_PARAMS_PR_ENA      1 
#define DCS6_PARAMS_PW_DIS      0
#define DCS6_PARAMS_PW_ENA      1 
#define DCS6_PARAMS_EDR_DIS     0
#define DCS6_PARAMS_EDR_ENA     1 
typedef struct DCS6_Params{
    Uint32 size;    /* size of the structure */
    Uint8 dev;      /* device */
    Uint8 psfil;    /* process zero fill (dMAX,DSP) */
    Uint8 pslc;     /* process serial link control (dMAX,DSP) */
    Uint8 pr;       /* process type 2,3,4 read alpha commands (dMAX,DSP) */
    Uint8 pw;       /* process type 2,3,4 write alpha commands (dMAX,DSP) */
    Uint8 edr;      /* error detection and reporting */
    Uint8 res[2];   /* reserved */
    Uint16 rxSize;  /* receive buffer size */
    Uint16 rxHwm;   /* receive buffer high watermark */
    Uint16 rxLwm;   /* receive buffer low watermark */
    Uint16 txSize;  /* transmit buffer size */
    DCS6_Params_Dev *pDev;    /* device parameters */
    Void *logObj;
}DCS6_Params;
extern const DCS6_Params DCS6_PARAMS;

/* Function declaration */
extern void DCS6_errorInit();
extern void DCS6_errorAssert();
extern void DCS6_errorDeassert();
#endif
