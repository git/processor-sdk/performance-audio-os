PCM decoder implements all functions and defines all constant structures common to all PCM algorithm implementations.

Majorly it has three functionalities:
1.	InputAudio   This converts the input audio in the buffer and converts from fixed to float if needed. Channel mapping can be done, also scaling can be done.
2.	Rampart      This avoids the sudden amplitude changes in the audio by multiplying the ramp function. This is generally used for first frame. 
3.	Downmix      This is used to downmix the audio to various speaker configurations. For now, it supports all the sat channels'configurations starting from Mono to Surround4 speaker configuration.

Dependent libraries:
	asp_std.lib
	com_dec.lib
	com_asp.lib
	pcm1.lib

asp/std referenced api's used:
	PAF_ASP_channelMask   
	PAF_ASP_dB2ToLinear   
	PAF_ASP_delay         
	PAF_ASP_programFormat 
	PAF_ASP_sampleRateHz  
	PAF_ASP_stdCCMT

dec/com referenced api's:
	CPL_* (common PAF library)
	CDM_* (Common DownMix  api's)
	
asp/com referenced api's:
	COM_TII_activateCommon_   
	COM_TII_activateInternal  
	COM_TII_control           
	COM_TII_deactivate        
	COM_TII_deactivateCommon_ 
	COM_TII_free              
	COM_TII_snatchCommon_     

dec/pcm1 referenced api's:
	PCM_MDS_alloc           
	PCM_MDS_control         
	PCM_MDS_decode          
	PCM_MDS_downmix         
	PCM_MDS_info            
	PCM_MDS_initObj         
	PCM_MDS_input           
	PCM_MDS_inputAudioFloat 
	PCM_MDS_inputCheck      
	PCM_MDS_rampart         
	PCM_MDS_reset           
	PCM_decode              
	PCM_info              
	