
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// CPL_fifo test code 
//
//
//

#include <stdlib.h>
#include <stdio.h>

#include "cpl_fifo.h"

void getRand(void *p,int size);

int writeThroughPtr=1;
int readThroughPtr=1;

main()
{
  test1();
  test2();
  return 0;
}

test1()
{
  CPL_Fifo fifo;
  unsigned char *pTest;
  int w1,i,r1,s;
  unsigned char *p;
  unsigned char *pW;
  unsigned char *pR;
  int oupSize = 576*2*2;
  int inpSize = 256*2*2;
  int inCount=0;
  int ouCount=0;
  int inp,oup;
  
  p = malloc(10*1024);
  // size of the fifo
  //for(s=0;s<50000;s++)
  s = 4*1024;
  {
      fifo.pStore = malloc(s);
      fifo.size   = s;
      CPL_fifoInit(&fifo);

	  // test this long
	  pTest = malloc(10*1024*1024);
      for(i=0; i<10*1024*1024; i+=inpSize)
      {
		  w1 = 0;
		  inpSize = 999;
		  if( (i+inpSize) >= (10*1024*1024) )
				  break;
          getRand(&pTest[inCount],inpSize);
		  for(inp=0;inp<inpSize;inp+=w1)
		  {
			  if(writeThroughPtr)
			  {
	           w1 = CPL_fifoGetWritePtr(&fifo,inpSize-inp,&pW);
	           memcpy(pW,&pTest[inCount+inp],w1);
			   CPL_fifoWrote(&fifo,w1);
			  }
			  else
	              w1 = CPL_fifoWrite(&fifo,&pTest[inCount+inp],inpSize-inp);
			  
			  printf("wrote %d bytes\n",w1);
			  oupSize = 256;
		      for(oup=0;oup<oupSize;oup+=r1)
		      {
				  if(readThroughPtr)
				  {
		              r1 = CPL_fifoGetReadPtr(&fifo,oupSize-oup,&pR);
					  memcpy(p,pR,r1);
				      CPL_fifoReadDone(&fifo,r1);
				  }
				  else
		              r1 = CPL_fifoRead(&fifo,p,oupSize-oup);
			      printf("read %d bytes\n",r1);
	              if(r1)
	              {
		              if(memcmp(&pTest[ouCount+oup],p,r1))
				          printf("comparision failed\n"),exit(2);
	              }
				  ouCount += r1;
		      }
         }
         inCount += inpSize;
	  }
	  free(pTest);
	  free(fifo.pStore);
  }
  printf("test1 passed");
}

test2()
{
  CPL_Fifo fifo;
  int circ_size;
  int size;
  int r;
  int t;
  unsigned char *pCmp;
  unsigned char *pOut;
  unsigned char *pW;

  // size of the fifo
  circ_size = 4*1024;
  //for(circ_size=0;circ_size<50000;circ_size++)
  {
      pCmp = malloc(circ_size);
      pOut = malloc(circ_size);
      fifo.pStore = malloc(circ_size);
      fifo.size   = circ_size;
      CPL_fifoInit(&fifo);

      for(size=0;size<circ_size;)
      {
		  for(t=0;t<circ_size;)
		  {
		  for(r=0;r<circ_size;)
		  {
              CPL_fifoMoveReadLocation(&fifo,r);
			  if(fifo.r != r)
			  {
                 printf("read start index did not move to intended position\n");
                 printf("circ_size=%d size=%d readStartIndex=%d readTargetIndex=%d\n",circ_size,size,r,t);
				 exit(6);
			  }
		      if(CPL_fifoGetWritePtr(&fifo,size,&pW) != size)
			  {
			      printf("write size wrong\n");
				  exit(3);
			  }
			  getRand(pCmp,size);
			  memcpy(pW,pCmp,size);
			  CPL_fifoWrote(&fifo,size);
              CPL_fifoMoveReadLocation(&fifo,t);
   		      if(fifo.r != t)
   		      {
                 printf("read target index did not move to intended position\n");
   			     exit(6);
   		      }
              if( CPL_fifoRead(&fifo,pOut,size) != size)
   		      {
                 printf("read size is wrong\n");
                 printf("circ_size=%d size=%d readStartIndex=%d readTargetIndex=%d\n",circ_size,size,r,t);
   			     exit(4);
   		      }
   		      if(memcmp(pCmp,pOut,size))
   		      {
                 printf("comparision failed\n");
                 printf("circ_size=%d size=%d readStartIndex=%d readTargetIndex=%d\n",circ_size,size,r,t);
   			     exit(5);
			  }
		  r++;
		  if(r>11 && r <(circ_size-101)) r+=100;
		  }
		  t++;
		  if(t>11 && t <(circ_size-101)) t+=100;
		  }
          printf("passed for circ_size=%d size=%d\n",circ_size,size);
		  size++;
		  if(size>11 && size <(circ_size-101)) size+=100;
  }
	  free(pCmp);
	  free(pOut);
	  free(fifo.pStore);
  } 
  printf("test2 passed");
}

void getRand(void *p,int size)
{
  unsigned char *pC=p;
  int i;

  for(i=0;i<size;i++)
    pC[i] = rand(); 
}
