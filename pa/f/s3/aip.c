
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 -- Alpha Interval Processing
//
//
//

#include <stdio.h>  // printf if error

#include <std.h>
#include <alg.h>
#include <clk.h>
#include <logp.h>
#include <sys.h>

#include <sem.h>
#include <tsk.h>

#include <ialg.h>

#include "pafhjt.h"

#include <acp.h>
#include <acp_mds.h>

#include <paf_alg_print.h>
extern int IRAM;
extern int SDRAM;
#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFF000000) == 0xD8000000)
 extern int L3RAM;
#endif

int gEnableRamReport = 0;  // set this in main if RAM_REPORT is desired.
//
// Alpha Interval Processing Status initialization data
//

#include <pafaip.h>

const PAF_AlphaIntervalProcessingStatus aipStatusInit = 
{
    sizeof (PAF_AlphaIntervalProcessingStatus),
    3,
    0x99,
    100,
};

//
// Alpha code shortcut arrays
//

#include <acptype.h>
#include <acpbeta.h>
#include <pafstd_a.h>

#ifndef execSTDAtBoot
#define execSTDAtBoot 0xc102
#endif

#ifndef execALTAtBoot
#define execALTAtBoot ((execSTDAtBoot) + (ACP_SERIES_ALT-ACP_SERIES_STD << 12))
#endif

#ifndef execOEMAtBoot
#define execOEMAtBoot ((execSTDAtBoot) + (ACP_SERIES_OEM-ACP_SERIES_STD << 12))
#endif

#ifndef execCUSAtBoot
#define execCUSAtBoot ((execSTDAtBoot) + (ACP_SERIES_CUS-ACP_SERIES_STD << 12))
#endif

const ACP_Unit atboot_sA[][2] = 
{
    { 0xc901, execSTDAtBoot, },
    { 0xc901, execALTAtBoot, },
    { 0xc901, execOEMAtBoot, },
    { 0xc901, execCUSAtBoot, },
};

#ifndef execSTDAtTime
#define execSTDAtTime 0xc104
#endif

#ifndef execALTAtTime
#define execALTAtTime ((execSTDAtTime) + (ACP_SERIES_ALT-ACP_SERIES_STD << 12))
#endif

#ifndef execOEMAtTime
#define execOEMAtTime ((execSTDAtTime) + (ACP_SERIES_OEM-ACP_SERIES_STD << 12))
#endif

#ifndef execCUSAtTime
#define execCUSAtTime ((execSTDAtTime) + (ACP_SERIES_CUS-ACP_SERIES_STD << 12))
#endif

const ACP_Unit attime_sA[][2] = 
{
    { 0xc901, execSTDAtTime, },
    { 0xc901, execALTAtTime, },
    { 0xc901, execOEMAtTime, },
    { 0xc901, execCUSAtTime, },
};

//
// Alpha Interval Processing Task
//
//   Name:      alphaIntervalProcessingTask
//   Purpose:   BIOS Task Function for Alpha Interval Processing: process
//              alpha code shortcuts at-boot and at-intervals for all alpha
//              code series.
//   From:      BIOS
//   Uses:      See code.
//   States:    x
//   Return:    void.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * Initialization reporting.
//              * Sequence processing errors.
//              * Error number macros.
//              * Line number macros.
//

LINNO_DEFN (alphaIntervalProcessingTask); /* Line number macros */
ERRNO_DEFN (alphaIntervalProcessingTask); /* Error number macros */

void
alphaIntervalProcessingTask (Int betaPrimeValue)
{
    Int mode;

    ALG_Handle acpAlg;

    ACP_Handle acp;

    Int betaPrime;
    Int betaPrimeOffset;

    Int errno;

    PAF_AlphaIntervalProcessingStatus aipStatus = aipStatusInit;

    //
    // Initialize
    //

    LINNO_RPRT (alphaIntervalProcessingTask, -1);

    // Create an ACP algorithm instance with trace enabled

    ACP_MDS_init ();

    if (! (acpAlg = (ALG_Handle )ACP_MDS_create (NULL))) {
        LOG_printf (&trace, "AIP: ACP algorithm instance creation failed");
        LINNO_RPRT (alphaIntervalProcessingTask, __LINE__);
        return;
    }

    acpAlg->fxns->algControl (acpAlg, ACP_GETBETAPRIMEOFFSET, 
        (IALG_Status *) &betaPrimeOffset);
    betaPrime = betaPrimeOffset * betaPrimeValue;

    acp = (ACP_Handle )acpAlg;
    acp->fxns->attach(acp, ACP_SERIES_STD, STD_BETA_SYSINT+betaPrime, 
        (IALG_Status *)&aipStatus);

    LOG_printf(&trace, "AIP: ACP processing initialized");

    if (gEnableRamReport)
    {
	#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFF000000) == 0xD8000000)
    	PAF_ALG_memStatusPrint(IRAM,SDRAM,L3RAM);
	#else
    	PAF_ALG_memStatusPrint(IRAM,SDRAM,IRAM);
	#endif
    }
    // Set default stream

    if (betaPrimeValue) {
        ACP_Unit stream_s[2] = { 0xcd09, 0x0400, };

        stream_s[1] += betaPrimeValue;
        if (errno = acp->fxns->apply (acp, stream_s, NULL)) {
            LOG_printf(&trace, "AIP: at boot stream error 0x%04x sending 0x%x, 0x%x",
            		errno, stream_s[0], stream_s[1]);
            LINNO_RPRT (alphaIntervalProcessingTask, __LINE__);
            printf("AIP.c line %d: at boot stream error 0x%04x sending 0x%x, 0x%x",
            		__LINE__, errno, stream_s[0], stream_s[1]);
            return;
        }
    }

    // Process at boot interval if mode is non-zero

    if (mode = aipStatus.mode) {
        Int i;
        Int mask = aipStatus.mask;
        for (i=0; i < 4; i++) {
            if (mask & (0x01 << i)) {
            	LOG_printf(&trace, "AIP: at boot %d: %x, %x", i, atboot_sA[i][0], atboot_sA[i][1]);
                if (errno = acp->fxns->sequence (acp, atboot_sA[i], NULL)) {
                    LOG_printf(&trace, "AIP: at boot series %d error (0x%04x) <ignored>", i, errno);
                    ERRNO_RPRT (alphaIntervalProcessingTask, errno);
                    printf("AIP: line %d: at boot series %d error 0x%04x sending 0x%x, 0x%x\n",
                    		__LINE__, i, errno, atboot_sA[i][0], atboot_sA[i][1]);
                }
                else
                    LOG_printf(&trace, "AIP: at boot series %d process", i);
            }
        }
    }

    LINNO_RPRT (alphaIntervalProcessingTask, -2);

    //
    // Run until task is destroyed by the system 
    //

    while (mode = aipStatus.mode) {
        Uns time, interval, elapsed, now, then;

        then = TSK_time ();

        TSK_settime (TSK_self ());

        // Process at time interval while mode & time are non-zero

        while ((mode = aipStatus.mode) > 1 && (time = aipStatus.time) > 0) {

            interval = (CLK_countspms() * time) / CLK_getprd();

            now = TSK_time ();
            elapsed = now - then;
            then = now;

            if (interval > elapsed)
                TSK_sleep (interval - elapsed);

            if (mode > 2) {
                Int i;
                Int mask = aipStatus.mask;
                for (i=0; i < 4; i++) {
                    if (mask & (0x10 << i)) {
                        if (errno = acp->fxns->sequence (acp, attime_sA[i], NULL)) {
                            // LOG_printf(&trace, "AIP: at time series %d error (0x%04x) <ignored>", i, errno);
                            ERRNO_RPRT (alphaIntervalProcessingTask, errno);
                        }
                        else
                        {
                            // LOG_printf(&trace, "AIP: at time series %d process", i);
                        }
                    }
                }
            }

            TSK_deltatime (TSK_self ());
        }

        // Block until reprioritized when mode or time is zero

        LINNO_RPRT (alphaIntervalProcessingTask, __LINE__);
        TSK_setpri( TSK_self(), -1);
        TSK_yield();
    }
}

