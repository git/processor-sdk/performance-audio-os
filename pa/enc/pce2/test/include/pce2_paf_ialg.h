/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Performance Audio Algorithm Memory Attribute Type Definitions
//
//
//

#ifndef PAF_IALG_
#define PAF_IALG_

#include <ti/xdais/ialg.h>

//
// Definitions to augment ialg.h
//

#define PAF_IALG_NONE ((IALG_MemSpace )-1)

//
// Definitions to support common memory
//

#define PAF_IALG_COMMON  1                  /* Enable PAF_IALG_COMMON */

#ifndef PAF_IALG_COMMON

#define PAF_IALG_COMMONN(N) \
        (IALG_MemAttrs )((N) == 0 ? IALG_SCRATCH : IALG_PERSIST)

#define PAF_IALG_COMMON_MEM0 IALG_SCRATCH
#define PAF_IALG_COMMON_MEM1 IALG_SCRATCH
#define PAF_IALG_COMMON_MEMN IALG_SCRATCH

#else

// #define PAF_IALG_COMMON_N 0          /* ala ALG */
// #define PAF_IALG_COMMON_N 1          /* ala ALGRF */
#define PAF_IALG_COMMON_N 16            /* PAF_IALG */

#define PAF_IALG_COMMONN(N) \
        (IALG_MemAttrs )((N) >= PAF_IALG_COMMON_N \
        ? IALG_PERSIST : (N) == 0 ? IALG_SCRATCH : IALG_WRITEONCE + (N))

#define PAF_IALG_COMMON_STEREO_DECODE_PROCESSING  PAF_IALG_COMMONN(1)
#define PAF_IALG_COMMON_MULTI_DECODE_PROCESSING   PAF_IALG_COMMONN(2)
#define PAF_IALG_COMMON_FRONT_SURROUND_PROCESSING PAF_IALG_COMMONN(2)
#define PAF_IALG_COMMON_RESERVED0                 PAF_IALG_COMMONN(3)
#define PAF_IALG_COMMON_BACK_SURROUND_PROCESSING  PAF_IALG_COMMONN(4)
#define PAF_IALG_COMMON_ENCODE_PROCESSING         PAF_IALG_COMMONN(5)
#define PAF_IALG_COMMON_VIRTUAL_PROCESSING        PAF_IALG_COMMONN(6)
#define PAF_IALG_COMMON_BASS_MANAGEMENT           PAF_IALG_COMMONN(7)
#define PAF_IALG_COMMON_DELAY_MANAGEMENT          PAF_IALG_COMMONN(8)
#define PAF_IALG_COMMON_RESERVED1                 PAF_IALG_COMMONN(9)
#define PAF_IALG_COMMON_RESERVED2                 PAF_IALG_COMMONN(10)
#define PAF_IALG_COMMON_RESERVED3                 PAF_IALG_COMMONN(11)
#define PAF_IALG_COMMON_CUSTOM1                   PAF_IALG_COMMONN(12)
#define PAF_IALG_COMMON_CUSTOM2                   PAF_IALG_COMMONN(13)
#define PAF_IALG_COMMON_CUSTOM3                   PAF_IALG_COMMONN(14)
#define PAF_IALG_COMMON_CUSTOM4                   PAF_IALG_COMMONN(15)

#define PAF_IALG_COMMON_MEM0 IALG_SCRATCH
#define PAF_IALG_COMMON_MEM1 \
        (IALG_MemAttrs )(PAF_IALG_COMMON_N > 1 \
        ? PAF_IALG_COMMONN (1) : IALG_SCRATCH)
#define PAF_IALG_COMMON_MEMN \
        (IALG_MemAttrs )(PAF_IALG_COMMON_N > 1 \
        ? PAF_IALG_COMMONN(PAF_IALG_COMMON_N - 1) : IALG_SCRATCH)

#endif /* PAF_IALG_COMMON */

/*
 * The PAF_IALG_Config is derived from the "ALGRF_Config" as defined in Standard
 * TI ALGRF module version "ALGRF 0.02.06 11-21-01" in the file algrf.h.
 */

typedef struct PAF_IALG_Config {
    Int iHeap;
    Int eHeap;
    Int lHeap;
    Int clr;
} PAF_IALG_Config;

/*
 * The PAF_IALG_Common is created as a baseline for common memory sections.
 */

typedef struct PAF_IALG_Common {
    Int size;
    Int flag;
} PAF_IALG_Common;

#endif /* PAF_IALG_ */
