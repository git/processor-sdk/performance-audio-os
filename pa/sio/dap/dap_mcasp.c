
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// McASP implementations for DAP_PORT (e4 specific)
//
//
//

#include <stdio.h>
#include <std.h>
#include <sys.h>
#include <clk.h>
#include <hwi.h>
#include <sio.h>

#include "dap.h"
#include "dap_mcasp.h"
#include "dap_csl_mcasp.h"

// don't use hidden jump table when using configurable dMAX (for now)
#ifndef dMAX_CFG
#include "pafhjt.h"
#endif

// #define ENABLE_TRACE
#ifdef ENABLE_TRACE
  #include <logp.h>
  #define TRACE(a) LOG_printf a
#else
  #define TRACE(a)
#endif

// .............................................................................
// Resource Access Control
//
// The resources which must be controlled here are the McASP registers. There
// is layer context (dapMcaspDrv) but this essentially only holds the
// addresses of the resources. Further this is intitialized, i.e. written to
// only once, before the task scheduler begins and hence need not be controlled.
// Therefore we focus only on the McASP registers themselves.
// 
// There are two sources of switching which must be considered:
//        (A) Interrupts
//        (B) Tasks
//
// While DAP does not access McASP registers at ISR levels, other users may 
// access some of the registers for other functionality, e.g. GPIO, at any
// priority level. Therefore all accesses to these registers must be protected
// using HWI disable/enable blocks. Registers in this category are:
//        PDIR/PFUNC/PDOUT/PDIN
// 
// Within (B) there are two types of accesses which we must consider:
//        (i)  Modifications to non-direction specific registers not in (A)
//        (ii) Modifications to direction specific registers
// 
// Within this layer there are no accesses to registers of type (i). If there
// were then there are at least two choices for  appropriate access control.
//        . HWI disable/enable macros
//            This is effective but inefficient since it blocks all
//            processing regardless of its resource usage.
//            
//        . Raise the TSK priority > max priority of all tasks using DAP.
//            This assumes that no tasks using DAP run at TSK_MAXPRI.
//
// For (ii) there is no need to perform resource access control since these
// registers are *owned* by the device and hence the calling thread and not
// accessed by any other context. Registers in this category are:
//        AFSyCTL, ACLKyCTL, AHCLKyCTL, SRCTLz, yTDM, yGBLCTL, yINTCTL,
//        ySTAT, ySLOTCNT, yCLKCHK, yBUFz
// where y is X or R and z is between 1 and 15.
// 
// Of concern maybe the ASYNC bit in the ACLKXCTL since this is arguably a
// receive option in a transmit register. However, to date, this bit is only
// modified when the transmitter is configured not when the input is
// configured; which would be problematic.
// 
// Also we make no assumptions about the calling priority; e.g. we do not
// depend on the DAP layer to disable switching before making a call to this
// (DAP_MCASP) layer.
// .............................................................................

// max time to wait in DAP_MCASP_enableX functions (in mS)
#define REGISTER_SET_TIMEOUT 2.

// .............................................................................
// Function table

Int  DAP_MCASP_alloc    (DEV_Handle device);
Int  DAP_MCASP_close    (DEV_Handle device);
Int  DAP_MCASP_enable   (DEV_Handle device);
Void DAP_MCASP_init     (Void);
Int  DAP_MCASP_open     (DEV_Handle device);
Int  DAP_MCASP_reset    (DEV_Handle device);
Int  DAP_MCASP_watchDog (DEV_Handle device);

Int  DAP_MCASP_waitSet (MCASP_Handle hMcasp, Uint32 wrReg, Uint32 rdReg, Uint32 mask, Uint32 timeout);
//Int  DAP_MCASP_updateDITRam(DEV_Handle device );

DAP_MCASP_Fxns DAP_MCASP_FXNS = 
{
    DAP_MCASP_alloc,
    DAP_MCASP_close,
    DAP_MCASP_enable,
    DAP_MCASP_init,
    DAP_MCASP_open,
    DAP_MCASP_reset,
    DAP_MCASP_watchDog,
    DAP_MCASP_waitSet
};

DAP_MCASP_DriverObject dapMcaspDrv;

// -----------------------------------------------------------------------------

Void DAP_MCASP_init (Void)
{
    volatile Uint32 *base;
    volatile Uint32 *fifoBase;
    volatile Uint32 *pSers;
    int i, j;

    TRACE((&trace, "DAP_MCASP_init"));
    // open McASP ports via CSL. This CSL call, MCASP_open, does not require
    // CSL_init which is important since this function is called before main
    // and hence before any chance to call CSL_init.
    for (i=0; i < _MCASP_PORT_CNT; i++) {
        dapMcaspDrv.hPort[i] = MCASP_open (i, MCASP_OPEN);
        base     = (volatile Uint32 *) dapMcaspDrv.hPort[i]->baseAddr;
        fifoBase = base + _MCASP_FIFO_OFFSET;
        if (fifoBase[_MCASP_AFIFOREV_OFFSET] == 0x44311100) 
            dapMcaspDrv.fifoPresent[i] = 1;
        else
            dapMcaspDrv.fifoPresent[i] = 0;

        // zero out all serializers
        pSers = &base[_MCASP_SRCTL0_OFFSET];
        for (j=0; j<_MCASP_CHANNEL_CNT; j++)
        {
            Uint32 save = *pSers;
            *pSers = 0;
            // TRACE((&trace, "DAP_MCASP_init: pSers: 0x%x.  was 0x%x.  is 0x%x", pSers, save, *pSers));
            pSers++;
        }
        base[_MCASP_PDIR_OFFSET] = 0;  // all input by default
    }

    

} // DAP_MCASP_init

// -----------------------------------------------------------------------------
// TODO:
//     Add control for:
//        1. DLB
//        2. DIT
//        3. AMUTE?

Int DAP_MCASP_alloc (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *)(device->object);
    const DAP_Params *pParams = pDevExt->pParams;
    MCASP_Handle hPort = dapMcaspDrv.hPort[pParams->sio.moduleNum];
    volatile Uint32 *base = (volatile Uint32 *) hPort->baseAddr;
    volatile Uint32 *pSers =&base[_MCASP_SRCTL0_OFFSET];
    Int i, oldMask, tdmMask;

    TRACE((&trace, "DAP_MCASP_alloc(0x%x)", device));

    // validate mcasp number
    if (pParams->sio.moduleNum >= _MCASP_PORT_CNT)
        return SYS_EINVAL;

    // Could/should validate pin mask here.
    // Check: McAsp0 has 16 pins.  McAsp1 has 12.  McAsp2 has 4.
    //  This code is DA8xx specific
    switch (pParams->sio.moduleNum)
    {
    case 0:
        if (pParams->dap.pinMask & 0x00FF0000)
        {
            TRACE((&trace, "ERROR. McAsp0 has only 16 pins.  PinMask requests 0x%x.\n", pParams->dap.pinMask));
            printf("ERROR. McAsp0 has only 16 pins.  PinMask requests 0x%x.\n", pParams->dap.pinMask);
            SYS_abort("ABORT. McAsp0 has only 16 pins.");
        }
        break;
    case 1:
        if (pParams->dap.pinMask & 0x00FFF000)
        {
            TRACE((&trace, "ERROR. McAsp1 has only 12 pins.  PinMask requests 0x%x.\n", pParams->dap.pinMask));
            printf("ERROR. McAsp1 has only 12 pins.  PinMask requests 0x%x.\n", pParams->dap.pinMask);
            SYS_abort("ABORT. McAsp1 has only 12 pins.");
        }
        break;
    case 2:
        if (pParams->dap.pinMask & 0xFFFFF0)
        {
            TRACE((&trace, "ERROR. McAsp2 has only 4 pins.  PinMask requests 0x%x.\n", pParams->dap.pinMask));
            printf("ERROR. McAsp2 has only 4 pins.  PinMask requests 0x%x.\n", pParams->dap.pinMask);
            SYS_abort("ABORT. McAsp2 has only 4 pins.");
        }
        break;
    }

    // lock context -- see class (A) above
    oldMask = HWI_disable ();

    // use pinMask to configure MCASP vs. GPIO functionality
    base[_MCASP_PFUNC_OFFSET] &= ~(pParams->dap.pinMask & 0xFE00FFFF);

    // infer serializer config from pinMask and mode
    TRACE((&trace, "DAP_MCASP_alloc(): serializer: pinMask : 0x%x PDIR on entry: 0x%x.", pParams->dap.pinMask, base[_MCASP_PDIR_OFFSET]));
    pDevExt->numSers = 0;
    for (i=0; i < _MCASP_CHANNEL_CNT; i++) {
        if (pParams->dap.pinMask & (1 << i)) {
            pDevExt->numSers += 1;
            if (device->mode == DEV_INPUT) {
                *pSers = MCASP_SRCTL_SRMOD_RCV;
                if (*pSers != MCASP_SRCTL_SRMOD_RCV) {
                    TRACE((&trace, "DAP_MCASP_alloc(): serializer input: write to pSers failed: 0x%x.  *pSers: 0x%x", pSers, *pSers));
                }
                else {
                    TRACE((&trace, "DAP_MCASP_alloc(): serializer input: pSers: 0x%x.  *pSers: 0x%x", pSers, *pSers));
                }
                base[_MCASP_PDIR_OFFSET] &= ~(1 << i);
                TRACE((&trace, "DAP_MCASP_alloc(): serializer input: clear pin %d.", i));
            }
            else {
                *pSers = MCASP_SRCTL_SRMOD_XMT;
                if (*pSers != MCASP_SRCTL_SRMOD_XMT) {
                    TRACE((&trace, "DAP_MCASP_alloc(): serializer output: write to pSers failed: 0x%x.  *pSers: 0x%x", pSers, *pSers));
                }
                else {
                    TRACE((&trace, "DAP_MCASP_alloc(): serializer output: pSers: 0x%x.  *pSers: 0x%x", pSers, *pSers));
                }
                base[_MCASP_PDIR_OFFSET] |= (1 << i);
                TRACE((&trace, "DAP_MCASP_alloc(): serializer output: set pin %d.", i));
            }
        }
        *pSers++;
    }
    TRACE((&trace, "DAP_MCASP_alloc(): serializer PDIR: 0x%x, numSers %d, *pSers: 0x%x", base[_MCASP_PDIR_OFFSET], pDevExt->numSers, *pSers));


    if (device->mode == DEV_INPUT) {
        MCASP_ConfigRcv *pRxConfig = (MCASP_ConfigRcv *)pParams->sio.pConfig;

        // infer clock pin directions
        if (pParams->dap.pinMask & _MCASP_PFUNC_ACLKR_MASK)
            if (pRxConfig->aclkrctl & _MCASP_ACLKRCTL_CLKRM_MASK)
                base[_MCASP_PDIR_OFFSET] |= _MCASP_PDIR_ACLKR_MASK;
            else
                base[_MCASP_PDIR_OFFSET] &= ~_MCASP_PDIR_ACLKR_MASK;
        if (pParams->dap.pinMask & _MCASP_PFUNC_AHCLKR_MASK)
            if (pRxConfig->ahclkrctl & _MCASP_AHCLKRCTL_HCLKRM_MASK)
                base[_MCASP_PDIR_OFFSET] |= _MCASP_PDIR_AHCLKR_MASK;
            else
                base[_MCASP_PDIR_OFFSET] &= ~_MCASP_PDIR_AHCLKR_MASK;
        if (pParams->dap.pinMask & _MCASP_PFUNC_AFSR_MASK)
            if (pRxConfig->afsrctl & _MCASP_AFSRCTL_FSRM_MASK)
                base[_MCASP_PDIR_OFFSET] |= _MCASP_PDIR_AFSR_MASK;
            else
                base[_MCASP_PDIR_OFFSET] &= ~_MCASP_PDIR_AFSR_MASK;

        TRACE((&trace, "DAP_MCASP_alloc(): rx clock: 0x%x", base[_MCASP_PDIR_OFFSET]));
        MCASP_configRcv (hPort, pRxConfig);
    }
    else {
        MCASP_ConfigXmt *pTxConfig = (MCASP_ConfigXmt *)pParams->sio.pConfig;

        // infer clock pin directions
        if (pParams->dap.pinMask & _MCASP_PFUNC_ACLKX_MASK)
            if (pTxConfig->aclkxctl & _MCASP_ACLKXCTL_CLKXM_MASK)
                base[_MCASP_PDIR_OFFSET] |= _MCASP_PDIR_ACLKX_MASK;
            else
                base[_MCASP_PDIR_OFFSET] &= ~_MCASP_PDIR_ACLKX_MASK;
        if (pParams->dap.pinMask & _MCASP_PFUNC_AHCLKX_MASK)
            if( pTxConfig->ahclkxctl & _MCASP_AHCLKXCTL_HCLKXM_MASK )
                base[_MCASP_PDIR_OFFSET] |= _MCASP_PDIR_AHCLKX_MASK;
            else
                base[_MCASP_PDIR_OFFSET] &= ~_MCASP_PDIR_AHCLKX_MASK;
        if (pParams->dap.pinMask & _MCASP_PFUNC_AFSX_MASK)
            if( pTxConfig->afsxctl & _MCASP_AFSXCTL_FSXM_MASK )
                base[_MCASP_PDIR_OFFSET] |= _MCASP_PDIR_AFSX_MASK;
            else
                base[_MCASP_PDIR_OFFSET] &= ~_MCASP_PDIR_AFSX_MASK;

        // HACK -- determine DIT need by FXWID
        if (((pTxConfig->afsxctl & _MCASP_AFSXCTL_FXWID_MASK)>> _MCASP_AFSXCTL_FXWID_SHIFT) == MCASP_AFSXCTL_FXWID_BIT)
            base[_MCASP_DITCTL_OFFSET] = MCASP_DITCTL_RMK (MCASP_DITCTL_VB_ZERO, MCASP_DITCTL_VA_ZERO, MCASP_DITCTL_DITEN_DIT);
        else
            base[_MCASP_DITCTL_OFFSET] = MCASP_DITCTL_DEFAULT;

        TRACE((&trace, "DAP_MCASP_alloc(): tx clock: 0x%x", base[_MCASP_PDIR_OFFSET]));
        MCASP_configXmt (hPort, pTxConfig);
    }
    DAP_PORT_FTABLE_reset (device);

    // unlock context
    HWI_restore (oldMask);

    // determine number of TDM slots (32 max), if DIT must be 2, must be
    // done after MCASP_config since the registers are referenced directly
    if (device->mode == DEV_OUTPUT) {
        if ((base[_MCASP_AFSXCTL_OFFSET] & _MCASP_AFSXCTL_XMOD_MASK) ==
            (MCASP_AFSXCTL_XMOD_OF(0x180) << _MCASP_AFSXCTL_XMOD_SHIFT) )
            tdmMask = 0x3;
        else
            tdmMask = base[_MCASP_XTDM_OFFSET];
    }
    else
        tdmMask = base[_MCASP_RTDM_OFFSET];
    pDevExt->numSlots = 0;
    for (i=0; i < 32; i++)
        if (tdmMask & (1 << i))
            pDevExt->numSlots +=1;

    TRACE((&trace, "Leaving DAP_MCASP_alloc: tdmMask: 0x%x.  numSlots: %d", tdmMask, pDevExt->numSlots));
    return SYS_OK;
} // DAP_MCASP_alloc

// -----------------------------------------------------------------------------

Int DAP_MCASP_close (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *)(device->object);
    const DAP_Params *pParams = pDevExt->pParams;
    MCASP_Handle hPort = dapMcaspDrv.hPort[pParams->sio.moduleNum];
    volatile Uint32 *base = (volatile Uint32 *) hPort->baseAddr;
    volatile Uint32 *pSers =&base[_MCASP_SRCTL0_OFFSET];
    Int i;

    TRACE((&trace, "DAP_MCASP_close(0x%x)", device));

    for (i=0; i < _MCASP_CHANNEL_CNT; i++) {
        if (pParams->dap.pinMask & (1 << i))
            *pSers = (MCASP_SRCTL_SRMOD_INACTIVE | ( MCASP_SRCTL_DISMOD_LOW << _MCASP_SRCTL_DISMOD_SHIFT ));
        *pSers++;
    }

    return SYS_OK;
} //DAP_MCASP_close

// -----------------------------------------------------------------------------
// Enable appropriate section of McASP. See McASP data sheet (Operation) for
// more info on the steps. 

Int DAP_MCASP_enable (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *)device->object;
    const DAP_Params *pParams = pDevExt->pParams;
    Int mcaspNum = pParams->sio.moduleNum;
    MCASP_Handle hPort = dapMcaspDrv.hPort[mcaspNum];
    volatile Uint32 *base = (volatile Uint32 *)(hPort->baseAddr);
    volatile Uint32 *fifoBase = base + _MCASP_FIFO_OFFSET;
    int i, mask0, mask1, mask2, mask3, mask4;
    Uint32 wrReg, rdReg;
    Int result = 0;

    TRACE((&trace, "DAP_MCASP_enable(0x%x)", device));

    // STEP 1: Reset McASP to default values
    // This was done in preceding function call. 

    // STEP 2: Enable FIFO
    // If present, for input and output. Set NUMEVT = NUMDMA to disable pacing
    // so that the same DMA setup can be used with FIFO enabled or disbaled
    if (dapMcaspDrv.fifoPresent[mcaspNum]) {
        if (device->mode == DEV_OUTPUT) {
            fifoBase[_MCASP_WFIFOCTL_OFFSET] =
                (pDevExt->numSers << _MCASP_WFIFOCTL_WNUMEVT_SHIFT) |
                (pDevExt->numSers << _MCASP_WFIFOCTL_WNUMDMA_SHIFT); 
            fifoBase[_MCASP_WFIFOCTL_OFFSET] |=
                (MCASP_WFIFOCTL_WENA_ENABLE << _MCASP_WFIFOCTL_WENA_SHIFT);

            TRACE((&trace, "DAP_MCASP_enable(): output fifos: numSers %d", pDevExt->numSers));
        }
        else {
            fifoBase[_MCASP_RFIFOCTL_OFFSET] =
                (pDevExt->numSers << _MCASP_RFIFOCTL_RNUMEVT_SHIFT) |
                (pDevExt->numSers << _MCASP_RFIFOCTL_RNUMDMA_SHIFT); 
            fifoBase[_MCASP_RFIFOCTL_OFFSET] |=
                (MCASP_RFIFOCTL_RENA_ENABLE << _MCASP_RFIFOCTL_RENA_SHIFT);
            TRACE((&trace, "DAP_MCASP_enable(): input fifos: numSers %d", pDevExt->numSers));
        }
    }

    mask1 = 0;
    rdReg = _MCASP_GBLCTL_OFFSET;
    if (device->mode == DEV_INPUT) {
        wrReg = _MCASP_RGBLCTL_OFFSET;
        mask0 = MCASP_FMKS (GBLCTL,RHCLKRST,ACTIVE);
        if (base[_MCASP_ACLKRCTL_OFFSET] & _MCASP_ACLKRCTL_CLKRM_MASK)
            mask1 = MCASP_FMKS (GBLCTL,RCLKRST,ACTIVE);
        mask2 = MCASP_FMKS (GBLCTL,RSRCLR,ACTIVE);
        mask3 = MCASP_FMKS (GBLCTL,RSMRST,ACTIVE);
        mask4 = MCASP_FMKS (GBLCTL,RFRST,ACTIVE);
    }
    else {
        wrReg = _MCASP_XGBLCTL_OFFSET;
        mask0 = MCASP_FMKS (GBLCTL,XHCLKRST,ACTIVE);
        if (base[_MCASP_ACLKXCTL_OFFSET] & _MCASP_ACLKXCTL_CLKXM_MASK)
            mask1 = MCASP_FMKS (GBLCTL,XCLKRST,ACTIVE);
        mask2 = MCASP_FMKS (GBLCTL,XSRCLR,ACTIVE);
        mask3 = MCASP_FMKS (GBLCTL,XSMRST,ACTIVE);
        mask4 = MCASP_FMKS (GBLCTL,XFRST,ACTIVE);
    }

    // STEP 4: Start high-frequency clocks
    // According to McASP datasheet this step is necessary even if the high-
    // frequency clocks are external.
    if (mask0) {
        TRACE((&trace, "DAP_MCASP_enable(): HF clock mask 0: 0x%x.  ", mask0));
        result = DAP_MCASP_FTABLE_waitSet (hPort, wrReg, rdReg, mask0, REGISTER_SET_TIMEOUT);
        if (result)
            return result;
    }

    // STEP 5: Enable bit clock generator if internally generated
    if (mask1) {
    	TRACE((&trace, "DAP_MCASP_enable(): bit clock mask 1: 0x%x.  ", mask1));
        result = DAP_MCASP_FTABLE_waitSet (hPort, wrReg, rdReg, mask1, REGISTER_SET_TIMEOUT);
        if (result)
            return result;
    }

    // STEP 6: Setup DMA (this was done earlier -- out of order OK)

    // STEP 7: Activate serializers
    TRACE((&trace, "DAP_MCASP_enable(): serializers mask 2: 0x%x.  ", mask2));
    result = DAP_MCASP_FTABLE_waitSet (hPort, wrReg, rdReg, mask2, REGISTER_SET_TIMEOUT);
    if (result)
        return result;

    // STEP 8: Verify that all transmit buffers are serviced.
    // This is guaranteed to succeed since by now we now that there is a clock
    // and hence XDATA empty will be generated
    if (device->mode == DEV_OUTPUT)
    {
        volatile Uint32 *pSers;
        volatile int timeout;

        result = 0;
        // configure serializers and associated pins
        pSers =&base[_MCASP_SRCTL0_OFFSET];
        for (i=0; i < _MCASP_CHANNEL_CNT; i++)
        {
        	timeout = 0;
            if (pDevExt->pParams->dap.pinMask & (1 << i))
            {
            	TRACE((&trace, "DAP_MCASP_enable(): wait for serializer %d.  pSers: 0x%x, *pSers: 0x%x", i, pSers, *pSers));
                while ((*pSers & _MCASP_SRCTL_XRDY_MASK) != 0)
                {
                	timeout++;
                	if (timeout > 10000)
                	{
                		TRACE((&trace, "DAP_MCASP_enable(): McASP serializer %d timed out.\n", i));
                		result = SYS_ETIMEOUT;
                		break;
                	}

                }
            }
            *pSers++;
        }
        if (result)
        {
          #ifdef ENABLE_TRACE
        	TRACE((&trace, "DAP_MCASP_enable(): Stopping trace on serializer error."));
        	LOG_disable(&trace);
          #endif
            return result;
        }
    }

    // STEP 9: Release state machine from reset
    TRACE((&trace, "DAP_MCASP_enable(): reset state machine, mask 3: 0x%x.", mask3));
    result = DAP_MCASP_FTABLE_waitSet (hPort, wrReg, rdReg, mask3, REGISTER_SET_TIMEOUT);
    if (result)
        return result;

    // STEP 10: Release FS generators from reset
    // This needs to be enabled even if external frame sync because counter
    //   for frame sync errors is in frame sync generator.
    TRACE((&trace, "DAP_MCASP_enable(): release FS generators, mask 4: 0x%x.", mask4));
    result = DAP_MCASP_FTABLE_waitSet (hPort, wrReg, rdReg, mask4, REGISTER_SET_TIMEOUT);

    TRACE((&trace, "DAP_MCASP_enable(): DAP_MCASP_enable(0x%x) complete.", device));
    return result;
} //DAP_MCASP_enable

// -----------------------------------------------------------------------------

Int DAP_MCASP_open (DEV_Handle device)
{
    return SYS_OK;
} // DAP_MCASP_open

// -----------------------------------------------------------------------------

Int DAP_MCASP_reset (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    const DAP_Params *pParams = pDevExt->pParams;
    Int mcaspNum = pParams->sio.moduleNum;
    MCASP_Handle hPort = dapMcaspDrv.hPort[mcaspNum];
    int oldhclk, oldclk, newhclk, newclk;
    volatile int * base = (volatile int *) hPort->baseAddr;
    volatile Uint32 *fifoBase = (volatile Uint32 *) (base + _MCASP_FIFO_OFFSET);


    if (device->mode == DEV_INPUT) 
    {
        TRACE((&trace, "DAP_MCASP_reset(0x%x) input device.", device));
        // disable FIFO if present
        if (dapMcaspDrv.fifoPresent[mcaspNum])
            fifoBase[_MCASP_RFIFOCTL_OFFSET] &=
                ~(MCASP_RFIFOCTL_RENA_ENABLE << _MCASP_RFIFOCTL_RENA_SHIFT);

        // set to internal clock source (see spru041d.pdf page 117)
        oldhclk = base[_MCASP_AHCLKRCTL_OFFSET];
        oldclk  = base[_MCASP_ACLKRCTL_OFFSET];
        newhclk = (oldhclk & ~_MCASP_AHCLKRCTL_HCLKRM_MASK) | (MCASP_AHCLKRCTL_HCLKRM_INTERNAL << _MCASP_AHCLKRCTL_HCLKRM_SHIFT);
        newclk  = (oldclk  & ~_MCASP_ACLKRCTL_CLKRM_MASK)   | (MCASP_ACLKRCTL_CLKRM_INTERNAL   << _MCASP_ACLKRCTL_CLKRM_SHIFT);
        base[_MCASP_AHCLKRCTL_OFFSET] = newhclk;
        base[_MCASP_ACLKRCTL_OFFSET]  = newclk;

        // assert RHCLKRST,RCLKRST to force signals to pass through with divide by 1
        base[_MCASP_RGBLCTL_OFFSET] &= ~ _MCASP_GBLCTL_RHCLKRST_MASK;
        base[_MCASP_RGBLCTL_OFFSET] &= ~ _MCASP_GBLCTL_RCLKRST_MASK;
        while (base[_MCASP_GBLCTL_OFFSET] & _MCASP_GBLCTL_RHCLKRST_MASK);
        while (base[_MCASP_GBLCTL_OFFSET] & _MCASP_GBLCTL_RCLKRST_MASK);

        // Reset all other transmitter functions 
        base[_MCASP_RGBLCTL_OFFSET] = 0;
        while (base[_MCASP_GBLCTL_OFFSET] & _MCASP_GBLCTL_RSMRST_MASK);
        while (base[_MCASP_GBLCTL_OFFSET] & _MCASP_GBLCTL_RFRST_MASK);
        while (base[_MCASP_GBLCTL_OFFSET] & _MCASP_GBLCTL_RSRCLR_MASK);

        // clear status register by writing ones
        //      don't write to the RTDMSLOT register since it is read only and
        //      causes a simulator warning when written
        base[_MCASP_RSTAT_OFFSET] = 0x1F7;
        while (base[_MCASP_RSTAT_OFFSET] & _MCASP_RSTAT_RDATA_MASK);

        // restore clock settings
        base[_MCASP_AHCLKRCTL_OFFSET] = oldhclk;
        base[_MCASP_ACLKRCTL_OFFSET]  = oldclk;
    }
    else 
    {
        TRACE((&trace, "DAP_MCASP_reset(0x%x) output device.", device));
        // disable FIFO if present
        if (dapMcaspDrv.fifoPresent[mcaspNum])
            fifoBase[_MCASP_WFIFOCTL_OFFSET] &=
                ~(MCASP_WFIFOCTL_WENA_ENABLE << _MCASP_WFIFOCTL_WENA_SHIFT);

        {
                volatile Uint32 *pSers = (volatile Uint32 *)(&base[_MCASP_SRCTL0_OFFSET]);
                Int i;

                for (i=0; i < _MCASP_CHANNEL_CNT; i++) {
                    if (pParams->dap.pinMask & (1 << i))
        	            *pSers = (MCASP_SRCTL_SRMOD_INACTIVE | ( MCASP_SRCTL_DISMOD_LOW << _MCASP_SRCTL_DISMOD_SHIFT ));
                    *pSers++;
                }
        }
        // set to internal clock source (see spru041d.pdf page 117)
        oldhclk = base[_MCASP_AHCLKXCTL_OFFSET];
        oldclk  = base[_MCASP_ACLKXCTL_OFFSET];
        newhclk = (oldhclk & ~_MCASP_AHCLKXCTL_HCLKXM_MASK) | (MCASP_AHCLKXCTL_HCLKXM_INTERNAL << _MCASP_AHCLKXCTL_HCLKXM_SHIFT);
        newclk  = (oldclk  & ~_MCASP_ACLKXCTL_CLKXM_MASK)   | (MCASP_ACLKXCTL_CLKXM_INTERNAL   << _MCASP_ACLKXCTL_CLKXM_SHIFT);
        base[_MCASP_AHCLKXCTL_OFFSET] = newhclk;
        base[_MCASP_ACLKXCTL_OFFSET]  = newclk;

        // assert XHCLKRST,XCLKRST to force signals to pass through with divide by 1
        base[_MCASP_XGBLCTL_OFFSET] &= ~ _MCASP_GBLCTL_XHCLKRST_MASK;
        base[_MCASP_XGBLCTL_OFFSET] &= ~ _MCASP_GBLCTL_XCLKRST_MASK;
        while (base[_MCASP_GBLCTL_OFFSET] & _MCASP_GBLCTL_XHCLKRST_MASK);
        while (base[_MCASP_GBLCTL_OFFSET] & _MCASP_GBLCTL_XCLKRST_MASK);

        // Reset all other transmitter functions 
        base[_MCASP_XGBLCTL_OFFSET] = 0;
        while (base[_MCASP_GBLCTL_OFFSET] & _MCASP_GBLCTL_XSMRST_MASK);
        while (base[_MCASP_GBLCTL_OFFSET] & _MCASP_GBLCTL_XFRST_MASK);
        while (base[_MCASP_GBLCTL_OFFSET] & _MCASP_GBLCTL_XSRCLR_MASK);

        // clear status register by writing ones
        //      don't write to the RTDMSLOT register since it is read only and
        //      causes a simulator warning when written
        base[_MCASP_XSTAT_OFFSET] = 0x1F7;
        while (base[_MCASP_XSTAT_OFFSET] & _MCASP_XSTAT_XDATA_MASK);

        {
            volatile Uint32 *pSers = (volatile Uint32 *)(&base[_MCASP_SRCTL0_OFFSET]);
            Int i;

            for (i=0; i < _MCASP_CHANNEL_CNT; i++) {
                if (pParams->dap.pinMask & (1 << i))
                    *pSers = MCASP_SRCTL_SRMOD_XMT;
                *pSers++;
            }
        }
        // restore clock settings
        base[_MCASP_AHCLKXCTL_OFFSET] = oldhclk;
        base[_MCASP_ACLKXCTL_OFFSET]  = oldclk;
    }

    TRACE((&trace, "DAP_MCASP_reset(0x%x) complete.", device));

    return SYS_OK;
} // DAP_MCASP_reset

// -----------------------------------------------------------------------------
// Set and wait for settings to registers (e.g. GBLCTL) to take effect
// we nee such a timeout since if the clocks are not available then 
// the settings will never take affect which would lead to a lockup condition.

#define INT_MAX 0xFFFFFFFF // maximum value returned by gethtime

Int DAP_MCASP_waitSet (MCASP_Handle hMcasp, Uint32 wrReg, Uint32 rdReg, Uint32 mask, Uint32 timeout)
{
    volatile Uint32 *base = (volatile Uint32 *)(hMcasp->baseAddr);
    Uint32 timeStart, timeNow, elapsed, resMask;

    TRACE((&trace, "DAP_MCASP_waitSet(0x%x, 0x%x, 0x%x).", hMcasp, wrReg, rdReg));

    // configure reserved mask to eliminate debug messages from simulator when
    // writing to read only bits. Set default to all ones so that this function
    // can be, although is not at this time, used by registers other than
    // R/XGBCTL.
    resMask = 0xFFFFFFFF;
    if (wrReg == _MCASP_RGBLCTL_OFFSET) 
        resMask = 0x1F;
    else if (wrReg == _MCASP_XGBLCTL_OFFSET) 
        resMask = 0x1F00;

    // set register with mask value
    base[wrReg] = (base[rdReg] | mask) & resMask;

    // convert timeout from milliseconds to nTicks
    timeout = CLK_countspms () * timeout;

    timeStart = CLK_gethtime ();
    while (1) {
        // return success if register has latched value
        if ((base[rdReg] & mask) == mask)
            break;
        timeNow = CLK_gethtime ();
        elapsed = timeNow - timeStart;
        // check for wrap aound
        if (timeNow <= timeStart)
            elapsed = INT_MAX - timeStart + timeNow;
        
        // return error if timeout reached
        if (elapsed > timeout)
            return SYS_ETIMEOUT;
    }

    return SYS_OK;
} //DAP_MCASP_waitSet

// -----------------------------------------------------------------------------

Int DAP_MCASP_watchDog (DEV_Handle device)
{
    DAP_DeviceExtension *pDevExt = (DAP_DeviceExtension *) device->object;
    MCASP_Handle hPort = dapMcaspDrv.hPort[pDevExt->pParams->sio.moduleNum];
    int stat;

    if (device->mode == DEV_INPUT) {
        stat = MCASP_RGETH (hPort, RSTAT);
        if ((stat & _MCASP_RSTAT_ROVRN_MASK) ||
            (stat & _MCASP_RSTAT_RDMAERR_MASK))
            return SYS_EBADIO;
    }
    else {
        stat = MCASP_RGETH (hPort, XSTAT);
        if ((stat & _MCASP_XSTAT_XUNDRN_MASK) ||
            (stat & _MCASP_XSTAT_XDMAERR_MASK))
        return SYS_EBADIO;
    }
    
    return SYS_OK;
} // DAP_MCASP_watchDog

// -----------------------------------------------------------------------------
