
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*


	.global _biquad2chMP 

_biquad2chMP	;		spa		spb		cpa		cpb		ipa		ipb		opa		opb		cnt		chb
				;		a4		b4		a6		b6		a8		b8		a10		b10		a12		b12

	.asg	a4,spa
	.asg	b4,spb
	.asg	a6,cpa
	.asg	b6,cpb
	.asg	a8,ipa
	.asg	b8,ipb
	.asg	a10,opa
	.asg	b10,opb
	.asg	b1,cnt
	.asg	b0,chb

	.asg	b2,b0a
	.asg	a5,b1a
	.asg	a7,b2a
	.asg	a9,b0b
	.asg	a11,b1b
	.asg	a13,b2b

	.asg	a2,nulll
	.asg	a3,nullh
	.asg	a14,x0a
	.asg	a15,x1a
	.asg	a16,x2a
	.asg	a17,x3a
	.asg	a18,x0b
	.asg	a19,x1b
	.asg	a20,x2b
	.asg	a21,x3b

	.asg	nullh:nulll,null
	.asg	a1:a0,pa2
	.asg	a23:a22,pa
	.asg	a25:a24,X0a
	.asg	a27:a26,X1a
	.asg	a29:a28,X0b
	.asg	a31:a30,X1b

	.asg	b12,sx3a
	.asg	b13,sx2a
	.asg	b14,sx3b
	.asg	b16,sx2b

	.asg	b17,a2a
	.asg	b18,a1a
	.asg	b19,a2b
	.asg	b20,a1b
	.asg	b21,out

	.asg	b23:b22,pb
	.asg	b25:b24,y1a
	.asg	b27:b26,y0a
	.asg	b29:b28,y1b
	.asg	b31:b30,y0b

	.asg	b24,y1al
	.asg	b25,y1ah
	.asg	b26,y0al
	.asg	b27,y0ah
	.asg	b28,y1bl
	.asg	b29,y1bh
	.asg	b30,y0bl
	.asg	b31,y0bh

		mvc			csr,b0
		and			1,b0,b1
		clr			b0,0,1,b0
 [b1]	or			2,b0,b0
		mvc			b0,csr

		; move parameter values and save compiler registers
		mv			b12,chb
||		mv			a12,cnt
||		stw			a10,*b15--[1]

		stw			b10,*b15--[1]
		stw			a11,*b15--[1]
		stw			b11,*b15--[1]
		stw			a12,*b15--[1]
		stw			b12,*b15--[1]
		stw			a13,*b15--[1]
		stw			b13,*b15--[1]
		stw			a14,*b15--[1]
		stw			b14,*b15--[1]
		stw			a15,*b15--[1]
		stw			b3,*b15--[1]

		; load coefficients and state updates
		mv			cnt,a0
		addaw		ipa,a0,ipa
		addaw		ipb,cnt,ipb
		ldw			*ipa[-2],sx3a
		ldw			*ipa[-1],sx2a
		ldw			*ipb[-2],sx3b
		ldw			*ipb[-1],sx2b
		subaw		ipa,a0,ipa
		subaw		ipb,cnt,ipb
				
		ldw			*cpa[0],b2a
		ldw			*cpa[1],b1a
		ldw			*cpa[2],b0a
		ldw			*cpa[3],a2a
		ldw			*cpa[4],a1a
		ldw			*cpb[0],b2b
		ldw			*cpb[1],b1b
		ldw			*cpb[2],b0b
		ldw			*cpb[3],a2b
		ldw			*cpb[4],a1b

PROLOG
		; load states/inputs

		ldw			*spa[0],x3a
		lddw		*spa[1],y1a
		ldw			*spb[0],x3b
		lddw		*spb[1],y1b
		ldw			*spa[1],x2a
		mpysp2dp	b2a,x3a,X0a
		ldw			*spb[1],x2b
		mpysp2dp	b2b,x3b,X0b
		lddw		*spa[2],y0a
		mpysp2dp	b2a,x2a,X1a
		lddw		*spb[2],y0b
		mpysp2dp	b2b,x2b,X1b
		ldw			*ipa++,x1a
		mpysp2dp	b1a,x2a,pa
		ldw			*ipb++,x1b
		mpysp2dp	b1b,x2b,pa
		zero		nulll

		mpysp2dp	b1a,x1a,pa
||		adddp		pa,X0a,X0a

		zero		nullh

		mpysp2dp	b1b,x1b,pa
||		adddp		pa,X0b,X0b

		ldw			*ipa++,x0a

		mpysp2dp	b0a,x1a,pa
||		adddp		pa,X1a,X1a

		ldw			*ipb++,x0b

		mpysp2dp	b0b,x1b,pa
||		adddp		pa,X1b,X1b

		nop

		mpysp2dp	b0a,x0a,pa
||		adddp		pa,X0a,X0a
||		mpyspdp		a2a,y1a,pb

		nop
		
		mpysp2dp	b0b,x0b,pa
||		adddp		pa,X0b,X0b

		mv.d		x1a,x3a
||		mpyspdp		a2b,y1b,pb

		mpysp2dp	b2a,x3a,X0a
||		adddp		pa,X1a,X1a

		mv.d		x1b,x3b
									
		mpysp2dp	b2b,x3b,X0b
||		adddp		pa,X1b,X1b
||		mpyspdp		a1a,y0a,pb
||		adddp		X0a,pb,y1a

		mv.d		x0a,x2a

		mpysp2dp	b2a,x2a,pa


		mv.d		x0b,x2b
||		mpyspdp		a1b,y0b,pb
||		adddp.l		X0b,pb,y1b

		mpysp2dp	b2b,x2b,pa2

		ldw			*ipa++,x1a

		mpysp2dp	b1a,x2a,pa
||		mpyspdp		a2a,y0a,pb
||		adddp.s		pb,y1a,y1a

		adddp.s		pa,null,X1a
||		ldw			*ipb++,x1b

		mpysp2dp	b1b,x2b,pa

		mpyspdp		a2b,y0b,pb
||		adddp.s		pb,y1b,y1b

		adddp.s		pa2,null,X1b
||		mpysp2dp	b1a,x1a,pa
||		adddp.l		pa,X0a,X0a

LOOP			
			ldw			*ipa++,x0a
		
			mpysp2dp	b1b,x1b,pa
||			adddp.l		pa,X0b,X0b
||			mpyspdp		a1a,y1a,pb
||			adddp.s		X1a,pb,y0a

			ldw			*ipb++,x0b
||			dpsp		y1a,out
		
			mpysp2dp	b0a,x1a,pa
||			adddp.l		pa,X1a,X1a
||			stw			y1al,*spa[2]

			mpyspdp		a1b,y1b,pb
||			adddp.s		X1b,pb,y0b
||			stw			y1ah,*spa[3]
	
			mpysp2dp	b0b,x1b,pa
||			adddp.l		pa,X1b,X1b
||			dpsp		y1b,out

			stw			out,*opa++
					
			mpysp2dp	b0a,x0a,pa
||			adddp.l		pa,X0a,X0a
||			mpyspdp		a2a,y1a,pb
||			adddp.s		pb,y0a,y0a
||	[chb]	stw			y1bl,*spb[2]

			sub.d		cnt,2,cnt

			mpysp2dp	b0b,x0b,pa
||			adddp.l		pa,X0b,X0b
||	[chb]	stw			out,*opb++

			mv.s		x1a,x3a
||			mpyspdp		a2b,y1b,pb
||			adddp.s		pb,y0b,y0b
||	[chb]	stw			y1bh,*spb[3]

			mpysp2dp	b2a,x3a,X0a
||			adddp.l		pa,X1a,X1a

			mv.d		x1b,x3b

			mpysp2dp	b2b,x3b,X0b
||			adddp.l		pa,X1b,X1b
||			mpyspdp		a1a,y0a,pb
||			adddp.s		X0a,pb,y1a

			mv.s		x0a,x2a
||			dpsp		y0a,out

			mpysp2dp	b2a,x2a,pa

			mv.d		x0b,x2b
||			mpyspdp		a1b,y0b,pb
||			adddp.s		X0b,pb,y1b

			mpysp2dp	b2b,x2b,pa2
||			dpsp		y0b,out

			ldw			*ipa++,x1a
||	[cnt]	b			LOOP

			mpysp2dp	b1a,x2a,pa
||			mpyspdp		a2a,y0a,pb
||			adddp.s		pb,y1a,y1a
||			stw			out,*opa++

			adddp.s		pa,null,X1a
||			ldw			*ipb++,x1b

			mpysp2dp	b1b,x2b,pa
||	[chb]	stw			out,*opb++

			mpyspdp		a2b,y0b,pb
||			adddp.s		pb,y1b,y1b

			adddp.s		pa2,null,X1b
||			mpysp2dp	b1a,x1a,pa
||			adddp.l		pa,X0a,X0a

ENDLOOP
		stw			sx3a,*spa[0]
 [chb]	stw			sx3b,*spb[0]
		stw			sx2a,*spa[1]
 [chb]	stw			sx2b,*spb[1]
		stw			y0al,*spa[4]
		stw			y0ah,*spa[5]
 [chb]	stw			y0bl,*spb[4]
 [chb]	stw			y0bh,*spb[5]

PREPARE_RETURN:
		
		ldw			*++b15[1],b3
		ldw			*++b15[1],a15
		ldw			*++b15[1],b14
		ldw			*++b15[1],a14
		ldw			*++b15[1],b13
		ldw			*++b15[1],a13
		ldw			*++b15[1],b12
		ldw			*++b15[1],a12
		ldw			*++b15[1],b11
		ldw			*++b15[1],a11
		ldw			*++b15[1],b10
		b 			b3
		ldw			*++b15[1],a10
		mvc			csr,b0
		extu		b0,1,1,b1
 [b1]	set			b0,0,0,b0
 		mvc			b0,csr
             
