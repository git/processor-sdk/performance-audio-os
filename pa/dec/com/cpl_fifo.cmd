
-heap 100000

MEMORY
{
  RAM: o=80000000h l=01000000h
}

SECTIONS
{
    vectors     >       RAM
    .text       >       RAM
    .data       >       RAM 
    .stack      >       RAM
    .bss        >       RAM
    .sysmem     >       RAM
    .cinit      >       RAM
    .const      >       RAM
    .cio        >       RAM
    .far        >       RAM
}
