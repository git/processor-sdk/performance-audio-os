/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/*
 *  ======== fil_setup.c ========
 *  FIL setup phase implementation.  This is common to all filter calls.
 */

/* Std header files */
#include <xdc/std.h>
#include <ti/sysbios/knl/Clock.h>
#include <xdc/runtime/LoggerBuf.h>

/* Header files */
#include <ifil.h>
#include <fil_tii.h>
#include <fil_tii_priv.h>
#include <filerr.h>
#include <filextern.h>
//#include  <xdc/std.h>
#include <ti/xdais/xdas.h>
#include <paftyp.h>
#include <filtyp.h>

/*
 *  ======== FIL_offsetParam() ========
 *  Shifts the layer2 structure pointers by a given offset 
 */
/* Memory section for the function code */
Void FIL_offsetParam( PAF_FilParam *pParam, Int offset )
{
    /* Offset the FIL layer2 structure pointers by given offset */
    pParam->pIn   += offset;
    pParam->pOut  += offset;
    pParam->pCoef += offset;
    pParam->pVar  += offset;
} /* Void FIL_offsetParam() */

/*
 *  ======== FIL_setup() ========
 *  Init. and setup of structures, before calling the filter
 */
/* Memory section for the function code */
Int FIL_setup(FIL_Handle handle, PAF_AudioFrame *pAudioFrame, PAF_FilParam *pParam )
{

    Uint type, cSampRate;
    Int impCh , varsPerCh;
    Int i = 0;
    FIL_TII_Obj * filterHandle = (FIL_TII_Obj *)handle; /* FIL handle */

    Uint mask = 0x1; /* Round Robin Mask - Starting from LSB bit */
    PAF_ChannelMask appCh, chTemplate;
    Int coeffPtrPerCh;
    Int coeffPtrPerSampleRate, cCh;
    
    Void *varsPtr, **input, **output, **coeffsPtr;
    
    /* Get various parameters into local variables */
    type        = filterHandle->pConfig->pCoefs->type; /* Coeff. type field */
    appCh       = filterHandle->pStatus->maskStatus; /* Ch's to filter */
    varsPtr     = (Void *)filterHandle->pConfig->pVars; /* State mem pointer */
    input       = (Void **)pAudioFrame->data.sample; 
    output      = (Void **)pAudioFrame->data.sample; /* FIL is inplace */
    coeffsPtr   = &(((PAF_FilCoef_Void *)filterHandle->
                    pConfig->pCoefs)->cPtr[0]);
    chTemplate  = filterHandle->pStatus->mode;
    varsPerCh   = handle->fxns->varsPerCh(type); /* Function returns state-mem / ch */
    cCh         = FilMacro_GetIRCh(type); /* Ch's in the coeff structure */
    
    coeffPtrPerCh = 0; /* Coeff Ptr increment is reset(Unicoef) */
    coeffPtrPerSampleRate = 1; /* Samprate Ptr increment, set to 1(Unicoef) */
    
    /* If not unicoefficient, coeff ptr is different for each channel */
    if( !(type & FIL_UNICOEFFSET) )
    {
        coeffPtrPerCh = 1;
        coeffPtrPerSampleRate = cCh; /* Each sample-rate has 'ch' no of ptrs */
    }
    
    /* If multiSampleRate */ 
    if( type & FIL_BIT31 )
    {
        Int dSampleRate, sampIdx;
        
        dSampleRate = pAudioFrame->sampleRate; /* Data stream sample rate */
        
        /* ASP says to return error if data sample rate is NONE */
        if(dSampleRate == PAF_SAMPLERATE_NONE)
            return(FIL_ERROR);
            
        /* Coefficient sample rate bit field */
        cSampRate    = filterHandle->pConfig->pCoefs->sampRate;
            
        /* ASP says to take sample rate as 48K, if its unknown */    
        if(dSampleRate == PAF_SAMPLERATE_UNKNOWN)
            dSampleRate = PAF_SAMPLERATE_48000HZ;
            
        /* Check whether Coeff structure contains sampRate matching the data */
        if( !( cSampRate & ( 0x1 << dSampleRate ) ) )
            return(FIL_ERROR);
            
        dSampleRate -= 2; /* Skip the 'none' and 'unknown' sample rates */
        cSampRate  >>= 2; /* Do the same in coeff bit field also */
        
        /* Find the no. of sample rates(1's) before the requested one(bit) */
        FilMacro_1sToRight(cSampRate, dSampleRate, sampIdx, i);
        
        /* Offset the coef ptr to the required sample rate */
        coeffsPtr += sampIdx * coeffPtrPerSampleRate;  
        
    } /* if( type & FIL_BIT31 ) */    
    
    /* Parse through the channel select bit field */
    for( impCh = 0, i = 0; impCh < PAF_MAXNUMCHAN; impCh++ )
    {
        /* If the channel bit is 1(select),fill its corresponding ptrs */
        if(appCh & mask)
        {
            pParam->pIn  [i] = *input;
            pParam->pOut [i] = *output;
            pParam->pVar [i] = varsPtr;
            pParam->pCoef[i] = *coeffsPtr;
            i++;
        }
        /* If channel template has the next ch, offset the pointers */
        if(chTemplate & mask)
        {
            varsPtr   = (Char *)varsPtr + varsPerCh;
            coeffsPtr += coeffPtrPerCh;
        }
        /* Update for next iteration */
        mask <<= 1;
        input++;
        output++;  
        
    } /* for( impCh = 0, i = 0; impCh < PAF_MAXNUMCHAN; impCh++ ) */
    
    /**** Filter specific setup ****/
    
    /* If Impulse Response Group */
    if( (FilMacro_GetGroupField(type) == FIL_IR ) || (FilMacro_GetGroupField(type) == FIL_CASCADE_IR) )
    {
        /* 'Use' element has both circular index and taps */
        pParam->use &= 0xFFFF0000;
        pParam->use |= FilMacro_GetIRTap(type); /* Get taps */
        
        /* If not unicoeff */
        if( !(type & FIL_UNICOEFFSET) )
        { 
            Int maskLength, dCh;
            
            /* Find mask size */            
            maskLength = sizeof(PAF_ChannelMask) * FIL_BITSPERBYTE; 
            
            /* Find the no of chs selected for filtering */
            FilMacro_1sToRight(appCh, maskLength, dCh, i);
            
            /* If not unicoeff, then coef-ch = selected-ch */
            if( cCh < dCh )
            {
                filterHandle->pStatus->maskStatus = 0x0; /* Reset status */
                return( FIL_ERROR );
            }
        } /* if( !(type & FIL_UNICOEFFSET) ) */
        
    } /* if( FilMacro_GetGroupField(type) == FIL_IR ) */
        
    return( FIL_SUCCESS );

} /* Int FIL_setup() */

/*
 *  ======== FIL_memReset() ========
 *  Resets the memory to 0, given start address and size in bytes. 
 */
/* Memory section for the function code */
Void FIL_memReset( Void *varPtr, Int size )
{
    Int i;
    Char *charPtr = (Char *)varPtr;
    
    /* Reset the memory, byte-wise */
    for( i=0; i < size; i++ )
        *charPtr++ = 0;
}

Void FIL_enquire(IALG_Params *algParams, IALG_MemRec *filter_memTab)
{
    IFIL_Params *params = (IFIL_Params *) algParams;
    Uint type;
    Int maxCh;
    Int i = 0;
    
    /* Setup of variables from the arguments. */
    type  = params->pConfig->pCoefs->type;
    
    /* Find the maximum channels filtered, by counting the bits=1 */
    FilMacro_1sToRight( params->pStatus->mode, PAF_MAXNUMCHAN, maxCh, i );
    
    /* Update the memTab structure */
    filter_memTab->size      = maxCh * FIL_varsPerCh(type);
    filter_memTab->alignment = 8; // This makes LDDW possible always.
}



