
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common defs and structures for PAF interface to Audio SIO drivers.
//
//
//

#include "fwkPort.h"
#include <xdc/runtime/IHeap.h>
#include <ti/sysbios/heaps/HeapMem.h>

#ifndef _TMS320C6X
#define STATIC static
#else
#define STATIC
#endif

#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)
#define PAF_DEVICE_MAJOR_REV (PAF_DEVICE & 0xF000)

// For Mahlar builds use older header file
#if (PAF_DEVICE_MAJOR_REV == 0xA000)
#include <paf_sio.h>
#define PAFSIO_H
#endif

#ifndef PAFSIO_H
#define PAFSIO_H

#include <xdc/std.h>
//#include <dev.h>
//#include <log.h>
//#include <que.h>
//#include <sem.h>
#include <sio.h>

//#include  <std.h>
#include <ti/xdais/xdas.h> // for data types
#include <xdc/runtime/LoggerBuf.h>

#include "dpll.h"

// .............................................................................
// device configuration structure -- used as argument to SIO_ctrl

struct PAF_SIO_Params;

struct DXX_Params_
{
    const char   *name;          //driver name, e.g. "DAP"
    XDAS_Int32    moduleNum;
    XDAS_Void    *pConfig;
    XDAS_Int16    wordSize;
    XDAS_Int16    precision;

    XDAS_Int32  (*control)( DEV_Handle, const struct PAF_SIO_Params *, XDAS_Int32, XDAS_Int32 );
};

typedef struct PAF_SIO_Params
{
    Int                size;    // Type-specific size
    struct DXX_Params_ sio;     // Common parameters
} PAF_SIO_Params;

// .............................................................................

typedef struct PAF_SIO_InputStatus
{
    XDAS_Int8 lock;
    XDAS_Int8 nonaudio;
    XDAS_Int8 emphasis;
    XDAS_Int8 sampleRateMeasured;
    XDAS_Int8 sampleRateData;
    XDAS_Int8 unused[3];

} PAF_SIO_InputStatus;

// .............................................................................
// driver transfer statistics

typedef struct PAF_SIO_Stats
{
    int                 state;
    float               freqSeed;
    unsigned int        lastClkCnt;
    double              clkTotal;
    DPLL2               dpll;
} PAF_SIO_Stats;

enum PAF_SIO_StatsState
{
    PAF_SIO_STATS_DISABLED,
    PAF_SIO_STATS_ENABLED
};

// .............................................................................
// argument for SIO_control

enum PAF_SIO_ControlCode
{
    PAF_SIO_CONTROL_OPEN                 = 0x100,
    PAF_SIO_CONTROL_CLOSE                = 0x101,
    PAF_SIO_CONTROL_GET_INPUT_STATUS     = 0x102,
    PAF_SIO_CONTROL_SET_WORDSIZE         = 0x103,
    PAF_SIO_CONTROL_GET_WORDSIZE         = 0x104,
    PAF_SIO_CONTROL_GET_PRECISION        = 0x105,
    PAF_SIO_CONTROL_GET_NUMCHANNELS      = 0x106,
    PAF_SIO_CONTROL_SET_RATEX            = 0x107,
    PAF_SIO_CONTROL_TRACE                = 0x108,
    PAF_SIO_CONTROL_MUTE                 = 0x109,
    PAF_SIO_CONTROL_UNMUTE               = 0x10a,
    PAF_SIO_CONTROL_IDLE                 = 0x10b,
    PAF_SIO_CONTROL_CHANNEL_STATUS       = 0x10c,
    PAF_SIO_CONTROL_LINK_DEVICE          = 0x10d,
    PAF_SIO_CONTROL_IDLE_WITH_CLOCKS     = 0x10e,
    PAF_SIO_CONTROL_SET_SOURCESELECT     = 0x10f,
    PAF_SIO_CONTROL_GET_SOURCEPROGRAM    = 0x110,
    PAF_SIO_CONTROL_SET_PCMFRAMELENGTH   = 0x111,
    PAF_SIO_CONTROL_SET_BUFSTATUSADDR    = 0x112,
    PAF_SIO_CONTROL_SET_OPT_LEVEL        = 0x113,
    PAF_SIO_CONTROL_ENABLE_STATS         = 0x114,
    PAF_SIO_CONTROL_DISABLE_STATS        = 0x115,
    PAF_SIO_CONTROL_GET_STATS            = 0x116,
    PAF_SIO_CONTROL_GET_CHILD_DEVICE     = 0x117,
    PAF_SIO_CONTROL_OUTPUT_START_CLOCKS  = 0x118,
    PAF_SIO_CONTROL_SET_DECSTATUSADDR    = 0x119,
    PAF_SIO_CONTROL_WATCHDOG             = 0x120,
    PAF_SIO_CONTROL_SET_ENCSTATUSADDR    = 0x121,
    PAF_SIO_CONTROL_GET_NUM_REMAINING    = 0x122,
    PAF_SIO_CONTROL_SET_AUTOREQUESTSIZE  = 0x123,
    PAF_SIO_CONTROL_SET_DITSTATUS        = 0x124,
    PAF_SIO_CONTROL_SET_IALGADDR         = 0x125,
    PAF_SIO_CONTROL_SET_NUMBUF           = 0x126,
    PAF_SIO_CONTROL_SET_MAX_NUMBUF       = 0x127,
    PAF_SIO_CONTROL_GET_NUM_EVENTS       = 0x128
};

// .............................................................................
// for use as argument to SIO_ctrl + PAF_SIO_CONTORL_SET_OPT_LEVEL
enum PAF_SIO_OptLevel
{
    PAF_SIO_OPT_LEVEL_NONE            = 0,
    PAF_SIO_OPT_LEVEL_REUSE_BUFFERS   = 1
};

// .............................................................................
// argument for SIO_issue
enum PAF_SIO_RequestCode
{
    PAF_SIO_REQUEST_SYNC              = 0x100,
    PAF_SIO_REQUEST_NEWFRAME          = 0x101,
    PAF_SIO_REQUEST_AUTO              = 0x102
};

// .............................................................................

enum PAF_SIO_ERROR
{
    PAF_SIO_ERROR_NONE,
    PAF_SIO_ERROR_IDLE_STAGE1,
    PAF_SIO_ERROR_IDLE_STAGE2,
    PAF_SIO_ERROR_ERRBUF_XFER,
    PAF_SIO_ERROR_FATAL
};

// .............................................................................
struct PAF_SIO_Fxns {
    Void (*free)( SIO_Handle hSio, int mode );
    SIO_Handle (*recreate)( PAF_SIO_Params *pParams, int mode, LOG_Obj *pLog, int heapID );
    Int (*pllEnable) (SIO_Handle hMaster, SIO_Handle hSlave, double seed);
};

extern struct PAF_SIO_Fxns PAF_SIO_fxns;

STATIC inline Void   PAF_SIO_free( SIO_Handle hSio, int mode )
{        PAF_SIO_fxns.free(            hSio,     mode ); }

STATIC inline SIO_Handle PAF_SIO_recreate( PAF_SIO_Params *pParams, int mode, LOG_Obj *pLog, int heapID )
{ return     PAF_SIO_fxns.recreate(                 pParams,     mode,          pLog,     heapID ); }

STATIC inline Int       PAF_SIO_pllEnable(SIO_Handle hMaster, SIO_Handle hSlave, double seed)
{ return    PAF_SIO_fxns.pllEnable(           hMaster,            hSlave,        seed); }

// .............................................................................

#endif // PAFSIO_H
