
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PCM Encoder algoritm interface implementation
//
//
//

/*
 *  IPCE default instance creation parameters
 */
#include <std.h>
#include <ipce.h>

/*
 *  ======== IPCE_PARAMS_CUS ========
 *  This static initialization defines the default parameters used to
 *  create instances of PCE objects.
 */

#if IPCE_PHASES != 6
#error internal error
#endif

// Start of customizable code -------------------------------------------------

#define IPCE_OUTPUT_NUMCHAN	12								// Number of output channels
#define IPCE_OUTPUT_ESIZE	3								// Element size for output: 24 or 32 bits

#define IPCE_FSIZE			256								// Size of Audio Frame

#define IPCE_DELAY_NUMCHAN	IPCE_OUTPUT_NUMCHAN

#define IPCE_MAXDELAY_LEFT	20*96
#define IPCE_MAXDELAY_RGHT	20*96
#define IPCE_MAXDELAY_CNTR	20*96
#define IPCE_MAXDELAY_LWID	20*96
#define IPCE_MAXDELAY_RWID	20*96
#define IPCE_MAXDELAY_LSUR	20*96
#define IPCE_MAXDELAY_RSUR	20*96
#define IPCE_MAXDELAY_LBAK	20*96
#define IPCE_MAXDELAY_RBAK	20*96
#define IPCE_MAXDELAY_SUBW	20*96
#define IPCE_MAXDELAY_LHED	20*96
#define IPCE_MAXDELAY_RHED	20*96

// End of customizable code ---------------------------------------------------

#define IPCE_DELAY_ESIZE	3								// Element size for delay: packed 24 bits

/*
 *  ======== IPCE_PARAMS_STATUS_DELAY_CUS ========
 */

const IPCE_Status IPCE_PARAMS_STATUS_CUS = {
	sizeof (IPCE_Status),
	1,														/* mode: enabled */
	0,														/* type: unused */
	1, 0,													/* phase 0 mode,type: enabled,unused */
	1, 0,													/* phase 1 mode,type: enabled,unused */
	1, 0,													/* phase 2 mode,type: enabled,unused */
	1, 0,													/* phase 3 mode,type: enabled,unused */
	1, 0,													/* phase 4 mode,type: enabled,unused */
	1, 0,													/* phase 5 mode,type: enabled,unused */
	/* Delay Status */
	0,														/* unused */
	1,														/* unit */
	PAF_MAXNUMCHAN_AF,											/* numc */
	IPCE_DELAY_NUMCHAN,										/* nums */
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,			/* delay */
	0,      /* masterDelay */
    0,          /* pceExceptionDetect */
    0,          /* pceExceptionFlag */
    0,          /* pceExceptionMute */
    0           /* pceClipDetect */														
};

/*
 *  ======== IPCE_PARAMS_CONFIG_DELAY_CUS ========
 *  This static initialization defines the parameters used to create
 *  instances of DEL objects
 */

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (IPCE_PARAMS_CONFIG_PHASE_VOLUME_CUS, ".text:IPCE_PARAMS_CONFIG_PHASE_VOLUME_CUS")
#endif // USE_EXT_RAM
const IPCE_ConfigPhaseVolume IPCE_PARAMS_CONFIG_PHASE_VOLUME_CUS = {
	sizeof (IPCE_ConfigPhaseVolume),
	-2 * 40,												/* volumeRamp */
};

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (IPCE_PARAMS_CONFIG_PHASE_DELAYSPEC_CUS, ".text:IPCE_PARAMS_CONFIG_PHASE_DELAYSPEC_CUS")
#endif // USE_EXT_RAM
const PAF_DelayState IPCE_PARAMS_CONFIG_PHASE_DELAYSPEC_CUS[] = {
	0, 0, IPCE_MAXDELAY_LEFT, 0, NULL, 0,
	0, 0, IPCE_MAXDELAY_RGHT, 0, NULL, 0,
	0, 0, IPCE_MAXDELAY_CNTR, 0, NULL, 0,
	0, 0, IPCE_MAXDELAY_LWID, 0, NULL, 0,
	0, 0, IPCE_MAXDELAY_RWID, 0, NULL, 0,
	0, 0, IPCE_MAXDELAY_LSUR, 0, NULL, 0,
	0, 0, IPCE_MAXDELAY_RSUR, 0, NULL, 0,
	0, 0, IPCE_MAXDELAY_LBAK, 0, NULL, 0,
	0, 0, IPCE_MAXDELAY_RBAK, 0, NULL, 0,
	0, 0, IPCE_MAXDELAY_SUBW, 0, NULL, 0,
	0, 0, IPCE_MAXDELAY_LHED, 0, NULL, 0,
	0, 0, IPCE_MAXDELAY_RHED, 0, NULL, 0,
};

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (IPCE_PARAMS_CONFIG_PHASE_DELAY_CUS, ".text:IPCE_PARAMS_CONFIG_PHASE_DELAY_CUS")
#endif // USE_EXT_RAM
const IPCE_ConfigPhaseDelay IPCE_PARAMS_CONFIG_PHASE_DELAY_CUS = {
	sizeof (IPCE_ConfigPhaseDelay) + IPCE_DELAY_NUMCHAN * sizeof (PAF_DelayState),
	(1 << PAF_LEFT) |
	(1 << PAF_RGHT) |
	(1 << PAF_CNTR) |
	(1 << PAF_LWID) |
	(1 << PAF_RWID) |
	(1 << PAF_LSUR) |
	(1 << PAF_RSUR) | 
	(1 << PAF_LBAK) | 
	(1 << PAF_RBAK) | 
	(1 << PAF_SUBW) | 
	(1 << PAF_LHED) | 
	(1 << PAF_RHED) | 
	0,
	IPCE_DELAY_ESIZE,
	(PAF_DelayState *) IPCE_PARAMS_CONFIG_PHASE_DELAYSPEC_CUS,
};

#define IPCE_DELAY_TOTAL ( \
			IPCE_MAXDELAY_LEFT + \
			IPCE_MAXDELAY_RGHT + \
			IPCE_MAXDELAY_CNTR + \
         	IPCE_MAXDELAY_LWID + \
         	IPCE_MAXDELAY_RWID + \
			IPCE_MAXDELAY_LSUR + \
			IPCE_MAXDELAY_RSUR + \
         	IPCE_MAXDELAY_LBAK + \
         	IPCE_MAXDELAY_RBAK + \
         	IPCE_MAXDELAY_SUBW + \
         	IPCE_MAXDELAY_LHED + \
         	IPCE_MAXDELAY_RHED + \
         	0 )

const PAF_IALG_Common IPCE_PARAMS_COMMON_PHASE_DELAY_CUS = {
	sizeof (PAF_IALG_Common) + IPCE_DELAY_TOTAL * IPCE_DELAY_ESIZE,
	PAF_IALG_COMMONN (8),									/* flag: specifies common memory type */
};

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (IPCE_PARAMS_CONFIG_CUS, ".text:IPCE_PARAMS_CONFIG_CUS")
#endif // USE_EXT_RAM
const IPCE_Config IPCE_PARAMS_CONFIG_CUS = {
	sizeof (IPCE_Config),
	0,														/* frameLength */
	0,														/* unused */
	(IPCE_ConfigPhase *) & IPCE_PARAMS_CONFIG_PHASE_VOLUME_CUS,	/* phase 0 config: volume */
	(IPCE_ConfigPhase *) & IPCE_PARAMS_CONFIG_PHASE_DELAY_CUS,	/* phase 1 config: delay */
	0,														/* phase 2 config: output */
	0,														/* phase 3 config: unused */
	0,														/* phase 4 config: unused */
	0,														/* phase 5 config: unused */
	0,														/* phase 0 common: volume */
	(PAF_IALG_Common *) & IPCE_PARAMS_COMMON_PHASE_DELAY_CUS,	/* phase 1 common: delay */
	0,														/* phase 2 common: output */
	0,														/* phase 3 common: unused */
	0,														/* phase 4 common: unused */
	0,														/* phase 5 common: unused */
	/* scale -- uninitialized */
};

/*
 *  ======== IPCE_PARAMS_ACTIVE ========
 */

const PAF_ActivePhaseVolume IPCE_PARAMS_ACTIVE_PHASE_VOLUME_CUS = {
    sizeof(PAF_ActivePhaseVolume),
                            /* pVolumeStatus: uninitialized */
};

const PAF_ActivePhaseOutput IPCE_PARAMS_ACTIVE_PHASE_OUTPUT_CUS = {
	sizeof (PAF_ActivePhaseOutput),
	/* pEncodeStatus: uninitialized */
	/* pOutBufConfig: uninitialized */
};

const IPCE_Active IPCE_PARAMS_ACTIVE_CUS = {
	0,
	0,
	(PAF_ActivePhase *) & IPCE_PARAMS_ACTIVE_PHASE_VOLUME_CUS,	/* phase 0 active: volume */
	0,														/* phase 1 active: delay */
	(PAF_ActivePhase *) & IPCE_PARAMS_ACTIVE_PHASE_OUTPUT_CUS,	/* phase 2 active: output */
	0,														/* phase 3 active: unused */
	0,														/* phase 4 active: unused */
	0,														/* phase 5 active: unused */
	/* bitstreamMask uninitialized */
};

/*
 *  ======== IPCE_PARAMS_SCRACH ========
 */
#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (IPCE_PARAMS_SCRACH_PHASE_DELAY_CUS, ".text:IPCE_PARAMS_SCRACH_PHASE_DELAY_CUS")
#endif // USE_EXT_RAM
const PAF_ScrachPhaseDelay IPCE_PARAMS_SCRACH_PHASE_DELAY_CUS = {
	sizeof (PAF_ScrachPhaseDelay) + IPCE_DELAY_ESIZE * IPCE_FSIZE,
};

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (IPCE_PARAMS_SCRACH_PHASE_OUTPUT_CUS, ".text:IPCE_PARAMS_SCRACH_PHASE_OUTPUT_CUS")
#endif // USE_EXT_RAM
const PAF_ScrachPhaseOutput IPCE_PARAMS_SCRACH_PHASE_OUTPUT_CUS = {
	sizeof (PAF_ScrachPhaseDelay) + IPCE_OUTPUT_NUMCHAN * IPCE_OUTPUT_ESIZE * IPCE_FSIZE,
};

#ifdef USE_EXT_RAM
    #pragma DATA_SECTION (IPCE_PARAMS_SCRACH_CUS, ".text:IPCE_PARAMS_SCRACH_CUS")
#endif // USE_EXT_RAM
const IPCE_Scrach IPCE_PARAMS_SCRACH_CUS = {
	sizeof (IPCE_Scrach),
	0,														/* phase 0 scrach: volume */
	(PAF_ScrachPhase *) & IPCE_PARAMS_SCRACH_PHASE_DELAY_CUS,	/* phase 1 scrach: delay */
	(PAF_ScrachPhase *) & IPCE_PARAMS_SCRACH_PHASE_OUTPUT_CUS,	/* phase 2 scrach: output */
	0,														/* phase 3 scrach: unused */
	0,														/* phase 4 scrach: unused */
	0,														/* phase 5 scrach: unused */
};

const IPCE_Params IPCE_PARAMS_CUS = {
	sizeof (IPCE_Params),
	&IPCE_PARAMS_STATUS_CUS,
	&IPCE_PARAMS_CONFIG_CUS,
	&IPCE_PARAMS_ACTIVE_CUS,
	&IPCE_PARAMS_SCRACH_CUS,
};
