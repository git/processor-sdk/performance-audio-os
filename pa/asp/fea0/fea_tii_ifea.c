
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  TII implementation of an FIL-ASP Example Demonstration algorithm -
 *  A simple FIR filter implementation.
 */

#include <std.h>
#include  <std.h>
#include <xdas.h>
#include "paftyp.h"

#include <ifea.h>
#include <fea_tii.h>
#include <fea_tii_priv.h>
#include <feaerr.h>

#if PAF_AUDIODATATYPE_FIXED
    #error fixed point audio data type not supported by this implementation
#endif /* PAF_AUDIODATATYPE_FIXED */

/*
 *  ======== FEA_TII_apply ========
 *  TII's implementation of the apply operation.
 *
 *  This function is called after the FEA_TII_reset function, and
 *  deals with control data AND sample data.
 *
 */

#ifndef _TMS320C6X
    #define restrict
#endif /* _TMS320C6X */

Int FEA_TII_apply(IFEA_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    FEA_TII_Obj * restrict fea = (Void *)handle; /* Module handle */
    FIL_Handle fil = fea->firHandle;

    /* On disabling FEA module, set the reset flag; so it resets the when enabled */ 
    if((fea->pStatus->mode==0) && (fea->pStatus->unused==0))
        fea->pStatus->unused = 1;
            
    if (fea->pStatus->mode == 0)
        return 0;   /* If mode is zero, ASP is Disabled, so exit. */
        
    /* Reset the filter states if FEA is nabled when reset flag is '1'*/
    if((fea->pStatus->mode==1) && (fea->pStatus->unused==1))
    {      
        fil->fxns->reset(fil, pAudioFrame);              
        fea->pStatus->unused = 0;
    }
    
    fil->fxns->apply(fil,pAudioFrame);
    
    return 0;
}

/*
 *  ======== FEA_TII_reset ========
 *  TII's implementation of the reset function, which is like an "information" 
 *  operation.  This is always called by the framework before calling the 
 *  FEA_TII_apply function, and only deals with control data, filter states etc
 *  and not sample data.
 */

Int FEA_TII_reset(IFEA_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    FEA_TII_Obj * restrict fea = (Void *)handle; /* FEA handle */
    FIL_Handle fil = fea->firHandle;

    fil->fxns->reset(fil, pAudioFrame);          
    fea->pStatus->unused = 0; /* Set the reset flag to '0' */
    
    return 0;
}

/* EOF */
