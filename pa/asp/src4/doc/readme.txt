This describes SRC test app written to test XDAIS ASP component SRC(Sampling Rate Converter)

Test App follows below folder structure:
asp/src4/test ---- include      -> Contains all include files(*.h) required
              ---- src          -> Contains all C files(*.c)
			  ---- doc          -> Related documents
			  ---- test_vectors --- config    -> Any configuration files
			                    --- input     -> Input stream files
								--- output    -> Output Stream files generated
								--- reference -> Reference Stream to compare Output for given input streams in folder input

src_test_cases.c file is the one which has all listed test cases in table form.
Each test case is picked from here and run for output generation.

Each test case input is designed to have parameters like srcParams structure,
Channel configuration(chSat and chSub),input buffer sample
count, output buffer sample count and output sample rate.

FIO(File Input\output) wrapper API's are used for reading and writing
the data from input stream file to output stream file respectively.

Main test app source code is present in testapp.c and testapp.h files.
Two files util_file_io.c and util_paf_file_io.c are FIO wrapper.
Two files src_alg_create.c and src_alg_malloc.c are used for generic PAF_ALG functions such
as PAF_ALG_create,PAF_ALG_delete,PAF_ALG_activate,PAF_ALG_deactivate.

Limitations: As of now it is been tested for mono,stereo and 5.1 channel inputs only.
             Input stream of bitRate 32-bit is only supported.(Since FIO API's supports only 32-bit read and write operations)

Dependent Libraries: SRC4 test app depends on these asp_std.lib, com_dec.lib, fil_elf.lib, com_asp.lib and src4.lib libraries.
                    PAF_AudioFunctions listed below and used in SRC4 are defined in asp_std.lib
					1)PAF_ASP_channelMask
					
					API's dnsamp2xSymmetricEven4Msample,upsamp2xSymmetricOdd8Msample are used from fil_elf.lib
					
					All CPL API's used in pafhjt.c files are defined in com_dec.lib.
					
					API's COM_TII_activateCommon_,COM_TII_control,COM_TII_deactivateCommon_,COM_TII_free,
					COM_TII_snatchCommon_ used in SRC4 library are part of com_asp.lib