#ifndef _PROCSDK_AUDIO_TYP_H_
#define _PROCSDK_AUDIO_TYP_H_

#include <stdint.h>
#include <xdc/std.h>

typedef int_least8_t    SmInt;
typedef int_least16_t   MdInt;
typedef int_least32_t   LgInt;
typedef uint_least8_t   SmUns;
typedef uint_least16_t  MdUns;
typedef uint_least32_t  LgUns;

/* Moved the declaration from C67Lib.h */
typedef unsigned  char     U8;
typedef signed    char     S8;
typedef unsigned  short    U16;
typedef signed    short    S16;
typedef unsigned  int      U32;
typedef signed    int      S32;
typedef float FLT;

#endif /* _PROCSDK_AUDIO_TYP_H_ */
