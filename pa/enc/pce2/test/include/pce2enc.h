/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

#ifndef PCEENC_
#define PCEENC_
#include <ipce.h>
#include <paf_alg.h>
#include <paf_alg_priv.h>
/*
 *  ======== PCE_Handle ========
 *  This pointer is used to reference all PCE instance objects
 */
typedef struct IPCE_Obj *PCE_Handle;

/*
 *  ======== PCE_Params ========
 *  This structure defines the creation parameters for all PCE objects
 */
typedef IPCE_Params PCE_Params;

/*
 *  ======== PCE_PARAMS ========
 *  This structure defines the default creation parameters for PCE objects
 */
#define PCE_PARAMS   IPCE_PARAMS

extern const IPCE_Fxns PCE_TII_IPCE; 
/*
 *
 * This structure defines the status parameters for PCE objects
*/
typedef IPCE_Status PCE_Status;

typedef IPCE_Cmd PCE_Cmd;
typedef IPCE_Fxns PCE_Fxns;

/*
 *  ======== Control commands ========
 *  control method commands
*/
#define PCE_MININFO    IPCE_MININFO
#define PCE_MINSAMGEN    IPCE_MINSAMGEN
#define PCE_MAXSAMGEN   IPCE_MAXSAMGEN


/*
 *  ======== PCE_create ========
 *  Create an MPEG4VENC instance object (using parameters specified by prms)
 */
extern PCE_Handle PCE_create(
    const IPCE_Fxns *fxns,
    const PCE_Params *params,
    PAF_IALG_Config *pafConfig);

/*
 *  ======== PCE_delete ========
 *  Delete the MPEG4VENC instance object specified by handle
 */
extern XDAS_Void PCE_delete(PCE_Handle handle);

extern  XDAS_Int32 PCE_encodeFrame(IPCE_Handle PCEHandle,
                           PAF_EncodeInStruct * inStruct,
 						   PAF_EncodeOutStruct * outStruct);
/*
 *  ======== PCE_init ========
 */
extern XDAS_Void PCE_init(void);

/*
 *  ======== PCE_exit ========
 */
extern XDAS_Void PCE_exit(void);

/*
 *  ======== PCE_decodeFrame ========
 */


/*
 *  ======== PCE_control ========
 * 
 */

extern XDAS_Int32 PCE_control(PCE_Handle handle, PCE_Cmd cmd, PCE_Status *status);

/*
 *  ======== PCE_info ========
 * 
 */
extern XDAS_Int32 PCE_Info(); 

/*
 *  ======== PCE_reset ========
 * 
 */
extern XDAS_Int32 PCE_Reset();

/*
 *  ======== PCE_volume ========
 * 
 */
extern XDAS_Int32 PCE_volume(); 
#endif   /* PCEENC_ */

