/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/*
 *  ======== iir1tapnch.c ========
 *  IIR filter implementation for tap-1 & channels-N, SP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 1    and CHANNELS N ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1                                    */
/*                           -------------                                   */
/*                            1  - a1*z~1                                    */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n) + b1*x(n-1) + a1*y(n-1)                 */
/*                                                                           */
/*   Canonical form : w(n) = x(n)    + a1*w(n-1)                             */
/*                    y(n) = b0*w(n) + b1*w(n-1)                             */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      w(n) -  w,         w(n-1) - w1                                       */
/*      b0 - filtCfsB[0], b1 - filtCfsB[1], a1 - filtCfsA[0]                 */
/*                                                                           */
/*                                                                           */
/*                              w(n)    b0                                   */
/*     x(n)-->---[+]----->-------@------>>-----[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+    b1     |                            */
/*                 -----<<----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"
#include "fil_table.h" 

/*
 *  ======== Filter_iirT1ChN() ========
 *  IIR filter, taps-1 & channels-N  
 */
/* Memory section for the function code */
#if (PAF_IROM_BUILD==0xD610A003 || PAF_IROM_BUILD==0xD610A004)

Int Filter_iirT1ChN( PAF_FilParam *pParam ) 
{
    Void ** pIn, ** pOut, ** pCoef, ** pVar; /* Handle parameters */
    Int channels, error = 0, multiple;
    
    /* Get handle parameters into temp local variables */
    pIn   = pParam->pIn;
    pOut  = pParam->pOut;
    pCoef = pParam->pCoef;
    pVar  = pParam->pVar;
    
    channels = pParam->channels; /* Get the no of chs filtered */

    if( channels <= PAF_MAXNUMCHAN )
    {
        multiple  = channels >> 1; /* Calculate int(ch/2) */
        channels -= (multiple << 1); /* Find ch % 2 */
        
        /* Multiple of 2 channels are filtered using 2-ch implementation */
        while( multiple )
        {
            error += Filter_iirT1Ch2( pParam );
            FIL_offsetParam( pParam, 2 ); /* Offset the param ptrs by 2 */
            multiple--; /* Decrement counter */     
        }  
        
        if( channels == 1 )
            error += Filter_iirT1Ch1( pParam );
        
    } /* else if( channels <= PAF_MAXNUMCHAN ) */

    /* Put back the handle elements */
    pParam->pIn   = pIn;
    pParam->pOut  = pOut;
    pParam->pCoef = pCoef;
    pParam->pVar  = pVar;  
    
    return(error);     
} /* Int Filter_iirT1ChN() */
    
#else /* (PAF_IROM_BUILD==0xD610A003) */

Int Filter_iirT1ChN( PAF_FilParam *pParam ) 
{
    Void ** pIn, ** pOut, ** pCoef, ** pVar; /* Handle parameters */
    Int i; /* Channel counter */
    Int channels, error = 0, multiple;
    Uint uniCoef = 0, inplace = 0;
    
    /* Get handle parameters into temp local variables */
    pIn   = pParam->pIn;
    pOut  = pParam->pOut;
    pCoef = pParam->pCoef;
    pVar  = pParam->pVar;
    
    /* Check whether unicoeff and inplace */
    for( i = 0; i < pParam->channels; i++ )
    {
      inplace = ( Uint )pParam->pIn[i]   ^  ( Uint )pParam->pOut[i];
      uniCoef = ( Uint )pParam->pCoef[0] ^  ( Uint )pParam->pCoef[i];
    }
              
    channels = pParam->channels; /* Get the no of chs filtered */
    
    /* Ch-5 */
    if( channels == 5 )
    {
        /* If unicoef */
        if( !uniCoef ) 
            error += Filter_iirT1Ch5_u( pParam );
        /* Non-unicoeff as 3-2 chs */
        else
        {
            error += Filter_iirT1Ch3( pParam );
            FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
            error += Filter_iirT1Ch2( pParam );
        }
    } /* if( channels == 5 ) */
    
    /* Ch-6 */
    else if( channels == 6 )
    {
        /* If unicoef & inplace */
        if( !(uniCoef | inplace) )
        {
            //error += Filter_iirT1Ch6_ui( pParam );
             error += Filter_iirT1Ch3( pParam );
            FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
            error += Filter_iirT1Ch3( pParam );
          }
        /* Non unicoef/inplace as 3-3 chs */    
        else
        {
            error += Filter_iirT1Ch3( pParam );
            FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
            error += Filter_iirT1Ch3( pParam );
        }
    } /* else if( channels == 6 ) */
    
    /* Ch-7 */
    else if( channels == 7 )
    {
        /* If unicoef & inplace */
        if( !(uniCoef | inplace) )
            error += Filter_iirT1Ch7_ui( pParam );
            
        /* If unicoeff, as 5u-2 chs */
        else if( !uniCoef ) 
            {
                error += Filter_iirT1Ch5_u( pParam );
                FIL_offsetParam( pParam, 5 ); /* Offset the param ptrs by 5 */
                error += Filter_iirT1Ch2( pParam );
            }
            /* If Non unicoef/inplace, as 4-3 chs */
            else
            {
                error += Filter_iirT1Ch4( pParam );
                FIL_offsetParam( pParam, 4 ); /* Offset the param ptrs by 4 */
                error += Filter_iirT1Ch3( pParam );
            }
    } /* else if( channels == 7 ) */   
    
    /* Ch-8 */
    else if( channels == 8 )
    {
        /* If unicoeff, as 5u-3 chs */
        if( !uniCoef ) 
        {
            error += Filter_iirT1Ch5_u( pParam );
            FIL_offsetParam( pParam, 5 ); /* Offset the param ptrs by 5 */
            error += Filter_iirT1Ch3( pParam );
        }
        /* If Non unicoeff/inplace, as 4-4 chs */
        else
        {
            error += Filter_iirT1Ch4( pParam );
            FIL_offsetParam( pParam, 4 ); /* Offset the param ptrs by 4 */
            error += Filter_iirT1Ch4( pParam );
        }
    } /* else if( channels == 8 ) */
    
    /* Ch-N */       
    else if( channels <= PAF_MAXNUMCHAN )
    {
        multiple  = channels >> 2; /* Calculate int(ch/4) */
        channels -= (multiple << 2); /* Find ch % 4 */
        
        /* Multiple of 4 channels are filtered using 4-ch implementation */
        while( multiple )
        {
            error += Filter_iirT1Ch4( pParam );
            FIL_offsetParam( pParam, 4 ); /* Offset the param ptrs by 4 */
            multiple--; /* Decrement counter */     
        }  
        
        /* If remaining ch=3 */
        if( channels == 3 )
            error += Filter_iirT1Ch3( pParam );
        /* Remaining ch=2 */
        else if( channels == 2 )
            error += Filter_iirT1Ch2( pParam );
        /* Remaining ch=1 */
        else if( channels == 1 )
            error += Filter_iirT1Ch1( pParam );
        
    } /* else if( channels <= PAF_MAXNUMCHAN ) */

    /* Put back the handle elements */
    pParam->pIn   = pIn;
    pParam->pOut  = pOut;
    pParam->pCoef = pCoef;
    pParam->pVar  = pVar;  
    
    return(error);     
} /* Int Filter_iirT1ChN() */

#endif /* (PAF_IROM_BUILD==0xD610A003) */
