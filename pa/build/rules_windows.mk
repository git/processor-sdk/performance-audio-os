# 
#  Copyright (C) 2004-2018 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 
#
# Common MAKE pattern rules for PA.
#
#
# -----------------------------------------------------------------------------

# .............................................................................
# set quietness

ifdef QUIET
	SILENCE := @
endif

# .............................................................................

# create target list from source list by replacing .c with .obj and stripping the path
OBJS     := $(notdir $(SOURCES:%.c=%.obj))
ASMOBJS  := $(notdir $(ASMSOURCES:%.asm=%.obj) $(SASOURCES:%.sa=%.obj))

# ensure the library is the default target so that it is selected when "make"
# is called without arguments.
.DEFAULT_GOAL := ${LIBNAME}.lib
${LIBNAME}.lib: $(OBJS) $(ASMOBJS)
	$(SILENCE)echo -e "Archive: ${LIBNAME}.lib\n"
	$(SILENCE)$(AR) $(ARFLAGS) ${LIBNAME}.lib $+
	$(SILENCE)del /f *.tmp
	
#python $(SILENCE)${ROOTDIR}/tools/symcheck.py --nm=${NM} -d ${LIBNAME}.lib
# Note: Python27\README.txt points to http://support.microsoft.com/kb/321788
# which says STDIN/STDOUT Redirection May Not Work If Started from a File Association
# Invoking python symcheck.py makes it work for me

# For some reason cl6x will only generate one preprocessed file per execution. The one depending on which -pp option occurs first. Unless -pp=
# is used then that takes precedence. So we run it twice. Further there doesn't seem a way to place the pp file into anything other
# than the source locations directory. So we move it.
%.obj: %.c
	$(SILENCE)echo -e "Compile: $<"
	$(SILENCE)$(CC) $(CFLAGS) -ppc $<
	$(SILENCE)echo $(basename $<).pp > temp1.txt
	$(SILENCE)sed -e s/^../"move "/; -e s/\//\\/g; -e s/$$/./g  temp1.txt > temp.bat
	$(SILENCE)temp.bat
	$(SILENCE)del /f temp.bat
	$(SILENCE)del /f temp1.txt
	$(SILENCE)$(CC) $(CFLAGS) -ppd=$*.e -ppa $<
	$(SILENCE)sed -e s/\//\\/g *.e > $*.d
	$(SILENCE)sed -e s/.*obj:\s// *.d > $*.p
	$(SILENCE)del /f $*.e

%.obj: %.asm
	$(SILENCE)$(CC) $(CFLAGS) $<

%.obj: %.sa
	$(SILENCE)$(CC) $(CFLAGS) $<

# include generated dependency files
ifneq ("","${OBJS}")
-include $(OBJS:.obj=.d)
#-include $(OBJS:.obj=.p)
endif

.PHONY: install $(INSTALLDIR)

install: ${LIBNAME}.lib | $(INSTALLDIR)
	$(SILENCE)copy ${LIBNAME}.lib $(INSTALLDIR)

$(INSTALLDIR):
	if exist $@ (echo $@ exist) ELSE (mkdir $@)
