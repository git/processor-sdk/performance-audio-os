
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Hidden Jump Table for PA/F on IROM -- Declarations
//
//
//

#ifndef PAFHJT_H_
#define PAFHJT_H_

// -----------------------------------------------------------------------------

// 0xDD000000 = DM64xx
#if (PAF_DEVICE&0xFF000000) == 0xDD000000

#include <std.h>

#include <com_tii_priv.h>

#include <cpl_vecops.h>
#include <cpl_cdm.h>
#include <cpl_fft.h>
#include <cpl_dec.h>
#include <cpl_moddemod.h>
#include <cpl_window.h>
#include <cpl_fifo.h>
#include <cpl_cdm_fxns.h>
#include <cpl_delay.h>
#include <csl_dat.h>

#define dmaxHandle          NULL
#define dMAX_Handle         int
#define CPL_CALL(x) CPL_##x

typedef struct PAFHJT_t {

    Int (*setAudioFrame) (IALG_Handle handle, PAF_AudioFrame *pAudioFrame, Int cnt, PAF_DecodeStatus * pDecodeStatus);
	void (*fft) (int n, float * ptr_x, const float * ptr_w, float * ptr_y, const unsigned char * brev, int n_min, int offset, int n_max);
	void (*Modulation_new) ( float *input,float *off_inp, float *restrict output,int n,const float *costable , const float *sintable,int off);
	void (*Demodulation_new) (float *input, float *restrict output,int n, const float *costable, const float *sintable, int sign , int off, int flag);
	void (*window_aac_ac3) (const float *dnmixptr1,const float *delayptr1, const float *win,int N,int mul, float *restrict pcmptr1,float *restrict pcmptr2, int sign );

	// COM ASP
	
	Int (*snatchCommon) (IALG_Handle handle, PAF_IALG_Common * common);
	Int (*activateCommon) (IALG_Handle handle, PAF_IALG_Common * common);
	Void (*deactivateCommon) (IALG_Handle handle, PAF_IALG_Common * common);

	// COM DEC

	void (*pCPL_cdm_downmixSetUp) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCPL_cdm_samsiz) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCPL_cdm_downmixApply) (void *pv, PAF_AudioData * pIn[], PAF_AudioData * pOut[], CPL_CDM * pCDMConfig, int sampleCount);
	void (*pCPL_cdm_downmixConfig) (void *pv, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Mono) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Stereo) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Phantom1) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Phantom2) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Phantom3) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Phantom4) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_3Stereo) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Surround1) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Surround2) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Surround3) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Surround4) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);

	void (*vecAdd) (float *pIn1, float *pIn2, float *pOut, int n);
	void (*vecLinear2) (float c1, float *pIn1, float c2, float *pIn2, float *pOut, int n);
	void (*vecScale) (float c, float *pIn, float *pOut, int n);
	void (*vecSet) (float val, float *pOut, int n);
	void (*ivecCopy) (int *pIn, int *pOut, int n);
	void (*svecSet) (int val, short *pOut, int n);
	void (*ivecSet) (int val, int *pOut, int n);
	void (*imaskScale) (int mask, float scale, int *restrict pIn, float *restrict pOut, int stride, int n);
	void (*smaskScale) (int mask, float scale, short *restrict pIn, float *restrict pOut, int stride, int n);
	void (*vecStrideScale) (float scale, float *restrict pIn, float *restrict pOut, int stride, int n);

	void (*vecLinear2Incr) (float c1, float Incr, float *restrict pIn1, float c2, float *restrict pIn2, float *restrict pOut, int nSamples);
	void (*vecLinear2Common) (float c, float *restrict pIn1, float *restrict pIn2, float *restrict pOut, int nSamples);
	void (*vecScaleAcc) (float c, float *restrict pIn, float *restrict pOut, int n);
	void (*vecScaleIncr) (float c, float Incr, float *restrict pIn, float *restrict pOut, int n);
	void (*vecSetArr) (float val, float **restrict pOut, int n, int a);
	float (*GetPAFSampleRate) (int sampRateEnum);
	int (*GetNumSat) (int config);
	void (*intDelayProc) (WaDelay *d, float *input, float *output, int n);
	void (*floatDelayProc) (WaDelay *d, float *input, float *output, int n);
	void (*DelaySet) (WaDelay *d, int delay);
	void (*DelayInit) (WaDelay *d, int type, int max_delay, int delay);
	void (*sumDiff) (float *p1, float *p2, int n);
	int (*delay) (PAF_AudioFrame * restrict pAudioFrame, PAF_DelayState * restrict state, Int apply, Int channel, Int unit);

	int (*fifoInit) (CPL_Fifo * pFifo);
	int (*fifoSize) (const CPL_Fifo *pFifo);
	int (*fifoSizeFull) (const CPL_Fifo *pFifo);
	int (*fifoSizeFree) (const CPL_Fifo *pFifo);
	int (*fifoSizeFullLinear) (const CPL_Fifo *pFifo);
	int (*fifoSizeFreeLinear) (const CPL_Fifo *pFifo);
	int (*fifoRead) (CPL_Fifo * pFifo, void *pBuf, int size);
	int (*fifoWrite) (CPL_Fifo * pFifo, void *pBuf, int size);
	int (*fifoReadNoPosMv) (CPL_Fifo * pFifo, void *pBuf, int size);
	int (*fifoGetReadPtr) (CPL_Fifo * pFifo, int size, void **p);
	int (*fifoReadDone) (CPL_Fifo * pFifo, int size);
	int (*fifoGetWritePtr) (CPL_Fifo * pFifo, int size, void **p);
	int (*fifoWrote) (CPL_Fifo * pFifo, int size);
	int (*fifoMoveReadLocation) (CPL_Fifo * pFifo, int location);
	void (*rotateLeft) (unsigned char * restrict p,int circ_size, int offset, int size, int rotate);
	void (*rotateRight) (unsigned char * restrict p,int circ_size, int offset, int size, int rotate);

	Uint32 (*DAT_copy) (void *src, void *dst, Uint16 cnt);
	void (*DAT_wait) (Uint32 eventid);

} PAFHJT_t;

extern const PAFHJT_t PAFHJT_RAM;
#ifdef ARMCOMPILE
extern const PAFHJT_t *pafhjt;
#else
extern far const PAFHJT_t *pafhjt;
#endif
#if !defined(PAFHJT_DISABLE_INDIRECTION)

// Since this is used to as an initializer (e.g. dwr_pcm.c) then can't be an indirection
// even though both are const
//#define CPL_setAudioFrame		pafhjt->setAudioFrame
#define CPL_setAudioFrame		CPL_setAudioFrame_

#define CPL_fft					pafhjt->fft
#define CPL_Modulation_new		pafhjt->Modulation_new
#define CPL_Demodulation_new	pafhjt->Demodulation_new
#define CPL_window_aac_ac3		pafhjt->window_aac_ac3

#define COM_TII_snatchCommon		pafhjt->snatchCommon
#define COM_TII_activateCommon		pafhjt->activateCommon
#define COM_TII_deactivateCommon	pafhjt->deactivateCommon

#define CPL_cdm_downmixSetUp	pafhjt->pCPL_cdm_downmixSetUp
#define CPL_cdm_samsiz			pafhjt->pCPL_cdm_samsiz
#define CPL_cdm_downmixApply	pafhjt->pCPL_cdm_downmixApply
#define CPL_cdm_downmixConfig	pafhjt->pCPL_cdm_downmixConfig
#define CDMTo_Mono				pafhjt->pCDMTo_Mono
#define CDMTo_Stereo			pafhjt->pCDMTo_Stereo
#define CDMTo_Phantom1			pafhjt->pCDMTo_Phantom1
#define CDMTo_Phantom2			pafhjt->pCDMTo_Phantom2
#define CDMTo_Phantom3			pafhjt->pCDMTo_Phantom3
#define CDMTo_Phantom4			pafhjt->pCDMTo_Phantom4
#define CDMTo_3Stereo			pafhjt->pCDMTo_3Stereo
#define CDMTo_Surround1			pafhjt->pCDMTo_Surround1
#define CDMTo_Surround2			pafhjt->pCDMTo_Surround2
#define CDMTo_Surround3			pafhjt->pCDMTo_Surround3
#define CDMTo_Surround4			pafhjt->pCDMTo_Surround4

#define CPL_vecAdd				pafhjt->vecAdd
#define CPL_vecLinear2			pafhjt->vecLinear2
#define CPL_vecScale			pafhjt->vecScale
#define CPL_vecSet				pafhjt->vecSet
#define CPL_ivecCopy			pafhjt->ivecCopy
#define CPL_svecSet				pafhjt->svecSet
#define CPL_ivecSet				pafhjt->ivecSet
#define CPL_imaskScale			pafhjt->imaskScale
#define CPL_smaskScale			pafhjt->smaskScale
#define CPL_vecStrideScale		pafhjt->vecStrideScale

#define CPL_vecLinear2Incr		pafhjt->vecLinear2Incr
#define CPL_vecLinear2Common	pafhjt->vecLinear2Common
#define CPL_vecScaleAcc			pafhjt->vecScaleAcc
#define CPL_vecScaleIncr		pafhjt->vecScaleIncr
#define CPL_vecSetArr			pafhjt->vecSetArr
#define CPL_GetPAFSampleRate	pafhjt->GetPAFSampleRate
#define CPL_GetNumSat			pafhjt->GetNumSat
#define CPL_intDelayProc		pafhjt->intDelayProc
#define CPL_floatDelayProc		pafhjt->floatDelayProc
#define CPL_DelaySet			pafhjt->DelaySet
#define CPL_DelayInit			pafhjt->DelayInit
#define CPL_sumDiff				pafhjt->sumDiff
#define CPL_delay				pafhjt->delay

#define CPL_fifoInit				pafhjt->fifoInit            
#define CPL_fifoSize              	pafhjt->fifoSize            
#define CPL_fifoSizeFull          	pafhjt->fifoSizeFull        
#define CPL_fifoSizeFree          	pafhjt->fifoSizeFree        
#define CPL_fifoSizeFullLinear    	pafhjt->fifoSizeFullLinear  
#define CPL_fifoSizeFreeLinear    	pafhjt->fifoSizeFreeLinear  
#define CPL_fifoRead              	pafhjt->fifoRead            
#define CPL_fifoWrite             	pafhjt->fifoWrite           
#define CPL_fifoReadNoPosMv       	pafhjt->fifoReadNoPosMv     
#define CPL_fifoGetReadPtr        	pafhjt->fifoGetReadPtr      
#define CPL_fifoReadDone          	pafhjt->fifoReadDone        
#define CPL_fifoGetWritePtr       	pafhjt->fifoGetWritePtr     
#define CPL_fifoWrote             	pafhjt->fifoWrote           
#define CPL_fifoMoveReadLocation  	pafhjt->fifoMoveReadLocation
#define CPL_rotateLeft				pafhjt->rotateLeft
#define CPL_rotateRight				pafhjt->rotateRight

#define DAT_copy				pafhjt->DAT_copy
#define DAT_wait				pafhjt->DAT_wait
// // why is there a case difference? used by DIB
#define DAT_Copy				pafhjt->DAT_copy
#define DAT_Wait				pafhjt->DAT_wait

#endif /* PAFHJT_DISABLE_INDIRECTION */

// -----------------------------------------------------------------------------

#elif ((PAF_DEVICE&0xFF000000) == 0xD8000000) || ((PAF_DEVICE&0xFF000000) == 0xDA000000)

// BIOS include files (required for Int, etc)
#include <xdc/std.h>

// COM ASP include files
#include <com_tii_priv.h>

// COM DEC include files
#include <cpl_vecops.h>
#include <cpl_cdm.h>
#include <cpl_fft.h>
#include <cpl_dec.h>
#include <cpl_moddemod.h>
#include <cpl_window.h>
#include <cpl_fifo.h>
#include <cpl_cdm_fxns.h>
#include <cpl_delay.h>
//#include <csl_dat.h>

typedef struct PAFHJT_t {

	Int (*setAudioFrame) (IALG_Handle handle, PAF_AudioFrame *pAudioFrame, Int cnt, PAF_DecodeStatus * pDecodeStatus);
	void (*fft) (int n, float * ptr_x, const float * ptr_w, float * ptr_y, const unsigned char * brev, int n_min, int offset, int n_max);
	void (*Modulation_new) ( float *input,float *off_inp, float *restrict output,int n,const float *costable , const float *sintable,int off);
	void (*Demodulation_new) (float *input, float *restrict output,int n, const float *costable, const float *sintable, int sign , int off, int flag);
	void (*window_aac_ac3) (const float *dnmixptr1,const float *delayptr1, const float *win,int N,int mul, float *restrict pcmptr1,float *restrict pcmptr2, int sign );

	// COM ASP
	
	Int (*snatchCommon) (IALG_Handle handle, PAF_IALG_Common * common);
	Int (*activateCommon) (IALG_Handle handle, PAF_IALG_Common * common);
	Void (*deactivateCommon) (IALG_Handle handle, PAF_IALG_Common * common);

	// COM DEC

	void (*pCPL_cdm_downmixSetUp) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCPL_cdm_samsiz) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCPL_cdm_downmixApply) (void *pv, PAF_AudioData * pIn[], PAF_AudioData * pOut[], CPL_CDM * pCDMConfig, int sampleCount);
	void (*pCPL_cdm_downmixConfig) (void *pv, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Mono) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Stereo) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Phantom1) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Phantom2) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Phantom3) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Phantom4) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_3Stereo) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Surround1) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Surround2) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Surround3) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);
	void (*pCDMTo_Surround4) (void *pv, PAF_AudioFrame * pAudioFrame, CPL_CDM * pCDMConfig);

	void (*vecAdd) (float *pIn1, float *pIn2, float *pOut, int n);
	void (*vecLinear2) (float c1, float *pIn1, float c2, float *pIn2, float *pOut, int n);
	void (*vecScale) (float c, float *pIn, float *pOut, int n);
	void (*vecSet) (float val, float *pOut, int n);
	void (*ivecCopy) (int *pIn, int *pOut, int n);
	void (*svecSet) (int val, short *pOut, int n);
	void (*ivecSet) (int val, int *pOut, int n);
	void (*imaskScale) (int mask, float scale, int *restrict pIn, float *restrict pOut, int stride, int n);
	void (*smaskScale) (int mask, float scale, short *restrict pIn, float *restrict pOut, int stride, int n);
	void (*vecStrideScale) (float scale, float *restrict pIn, float *restrict pOut, int stride, int n);

	void (*vecLinear2Incr) (float c1, float Incr, float *restrict pIn1, float c2, float *restrict pIn2, float *restrict pOut, int nSamples);
	void (*vecLinear2Common) (float c, float *restrict pIn1, float *restrict pIn2, float *restrict pOut, int nSamples);
	void (*vecScaleAcc) (float c, float *restrict pIn, float *restrict pOut, int n);
	void (*vecScaleIncr) (float c, float Incr, float *restrict pIn, float *restrict pOut, int n);
	void (*vecSetArr) (float val, float **restrict pOut, int n, int a);
	float (*GetPAFSampleRate) (int);
	int (*GetNumSat) (int);
	void (*intDelayProc) (WaDelay *, float *, float *, int);
	void (*floatDelayProc) (WaDelay *, float *, float *, int);
	void (*DelaySet) (WaDelay *, int);
	void (*DelayInit) (WaDelay *, int, int, int);
	void (*sumDiff) (float *p1, float *p2, int n);
	int (*delay) (PAF_AudioFrame * restrict pAudioFrame, PAF_DelayState * restrict state, Int apply, Int channel, Int unit);

	int (*fifoInit) (CPL_Fifo * pFifo);
	int (*fifoSize) (const CPL_Fifo *pFifo);
	int (*fifoSizeFull) (const CPL_Fifo *pFifo);
	int (*fifoSizeFree) (const CPL_Fifo *pFifo);
	int (*fifoSizeFullLinear) (const CPL_Fifo *pFifo);
	int (*fifoSizeFreeLinear) (const CPL_Fifo *pFifo);
	int (*fifoRead) (CPL_Fifo * pFifo, void *pBuf, int size);
	int (*fifoWrite) (CPL_Fifo * pFifo, void *pBuf, int size);
	int (*fifoReadNoPosMv) (CPL_Fifo * pFifo, void *pBuf, int size);
	int (*fifoGetReadPtr) (CPL_Fifo * pFifo, int size, void **p);
	int (*fifoReadDone) (CPL_Fifo * pFifo, int size);
	int (*fifoGetWritePtr) (CPL_Fifo * pFifo, int size, void **p);
	int (*fifoWrote) (CPL_Fifo * pFifo, int size);
	int (*fifoMoveReadLocation) (CPL_Fifo * pFifo, int location);
	void (*rotateLeft) (unsigned char * restrict p,int circ_size, int offset, int size, int rotate);
	void (*rotateRight) (unsigned char * restrict p,int circ_size, int offset, int size, int rotate);

	Uint32 (*DAT_copy) (void *src, void *dst, Uint16 cnt);
	void (*DAT_wait) (Uint32 eventid);

	// BIOS

	// CSL

	// dMAX

	// EDMA

} PAFHJT_t;

#if defined(ROM_BUILD)
extern const PAFHJT_t PAFHJT;
#else /* ROM_BUILD */
extern const PAFHJT_t PAFHJT_RAM;
#endif /* ROM_BUILD */
#ifdef ARMCOMPILE
extern const PAFHJT_t *pafhjt;
#else
extern far const PAFHJT_t *pafhjt;
#endif

#define CPL_CALL(x) CPL_##x

#if !defined(PAFHJT_DISABLE_INDIRECTION)

#define CPL_setAudioFrame		pafhjt->setAudioFrame
#define CPL_fft					pafhjt->fft
#define CPL_Modulation_new		pafhjt->Modulation_new
#define CPL_Demodulation_new	pafhjt->Demodulation_new
#define CPL_window_aac_ac3		pafhjt->window_aac_ac3

#define COM_TII_snatchCommon		pafhjt->snatchCommon
#define COM_TII_activateCommon		pafhjt->activateCommon
#define COM_TII_deactivateCommon	pafhjt->deactivateCommon

#define CPL_cdm_downmixSetUp	pafhjt->pCPL_cdm_downmixSetUp
#define CPL_cdm_samsiz			pafhjt->pCPL_cdm_samsiz
#define CPL_cdm_downmixApply	pafhjt->pCPL_cdm_downmixApply
#define CPL_cdm_downmixConfig	pafhjt->pCPL_cdm_downmixConfig
#define CDMTo_Mono				pafhjt->pCDMTo_Mono
#define CDMTo_Stereo			pafhjt->pCDMTo_Stereo
#define CDMTo_Phantom1			pafhjt->pCDMTo_Phantom1
#define CDMTo_Phantom2			pafhjt->pCDMTo_Phantom2
#define CDMTo_Phantom3			pafhjt->pCDMTo_Phantom3
#define CDMTo_Phantom4			pafhjt->pCDMTo_Phantom4
#define CDMTo_3Stereo			pafhjt->pCDMTo_3Stereo
#define CDMTo_Surround1			pafhjt->pCDMTo_Surround1
#define CDMTo_Surround2			pafhjt->pCDMTo_Surround2
#define CDMTo_Surround3			pafhjt->pCDMTo_Surround3
#define CDMTo_Surround4			pafhjt->pCDMTo_Surround4

#define CPL_vecAdd				pafhjt->vecAdd
#define CPL_vecLinear2			pafhjt->vecLinear2
#define CPL_vecScale			pafhjt->vecScale
#define CPL_vecSet				pafhjt->vecSet
#define CPL_ivecCopy			pafhjt->ivecCopy
#define CPL_svecSet				pafhjt->svecSet
#define CPL_ivecSet				pafhjt->ivecSet
#define CPL_imaskScale			pafhjt->imaskScale
#define CPL_smaskScale			pafhjt->smaskScale
#define CPL_vecStrideScale		pafhjt->vecStrideScale

#define CPL_vecLinear2Incr		pafhjt->vecLinear2Incr
#define CPL_vecLinear2Common	pafhjt->vecLinear2Common
#define CPL_vecScaleAcc			pafhjt->vecScaleAcc
#define CPL_vecScaleIncr		pafhjt->vecScaleIncr
#define CPL_vecSetArr			pafhjt->vecSetArr
#define CPL_GetPAFSampleRate	pafhjt->GetPAFSampleRate
#define CPL_GetNumSat			pafhjt->GetNumSat
#define CPL_intDelayProc		pafhjt->intDelayProc
#define CPL_floatDelayProc		pafhjt->floatDelayProc
#define CPL_DelaySet			pafhjt->DelaySet
#define CPL_DelayInit			pafhjt->DelayInit
#define CPL_sumDiff				pafhjt->sumDiff
#define CPL_delay				pafhjt->delay

#define CPL_fifoInit				pafhjt->fifoInit            
#define CPL_fifoSize              	pafhjt->fifoSize            
#define CPL_fifoSizeFull          	pafhjt->fifoSizeFull        
#define CPL_fifoSizeFree          	pafhjt->fifoSizeFree        
#define CPL_fifoSizeFullLinear    	pafhjt->fifoSizeFullLinear  
#define CPL_fifoSizeFreeLinear    	pafhjt->fifoSizeFreeLinear  
#define CPL_fifoRead              	pafhjt->fifoRead            
#define CPL_fifoWrite             	pafhjt->fifoWrite           
#define CPL_fifoReadNoPosMv       	pafhjt->fifoReadNoPosMv     
#define CPL_fifoGetReadPtr        	pafhjt->fifoGetReadPtr      
#define CPL_fifoReadDone          	pafhjt->fifoReadDone        
#define CPL_fifoGetWritePtr       	pafhjt->fifoGetWritePtr     
#define CPL_fifoWrote             	pafhjt->fifoWrote           
#define CPL_fifoMoveReadLocation  	pafhjt->fifoMoveReadLocation
#define CPL_rotateLeft				pafhjt->rotateLeft
#define CPL_rotateRight				pafhjt->rotateRight

#define DAT_copy				pafhjt->DAT_copy
#define DAT_wait				pafhjt->DAT_wait

#endif /* PAFHJT_DISABLE_INDIRECTION */

#else  // PAF_DEVICE = 0xD8000000 ---------------------------------------------

//#include <std.h>
//#include <clk.h>
//#include <dev.h>
//#include <knl.h>
//#include <hwi.h>
//#include <log.h>
//#include <mem.h>
//#include <que.h>
//#include <sem.h>
//#include <swi.h>
//#include <sio.h>
//#include <tsk.h>
#if defined(PAF_IROM_BUILD) && (PAF_IROM_BUILD == 0xD710E001)
// FIXME: Bad choice of build control
//  gbl.h is applicable to all builds that use BIOS 5.xx
//  Is there a version symbol defined in one of the BIOS
//  headers (std.h?) that we can use?
#   include <gbl.h>
#   include <clk.h>
#endif
#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFFFF0000) == 0xD7100000)
#ifndef dMAX_CFG
#   include <dmax.h>
#else
#include <dmax_struct.h>
#include <dmax_params.h>
#endif /* dMAX_CFG */
#endif
//#include <pafcsl.h>
#include <csl.h>
#include <csl_cache.h>
#include <csl_edma.h>
//#include <csl_mcbsp.h>
#include <csl_mcasp.h>
#include <csl_timer.h>      
#include <csl_gpio.h>
#include <stdlib.h>

#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)

#if !defined(PAF_SUPPORT_HJT) && PAF_IROM_BUILD == 0xD610A003
#   define PAF_SUPPORT_HJT
#endif

#if !defined(PAF_SUPPORT_HJT) && PAF_DEVICE_VERSION == 0xA004
#   define PAF_SUPPORT_HJT
#endif

#if !defined(PAF_SUPPORT_HJT) && PAF_IROM_BUILD == 0xD710E001
#   define PAF_SUPPORT_HJT
#   ifndef ANTARA_BUILD
#       define ANTARA_BUILD
#   endif
#endif

#ifndef FAR_SYMBOL
#define FAR_SYMBOL far
#endif

#define COM_TII_snatchCommon		COM_TII_snatchCommon_
#define COM_TII_activateCommon		COM_TII_activateCommon_
#define COM_TII_deactivateCommon	COM_TII_deactivateCommon_

typedef struct PAF_HJT_fxns {
    LgUns       (*pCLK_countspms)(Void);
    LgUns       (*pCLK_gethtime)(Void);
    LgUns       (*pCLK_getprd)(Void);
    String		(*pDEV_match)(String name, DEV_Device **driver);
    DEV_Frame	*(*pDEV_mkframe)(Int segid, Uns size, Uns align);
    Void		(*pDEV_rmframe)(DEV_Frame *frame, Int segid, Uns size);
    Uns         (*pHWI_disable)(Void);
    Void        (*pHWI_dispatchPlug)(Int vecid, Fxn fxn, Int dmachan, HWI_Attrs *attrs);
    Void        (*pHWI_restore)(Uns old);
    Ptr	        (*pMEM_alloc)(Int segid, Uns size, Uns align);
    Int	        (*pMEM_define)(Ptr base, Uns length, MEM_Attrs *attrs);
    Bool	    (*pMEM_free)(Int segid, Ptr addr, Uns size);
    Void	    (*pMEM_redefine)(Int segid, Ptr base, Uns length);
    Ptr	        (*pMEM_valloc)(Int segid, Uns size, Uns align, Char val);
    QUE_Handle  (*pQUE_create)(QUE_Attrs *attrs);
    Ptr         (*pQUE_get)(QUE_Handle queue);
    Void        (*pQUE_put)(QUE_Handle queue, Ptr elem);
    SEM_Handle  (*pSEM_create)(Int, SEM_Attrs *);
    Void        (*pSEM_delete)(SEM_Handle sem);
    Void        (*pSEM_ipost)(SEM_Handle sem);
    Bool        (*pSEM_pend)(SEM_Handle sem, Uns timeout);
    Void        (*pSEM_post)(SEM_Handle sem);
    SIO_Handle  (*pSIO_create)(String name, Int mode, Uns size, SIO_Attrs *attrs);
    Int         (*pSIO_delete)(SIO_Handle stream);
    Int         (*p_SIO_idle)(SIO_Handle stream, Bool flush);
    Int         (*pSIO_issue)(SIO_Handle stream, Ptr pbuf, Uns nbytes, Arg arg);
    Int         (*pSIO_reclaim)(SIO_Handle stream, Ptr *ppbuf, Arg *parg);
    SWI_Handle  (*pSWI_create)(SWI_Attrs *attrs);
    Void        (*pSWI_delete)(SWI_Handle swihandle);
    Void	    (*pSWI_disable)(Void);
    Void	    (*pSWI_enable)(Void);
    TSK_Handle  (*pTSK_create)(Fxn fxn, TSK_Attrs *attrs, ...);
    Void        (*pTSK_deltatime)(TSK_Handle task);
    Void        (*pTSK_disable)(void);
    Void        (*pTSK_enable)(void);
    Int         (*pTSK_setpri)(TSK_Handle task, Int newpri);
    Void        (*pTSK_settime)(TSK_Handle task);
    Void 		(*pSYS_error)(String s, Int errno, ...);
    _CODE_ACCESS void  *(*pcalloc)(size_t _num, size_t _size);
    _CODE_ACCESS void  *(*pmalloc)(size_t _size);
    _CODE_ACCESS void  *(*prealloc)(void *_ptr, size_t _size);
    _CODE_ACCESS void   (*pfree)(void *_ptr);
    _CODE_ACCESS void  *(*pmemalign)(size_t _aln, size_t _size);
    
    
    //Data structures.
    KNL_Handle *pKNL_curtask;
    volatile Uns *pKNL_curtime;
    SEM_Obj	*pTSK_timerSem; 
    
    // CSL Calls
    void 		(* pCSLDA610_LIB_)();
    void 		(* p_CSL_init)(_CSL_Config *config);
    CACHE_L2Mode (* pCACHE_setL2Mode)(CACHE_L2Mode newMode);
    void 		(* pEDMA_intFree)(int tcc);
    void 		(* pEDMA_reset)(EDMA_Handle hEdma);
    void 		(* pCACHE_flush)(CACHE_Region region,void *addr,Uint32 wordCnt);
    void 		(* pEDMA_close)(EDMA_Handle hEdma);
    void 		(* pIRQ_map)(Uint32 eventId, Uint32 intNumber);
    void 		(* pCACHE_clean)(CACHE_Region region,void *addr,Uint32 wordCnt);
    MCASP_Handle (* pMCASP_open)(int devNum, Uint32 flags);
    int 		(* pEDMA_allocTableEx)(int cnt, EDMA_Handle *array);
    int 		(* pEDMA_map)(int eventNum,int chaNum);
    void 		(* pMCBSP_reset)(MCBSP_Handle hMcbsp);
    void 		(* pMCBSP_close)(MCBSP_Handle hMcbsp);
    MCBSP_Handle (* pMCBSP_open)(int devNum, Uint32 flags);
    Uint32 		(* pMCBSP_getPins)(MCBSP_Handle hMcbsp);
    void 		(* pTIMER_close)(TIMER_Handle hTimer);
    TIMER_Handle (* pTIMER_open)(int devNum, Uint32 flags);
    EDMA_Handle (* pEDMA_allocTable)(int tableNum);
    int 		(* pEDMA_intAlloc)(int tcc);
    EDMA_Handle (* pEDMA_open)(int chaNum, Uint32 flags);
	GPIO_Handle (* pGPIO_open)(int devnum, Uint32 flags); 
	void 		(* pEDMA_freeTableEx)(int cnt, EDMA_Handle *array); 
	//CSL Data structures
	Uint32 * p_IRQ_eventTable;            
    
#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFFFF0000) == 0xD7100000)
    // Indirection added for Antara (D710E001)
    void (*pBIOS_init)(void);
    void (*pBIOS_start)(void);
    void (*pIDL_loop)(void);
    void (*pCLK_reconfig)(void);
    Uint32  *pGBL_freq;
#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFFFF0000) == 0xD7100000)
    dMAX_Obj *pdmaxHandle;
#else /* (PAF_DEVICE) && ((PAF_DEVICE&0xFFFF0000) == 0xD7100000) */
    void *dummy1;
#endif
    Void (*pLOG_printf)(LOG_Handle log, String format, ...);
#endif /* PAF_DEVICE = 0xD710E000 */
} PAF_HJT_fxns;
extern FAR_SYMBOL const PAF_HJT_fxns *hjtFxns;
#ifdef PAF_SUPPORT_HJT
#ifndef PAF_HJT_ORIG /* To turn of pre-proc during table definition */
#define CLK_countspms       hjtFxns->pCLK_countspms
#define CLK_gethtime        hjtFxns->pCLK_gethtime
#define CLK_getprd          hjtFxns->pCLK_getprd
#define DEV_match           hjtFxns->pDEV_match
#define DEV_mkframe         hjtFxns->pDEV_mkframe
#define DEV_rmframe         hjtFxns->pDEV_rmframe
#define HWI_disable         hjtFxns->pHWI_disable   
#define HWI_dispatchPlug    hjtFxns->pHWI_dispatchPlug
#define HWI_restore         hjtFxns->pHWI_restore
#define MEM_alloc           hjtFxns->pMEM_alloc
#define MEM_define          hjtFxns->pMEM_define
#define MEM_free            hjtFxns->pMEM_free
#define MEM_redefine        hjtFxns->pMEM_redefine
#define MEM_valloc          hjtFxns->pMEM_valloc
#define QUE_create          hjtFxns->pQUE_create
#define QUE_get             hjtFxns->pQUE_get
#define QUE_put             hjtFxns->pQUE_put
#define SEM_create          hjtFxns->pSEM_create
#define SEM_delete          hjtFxns->pSEM_delete
#define SEM_ipost           hjtFxns->pSEM_ipost
#define SEM_pend            hjtFxns->pSEM_pend
#define SEM_post            hjtFxns->pSEM_post
#define SIO_create          hjtFxns->pSIO_create
#define SIO_delete          hjtFxns->pSIO_delete
#define _SIO_idle           hjtFxns->p_SIO_idle 
#define SIO_issue           hjtFxns->pSIO_issue
#define SIO_reclaim         hjtFxns->pSIO_reclaim
#define SWI_create          hjtFxns->pSWI_create
#define SWI_delete          hjtFxns->pSWI_delete
#define SWI_disable         hjtFxns->pSWI_disable
#define SWI_enable          hjtFxns->pSWI_enable
#define TSK_create          hjtFxns->pTSK_create
#define TSK_deltatime       hjtFxns->pTSK_deltatime
#define TSK_disable         hjtFxns->pTSK_disable
#define TSK_enable          hjtFxns->pTSK_enable
#define TSK_setpri          hjtFxns->pTSK_setpri
#define TSK_settime         hjtFxns->pTSK_settime
#define SYS_error           hjtFxns->pSYS_error
// Note that for ROM4 with IBIOS, these functions are not there in
// IROM and so if pulled, they will be pulled from RTS and will lead
// to allocation failure as memory pool .sysmem not defined.
// For ROM4 with IBIOS, these functions are NULL, and hence
// with ROM4 with IBIOS, these functions shouldn't be used.
#if PAF_DEVICE_VERSION == 0xA004 
#define calloc              hjtFxns->pcalloc
#define malloc              hjtFxns->pmalloc
#define realloc             hjtFxns->prealloc
#define free                hjtFxns->pfree
#define memalign            hjtFxns->pmemalign
#endif

#define KNL_curtask			(*(hjtFxns->pKNL_curtask))
#define KNL_curtime         (*(hjtFxns->pKNL_curtime))
#define TSK_timerSem        (*(hjtFxns->pTSK_timerSem))

// CSL Calls
#define CSLDA610_LIB_	    hjtFxns->pCSLDA610_LIB_
#define _CSL_init	        hjtFxns->p_CSL_init
#define CACHE_setL2Mode	    hjtFxns->pCACHE_setL2Mode
#define CACHE_flush	        hjtFxns->pCACHE_flush
#define CACHE_clean	        hjtFxns->pCACHE_clean
#define EDMA_close	        hjtFxns->pEDMA_close
#define EDMA_intFree	    hjtFxns->pEDMA_intFree
#define EDMA_reset	        hjtFxns->pEDMA_reset
#define EDMA_allocTableEx	hjtFxns->pEDMA_allocTableEx
#define EDMA_map	        hjtFxns->pEDMA_map
#define EDMA_allocTable	    hjtFxns->pEDMA_allocTable
#define EDMA_intAlloc	    hjtFxns->pEDMA_intAlloc
#define EDMA_open	    	hjtFxns->pEDMA_open 
#define IRQ_map	            hjtFxns->pIRQ_map
#define MCASP_open	        hjtFxns->pMCASP_open
#define MCBSP_reset	    	hjtFxns->pMCBSP_reset
#define MCBSP_close	    	hjtFxns->pMCBSP_close
#define MCBSP_open	    	hjtFxns->pMCBSP_open
#define MCBSP_getPins	    hjtFxns->pMCBSP_getPins
#define TIMER_close	    	hjtFxns->pTIMER_close
#define TIMER_open	    	hjtFxns->pTIMER_open
#define GPIO_open			hjtFxns->pGPIO_open
#define EDMA_freeTableEx			hjtFxns->pEDMA_freeTableEx

//CSL Data structures
#define _IRQ_eventTable	    hjtFxns->p_IRQ_eventTable

#if defined(PAF_DEVICE) && ((PAF_DEVICE&0xFFFF0000) == 0xD7100000)
// Indirection added for Antara (D710E001)
#define BIOS_init           hjtFxns->pBIOS_init
#define BIOS_start          hjtFxns->pBIOS_start
#define IDL_loop            hjtFxns->pIDL_loop
#define CLK_reconfig        hjtFxns->pCLK_reconfig
#define GBL_freq            (*(hjtFxns->pGBL_freq))
#define dmaxHandle          hjtFxns->pdmaxHandle
#endif /* PAF_DEVICE = 0xD710E000 */

#if defined(PAF_IROM_BUILD) && (PAF_IROM_BUILD == 0xD710E001)
// LOG_printf is being used in Antara ROM1
#define LOG_printf          hjtFxns->pLOG_printf
#endif

#endif /* PAF_HJT_ORIG */

#else /* PAF_SUPPORT_HJT */

#ifndef dMAX_CFG
#define dmaxHandle          (&dMAX_OBJ)
#else /* dMAX_CFG */
#define dmaxHandle          dMAX_HANDLE
#endif /* dMAX_CFG */

#endif /* PAF_SUPPORT_HJT */

#if defined(PAF_IROM_NOIBIOS) || (PAF_DEVICE_VERSION == 0xA004) || \
    (defined(PAF_IROM_BUILD) && (PAF_IROM_BUILD == 0xD710E001))
// We want IROM Code to use a different variable for 
// heapid so that this can be patched if required.
#define IRAM PA_IRAM
#define SDRAM PA_SDRAM
extern FAR_SYMBOL int PA_IRAM;
extern FAR_SYMBOL int PA_SDRAM; 

#ifndef IDEF
#   define IDEF static inline
#endif

/* The below is required because preprocessing happens before inline 
* takes effect. So this way IRQ_evenTable for the below
* function goes through indirection.
*/ 
#define IRQ_enable PAF_IRQ_enable

IDEF void PAF_IRQ_enable(Uint32 eventId) {
#if ((PAF_DEVICE == 0xD710E000) || (PAF_DEVICE == 0xD710E001))
  IRQ_map (eventId,eventId);
#endif
  IER |= _IRQ_eventTable[eventId];
}

#define IRQ_disable PAF_IRQ_disable

IDEF Uint32 PAF_IRQ_disable(Uint32 eventId) {
  Uint32 ie = IER & _IRQ_eventTable[eventId];
  IER &= ~_IRQ_eventTable[eventId];
  return ie;
}

#define  IRQ_restore  PAF_IRQ_restore
IDEF void PAF_IRQ_restore(Uint32 eventId, Uint32 ie) {
  if (ie) {
    IER |= _IRQ_eventTable[eventId];
  } else {
    IER &= ~_IRQ_eventTable[eventId];
  }
}

#define  IRQ_clear  PAF_IRQ_clear
IDEF void IRQ_clear(Uint32 eventId) {
  ICR = _IRQ_eventTable[eventId];
}

#define CSL_init PAF_CSL_init
static inline void CSL_init() {

  #if (CHIP_6711)
    CSL6711_LIB_();
  #elif (CHIP_6713)
    CSL6713_LIB_();  /*** replaced by 6713 **/
  #elif (CHIP_DA610)
    CSLDA610_LIB_();  /*** replaced by 6713 **/
  #endif

  _CSL_init((_CSL_Config*)INV);
}
// Indirection added for Antara (D710E001)
#if defined(PAF_IROM_BUILD) && (PAF_IROM_BUILD == 0xD710E001)
// FIXME: See previous FIXME comment about build control
#define GBL_getFrequency    PAF_GBL_getFrequency
static inline Uint32 PAF_GBL_getFrequency(Void)
{
    return (GBL_freq);
}
#define GBL_setFrequency    PAF_GBL_setFrequency
static inline Bool PAF_GBL_setFrequency(Uint32 frequency)
{
    GBL_freq = frequency;

    return (TRUE);
}
#endif

#endif

#endif // PAF_DEVICE = 0xD8000000 ---------------------------------------------

#endif /* PAFHJT_H_ */
