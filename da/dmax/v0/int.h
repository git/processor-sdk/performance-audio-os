
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX Interrupt generation Service Routine data structures
//
//
//

#ifndef INT_H_ 
#define INT_H_ 

#include <int.inc>
#include <tistdtypes.h>
#include <dmax_struct.h>

/* INT macros */
#define INT_MKCTRL(EDGE, SOFT)                   \
    (((EDGE) << INT_EDGE_SHFT) |                 \
     ((SOFT) << INT_SOFT_SHFT))

#define INT_MKEE(PTB, PT)                        \
    ((((PTB) & 0x000001FF) << 8) |               \
     (PT))

/* INT parameter format */
typedef struct intp {
    Uint8  ctrl;
    Uint8  bit;
    Uint8  tcint;
    Uint8  tcc;
    Uint32 add;
} intp;

/* INT API declarations */
extern Uint32 dMAX_intOpen(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                           Uint32 tcc,Uint32 tcint,Fxn fxn);
extern void dMAX_intClose(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 tcc,Uint32 tcint,Uint32 pt);
extern void dMAX_intCfgEE(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 ee);
extern void dMAX_intGen(dMAX_Handle handle,Uint32 evt,Uint32 pt,
                        intp *pint);
extern void dMAX_intNoWait(dMAX_Handle handle,Uint32 tcc);
#endif // INT_H_

