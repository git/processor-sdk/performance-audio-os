
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DA8xx SPI/I2C slave control status (DCS6) implementation
//
//
//

#include <dcs6_params.h>

const DCS6_Params_Dev_Spi DCS6_PARAMS_DEV_SPI={
    sizeof(DCS6_Params_Dev_Spi),
    /* DCS6 supports following values for character length (clen):
           DCS6_PARAMS_DEV_SPI_CLEN_8 - 8 bit character
           DCS6_PARAMS_DEV_SPI_CLEN_16 - 16 bit character */
    DCS6_PARAMS_DEV_SPI_CLEN_16,
    /* DCS6 supports following values for number of pins (np):
           DCS6_PARAMS_DEV_SPI_NP_3 - 3 pins (SIMO,SOMI,CLK)
           DCS6_PARAMS_DEV_SPI_NP_4_SCS - 4 pins (SIMO,SOMI,CLK,SCS)
           DCS6_PARAMS_DEV_SPI_NP_4_ENA - 4 pins (SIMO,SOMI,CLK,ENA)
           DCS6_PARAMS_DEV_SPI_NP_5 - 5 pins (SIMO,SOMI,CLK,SCS,ENA)
           DCS6_PARAMS_DEV_SPI_NP_5_SRDY - 5 pins (SIMO,SOMI,CLK,SCS,ENA-soft)
       The ENA pin in DCS6_PARAMS_DEV_SPI_NP_5_SRDY is configured in GPIO mode
       and the pin behavior is controlled by the watermarks */
    DCS6_PARAMS_DEV_SPI_NP_5,
    /* DCS6 supports following values for polarity (pol):
           DCS6_PARAMS_DEV_SPI_POL_LOW - CLK low inactive
           DCS6_PARAMS_DEV_SPI_POL_HIGH - CLK high inactive */
    DCS6_PARAMS_DEV_SPI_POL_HIGH,
    /* DCS6 supports following values for phase (pha):
          DCS6_PARAMS_DEV_SPI_PHA_IN - CLK no delay
          DCS6_PARAMS_DEV_SPI_PHA_OUT - CLK delay */
    DCS6_PARAMS_DEV_SPI_PHA_IN,
    /* DCS6 supports following values for ENA high impedance (enahiz):
           DCS6_PARAMS_DEV_SPI_ENAHIZ_DIS - ENA high inactive
           DCS6_PARAMS_DEV_SPI_ENAHIZ_ENA - ENA high impedance inactive */
    DCS6_PARAMS_DEV_SPI_ENAHIZ_ENA,
    /* Reserved fields */
    0,0,0,
    /* DCS6 supports following values for error mask (emask):
           DCS6_PARAMS_DEV_SPI_EMASK_OVRN - receive overrun error
           DCS6_PARAMS_DEV_SPI_EMASK_BITERR - bit receive error
           DCS6_PARAMS_DEV_SPI_EMASK_DLENERR - receive data length error
       These values can be combined together into a error mask */
    DCS6_PARAMS_DEV_SPI_EMASK_OVRN|
    DCS6_PARAMS_DEV_SPI_EMASK_BITERR|
    DCS6_PARAMS_DEV_SPI_EMASK_DLENERR,
};

const DCS6_Params_Dev_I2c DCS6_PARAMS_DEV_I2C={
    sizeof(DCS6_Params_Dev_I2c),
    /* DCS6 supports following values for I2C own/slave address (icoar):
           DCS6_PARAMS_DEV_I2C0_OA - own/slave address 0x28
           DCS6_PARAMS_DEV_I2C1_OA - own/slave address 0x29 */
    DCS6_PARAMS_DEV_I2C0_OA,
    /* Refer the I2C user guide for determining I2C prescale (icpsc) value */
    2,
    /* Refer the I2C user guide for determining I2C clock low (icclkl) value */
    6,
    /* Refer the I2C user guide for determining I2C clock high (icclkh) value */
    6,
    /* DCS6 supports following values for error mask (emask):
           DCS6_PARAMS_DEV_I2C_EMASK_OVRN - receive overrun error */
    DCS6_PARAMS_DEV_I2C_EMASK_OVRN,
};

const DCS6_Params_Dev_Uart DCS6_PARAMS_DEV_UART={
    sizeof(DCS6_Params_Dev_Uart),
    /* Refer the UART user guide for determining Uart divisor LSB latch 
           (dll) value and MSB latch (dlh) value.
       DCS6 provides the Uart divisor values for the following commonly used
           baud rates. These values are computed using UART input clock of 
           150 MHz.
           DCS6_PARAMS_DEV_UART_DIV_16X_B2400 - 2400 (16X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_16X_B4800 - 4800 (16X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_16X_B9600 - 9600 (16X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_16X_B19200 - 19200 (16X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_16X_B38400 - 38400 (16X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_16X_B57600 - 57600 (16X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_16X_B115200 - 115200 (16X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_13X_B2400 - 2400 (13X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_13X_B4800 - 4800 (13X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_13X_B9600 - 9600 (13X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_13X_B19200 - 19200 (13X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_13X_B38400 - 38400 (13X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_13X_B57600 - 57600 (13X oversampling)
           DCS6_PARAMS_DEV_UART_DIV_13X_B115200 - 115200 (13X oversampling)
       For other baud rates refer the UART user guide for information on how
           to compute the divisor values.
       The oversampling mode is configured in the UART mode definition (mdr)
           register below */
    DCS6_PARAMS_DEV_UART_DIV_16X_B19200&0xFF,
    DCS6_PARAMS_DEV_UART_DIV_16X_B19200>>8,
    /* DCS6 supports following values for UART mode definition (mdr):
           DCS6_PARAMS_DEV_UART_MDR_OSMSEL_16X - 16x over-sampling
           DCS6_PARAMS_DEV_UART_MDR_OSMSEL_13X - 13x over-sampling.
       Refer the UART user guide for determining mode definition (mdr)
           value */ 
    DCS6_PARAMS_DEV_UART_MDR_OSMSEL_16X,
    /* DCS6 supports following values for fifo level (fl):
           DCS6_PARAMS_DEV_UART_FL_4 - 4 bytes
           DCS6_PARAMS_DEV_UART_FL_8 - 8 bytes
           DCS6_PARAMS_DEV_UART_FL_14 - 14 bytes */
    DCS6_PARAMS_DEV_UART_FL_14,
    /* The ascii receive buffer size, watermarks values and the ascii transmit 
       buffer size values should be chosen depending upon the data rate, latency 
       requirements, application memory requirements, etc. 
       The ascii receive buffer size, and the ascii transmit buffer size
       values are related to the binary receive buffer size and 
       binary transmit buffer size. The ascii buffer sizes should be 
       specified using the equation below:
           xrxSize >= 2.4 * rxSize
           xtxSize >= 2.4 * txSize  */
    5120,
    512,
    1024,
    5120,
    /* DCS6 supports following values for error mask (emask):
           DCS6_PARAMS_DEV_UART_EMASK_OVRN - receive overrun error */
    DCS6_PARAMS_DEV_UART_EMASK_OVRN,
};

const DCS6_Params_Dev DCS6_PARAMS_DEV={
    /* Select the appropriate device from the following choices:
           DCS6_PARAMS_DEV_SPI - Select SPI device
           DCS6_PARAMS_DEV_I2C - Select I2C device
           DCS6_PARAMS_DEV_UART - Select UART device */
//    (Uint32)&DCS6_PARAMS_DEV_SPI,
//    (Uint32)&DCS6_PARAMS_DEV_I2C,
    (Uint32)&DCS6_PARAMS_DEV_UART,
};

#include <log.h>
extern LOG_Obj dcs6Trace;

const DCS6_Params DCS6_PARAMS={
    sizeof(DCS6_Params),
    /* DCS6 supports following values for device (dev):
           DCS6_PARAMS_DEV_SPI0 - SPI0 device
           DCS6_PARAMS_DEV_SPI1 - SPI1 device
           DCS6_PARAMS_DEV_I2C0 - I2C0 device
           DCS6_PARAMS_DEV_I2C1 - I2C1 device 
           DCS6_PARAMS_DEV_UART0 - UART0 device
           DCS6_PARAMS_DEV_UART1 - UART1 device
           DCS6_PARAMS_DEV_UART2 - UART2 device */
    DCS6_PARAMS_DEV_UART2,
    /* DCS6 supports following values for dMAX processing fill (psfil):
           DCS6_PARAMS_PSFIL_ENA - enable dMAX processing fill */
    DCS6_PARAMS_PSFIL_ENA,
    /* DCS6 supports following values for dMAX processing slc (pslc):
           DCS6_PARAMS_PSLC_DIS - disable dMAX processing slc
           DCS6_PARAMS_PSLC_ENA - enable dMAX processing slc */
    DCS6_PARAMS_PSLC_ENA,
    /* DCS6 supports following values for dMAX processing 
       type 2, 3, 4 read commands (pr):
           DCS6_PARAMS_PR_DIS - disable dMAX processing type 2,3,4 read commands
           DCS6_PARAMS_PR_ENA - enable dMAX processing type 2,3,4 read commands */
    DCS6_PARAMS_PR_DIS,
    /* DCS6 supports following values for dMAX processing 
       type 2, 3, 4 write commands (pw):
           DCS6_PARAMS_PW_DIS - disable dMAX processing type 2,3,4 write commands
           DCS6_PARAMS_PW_ENA - enable dMAX processing type 2,3,4 write commands */
    DCS6_PARAMS_PW_DIS,
    /* DCS6 supports following values for error detection and report (edr):
           DCS6_PARAMS_EDR_DIS - disable error detection and report
           DCS6_PARAMS_EDR_ENA - enable error detection and report */
    DCS6_PARAMS_EDR_ENA,
    /* Reserved fields */
    0,0,
    /* The receive buffer size, watermarks values and the transmit buffer size
       values should be chosen depending upon the data rate, latency 
       requirements, application memory requirements, etc. */
    2048,
    256,
    512,
    2048,
    /* DCS6 device parameters */
    (DCS6_Params_Dev*)&DCS6_PARAMS_DEV,
    /* DCS6 log param */
    (Void*) &dcs6Trace,
};

void DCS6_errorInit()
{
    /* Initialize the physical interface that will be used for reporting 
       error occurence to the master */
}

void DCS6_errorAssert()
{
    /* Configure the physical interface to indicate occurence of an error
       to the master */ 
}
void DCS6_errorDeassert()
{
    /* Configure the physical interface to indicate error recovery is 
       completed and the slave is ready to accept new data */
}
