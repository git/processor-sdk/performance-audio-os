
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  Vendor specific (TII) interface header for
 *  FIL-ASP Example Demonstration algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular FEB implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef FEB_TII_
#define FEB_TII_

#include <ialg.h>

#include <ifeb.h>
#include <icom.h>

/*
 *  ======== FEB_TII_exit ========
 *  Required module finalization function
 */
#define FEB_TII_exit COM_TII_exit

/*
 *  ======== FEB_TII_init ========
 *  Required module initialization function
 */
#define FEB_TII_init COM_TII_init

/*
 *  ======== FEB_TII_IALG ========
 *  TII's implementation of FEB's IALG interface
 */
extern IALG_Fxns FEB_TII_IALG; 

/*
 *  ======== FEB_TII_IFEB ========
 *  TII's implementation of FEB's IFEB interface
 */
extern const IFEB_Fxns FEB_TII_IFEB; 


/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of TII's
 *  FIL-ASP Example Demonstration algorithm. However, other
 *  implementation specific operations can also be added.
 */

/*
 *  ======== FEB_TII_Handle ========
 */
typedef struct FEB_TII_Obj *FEB_TII_Handle;

/*
 *  ======== FEB_TII_Params ========
 *  We don't add any new parameters to the standard ones defined by IFEB.
 */
typedef IFEB_Params FEB_TII_Params;

/*
 *  ======== FEB_TII_Status ========
 *  We don't add any new status to the standard one defined by IFEB.
 */
typedef IFEB_Status FEB_TII_Status;

/*
 *  ======== FEB_TII_Config ========
 *  We don't add any new config to the standard one defined by IFEB.
 */
typedef IFEB_Config FEB_TII_Config;

/*
 *  ======== FEB_TII_PARAMS ========
 *  Define our default parameters.
 */
#define FEB_TII_PARAMS   IFEB_PARAMS

/*
 *  ======== FEB_TII_create ========
 *  Create a FEB_TII instance object.
 */
extern FEB_TII_Handle FEB_TII_create(const FEB_TII_Params *params);

/*
 *  ======== FEB_TII_delete ========
 *  Delete a FEB_TII instance object.
 */
#define FEB_TII_delete COM_TII_delete

#endif  /* FEB_TII_ */
