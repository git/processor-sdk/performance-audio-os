/******************************************************************************
 * Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

//
//
// Vendor-specific (TII) Downmix algorithm
// instantiation implementation
//
//
//
#include <xdc/std.h>
#include <alg.h>
#include <ti/xdais/ialg.h>
#include <paf_ialg.h>

#include <dm.h>
#include <idm.h>
#include <dm_tii.h>
#include <dm_tii_priv.h>

/*
 *  ======== DM_TII_create ========
 */
DM_TII_Handle DM_TII_create(const DM_Params *params) 
{
    return ((Void *) PAF_ALG_create(&DM_TII_IALG, NULL, (IALG_Params *) params,
            NULL, NULL));
}

/*
 *  ======== DM_TII_delete ========
 */
/* COM_TIH_delete */

/*
 *  ======== DM_TII_exit ========
 */
/* COM_TIH_exit */

/*
 *  ======== DM_TII_init ========
 */
/* COM_TIH_init */
