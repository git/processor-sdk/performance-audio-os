
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// PA/F LOG_printf override
//
//
//

#ifndef LOGP_
#define LOGP_

#include <xdc/runtime/LoggerBuf.h>
//#include <log.h>

#define PAF_DEVICE_VERSION (PAF_DEVICE & 0xffff)

// Override LOG_printf as per build

// No LOG_printf() for IROM builds, Release components (algorithms/drivers),
// and Release system files (for use w/ NoIrom, IROM3, and IROM4)

#if defined( PAF_IROM_BUILD) \
||(!defined( _DEBUG) \
    &&(PAF_DEVICE_VERSION == 0xA000 \
    || PAF_DEVICE_VERSION == 0xA003 \
    || PAF_DEVICE_VERSION == 0xA004 \
    || PAF_DEVICE_VERSION == 0xE000 \
    || PAF_DEVICE_VERSION == 0xE001))
#define NO_LOG_PRINTF
#endif

// Extra-Special-Never-Before-Never-Again Enabling of LOG_printf for Antara ROM1
#if defined( PAF_IROM_BUILD) && (PAF_IROM_BUILD == 0xD710E001)
#   undef NO_LOG_PRINTF
#endif

#ifdef NO_LOG_PRINTF
#define LOG_printf PAF_LOG_printf

inline Void 
PAF_LOG_printf(LOG_Handle log, String format, ...)
{
}

#define trace (*(LoggerBuf_Struct *)(NULL))
//#define trace (*(LOG_Obj *)(NULL))

#else /* NO_LOG_PRINTF */
#ifdef ARMCOMPILE
extern LoggerBuf_Struct trace;
#else
extern far LoggerBuf_Struct trace;
#endif

#endif /* NO_LOG_PRINTF */

// Error handling macro definitions, line number

#define LINNO_DECL(TASK) extern volatile far int linno_##TASK
#define LINNO_DEFN(TASK) volatile far int linno_##TASK = 0
#define LINNO_RPRT(TASK,X) linno_##TASK = (X)

// Error handling macro definitions, error number (inline < extern)

#define ERRNO_DECL(TASK) \
    inline void f_errno_##TASK (int x) \
    { \
        extern volatile far int errno_##TASK, errst_##TASK; \
        errno_##TASK = x; \
        if (! errst_##TASK) errst_##TASK = x; \
    } \
    extern volatile far int errno_##TASK, errst_##TASK
#define ERRNO_DEFN(TASK) \
    /* not sure if this will work if decl+defn, but okay for now --Kurt */ \
    inline void f_errno_##TASK (int x) \
    { \
        extern volatile far int errno_##TASK, errst_##TASK; \
        errno_##TASK = x; \
        if (! errst_##TASK) errst_##TASK = x; \
    } \
    volatile far int errno_##TASK = 0, errst_##TASK = 0
#define ERRNO_RPRT(TASK,X) f_errno_##TASK (X)

#endif /* LOGP_ */
