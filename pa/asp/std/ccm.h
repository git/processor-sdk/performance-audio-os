
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Audio Stream Processing Standard Channel Configuration Mask Definitions
//
//     Since these symbols are not PA/F-specific, use of this file is to
//     be restricted -- it must be explicitly included only as needed.
//
//
//

#ifndef _CCM
#define _CCM

#include <paftyp_a.h>

#ifdef PAF_LEFT
#define mLEFT (1<<PAF_LEFT)
#else
#define mLEFT 0
#endif

#ifdef PAF_RGHT
#define mRGHT (1<<PAF_RGHT)
#else
#define mRGHT 0
#endif

#ifdef PAF_CNTR
#define mCNTR (1<<PAF_CNTR)
#else
#define mCNTR 0
#endif

#ifdef PAF_LCTR
#define mLCTR (1<<PAF_LCTR)
#else
#define mLCTR 0
#endif

#ifdef PAF_RCTR
#define mRCTR (1<<PAF_RCTR)
#else
#define mRCTR 0
#endif

#ifdef PAF_WIDE
#define mWIDE (1<<PAF_WIDE)
#else
#define mWIDE 0
#endif

#ifdef PAF_LWID
#define mLWID (1<<PAF_LWID)
#else
#define mLWID 0
#endif

#ifdef PAF_RWID
#define mRWID (1<<PAF_RWID)
#else
#define mRWID 0
#endif

#ifdef PAF_OVER
#define mOVER (1<<PAF_OVER)
#else
#define mOVER 0
#endif

#ifdef PAF_LOVR
#define mLOVR (1<<PAF_LOVR)
#else
#define mLOVR 0
#endif

#ifdef PAF_ROVR
#define mROVR (1<<PAF_ROVR)
#else
#define mROVR 0
#endif

#ifdef PAF_SURR
#define mSURR (1<<PAF_SURR)
#else
#define mSURR 0
#endif

#ifdef PAF_LSUR
#define mLSUR (1<<PAF_LSUR)
#else
#define mLSUR 0
#endif

#ifdef PAF_RSUR
#define mRSUR (1<<PAF_RSUR)
#else
#define mRSUR 0
#endif

#ifdef PAF_BACK
#define mBACK (1<<PAF_BACK)
#else
#define mBACK 0
#endif

#ifdef PAF_LBAK
#define mLBAK (1<<PAF_LBAK)
#else
#define mLBAK 0
#endif

#ifdef PAF_RBAK
#define mRBAK (1<<PAF_RBAK)
#else
#define mRBAK 0
#endif

#ifdef PAF_SUBW
#define mSUBW (1<<PAF_SUBW)
#else
#define mSUBW 0
#endif

#ifdef PAF_LSUB
#define mLSUB (1<<PAF_LSUB)
#else
#define mLSUB 0
#endif

#ifdef PAF_RSUB
#define mRSUB (1<<PAF_RSUB)
#else
#define mRSUB 0
#endif

#ifdef PAF_HEAD
#define mHEAD (1<<PAF_HEAD)
#else
#define mHEAD 0
#endif

#ifdef PAF_LHED
#define mLHED (1<<PAF_LHED)
#else
#define mLHED 0
#endif

#ifdef PAF_RHED
#define mRHED (1<<PAF_RHED)
#else
#define mRHED 0
#endif

// ............................................................................

#ifdef PAF_LCTR_HD
#define mLCTR_HD (1<<PAF_LCTR_HD)
#else
#define mLCTR_HD 0
#endif

#ifdef PAF_RCTR_HD
#define mRCTR_HD (1<<PAF_RCTR_HD)
#else
#define mRCTR_HD 0
#endif

#ifdef PAF_CHED
#define mCHED (1<<PAF_CHED)
#else
#define mCHED 0
#endif

#ifdef PAF_OVER_HD
#define mOVER_HD (1<<PAF_OVER_HD)
#else
#define mOVER_HD 0
#endif

#ifdef PAF_LHSI
#define mLHSI (1<<PAF_LHSI)
#else
#define mLHSI 0
#endif

#ifdef PAF_RHSI
#define mRHSI (1<<PAF_RHSI)
#else
#define mRHSI 0
#endif

#ifdef PAF_LHBK
#define mLHBK (1<<PAF_LHBK)
#else
#define mLHBK 0
#endif

#ifdef PAF_RHBK
#define mRHBK (1<<PAF_RHBK)
#else
#define mRHBK 0
#endif

#ifdef PAF_CHBK
#define mCHBK (1<<PAF_CHBK)
#else
#define mCHBK 0
#endif
// ............................................................................

#ifdef PAF_LS1
#define mLS1 (1<<PAF_LS1)
#else
#define mLS1 0
#endif

#ifdef PAF_RS1
#define mRS1 (1<<PAF_RS1)
#else
#define mRS1 0
#endif

#ifdef PAF_LS2
#define mLS2 (1<<PAF_LS2)
#else
#define mLS2 0
#endif

#ifdef PAF_RS2
#define mRS2 (1<<PAF_RS2)
#else
#define mRS2 0
#endif

#ifdef PAF_LRS1
#define mLRS1 (1<<PAF_LRS1)
#else
#define mLRS1 0
#endif

#ifdef PAF_RRS1
#define mRRS1 (1<<PAF_RRS1)
#else
#define mRSR1 0
#endif

#ifdef PAF_LRS2
#define mLRS2 (1<<PAF_LRS2)
#else
#define mLRS2 0
#endif

#ifdef PAF_RRS2
#define mRRS2 (1<<PAF_RRS2)
#else
#define mRRS2 0
#endif

#ifdef PAF_LSC
#define mLSC (1<<PAF_LSC)
#else
#define mLSC 0
#endif

#ifdef PAF_RSC
#define mRSC (1<<PAF_RSC)
#else
#define mRSC 0
#endif

#ifdef PAF_LCS
#define mLCS (1<<PAF_LCS)
#else
#define mLCS 0
#endif

#ifdef PAF_RCS
#define mRCS (1<<PAF_RCS)
#else
#define mRCS 0
#endif

#ifdef PAF_LSD
#define mLSD (1<<PAF_LSD)
#else
#define mLSD 0
#endif

#ifdef PAF_RSD
#define mRSD (1<<PAF_RSD)
#else
#define mRSD 0
#endif

#ifdef PAF_CS
#define mCS (1<<PAF_CS)
#else
#define mCS 0
#endif

// ............................................................................

#ifdef PAF_LTFH		//Left front height
#define mLTFH (1<<PAF_LTFH)
#else
#define mLTFH 0
#endif

#ifdef PAF_RTFH		//Right front height
#define mRTFH (1<<PAF_RTFH)
#else
#define mRTFH 0
#endif

#ifdef PAF_LTFT		//Left top front
#define mLTFT (1<<PAF_LTFT)
#else
#define mLTFT 0
#endif

#ifdef PAF_RTFT		//Right top front
#define mRTFT (1<<PAF_RTFT)
#else
#define mRTFT 0
#endif

#ifdef PAF_LTRR		//Left top rear
#define mLTRR (1<<PAF_LTRR)
#else
#define mLTRR 0
#endif

#ifdef PAF_RTRR		//Right top rear
#define mRTRR (1<<PAF_RTRR)
#else
#define mRTRR 0
#endif

#ifdef PAF_LTMD		//Left top middle
#define mLTMD (1<<PAF_LTMD)
#else
#define mLTMD 0
#endif

#ifdef PAF_RTMD		//Right top middle
#define mRTMD (1<<PAF_RTMD)
#else
#define mRTMD 0
#endif

#ifdef PAF_LTRH		//Left rear height
#define mLTRH (1<<PAF_LTRH)
#else
#define mLTRH 0
#endif

#ifdef PAF_RTRH		//Right rear height
#define mRTRH (1<<PAF_RTRH)
#else
#define mRTRH 0
#endif

// ............................................................................

#ifdef PAF_FORM_NSSE
#define mNSSE (1 << PAF_FORM_NSSE)
#else
#define mNSSE 0
#endif

#ifdef PAF_FORM_YSSE
#define mYSSE (1 << PAF_FORM_YSSE)
#else
#define mYSSE 0
#endif

#ifdef PAF_FORM_NBSE
#define mNBSE (1 << PAF_FORM_NBSE)
#else
#define mNBSE 0
#endif

#ifdef PAF_FORM_YBSE
#define mYBSE (1 << PAF_FORM_YBSE)
#else
#define mYBSE 0
#endif

#ifdef PAF_FORM_MONO
#define mMONO (1 << PAF_FORM_MONO)
#else
#define mMONO 0
#endif

#ifdef PAF_FORM_DUAL
#define mDUAL (1 << PAF_FORM_DUAL)
#else
#define mDUAL 0
#endif

// ............................................................................

#define mFRNT (mLEFT|mRGHT|mCNTR|mLCTR|mRCTR|mWIDE|mLWID|mRWID|mOVER|mLOVR|mROVR)
#define mREAR (mSURR|mLSUR|mRSUR|mBACK|mLBAK|mRBAK)
#define mBASS (mSUBW|mLSUB|mRSUB)
#define mXXXX (mHEAD|mLHED|mRHED)

#endif  /* _CCM */
