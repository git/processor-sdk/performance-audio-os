/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Common PCM Encoder algoritm interface implementation
//
//
//

/*
 *  IPCE default instance creation parameters
 */
#include <xdc/std.h>
#include <ipce.h>

/*
 *  ======== IPCE_PARAMS ========
 *  This static initialization defines the default parameters used to
 *  create instances of PCE objects.
 */

#if IPCE_PHASES != 6 
#error internal error
#endif

//#define PAF_MAXNUMCHAN_AF 2
/*
 *  ======== IPCE_PARAMS_STATUS_DELAY_BASIC ========
 */

#if PAF_MAXNUMCHAN_AF == 2 
    #define IPCE_DELAY_NUMCHAN 2
    #define IPCE_PARAMS_STATUS_DELAY_BASIC \
        0, /* unused */ \
        1, /* unit */ \
        PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
        0, 0, /* delay */ \
        0, /* masterDelay */
#elif PAF_MAXNUMCHAN_AF == 4 
    #define IPCE_DELAY_NUMCHAN 2
    #define IPCE_PARAMS_STATUS_DELAY_BASIC \
        0, /* unused */ \
        1, /* unit */ \
        PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
        0, 0, 0, 5, /* delay */ \
        0, /* masterDelay */
#elif PAF_MAXNUMCHAN_AF == 6 
    #define IPCE_DELAY_NUMCHAN 3 
    #define IPCE_PARAMS_STATUS_DELAY_BASIC \
        0, /* unused */ \
        1, /* unit */ \
        PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
        0, 0, 0, 0, 5, 5, /* delay */ \
        0, /* masterDelay */
#elif PAF_MAXNUMCHAN_AF == 8 
    #define IPCE_DELAY_NUMCHAN 5 
    #define IPCE_PARAMS_STATUS_DELAY_BASIC \
        0, /* unused */ \
        1, /* unit */ \
        PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
        0, 0, 0, 0, 5, 5, 5, 5, /* delay */ \
        0, /* masterDelay */
#elif PAF_MAXNUMCHAN_AF == 16 
    #define IPCE_DELAY_NUMCHAN 5 
    #define IPCE_PARAMS_STATUS_DELAY_BASIC \
        0, /* unused */ \
        1, /* unit */ \
        PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
        0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 0, 0, 0, /* delay */ \
        0, /* masterDelay */

#elif PAF_MAXNUMCHAN_AF == 32

    #define IPCE_DELAY_NUMCHAN 5 /* Only 5 channels delays are specified */
    #define IPCE_PARAMS_STATUS_DELAY_BASIC \
        0, /* unused */ \
        1, /* unit */ \
        PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
        0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* delay */ \
    	0, /* masterDelay */    

#else /* PAF_MAXNUMCHAN_AF == 32 */
 
    #define IPCE_DELAY_NUMCHAN 0 
    #define IPCE_PARAMS_STATUS_DELAY_BASIC
 
    #error code not written
#endif/* PAF_MAXNUMCHAN_AF */

const IPCE_Status IPCE_PARAMS_STATUS_BASIC = {
    sizeof(IPCE_Status),
    1,          /* mode: enabled */
    0,          /* type: unused */
    1, 0,       /* phase 0 mode,type: enabled,unused */
    1, 0,       /* phase 1 mode,type: enabled,unused */
    1, 0,       /* phase 2 mode,type: enabled,unused */
    1, 0,       /* phase 3 mode,type: enabled,unused */
    1, 0,       /* phase 4 mode,type: enabled,unused */
    1, 0,       /* phase 5 mode,type: enabled,unused */
    IPCE_PARAMS_STATUS_DELAY_BASIC/* Delay phase: Basic configuration */
    0,          /* pceExceptionDetect */
    0,          /* pceExceptionFlag */
    0,          /* pceExceptionMute */
    0           /* pceClipDetect */
};

/*
 *  ======== IPCE_PARAMS_CONFIG_DELAY_BASIC ========
 *  This static initialization defines the parameters used to create
 *  instances of DEL objects - 5-channel @ 48 kHz.
 */

const IPCE_ConfigPhaseVolume IPCE_PARAMS_CONFIG_PHASE_VOLUME_BASIC = {
    sizeof(IPCE_ConfigPhaseVolume),
    -2*40,      /* volumeRamp */
};

#define IPCE_DELAY_CNTR  5*96
#define IPCE_DELAY_LSUR 15*96
#define IPCE_DELAY_RSUR 15*96
#define IPCE_DELAY_LBAK 15*96
#define IPCE_DELAY_RBAK 15*96
#define IPCE_DELAY_TOTAL                                        \
        (IPCE_DELAY_CNTR + IPCE_DELAY_LSUR + IPCE_DELAY_RSUR +  \
         IPCE_DELAY_LBAK + IPCE_DELAY_RBAK)

#define IPCE_DELAY_ESIZE 3     /* delay sizeofElement: packed 24 bits */ 
#define IPCE_DELAY_FSIZE 256   /* delay sizeofFrame: 256 samples */ 
 
const PAF_DelayState IPCE_PARAMS_CONFIG_PHASE_DELAYSPEC_BASIC[] = {
    0, 0, IPCE_DELAY_CNTR, 0, NULL, 0, 
    0, 0, IPCE_DELAY_LSUR, 0, NULL, 0, 
    0, 0, IPCE_DELAY_RSUR, 0, NULL, 0, 
    0, 0, IPCE_DELAY_LBAK, 0, NULL, 0, 
    0, 0, IPCE_DELAY_RBAK, 0, NULL, 0,
};

const IPCE_ConfigPhaseDelay IPCE_PARAMS_CONFIG_PHASE_DELAY_BASIC = {
    sizeof(IPCE_ConfigPhaseDelay) + IPCE_DELAY_NUMCHAN * sizeof(PAF_DelayState),
    (1<<PAF_CNTR)|(1<<PAF_LSUR)|(1<<PAF_RSUR)|(1<<PAF_LBAK)|(1<<PAF_RBAK),
    IPCE_DELAY_ESIZE,
    (PAF_DelayState *)IPCE_PARAMS_CONFIG_PHASE_DELAYSPEC_BASIC,
};

const PAF_IALG_Common IPCE_PARAMS_COMMON_PHASE_DELAY_BASIC = {
    sizeof(PAF_IALG_Common) + IPCE_DELAY_TOTAL * IPCE_DELAY_ESIZE,
    PAF_IALG_COMMONN(8),                /* flag: specifies common memory type*/
};
 
const IPCE_Config IPCE_PARAMS_CONFIG_BASIC = {
    sizeof(IPCE_Config),
    0,          /* frameLength */
    0,          /* unused */
    (IPCE_ConfigPhase *)&IPCE_PARAMS_CONFIG_PHASE_VOLUME_BASIC,/* phase 0 config: volume */
    (IPCE_ConfigPhase *)&IPCE_PARAMS_CONFIG_PHASE_DELAY_BASIC, /* phase 1 config: delay  */
    0,                                                         /* phase 2 config: output */
    0,                                                         /* phase 3 config: unused */
    0,                                                         /* phase 4 config: unused */
    0,                                                         /* phase 5 config: unused */
    0,                                                         /* phase 0 common: volume */
    (PAF_IALG_Common*)&IPCE_PARAMS_COMMON_PHASE_DELAY_BASIC,   /* phase 1 common: delay  */
    0,                                                         /* phase 2 common: output */
    0,                                                         /* phase 3 common: unused */
    0,                                                         /* phase 4 common: unused */
    0,                                                         /* phase 5 common: unused */
                /* scale -- uninitialized */
};

/*
 *  ======== IPCE_PARAMS_ACTIVE ========
 */
const PAF_VolumeStatus PAF_VOLUME_STATUS = {
        sizeof(PAF_VolumeStatus),
    1,    // mode;
    PAF_MAXNUMCHAN_AF, // channelCount;
    15, // implementation;
    0, // unused1;
    0, // rampTime;
    0, // unused2;
    0, // unused3;
    0xffd8, 0x0/*24*/, 0xff38, 0, // master(control, offset, instat, exstat);

    0, 0, 0, 0, // trim[PAF_MAXNUMCHAN_AF];
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0,
};
/*
 *  ======== IPCE_PARAMS_ACTIVE ========
 */

const PAF_ActivePhaseVolume IPCE_PARAMS_ACTIVE_PHASE_VOLUME_BASIC = {
    sizeof(PAF_ActivePhaseVolume),

};
const PAF_EncodeStatus ENCODESTATUS =
{
    sizeof (PAF_EncodeStatus),          // size
    1,                                  // mode, 2 is compact mode
    0,                                  // unused1
    PAF_SAMPLERATE_UNKNOWN,             // sampleRate
    PAF_MAXNUMCHAN_AF,                                  // channelCount
    0,0,0,0,
    0,0,0,0,
    0,0,0,0,
    0,0,0,0,
    0,0,0,0,
    0,                                  // frameLength
    0,                                  // encBypass
    PAF_SOURCE_PCM,                     // select
    3, 0, 0, 0,0,0,0,0,
                                        // channelConfigurationRequest.full
    3, 0, 0, 0,0,0,0,0,
                                        // channelConfigurationStream.full
    3, 0, 0, 0,0,0,0,0,
                                        // channelConfigurationEncode.full
    0,0,0,0,0,0,0,0,                    //Unused6[8]
    -3,                                 // channelMap.from[0]
    -3,                                 // channelMap.from[1]
    -3,                                 // channelMap.from[2]
    -3,                                 // channelMap.from[3]
    -3,                                 // channelMap.from[4]
    -3,                                 // channelMap.from[5]
    -3,                                 // channelMap.from[6]
    -3,                                 // channelMap.from[7]
    -3,                                 // channelMap.from[8]
    -3,                                 // channelMap.from[9]
    -3,                                 // channelMap.from[10]
    -3,                                 // channelMap.from[11]
    -3,                                 // channelMap.from[12]
    -3,                                 // channelMap.from[13]
    -3,                                 // channelMap.from[14]
    -3,                                 // channelMap.from[15]

    -3,                                 // channelMap.from[16]
    -3,                                 // channelMap.from[1]
    -3,                                 // channelMap.from[2]
    -3,                                 // channelMap.from[3]
    -3,                                 // channelMap.from[4]
    -3,                                 // channelMap.from[5]
    -3,                                 // channelMap.from[6]
    -3,                                 // channelMap.from[7]
    -3,                                 // channelMap.from[8]
    -3,                                 // channelMap.from[9]
    -3,                                 // channelMap.from[10]
    -3,                                 // channelMap.from[11]
    -3,                                 // channelMap.from[12]
    -3,                                 // channelMap.from[13]
    -3,                                 // channelMap.from[14]
    -3,                                 // channelMap.from[32]

    -3,                                 // channelMap.to[0]
    -3,                                 // channelMap.to[1]
    -3,                                 // channelMap.to[2]
    -3,                                 // channelMap.to[3]
    -3,                                 // channelMap.to[4]
    -3,                                 // channelMap.to[5]
    -3,                                 // channelMap.to[6]
    -3,                                 // channelMap.to[7]
    -3,                                 // channelMap.to[8]
    -3,                                 // channelMap.to[9]
    -3,                                 // channelMap.to[10]
    -3,                                 // channelMap.to[11]
    -3,                                 // channelMap.to[12]
    -3,                                 // channelMap.to[13]
    -3,                                 // channelMap.to[14]
    -3,                                 // channelMap.to[15]

    -3,                                 // channelMap.to[16]
    -3,                                 // channelMap.to[1]
    -3,                                 // channelMap.to[2]
    -3,                                 // channelMap.to[3]
    -3,                                 // channelMap.to[4]
    -3,                                 // channelMap.to[5]
    -3,                                 // channelMap.to[6]
    -3,                                 // channelMap.to[7]
    -3,                                 // channelMap.to[8]
    -3,                                 // channelMap.to[9]
    -3,                                 // channelMap.to[10]
    -3,                                 // channelMap.to[11]
    -3,                                 // channelMap.to[12]
    -3,                                 // channelMap.to[13]
    -3,                                 // channelMap.to[14]
    -3,                                 // channelMap.to[32]

    1,                                  // programFormat.mask
    0,                                  // programFormat.form
    0,                                  // listeningFormat.mask
    0,                                  // listeningFormat.form
    0,                                  // sampleProcess[0]
    0,0,0,0,                      // Unused7[4]
    3, 0, 0, 0,0,0,0,0,
                                       //channelConfigurationCompact
    0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,
                                       //channelCompact[PAF_MAXNUMCHAN_HD]
};


/*
const PAF_EncodeStatus ENCODESTATUS = {
0x000000C0,
0x01,
0x80,
0x04,
0x10,
}
};*/
const PAF_OutBufConfig OUTBUFCONFIG = {
    0,                  //PAF_UnionPointer base;
    0,                  //PAF_UnionPointer pntr;
    0,                  //PAF_UnionPointer head;
    0x18000,         //XDAS_Int32 sizeofBuffer;
    0x03,               //XDAS_Int8 sizeofElement;
    0x18,               //XDAS_Int8 precision;
    0x10,               //XDAS_Int8 stride;
    0x00,               //XDAS_Int8 unused;
    0x100,         //XDAS_Int32 lengthofFrame;
    0x0,         //XDAS_Int32 lengthofData;
    0x0,             //XDAS_Int16 putCount;
    0x10,             //XDAS_Int16 putStride;
    0x18000,         //XDAS_Int32 allocation;
};

const PAF_ActivePhaseOutput IPCE_PARAMS_ACTIVE_PHASE_OUTPUT_BASIC = {
    sizeof(PAF_ActivePhaseOutput),
   // ENCODESTATUS,                    /* pEncodeStatus: initialization */
   // OUTBUFCONFIG                     /* pOutBufConfig: initialization */
};

const IPCE_Active IPCE_PARAMS_ACTIVE_BASIC = {
    sizeof(IPCE_Active),
    0,
    (PAF_ActivePhase *)&IPCE_PARAMS_ACTIVE_PHASE_VOLUME_BASIC,  /* phase 0 active: volume */
    0,                                                          /* phase 1 active: delay  */
    (PAF_ActivePhase *)&IPCE_PARAMS_ACTIVE_PHASE_OUTPUT_BASIC,  /* phase 2 active: output */   
    0,                                                          /* phase 3 active: unused */
    0,                                                          /* phase 4 active: unused */
    0,                                                          /* phase 5 active: unused */
                            /* bitstreamMask uninitialized */
};
 
/*
 *  ======== IPCE_PARAMS_SCRACH ========
 */
const PAF_ScrachPhaseDelay IPCE_PARAMS_SCRACH_PHASE_DELAY_BASIC = {
    sizeof(PAF_ScrachPhaseDelay)+IPCE_DELAY_ESIZE*IPCE_DELAY_FSIZE,
};

//Loooks to be an error ; need to be of PAF_ScrachPhaseOutput type : \ipce_cus.c
const PAF_ScrachPhaseDelay IPCE_PARAMS_SCRACH_PHASE_OUTPUT_BASIC = {
    sizeof(PAF_ScrachPhaseDelay)+12*4*IPCE_DELAY_FSIZE, // 12*256*4 = no of output channels * block size * output sample size
};

const IPCE_Scrach IPCE_PARAMS_SCRACH_BASIC = {
    sizeof(IPCE_Scrach),
    0,                                                          /* phase 0 scrach: volume */
#if defined(PAF_DEVICE) && (((PAF_DEVICE&0xFFFF0000) == 0xD7100000) || ((PAF_DEVICE&0xFF000000) == 0xD8000000))
    (PAF_ScrachPhase *)&IPCE_PARAMS_SCRACH_PHASE_DELAY_BASIC,   /* phase 1 scrach: delay  */
#else /* PAF_DEVICE */
    0,                                                          /* phase 1 scrach: delay  */
#endif /* PAF_DEVICE */
#if defined(PAF_DEVICE) && (((PAF_DEVICE&0xFFFF0000) == 0xD7100000) || ((PAF_DEVICE&0xFF000000) == 0xD8000000))
    (PAF_ScrachPhase *)&IPCE_PARAMS_SCRACH_PHASE_OUTPUT_BASIC,  /* phase 2 scrach: output */
#else /* PAF_DEVICE */
    0,                                                          /* phase 2 scrach: output */
#endif /* PAF_DEVICE */
    0,                                                          /* phase 3 scrach: unused */  
    0,                                                          /* phase 4 scrach: unused */  
    0,                                                          /* phase 5 scrach: unused */  
};

const IPCE_Params IPCE_PARAMS = {
    sizeof(IPCE_Params),
    &IPCE_PARAMS_STATUS_BASIC ,
    &IPCE_PARAMS_CONFIG_BASIC ,
    &IPCE_PARAMS_ACTIVE_BASIC ,
    &IPCE_PARAMS_SCRACH_BASIC ,
};

//asm (" .global _IPCE_PARAMS");
//asm ("_IPCE_PARAMS .set _IPCE_PARAMS_BASIC");

