/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/*
 *  ======== iir2tapnch.c ========
 *  IIR filter implementation for tap-2 & channels-N, SP.
 */
 
/************************ IIR FILTER *****************************************/
/****************** ORDER 2    and CHANNELS N ********************************/
/*                                                                           */
/* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2                           */
/*                           ----------------------                          */
/*                            1  - a1*z~1 - a2*z~2                           */
/*                                                                           */
/*   Direct form    : y(n) = b0*x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2) */
/*                                                                           */
/*   Canonical form : w(n) = x(n)    + a1*w(n-1) + a2*w(n-2)                 */
/*                    y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                 */
/*                                                                           */
/*   Filter variables :                                                      */
/*      y(n) - *y,         x(n)   - *x                                       */
/*      w(n) -             w(n-1) - w1         w(n-2) - w2                   */
/*      b0 - filtCfsB[0], b1 - filtCfsB[1], b2 - filtCfsB[2]                 */
/*      a1 - filtCfsA[0], a2 - filtCfsA[1]                                   */
/*                                                                           */
/*                                                                           */
/*                              w(n)    b0                                   */
/*     x(n)-->---[+]---->--------@------>>-----[+]--->--y(n)                 */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a1    +-----+    b1     |                            */
/*               [+]----<<----| Z~1 |---->>----[+]                           */
/*                |           +-----+           |                            */
/*                |              |              |                            */
/*                ^              v              ^                            */
/*                |     a2    +-----+    b2     |                            */
/*                 ----<<-----| Z~1 |---->>-----                             */
/*                            +-----+                                        */
/*                                                                           */
/*****************************************************************************/

/* Header files */
#include "filters.h"
#include "fil_table.h"

/*
 *  ======== Filter_iirT2ChN() ========
 *  IIR filter, taps-2 & channels-N, SP 
 */
 

/* Memory section for the function code */
#pragma CODE_SECTION(Filter_iirT2ChN, ".text:Filter_iirT2ChN")
Int Filter_iirT2ChN( PAF_FilParam *pParam ) 
{
    Void ** pIn, ** pOut, ** pCoef, ** pVar; /* Handle parameters */
    Int i; /* Channel counter */
    Int channels, error = 0, multiple;
    Uint uniCoef = 0, inplace = 0;
    
    /* Get handle parameters into temp local variables */
    pIn   = pParam->pIn;
    pOut  = pParam->pOut;
    pCoef = pParam->pCoef;
    pVar  = pParam->pVar;
    
    /* Check whether unicoeff and inplace */
    for( i = 0; i < pParam->channels; i++ )
    {
      inplace |= ( Uint )pParam->pIn[i]   ^  ( Uint )pParam->pOut[i];
      uniCoef |= ( Uint )pParam->pCoef[0] ^  ( Uint )pParam->pCoef[i];
    }
              
    channels = pParam->channels; /* Get the no of chs filtered */
    
    /* Ch-4 */
    if( channels == 4 )
    {
        /* If unicoef & inplace */
        if( !(uniCoef | inplace) )
            error += Filter_iirT2Ch4_ui( pParam );
            
        /* If unicoef */    
        else if( !uniCoef ) 
            error += Filter_iirT2Ch4_u( pParam );
        
        /* General, 3-1 chs */    
        else
        	{
            	error += Filter_iirT2Ch3( pParam );
            	FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
            	error += Filter_iirT2Ch1( pParam );
        	}
    } /* if( channels == 4 ) */
    
    /* Ch-5, as 3-2 chs */   
    else if( channels == 5 )
    {
    	if( !uniCoef ) 
            	error += Filter_iirT2Ch5_u( pParam );
        
        else
        	{
    
        		error += Filter_iirT2Ch3( pParam );
        		FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
        		error += Filter_iirT2Ch2( pParam );
        	}
    }
    
    /* Ch-6, as 3-3 chs */
    else if( channels == 6 )
    {
    	if( !uniCoef ) 
            	error += Filter_iirT2Ch6_u( pParam );
        
        else
        	{
        		error += Filter_iirT2Ch3( pParam );
        		FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
        		error += Filter_iirT2Ch3( pParam );
        	}
    }
    
    /* Ch-7 */
    else if( channels == 7 )
    {
        /* If unicoef & inplace, as 4ui-3 chs */
        if( !(uniCoef | inplace) )
        	{
            	error += Filter_iirT2Ch4_ui( pParam );
            	FIL_offsetParam( pParam, 4 ); /* Offset the param ptrs by 4 */
            	error += Filter_iirT2Ch3( pParam );
        	}
        
        /* If unicoeff, as 7u chs */
        else if( !uniCoef ) 
            {
                error += Filter_iirT2Ch7_u( pParam );
                
            }
             
            /* General, as 3-3-1 chs */
        else
         	{
            	error += Filter_iirT2Ch3( pParam );
            	FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
            	error += Filter_iirT2Ch3( pParam );
            	FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
            	error += Filter_iirT2Ch1( pParam );
         	}
    } /* else if( channels == 7 ) */   

    /* Ch-8 */
    else if( channels == 8 )
    {
        /* If unicoeff/inplace, as 4ui-4ui chs */
        if( !(uniCoef | inplace) )
        	{
            	error += Filter_iirT2Ch4_ui( pParam );
            	FIL_offsetParam( pParam, 4 ); /* Offset the param ptrs by 4 */ 
            	error += Filter_iirT2Ch4_ui( pParam );
        	}
        
        /* If unicoeff, as 8u chs */
        else if( !uniCoef ) 
        	{
                error += Filter_iirT2Ch8_u( pParam );
                
            }
             
            /* General, as 3-3-2 chs */
        else
            {
                error += Filter_iirT2Ch3( pParam );
                FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
                error += Filter_iirT2Ch3( pParam );
                FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
                error += Filter_iirT2Ch2( pParam );
            }
    } /* else if( channels == 8 ) */
            
    /* Ch-N */
    else if( channels <= PAF_MAXNUMCHAN )
    {
        multiple  = (Int)(channels / 3); /* Calculate int(ch/3) */
        channels -= (multiple * 3); /* Find ch % 3 */
        
        /* Multiple of 3 channels are filtered using 3-ch implementation */
        while( multiple )
        {
            error += Filter_iirT2Ch3( pParam );
            FIL_offsetParam( pParam, 3 ); /* Offset the param ptrs by 3 */
            multiple--; /* Decrement counter */          
        }
        /* Remaining ch=2 */
        if( channels == 2 )
            error += Filter_iirT2Ch2( pParam );
        /* Remaining ch=1 */        
        else if( channels == 1 )
            error += Filter_iirT2Ch1( pParam );
    } /* else if( channels <= PAF_MAXNUMCHAN ) */

    /* Put back the handle elements */
    pParam->pIn   = pIn;
    pParam->pOut  = pOut;
    pParam->pCoef = pCoef;
    pParam->pVar  = pVar;  
    
    return(error);     
} /* Int Filter_iirT2ChN() */

