
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <std.h>
#include <stdio.h>
#include <stdlib.h>

#include <asp0.h>
#include <paftyp.h>
#include <paftyp_a.h>
#include <stdasp.h>

#include <simasp.h>

#include <ae.h>
#include <iae.h>
#include <ae_mds.h>
#include <ae_mds_priv.h>

#ifndef OUTDATAFILE
#define OUTDATAFILE "simasp_out.dat"
#endif

#ifndef INDATAFILE
#define INDATAFILE "simasp_in.dat"
#endif

#define NUMCHAN 3

const IAE_Status SIM_IAE_PARAMS_STATUS = {
    sizeof(IAE_Status),     /* Size of this structure.  */
    1,                      /* AE Mode Control Register.  1=enabled. 0=disabled.  */
    0,                      /* Unused.  */
    16284,                  /* scaleQ15 value.  Default=0 means scaling is disabled. */
};
const IAE_Params SIM_IAE_PARAMS = {
    sizeof(IAE_Params),     /* size */
    &SIM_IAE_PARAMS_STATUS, /* pStatus */
    0,                      /* config */
};

// -----------------------------------------------------------------------------

int simASP (void)
{
    ALG_Handle alg;
    FILE *pInFile;
    FILE *pOutFile;
    int errno;
    int i;
    

    // allocate memory for audio frame
    errno = initAudioFrame(NUMCHAN);
    if (errno) {
        fprintf( stderr, "initAudioFrame failed ... Aborting\n" );
        exit(EXIT_FAILURE);
    }

    //init audio frame from file
    pInFile = fopen( INDATAFILE, "r");
    for (i=0; i < FRAMELENGTH; i++)
        fscanf(pInFile, "%g\t%g\t%g\n",
                &audioFrame[0].data.sample[PAF_LEFT][i],
                &audioFrame[0].data.sample[PAF_RGHT][i],
                &audioFrame[0].data.sample[PAF_CNTR][i] );
    fclose( pInFile );
    
    // apply ASP
    alg = PAF_ALG_create(&AE_MDS_IALG, NULL, (IALG_Params *) &SIM_IAE_PARAMS ,NULL, NULL);
    ((ASP_Handle) alg)->fxns->apply( (ASP_Handle) alg, &audioFrame[0]);
    
    //output audio frame to file
    pOutFile = fopen( OUTDATAFILE, "w+");
    for (i=0; i < FRAMELENGTH; i++)
        fprintf(pOutFile, "% 0.7f\t% 0.7f\t% 0.7f\n",
                audioFrame[0].data.sample[PAF_LEFT][i],
                audioFrame[0].data.sample[PAF_RGHT][i],
                audioFrame[0].data.sample[PAF_CNTR][i] );
    fclose( pOutFile );

    // indicate done
    fprintf( stdout, "Finished\n");

    return errno;
} //simASP

// -----------------------------------------------------------------------------
