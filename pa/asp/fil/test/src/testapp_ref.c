/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

#include <stdio.h>

#include <testapp.h>


Int iir_generic_cas(
    PAF_FilParam *pParam)
{
    Int count;
    Int i,t,taps,ch,casc,c;
    PAF_AudioData accY,accW;
    PAF_AudioData input;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtVars;
    PAF_AudioData * restrict filtCfsB,* restrict filtCfsA;

    PAF_AudioData filtVarsTemp0,filtVarsTemp1;
    
    count=pParam->sampleCount;
    taps=(pParam->use)&0xFFFF;
    casc=((pParam->use)&0xFFFF0000)>>16;

    for(ch=0;ch<pParam->channels;ch++){
        x=(PAF_AudioData*)pParam->pIn[ch];
        y=(PAF_AudioData*)pParam->pOut[ch];
        for(i=0;i<count;i++){
            filtCfsB=(PAF_AudioData*)pParam->pCoef[ch];
            input=filtCfsB[0]*x[i];
            for (c=0;c<casc;c++){
                filtCfsB=(PAF_AudioData*)pParam->pCoef[ch]+c*taps*2+1;
                filtCfsA=filtCfsB+taps;
                filtVars=(PAF_AudioData*)pParam->pVar[ch]+c*taps;
                accW=input;
                accY=0;
                for(t=0;t<taps;t++){
                    accW+=filtCfsA[t]*filtVars[t];
                    accY+=filtCfsB[t]*filtVars[t];
                }
                accY+=accW;
                input=accY;
                filtVarsTemp0=filtVars[0];
                for(t=0;t<taps;t++){
                    filtVarsTemp1=filtVars[t+1];
                    filtVars[t+1]=filtVarsTemp0;
                    filtVarsTemp0=filtVarsTemp1;
                }
                filtVars[taps]=filtVarsTemp1;
                filtVars[0]=accW;
            }
            y[i]=input;
        }
    }
    return 0;
}

Int iir_generic_mx(
    PAF_FilParam *pParam)
{
    Int count;
    Int i,t,taps,ch;
    double accY,accW;
    double *restrict filtVars;
    double filtVarsTemp0,filtVarsTemp1;
    
    PAF_AudioData *restrict x;
    PAF_AudioData *restrict y;
    double *restrict filtCfsB,*restrict filtCfsA;

    count=pParam->sampleCount;
    taps=pParam->use;
    
    for(ch=0;ch<pParam->channels;ch++){
        x=(PAF_AudioData*)pParam->pIn[ch];
        y=(PAF_AudioData*)pParam->pOut[ch];
        filtCfsB=(double*)pParam->pCoef[ch];
        filtCfsA=filtCfsB+taps+1;
        filtVars=(double*)pParam->pVar[ch];
        for(i=0;i<count;i++){
            accY=0;
            accW=*x*filtCfsB[0];
            for(t=0;t<taps;t++){
                accW+=filtCfsA[t]*filtVars[t];
                accY+=filtCfsB[t+1]*filtVars[t];
            }
            accY+=accW;
            *y++=accY;
            x++;
            filtVarsTemp0=filtVars[0];
            for(t=0;t<taps;t++){
                filtVarsTemp1=filtVars[t+1];
                filtVars[t+1]=filtVarsTemp0;
                filtVarsTemp0=filtVarsTemp1;
            }
            filtVars[taps]=filtVarsTemp1;
            filtVars[0]=accW;
         }
     }
     return 0;
}

Int iir_generic(
    PAF_FilParam *pParam)
{
    Int count;
    Int i,t,taps,ch;
    PAF_AudioData accY,accW;
    PAF_AudioData *restrict filtVars;
    PAF_AudioData filtVarsTemp0,filtVarsTemp1;
    
    PAF_AudioData *restrict x;
    PAF_AudioData *restrict y;
    PAF_AudioData *restrict filtCfsB,*restrict filtCfsA;

    count=pParam->sampleCount;
    taps=pParam->use;
    
    for(ch=0;ch<pParam->channels;ch++){
        x=(PAF_AudioData*)pParam->pIn[ch];
        y=(PAF_AudioData*)pParam->pOut[ch];
        filtCfsB=(PAF_AudioData*)pParam->pCoef[ch];
        filtCfsA=filtCfsB+taps+1;
        filtVars=(Float*)pParam->pVar[ch];
        for(i=0;i<count;i++){
            accY=0;
            accW=0;
            for(t=0;t<taps;t++){
                accW+=filtCfsA[t]*filtVars[t];
                accY+=filtCfsB[t+1]*filtVars[t];
            }
            accW += *x;
            accY += accW*filtCfsB[0];
            *y++=accY;
            x++;
            filtVarsTemp0=filtVars[0];
            for(t=0;t<taps;t++){
                filtVarsTemp1=filtVars[t+1];
                filtVars[t+1]=filtVarsTemp0;
                filtVarsTemp0=filtVarsTemp1;
            }
            filtVars[taps]=filtVarsTemp1;
            filtVars[0]=accW;
         }
     }
     return 0;
}
