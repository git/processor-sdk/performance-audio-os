/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Common PCM algorithm interface implementation
//


/*
 *  IPCM default instance creation parameters
 */
#include <xdc/std.h>
#include <ipcm.h>

/*
 *  ======== IPCM_PARAMS ========
 *  This static initialization defines the default parameters used to
 *  create instances of PCM objects.
 */
const IPCM_Status IPCM_PARAMS_STATUS = {
    sizeof(IPCM_Status),
    1,   // mode
#ifndef _PATE_
    1,   // ramp
#else /* _PATE_ */
    0,   // ramp
#endif /* _PATE_ */
    0,   // scaleVolume
    2*10,  // LFEDownmixVolume
    0,0,0,0,  // Unused1[4];
    -2*3,  // CntrMixLev
    -2*3,   // SurrMixLev
    0,      // LFEDownmixInclude
    0,0,0,   // unused[3]
	0,0,0,0,  // Unused2[4];
	{ PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, PAF_CC_AUX_STEREO_UNKNOWN, 0,0,0,0,0 },  // channelConfigurationProgram
};
const IPCM_Config IPCM_PARAMS_CONFIG = {
    sizeof(IPCM_Config),
#ifndef _PATE_
    8,
    4096,
#else /* _PATE_ */
    128,  /* minimumFrameLength */
    256,  /* maximumFrameLength */
#endif /* _PATE_ */
    NULL, // &CPL_TII_ICPL,
};

PAF_InpBufConfig InpBufConfig = {
//        const PAF_InpBufConfig *pInpBufConfig;
    0,//PAF_UnionPointer base;				// base address of input buffer
    0,//   PAF_UnionPointer pntr;				// beginning address of valid data in buffer
    0,//   PAF_UnionPointer head;				// end address of valid data in buffer
    0,//   PAF_UnionPointer futureHead;		// end address of valid data after next DMA completes
    0,//   XDAS_Int32 sizeofBuffer;			// amount of buffer memory available
    4,//   XDAS_Int8 sizeofElement;			// number of bytes per input data word
    0,//   XDAS_Int8 precision;				// number of valid bits per input data word
    0,//   XDAS_Int8 stride;					// number of input data words per sample
    0,//   XDAS_Int8 deliverZeros;				// used to inform PCM decoder when to ignore data
    0,//XDAS_Int32 frameLength;				// number of words in current frame
    0,//XDAS_Int32 lengthofData;			// number of words needed to contain one frame
    0,//PAF_InpBufStatus   *pBufStatus;		// pointer to status (see f/alpha/inpuf_a.h)
    0//XDAS_Int32 allocation;
//        const PAF_InpBufConfig *pInpBufConfig; ends
};

const IPCM_Active IPCM_PARAMS_ACTIVE = {
    sizeof(IPCM_Active), // size
    0,//    Int flag;
    NULL,//    PAF_DecodeStatus *pDecodeStatus;
    &InpBufConfig,
    0,//XDAS_Int8 rampState;
    0,//XDAS_Int8 unused[3];
    0,//PAF_ChannelMask programMask;
    0//PAF_ChannelMask programWarp;
};
const IPCM_Params IPCM_PARAMS = {
    sizeof(IPCM_Params),
    &IPCM_PARAMS_STATUS,
    &IPCM_PARAMS_CONFIG,
    NULL,//&IPCM_PARAMS_ACTIVE,//NULL,
    NULL,
};





