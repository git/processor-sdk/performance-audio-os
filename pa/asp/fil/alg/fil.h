/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/**
 *  @file   fil.h
 *
 *  @brief  This header defines all types, constants, and functions used by 
 *          applications that use the FIL algorithm.
 *
 *          Applications that use this interface enjoy type safety and
 *          the ability to incorporate multiple implementations of the FIL
 *          algorithm in a single application at the expense of some
 *          additional indirection.
 *
 */
/*
 *  ======== fil.h ========
 *  This header defines all types, constants, and functions used by 
 *  applications that use the FIL algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the FIL
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
 
#ifndef FIL_
#define FIL_

#include <alg.h>
#include <ifil.h>
#include <ti/xdais/ialg.h>
#include <paf_alg.h>

/**
 *  @brief FIL algorithm instance handle
 */
/*
 *  ======== FIL_Handle ========
 *  FIL algorithm instance handle
 */
typedef struct IFIL_Obj *FIL_Handle;

/**
 *  @brief FIL algorithm instance creation parameters
 */
/*
 *  ======== FIL_Params ========
 *  FIL algorithm instance creation parameters
 */
typedef struct IFIL_Params FIL_Params;

/** 
  *  @def FIL_PARAMS
  *       Default instance parameters
  */
/*
 *  ======== FIL_PARAMS ========
 *  Default instance parameters
 */
#define FIL_PARAMS IFIL_PARAMS

/**
 *  @brief Status structure for getting FIL instance attributes
 */
/*
 *  ======== FIL_Status ========
 *  Status structure for getting FIL instance attributes
 */
typedef volatile struct IFIL_Status FIL_Status;

/**
 *  @brief      Create an instance of a FIL object.
 *
 *  @param[in]  fxns XDAIS algorithm fields and methods.
 *
 *  @param[in]  prms Algorithm instance creation parameters.
 *
 *
 *  @retval     Handle for instance of FIL object.
 *
 */
/*
 *  ======== Fil_create ========
 *  Create an instance of a FIL object.
 */
static inline FIL_Handle FIL_create(const IFIL_Fxns *fxns, const FIL_Params *prms)
{
    return ((FIL_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/**
 *  @brief      Delete a FIL instance object.
 *
 *  @param[in]  handle Handle for instance of FIL object.
 *
 */
/*
 *  ======== Fil_delete ========
 *  Delete a FIL instance object
 */
static inline Void FIL_delete(FIL_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/** Global function declarations */

/**
 *  @brief      Actual filter process call.
 *
 *  @param[in]  handle       Handle for instance of FIL object.
 *
 *  @param[in]  pAudioFrame  PAF Audio frame structure.
 *
 *
 *  @retval     Zero  upon successful filter operation, non-zero if error.
 *
 */
/*
 *  ======== Fil_apply ========
 */
extern Int FIL_apply(FIL_Handle handle, PAF_AudioFrame *pAudioFrame);

/**
 *  @brief      Reset the filter state memory.
 *
 *  @param[in]  handle       Handle for instance of FIL object.
 *
 *  @param[in]  pAudioFrame  PAF Audio frame structure.
 *
 *
 *  @retval     Zero upon success, non-zero if error.
 *
 *  @remarks    This function is used to �reset� the filter state memory, which is normally
 *              required when there is some filtering discontinuity.
 *
 */
/*
 *  ======== FIL_reset ========
 */
extern Int FIL_reset(FIL_Handle handle, PAF_AudioFrame *pAudioFrame);

/**
 *  @brief       Module finalization.
 *
 *  @remarks    Called during system shutdown, to perform any run-time finalization.
 *
 */
/*
 *  ======== FIL_exit ========
 *  Module finalization
 */
extern Void FIL_exit(Void);

/**
 *  @brief       Module initialization.
 *
 *  @remarks    Algorithm initialization is done here. FIL currently does nothing in 
 *              this function.
 *
 */
/*
 *  ======== FIL_init ========
 *  Module initialization
 */
extern Void FIL_init(Void);

/** @defgroup  Control_Method control method commands 
 *@{
 */
/*@{*/
/*
 * ===== control method commands =====
 */
/** 
  *  @def FIL_NULL
  * 
  */
#define FIL_NULL IFIL_NULL
/** 
  *  @def FIL_GETSTATUSADDRESS1
  * 
  */
#define FIL_GETSTATUSADDRESS1 IFIL_GETSTATUSADDRESS1
/** 
  *  @def FIL_GETSTATUSADDRESS2
  * 
  */
#define FIL_GETSTATUSADDRESS2 IFIL_GETSTATUSADDRESS2
/** 
  *  @def FIL_GETSTATUS
  * 
  */
#define FIL_GETSTATUS         IFIL_GETSTATUS
/** 
  *  @def FIL_SETSTATUS
  * 
  */
#define FIL_SETSTATUS         IFIL_SETSTATUS
/*@}*/
/** @} */

#endif  /* FIL_ */
