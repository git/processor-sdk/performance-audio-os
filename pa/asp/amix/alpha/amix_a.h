
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
// One-to-Many Audio Mixer alpha codes
//
//


#ifndef _AMIX_A
#define _AMIX_A

#include <acpbeta.h>
#include <pafmac_a.h>
#include <paftyp_a.h>

#if (PAF_MAXNUMCHAN != 16)
  #error "AMIX is only tested for PAF_MAXNUMCHAN == 16"
#endif

/*  Status structure define in iamix.h.  Here to see offsets.

typedef volatile struct IAMIX_Status {
    Int size;               // offset 0x0
    XDAS_Int8 mode;	        // offset 0x04
    XDAS_Int8 unused1;      // offset 0x05

    XDAS_Int8 stream_A_EqualBalance_ack;   // offset 0x06
    XDAS_Int8 stream_B_EqualBalance_ack;   // offset 0x07

    XDAS_Int16 stream_A_EqualBalance_set;   // offset 0x08   
    XDAS_Int16 stream_B_EqualBalance_set;   // offset 0x0A
    XDAS_Int16 stream_A_OutputGain_set;     // offset 0x0C
    XDAS_Int16 stream_B_OutputGain_set;     // offset 0x0E

    // offset 0x10 to 20F
    XDAS_Int16 gainMatrix_A[PAF_MAXNUMCHAN][PAF_MAXNUMCHAN];
    // offset 0x210 to 40F
    XDAS_Int16 gainMatrix_B[PAF_MAXNUMCHAN][PAF_MAXNUMCHAN];

    XDAS_UInt16 rampTime_A;                 // offset 0x410
    XDAS_UInt16 rampTime_B;                 // offset 0x412

} IAMIX_Status;
 */

#ifndef STD_BETA_AMIX
  #define STD_BETA_AMIX 0x85
#endif

#define  readAMIX_Mode         0xc200+STD_BETA_AMIX,0x0400
#define  readAMIXMode          readAMIX_Mode
#define writeAMIX_Mode_Disable 0xca00+STD_BETA_AMIX,0x0400
#define writeAMIXModeDisable   writeAMIX_Mode_Disable
#define writeAMIX_Mode_MixWith 0xca00+STD_BETA_AMIX,0x0401
#define writeAMIXModeMixWith   writeAMIX_Mode_MixWith
#define writeAMIX_Mode_MixOver 0xca00+STD_BETA_AMIX,0x0402
#define writeAMIXModeMixOver   writeAMIX_Mode_MixOver

#define writeAMIX_stream_A_EqualBalance_set(dBover2)  0xcb00+STD_BETA_AMIX,0x0008,dBover2 
#define writeAMIXStreamAEqualBalanceSet(dBover2)      writeAMIX_stream_A_EqualBalance_set(dBover2)
#define  readAMIXStreamAEqualBalanceSet               0xc300+STD_BETA_AMIX,0x0008,0x0000

#define writeAMIX_stream_A_EqualBalance_ack           0xca00+STD_BETA_AMIX,0x0601
#define writeAMIXStreamAEqualBalanceAck               writeAMIX_stream_A_EqualBalance_ack
#define  readAMIXStreamAEqualBalanceAck               0xc200+STD_BETA_AMIX,0x0600

#define writeAMIX_setStreamA_EqualBalance(dBover2)   \
            writeAMIX_stream_A_EqualBalance_set(dBover2), \
            writeAMIX_stream_A_EqualBalance_ack
#define writeAMIXSetStreamAEqualBalance(dBover2)      writeAMIX_setStreamA_EqualBalance(dBover2)

#define writeAMIX_stream_B_EqualBalance_set(dBover2)  0xcb00+STD_BETA_AMIX,0x000a,dBover2 
#define writeAMIXStreamBEqualBalanceSet(dBover2)      writeAMIX_stream_B_EqualBalance_set(dBover2)
#define  readAMIXStreamBEqualBalanceSet               0xc300+STD_BETA_AMIX,0x000a,0x0000

#define writeAMIX_stream_B_EqualBalance_ack           0xca00+STD_BETA_AMIX,0x0701
#define writeAMIXStreamBEqualBalanceAck               writeAMIX_stream_B_EqualBalance_ack
#define  readAMIXStreamBEqualBalanceAck               0xc200+STD_BETA_AMIX,0x0700

#define writeAMIX_setStreamB_EqualBalance(dBover2)   \
            writeAMIX_stream_B_EqualBalance_set(dBover2), \
            writeAMIX_stream_B_EqualBalance_ack
#define writeAMIXSetStreamBEqualBalance(dBover2)      writeAMIX_setStreamB_EqualBalance(dBover2)

#define writeAMIX_stream_A_OutputGain_set(dBover2)    0xcb00+STD_BETA_AMIX,0x000c,dBover2 
#define writeAMIXStreamAOutputGainSet(dBover2)        writeAMIX_stream_A_OutputGain_set(dBover2)
#define  readAMIXStreamAOutputGainSet                 0xc300+STD_BETA_AMIX,0x000c,0x0000

#define writeAMIX_stream_B_OutputGain_set(dBover2)    0xcb00+STD_BETA_AMIX,0x000E,dBover2 
#define writeAMIXStreamBOutputGainSet(dBover2)        writeAMIX_stream_B_OutputGain_set(dBover2)
#define  readAMIXStreamBOutputGainSet                 0xc300+STD_BETA_AMIX,0x000e,0x0000

// alpha type 5-6
#define writeAMIX_stream_A_InputGain(inputChan,G0,G1,G2,G3,G4,G5,G6,G7,G8,G9,Ga,Gb,Gc,Gd,Ge,Gf) \
            0xcd00+6, STD_BETA_AMIX, \
            0x10+32*inputChan, 0x20, /* base, offset, numBytes */ \
            G0,G1,G2,G3,G4,G5,G6,G7,G8,G9,Ga,Gb,Gc,Gd,Ge,Gf /* data */
#define writeAMIXStreamAInputGain(inputChan,G0,G1,G2,G3,G4,G5,G6,G7,G8,G9,Ga,Gb,Gc,Gd,Ge,Gf) \
            writeAMIX_stream_A_InputGain(inputChan,G0,G1,G2,G3,G4,G5,G6,G7,G8,G9,Ga,Gb,Gc,Gd,Ge,Gf)
#define  readAMIXStreamAInputGain1                    0xc506,STD_BETA_AMIX,0x0010,0x0100
#define  readAMIXStreamAInputGain2                    0xc506,STD_BETA_AMIX,0x0110,0x0100

#define writeAMIX_stream_B_InputGain(inputChan,G0,G1,G2,G3,G4,G5,G6,G7,G8,G9,Ga,Gb,Gc,Gd,Ge,Gf) \
            0xcd00+6, STD_BETA_AMIX, \
            0x210+32*inputChan, 0x20, /* base, offset, numBytes */ \
            G0,G1,G2,G3,G4,G5,G6,G7,G8,G9,Ga,Gb,Gc,Gd,Ge,Gf 
#define writeAMIXStreamBInputGain(inputChan,G0,G1,G2,G3,G4,G5,G6,G7,G8,G9,Ga,Gb,Gc,Gd,Ge,Gf) \
            writeAMIX_stream_B_InputGain(inputChan,G0,G1,G2,G3,G4,G5,G6,G7,G8,G9,Ga,Gb,Gc,Gd,Ge,Gf)
#define  readAMIXStreamBInputGain1                    0xc506,STD_BETA_AMIX,0x0210,0x0100
#define  readAMIXStreamBInputGain2                    0xc506,STD_BETA_AMIX,0x0310,0x0100

#define readAMIXRampTimeA                             0xc300+STD_BETA_AMIX,0x0410
#define writeAMIXRampTimeA(wValue)                    0xcb00+STD_BETA_AMIX,0x0410,(0x0ffff&wValue)

#define readAMIXRampTimeB                             0xc300+STD_BETA_AMIX,0x0412
#define writeAMIXRampTimeB(wValue)                    0xcb00+STD_BETA_AMIX,0x0412,(0x0ffff&wValue)

#endif /* _AMIX_A */

