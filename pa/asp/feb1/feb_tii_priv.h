
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  Internal vendor specific (TII) interface header for FEB
 *  algorithm. Only the implementation source files include
 *  this header.
 *
 *  This header contains declarations that are specific to this
 *  implementation and which do not need to be exposed in order for
 *  an application to use the FIL-ASP Example Demonstration algorithm.
 */
#ifndef FEB_TII_PRIV_
#define FEB_TII_PRIV_

#include <ialg.h>
#include <log.h>

#include "ifeb.h"
#include "com_tii_priv.h"
#include "fil_tii_priv.h"

typedef struct FEB_TII_Obj {
    IALG_Obj alg;           /* MUST be first field of all XDAS algs */
	int mask;
    FEB_TII_Status *pStatus;  /* public interface */
    FEB_TII_Config config;    /* private interface */
    FIL_Handle filHandle;     /* Pointer to the FIL object handle */
    PAF_FilCoef_Void filCoef; /* FIL coef structure */
    Float coef[41];           /* Total 4*10bands + 1(gain) coefficient */
} FEB_TII_Obj;

#define FEB_TII_activate COM_TII_activate

#define FEB_TII_deactivate COM_TII_deactivate

extern Int FEB_TII_alloc(const IALG_Params *algParams, IALG_Fxns **pf,
                        IALG_MemRec memTab[]);

extern Int FEB_TII_free(IALG_Handle handle, IALG_MemRec memTab[]);

#define FEB_TII_control COM_TII_control

extern Int FEB_TII_initObj(IALG_Handle handle,
                          const IALG_MemRec memTab[], IALG_Handle parent,
                          const IALG_Params *algParams);
                
#define FEB_TII_moved COM_TII_moved
                
extern Int FEB_TII_apply(IFEB_Handle, PAF_AudioFrame *);

extern Int FEB_TII_reset(IFEB_Handle, PAF_AudioFrame *);



extern Int  FEB_TII_numAlloc();

extern Double  gFEB_EQFreqs[10]; 

extern Double gByEqSampleRate[15];

#endif  /* FEB_TII_PRIV_ */
