/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/* 
    Memory regions.
    This command file uses the memory layout of C6424 by default.
    If executing on hardware(non C6424) and not simulator, please
    adjust the below definitions to match your target
    memory map.
    See the manual of your target chip for memory mapping details.
*/

--args 1024
-c
-stack 0x60000
-heap 0x80000
-m dlb_ti_c6x.map


MEMORY
{
    L2RAM:      o = 0x10800000  l = 0x00010000
    DDR3:       o = 0x80000000  l = 0x10000000
}

SECTIONS
{
    .args       >   DDR3
    .bss        >   DDR3 
    .cinit      >   DDR3
    .cio        >   DDR3
    .const      >   DDR3
    .data       >   DDR3
    .far        >   DDR3
    .stack      >   DDR3
    .switch     >   DDR3
    .sysmem     >   DDR3
    .text       >   DDR3
    
    .ddr2       >   DDR3
}

