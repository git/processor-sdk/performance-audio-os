;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v6.1.5 *
;* Date/Time created: Thu Oct 23 14:03:20 2008                                *
;******************************************************************************
	.compiler_opts --c64p_l1d_workaround=off --endian=little --hll_source=on --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --quiet --silicon_version=6740 --symdebug:skeletal 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("DINKUMLIB/source/xfsin.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v6.1.5 Copyright (c) 1996-2008 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\lib")

$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("fmodf")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_fmodf")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$16)
$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$16)
	.dwendtag $C$DW$1


$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("_Feraise")
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("__Feraise")
	.dwattr $C$DW$4, DW_AT_declaration
	.dwattr $C$DW$4, DW_AT_external
$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$10)
	.dwendtag $C$DW$4


$C$DW$6	.dwtag  DW_TAG_subprogram, DW_AT_name("_FDtest")
	.dwattr $C$DW$6, DW_AT_TI_symbol_name("__FDtest")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$6, DW_AT_declaration
	.dwattr $C$DW$6, DW_AT_external
$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$23)
	.dwendtag $C$DW$6

$C$DW$8	.dwtag  DW_TAG_variable, DW_AT_name("_FNan")
	.dwattr $C$DW$8, DW_AT_TI_symbol_name("__FNan")
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$8, DW_AT_declaration
	.dwattr $C$DW$8, DW_AT_external
$C$DW$9	.dwtag  DW_TAG_variable, DW_AT_name("_FRteps")
	.dwattr $C$DW$9, DW_AT_TI_symbol_name("__FRteps")
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$9, DW_AT_declaration
	.dwattr $C$DW$9, DW_AT_external
	.sect	".const:_c"
	.clink
	.align	8
_c:
	.word	0bab24993h		; _c[0] @ 0
	.word	03d2aa036h		; _c[1] @ 32
	.word	0beffffdfh		; _c[2] @ 64

$C$DW$10	.dwtag  DW_TAG_variable, DW_AT_name("c")
	.dwattr $C$DW$10, DW_AT_TI_symbol_name("_c")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_addr _c]
	.sect	".const:_s"
	.clink
	.align	8
_s:
	.word	0b94c8c6eh		; _s[0] @ 0
	.word	03c088342h		; _s[1] @ 32
	.word	0be2aaaa1h		; _s[2] @ 64

$C$DW$11	.dwtag  DW_TAG_variable, DW_AT_name("s")
	.dwattr $C$DW$11, DW_AT_TI_symbol_name("_s")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_addr _s]
	.sect	".const"
	.align	4
_c1:
	.word	03fc90fdah		; _c1 @ 0

$C$DW$12	.dwtag  DW_TAG_variable, DW_AT_name("c1")
	.dwattr $C$DW$12, DW_AT_TI_symbol_name("_c1")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_addr _c1]
	.sect	".const"
	.align	4
_c2:
	.word	033a22169h		; _c2 @ 0

$C$DW$13	.dwtag  DW_TAG_variable, DW_AT_name("c2")
	.dwattr $C$DW$13, DW_AT_TI_symbol_name("_c2")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_addr _c2]
	.sect	".const"
	.align	4
_twobypi:
	.word	03f22f983h		; _twobypi @ 0

$C$DW$14	.dwtag  DW_TAG_variable, DW_AT_name("twobypi")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_twobypi")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_addr _twobypi]
	.sect	".const"
	.align	4
_twopi:
	.word	040c90fdbh		; _twopi @ 0

$C$DW$15	.dwtag  DW_TAG_variable, DW_AT_name("twopi")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_twopi")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_addr _twopi]
;	c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\bin\opt6x.exe OBJ_CYGWIN_RTS6740_LIB\\xfsin.if OBJ_CYGWIN_RTS6740_LIB\\xfsin.opt 
	.sect	".text:__FSin"
	.clink
	.global	__FSin

$C$DW$16	.dwtag  DW_TAG_subprogram, DW_AT_name("_FSin")
	.dwattr $C$DW$16, DW_AT_low_pc(__FSin)
	.dwattr $C$DW$16, DW_AT_high_pc(0x00)
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("__FSin")
	.dwattr $C$DW$16, DW_AT_external
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$16, DW_AT_TI_begin_file("DINKUMLIB\source\xxxsin.h")
	.dwattr $C$DW$16, DW_AT_TI_begin_line(0xa4)
	.dwattr $C$DW$16, DW_AT_TI_begin_column(0x07)
	.dwattr $C$DW$16, DW_AT_frame_base[DW_OP_breg31 40]
	.dwattr $C$DW$16, DW_AT_TI_skeletal
	.dwattr $C$DW$16, DW_AT_TI_category("TI Library")
	.dwpsn	file "DINKUMLIB\source\xxxsin.h",line 165,column 2,is_stmt,address __FSin
$C$DW$17	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg4]
$C$DW$18	.dwtag  DW_TAG_formal_parameter, DW_AT_name("qoff")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_qoff")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg20]

;******************************************************************************
;* FUNCTION NAME: _FSin                                                       *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,SP,A16,A17,A18,  *
;*                           A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30, *
;*                           A31,B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26, *
;*                           B27,B28,B29,B30,B31                              *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,SP,A16,A17,A18,  *
;*                           A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30, *
;*                           A31,B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26, *
;*                           B27,B28,B29,B30,B31                              *
;*   Local Frame Size  : 0 Args + 4 Auto + 32 Save = 36 byte                  *
;******************************************************************************
__FSin:
;** --------------------------------------------------------------------------*
           STW     .D2T1   A11,*SP--(8)      ; |165| 
           STDW    .D2T2   B11:B10,*SP--     ; |165| 
           STDW    .D2T1   A13:A12,*SP--     ; |165| 
           STW     .D2T1   A10,*SP--(8)      ; |165| 

           SUB     .L2     SP,8,SP           ; |165| 
||         MV      .L1X    B4,A11            ; |165| 

           MV      .L1X    B3,A13            ; |165| 
$C$DW$19	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$19, DW_AT_low_pc(0x00)
	.dwattr $C$DW$19, DW_AT_name("__FDtest")
	.dwattr $C$DW$19, DW_AT_TI_call

           CALLP   .S2     __FDtest,B3
||         STW     .D2T1   A4,*+SP(4)        ; |165| 
||         ADD     .L1X    4,SP,A4           ; |169| 

$C$RL0:    ; CALL OCCURS {__FDtest} {0}      ; |169| 
;** --------------------------------------------------------------------------*
           MV      .L1     A4,A0             ; |169| 

   [!A0]   B       .S1     $C$L1             ; |169| 
||         MV      .D1     A0,A2             ; guard predicate rewrite
||         CMPEQ   .L1     A4,1,A1           ; |169| 
|| [ A0]   MVKL    .S2     __FNan,B10

   [!A2]   ZERO    .S1     A1                ; |169| nullify predicate
|| [ A0]   MVKH    .S2     __FNan,B10
|| [ A0]   MVK     .D1     0x1,A4            ; |176| 
|| [ A0]   CMPEQ   .L1     A0,2,A0           ; |169| 

   [ A1]   BNOP    .S1     $C$L2,3           ; |169| 
           ; BRANCHCC OCCURS {$C$L1}         ; |169| 
;** --------------------------------------------------------------------------*

   [ A1]   ZERO    .L1     A0                ; |172| nullify predicate
|| [!A1]   LDW     .D2T1   *+SP(4),A4        ; |172| 

   [ A0]   B       .S1     $C$L8             ; |169| 
           ; BRANCHCC OCCURS {$C$L2}         ; |169| 
;** --------------------------------------------------------------------------*
   [!A0]   BNOP    .S1     $C$L3,4           ; |169| 
           ; BRANCHCC OCCURS {$C$L8}         ; |169| 
;** --------------------------------------------------------------------------*

           LDW     .D2T1   *+SP(4),A12       ; |179| 
||         MVKL    .S1     0x9999999a,A6
||         MVKL    .S2     0x40445999,B5

           ; BRANCH OCCURS {$C$L3}           ; |169| 
;** --------------------------------------------------------------------------*
$C$L1:    

           ZERO    .L2     B4
||         MV      .L1     A11,A0            ; |174| 
||         MV      .S2X    A13,B3            ; |202| 

           SET     .S2     B4,0x17,0x1d,B4
|| [!A0]   LDW     .D2T2   *+SP(4),B4        ; |174| 
||         ADD     .L2     8,SP,SP           ; |202| 

           LDW     .D2T1   *++SP(8),A10      ; |202| 
           LDDW    .D2T1   *++SP,A13:A12     ; |202| 
$C$DW$20	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$20, DW_AT_low_pc(0x04)
	.dwattr $C$DW$20, DW_AT_TI_return

           LDDW    .D2T2   *++SP,B11:B10     ; |202| 
||         RET     .S2     B3                ; |202| 

           LDW     .D2T1   *++SP(8),A11      ; |202| 
           MV      .L1X    B4,A4             ; |174| 
           NOP             3
           ; BRANCH OCCURS {B3}              ; |202| 
;** --------------------------------------------------------------------------*
$C$L2:    
$C$DW$21	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$21, DW_AT_low_pc(0x00)
	.dwattr $C$DW$21, DW_AT_name("__Feraise")
	.dwattr $C$DW$21, DW_AT_TI_call
           CALL    .S1     __Feraise         ; |176| 
           ADDKPC  .S2     $C$RL1,B3,4       ; |176| 
$C$RL1:    ; CALL OCCURS {__Feraise} {0}     ; |176| 
;** --------------------------------------------------------------------------*

           ADD     .L2     8,SP,SP           ; |202| 
||         LDW     .D2T1   *B10,A4           ; |177| 

           LDW     .D2T1   *++SP(8),A10      ; |202| 

           MV      .L2X    A13,B3            ; |202| 
||         LDDW    .D2T1   *++SP,A13:A12     ; |202| 

$C$DW$22	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$22, DW_AT_low_pc(0x04)
	.dwattr $C$DW$22, DW_AT_TI_return

           LDDW    .D2T2   *++SP,B11:B10     ; |202| 
||         RET     .S2     B3                ; |202| 

           LDW     .D2T1   *++SP(8),A11      ; |202| 
           NOP             4
           ; BRANCH OCCURS {B3}              ; |202| 
;** --------------------------------------------------------------------------*
$C$L3:    
           MVKH    .S2     0x40445999,B5
           MVKL    .S1     0xc0445999,A7
           MVKH    .S1     0x9999999a,A6
           MVKH    .S1     0xc0445999,A7
           SPDP    .S1     A12,A5:A4         ; |179| 
           MV      .L2X    A6,B4             ; |179| 
           CMPLTDP .S1     A5:A4,A7:A6,A3    ; |179| 
           CMPGTDP .S2X    A5:A4,B5:B4,B5    ; |179| 
           MV      .L1     A12,A4            ; |180| 
           OR      .L2X    B5,A3,B0          ; |179| 
   [!B0]   MVKL    .S1     0x3f22f983,A3

   [!B0]   MVKH    .S1     0x3f22f983,A3
|| [!B0]   B       .S2     $C$L4             ; |179| 

$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_name("_fmodf")
	.dwattr $C$DW$23, DW_AT_TI_call

   [ B0]   CALL    .S1     _fmodf            ; |180| 
|| [!B0]   MPYSP   .M1     A3,A12,A3         ; |181| 

           MVKL    .S2     0x40c90fdb,B4
           MVKH    .S2     0x40c90fdb,B4
   [!B0]   ZERO    .L1     A4                ; |182| 
   [!B0]   CMPGTSP .S1     A3,A4,A0          ; |182| 
           ; BRANCHCC OCCURS {$C$L4}         ; |179| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL2,B3,0       ; |180| 
$C$RL2:    ; CALL OCCURS {_fmodf} {0}        ; |180| 
;** --------------------------------------------------------------------------*
           MVKL    .S1     0x3f22f983,A3

           MVKH    .S1     0x3f22f983,A3
||         MV      .L1     A4,A12            ; |180| 

           MPYSP   .M1     A3,A12,A3         ; |181| 
           ZERO    .L1     A4                ; |182| 
           NOP             2
           CMPGTSP .S1     A3,A4,A0          ; |182| 
;** --------------------------------------------------------------------------*
$C$L4:    

           ZERO    .L2     B4
|| [!A0]   B       .S1     $C$L5             ; |182| 

$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("__fixfli")
	.dwattr $C$DW$24, DW_AT_TI_call

   [ A0]   CALL    .S1     __fixfli          ; |182| 
||         SET     .S2     B4,0x18,0x1d,B4

           NOP             1
           ADDSP   .L1X    B4,A3,A4          ; |182| 
$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_name("__fixfli")
	.dwattr $C$DW$25, DW_AT_TI_call
   [!A0]   CALL    .S1     __fixfli          ; |182| 
           NOP             1
           ; BRANCHCC OCCURS {$C$L5}         ; |182| 
;** --------------------------------------------------------------------------*
           ADDKPC  .S2     $C$RL3,B3,0       ; |182| 
$C$RL3:    ; CALL OCCURS {__fixfli} {0}      ; |182| 
;** --------------------------------------------------------------------------*
           B       .S1     $C$L6             ; |182| 
           CALL    .S1     __fltlif          ; |185| 
           MV      .L1     A4,A10            ; |182| 
           NOP             3
           ; BRANCH OCCURS {$C$L6}           ; |182| 
;** --------------------------------------------------------------------------*
$C$L5:    
           SUBSP   .L1X    A3,B4,A4          ; |182| 
           ADDKPC  .S2     $C$RL4,B3,2       ; |182| 
$C$RL4:    ; CALL OCCURS {__fixfli} {0}      ; |182| 
;** --------------------------------------------------------------------------*
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("__fltlif")
	.dwattr $C$DW$26, DW_AT_TI_call
           CALL    .S1     __fltlif          ; |185| 
           MV      .L1     A4,A10            ; |182| 
           NOP             3
;** --------------------------------------------------------------------------*
$C$L6:    
           ADDKPC  .S2     $C$RL5,B3,0       ; |185| 
$C$RL5:    ; CALL OCCURS {__fltlif} {0}      ; |185| 
;** --------------------------------------------------------------------------*

           MVKL    .S1     0x3fc90fda,A3
||         MVKL    .S2     0x33a22169,B4
||         AND     .L2X    3,A10,B31         ; |182| 
||         ZERO    .D2     B7                ; |186| 

           MVKH    .S1     0x3fc90fda,A3
||         MVKH    .S2     0x33a22169,B4
||         MV      .L2X    A11,B30           ; |185| 

           MPYSP   .M1     A3,A4,A3          ; |185| 
           MVKL    .S2     __FRteps,B5
           MPYSP   .M2X    B4,A4,B4          ; |185| 
           MVKH    .S2     __FRteps,B5
           SUBSP   .L1     A12,A3,A3         ; |185| 
           SET     .S2     B7,31,31,B7       ; |186| 
           LDW     .D2T2   *B5,B6            ; |186| 
           MVKL    .S2     _s,B8

           SUBSP   .L1X    A3,B4,A3          ; |185| 
||         ADDU    .L2     B31,B30,B5:B4     ; |182| 

           MVKH    .S2     _s,B8
           MV      .L1X    B4,A11            ; |182| 
           XOR     .L2     B6,B7,B29         ; |186| 

           CMPLTSP .S1X    A3,B6,A4          ; |186| 
||         MV      .L2X    A11,B4            ; |186| 

           CMPLTSP .S2X    B29,A3,B5         ; |186| 
||         AND     .L2     1,B4,B0           ; |195| 

           AND     .L2X    A4,B5,B1          ; |186| 
||         MPYSP   .M1     A3,A3,A4          ; |193| 

   [ B1]   MVK     .L2     0x1,B0            ; |196| 
   [!B0]   LDW     .D2T2   *B8,B5            ; |198| 
   [!B1]   ADD     .L1X    4,B8,A10
   [!B0]   LDNDW   .D1T1   *A10,A7:A6        ; |198| 
           AND     .L2     1,B4,B2           ; |188| 
   [!B1]   ZERO    .L2     B2                ; |189| 
   [!B0]   MPYSP   .M2X    A4,B5,B5          ; |198| 
   [ B1]   ZERO    .L1     A12
   [ B1]   SET     .S1     A12,0x17,0x1d,A12
   [ B2]   MV      .L1     A12,A3            ; |189| 
   [!B1]   MVKL    .S2     _c+4,B11
   [!B0]   ADDSP   .L1X    A6,B5,A5          ; |198| 
   [!B1]   ZERO    .L2     B10
   [!B1]   MVKH    .S2     _c+4,B11
   [!B1]   SET     .S2     B10,0x17,0x1d,B10
   [!B0]   MPYSP   .M1     A4,A5,A5          ; |198| 
   [!B1]   MVKL    .S1     _c,A10
   [!B1]   MVKH    .S1     _c,A10
           NOP             1

   [!B0]   ADDSP   .L1     A7,A5,A6          ; |198| 
||         MPYSP   .M1     A4,A3,A5          ; |198| 

           NOP             3
   [!B0]   MPYSP   .M1     A6,A5,A5          ; |198| 
           NOP             3

   [!B0]   ADDSP   .L1     A5,A3,A3          ; |198| 
|| [ B1]   ZERO    .L2     B0                ; |198| 

   [!B0]   BNOP    .S1     $C$L7,3           ; |198| 
|| [ B0]   LDW     .D1T1   *A10,A3           ; |196| 

   [ B0]   LDNDW   .D2T2   *B11,B5:B4        ; |196| 
   [ B0]   MPYSP   .M1     A4,A3,A3          ; |196| 
           ; BRANCHCC OCCURS {$C$L7}         ; |198| 
;** --------------------------------------------------------------------------*
           NOP             3
           ADDSP   .L1X    B4,A3,A3          ; |196| 
           NOP             3
           MPYSP   .M1     A4,A3,A3          ; |196| 
           NOP             3
           ADDSP   .L1X    B5,A3,A3          ; |196| 
           NOP             3
           MPYSP   .M1     A4,A3,A3          ; |196| 
           NOP             3
           ADDSP   .L1X    B10,A3,A3         ; |196| 
;** --------------------------------------------------------------------------*
$C$L7:    

           ZERO    .L1     A4                ; |200| 
||         MV      .L2X    A11,B4            ; |200| 

           AND     .L2     2,B4,B0           ; |200| 
           SET     .S1     A4,31,31,A4       ; |200| 
           XOR     .L1     A3,A4,A4          ; |200| 
   [!B0]   MV      .L1     A3,A4             ; |200| 
;** --------------------------------------------------------------------------*
$C$L8:    
           ADD     .L2     8,SP,SP           ; |202| 
           LDW     .D2T1   *++SP(8),A10      ; |202| 

           MV      .L2X    A13,B3            ; |202| 
||         LDDW    .D2T1   *++SP,A13:A12     ; |202| 

$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x04)
	.dwattr $C$DW$27, DW_AT_TI_return

           LDDW    .D2T2   *++SP,B11:B10     ; |202| 
||         RET     .S2     B3                ; |202| 

           LDW     .D2T1   *++SP(8),A11      ; |202| 
	.dwpsn	file "DINKUMLIB\source\xxxsin.h",line 202,column 2,is_stmt
           NOP             4
           ; BRANCH OCCURS {B3}              ; |202| 
	.dwattr $C$DW$16, DW_AT_TI_end_file("DINKUMLIB\source\xxxsin.h")
	.dwattr $C$DW$16, DW_AT_TI_end_line(0xca)
	.dwattr $C$DW$16, DW_AT_TI_end_column(0x02)
	.dwendtag $C$DW$16

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_fmodf
	.global	__Feraise
	.global	__FDtest
	.global	__FNan
	.global	__FRteps
	.global	__fixfli
	.global	__fltlif

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)

$C$DW$T$19	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$19, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$19, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x10)
$C$DW$28	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$28, DW_AT_upper_bound(0x07)
	.dwendtag $C$DW$T$19

$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("_Wchart")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
$C$DW$T$28	.dwtag  DW_TAG_typedef, DW_AT_name("_Wintt")
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$28, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("ptrdiff_t")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("_Ptrdifft")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("size_t")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("_Sizet")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("_Int32t")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$35	.dwtag  DW_TAG_typedef, DW_AT_name("_Uint32t")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$35, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("_Longlong")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("_ULonglong")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$23	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$23, DW_AT_address_class(0x20)
$C$DW$T$41	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$16)

$C$DW$T$42	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$T$42, DW_AT_byte_size(0x0c)
$C$DW$29	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$29, DW_AT_upper_bound(0x02)
	.dwendtag $C$DW$T$42

$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

$C$DW$T$20	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x10)
$C$DW$30	.dwtag  DW_TAG_member
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$30, DW_AT_name("_Word")
	.dwattr $C$DW$30, DW_AT_TI_symbol_name("__Word")
	.dwattr $C$DW$30, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$30, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$31	.dwtag  DW_TAG_member
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$31, DW_AT_name("_Float")
	.dwattr $C$DW$31, DW_AT_TI_symbol_name("__Float")
	.dwattr $C$DW$31, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$31, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$32	.dwtag  DW_TAG_member
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$32, DW_AT_name("_Double")
	.dwattr $C$DW$32, DW_AT_TI_symbol_name("__Double")
	.dwattr $C$DW$32, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$32, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$33	.dwtag  DW_TAG_member
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$18)
	.dwattr $C$DW$33, DW_AT_name("_Long_double")
	.dwattr $C$DW$33, DW_AT_TI_symbol_name("__Long_double")
	.dwattr $C$DW$33, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$33, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("_Dconst")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$49	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$49, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$49, DW_AT_name("signed char")
	.dwattr $C$DW$T$49, DW_AT_byte_size(0x01)
$C$DW$T$50	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$50, DW_AT_address_class(0x20)
$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("va_list")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("_Va_list")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)
$C$DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("_Sysch_t")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$34	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg0]
$C$DW$35	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg1]
$C$DW$36	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg2]
$C$DW$37	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg3]
$C$DW$38	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg4]
$C$DW$39	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg5]
$C$DW$40	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg6]
$C$DW$41	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$41, DW_AT_location[DW_OP_reg7]
$C$DW$42	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_reg8]
$C$DW$43	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_reg9]
$C$DW$44	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg10]
$C$DW$45	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg11]
$C$DW$46	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg12]
$C$DW$47	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_reg13]
$C$DW$48	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg14]
$C$DW$49	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg15]
$C$DW$50	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg16]
$C$DW$51	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg17]
$C$DW$52	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg18]
$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg19]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg20]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg21]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg22]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg23]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg24]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg25]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg26]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg27]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg28]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg29]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg30]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg31]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x20]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x21]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x22]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x23]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x24]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x25]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_regx 0x26]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_regx 0x27]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_regx 0x28]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_regx 0x29]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_regx 0x30]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_regx 0x31]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_regx 0x32]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_regx 0x33]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_regx 0x34]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_regx 0x35]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_regx 0x36]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x37]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x38]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x39]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x40]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x41]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x42]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x43]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x44]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x45]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x46]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x47]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x48]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x49]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_regx 0x50]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_regx 0x51]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_regx 0x52]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_regx 0x53]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_regx 0x54]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_regx 0x55]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_regx 0x56]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_regx 0x57]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_regx 0x58]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_regx 0x59]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x60]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x61]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x62]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x63]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x64]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_regx 0x65]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_regx 0x66]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_regx 0x67]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_regx 0x68]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_regx 0x69]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x70]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x71]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_regx 0x72]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x73]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_regx 0x74]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_regx 0x75]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_regx 0x76]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_regx 0x77]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_regx 0x78]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x79]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$157	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$158	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$159	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$160	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$161	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$162	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

