/* 
 *
 * Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h> 
#include <stdlib.h> 

#ifndef _GLOBAL_H
#define _GLOBAL_H

enum datapath_types { bad_path, a_path, b_path };

enum load_store_types 
{
  bad_ls_type, 
  load_type, 
  store_type, 
  periph_load_type,   // memory read from outside cpu (eg. DMA)
  periph_store_type   // memory write "     "      "   "    "
};

typedef unsigned uword;
typedef int word;

typedef struct {
  uword high;
  uword low;
} dword;

typedef struct {
  uword word0;
  uword word1;
  uword word2;
  uword word3;
} qword;

#if !defined(AVOID_LONG_LONG)
typedef struct {
  long long  low ;
  long long  high ;
} two_longlong ;
#else
typedef struct {
  int64_ll low ;
  int64_ll high ;
} two_longlong ;
#endif

typedef struct {
  word SAT ;
  word EN ;
} csr_type;

typedef struct {
  unsigned int NAN1ADD1 : 1;
  unsigned int NAN2ADD1 : 1;
  unsigned int DEN1ADD1 : 1;
  unsigned int DEN2ADD1 : 1;
  unsigned int INVALADD1 : 1;
  unsigned int INFOADD1 : 1;
  unsigned int OVERADD1 : 1;
  unsigned int INEXADD1 : 1;
  unsigned int UNDERADD1 : 1;
  unsigned int ROUNDINGMODEADD1 : 2;
  unsigned int RSV1 : 5;
  unsigned int NAN1ADD2 : 1;
  unsigned int NAN2ADD2 : 1;
  unsigned int DEN1ADD2 : 1;
  unsigned int DEN2ADD2 : 1;
  unsigned int INVALADD2 : 1;
  unsigned int INFOADD2 : 1;
  unsigned int OVERADD2 : 1;
  unsigned int INEXADD2 : 1;
  unsigned int UNDERADD2 : 1;
  unsigned int ROUNDINGMODEADD2 : 2;
  unsigned int RSV2 : 5;
} fadcr_type ;

typedef struct {
  unsigned int NAN1AU1 : 1;
  unsigned int NAN2AU1 : 1;
  unsigned int DEN1AU1 : 1;
  unsigned int DEN2AU1 : 1;
  unsigned int INVALAU1 : 1;
  unsigned int INFOAU1 : 1;
  unsigned int OVERAU1 : 1;
  unsigned int INEXAU1 : 1;
  unsigned int UNDERAU1 : 1;
  unsigned int UNORDAU1 : 1;
  unsigned int DIV0AU1 : 1;
  unsigned int RSV1 : 5;
  unsigned int NAN1AU2 : 1;
  unsigned int NAN2AU2 : 1;
  unsigned int DEN1AU2 : 1;
  unsigned int DEN2AU2 : 1;
  unsigned int INVALAU2 : 1;
  unsigned int INFOAU2 : 1;
  unsigned int OVERAU2 : 1;
  unsigned int INEXAU2 : 1;
  unsigned int UNDERAU2 : 1;
  unsigned int UNORDAU2 : 1;
  unsigned int DIV0AU2 : 1;
  unsigned int RSV2 : 5;
} faucr_type ;

typedef struct {
  unsigned int NAN1MPY1 : 1;
  unsigned int NAN2MPY1 : 1;
  unsigned int DEN1MPY1 : 1;
  unsigned int DEN2MPY1 : 1;
  unsigned int INVALMPY1 : 1;
  unsigned int INFOMPY1 : 1;
  unsigned int OVERMPY1 : 1;
  unsigned int INEXMPY1 : 1;
  unsigned int UNDERMPY1 : 1;
  unsigned int ROUNDINGMODEMPY1 : 2;
  unsigned int RSV1 : 5;
  unsigned int NAN1MPY2 : 1;
  unsigned int NAN2MPY2 : 1;
  unsigned int DEN1MPY2 : 1;
  unsigned int DEN2MPY2 : 1;
  unsigned int INVALMPY2 : 1;
  unsigned int INFOMPY2 : 1;
  unsigned int OVERMPY2 : 1;
  unsigned int INEXMPY2 : 1;
  unsigned int UNDERMPY2 : 1;
  unsigned int ROUNDINGMODEMPY2 : 2;
  unsigned int RSV2 : 5;
} fmcr_type ;

#if !defined(NO_GLOBAL_DATA_DECLS)
extern csr_type        CSR    ;
extern fadcr_type      FADCR  ;
extern faucr_type      FAUCR  ;
extern fmcr_type       FMCR   ;
extern word            GPLYA  ;
extern word            GPLYB  ;
extern word            GFPGFR ;

extern datapath_types datapath;
#endif

#endif
