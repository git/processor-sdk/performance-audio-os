/******************************************************************************
* Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// DCS private API
//


#ifndef __DCS7_PRIV_H__
#define __DCS7_PRIV_H__

#include <xdc/std.h>
//#include "dcs7.h"

/* Macros */
// Alpha command macro
#define ALPHA_CMD_ENCAPSULATION            0xC900
#define ALPHA_CMD_ENCAP_HIGH_BYTE          0xC9
#define ALPHA_CMD_ENCAP_LOW_BYTE           0x00
// Short Mask
#define SHORT_HIGH_BYTE_MASK               0xFF00
#define SHORT_LOW_BYTE_MASK                0x00FF
// Serial Link (SL) contorl macros
#define SL_MASK                            0xF001
#define SL_SIZE                            2
#define SLC_MASK                           0x8001
#define SLC_RESET_QUERY                    0x8001
#define SLC_READY_QUERY                    0x8101
#define SLS_MASK                           0xF001
#define SLS_OK                             0xF001
#define SLS_SLC_NOT_DEFINED                0xF201

// Srecord Macros
// srecord_size sizes in Bytes/characters
#define ASCII_SRECORD_START_SIZE     12
#define ASCII_SRECORD_STOP_SIZE      12
#define ASCII_SRECORD_S1_MAX_SIZE    72  // ASCII_SRECORD_TYP_SIZE +
                                         // ASCII_SRECORD_CNT_SIZE +
                                         // ASCII_SRECORD_CSM_SIZE +
                                         // ASCII_SRECORD_S1_ADD_SIZE +
                                         // <MAX data size = 30*2(15 alpha words)>+
                                         // MSC_CRLF_SIZE

// Srecord count
// One S1 record can have max 30 words.
#define SRECORD_REC_MAX_S1_CNT   18      // Max number of S1 records that can be
                                         // present in received alpha command
#define SRECORD_RES_MAX_S1_CNT   18      // Max number of S1 records that can exist
                                         // in reponse.


// UART operation mode
// Indicates the operation due to which the UART
// Callback function was called
#define UART_CALLBACK_READ    0
#define UART_CALLBACK_WRITE   1
#define UART_CALLBACK_CANCEL  2


/* Functions declarations */
// OS dependent function
Uint32 DCS7_msToTicks(DCS7_Handle handle, Uint32);
void* DCS7_semAlloc(DCS7_Handle handle);
Uint32 DCS7_semPend(DCS7_Handle handle, void*);
Uint32 DCS7_semPendTimeout(DCS7_Handle handle, void*,Uint32);
void DCS7_semPost(DCS7_Handle handle, void*);
void DCS7_semReset(DCS7_Handle handle, void*, Uint32);
void* DCS7_memAlloc(DCS7_Handle handle, void*, Uint32, Uint32);
void DCS7_taskDisable(DCS7_Handle handle);
void DCS7_taskEnable(DCS7_Handle handle);
void DCS7_configureInt(DCS7_Handle handle);
void DCS7_cacheInv(DCS7_Handle handle, void*, Uint32);
void DCS7_cacheWbInv(DCS7_Handle handle, void*, Uint32);

// DCS functions
void DCS7_resAllocSPI(DCS7_Handle handle);
void DCS7_resAllocI2C(DCS7_Handle handle);
Uint32 DCS7_resAllocUART(DCS7_Handle handle);
Uint32 DCS7_resAlloc(DCS7_Handle handle);
void DCS7_errorRecover(DCS7_Handle handle);
void DCS7_slc(DCS7_Handle handle);
Uint32 DCS7_configureSPI(DCS7_Handle handle);
Uint32 DCS7_configureI2C(DCS7_Handle handle);
Uint32 DCS7_configureUART(DCS7_Handle handle);
Uint32 DCS7_configure(DCS7_Handle handle);
void DCS7_resetSPI(DCS7_Handle handle);
void DCS7_resetI2C(DCS7_Handle handle);
void DCS7_resetUART(DCS7_Handle handle);
void DCS7_reset(DCS7_Handle handle);
void DCS7_errorInit();
void DCS7_errorAssert();
void DCS7_errorDeassert();


#endif
