/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/*
 *  ======== fil_table.c ========
 *  FIL tables file.  This contains all info about filter functions.
 */

/* Std header files */ 
#include <xdc/std.h>
#include <ti/xdais/xdas.h>

/* Header files */
#include <ifil.h>
#include <fil_tii.h>
#include <fil_tii_priv.h>
#include <filerr.h>
#include <filextern.h>
#include <paftyp.h>
#include <filtyp.h>
#include <fil_table.h>

/*
 *  ======== FIL_IIRFilterRec[] ========
 *  IIR filter function lookup table
 */
#if (PAF_IROM_BUILD==0xD610A003 || PAF_IROM_BUILD==0xD610A004  )

/*                          [Prec]    [taps]           [ch]     */
const IFIL_FunRec FIL_IIRFilterRec[2][FIL_IR_IIRTAPGEN][FIL_IR_IIRCHGEN] = 
{
/*        |   filterFunction     |       */
    /******* PRECISION = Single *********/
    {    /* TAP = 1 */
         {
             Filter_iirT1Ch1 , /* Channels = 1 */
             Filter_iirT1Ch2 , /* Channels = 2 */
             0               , /* Channels = 3 */
             0               , /* Channels = 4 */
             Filter_iirT1ChN   /* Channels = N */
         },
        /* TAP = 2 */
         {
             Filter_iirT2Ch1 , /* Channels = 1 */
             Filter_iirT2Ch2 , /* Channels = 2 */
             0               , /* Channels = 3 */
             0               , /* Channels = 4 */
             Filter_iirT2ChN   /* Channels = N */
         },
         /* TAP = 3 */
         {
             0               , /* Channels = 1 */
             0               , /* Channels = 2 */
             0               , /* Channels = 3 */
             0               , /* Channels = 4 */
             0               , /* Channels = N */
         },          
         /* TAP = 4 */
         {
             0               , /* Channels = 1 */
             0               , /* Channels = 2 */
             0               , /* Channels = 3 */
             0               , /* Channels = 4 */
             0               , /* Channels = N */
         },       
         /* TAP = N */
         {
             0               , /* Channels = 1 */
             0               , /* Channels = 2 */
             0               , /* Channels = 3 */
             0               , /* Channels = 4 */
             Filter_iirTNChN , /* Channels = N */
         }         
    },
    /* Samples = SP, Coefficients = DP. */
    {    /* TAP = 1 */
         {
             0                   , /* Channels = 1 */
             0                   , /* Channels = 2 */
             0                   , /* Channels = 3 */
             0                   , /* Channels = 4 */
             0                   , /* Channels = N */
         },
         /* TAP = 2 */
         {
             0                   , /* Channels = 1 */
             0                   , /* Channels = 2 */
             0                   , /* Channels = 3 */
             0                   , /* Channels = 4 */
             Filter_iirT2ChN_mx  , /* Channels = N */ /* BM-IROM3 */
         },
         /* TAP = 3 */
         {
             0                   , /* Channels = 1 */
             0                   , /* Channels = 2 */
             0                   , /* Channels = 3 */
             0                   , /* Channels = 4 */
             0                   , /* Channels = N */
         },          
         /* TAP = 4 */
         {
             Filter_iirT4Ch1_mx  , /* Channels = 1 */
             0                   , /* Channels = 2 */
             0                   , /* Channels = 3 */
             0                   , /* Channels = 4 */
             Filter_iirT4ChN_mx  , /* Channels = N */
         },          
         /* TAP = N */
         {
             0                   , /* Channels = 1 */
             0                   , /* Channels = 2 */
             0                   , /* Channels = 3 */
             0                   , /* Channels = 4 */
             0                   , /* Channels = N */
         }                           
    }

}; /* IFIL_FunRec FIL_IIRFilterRec[] */

/*
 *  ======== FIL_FIRFilterRec[] ========
 *  IIR filter function lookup table
 */
/* Memory section for the table */
#if PAF_IROM_BUILD==0xD610A003

/*                          [Prec]    [taps]           [ch]     */
const IFIL_FunRec FIL_FIRFilterRec[2][FIL_IR_FIRTAPGEN][FIL_IR_FIRCHGEN] = 
{
/*        |   filterFunction     |       */
    /****** PRECISION = Single *********/
    /* TAP = N */
    {  
         {
             Filter_firTNCh1 , /* Channels = 1 */
             Filter_firTNChN   /* Channels = N */ /* Hacked for IROM3 */
         }                  
    },
    /* Samples = SP, Coefficients = DP. */
    /* TAP = N */
    {  
         {
             0               , /* Channels = 1 */
             0                 /* Channels = N */
         }                           
    }
}; /* IFIL_FunRec FIL_FIRFilterRec[] */
#else
#if PAF_IROM_BUILD==0xD610A004
/*                          [Prec]    [taps]           [ch]     */
const IFIL_FunRec FIL_FIRFilterRec[2][FIL_IR_FIRTAPGEN][FIL_IR_FIRCHGEN] = 
{
/*        |   filterFunction     |       */
    /****** PRECISION = Single *********/
    /* TAP = N */
    {  
         {
             0 , /* Channels = 1 */
             0   /* Channels = N */ /* Hacked for IROM3 */
         }                  
    },
    /* Samples = SP, Coefficients = DP. */
    /* TAP = N */
    {  
         {
             0               , /* Channels = 1 */
             0                 /* Channels = N */
         }                           
    }
}; /* IFIL_FunRec FIL_FIRFilterRec[] */
#endif /* PAF_IROM_BUILD==0xD610A004 */
#endif
const IFIL_FunRec FIL_IIR_SOS_DF2_FilterRec[1][FIL_CASC_IR_SOS_DF2_CH_GEN][FIL_CASC_IR_SOS_DF2_CASC_GEN] = 
{
/*   |    filterFunction    |       */
    /****** PRECISION = Single *********/

    {    /* CH = 1 */  
         {
             0, /* Casc = 2 */
             0, /* Casc = 3 */
             0, /* Casc = 4 */
             0, /* Casc = 5 */
             0, /* Casc = N */                                      
         },
         /* CH = N */
         {
             0, /* Casc = 2 */
             0, /* Casc = 3 */
             0, /* Casc = 4 */
             0, /* Casc = 5 */
             0, /* Casc = N */                                      
         },                           
    },
}; /* IFIL_FunRec FIL_IIR_SOS_DF2_FilterRec[] */
#else /* (PAF_IROM_BUILD==0xD610A003) */

/* Memory section for the table */
#pragma DATA_SECTION (FIL_IIRFilterRec, ".data:FIL_IIRFilterRec");
/*                          [Prec]    [taps]           [ch]     */
const IFIL_FunRec FIL_IIRFilterRec[2][FIL_IR_IIRTAPGEN][FIL_IR_IIRCHGEN] = 
{
/*        |   filterFunction     |       */
    /******* PRECISION = Single *********/
    {    /* TAP = 1 */
         {
             Filter_iirT1Ch1 , /* Channels = 1 */
             Filter_iirT1Ch2 , /* Channels = 2 */
             Filter_iirT1Ch3 , /* Channels = 3 */
             Filter_iirT1Ch4 , /* Channels = 4 */
             Filter_iirT1ChN   /* Channels = N */
         },
        /* TAP = 2 */
         {
             Filter_iirT2Ch1 , /* Channels = 1 */
             Filter_iirT2Ch2 , /* Channels = 2 */
             Filter_iirT2Ch3 , /* Channels = 3 */
             0               , /* Channels = 4 */
             Filter_iirT2ChN   /* Channels = N */
         },
         /* TAP = 3 */
         {
             0               , /* Channels = 1 */
             0               , /* Channels = 2 */
             0               , /* Channels = 3 */
             0               , /* Channels = 4 */
             0               , /* Channels = N */
         },          
         /* TAP = 4 */
         {
             0               , /* Channels = 1 */
             0               , /* Channels = 2 */
             0               , /* Channels = 3 */
             0               , /* Channels = 4 */
             0               , /* Channels = N */
         },       
         /* TAP = N */
         {
             0               , /* Channels = 1 */
             0               , /* Channels = 2 */
             0               , /* Channels = 3 */
             0               , /* Channels = 4 */
             Filter_iirTNChN , /* Channels = N */
         }         
    },
    /* Samples = SP, Coefficients = DP. */
    {    /* TAP = 1 */
         {
             Filter_iirT1Ch1_mx  , /* Channels = 1 */
             0                   , /* Channels = 2 */
             0                   , /* Channels = 3 */
             0                   , /* Channels = 4 */
             0                   , /* Channels = N */
         },
         /* TAP = 2 */
         {
             Filter_iirT2Ch1_mx  , /* Channels = 1 */
             Filter_iirT2Ch2_mx  , /* Channels = 2 */
             0                   , /* Channels = 3 */
             0                   , /* Channels = 4 */
             Filter_iirT2ChN_mx  , /* Channels = N */
         },
         /* TAP = 3 */
         {
             0                   , /* Channels = 1 */
             0                   , /* Channels = 2 */
             0                   , /* Channels = 3 */
             0                   , /* Channels = 4 */
             0                   , /* Channels = N */
         },          
         /* TAP = 4 */
         {
             Filter_iirT4Ch1_mx  , /* Channels = 1 */
             0                   , /* Channels = 2 */
             0                   , /* Channels = 3 */
             0                   , /* Channels = 4 */
             Filter_iirT4ChN_mx  , /* Channels = N */
         },          
         /* TAP = N */
         {
             0                   , /* Channels = 1 */
             0                   , /* Channels = 2 */
             0                   , /* Channels = 3 */
             0                   , /* Channels = 4 */
             0                   , /* Channels = N */
         }                           
    }

}; /* IFIL_FunRec FIL_IIRFilterRec[] */

/*
 *  ======== FIL_FIRFilterRec[] ========
 *  IIR filter function lookup table
 */
/* Memory section for the table */
#pragma DATA_SECTION(FIL_FIRFilterRec, ".data:FIL_FIRFilterRec");
/*                          [Prec]    [taps]           [ch]     */
const IFIL_FunRec FIL_FIRFilterRec[2][FIL_IR_FIRTAPGEN][FIL_IR_FIRCHGEN] = 
{
/*      |    filterFunction    |       */
    /****** PRECISION = Single *********/
    /* TAP = N */
    {  
         {
             Filter_firTNCh1 , /* Channels = 1 */
             Filter_firTNChN   /* Channels = N */
         }                  
    },
    /* Samples = SP, Coefficients = DP. */
    /* TAP = N */
    {  
         {
             0, /* Channels = 1 */
             0  /* Channels = N */
         }                           
    }
}; /* IFIL_FunRec FIL_FIRFilterRec[] */

/*
 *  ======== FIL_IIR_SOS_DF2_FilterRec[] ========
 *  IIR filter function lookup table
 */
/* Memory section for the table */
#pragma DATA_SECTION(FIL_IIR_SOS_DF2_FilterRec, ".data:FIL_IIR_SOS_DF2_FilterRec");
/*                          [Prec]    [taps]           [ch]     */
const IFIL_FunRec FIL_IIR_SOS_DF2_FilterRec[1][FIL_CASC_IR_SOS_DF2_CH_GEN][FIL_CASC_IR_SOS_DF2_CASC_GEN] = 
{
/*   |    filterFunction    |       */
    /****** PRECISION = Single *********/

    {    /* CH = 1 */  
         {
             Filter_iirT2Ch1_c2_df2, /* Casc = 2 */
             Filter_iirT2Ch1_c3_df2, /* Casc = 3 */
             Filter_iirT2Ch1_c4_df2, /* Casc = 4 */
             Filter_iirT2Ch1_c5_df2, /* Casc = 5 */
             Filter_iirT2Ch1_cN_df2, /* Casc = N */                                      
         },
         /* CH = N */
         {
             0,                      /* Casc = 2 */
             0,                      /* Casc = 3 */
             0,                      /* Casc = 4 */
             0,                      /* Casc = 5 */
             Filter_iirT2ChN_cN_df2, /* Casc = N */                                      
         },                           
    },
}; /* IFIL_FunRec FIL_IIR_SOS_DF2_FilterRec[] */
#endif /* (PAF_IROM_BUILD==0xD610A003) */
