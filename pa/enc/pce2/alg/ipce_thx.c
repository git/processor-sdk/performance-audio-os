
/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Common PCM Encoder algoritm interface implementation
//
//
//

/*
 *  IPCE default instance creation parameters
 */
#include <std.h>
#include <ipce.h>

/*
 *  ======== IPCE_PARAMS ========
 *  This static initialization defines the default parameters used to
 *  create instances of PCE objects.
 */

#if IPCE_PHASES != 6 
#error internal error
#endif

/*
 *  ======== IPCE_PARAMS_STATUS_DELAY_THX ========
 */

#if PAF_MAXNUMCHAN_AF == 2 
#define IPCE_DELAY_NUMCHAN 2
#define IPCE_PARAMS_STATUS_DELAY_THX \
    0, /* unused */ \
    1, /* unit */ \
    PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
    0, 0, /* delay */ \
	0, /* masterDelay */    
#elif PAF_MAXNUMCHAN_AF == 4
#define IPCE_DELAY_NUMCHAN 2
#define IPCE_PARAMS_STATUS_DELAY_THX \
    0, /* unused */ \
    1, /* unit */ \
    PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
    0, 0, 0, 5, /* delay */ \
	0, /* masterDelay */    
#elif PAF_MAXNUMCHAN_AF == 6 
#define IPCE_DELAY_NUMCHAN 3 
#define IPCE_PARAMS_STATUS_DELAY_THX \
    0, /* unused */ \
    1, /* unit */ \
    PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
    0, 0, 0, 0, 5, 5, /* delay */ \
	0, /* masterDelay */    
#elif PAF_MAXNUMCHAN_AF == 8 
#define IPCE_DELAY_NUMCHAN 8 
#define IPCE_PARAMS_STATUS_DELAY_THX \
    0, /* unused */ \
    1, /* unit */ \
    PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
    0, 0, 0, 0, 5, 5, 5, 5, /* delay */ \
	0, /* masterDelay */    
#elif PAF_MAXNUMCHAN_AF == 16 
#define IPCE_DELAY_NUMCHAN 8 
#define IPCE_DELAY_NUMCHAN_EXT 12



#define IPCE_PARAMS_STATUS_DELAY_THX \
    0, /* unused */ \
    1, /* unit */ \
    PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
    0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 0, 0, 0, /* delay */ \
	0, /* masterDelay */  
#define IPCE_PARAMS_STATUS_DELAY_THX_EXT \
    0, /* unused */ \
    1, /* unit */ \
    PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN_EXT, /* numc, nums */ \
    0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 0, 0, 0, /* delay */ \
	0, /* masterDelay */

#elif PAF_MAXNUMCHAN_AF == 32
 
#define IPCE_DELAY_NUMCHAN 8 
#define IPCE_DELAY_NUMCHAN_EXT 12
#define IPCE_PARAMS_STATUS_DELAY_THX \
    0, /* unused */ \
    1, /* unit */ \
    PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN, /* numc, nums */ \
    0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* delay */ \
	0, /* masterDelay */  
#define IPCE_PARAMS_STATUS_DELAY_THX_EXT \
    0, /* unused */ \
    1, /* unit */ \
    PAF_MAXNUMCHAN_AF, IPCE_DELAY_NUMCHAN_EXT, /* numc, nums */ \
    0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 5, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, /* delay */ \
	0, /* masterDelay */

    
#else /* PAF_MAXNUMCHAN_AF */ 
#define IPCE_DELAY_NUMCHAN 0 
#define IPCE_PARAMS_STATUS_DELAY_THX 
#error code not written 
#endif/* PAF_MAXNUMCHAN_AF */

const IPCE_Status IPCE_PARAMS_STATUS_THX = {
    sizeof(IPCE_Status),
    1,          /* mode: enabled */
    0,          /* type: unused */
    1, 0,       /* phase 0 mode,type: enabled,unused */
    1, 0,       /* phase 1 mode,type: enabled,unused */
    1, 0,       /* phase 2 mode,type: enabled,unused */
    1, 0,       /* phase 3 mode,type: enabled,unused */
    1, 0,       /* phase 4 mode,type: enabled,unused */
    1, 0,       /* phase 5 mode,type: enabled,unused */
    IPCE_PARAMS_STATUS_DELAY_THX     /* Delay phase: THX configuration */ 
    0,          /* pceExceptionDetect */
    0,          /* pceExceptionFlag */
    0,          /* pceExceptionMute */
    0           /* pceClipDetect */
};

const IPCE_Status IPCE_PARAMS_STATUS_THX_EXT = {
    sizeof(IPCE_Status),
    1,          /* mode: enabled */
    0,          /* type: unused */
    1, 0,       /* phase 0 mode,type: enabled,unused */
    1, 0,       /* phase 1 mode,type: enabled,unused */
    1, 0,       /* phase 2 mode,type: enabled,unused */
    1, 0,       /* phase 3 mode,type: enabled,unused */
    1, 0,       /* phase 4 mode,type: enabled,unused */
    1, 0,       /* phase 5 mode,type: enabled,unused */
    IPCE_PARAMS_STATUS_DELAY_THX_EXT     /* Delay phase: THX configuration */ 
    0,          /* pceExceptionDetect */
    0,          /* pceExceptionFlag */
    0,          /* pceExceptionMute */
    0           /* pceClipDetect */
};

/*
 *  ======== IPCE_PARAMS_CONFIG_DELAY_THX ========
 *  This static initialization defines the parameters used to create
 *  instances of DEL objects - 8-channel @ 96 kHz.
 */

const IPCE_ConfigPhaseVolume IPCE_PARAMS_CONFIG_PHASE_VOLUME_THX = {
    sizeof(IPCE_ConfigPhaseVolume),
    -2*40,      /* volumeRamp */
};

#define IPCE_DELAY_LEFT 10*96
#define IPCE_DELAY_RGHT 10*96
#define IPCE_DELAY_CNTR 10*96
#define IPCE_DELAY_LHED 10*96
#define IPCE_DELAY_RHED 10*96
#define IPCE_DELAY_LWID 10*96
#define IPCE_DELAY_RWID 10*96
#define IPCE_DELAY_LSUR 20*96
#define IPCE_DELAY_RSUR 20*96
#define IPCE_DELAY_LBAK 20*96
#define IPCE_DELAY_RBAK 20*96
#define IPCE_DELAY_SUBW 10*96
#define IPCE_DELAY_TOTAL                                        \
        (IPCE_DELAY_LEFT + IPCE_DELAY_RGHT + IPCE_DELAY_CNTR +  \
         IPCE_DELAY_LSUR + IPCE_DELAY_RSUR +                    \
         IPCE_DELAY_LBAK + IPCE_DELAY_RBAK + IPCE_DELAY_SUBW) 
         
#define IPCE_DELAY_TOTAL_EXT                                        \
        (IPCE_DELAY_LEFT + IPCE_DELAY_RGHT + IPCE_DELAY_CNTR +  \
         IPCE_DELAY_LSUR + IPCE_DELAY_RSUR +                    \
         IPCE_DELAY_LWID + IPCE_DELAY_RWID +                    \
         IPCE_DELAY_LHED + IPCE_DELAY_RHED +                    \
         IPCE_DELAY_LBAK + IPCE_DELAY_RBAK + IPCE_DELAY_SUBW) 

#define IPCE_DELAY_ESIZE 3     /* delay sizeofElement: packed 24 bits */
#define IPCE_DELAY_FSIZE 256   /* delay sizeofFrame: 256 samples */ 

const PAF_DelayState IPCE_PARAMS_CONFIG_PHASE_DELAYSPEC_THX[] = {
    0, 0, IPCE_DELAY_LEFT, 0, NULL, 0, 
    0, 0, IPCE_DELAY_RGHT, 0, NULL, 0, 
    0, 0, IPCE_DELAY_CNTR, 0, NULL, 0, 
    0, 0, IPCE_DELAY_LSUR, 0, NULL, 0, 
    0, 0, IPCE_DELAY_RSUR, 0, NULL, 0, 
    0, 0, IPCE_DELAY_LBAK, 0, NULL, 0, 
    0, 0, IPCE_DELAY_RBAK, 0, NULL, 0, 
    0, 0, IPCE_DELAY_SUBW, 0, NULL, 0,
};

const PAF_DelayState IPCE_PARAMS_CONFIG_PHASE_DELAYSPEC_THX_EXT[] = {
    0, 0, IPCE_DELAY_LEFT, 0, NULL, 0, 
    0, 0, IPCE_DELAY_RGHT, 0, NULL, 0, 
    0, 0, IPCE_DELAY_CNTR, 0, NULL, 0,
    0, 0, IPCE_DELAY_LWID, 0, NULL, 0, 
    0, 0, IPCE_DELAY_RWID, 0, NULL, 0, 
    0, 0, IPCE_DELAY_LSUR, 0, NULL, 0, 
    0, 0, IPCE_DELAY_RSUR, 0, NULL, 0, 
    0, 0, IPCE_DELAY_LBAK, 0, NULL, 0, 
    0, 0, IPCE_DELAY_RBAK, 0, NULL, 0, 
    0, 0, IPCE_DELAY_SUBW, 0, NULL, 0,
    0, 0, IPCE_DELAY_LHED, 0, NULL, 0, 
    0, 0, IPCE_DELAY_RHED, 0, NULL, 0, 
};

const IPCE_ConfigPhaseDelay IPCE_PARAMS_CONFIG_PHASE_DELAY_THX = {
    sizeof(IPCE_ConfigPhaseDelay) + IPCE_DELAY_NUMCHAN * sizeof(PAF_DelayState), 
    (1<<PAF_LEFT)|(1<<PAF_RGHT)|(1<<PAF_CNTR)|(1<<PAF_LSUR)| 
    (1<<PAF_RSUR)|(1<<PAF_LBAK)|(1<<PAF_RBAK)|(1<<PAF_SUBW), 
    IPCE_DELAY_ESIZE,
    (PAF_DelayState *)IPCE_PARAMS_CONFIG_PHASE_DELAYSPEC_THX,
};

const IPCE_ConfigPhaseDelay IPCE_PARAMS_CONFIG_PHASE_DELAY_THX_EXT = {
    sizeof(IPCE_ConfigPhaseDelay) + IPCE_DELAY_NUMCHAN_EXT * sizeof(PAF_DelayState), 
    (1<<PAF_LEFT)|(1<<PAF_RGHT)|(1<<PAF_CNTR)|(1<<PAF_LSUR)| 
    (1<<PAF_RSUR)|(1<<PAF_LBAK)|(1<<PAF_RBAK)|(1<<PAF_SUBW)|
    (1<<PAF_RHED)|(1<<PAF_LHED)|(1<<PAF_RWID)|(1<<PAF_LWID),
    IPCE_DELAY_ESIZE,
    (PAF_DelayState *)IPCE_PARAMS_CONFIG_PHASE_DELAYSPEC_THX_EXT,
};

const PAF_IALG_Common IPCE_PARAMS_COMMON_PHASE_DELAY_THX = {
    sizeof(PAF_IALG_Common) + IPCE_DELAY_TOTAL * IPCE_DELAY_ESIZE,
    PAF_IALG_COMMONN(8),                /* flag: specifies common memory type*/
};

const PAF_IALG_Common IPCE_PARAMS_COMMON_PHASE_DELAY_THX_EXT = {
    sizeof(PAF_IALG_Common) + IPCE_DELAY_TOTAL_EXT * IPCE_DELAY_ESIZE,
    PAF_IALG_COMMONN(8),                /* flag: specifies common memory type*/
};

const IPCE_Config IPCE_PARAMS_CONFIG_THX = {
    sizeof(IPCE_Config),
    0,          /* frameLength */
    0,          /* unused */
    (IPCE_ConfigPhase *)&IPCE_PARAMS_CONFIG_PHASE_VOLUME_THX,/* phase 0 config: volume */
    (IPCE_ConfigPhase *)&IPCE_PARAMS_CONFIG_PHASE_DELAY_THX, /* phase 1 config: delay  */
    0,                                                       /* phase 2 config: output */
    0,                                                       /* phase 3 config: unused */
    0,                                                       /* phase 4 config: unused */
    0,                                                       /* phase 5 config: unused */
    0,                                                       /* phase 0 common: volume */
    (PAF_IALG_Common*)&IPCE_PARAMS_COMMON_PHASE_DELAY_THX,   /* phase 1 common: delay  */
    0,                                                       /* phase 2 common: output */
    0,                                                       /* phase 3 common: unused */
    0,                                                       /* phase 4 common: unused */
    0,                                                       /* phase 5 common: unused */
                /* scale -- uninitialized */
};

const IPCE_Config IPCE_PARAMS_CONFIG_THX_EXT = {
    sizeof(IPCE_Config),
    0,          /* frameLength */
    0,          /* unused */
    (IPCE_ConfigPhase *)&IPCE_PARAMS_CONFIG_PHASE_VOLUME_THX,/* phase 0 config: volume */
    (IPCE_ConfigPhase *)&IPCE_PARAMS_CONFIG_PHASE_DELAY_THX_EXT, /* phase 1 config: delay  */
    0,                                                       /* phase 2 config: output */
    0,                                                       /* phase 3 config: unused */
    0,                                                       /* phase 4 config: unused */
    0,                                                       /* phase 5 config: unused */
    0,                                                       /* phase 0 common: volume */
    (PAF_IALG_Common*)&IPCE_PARAMS_COMMON_PHASE_DELAY_THX_EXT,   /* phase 1 common: delay  */
    0,                                                       /* phase 2 common: output */
    0,                                                       /* phase 3 common: unused */
    0,                                                       /* phase 4 common: unused */
    0,                                                       /* phase 5 common: unused */
                /* scale -- uninitialized */
};

/*
 *  ======== IPCE_PARAMS_ACTIVE ========
 */
const PAF_ActivePhaseVolume IPCE_PARAMS_ACTIVE_PHASE_VOLUME_THX = {
    sizeof(PAF_ActivePhaseVolume),
                            /* pVolumeStatus: uninitialized */
};

const PAF_ActivePhaseOutput IPCE_PARAMS_ACTIVE_PHASE_OUTPUT_THX = {
    sizeof(PAF_ActivePhaseOutput),
                            /* pEncodeStatus: uninitialized */
                            /* pOutBufConfig: uninitialized */
};

const IPCE_Active IPCE_PARAMS_ACTIVE_THX = {
    0,
    0,
    (PAF_ActivePhase *)&IPCE_PARAMS_ACTIVE_PHASE_VOLUME_THX,  /* phase 0 active: volume */
    0,                                                        /* phase 1 active: delay  */
    (PAF_ActivePhase *)&IPCE_PARAMS_ACTIVE_PHASE_OUTPUT_THX,  /* phase 2 active: output */   
    0,                                                        /* phase 3 active: unused */
    0,                                                        /* phase 4 active: unused */
    0,                                                        /* phase 5 active: unused */
                            /* bitstreamMask uninitialized */
};

/*
 *  ======== IPCE_PARAMS_SCRACH ========
 */
const PAF_ScrachPhaseDelay IPCE_PARAMS_SCRACH_PHASE_DELAY_THX = {
    sizeof(PAF_ScrachPhaseDelay)+IPCE_DELAY_ESIZE*IPCE_DELAY_FSIZE,
};

//Loooks to be an error ; need to be of PAF_ScrachPhaseOutput
const PAF_ScrachPhaseDelay IPCE_PARAMS_SCRACH_PHASE_OUTPUT_THX = {
    sizeof(PAF_ScrachPhaseDelay)+12*4*IPCE_DELAY_FSIZE, // 12*256*4 = no of output channels * block size * output sample size
};

const IPCE_Scrach IPCE_PARAMS_SCRACH_THX = {
    sizeof(IPCE_Scrach),
    0,                                                          /* phase 0 scrach: volume */
#if defined(PAF_DEVICE) && \
        (((PAF_DEVICE&0xFFFF0000) == 0xD7100000) || \
         ((PAF_DEVICE&0xFF000000) == 0xD8000000) || \
         ((PAF_DEVICE&0xFF000000) == 0xDA000000))
    (PAF_ScrachPhase *)&IPCE_PARAMS_SCRACH_PHASE_DELAY_THX,     /* phase 1 scrach: delay  */
#else /* PAF_DEVICE */
    0,                                                          /* phase 1 scrach: delay  */
#endif /* PAF_DEVICE */
#if defined(PAF_DEVICE) && \
        (((PAF_DEVICE&0xFFFF0000) == 0xD7100000) || \
         ((PAF_DEVICE&0xFF000000) == 0xD8000000) || \
         ((PAF_DEVICE&0xFF000000) == 0xDA000000))
    (PAF_ScrachPhase *)&IPCE_PARAMS_SCRACH_PHASE_OUTPUT_THX,    /* phase 2 scrach: output */
#else /* PAF_DEVICE */
    0,                                                          /* phase 2 scrach: output */
#endif /* PAF_DEVICE */
    0,                                                          /* phase 3 scrach: unused */
    0,                                                          /* phase 4 scrach: unused */
    0,                                                          /* phase 5 scrach: unused */
};

const IPCE_Params IPCE_PARAMS_THX = {
    sizeof(IPCE_Params),
    &IPCE_PARAMS_STATUS_THX ,
    &IPCE_PARAMS_CONFIG_THX ,
    &IPCE_PARAMS_ACTIVE_THX ,
    &IPCE_PARAMS_SCRACH_THX ,
};

const IPCE_Params IPCE_PARAMS_THX_EXT = {
    sizeof(IPCE_Params),
    &IPCE_PARAMS_STATUS_THX_EXT ,
    &IPCE_PARAMS_CONFIG_THX_EXT ,
    &IPCE_PARAMS_ACTIVE_THX ,
    &IPCE_PARAMS_SCRACH_THX ,
};

//asm (" .global _IPCE_PARAMS");
//asm ("_IPCE_PARAMS .set _IPCE_PARAMS_THX"); 

