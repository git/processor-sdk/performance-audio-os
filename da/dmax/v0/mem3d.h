
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX MEM 3D Service Routine data structures
//
//
//

#ifndef MEM3D_H_ 
#define MEM3D_H_ 

#include <mem3d.inc>
#include <tistdtypes.h>
#include <dmax_struct.h>

/* MEM3D macros */
#define MEM3D_MKEE(QTSL,SYNC,TCC,ATCINT,TCINT,RLOAD,CC,ESIZE,PTE,PP,ETYPE) \
    ((((QTSL)&MEM3D_QTSL_MSK)<<MEM3D_QTSL_SHFT) |         \
     (((SYNC)&MEM3D_SYNC_MSK)<<MEM3D_SYNC_SHFT) |         \
     (((PP)>>4&MEM3D_PP_P3_MSK)<<MEM3D_PP_P3_SHFT) |      \
     (((TCC)&MEM3D_TCC_MSK)<<MEM3D_TCC_SHFT) |            \
     (((ATCINT)&MEM3D_ATCINT_MSK)<<MEM3D_ATCINT_SHFT) |   \
     (((TCINT)&MEM3D_TCINT_MSK)<<MEM3D_TCINT_SHFT) |      \
     (((PP)>>3&MEM3D_PP_P2_MSK)<<MEM3D_PP_P2_SHFT) |      \
     (((RLOAD)&MEM3D_RLOAD_MSK)<<MEM3D_RLOAD_SHFT) |      \
     (((CC)&MEM3D_CC_MSK)<<MEM3D_CC_SHFT) |               \
     (((ESIZE)&MEM3D_ESIZE_MSK)<<MEM3D_ESIZE_SHFT) |      \
     (((PTE)>>2&MEM3D_PTE_MSK)<<MEM3D_PTE_SHFT) |         \
     (((PP)&MEM3D_PP_P1_MSK)<<MEM3D_PP_P1_SHFT) |         \
     (((ETYPE)&MEM3D_ETYPE_MSK)<<MEM3D_ETYPE_SHFT))

/* MEM3D parameter format */
typedef struct cnt_type0{
    Uint32 cnt0:8;
    Uint32 cnt1:8;
    Uint32 cnt2:15;
    Uint32 rs:1;
}cnt_type0;

typedef struct cnt_type1{
    Uint32 cnt0:16;
    Uint32 cnt1:8;
    Uint32 cnt2:7;
    Uint32 rs:1;
}cnt_type1;

typedef struct cnt_type2{
    Uint32 cnt0:8;
    Uint32 cnt1:16;
    Uint32 cnt2:7;
    Uint32 rs:1;
}cnt_type2;

typedef struct mem3d{
    Uint32 src;
    Uint32 dst;
    union{
        Uint32 cnt_full;
        cnt_type0 cnt_t0;
        cnt_type1 cnt_t1;
        cnt_type2 cnt_t2;
    } cnt;
    Int16 src_idx0;
    Int16 dst_idx0;
    Int16 src_idx1;
    Int16 dst_idx1;
    Int16 src_idx2;
    Int16 dst_idx2;
    union{
        Uint32 cnt_full;
        cnt_type0 cnt_t0;
        cnt_type1 cnt_t1;
        cnt_type2 cnt_t2;
    } cnt_ref;
    Uint32 src_rld0;
    Uint32 dst_rld0;
    Uint32 src_rld1;
    Uint32 dst_rld1;
} mem3d;

/* MEM3D API declarations */
extern Uint32 dMAX_mem3dOpen(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                             Uint32 pp,Uint32 tcc,Fxn fxn);
extern void dMAX_mem3dClose(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                            Uint32 pp,Uint32 tcc,Uint32 pt);
extern void dMAX_mem3dCfgEE(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                            Uint32 ee);
extern void dMAX_mem3dCfgPT(dMAX_Handle handle,Uint32 pt,mem3d *pmem3d);
extern void dMAX_mem3dCopy(dMAX_Handle handle,Uint32 evt);
extern void dMAX_mem3dNoWait(dMAX_Handle handle,Uint32 tcc);
#endif // MEM3D_H_

