
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// SIO driver implementation for audio IO using McBSP.
//
//
#if (PAF_DEVICE&0xFF000000) != 0xD8000000
#include <xdc/std.h>
#endif
#include <dev.h>
#include <sem.h>
#include <hwi.h>

#if (PAF_DEVICE&0xFF000000) == 0xD8000000
#include <dab_csl_mcbsp.h>
unsigned int fifo_rxbuf[2] = {0x01F10000,0x01F11000};
unsigned int fifo_address_offset[2]={0x01D10800,0x01D11800};
#else
#include <csl_mcbsp.h>
#include <cslr_mcbsp.h>
#endif
#include <ti/sdo/edma3/drv/edma3_drv.h>
#include <ti/sdo/edma3/drv/src/edma3.h>
#include <ti/sdo/edma3/rm/edma3_rm.h>

#include <dab.h>

#include <tsk.h>
/*
Even though these functions are declared in ti/sdo/edma3/drv/sample/bios6_edma3_drv_sample.h,
#including that header file leads to compile error.
*/
EDMA3_DRV_Result Edma3_CacheInvalidate(unsigned int mem_start_ptr,
                           unsigned int num_bytes);
EDMA3_DRV_Result Edma3_CacheFlush(unsigned int mem_start_ptr,
                      unsigned int num_bytes);		
					  
// .............................................................................
// NOTES:
//    . circular buffering not supported
//         - for one there is a problem allocating enough PaRAM entries
//         - for two I'm not sure how to use different TCCs
//    . not insuring channel alignment currently in mcbspEnable
// .............................................................................

// global allocated in bios_edma3_drv_sample_init.c
extern EDMA3_DRV_Handle hEdma;

// HACK: 
static CSL_McbspObj  mcbsp_obj;

// from c:/CCStudio_v3.3/boards/davincievm/examples/sd/davinciEdma.h
#if (PAF_DEVICE&0xFF000000) == 0xD8000000
#define CSL_EDMA3_CHA_XEVT0	    		      3
#define CSL_EDMA3_CHA_REVT0		   		      2
#define CSL_EDMA3_CHA_XEVT1	    		      5
#define CSL_EDMA3_CHA_REVT1		   		      4
#else
#define CSL_EDMA3_CHA_XEVT0	    		      2
#define CSL_EDMA3_CHA_REVT0		   		      3
#endif


// only one global variable
DAB_DriverObject dabDrv;

#ifdef DAB_DEBUG
#include <stdio.h>
#endif

/* .......................................................................... */

//Int  DAB_close (DEV_Handle);
Int  DAB_ctrl (DEV_Handle, Uns, Arg);
Int  DAB_idle (DEV_Handle, Bool);
void DAB_isrCallback (Uint32 tcc, EDMA3_RM_TccStatus status, Ptr context);
Int  DAB_issue (DEV_Handle);
Int  DAB_open (DEV_Handle, String);
//Bool DAB_ready (DEV_Handle, SEM_Handle);
Int  DAB_reclaim (DEV_Handle);
Int  DAB_shutdown (DEV_Handle);
Int  DAB_start (DEV_Handle);
Int  DAB_config (DEV_Handle pDevObj, const DAB_Params *pParams);
Int  DAB_mcbspEnable (DEV_Handle device);
Int  DAB_mcbspReset (DEV_Handle device);
Int  DAB_setupParam (DEV_Handle device, XDAS_UInt32 targetEdma, XDAS_UInt32 childEdma, unsigned int addr, unsigned int size);
Int  DAB_setupXfer (DEV_Handle device, XDAS_UInt32 targetEdma, XDAS_UInt32 parentEdma, XDAS_UInt32 childEdma, DEV_Frame *pFrame);

DAB_Fxns DAB_FXNS =
{
    NULL, //DAB_close -- remove since unused
    DAB_ctrl,
    DAB_idle,
    DAB_issue,
    DAB_open,
    NULL, //DAB_ready -- removed since unused
    DAB_reclaim,
    DAB_shutdown,
    DAB_start,
    DAB_config,
    DAB_mcbspEnable,
    DAB_mcbspReset,
    DAB_setupParam,
    DAB_setupXfer
};

#define DAB_FTABLE_shutdown(_a)                   (*pDevExt->pFxns->shutdown)(_a)
#define DAB_FTABLE_start(_a)                      (*pDevExt->pFxns->start)(_a)
#define DAB_FTABLE_config(_a,_b)                  (*pDevExt->pFxns->config)(_a,_b)
#define DAB_FTABLE_mcbspEnable(_a)                (*pDevExt->pFxns->mcbspEnable)(_a)
#define DAB_FTABLE_mcbspReset(_a)                 (*pDevExt->pFxns->mcbspReset)(_a)
#define DAB_FTABLE_setupParam(_a,_b,_c,_d,_e)     (*pDevExt->pFxns->setupParam)(_a,_b,_c,_d,_e)
#define DAB_FTABLE_setupXfer(_a,_b,_c,_d,_e)      (*pDevExt->pFxns->setupXfer)(_a,_b,_c,_d,_e)

int dab_ZDFE = 0; // used for underrun
int dab_BDFE;     // used for overrun

// -----------------------------------------------------------------------------

#define EDMA_HINV NULL

Void DAB_init (Void)
{
    int i;

	unsigned int * fifoBase; 
    for (i=0; i < _MCBSP_PORT_CNT; i++) {
        dabDrv.hMcbsp[i] = NULL;
        dabDrv.configCount[i] = 0;
        fifoBase = (unsigned int * )fifo_address_offset[i];
        if (fifoBase[_MCBSP_AFIFOREV_OFFSET] == 0x44311100) 
            dabDrv.fifoPresent[i] = 1;
        else
            dabDrv.fifoPresent[i] = 0;
    }
    
    dabDrv.numDevices = 0;

} // DAB_init

// -----------------------------------------------------------------------------

Int DAB_open (DEV_Handle device, String name)
{
    DAB_DeviceExtension   *pDevExt;
    DEV_Device            *entry;
    EDMA3_DRV_Result       edmaResult;
    Uint32 reqTcc;
    int i;


    // check SIO mode 
    if ((device->mode != DEV_INPUT) && (device->mode != DEV_OUTPUT))
        return SYS_EMODE;

    // allocate memory for device extension
    device->object = NULL;
    pDevExt = MEM_alloc (device->segid, (sizeof(DAB_DeviceExtension)+3)/4*4, 4);
    if (pDevExt == MEM_ILLEGAL)
        return SYS_EALLOC;
    device->object = (Ptr) pDevExt;

    // inits
    pDevExt->device = device;
    pDevExt->sync = NULL;
    pDevExt->ready = NULL;
    pDevExt->pParams = NULL;
    pDevExt->pTraceLog = NULL;
    pDevExt->runState = 0;  
    pDevExt->errorState = PAF_SIO_ERROR_NONE;
    pDevExt->shutDown = 1;
    pDevExt->activeEdma = EDMA_HINV;
    pDevExt->errorEdma = EDMA_HINV;
    pDevExt->firstTCC = 0;
    pDevExt->optLevel = 0;
    pDevExt->numParamSetup = 0;

    // use dev match to fetch function table pointer for DAB
    DEV_match ("/DAB", &entry);
    if (entry == NULL) {
        SYS_error("DAB", SYS_ENODEV);
        return SYS_ENODEV;
    }
    pDevExt->pFxns = (DAB_Fxns *) entry->fxns;

    // create semaphore for device
    pDevExt->sync = SEM_create(0, NULL);
    if (pDevExt->sync == NULL)
        return SYS_EALLOC;

    // queue inits
    QUE_new (&pDevExt->xferQue); 
    QUE_new (&pDevExt->paramQue);

    // allocate EDMA param memory and place on queue
    pDevExt->numEdmaParams = 4;
    for (i=0; i < pDevExt->numEdmaParams; i++) {
        reqTcc = EDMA3_DRV_TCC_ANY;
        pDevExt->edmaParams[i].hEdma = EDMA3_DRV_LINK_CHANNEL;
        edmaResult = EDMA3_DRV_requestChannel (
            hEdma,
            &pDevExt->edmaParams[i].hEdma,
            &reqTcc,
            (EDMA3_RM_EventQueue) 0,
            DAB_isrCallback,
            (void *) device);
        if (edmaResult != EDMA3_DRV_SOK)
            return SYS_EALLOC;

        //not running => can use non-atomic functions
        QUE_enqueue (&pDevExt->paramQue, &pDevExt->edmaParams[i]);
    }

    reqTcc = EDMA3_DRV_TCC_ANY;
    pDevExt->errorEdma = EDMA3_DRV_LINK_CHANNEL;
    edmaResult = EDMA3_DRV_requestChannel (
        hEdma,
        &pDevExt->errorEdma,
        &reqTcc,
        (EDMA3_RM_EventQueue)0,
        DAB_isrCallback,
        (void *) device);
    if (edmaResult != EDMA3_DRV_SOK)
        return SYS_EALLOC;

    dabDrv.pDevice[dabDrv.numDevices] = device;
    pDevExt->deviceNum = dabDrv.numDevices++;

    return SYS_OK;
}  // DAB_open

// -----------------------------------------------------------------------------

Int DAB_config (DEV_Handle device, const DAB_Params *pParams)
{
    DAB_DeviceExtension *pDevExt = (DAB_DeviceExtension *)device->object;
    int mcbspNum = pDevExt->pParams->sio.moduleNum;
    CSL_Status           status;
    EDMA3_DRV_Result     edmaResult;


/* .......................................................................... */
// McBSP configuration

    // cannot configure if transfer started
    if (pDevExt->runState == 1)
        return SYS_EBADIO;

    if (!pParams)
        return SYS_EINVAL;

    mcbspNum = pParams->sio.moduleNum;
    if( (mcbspNum + 1) > _MCBSP_PORT_CNT )
        return SYS_EINVAL;

    // save pointer to config structure in device extension. here so that
    //   forthcoming functions can use/modify config structure.
    pDevExt->pParams = pParams;
    pDevExt->edmaWordSize = pParams->sio.wordSize;
         
     // Allocate and configure mcbsp if necessary
    if (!dabDrv.hMcbsp[mcbspNum]) {
        dabDrv.hMcbsp[mcbspNum] = CSL_mcbspOpen (&mcbsp_obj, mcbspNum, 0, &status);
        if (!dabDrv.hMcbsp[mcbspNum])
            return SYS_EALLOC;
    }

    // HACK: only config McBSP when count is 0
    if (dabDrv.configCount[mcbspNum] == 0)
        CSL_mcbspHwSetupRaw (dabDrv.hMcbsp[mcbspNum], pParams->sio.pConfig);    
    dabDrv.configCount[mcbspNum] += 1;

// .............................................................................
// EDMA configuration

    // allocate edma channel -- also disable and clear the interrupt
    if (device->mode == DEV_INPUT)
    {
    	if(mcbspNum ==0)
        pDevExt->activeEdma = CSL_EDMA3_CHA_REVT0;
        else
       pDevExt->activeEdma = CSL_EDMA3_CHA_REVT1;
    }
    else
    {
        if(mcbspNum ==0) pDevExt->activeEdma = CSL_EDMA3_CHA_XEVT0;
        else
        pDevExt->activeEdma = CSL_EDMA3_CHA_XEVT1;
    }

    pDevExt->firstTCC = pDevExt->activeEdma ;
    edmaResult = EDMA3_DRV_requestChannel (
        hEdma,
        &pDevExt->activeEdma,
        &pDevExt->firstTCC,
        (EDMA3_RM_EventQueue) 0, 
        DAB_isrCallback,
        (void *) device);
    if (edmaResult != EDMA3_DRV_SOK)
        return SYS_EALLOC;
    
    // Configure error transfer
    //   make cnt same as # of channels in order to maintain alignment
    //   and the error transfer small so that we never have to wait
    //   long for it to complete and trigger a linked transfer. This is
    //   important for establishing output timing when we are idling with
    //   clocks still running. Is fine for Rx as well.
    DAB_FTABLE_setupParam (device, pDevExt->errorEdma, pDevExt->errorEdma, NULL, pDevExt->edmaWordSize * pParams->dab.numChannels);

    return SYS_OK;
} // DAB_config

// -----------------------------------------------------------------------------

Int DAB_ctrl (DEV_Handle device, Uns code, Arg arg)
{
    DAB_DeviceExtension *pDevExt = (DAB_DeviceExtension *) device->object;
    int result = SYS_OK;
    const DAB_Params *pParams;


    switch (code) {

/* .......................................................................... */

        case PAF_SIO_CONTROL_GET_INPUT_STATUS:
        case PAF_SIO_CONTROL_SET_RATEX:
        case PAF_SIO_CONTROL_MUTE:
        case PAF_SIO_CONTROL_UNMUTE:
            pParams = pDevExt->pParams;
            if (!pParams)
                return SYS_OK;

            if (pParams->sio.control && (result = pParams->sio.control (device, (const PAF_SIO_Params *)pParams, code, arg)))
                return result;
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_OPEN:
            if (pDevExt->runState)
                return SYS_EBUSY;

            if (!( pParams = (const DAB_Params *) arg ))
                return SYS_OK;

            if (result = DAB_FTABLE_config (device, pParams))
                return result;

            if (pParams->sio.control && (result = pParams->sio.control(device, (const PAF_SIO_Params *)pParams, code, arg)))
                return result;
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_CLOSE:
            if (pDevExt->runState)
                return SYS_EBUSY;

            if (pDevExt->activeEdma != EDMA_HINV) {
                EDMA3_DRV_freeChannel (hEdma, pDevExt->activeEdma);
                pDevExt->activeEdma = EDMA_HINV;
            }

            if (!(pParams = pDevExt->pParams))
                return SYS_OK;

            if (pParams->sio.control && (result = pParams->sio.control (device, (const PAF_SIO_Params *)pParams, code, arg)))
                return result;

            // decrement config count
            if (dabDrv.configCount[pParams->sio.moduleNum] > 0)
                dabDrv.configCount[pParams->sio.moduleNum] -= 1;

            pDevExt->pParams = NULL;
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_TRACE:
            pDevExt->pTraceLog = (LOG_Obj *) arg;
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_GET_WORDSIZE:
            if (!arg)
                return SYS_EINVAL;
            *((int *) arg) = pDevExt->edmaWordSize;
            break;

        case PAF_SIO_CONTROL_SET_WORDSIZE:
            // currently only supported for input
            if (device->mode != DEV_INPUT)
                return SYS_EINVAL;

            // can't be running 
            if (pDevExt->runState)
                return SYS_EBUSY;

            // driver only supports 2 or 4 bytes
           if ((arg != 2) && (arg != 4))
                return SYS_EINVAL;

            // return success for unconfigured devices?
            if (!pDevExt->pParams)
                return SYS_OK;

            // ask platform if size is supported
            if (pDevExt->pParams->sio.control && (result = pDevExt->pParams->sio.control (device, (const PAF_SIO_Params *)pParams, code, arg)))
                return result;

            pDevExt->edmaWordSize = arg;
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_GET_PRECISION:
            if (!arg)
                return SYS_EINVAL;
                    
            if (!pDevExt->pParams)
                return SYS_OK;
            pParams = pDevExt->pParams;

            *((int *) arg) = pParams->sio.precision;
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_GET_NUMCHANNELS:
            if (!arg)
                return SYS_EINVAL;

            if (!pDevExt->pParams)
                return SYS_OK;
            pParams = pDevExt->pParams;
                    
            *((int *) arg) = pParams->dab.numChannels;
            break;

/* .......................................................................... */

        case PAF_SIO_CONTROL_IDLE_WITH_CLOCKS:
            /* 1. Here we are intentionally not using SIO_Idle() and 
               leaving the Tx clock running. We need this to avoid DAC noise,
               as well as provide a DIT clock when using digital output.
            */
            if (device->mode != DEV_OUTPUT)
                return SYS_EINVAL;

            pParams = pDevExt->pParams;
            if (!pParams)
                return SYS_OK;

            result = DAB_FTABLE_shutdown (device);
            if (result)
                return result;
            EDMA3_DRV_enableTransfer (hEdma, pDevExt->activeEdma, EDMA3_DRV_TRIG_MODE_EVENT);
            pDevExt->errorState = PAF_SIO_ERROR_IDLE_STAGE1;
            break;
            
/* .......................................................................... */

    }

    return result;
} // DAB_ctrl

// -----------------------------------------------------------------------------

Int DAB_idle (DEV_Handle device, Bool flush)
{
    DAB_DeviceExtension *pDevExt = (DAB_DeviceExtension *) device->object;
    Int result = SYS_OK;


    // do nothing if already idled or unattached
    if ((!pDevExt->runState) || (pDevExt->pParams == NULL))
        return result;

    pDevExt->shutDown = 0; /* force shutdown to run */
    result = DAB_FTABLE_shutdown (device);
    if (result)
        return result;

    // must reset McBSP before EDMA to ensure proper channel alignment upon restart.
    result = DAB_FTABLE_mcbspReset (device);
    if (result)
        return result;

    // disable interrupts and EDMA servicing
    if (pDevExt->activeEdma != EDMA_HINV)
        EDMA3_DRV_disableTransfer (hEdma, pDevExt->activeEdma, EDMA3_DRV_TRIG_MODE_EVENT);

    pDevExt->numQueued = 0;

    // signal stopped
    pDevExt->runState = 0;

    // reset errorState
    pDevExt->errorState = PAF_SIO_ERROR_NONE;

    // place call to physical device
    if ((pDevExt->pParams != NULL) && (pDevExt->pParams->sio.control != NULL))
        result = pDevExt->pParams->sio.control (device, (const PAF_SIO_Params *)pDevExt->pParams, PAF_SIO_CONTROL_IDLE, 0);

    return result;
} // DAB_idle

// -----------------------------------------------------------------------------
#include <ti/sysbios/hal/Cache.h>
Int DAB_issue (DEV_Handle device)
{
    DAB_DeviceExtension *pDevExt = (DAB_DeviceExtension *) device->object;
    DEV_Frame *pFrame;
    DAB_EDMA_Param *pParam;
    XDAS_UInt32 parentEdma;


    if ((device->mode == DEV_OUTPUT) && (pDevExt->errorState == PAF_SIO_ERROR_ERRBUF_XFER))
        return SYS_EBADIO;

    if ((device->mode == DEV_INPUT) && pDevExt->errorState)
    {
    	
        return SYS_EBADIO;
    }

    // if not yet running then configure active xfer and start
    if (pDevExt->runState == 0)
        return (DAB_FTABLE_start(device));

    // .........................................................................
    // here if running
    
    // TODO: is there an API to just disable the IER bit for this tcc?
    // disable interrupts to protect context
  //  edma3OsProtectEntry (EDMA3_OS_PROTECT_INTERRUPT_XFER_COMPLETION, NULL);
 HWI_disable ();
    /* determine parent EDMA
       if no xfers in queue and we are running then must be in the
       error state so link to active channel otherwise link to last
       transfer queued.
    */

    /* here we assume after Tx SIO_idle or overrun, the user
       will issue, at least, back-to-back issue requests so
       there should be no problem here.
    */
    if ((pDevExt->numQueued <= 1) && (pDevExt->errorState != 2))
        parentEdma = pDevExt->activeEdma;
    else {
        // if here then xferQue has more than one element so ok to use tail
        // last scheduled transfer must be queue->prev
        DEV_Frame *tail = (DEV_Frame *) QUE_prev (&pDevExt->xferQue);
        parentEdma = ((DAB_EDMA_Param *) tail->misc)->hEdma;
    }
	
    // get frame and parameter table to use; ints off => non-atomic OK
    //     dont need to check for empty queues since were here then todevice
    //     must have a frame placed there by the SIO_issue layer.
    //     paramQue must be valid since it is accessed the same as todevice.
    //     (indirectly -- isr places used items onto paramQue and fromdevice que
    //      at the same time)
    pFrame = (DEV_Frame *) QUE_dequeue (device->todevice);
    pParam = (DAB_EDMA_Param *) QUE_dequeue (&pDevExt->paramQue);

    // set misc argument to pParam so get enqueue later
    pFrame->misc = (Arg) pParam;

    // place on holder queue, ints off => non-atomic OK
    QUE_enqueue (&pDevExt->xferQue, pFrame);
      if (pFrame->addr) {
        if (device->mode == DEV_INPUT)
            Cache_inv (pFrame->addr, pFrame->size, Cache_Type_ALL, TRUE);
        else
            Cache_wbInv (pFrame->addr, pFrame->size, Cache_Type_ALL, TRUE);
    }

    // increment count
    pDevExt->numQueued += 1;

    // configure and insert transfer
    DAB_FTABLE_setupXfer (device, pParam->hEdma, parentEdma, pDevExt->errorEdma, pFrame);

    if ((pDevExt->errorState == PAF_SIO_ERROR_IDLE_STAGE1) && (device->mode == DEV_OUTPUT))
        pDevExt->errorState = PAF_SIO_ERROR_NONE;

    pDevExt->shutDown = 0;

    // re-enable interrupts
    //edma3OsProtectExit (EDMA3_OS_PROTECT_INTERRUPT_XFER_COMPLETION, NULL);
 HWI_enable ();
    return SYS_OK;
} // DAB_issue

// -----------------------------------------------------------------------------

Int DAB_reclaim (DEV_Handle device)
{ 
    DAB_DeviceExtension *pDevExt = (DAB_DeviceExtension *) (device->object);
    DEV_Frame *pFrame;


    // must be running and error free 
    if ((!pDevExt->runState) || (pDevExt->errorState))
        return SYS_EBADIO;

    // idle if necessary
    if (pDevExt->errorState == PAF_SIO_ERROR_FATAL) {
        DEV_idle (device, 1);
        return SYS_EBADIO;
    }

    if (!SEM_pend (pDevExt->sync, device->timeout))
        return SYS_ETIMEOUT;

    // idle if necessary
    if (pDevExt->errorState == PAF_SIO_ERROR_FATAL) {
        DEV_idle (device, 1);
        return SYS_EBADIO;
    }

    // since pend returned we know that head of fromdevice is valid
    pFrame = QUE_head (device->fromdevice);

    // invalidate CACHE region if input 
    //    Don't do anything if was for fill.
    // TODO: verify this invalidates L2 as well as L1.
    if ((device->mode == DEV_INPUT) && (pFrame->addr != NULL))
        //Edma3_CacheInvalidate ((unsigned int) pFrame->addr, (pFrame->size));
        Cache_inv (pFrame->addr, pFrame->size, Cache_Type_ALL, TRUE);

    return SYS_OK;
} // DAB_reclaim

// -----------------------------------------------------------------------------

Int DAB_mcbspReset (DEV_Handle device)
{
    DAB_DeviceExtension  *pDevExt = (DAB_DeviceExtension *) (device->object);
    int mcbspNum = pDevExt->pParams->sio.moduleNum;
    CSL_McbspRegsOvly pMcBspRegs = dabDrv.hMcbsp[mcbspNum]->regs;
	unsigned int * fifoBase = (unsigned int * )fifo_address_offset[mcbspNum];
   
        if (device->mode == DEV_OUTPUT) {
             if (dabDrv.fifoPresent[mcbspNum])
            	fifoBase[_MCBSP_WFIFOCTL_OFFSET]&=
                ~(MCBSP_WFIFOCTL_WENA_ENABLE << _MCBSP_WFIFOCTL_WENA_SHIFT); 
         	pMcBspRegs->SPCR &= ~CSL_MCBSP_SPCR_XRST_MASK;
        }
        else {
             if (dabDrv.fifoPresent[mcbspNum]) 
            	fifoBase[_MCBSP_RFIFOCTL_OFFSET] &=
                	~(MCBSP_RFIFOCTL_RENA_ENABLE << _MCBSP_RFIFOCTL_RENA_SHIFT);
        	pMcBspRegs->SPCR &= ~CSL_MCBSP_SPCR_RRST_MASK;
        }
    return SYS_OK;
} // DAB_mcbspReset

// -----------------------------------------------------------------------------
// This function disables HWI since the polling for FS change is time critical
#define FIFO_WNUMEVT 1
#define FIFO_WNUMDMA 1
#define FIFO_RNUMEVT 1
#define FIFO_RNUMDMA 1

Int DAB_mcbspEnable (DEV_Handle device)
{
    DAB_DeviceExtension  *pDevExt = (DAB_DeviceExtension *) (device->object);
    int mcbspNum = pDevExt->pParams->sio.moduleNum;
    CSL_McbspRegsOvly pMcBspRegs = dabDrv.hMcbsp[mcbspNum]->regs;
    Uint32 clkMask, fsmMask, fspMask, ioMask, rstMask;
	unsigned int * fifoBase= (unsigned int * )fifo_address_offset[mcbspNum];

    HWI_disable ();
    if (dabDrv.fifoPresent[mcbspNum]) {
        if (device->mode == DEV_OUTPUT) {
            fifoBase[_MCBSP_WFIFOCTL_OFFSET] =
                (FIFO_WNUMEVT << _MCBSP_WFIFOCTL_WNUMEVT_SHIFT) |
                (FIFO_WNUMDMA << _MCBSP_WFIFOCTL_WNUMDMA_SHIFT); 
            fifoBase[_MCBSP_WFIFOCTL_OFFSET] |=
                (MCBSP_WFIFOCTL_WENA_ENABLE << _MCBSP_WFIFOCTL_WENA_SHIFT);
        }
        else {
            fifoBase[_MCBSP_RFIFOCTL_OFFSET] =
                (FIFO_RNUMEVT << _MCBSP_RFIFOCTL_RNUMEVT_SHIFT) |
                (FIFO_RNUMDMA << _MCBSP_RFIFOCTL_RNUMDMA_SHIFT); 
            fifoBase[_MCBSP_RFIFOCTL_OFFSET] |=
                (MCBSP_RFIFOCTL_RENA_ENABLE << _MCBSP_RFIFOCTL_RENA_SHIFT);
        }
    }

    if (device->mode == DEV_INPUT) {
    	//*(unsigned int *)0x01d11818 = 0x10101;
        clkMask = CSL_MCBSP_PCR_CLKRP_MASK;
        fsmMask = CSL_MCBSP_PCR_FSRM_MASK;
        fspMask = CSL_MCBSP_PCR_FSRP_MASK;
        ioMask  = CSL_MCBSP_PCR_RIOEN_MASK;
        rstMask = CSL_MCBSP_SPCR_RRST_MASK;
    }
    else {
        clkMask = CSL_MCBSP_PCR_CLKXP_MASK;
        fsmMask = CSL_MCBSP_PCR_FSXM_MASK;
        fspMask = CSL_MCBSP_PCR_FSXP_MASK;
        ioMask  = CSL_MCBSP_PCR_XIOEN_MASK;
        rstMask = CSL_MCBSP_SPCR_XRST_MASK;
    }

    // place section in reset. This will be the case after power up but not after idle e.g.
    pMcBspRegs->SPCR &= ~rstMask;

    // alignment synchronization only needed if using external frame sync
    if (!(pMcBspRegs->PCR & fsmMask)) {
        int fsPolarity  = pMcBspRegs->PCR & fspMask;
        int clkPolarity = pMcBspRegs->PCR & clkMask;

        // set to IO mode
        pMcBspRegs->PCR |= ioMask;

        // wait for active to inactive transition on FS signal
        while ((pMcBspRegs->PCR & fspMask) == fsPolarity);
        while ((pMcBspRegs->PCR & fspMask) != fsPolarity);

        // set to serial port mode
        pMcBspRegs->PCR &= ~ioMask;
			   
        // restore FS and clock polarity since GPIO corrupted it
        pMcBspRegs->PCR |= fsPolarity;
        pMcBspRegs->PCR |= clkPolarity;
    }

    // take out of reset
    pMcBspRegs->SPCR |= rstMask;

    HWI_enable ();

    return SYS_OK;
} // DAB_mcbspEnable

// -----------------------------------------------------------------------------

Int DAB_shutdown (DEV_Handle device)
{
    DAB_DeviceExtension *pDevExt = (DAB_DeviceExtension *)(device->object);
    DEV_Frame *pFrame;
    int i;


    if (pDevExt->shutDown)
        return 1;

    if (pDevExt->pParams == NULL)
        return 0;

    if (pDevExt->activeEdma != EDMA_HINV)
        EDMA3_DRV_disableTransfer (hEdma, pDevExt->activeEdma, EDMA3_DRV_TRIG_MODE_EVENT);

    // reset queues
    while (!QUE_empty(device->todevice)) {
        // place oustanding requests onto holding queue
        pFrame = (DEV_Frame *) QUE_dequeue (device->todevice);
        QUE_enqueue (&pDevExt->xferQue, (QUE_Elem *) pFrame);
    }

    while (!QUE_empty(&pDevExt->xferQue)) {
        // pull frame from holding queue
        pFrame = (DEV_Frame *) QUE_dequeue (&pDevExt->xferQue);

        // place frame onto user queue and signal user thread
        QUE_enqueue (device->fromdevice, (QUE_Elem *) pFrame);
    }

    while (!QUE_empty(&pDevExt->paramQue))
        QUE_dequeue (&pDevExt->paramQue);

    // not running => can use non-atomic functions
    for (i=0; i < pDevExt->numEdmaParams; i++)
        QUE_enqueue (&pDevExt->paramQue, (QUE_Elem *) &pDevExt->edmaParams[i]);

    // reset counter
    pDevExt->numQueued = 0;

    // make sure active is linked to error
    EDMA3_DRV_linkChannel (hEdma, pDevExt->activeEdma, pDevExt->errorEdma);

    // refill frame list -- so user needn't call reclaim, which may cause Rx underrun.
    {
        SIO_Handle stream = (SIO_Handle) device;

        while (!QUE_empty(device->fromdevice)) {
            /* place oustanding requests onto holding queue */
            pFrame = (DEV_Frame *) QUE_dequeue (device->fromdevice);
            QUE_enqueue (&stream->framelist, (QUE_Elem *) pFrame);
        }
        SEM_reset (pDevExt->sync, 0);
    }

    pDevExt->shutDown = 1;
    pDevExt->numParamSetup = 0;

    return 0;
} // DAB_shutdown

// -----------------------------------------------------------------------------

Int DAB_setupXfer (DEV_Handle device, XDAS_UInt32 targetEdma, XDAS_UInt32 parentEdma, XDAS_UInt32 childEdma, DEV_Frame *pFrame)
{
    DAB_DeviceExtension *pDevExt = (DAB_DeviceExtension *)device->object;
//     int mcbspNum = pDevExt->pParams->sio.moduleNum;
//     DAB_EDMA_Param      *pParam;


    // TODO: shouldn't this just be tcc interrupt disable?
    // at least until linkage phase...
    HWI_disable ();

    // configure transfer
    DAB_FTABLE_setupParam (device, targetEdma, childEdma, (unsigned int) pFrame->addr, pFrame->size);

// TODO:
#if 0
    pParam = (DAB_EDMA_Param *) pFrame->misc;
    // link to parent(active) xfer last
    if (parentEdma != EDMA_HINV) {
        /* override parent, if necessary, since it is possible that
           last queued transfer is running. in this case it is the active
           transfer.
        */
        pTable = (volatile Uint32 *) EDMA_getTableAddress(pDevExt->activeEdma);
        if( (pTable[_EDMA_RLD_OFFSET] & 0xFFFF) == (pDevExt->errorEdma & 0xFFFF))
            hParent = pDevExt->activeEdma;

        DAB_FTABLE_edmaLink (hParent, hEdma);

        if (hParent != pDevExt->activeEdma) {
			if (!(hParent == hEdma)) {
				pTable = (volatile Uint32 *) EDMA_getTableAddress(pDevExt->activeEdma);
				if ((pTable[_EDMA_RLD_OFFSET] & 0xFFFF) == (pDevExt->errorEdma & 0xFFFF)) {
		            hParent = pDevExt->activeEdma;
		            DAB_FTABLE_edmaLink (hParent, hEdma);
				}
			}
		}
    }
#else
    if (parentEdma != EDMA_HINV)
        EDMA3_DRV_linkChannel (hEdma, parentEdma, targetEdma);
#endif

    HWI_enable ();

    return SYS_OK;
} // DAB_setupXfer

// -----------------------------------------------------------------------------

Int DAB_start (DEV_Handle device)
{
    DAB_DeviceExtension *pDevExt = (DAB_DeviceExtension *) device->object;
    DEV_Frame *pFrame;
    Int result;


    // Assume todevice queue is not empty -- how else could we be here?
    pFrame = (DEV_Frame *) QUE_get (device->todevice);

    // inidicate this xfer did not use param entry - just the active one
    pFrame->misc = NULL;

    // non-atomic functions since not running yet.
    QUE_enqueue (&pDevExt->xferQue, pFrame);

    // initialize count
    pDevExt->numQueued = 1;

    result = DAB_FTABLE_mcbspReset (device);
    if (result)
        return result;

    // config active xfer for this buffer
    DAB_FTABLE_setupXfer (device, pDevExt->activeEdma, EDMA_HINV, pDevExt->errorEdma, pFrame);

    // signal we have started -- this must come before last enable to prevent a race
    // condition where the initial EDMA transfer is very small (e.g. due to startClocks)
    // and completes before any further instructions in this thread are executed.
    // This comes before the EDMA enable since, if the # of samples is 1, then the EDMA
    // will be serviced and generate an interrupt even before the McBSP is enabled.
    pDevExt->runState = 1;
    pDevExt->shutDown = 0;

    // enable interrupts and event servicing for this channel
    EDMA3_DRV_enableTransfer (hEdma, pDevExt->activeEdma, EDMA3_DRV_TRIG_MODE_EVENT);

    result = DAB_FTABLE_mcbspEnable (device);
    if (result)
        return result;

    return SYS_OK;
} // DAB_start

// -----------------------------------------------------------------------------
// Assumes that EDMA3 dispatcher handles TCC clearing.

void DAB_isrCallback (Uint32 tcc, EDMA3_RM_TccStatus status, Ptr context)
{
    DEV_Handle                 device;
    DAB_DeviceExtension       *pDevExt;
    DEV_Frame                 *pFrame;
    unsigned int               opt;


    // could be here after Tx idle/overrun and this is the interrupt
    // for the last occuring error transfer so there is no transfer
    // to release, we just clear the int and exit. 
   
    device = (DEV_Handle) context;
    pDevExt = (DAB_DeviceExtension *)(device->object);

    if ((pDevExt->runState == 1) && !pDevExt->errorState) {
        // if here then an interrupt occured due to errorEdma or valid
        // transfer, we assume the xfer is long enough so it will not complete
        // before we are finished here.

        // if last transfer was valid then complete it
        if (!QUE_empty(&pDevExt->xferQue)) {

            // pull frame from holding queue
            pFrame = (DEV_Frame *) QUE_dequeue (&pDevExt->xferQue);

            // if used param entry then return it to queue
            if (pFrame->misc != NULL)
                QUE_enqueue (&pDevExt->paramQue, (Ptr) pFrame->misc);

            // decrement count
            pDevExt->numQueued -= 1;

            // place frame onto user queue and signal user thread
            QUE_enqueue (device->fromdevice, (Ptr) pFrame);

            // signal user thread
            SEM_ipost (pDevExt->sync);
        }

        // determine if currently transferring buffer is valid based on interrupt enable bit
        // only valid transfers will generate interrupts
        EDMA3_DRV_getPaRAMEntry (hEdma, pDevExt->activeEdma, EDMA3_DRV_PARAM_ENTRY_OPT, &opt);
        
        if (!(opt & EDMA3_DRV_OPT_TCINTEN_SET_MASK (1)))
            pDevExt->errorState = PAF_SIO_ERROR_ERRBUF_XFER;
		
    } // runState

    return;
} //DAB_isrCallback

// -----------------------------------------------------------------------------
// Configure EDMA3 parameter entry

Int DAB_setupParam (DEV_Handle device, XDAS_UInt32 targetEdma, XDAS_UInt32 childEdma, unsigned int addr, unsigned int size)
{
    DAB_DeviceExtension *pDevExt = (DAB_DeviceExtension *)device->object;
    int mcbspNum = pDevExt->pParams->sio.moduleNum;
    EDMA3_DRV_PaRAMRegs  edmaConfig;


    // Init opt parameter to 0 which, without being overriden, configures as:
    //    A synchronized transfer (no FIFO mode on src or dst)
    //    no chaining or intermediate interrupts
    //    param is not static
    //    normal completion
    //    don't generate an interrupt (overriden below for regular xfers)
    edmaConfig.opt = 0;

    // not transferring blocks so c index is 0
    edmaConfig.destCIdx = 0;
    edmaConfig.srcCIdx = 0;

    // if regular transfer then enable interrupt with tcc code
    if (targetEdma != pDevExt->errorEdma) {
        edmaConfig.opt |= EDMA3_DRV_OPT_TCINTEN_SET_MASK (1);
        edmaConfig.opt |= EDMA3_DRV_OPT_TCC_SET_MASK (pDevExt->firstTCC);
    }

    // cCnt blocks of bCnt words with word size of aCnt bytes
    //  since cCnt is 1 no bCntReload
    edmaConfig.aCnt = pDevExt->edmaWordSize;
    edmaConfig.bCnt = size/pDevExt->edmaWordSize;
    edmaConfig.cCnt = 1;
    edmaConfig.bCntReload = 0;
    // handle direction specific requirements
    if (device->mode == DEV_INPUT) {
        edmaConfig.srcBIdx  = 0;
        if(dabDrv.fifoPresent[mcbspNum]) 
         	edmaConfig.srcAddr  = (unsigned int) (fifo_rxbuf[mcbspNum]);
        else
        	edmaConfig.srcAddr  = (unsigned int) (dabDrv.hMcbsp[mcbspNum]->regs);
        
        
        if (addr) {
            edmaConfig.destBIdx = pDevExt->edmaWordSize;
            edmaConfig.destAddr = addr;
            if(pDevExt->edmaWordSize == 2)
            	edmaConfig.srcAddr= (unsigned int)edmaConfig.srcAddr+ 2;
        }
        else {
        	//edmaConfig.srcAddr  = 0x01D11000;
        	//edmaConfig.srcAddr  = 0x01F11002;
        	if(pDevExt->edmaWordSize == 2)
            	edmaConfig.srcAddr= (unsigned int)edmaConfig.srcAddr+ 2;
            edmaConfig.destBIdx = 0;
            edmaConfig.destAddr = (unsigned int) &dab_BDFE;
        }        
    }
    else {
        edmaConfig.destBIdx = 0;
        if(dabDrv.fifoPresent[mcbspNum])
        	 edmaConfig.destAddr = (unsigned int) (fifo_rxbuf[mcbspNum]);
       	else
        	edmaConfig.destAddr = (unsigned int) (dabDrv.hMcbsp[mcbspNum]->regs) + 4;

        if (addr) {
            edmaConfig.srcBIdx  = pDevExt->edmaWordSize;
            edmaConfig.srcAddr  = addr;
            Edma3_CacheFlush ((unsigned int) addr, (size+3)/4);
        }
        else {
            edmaConfig.srcBIdx  = 0;
            edmaConfig.srcAddr  = (unsigned int) &dab_ZDFE;
        }
    }

    EDMA3_DRV_setPaRAM (hEdma, targetEdma, &edmaConfig);

    // link child xfer
    if (childEdma != EDMA_HINV)
        EDMA3_DRV_linkChannel (hEdma, targetEdma, childEdma);
#if 0
gArr[gCount++]=*(unsigned int *)0x01c0408c;
gArr[gCount++]=(((((*(unsigned int *)0x01c04094)<<16)>> 16)-0x4000)/0x20) +(targetEdma << 16);
gArr[gCount++]=((*(unsigned int *)0x01c0409c)& 0xffff) + ((size/pDevExt->edmaWordSize) << 16);
gArr[gCount++]=addr;
if(gCount==160)
		gCount=0;
#endif		
    return SYS_OK;
} //DAB_setupParam

// -----------------------------------------------------------------------------










