
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Notify Information Change data structures file
//
//
//

#ifndef PAFPINASSERT_
#define PAFPINASSERT_

#include <paftyp.h>
#include <pafpin.h>

#define NUM_SOURCE_INTERRUPT	3

typedef struct PAF_PinAssertStatus {
	Int size;
	XDAS_UInt8 mode;
	XDAS_Int8 assertmode;
	XDAS_Int8 assertstate;
	XDAS_Int8 deassertallow;
	XDAS_Int8 statChange;			//Internal state
	PAF_Pin pinAssert;
	volatile Int prevStat[NUM_SOURCE_INTERRUPT]; 	//To store the previous state
	XDAS_UInt16 from[NUM_SOURCE_INTERRUPT][5];//Read Alpha codes,Length of an Type 7 read alpha code is 4 i.e max.
	XDAS_Int8 unused[3];
}PAF_PinAssertStatus;



#endif  /* PAFPINASSERT_ */
