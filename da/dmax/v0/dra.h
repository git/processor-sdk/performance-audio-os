
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX RAM Access Service Routine data structures
//
//
//

#ifndef DRA_H_ 
#define DRA_H_ 

#include <dra.inc>
#include <tistdtypes.h>
#include <dmax_struct.h>

/* DRA macros */
#define DRA_MKCTRL(RW, OFF)                      \
    (((RW) << DRA_RW_SHFT) |                     \
     ((OFF) << DRA_OFF_SHFT))

#define DRA_MKEE(PTB, PT)                        \
    ((((PTB) & 0x000001FF) << 8) |               \
     (PT))

/* DRA parameter format */
typedef struct dra {
    Uint16 ctrl;
    Uint8  cnt;
    Uint8  tcc;
    Uint32 data;
} dra;

/* DRA API declarations */
extern Uint32 dMAX_draOpen(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                           Int32 tcc);
extern void dMAX_draClose(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 tcc,Uint32 pt);
extern void dMAX_draCfgEE(dMAX_Handle handle,Uint32 evt,Uint32 maxid,
                          Uint32 ee);
extern Uint32 dMAX_draOff(dMAX_Handle handle,Uint32 maxid,Uint32 pt,
                          Uint32 off);
extern Uint32 dMAX_draRW(dMAX_Handle handle,Uint32 evt,Uint32 pt,
                         dra *pdra);
#endif // DRA_H_

