#include <xdc/std.h>
#include "acptype.h"
#include "procsdk_audio_typ.h"


Int
alpha_length(const ACP_Unit *from)
{
    ACP_Union *x = (ACP_Union *)from;

    SmInt len;

    static const SmInt length[16] = {
        2, // type 0 read
        1, // type 1 read
        2, // type 2 read
        2, // type 3 read
        2, // type 4 read
        -1, // type 5 read
        2, // type 6 read
        4, // type 7 read
        2, // type 0 write
        -4, // type 1 write
        2, // type 2 write
        3, // type 3 write
        4, // type 4 write
        -1, // type 5 write
        -2, // type 6 write
        -3, // type 7 write
    };

    if ((x[0].byte.hi & 0xc0) == 0x40)
        return -1; // DA/M
    else if ((x[0].byte.hi & 0xc0) != 0xc0)
        return (x[1].word & 0x7fff) + 14; // IA/M
    else if (x[0].word == 0xffff)
        return 1; // non-zero fill

    len = length[x[0].byte.hi & 0x0f];

    if (len >= 0)
        return len;
    else if (len == -1) {
        MdUns alpha = x[0].word & 0xcfff;
        if (alpha == 0xc500)
            /* type 5-0 read  */ return 4;
        else if (alpha == 0xcd00)
            /* type 5-0 write */ return 4;
        else if (alpha == 0xc501)
            /* type 5-1 read  */ return 2;
        else if (alpha == 0xcd01)
            /* type 5-1 write */ return x[1].word + 2;
        else if (alpha == 0xc506)
            /* type 5-6 read  */ return 4;
        else if (alpha == 0xcd06)
            /* type 5-6 write */ return 4+(x[3].word+1)/2;
        else if (alpha == 0xc508)
            /* type 5-8 read  */ return 2;
        else if (alpha == 0xc509)
            /* type 5-9 read  */ return 2;
        else if (alpha == 0xcd09)
            /* type 5-9 write */ return 2;
        else if (alpha == 0xc50a)
            /* type 5-10 read  */ return 4;
        else if (alpha == 0xcd0a)
            /* type 5-10 write */ return 4+(x[3].word+1)/2;
        else if (alpha == 0xc50b)
            /* type 5-11 read  */ return x[1].word + 2;
        else if (alpha == 0xcd0b)
            /* type 5-11 write */ return x[1].word + 2;
        else if (alpha == 0xc50c)
            /* type 5-12 read  */ return 2;
        else if (0xc5f0 <= alpha && alpha <= 0xc5f3)
            /* type 5-24X read  */ return 2;
        else if (0xcdf0 <= alpha && alpha <= 0xcdff)
            /* type 5-24X write */ return 2;
        return -3; // Unknown ?
    }
    else if (len == -2) {
        SmUns kappa = (SmUns )x[1].byte.lo;
        return 2+(kappa+1)/2;
    }
    else if (len == -3) {
        Uns kappa = (MdUns )x[1].word;
        return 4+(kappa+1)/2;
    }
    else if (len == -4) {
        Uns lambda = (SmUns )x[0].byte.lo;
        return 1+lambda;
    }
    else
        return -3; // Unknown ?
}
