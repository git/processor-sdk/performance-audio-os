
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the Synchornous Rate Conversion Algorithm.
 */
#ifndef ISRC_
#define ISRC_

#include <ialg.h>
#include  <std.h>
#include <xdas.h>

#include "icom.h"
#include "paftyp.h"

typedef struct {
	unsigned short	type;		// 0: symmetric half, 1: symmetric full
	unsigned short	order;		// filter order
	float *			cf;
} FilterCoefs;

typedef struct ISRC_memRec {
    unsigned int	size;       // size in MAU of allocation 
    char			space;      // allocation space //IALG_MemSpace
    unsigned char   attrs;      // memory attributes //IALG_MemAttrs
} ISRC_memRec;

/*
 *  ======== ISRC_Obj ========
 *  Every implementation of ISRC *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct ISRC_Obj {
    struct ISRC_Fxns *fxns;    /* function list: standard, public, private */
} ISRC_Obj;

/*
 *  ======== ISRC_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct ISRC_Obj *ISRC_Handle;

/*
 *  ======== ISRC_Status ========
 *  Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.
 */
typedef volatile struct ISRC_Status {
    Int size;
    XDAS_Int8 mode;
    XDAS_Int8 rateRequest;
    XDAS_Int8 rateStream;
    XDAS_Int8 sampleRate;
   	const FilterCoefs *dn1toH;
   	const FilterCoefs *dnHtoQ;
	const FilterCoefs *up1to2;
	const FilterCoefs *up2to4;
   
} ISRC_Status;

/*
 *  ======== ISRC_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */

typedef struct ISRC_Config {
    XDAS_Int8			nChannels;
	XDAS_Int8			nMemRec;
    XDAS_UInt16			mask;
	PAF_AudioData		*state;
    PAF_AudioData		*tmp;
	const ISRC_memRec	*pMemRec;
} ISRC_Config;

/*
 *  ======== ISRC_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a SRC object.
 *
 *  Every implementation of ISRC *must* declare this structure as
 *  the first member of the implementation's parameter structure.
 */

typedef struct ISRC_Params {
    Int size;
    const ISRC_Status	*pStatus;
    ISRC_Config			config;
} ISRC_Params;

/*
 *  ======== ISRC_PARAMS ========
 *  Default instance creation parameters (defined in isrc.c)
 */

extern const ISRC_Params ISRC_PARAMS;

extern const ISRC_Params ISRC_PARAMS_MAX192;
extern const ISRC_Params ISRC_PARAMS_MAX48;
extern const ISRC_Params ISRC_PARAMS_MIN32;
extern const ISRC_Params ISRC_PARAMS_DS_2CH;
extern const ISRC_Params ISRC_PARAMS_DS_8CH;
extern const ISRC_Params ISRC_PARAMS_DS_10CH;
extern const ISRC_Params ISRC_PARAMS_US_2CH;
extern const ISRC_Params ISRC_PARAMS_US_8CH;
extern const ISRC_Params ISRC_PARAMS_US_10CH;

extern const ISRC_Params ISRC_PARAMS_MAX192_HBW;
extern const ISRC_Params ISRC_PARAMS_MAX48_HBW;
extern const ISRC_Params ISRC_PARAMS_MIN32_HBW;
extern const ISRC_Params ISRC_PARAMS_DS_2CH_HBW;
extern const ISRC_Params ISRC_PARAMS_DS_8CH_HBW;
extern const ISRC_Params ISRC_PARAMS_DS_10CH_HBW;
extern const ISRC_Params ISRC_PARAMS_US_2CH_HBW;
extern const ISRC_Params ISRC_PARAMS_US_8CH_HBW;
extern const ISRC_Params ISRC_PARAMS_US_10CH_HBW;

/*
 *  ======== ISRC_Fxns ========
 *  All implementation's of SRC must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is SRC_<vendor>_ISRC, where
 *  <vendor> is the vendor name.
 */
typedef struct ISRC_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(ISRC_Handle, PAF_AudioFrame *);
    Int         (*apply)(ISRC_Handle, PAF_AudioFrame *);
    /* private */
    Int         (*ratio[4])(ISRC_Handle, PAF_AudioFrame *, Int);
 	Void (*dsamp2x)(PAF_AudioData *, PAF_AudioData *, PAF_AudioData *, int, int);
	Void (*usamp2x)(PAF_AudioData *, PAF_AudioData *, PAF_AudioData *, int, int);
} ISRC_Fxns;

/*
 *  ======== ISRC_Cmd ========
 *  The Cmd enumeration defines the control commands for the SRC
 *  control method.
 */
typedef enum ISRC_Cmd {
    ISRC_NULL                   = ICOM_NULL,
    ISRC_GETSTATUSADDRESS1      = ICOM_GETSTATUSADDRESS1,
    ISRC_GETSTATUSADDRESS2      = ICOM_GETSTATUSADDRESS2,
    ISRC_GETSTATUS              = ICOM_GETSTATUS,
    ISRC_SETSTATUS              = ICOM_SETSTATUS
} ISRC_Cmd;

#endif  /* ISRC_ */
