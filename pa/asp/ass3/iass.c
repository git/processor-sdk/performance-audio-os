
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common Audio Stream Split Algorithm interface implementation
//
//
//

/*
 *  IASS default instance creation parameters
 */
#include <std.h>

#include "iass.h"
#include "paftyp.h"

/*
 *  ======== IASS_PARAMS ========
 *  This static initialization defines the default parameters used to
 *  create instances of ASS objects.
 */
const IASS_Status IASS_PARAMS3_STATUS = {
    sizeof(IASS_Status),
    1,
    2*10, 1, 0,
    { PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, PAF_CC_AUX_STEREO_STEREO, 0, },
    { 0, 0, 0, 0, },
    -2*3,
    -2*3,    
};
const IASS_Params IASS_PARAMS3 = {
    sizeof(IASS_Params),
    &IASS_PARAMS3_STATUS,
    0, 1,
    0, 0,
};

const IASS_Status IASS_PARAMS4_STATUS = {
    sizeof(IASS_Status),
    1,
    2*10, 0, 0,
    { PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, PAF_CC_AUX_SURROUND4_UNKNOWN, 0, },
    { 0, 0, 0, 0, },
    -2*3,
    -2*3,    
};
const IASS_Params IASS_PARAMS4 = {
    sizeof(IASS_Params),
    &IASS_PARAMS4_STATUS,
    0, 2,
    0, 0,
};

