
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
//
//

#ifndef _CSLR_DMAXPDSP_1_H_
#define _CSLR_DMAXPDSP_1_H_

#if 0
#include <cslr.h>
#endif
#include "dmaxpdsp.h"


/**************************************************************************\
 * Peripheral Instance count
\**************************************************************************/

#define DMAXPDSP_PER_CNT                  2
/**************************************************************************\
 * Overlay structure typedef definition
\**************************************************************************/
typedef CSL_DmaxpdspRegs    DMAXPDSP_REGS;
/**************************************************************************\
 * Peripheral Base Address
\**************************************************************************/

#ifndef BOARD_E4
#define DMAXPDSP0_BASEADDR                	(0x01C37000U)
#define DMAXPDSP0_REGS                  	 ((CSL_DmaxpdspRegs  *) DMAXPDSP0_BASEADDR)
#define DMAXPDSP1_BASEADDR                	(0x01C37800U)
#define DMAXPDSP1_REGS                          (( CSL_DmaxpdspRegs *) DMAXPDSP1_BASEADDR )
#define DMAXPDSP0_IRAM_BASEADDR                	(0x01C38000U)
#define DMAXPDSP1_IRAM_BASEADDR                	(0x01C3C000U)
#define DMAX_DRAM0_BASEADDR                	(0x01C30000U)
#define DMAX_DRAM1_BASEADDR                	(0x01C32000U)
#else
#define DMAXPDSP0_BASEADDR                	(0xB0207000U)
#define DMAXPDSP0_REGS                  	((CSL_DmaxpdspRegs  *) DMAXPDSP0_BASEADDR)	
#define DMAXPDSP1_BASEADDR                	(0xB0207800U)
#define DMAXPDSP1_REGS                      ((CSL_DmaxpdspRegs *) DMAXPDSP1_BASEADDR )
#define DMAXPDSP0_IRAM_BASEADDR             (0xB0208000U)
#define DMAXPDSP1_IRAM_BASEADDR             (0xB020C000U)
#define DMAX_DRAM0_BASEADDR                	(0xB0200000U)
#define DMAX_DRAM1_BASEADDR                	(0xB0202000U)
#endif

#endif //_CSLR_DMAXPDSP_1_H_
