/******************************************************************************
 * Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

//
//
// Vendor-specific (TII) Downmix algorithm
// IALG table definitions
//
//
//
/*
 *  This file contains the function table definitions for all
 *  interfaces implemented by the DM_TII module that derive
 *  from IALG
 *
 *  We place these tables in a separate file for two reasons:
 *     1. We want allow one to one to replace these tables
 *        with different definitions.  For example, one may
 *        want to build a system where the DM is activated
 *        once and never deactivated, moved, or freed.
 *
 *     2. Eventually there will be a separate "system build"
 *        tool that builds these tables automatically 
 *        and if it determines that only one implementation
 *        of an API exists, "short circuits" the vtable by
 *        linking calls directly to the algorithm's functions.
 */
#include <xdc/std.h>
#include <ti/xdais/ialg.h>

#include <idm.h>
#include <dm_tii.h>
#include <dm_tii_priv.h>

#define IALGFXNS \
    &DM_TII_IALG,       /* module ID */                         \
    DM_TII_activate,    /* activate */                          \
    DM_TII_alloc,       /* alloc */                             \
    DM_TII_control,     /* control */                           \
    DM_TII_deactivate,  /* deactivate */                        \
    DM_TII_free,        /* free */                              \
    DM_TII_initObj,     /* init */                              \
    DM_TII_moved,       /* moved */                             \
    NULL                /* numAlloc (NULL => IALG_MAXMEMRECS) */\

/*
 *  ======== DM_TII_IDM ========
 *  This structure defines TII's implementation of the IDM interface
 *  for the DM_TII module.
 */
const IDM_Fxns DM_TII_IDM = { /* module_vendor_interface */
IALGFXNS, DM_TII_reset, DM_TII_apply, };

/*
 *  ======== DM_TII_IALG ========
 *  This structure defines TII's implementation of the IALG interface
 *  for the DM_TII module.
 */
#ifdef _TMS320C6X

asm("DM_TII_IALG .set DM_TII_IDM");

#else

/*
 *  We duplicate the structure here to allow this code to be compiled and
 *  run non-DSP platforms at the expense of unnecessary data space
 *  consumed by the definition below.
 */
IALG_Fxns DM_TII_IALG = { /* module_vendor_interface */
    IALGFXNS
};

#endif
