
# 
#  Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
#  All rights reserved.	
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
# 
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
# 
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# 

#
#
# Component Makefile template
#
#
# -----------------------------------------------------------------------------

# First invocation is normally from primary sources directory for the component.
# It is assumed the name of this directory doesn't match either of the standard
# target directory names (i.e. debug or release). So this first call then
# includes the target.mk file which only has one target -- the target directory.
# This is a phony target which causes a recursive call to this file every time
# but rooted in the target directory. Upon this second call the else statement
# is executed which performs the conventional make process.
ifeq (,$(filter debug release ,$(notdir $(CURDIR))))
include ../build/target.mk
else
include ../../../build/tools.mk

LIBNAME=fwk

# architecture dependent settings (must come first)
ifeq (${ARCH},c674x)
DEFINES  += -d"dMAX_CFG=dMAX_CFG1"
DEFINES  += -d"FULL_SPEAKER"
DEFINES  += -d"DAP_CACHE_SUPPORT"
INCLUDES += -I"../../../../da/dmax/v0/da8xx"
vpath %.c $(SRCDIR)/../../da/dmax/v0/da8xx
endif

# architecture independent settings
SOURCES=                                        \
    default.c                                   \
    dmax.c                                      \
    dmax_struct.c                               \
    pafsio.c                                    \
    pafsio_ialg.c                               \
    ios_s.c                                     \
    attime_s.c                                  \
    aip.c                                       \
    as0.c                                       \
    asp0.c                                      \
    atboot_s.c                                  \
    lm_s.c                                      \
    paf_alg_create.c                            \
    paf_alg_malloc.c                            \
    paf_alg_print.c                             \
    ss0.c                                       \
    ss0_6.c                                     \
    as1-f2.c                                    \
    afp6.c                                      \
    afp_uart.c

# component specific includes
INCLUDES += -I../..
INCLUDES += -I"../../../asp/arc1/alpha"
INCLUDES += -I"../../../asp/bm2"
INCLUDES += -I"../../../asp/del"
INCLUDES += -I"../../../asp/fil/alg"
INCLUDES += -I"../../../asp/std"
INCLUDES += -I"../../../dec/dwr"
INCLUDES += -I"../../../dec/pcm1"
INCLUDES += -I"../../../dec/sng1"
INCLUDES += -I"../../../enc/pce1"
INCLUDES += -I"../../../sio/acp1"
INCLUDES += -I"../../../sio/dcs6"
INCLUDES += -I"../../../sio/dib"
INCLUDES += -I"../../../sio/dob"
INCLUDES += -I"../../../sio/paf"
INCLUDES += -I"../../s2"
INCLUDES += -I"../../s3"
INCLUDES += -I"../../s19"
INCLUDES += -I"../../../../da/chip/primus/csl"
INCLUDES += -I"../../../../da/chip/primus/csl/c674x"

vpath %.c $(SRCDIR)/../dec/dwr
vpath %.c $(SRCDIR)/../sio/paf
vpath %.c $(SRCDIR)/s17
vpath %.c $(SRCDIR)/s3
vpath %.c $(SRCDIR)/s2
vpath %.c $(SRCDIR)/s19

include ../../../build/rules.mk

endif
