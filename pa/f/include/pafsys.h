
/*
* Copyright (C) 2004-2017 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.    
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework System Declarations
//
//
//

#ifndef PAFSYS_
#define PAFSYS_

#include <xdc/std.h>

#include <pafcc.h>

#include "pfp/pfp.h"

#define NUM_TASK_ERROR_REPORT 5

#if 0 //Integer only PFP alpha interface
/* PAF System PFP statistics reporting structure */
typedef struct PAF_SystemStatus_PfpStats {
    long long       c_total;
    uint_least32_t  n_count;
    uint_least32_t  c_min;
    uint_least32_t  c_max;
    uint_least32_t  c_average;    // U32Q0
    uint_least32_t  alpha;        // U32Q32
    //float  c_average;
    //float  alpha;
} PAF_SystemStatus_PfpStats;
#endif

typedef volatile struct PAF_SystemStatus {
    Int size;
    UInt8 mode;                                             //0x04
    UInt8 listeningMode;                                    //0x05
    UInt8 recreationMode;                                   //0x06
    UInt8 speakerMain;                                      //0x07
    UInt8 speakerCntr;                                      //0x08
    UInt8 speakerSurr;                                      //0x09
    UInt8 speakerBack;                                      //0x0a
    UInt8 speakerSubw;                                      //0x0b
    UInt8 channelConfigurationRequestType;                  //0x0c
    UInt8 switchImage;                                      //0x0d
    UInt8 imageNum;                                         //0x0e
    UInt8 imageNumMax;                                      //0x0f
    UInt32 reserved; // can be used in future               //0x10
    UInt16 cpuLoad;                                         //0x14
    UInt16 peakCpuLoad;                                     //0x16
    UInt8 speakerWide;                                      //0x18
    UInt8 speakerHead;                                      //0x19
    UInt8 speakerTopfront;                                  //0x1a
    UInt8 speakerToprear;                                   //0x1b
    UInt8 speakerTopmiddle;                                 //0x1c
    UInt8 speakerFrontheight;                               //0x1d
    UInt8 speakerRearheight;                                //0x1e
    UInt8 unused2; // to make 64 bit aligned                //0x1f
    PAF_ChannelConfiguration channelConfigurationRequest;   //0x20
#ifdef FULL_SPEAKER
    UInt8 speakerScreen;                                    //0x28
    UInt8 speakerSurr1;                                     //0x29
    UInt8 speakerSurr2;                                     //0x2a
    UInt8 speakerRearSurr1;                                 //0x2b
    UInt8 speakerRearSurr2;                                 //0x2c
    UInt8 speakerCntrSurr;                                  //0x2d
    UInt8 speakerLRCntr;                                    //0x2e
    UInt8 speakerLRCntrSurr;                                //0x2f
#else
    UInt8 unused3[8];                                       //0x28
#endif
    UInt8 asitLoad;                                        //0x30
    UInt8 peakAsitLoad;                                     //0x31
    UInt8 asotLoad;                                         //0x32
    UInt8 peakAsotLoad;                                     //0x33
    UInt8 aipLoad;                                          //0x34
    UInt8 peakAipLoad;                                      //0x35
    UInt8 afpLoad;                                          //0x36
    UInt8 peakAfpLoad;                                      //0x37
    UInt8 ssLoad;                                           //0x38
    UInt8 peakSsLoad;                                       //0x39
    UInt8 unused4[2];                                       //0x3a
    UInt32 pfpDisableMask;                                  //0x3c
    UInt32 pfpEnableMask;                                   //0x40
    UInt32 pfpEnabledBf;                                    //0x44
    UInt32 pfpLatchStatsMask;                               //0x48
    UInt32 pfpResetStatsMask;                               //0x4c
    UInt32 pfpAlphaUpdateMask;                              //0x50
    float pfpAlphaUpdate;                                   //0x54
    pfpStats_t ssPfpStats[PFP_PPCNT_MAX];                   //0x58
    //Integer only PFP alpha interface
    //PAF_SystemStatus_PfpStats ssPfpStats[PFP_PPCNT_MAX];    //0x58
} PAF_SystemStatus;

typedef volatile struct PAF_ErrorStatus {
    Int size;
    UInt8  mode;
    UInt8  errRst[2*NUM_TASK_ERROR_REPORT];
    UInt8  unused;
    UInt32 prevErr[2*NUM_TASK_ERROR_REPORT];
    UInt32 curErr[2*NUM_TASK_ERROR_REPORT];
} PAF_ErrorStatus;


#endif  /* PAFSYS_ */
