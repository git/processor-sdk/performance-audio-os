* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.

			.global _biquadFf2chSP 

_biquadFf2chSP	;		sptr		cptr		iptra		iptrb		optr		cnt
				;		a4			b4			a6			b6			a8			b8

	.asg	a0,sptr
	.asg	b0,cptr
	.asg	a1,cnt
	.asg	a2,optra
	.asg	a3,iptra
	.asg	b1,optrb
	.asg	b2,iptrb

	.asg	a4,s0a
	.asg	a5,s1a
	.asg	a6,s2a
	.asg	a7,s3a
	.asg	a8,prda
	.asg	b3,s0b
	.asg	b4,s1b
	.asg	b5,s2b
	.asg	b6,s3b
	.asg	b7,prdb

	.asg	a9,i0a
	.asg	a10,i1a
	.asg	a11,i2a
	.asg	a12,i3a
	.asg	b8,i0b
	.asg	b9,i1b
	.asg	b10,i2b
	.asg	b11,i3b

	.asg	a13,b0a
	.asg	a14,b1a
	.asg	a15,b2a
	.asg	b12,b0b
	.asg	b13,b1b
	.asg	b14,b2b


		stw.d2		a10,*b15--[1]
||		mvc			csr,b0

		stw.d2		b10,*b15--[1]
		stw.d2		a11,*b15--[1]
		
		stw.d2		b11,*b15--[1]
||		clr			b0,0,0,b0

		stw.d2		a12,*b15--[1]
		stw.d2		b12,*b15--[1]
		stw.d2		a13,*b15--[1]
		
		stw.d2		b13,*b15--[1]
||		mvc			b0,csr

		stw.d2		a14,*b15--[1]
		stw.d2		b14,*b15--[1]
		stw.d2		a15,*b15--[1]				; save a10-a15, b10-b14 on stack
		stw.d2		b3,*b15--[1]				; save return address

		; reassign parameters

		mv		a4,sptr
		mv		b4,cptr
		mv		a6,iptra
		mv		b6,iptrb
		mv		a8,optra
		add		a8,4,optrb
		sub		b8,4,cnt

		; load filter states

PROLOG		
		ldw		*sptr[0],i0a
		ldw		*cptr[0],b0a
		ldw		*sptr[1],i1a
		ldw		*cptr[1],b1a
		ldw		*sptr[2],i0b
||		ldw		*cptr[2],b2a
		ldw		*sptr[3],i1b
		ldw		*cptr[3],b0b
		ldw		*cptr[4],b1b
		ldw		*cptr[5],b2b

		ldw		*iptra++[1],i2a
||		ldw		*iptrb++[1],i2b

		ldw		*iptra++[1],i3a
||		ldw		*iptrb++[1],i3b

		nop

		ldw		*iptra++,i0a
||		ldw		*iptrb++,i0b
||		mpysp	i0a,b0a,s0a
||		mpysp	i0b,b0b,s0b

		ldw		*iptra++,i1a
||		ldw		*iptrb++,i1b
||		mpysp	i1a,b0a,s1a
||		mpysp	i1b,b0b,s1b

		mpysp	i2a,b0a,s2a
||		mpysp	i2b,b0b,s2b

		mpysp	i3a,b0a,s3a
||		mpysp	i3b,b0b,s3b

		mpysp	i1a,b1a,prda
||		mpysp	i1b,b1b,prdb

		mpysp	i2a,b1a,prda
||		mpysp	i2b,b1b,prdb

		mpysp	i3a,b1a,prda
||		mpysp	i3b,b1b,prdb

LOOP
			ldw		*iptra++,i2a
||			ldw		*iptrb++,i2b
||			mpysp	i0a,b1a,prda
||			mpysp	i0b,b1b,prdb

			ldw		*iptra++,i3a
||			ldw		*iptrb++,i3b
||			mpysp	i2a,b2a,prda
||			mpysp	i2b,b2b,prdb
||			addsp	prda,s0a,s0a
||			addsp	prdb,s0b,s0b

			mpysp	i3a,b2a,prda
||			mpysp	i3b,b2b,prdb
||			addsp	prda,s1a,s1a
||			addsp	prdb,s1b,s1b

			mpysp	i0a,b2a,prda
||			mpysp	i0b,b2b,prdb
||			addsp	prda,s2a,s2a
||			addsp	prdb,s2b,s2b

			mpysp	i1a,b2a,prda
||			mpysp	i1b,b2b,prdb
||			addsp	prda,s3a,s3a
||			addsp	prdb,s3b,s3b

			addsp	prda,s0a,s0a
||			addsp	prdb,s0b,s0b

			ldw		*iptra++,i0a
||			ldw		*iptrb++,i0b
||			mpysp	i0a,b0a,s0a
||			mpysp	i0b,b0b,s0b
||			addsp	prda,s1a,s1a
||			addsp	prdb,s1b,s1b
||			sub		cnt,4,cnt

			ldw		*iptra++,i1a
||			ldw		*iptrb++,i1b
||			mpysp	i1a,b0a,s1a
||			mpysp	i1b,b0b,s1b
||			addsp	prda,s2a,s2a
||			addsp	prdb,s2b,s2b
|| [cnt]	b		LOOP

			mpysp	i2a,b0a,s2a
||			mpysp	i2b,b0b,s2b
||			addsp	prda,s3a,s3a
||			addsp	prdb,s3b,s3b

			stw		s0a,*optra++[4]
||			stw		s0b,*optrb++[4]
||			mpysp	i3a,b0a,s3a
||			mpysp	i3b,b0b,s3b

			stw		s1a,*optra++[4]
||			stw		s1b,*optrb++[4]																																
||			mpysp	i1a,b1a,prda
||			mpysp	i1b,b1b,prdb

			stw		s2a,*optra++[4]
||			stw		s2b,*optrb++[4]
||			mpysp	i2a,b1a,prda
||			mpysp	i2b,b1b,prdb

			stw		s3a,*optra++[4]
||			stw		s3b,*optrb++[4]
||			mpysp	i3a,b1a,prda
||			mpysp	i3b,b1b,prdb
								
EPILOG

		mpysp	i0a,b1a,prda
||		mpysp	i0b,b1b,prdb

		mpysp	i2a,b2a,prda
||		mpysp	i2b,b2b,prdb
||		addsp	prda,s0a,s0a
||		addsp	prdb,s0b,s0b

		mpysp	i3a,b2a,prda
||		mpysp	i3b,b2b,prdb
||		addsp	prda,s1a,s1a
||		addsp	prdb,s1b,s1b

		mpysp	i0a,b2a,prda
||		mpysp	i0b,b2b,prdb
||		addsp	prda,s2a,s2a
||		addsp	prdb,s2b,s2b

		mpysp	i1a,b2a,prda
||		mpysp	i1b,b2b,prdb
||		addsp	prda,s3a,s3a
||		addsp	prdb,s3b,s3b

		addsp	prda,s0a,s0a
||		addsp	prdb,s0b,s0b

		addsp	prda,s1a,s1a
||		addsp	prdb,s1b,s1b

		addsp	prda,s2a,s2a
||		addsp	prdb,s2b,s2b

		addsp	prda,s3a,s3a
||		addsp	prdb,s3b,s3b

		stw		s0a,*optra++[4]
||		stw		s0b,*optrb++[4]

		stw		s1a,*optra++[4]
||		stw		s1b,*optrb++[4]

		stw		s2a,*optra++[4]
||		stw		s2b,*optrb++[4]

		stw		s3a,*optra++[4]
||		stw		s3b,*optrb++[4]

LOOP_END
			; save filter states

		stw		i2a,*sptr[0]			
		stw		i3a,*sptr[1]
		stw		i2b,*sptr[2]
		stw		i3b,*sptr[3]
									
PREPARE_RETURN:
		
		ldw.d2		*++b15[1],b3					; restore b3, free local variable memory
		ldw.d2		*++b15[1],a15
			
		ldw.d2		*++b15[1],b14
	||	mvc			csr,b0

		ldw.d2		*++b15[1],a14
		ldw.d2		*++b15[1],b13
		ldw.d2		*++b15[1],a13
		ldw.d2		*++b15[1],b12

		ldw.d2		*++b15[1],a12
	||	set 		b0,0,0,b0

		ldw.d2		*++b15[1],b11
		ldw.d2		*++b15[1],a11		
											
		b.s2		b3
	||	ldw.d2		*++b15[1],b10
		ldw.d2		*++b15[1],a10
		mvc			b0,csr
		nop			3
             