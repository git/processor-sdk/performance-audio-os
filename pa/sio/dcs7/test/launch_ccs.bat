::@ECHO OFF
rem:   launch_ccs.bat
rem:   batch file to launch CCS to build the DCS test app.

rem:  OS_SDK_ROOT is where the open source version of PA resides.
rem:  This directory contains pa, da, etc.
rem:  Use double backslashes so that the path can be used by sed.

rem:  TI_DIR is where different tools reside.
 
rem:  CG_TOOLS is where c6xx compiler reside.


if "%COMPUTERNAME%"=="CPU-158" (
    echo "Launching Code Composer using settings for CPU-158."
    set PAF_ROOT=E:\\K2G_paf\\code\\work\\alpha_cmd\\os_paf_dev_dcs6
    set TI_DIR=C:\\ti
    set DEVICE=66AK2G02
    set EDMA_INSTALL_DIR=${TI_DIR}\\edma3_lld_02_12_01_22
    set PDK_ROOT_PATH=C:\\ti\\pdk_k2g_1_0_1\\packages
    c:
    cd C:\\ti\\ccsv6\\eclipse
    start ccstudio.exe -data E:\\K2G_paf\\code\\work\\alpha_cmd\\workspace_v6_0_dcs7
    goto end
)

if "%COMPUTERNAME%"=="LTA0322553" (
    echo "Launching Code Composer using settings for LTA0322553."
    set PAF_ROOT=C:\\ti\\processor_sdk_audio_1_02_01_02\\pasrc\\paf
    set TI_DIR=C:\\ti
    set DEVICE=66AK2G02
    set EDMA_INSTALL_DIR=${TI_DIR}\\edma3_lld_2_12_01_23
    set PDK_ROOT_PATH=C:\\ti\\pdk_k2g_1_0_1\\packages
    c:
    pushd C:\\ti\\ccsv6\\eclipse
    start ccstudio.exe -data C:\\ti\\processor_sdk_audio_1_02_01_02\\pasrc\\workspace_v6_1_dcs7
    popd
    goto end
)

if "%COMPUTERNAME%"=="DTA0322553B" (
    echo "Launching Code Composer using settings for DTA0322553B."
    set PAF_ROOT=C:\\ti\\processor_sdk_audio_1_02_01_02\\pasrc\\paf
    set TI_DIR=C:\\ti
    set DEVICE=66AK2G02
    set EDMA_INSTALL_DIR=${TI_DIR}\\edma3_lld_2_12_04_28
    set PDK_ROOT_PATH=C:\\ti\\pdk_k2g_1_0_6\\packages
    c:
    pushd C:\\ti\\ccsv6\\eclipse
    start ccstudio.exe -data C:\\ti\\processor_sdk_audio_1_02_01_02\\pasrc\\workspace_v6_1_dcs7
    popd
    goto end
)

:end
