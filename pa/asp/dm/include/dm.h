/******************************************************************************
 * Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions are met:
 *       * Redistributions of source code must retain the above copyright
 *         notice, this list of conditions and the following disclaimer.
 *       * Redistributions in binary form must reproduce the above copyright
 *         notice, this list of conditions and the following disclaimer in the
 *         documentation and/or other materials provided with the distribution.
 *       * Neither the name of Texas Instruments Incorporated nor the
 *         names of its contributors may be used to endorse or promote products
 *         derived from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 *   THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

//
//
// Downmix algorithm interface declarations
//
//
//
/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the Downmix algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the DM
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef DM_
#define DM_

#include <alg.h>
#include <ti/xdais/ialg.h>
#include <paf_alg.h>
#include <idm.h>

#include "paftyp.h"

/*
 *  ======== DM_Handle ========
 *  Downmix algorithm instance handle
 */
typedef struct IDM_Obj *DM_Handle;

/*
 *  ======== DM_Params ========
 *  Downmix algorithm instance creation parameters
 */
typedef struct IDM_Params DM_Params;

/*
 *  ======== DM_PARAMS ========
 *  Default instance parameters
 */
#define DM_PARAMS IDM_PARAMS

/*
 *  ======== DM_Status ========
 *  Status structure for getting DM instance attributes
 */
typedef volatile struct IDM_Status DM_Status;

/*
 *  ======== DM_Cmd ========
 *  This typedef defines the control commands DM objects
 */
typedef IDM_Cmd DM_Cmd;

/*
 * ===== control method commands =====
 */
#define DM_NULL IDM_NULL
#define DM_GETSTATUSADDRESS1 IDM_GETSTATUSADDRESS1
#define DM_GETSTATUSADDRESS2 IDM_GETSTATUSADDRESS2
#define DM_GETSTATUS IDM_GETSTATUS
#define DM_SETSTATUS IDM_SETSTATUS

/*
 *  ======== DM_create ========
 *  Create an instance of a DM object.
 */
static inline DM_Handle DM_create(const IDM_Fxns *fxns, const DM_Params *prms) 
{
    return ((DM_Handle) PAF_ALG_create((IALG_Fxns *) fxns, NULL,
            (IALG_Params *) prms, NULL, NULL));
}

/*
 *  ======== DM_delete ========
 *  Delete a DM instance object
 */
static inline Void DM_delete(DM_Handle handle) 
{
    PAF_ALG_delete((ALG_Handle) handle);
}

/*
 *  ======== DM_apply ========
 */
extern Int DM_apply(DM_Handle, PAF_AudioFrame *);

/*
 *  ======== DM_reset ========
 */
extern Int DM_reset(DM_Handle, PAF_AudioFrame *);

/*
 *  ======== DM_exit ========
 *  Module finalization
 */
extern Void DM_exit(Void);

/*
 *  ======== DM_init ========
 *  Module initialization
 */
extern Void DM_init(Void);

#endif  /* DM_ */
