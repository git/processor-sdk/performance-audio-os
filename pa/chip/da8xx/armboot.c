
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DA8xx ARM Boot library function
//
//
//

#define WA
#if defined(WA)
#include <primus.h>
#else /* WA */

#include <ti/psp/cslr/cslr_dmaxpdsp.h>
#include <ti/psp/cslr/cslr_psc_DA830.h>
#include <ti/psp/cslr/soc_DA830.h>

#define CSL_DMAXPDSP0_IRAM_BASEADDR             (0x01C38000u)

#define CSL_DMAXPDSP_CONTROL_CYCLECOUNT_MASK (0x00000008u)
#define CSL_DMAXPDSP_CONTROL_CYCLECOUNT_SHIFT (0x00000003u)
#define CSL_DMAXPDSP_CONTROL_CYCLECOUNT_RESETVAL (0x00000000u)
#define CSL_DMAXPDSP_CONTROL_CYCLECOUNT_DISABLE (0x00000000u)
#define CSL_DMAXPDSP_CONTROL_CYCLECOUNT_ENABLE (0x00000001u)

#endif /* WA */

#include <da830lib.h>

const unsigned int armBootDmaxCodeConst[] = {
	0x24000080,												// Load ARM vector table address to r0
	0x24ffffc0,
	0x24000081,												// Load ARM code address to r1
	0x240000c1,
	0xf500e182,												// Read ARM code from *r1 to r2,...
	0xe500e082,												// Write ARM code from r2,... to *r0
	0x79000000,												// Self loop
};

const unsigned int armBootArmCodeConst[] = {
	0xEA000007,												// vt0: B boot
	0xEA000006,												// vt1: B boot
	0xEA000005,												// vt2: B boot
	0xEA000004,												// vt3: B boot
	0xEA000003,												// vt4: B boot
	0xEA000002,												// vt5: B boot
	0xEA000001,												// vt6: B boot
	0xEA000000,												// vt7: B boot
	// jump:, DATA:
	0xC1080000,
	// boot:
	0xE51FF00C,												// LDR PC, jump
	0xEAFFFFFE,												// loop: B loop
};

void armBoot (unsigned int address)
{
	CSL_DmaxpdspRegs *dmax = (CSL_DmaxpdspRegs *) CSL_DMAXPDSP_0_REGS;
	CSL_PscRegs *psc0 = (CSL_PscRegs *) CSL_PSC_0_REGS;
	unsigned int i, *p = (unsigned int *) CSL_DMAXPDSP0_IRAM_BASEADDR;
	unsigned int *armCode = (unsigned int *) armBootArmCodeConst;
	unsigned int *dmaxCode = (unsigned int *) armBootDmaxCodeConst;

	// Update ARM entry point address
	armCode[8] = address;

	// Tell location of armCode to dmax code
	dmaxCode[2] |= ((int) armCode & 0xffff) << 8;
	dmaxCode[3] |= ((int) armCode & 0xffff0000) >> 8;

	// Flush L2
	L2WB = 1;
	while ((L2WB & 1) != 0);

	// Enable dMAX
	lpscEnable (PSC0, PD0, LPSC_PRUSS);
	// NOTE: ARM RAM comes up as enabled

	// Reset dMAX
	dmax->CONTROL = CSL_DMAXPDSP_CONTROL_RESETVAL;

	// Copy dMAX code to its instruction RAM
	for (i = 0; i < sizeof (armBootDmaxCodeConst) / sizeof (int); i++)
		*p++ = dmaxCode[i];

	// Enable dMAX, let it execute the code we just copied
	CSL_FINS (dmax->CONTROL, DMAXPDSP_CONTROL_CYCLECOUNT, CSL_DMAXPDSP_CONTROL_CYCLECOUNT_ENABLE);
	CSL_FINS (dmax->CONTROL, DMAXPDSP_CONTROL_ENABLE, CSL_DMAXPDSP_CONTROL_ENABLE_ENABLE);

	// Wait for dMAX to finish
	while (dmax->STATUS != (sizeof (armBootDmaxCodeConst) / sizeof (int)) - 1);

	// Reset dMAX
	dmax->CONTROL = CSL_DMAXPDSP_CONTROL_RESETVAL;

	// Enable ARM
	CSL_BOOTCFG_0_REGS->HOST0CFG = 0x00000001;
	lpscEnable (PSC0, PD0, LPSC_ARM);

	// De-assert ARM local reset
	CSL_FINS (psc0->MDCTL[LPSC_ARM], PSC_MDCTL_LRSTZ, 0x01);
}
