
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  FEB Module - implements all functions and defines all constant
 *  structures common to all FIL-ASP Example Demonstration algorithm
 *  implementations.
 */
#include <std.h>
#include <alg.h>

#include "feb.h"

/*
 *  ======== FEB_apply ========
 */
Int FEB_apply(FEB_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->apply(handle, pAudioFrame);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

/*
 *  ======== FEB_exit ========
 *  Module finalization
 */
Void FEB_exit()
{
}

/*
 *  ======== FEB_reset ========
 */
Int FEB_reset(FEB_Handle handle, PAF_AudioFrame *pAudioFrame)
{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->reset(handle, pAudioFrame);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

/*
 *  ======== FEB_init ========
 *  Module initialization
 */
Void FEB_init()
{
}
