/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/


/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

#include <stdio.h>
#include <string.h>

/* TI-RTOS Header files */
#include <ti/drv/i2c/I2C.h>
#include <ti/drv/i2c/src/v0/I2C_v0.h>
#include <ti/drv/i2c/soc/I2C_soc.h>
#include <ti/drv/i2c/test/eeprom_read/src/I2C_board.h>

/**********************************************************************
 ********************** I2C Master ************************************
 **********************************************************************/
// Comment/Uncomment to enable or disable profiling
//#define PROFILER
#ifdef PROFILER
#include "util_profiling.h"
#include "pa_i13_evmk2g_io_a.h"
#include "acptype.h"
#define REGRESS_TEST
#endif

// Comment/Uncomment to enable or disable printf messages,
// These when enabled will effect the regress test behaviour.
//#define REGRESS_TEST
#ifdef REGRESS_TEST
#define printf(fmt, ...)
#endif

// Comment/Uncomment to enable or disable SLC (serial link control) test
//#define SENDSLC


// Slave address of K2G to communicate.
#define K2G_2_SLAVE_ADDR 0x11


// sizes in Bytes
typedef enum testapp_size_e
{
    ALPHA_CMD_MAX_SIZE     = 256,
    ALPHA_CMD_RES_MAX_SIZE = 256

} testapp_size;

// Counts
typedef enum testapp_count_e
{
#ifdef PROFILER
    TESTAPP_ALPHA_MAX_CNT  = 8,
#else
    TESTAPP_ALPHA_MAX_CNT  = 22,
#endif
    TESTAPP_SLC_MAX_CNT    = 3

} testapp_count;


#ifdef SENDSLC
Uint16 SlcSequenceSend[TESTAPP_SLC_MAX_CNT][ALPHA_CMD_MAX_SIZE] =
{
        {0x8001},                                  // 0 reset query
        {0x8101},                                  // 1 ready query
        {0x8201}                                   // 2 not implemented
};
#endif

Uint16 alphaSequenceSend[TESTAPP_ALPHA_MAX_CNT][ALPHA_CMD_MAX_SIZE] =
{
#ifdef PROFILER
        {readENCListeningFormat},
        {readDECSourceProgram},
        {readDECProgramFormat},
        {writeVOLControlMasterN(0)},
        {writeSYSSpeakerMainLarge2},
        {writeENCChannelMapTo16(0,8,-3,-3,1,9,-3,-3,-3,-3,2,10,3,11,-3,-3)},
        {writeSYSChannelConfigurationRequestSurround4_1},
        {readPCEControl}
#else
        {0x0002, 0xC625, 0x6020},                                  // 0
        {0x0002, 0xC024, 0x0A49},                                  // 1
        {0x0002, 0xC805, 0x0D00},                                  // 2
        {0x0001, 0xC100},                                          // 3
        {0x0001, 0xDEAD},                                          // 4
        {0x0002, 0xC224, 0x0400},                                  // 5
        {0x0002, 0xCA24, 0x2E01},                                  // 6
        {0x0002, 0xC324, 0x0030},                                  // 7
        {0x0003, 0xCB20, 0x0024, 0x0000},                          // 8
        {0x0002, 0xC424, 0x0040},                                  // 9
        {0x0004, 0xCC24, 0x0038, 0x0108, 0x0000},                  // 10
        {0x0006, 0xCE34, 0x1808, 0x0000, 0x0000, 0x0000, 0x0000},  // 11
        {0x0002, 0xC509, 0x0400},                                  // 12
        {0x0002, 0xCD09, 0x0400},                                  // 13
        {0x0002, 0xC425, 0x0088},                                  // 14
        {0x0002, 0xC224, 0x0A00},                                  // 15
        {0x0002, 0xC424, 0x00A0},                                  // 16
        {0x0003, 0xCB26, 0x0010, 0x0000},                          // 17
        {0x0002, 0xCA20, 0x0732},                                  // 18
        {0x000A, 0xCE25, 0x6010, 0x0800, 0xFDFD, 0x0901, 0xFDFD,
         0xFDFD, 0x0A02, 0x0B03, 0xFDFD},                          // 19
        {0x0004, 0xCC20, 0x0020, 0x010C, 0x0000},                  // 20
        {0x0096, 0xc22e, 0x0400, 0xc22e, 0x0600, 0xc22e, 0x0700,
         0xc22e, 0x0800, 0xc22e, 0x0900, 0xc22e, 0x0a00, 0xc22e,
         0x0b00, 0xc22e, 0x0c00, 0xc22e, 0x0d00, 0xc22e, 0x0e00,
         0xc22e, 0x0f00, 0xc22e, 0x1000, 0xc22e, 0x1100, 0xc22e,
         0x1300, 0xc22e, 0x1400, 0xc32e, 0x0016, 0xc32e, 0x0018,
         0xc32e, 0x001a, 0xc32e, 0x001a, 0xc32e, 0x001c, 0xc32e,
         0x001e, 0xc32e, 0x001e, 0xc32e, 0x0020, 0xc32e, 0x0022,
         0xc32e, 0x0022, 0xc32e, 0x0024, 0xc32e, 0x0026, 0xc32e,
         0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a,
         0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024,
         0xc32e, 0x0026, 0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e,
         0x002a, 0xc32e, 0x002a, 0xc32e, 0x002c, 0xc32e, 0x002e,
         0xc32e, 0x002e, 0x0024, 0xc32e, 0x0026, 0xc32e, 0x0026,
         0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a, 0xc32e,
         0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024, 0xc32e,
         0x0026, 0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a,
         0xc32e, 0x002a, 0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e,
         0x002e, 0x0024, 0xc32e, 0x0026, 0xc32e, 0x0026, 0xc32e,
         0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a, 0xc32e, 0x002c,
         0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024, 0xc32e, 0x0026,
         0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e,
         0x002a, 0xc32e, 0x002c, 0xc32e}                            // 21
#endif
};

Uint16 alphaSequenceReceive[TESTAPP_ALPHA_MAX_CNT][ALPHA_CMD_RES_MAX_SIZE] =
{
#ifdef PROFILER
        {0x0004, 0xCC25, 0x0088, 0x0000, 0x0000},
        {0x0002, 0xCA24, 0x0A00},
        {0x0004, 0xCC24, 0x00A0, 0x0000, 0x0000},
        {0x0000},
        {0x0000},
        {0x0000},
        {0x0006, 0xCE20, 0x2008, 0x010C, 0x0200,
         0x007F, 0x0000},
        {0x004A, 0xc22e, 0x0400, 0xc22e, 0x0600, 0xc22e, 0x0700,
         0xc22e, 0x0800, 0xc22e, 0x0900, 0xc22e, 0x0a00, 0xc22e,
         0x0b00, 0xc22e, 0x0c00, 0xc22e, 0x0d00, 0xc22e, 0x0e00,
         0xc22e, 0x0f00, 0xc22e, 0x1000, 0xc22e, 0x1100, 0xc22e,
         0x1300, 0xc22e, 0x1400, 0xc32e, 0x0016, 0xc32e, 0x0018,
         0xc32e, 0x001a, 0xc32e, 0x001a, 0xc32e, 0x001c, 0xc32e,
         0x001e, 0xc32e, 0x001e, 0xc32e, 0x0020, 0xc32e, 0x0022,
         0xc32e, 0x0022, 0xc32e, 0x0024, 0xc32e, 0x0026, 0xc32e,
         0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a,
         0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0xc32e,
         0x0030, 0xc32e, 0x0032, 0xc32e, 0x0034},  // readPCEControl
#else
        {0x0012, 0xCE25, 0x6020, 0xFDFD, 0xFDFD, 0xFDFD,
         0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD,
         0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD,
         0xFDFD},                                   // 0
        {0x0002, 0x0000, 0x0000},                   // 1
        {0x0002, 0xD300, 0x0002},                   // 2
        {0x0001, 0xC100},                           // 3
        {0x0001, 0xDEAD},                           // 4
        {0x0002, 0xCA24, 0x0401},                   // 5
        {0x0000},                                   // 6
        {0x0003, 0xCB24, 0x0030, 0x0000},           // 7
        {0x0000},                                   // 8
        {0x0004, 0xCC24, 0x0040, 0x0000, 0x0000},   // 9
        {0x0000},                                   // 10
        {0x0000},                                   // 11
        {0x0002, 0xCD09, 0x0400},                   // 12
        {0x0000},                                   // 13
        {0x0004, 0xCC25, 0x0088, 0x0000, 0x0000},   // 14
        {0x0002, 0xCA24, 0x0A00},                   // 15
        {0x0004, 0xCC24, 0x00A0, 0x0000, 0x0000},   // 16
        {0x0000},                                   // 17
        {0x0000},                                   // 18
        {0x0000},                                   // 19
        {0x0006, 0xCE20, 0x2008, 0x010C, 0x0200,
         0x007F, 0x0000},                           // 20
        {0x0096, 0xc22e, 0x0400, 0xc22e, 0x0600, 0xc22e, 0x0700,
         0xc22e, 0x0800, 0xc22e, 0x0900, 0xc22e, 0x0a00, 0xc22e,
         0x0b00, 0xc22e, 0x0c00, 0xc22e, 0x0d00, 0xc22e, 0x0e00,
         0xc22e, 0x0f00, 0xc22e, 0x1000, 0xc22e, 0x1100, 0xc22e,
         0x1300, 0xc22e, 0x1400, 0xc32e, 0x0016, 0xc32e, 0x0018,
         0xc32e, 0x001a, 0xc32e, 0x001a, 0xc32e, 0x001c, 0xc32e,
         0x001e, 0xc32e, 0x001e, 0xc32e, 0x0020, 0xc32e, 0x0022,
         0xc32e, 0x0022, 0xc32e, 0x0024, 0xc32e, 0x0026, 0xc32e,
         0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a,
         0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024,
         0xc32e, 0x0026, 0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e,
         0x002a, 0xc32e, 0x002a, 0xc32e, 0x002c, 0xc32e, 0x002e,
         0xc32e, 0x002e, 0x0024, 0xc32e, 0x0026, 0xc32e, 0x0026,
         0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a, 0xc32e,
         0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024, 0xc32e,
         0x0026, 0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a,
         0xc32e, 0x002a, 0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e,
         0x002e, 0x0024, 0xc32e, 0x0026, 0xc32e, 0x0026, 0xc32e,
         0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a, 0xc32e, 0x002c,
         0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024, 0xc32e, 0x0026,
         0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e,
         0x002a, 0xc32e, 0x002c, 0xc32e}            // 21
#endif
};

Uint8 txBuf[512] = {0x00};
Uint8 rxBuf[512] = {0x00};

#ifdef PROFILER
extern Int alpha_length(const ACP_Unit *from);
#endif


/*
 *  ======== Board_initI2C ========
 */
void Board_initI2C(void)
{
    Board_initCfg boardCfg;
    I2C_HwAttrs uart_cfg;

    /* Get the default I2C init configurations */
    I2C_socGetInitCfg(I2C_EEPROM_INSTANCE, &uart_cfg);

    /* Set the default I2C init configurations */
    I2C_socSetInitCfg(I2C_EEPROM_INSTANCE, &uart_cfg);

    boardCfg = BOARD_INIT_PINMUX_CONFIG |
        BOARD_INIT_MODULE_CLOCK |
        BOARD_INIT_UART_STDIO;

    Board_init(boardCfg);
}

/*
 *  ======== test function ========
 */
void i2c_test(UArg arg0, UArg arg1)
{
    I2C_Params i2cParams;
    I2C_Handle handle = NULL;
    I2C_Transaction i2cTransaction;
    bool status;
    uint32_t alphaCmdNum = 0;
    Uint16 alphaResCnt = 0;
    Uint16 alphaSendCnt = 0;
    Uint32 i, j;
#ifdef PROFILER
    tPrfl profiler;
    Int prlf_flag = 0;
#endif

    I2C_init();

    I2C_Params_init(&i2cParams);

    i2cParams.isSlave = I2C_MASTER;  // master
    i2cParams.bitRate = I2C_100kHz;

    handle = I2C_open(I2C_EEPROM_INSTANCE, &i2cParams);

#ifdef PROFILER
    while (alphaCmdNum < TESTAPP_ALPHA_MAX_CNT)
    {
        //alphaSendCnt = alpha_length(alphaSequenceSend[alphaCmdNum]);
        alphaSendCnt = alphaCmdNum < 7 ?  // todo: test
						alpha_length(alphaSequenceSend[alphaCmdNum]) :
						0x004A; // special case for readPCEControl
#else
    #ifdef SENDSLC
    while (alphaCmdNum < TESTAPP_SLC_MAX_CNT)
    {
        alphaSendCnt = 1;
    #else
    while (alphaCmdNum < TESTAPP_ALPHA_MAX_CNT)
    {
        alphaSendCnt = alphaSequenceSend[alphaCmdNum][0];
    #endif
#endif

        printf("\n ****** New Command Send ****** \n");

        if ((alphaSendCnt*2) < 254)
        {
        	txBuf[0] = alphaSendCnt*2;
        	txBuf[1] = 0;
        }
        else
        {
        	Uint32 bytes = alphaSendCnt*2 + 1;
        	txBuf[0] = bytes & 0x00FF;  // todo
        	txBuf[1] = (bytes & 0xFF00) >> 8;  // todo
        }
        /* Send Count */
        i2cTransaction.slaveAddress = K2G_2_SLAVE_ADDR;
        i2cTransaction.writeBuf = (uint8_t *)&txBuf[0];
        i2cTransaction.writeCount = 2;
        i2cTransaction.readBuf = (uint8_t *)&rxBuf[0];
        i2cTransaction.readCount = 0;
        status = I2C_transfer(handle, &i2cTransaction);

#ifdef PROFILER
        Prfl_Init(&profiler);
        Prfl_Start(&profiler);
#endif
        if (FALSE == status)
        {
            printf("\n Data Transfer failed. \n");
        }

        /* Send DATA  */
        j = 0;
#ifdef PROFILER
        for (i = 0; i < (alphaSendCnt + 1); i++)
        {
            txBuf[j] = alphaSequenceSend[alphaCmdNum][i] & (0x00FF); // low byte
            j++;
            txBuf[j] = (alphaSequenceSend[alphaCmdNum][i] & (0xFF00)) >> 8;  // high byte
            j++;
        }
#else
    #ifdef SENDSLC
        for (i = 0; i < (alphaSendCnt + 1); i++)
        {
            txBuf[j] = SlcSequenceSend[alphaCmdNum][i] & (0x00FF); // low byte
            j++;
            txBuf[j] = (SlcSequenceSend[alphaCmdNum][i] & (0xFF00)) >> 8;  // high byte
            j++;
        }
    #else
        for (i = 1; i < (alphaSendCnt + 1); i++)
        {
            txBuf[j] = alphaSequenceSend[alphaCmdNum][i] & (0x00FF); // low byte
            j++;
            txBuf[j] = (alphaSequenceSend[alphaCmdNum][i] & (0xFF00)) >> 8;  // high byte
            j++;
        }
    #endif
#endif


        i2cTransaction.slaveAddress = K2G_2_SLAVE_ADDR;
        i2cTransaction.writeBuf = (uint8_t *)&txBuf[0];
        i2cTransaction.writeCount = alphaSendCnt*2;
        i2cTransaction.readBuf = (uint8_t *)&rxBuf[0];
        i2cTransaction.readCount = 0;
#ifdef PROFILER
            Prfl_Stop(&profiler);
#endif
        status = I2C_transfer(handle, &i2cTransaction);
#ifdef PROFILER
            Prfl_Start(&profiler);
#endif
        if (FALSE == status)
        {
            printf("\n Data Transfer failed. \n");
        }

#ifdef SENDSLC
        Task_sleep(1000);
#endif
        Task_sleep(100);
        // Get response from Slave
        // get res count
        i2cTransaction.slaveAddress = K2G_2_SLAVE_ADDR;
        i2cTransaction.writeBuf = (uint8_t *)&txBuf[0];
        i2cTransaction.writeCount = 0;
        i2cTransaction.readBuf = (uint8_t *)&rxBuf[0];
        i2cTransaction.readCount = 2;
#ifdef PROFILER
            Prfl_Stop(&profiler);
#endif
        status = I2C_transfer(handle, &i2cTransaction);
#ifdef PROFILER
            Prfl_Start(&profiler);
#endif
        alphaResCnt = rxBuf[0] | (rxBuf[1] << 8);  // todo : verify for >256

        printf("receive res cnt %x \n", alphaResCnt);

        // Get paylod
        i2cTransaction.slaveAddress = K2G_2_SLAVE_ADDR;
        i2cTransaction.writeBuf = (uint8_t *)&txBuf[0];
        i2cTransaction.writeCount = 0;
        i2cTransaction.readBuf = (uint8_t *)&rxBuf[0];
        i2cTransaction.readCount = alphaResCnt < 254 ?
        							alphaResCnt : (alphaResCnt - 1);
#ifdef PROFILER
            Prfl_Stop(&profiler);
#endif
        status = I2C_transfer(handle, &i2cTransaction);

#ifdef PROFILER
        {
            // write porfiling info in file
            FILE *pStdout_profile =
                          fopen("..\\..\\test_vectors\\output\\"
                                "dcs7_profile_i2c_master.txt", "a");
            fprintf(pStdout_profile,
                    "Total Process Cycles %llu, "
                     "Peak Process Cycles %llu "
                     "Process Count %u",
                     profiler.total_process_cycles,
                     profiler.peak_process_cycles,
                     profiler.process_count);
            fprintf(pStdout_profile, "\n****************\n");
            fclose(pStdout_profile);
            // Clear profiling data
            Prfl_Init(&profiler);
            prlf_flag = 0;
        }
#endif

        for (i = 0; i < alphaResCnt/2; ++i)
        {
            printf("receive res data %x \n", *(((Uint16*)rxBuf)+i));
        }

        // Verify received data
        if (memcmp(rxBuf, &alphaSequenceReceive[alphaCmdNum][1], alphaResCnt))
        {
            printf("Test FAILED: cmd index %d\n", alphaCmdNum);
#ifdef REGRESS_TEST
            while(1);  // Test failed
#endif
        }
        else
        {
            printf("Test PASSED \n");
        }

        alphaCmdNum++;

    }
    I2C_close(handle);

    printf("I2C master test Done. \n");

    while (1) {

    }
}

/*
 *  ======== main ========
 */
int main(void)
{
    Board_initI2C();

    /* Start BIOS */
    BIOS_start();
    return (0);
}
