
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#include <afp6.h>

#include <acp_mds.h>
#include <acp_mds_priv.h>
#include <logp.h>

#define ENABLE_ALPHA_ERROR_TRACE
#ifdef ENABLE_ALPHA_ERROR_TRACE
 #define AE_TRACE(a) LOG_printf a
#else
 #define AE_TRACE(a)
#endif


extern AFP6_Obj AFP6_OBJ;

void AFP6_log(
    AFP6_Handle handle,
    void *log,
    char *msg)
{
    if(log)
#ifdef BIOS6_NONLEGACY
        Log_print0((Uint32)log,msg);
#else
        LOG_printf(log,msg);
#endif
}

void* AFP6_memAlloc(
    AFP6_Handle handle,
    void *memSeg,
    Uint32 size,
    Uint32 align)
{
#ifdef BIOS6_NONLEGACY
    return (void*)Memory_alloc((IHeap_Handle)*(Int32*)memSeg,size,align,NULL);
#else
    return MEM_alloc(*(Int32*)memSeg,size,align);
#endif
}

void AFP6_process(
    AFP6_Handle handle)
{
    Int rxIdx,txIdx;
    Int encap,len,res_len,error,cnt,txStrip,i,rxOffset,txOffset;

    /* Process Alpha commands */
    for(;;){
        /* Set read and write buffer indices */
        rxIdx=2;
        txIdx=3;
        txOffset=0;
        encap=0;
        /* Read the payload */
        if(handle->dcs6->fxns->read(handle->dcs6,handle->rxBuf+rxIdx,
            handle->params->rxBufSize-(rxIdx<<1))){
            handle->fxns->log(handle,handle->logObj,
                "AFP6: Error in reading message");
            /* Write the response */
            handle->txBuf[txIdx++]=0xdead;
            AE_TRACE((&trace, "AFP6_process.%d:  0xDEAD: Error in reading alpha message.", __LINE__));
            handle->txBuf[3-txOffset-1]=txIdx-3+txOffset;
            handle->dcs6->fxns->write(handle->dcs6,&handle->txBuf[3-txOffset-1],
                handle->params->txBufSize-(3-txOffset-1<<1));
            continue;
        }
        cnt=handle->rxBuf[rxIdx++];
        /* Process the C-record */
        while(cnt) {
            /* Get the length of alpha code */
            len=handle->acp->fxns->length(handle->acp,&handle->rxBuf[rxIdx]);
            /* Get the response length */
            res_len=handle->acp->fxns->htgnel(handle->acp,&handle->rxBuf[rxIdx]);
            if(res_len>(handle->params->txBufSize-2>>1)-txIdx){
                handle->txBuf[txIdx++]=0xdead;
                AE_TRACE((&trace, "AFP6_process.%d:  0xDEAD: Response too big.", __LINE__));
                handle->fxns->log(handle,handle->logObj,
                    "AFP6: Response too big");
                break;
            }
            /* Add encapsulation to alpha command -- ACP limitation */
            if((handle->rxBuf[rxIdx]&0xff00)==0xc900||
                handle->rxBuf[rxIdx]==0xcd01){
                Int lene,nele;
                Int lenp,nelp;
                Int k;
                if((handle->rxBuf[rxIdx]&0xff00)==0xc900)
                    k=1;
                else
                    k=2;
                lene=len-k;
                nele=0;
                while(lene){
                    lenp=handle->acp->fxns->length(handle->acp,&handle->rxBuf[rxIdx+k]);
                    nelp=handle->acp->fxns->htgnel(handle->acp,&handle->rxBuf[rxIdx+k]);
                    k+=lenp;
                    lene-=lenp;
                    nele+=nelp;
                }
                if(nele<256)
                    res_len=nele+1;
                else
                    res_len=nele+2;
                rxOffset=0;
                if(res_len>(handle->params->txBufSize-4>>1)-txIdx){
                    handle->txBuf[txIdx++]=0xc901;
                    handle->txBuf[txIdx++]=0xdead;
                    AE_TRACE((&trace, "AFP6_process.%d:  0xDEAD: Response too big.", __LINE__));
                    handle->fxns->log(handle,handle->logObj,
                        "AFP6: Response too big");
                    break;
                }
            }
            else if(len<256){
                handle->rxBuf[--rxIdx]=0xc900+len;
                rxOffset=1;
            }
            else{
                handle->rxBuf[--rxIdx]=len;
                handle->rxBuf[--rxIdx]=0xcd01;
                rxOffset=2;
            }
            /* Process alpha command */
            error=handle->acp->fxns->sequence(handle->acp,
                &handle->rxBuf[rxIdx],&handle->txBuf[txIdx]);
            if(error){
                handle->fxns->log(handle,handle->logObj,
                    "AFP6: Sequence error");
                AE_TRACE((&trace, "AFP6_process.%d:  0xDEAD: Sequence error.", __LINE__));
                if(res_len||encap){
                    handle->txBuf[txIdx++]=0xdead;
                }
                cnt-=len;
                rxIdx+=len+rxOffset;
                continue;
            }
            else if(rxOffset){
                /* Delete encapsulation -- ACP limitation */
                if((handle->txBuf[txIdx]&0xff00)==0xc900)
                    txStrip=1;
                else if(handle->txBuf[txIdx]==0xcd01)
                    txStrip=2;
                for(i=0;i<res_len;++i)
                    handle->txBuf[txIdx+i]=handle->txBuf[txIdx+i+txStrip];
            }
            /* Update the count, rxIdx and txIdx */
            cnt-=len;
            rxIdx+=len+rxOffset;
            txIdx+=res_len;
        }
        /* Write the response */
        handle->txBuf[3-txOffset-1]=txIdx-3+txOffset;
        handle->dcs6->fxns->write(handle->dcs6,&handle->txBuf[3-txOffset-1],
            handle->params->txBufSize-(3-txOffset-1<<1));
    }
}

void AFP6_task(
    AFP6_Handle handle)
{
    if(!handle)
        handle=&AFP6_OBJ;

    /* Create an instance of ACP Algorithm */
    handle->fxns->acpInit();
    if(!(handle->acp=ACP_create(&ACP_MDS_IACP,handle->acpParams)))
        handle->fxns->log(handle,handle->logObj,
            "AFP6: ACP instance creation failed");
    else{
        IALG_Status *pStatus;
        extern struct{
           int size;
        }pafIdentification;
        ((ALG_Handle)handle->acp)->fxns->algControl((IALG_Handle)(handle->acp),
            ACP_GETSTATUSADDRESS1,(IALG_Status*)&pStatus);
        handle->acp->fxns->attach(handle->acp,ACP_SERIES_STD,STD_BETA_UART,
            pStatus);
        handle->acp->fxns->attach(handle->acp,ACP_SERIES_STD,STD_BETA_BETATABLE, 
            (IALG_Status*)((ACP_MDS_Obj*)handle->acp)->config.betaTable[ACP_SERIES_STD]);
        handle->acp->fxns->attach(handle->acp,ACP_SERIES_STD,STD_BETA_PHITABLE, 
            (IALG_Status*)((ACP_MDS_Obj*)handle->acp)->config.phiTable[ACP_SERIES_STD]);
        handle->acp->fxns->attach(handle->acp,ACP_SERIES_STD,STD_BETA_SIGMATABLE, 
            (IALG_Status*)((ACP_MDS_Obj*)handle->acp)->config.sigmaTable[ACP_SERIES_STD]);
        handle->acp->fxns->attach(handle->acp,ACP_SERIES_STD,STD_BETA_IDENTITY,
            (IALG_Status *) & pafIdentification);
        handle->fxns->log(handle,handle->logObj,"AFP6: ACP initialized");
        handle->dmax=dMAX_HANDLE;
        if(!(handle->dcs6=handle->dcs6Fxns->open(handle->params->dcs6Params,
            handle->dcs6Config,(DCS6_Fxns_Ptr)handle->dcs6Fxns,
            handle->dmax,handle->acp)))
            handle->fxns->log(handle,handle->logObj,
                "AFP6: DCS6 creation failed");
        else{
            handle->fxns->log(handle,handle->logObj,"AFP6: DCS6 initialized");
            handle->rxBuf=(Uint16*)handle->fxns->memAlloc(handle,
                handle->pBufSeg,handle->params->rxBufSize,4);
            handle->txBuf=(Uint16*)handle->fxns->memAlloc(handle,
                handle->pBufSeg,handle->params->txBufSize,4);
            if(!handle->rxBuf||!handle->txBuf)
                handle->fxns->log(handle,handle->logObj,
                    "AFP6: Buffer allocation failed");
            else{
                handle->fxns->log(handle,handle->logObj,"AFP6: AFP6 initialized");
                handle->fxns->process(handle);
            }
        }
    }
    /* Terminate AFP */
    handle->fxns->acpExit();
    handle->fxns->log(handle,handle->logObj,"AFP6: Terminating");
#ifdef BIOS6_NONLEGACY
    Task_setPri(Task_self(),-1);
    Task_yield();
#else
    TSK_setpri(TSK_self(),-1);
    TSK_yield();
#endif
}

#ifdef BIOS6_NONLEGACY
extern HeapMem_Handle SDRAMHeap;
#else
extern Int SDRAM;
#endif

const AFP6_Fxns AFP6_DEFAULT_FXNS = {
    AFP6_log,
    AFP6_memAlloc,
    AFP6_task,
    AFP6_process,
    ACP_MDS_init,
    ACP_MDS_exit
};

AFP6_Obj AFP6_OBJ = {
    sizeof(AFP6_Obj),
    &AFP6_DEFAULT_FXNS,
    &AFP6_PARAMS,
    NULL,
    &DCS6_FXNS,
    NULL,
    NULL,
    NULL,
    (ACP_Params*)&ACP_PARAMS,
    NULL,
    NULL,
#ifdef BIOS6_NONLEGACY
    (void*)&SDRAMHeap,
    (void*)&SDRAMHeap,
    NULL,
#else
    (void*)&SDRAM,
    (void*)&SDRAM,
    NULL,
#endif
};

