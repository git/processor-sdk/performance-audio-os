/*
 *  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
 *  ALL RIGHTS RESERVED 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *  @file   util_paf_file_io.h
 *
 *  @brief This file contains the common declarations for operations
 *         used to read audio data from file buffer to paf data array
 *         and write data back to file buffer from paf data array.
 *
 *         The code is wrapper to core FILE IO implementation. Added to provide
 *         interface between buffer used by core FILE IO and audio data
 *         present in PAF_AudioFrame structure.
 *
 *         It is expected that PAF_AudioFrame is already initialized externally.
 *
 *         Presently, .wav files are only supported (32bit PCM).
 *
 *  @todo  Further modifications are required to handle metadata and read
 *         encoded (.mlp, .mat, .ec3 and etc.) file.
 *
 *
 *****************************************************************************/
#ifndef __UTIL_PAF_FILE_IO_H__
#define __UTIL_PAF_FILE_IO_H__

/************************************************************
                     Include files
*************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "paftyp.h"

/************************************************************
                     Globals variables
*************************************************************/
/* look-up table will be here for mapping the data to AF as per
 part of ccm.c, in std asp library*/
extern const char AFChanPtrMap[PAF_MAXNUMCHAN+1][PAF_MAXNUMCHAN];

/************************************************************
                Function Declarations
*************************************************************/
/**
 *  @brief     Initialization
 *
 *  @param[in] file      Handle to audio files
 *
 *  @param[in] inFile    Input file name
 *
 *  @param[in] outFile   Output file name
 *
 *  @retval    RETRUN_SUCCESS on Success, RETRUN_ERROR on error
 *
 *  @remark    Read input/ouput files specified and
 *             get the input file info
 *
 */
int PAF_FIO_Init(tFIOHandle *file, char *inFile, char *outFile);

/**
 *  @brief     Configuration call
 *
 *  @param[in] file             Handle to audio files
 *
 *  @param[in] bufChannelCount  Requested number of audio channels in buffers
 *
 *  @param[in] inBufSampleCount Requested number of samples per channel
 *                              in buffers
 *
 *  @retval    RETRUN_SUCCESS on Success, RETRUN_ERROR on error
 *
 *  @remark    Configure the requested parameters for future use.
 *             Allocate the memory required for input/ouput buffers as per
 *             number of channels and samples requested.
 *
 *             The data will be provided in inBuf and outBuf as per these
 *             configuration.
 *
 */
int PAF_FIO_Config(tFIOHandle *file, PAF_AudioFrame *pAudioFrame,
                   int bufChannelCount);

/**
 *  @brief     Data read from input buffer into PAF structure
 *
 *  @param[in] inBuf Input buffer having data
 *
 *  @retval    RETRUN_SUCCESS on Success, RETRUN_ERROR on error/end of file
 *
 *  @remark    Read data from input buffer and copy into PAF Audio
 *             Frame structure field "sample"
 *             buffer as configured for corresponding channels.
 *
 */
int PAF_FIO_Read(tFIOHandle *file, PAF_AudioFrame *pAudioFrame);

/**
 *  @brief     Data write to output file
 *
 *  @param[in] file   Handle to audio files
 *
 *  @retval    RETRUN_SUCCESS on Success, RETRUN_ERROR on error
 *
 *  @remark    Write data present in output buffer to output file (interleaved)
 *
 */
int PAF_FIO_Write(tFIOHandle *file, PAF_AudioFrame *pAudioFrame);

/**
 *  @brief     Deinitialization
 *
 *  @param[in] file   Handle to audio files
 *
 *  @retval    RETRUN_SUCCESS on Success, RETRUN_ERROR on error
 *
 *  @remark    free the allocated memory for buffers and
 *             close the input/ouput files.
 *
 */
int PAF_FIO_DeInit(tFIOHandle *file,PAF_AudioFrame *pAudioFrame);

#endif  /* __UTIL_PAF_FILE_IO_H__ */
/***************************End of file***************************************/

