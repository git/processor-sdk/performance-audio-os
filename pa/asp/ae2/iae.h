
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the ASP Example Demonstration algorithm.
 */
#ifndef IAE_
#define IAE_

#include <ialg.h>
#include  <std.h>
#include <xdas.h>

#include "icom.h"
#include "paftyp.h"

#define AE_MAX_NUMCHAN      (2)
#define AE_MAX_COMB         (8)
#define AE_MAX_ALL_PASS     (4)
#define AE_MAX_FSIZE        (256)

/*
 *  ======== IAE_Cdl ========
 *  This structure is used to describe one instance of
 *  a long delay memory (circular delay line)
 */
typedef struct IAE_Cdl {
    PAF_AudioData *ptr;
    Int ndx;
    Int len;
} IAE_Cdl;

/*
 *  ======== IAE_Cdl ========
 *  This structure is used to describe the method requested for 
 *  accessing the circular delay lines placed in SDRAM
 */
typedef enum IAE_CdlAccess {
    IAE_CDL_ACCESS_DIRECT,
    IAE_CDL_ACCESS_SIMPLE_DMA,
    IAE_CDL_ACCESS_CONCURRENT_DMA
} IAE_CdlAccess;


/*
 *  ======== IAE_Obj ========
 *  Every implementation of IAE *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IAE_Obj {
    struct IAE_Fxns *fxns;    /* function list: standard, public, private */
} IAE_Obj;

/*
 *  ======== IAE_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IAE_Obj *IAE_Handle;

/*
 *  ======== IAE_Status ========
 *  This Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.  This structure is actually
 *  instantiated and initialized in iae.c.
 */
typedef volatile struct IAE_Status {

    Int         size;             /* This value must always be here, and must be set to the 
                             total size of this structure in 8-bit bytes, as the 
                             sizeof() operator would do. */
    XDAS_Int8   mode;       /* This is the 8-bit AE Mode Control Register.  All 
                             Algorithms must have a mode control register.  */
    XDAS_Int8   unused;     /* This 8-bit register is unused, and is not only useful as a 
                             "spare", but also provides 16-bit alignment to the next 
                             value. */
    XDAS_Int16  roomSize;
    XDAS_Int16  damping;

    XDAS_Int16  preScale;
    XDAS_Int16  wetMix;
    XDAS_Int16  xwetMix;
    XDAS_Int16  dryMix;

    XDAS_Int8   skipCombs;
    XDAS_Int8   skipAllPass;
    
} IAE_Status;

/*
 *  ======== IAE_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IAE_Config {
    IAE_CdlAccess cdlAccess;
    Int (*applyFilter)(IAE_Handle handle,
            Int (*pFilter)(IAE_Handle handle, PAF_AudioData * pIn,
                PAF_AudioData * pOut, PAF_AudioData * pDelay,
                PAF_AudioData * pState, PAF_AudioData * pCoef,
                Int nSamples),
            PAF_AudioData * pIn, PAF_AudioData * pOut, 
            IAE_Cdl *pBuf, PAF_AudioData **pState, PAF_AudioData **pCoef,
            Int filterCount, Int sampleCount);
    Int (*cdlClear)(IAE_Handle handle, IAE_Cdl *pBuf);   
    PAF_AudioData alphaScale;
    PAF_AudioData alphaCoef;
    Int numComb;
    Int combLen[AE_MAX_NUMCHAN][AE_MAX_COMB];
    Int numAllPass;
    Int allPassLen[AE_MAX_NUMCHAN][AE_MAX_ALL_PASS];
} IAE_Config;

/*
 *  ======== IAE_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a AE object.
 *
 *  Every implementation of IAE *must* declare this structure as
 *  the first member of the implementation's parameter structure.  This structure is actually
 *  instantiated and initialized in iae.c.
 */
typedef struct IAE_Params {
    Int size;
    const IAE_Status *pStatus;
    IAE_Config config;
} IAE_Params;

/*
 *  ======== IAE_PARAMS ========
 *  Default instance creation parameters (defined in iae.c)
 */
extern const IAE_Params IAE_PARAMS;

/*
 *  ======== IAE_Fxns ========
 *  All implementation's of AE must declare and statically 
 *  initialize a constant variable of this type.
 *
 *  By convention the name of the variable is AE_<vendor>_IAE, where
 *  <vendor> is the vendor name.
 */
typedef struct IAE_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int         (*reset)(IAE_Handle, PAF_AudioFrame *);
    Int         (*apply)(IAE_Handle, PAF_AudioFrame *);
    /* private */
    Int (*process)(IAE_Handle handle, PAF_AudioFrame * pAudioFrame);
    Int (*setup)(IAE_Handle handle, PAF_AudioFrame * pAudioFrame);
    Int (*comb)(IAE_Handle handle, PAF_AudioData * pIn,
                    PAF_AudioData * pOut, PAF_AudioData * pDelay,
                    PAF_AudioData * pState, PAF_AudioData * pCoef,
                    Int nSamples);
    Int (*allPass)(IAE_Handle handle, PAF_AudioData * pIn,
                       PAF_AudioData * pOut, PAF_AudioData * pDelay,
                       PAF_AudioData * pState, PAF_AudioData * pCoef,
                       Int nSamples);
} IAE_Fxns;

/*
 *  ======== IAE_Cmd ========
 *  The Cmd enumeration defines the control commands for the AE
 *  control method.
 */
typedef enum IAE_Cmd {
    IAE_NULL                    = ICOM_NULL,
    IAE_GETSTATUSADDRESS1       = ICOM_GETSTATUSADDRESS1,
    IAE_GETSTATUSADDRESS2       = ICOM_GETSTATUSADDRESS2,
    IAE_GETSTATUS               = ICOM_GETSTATUS,
    IAE_SETSTATUS               = ICOM_SETSTATUS
} IAE_Cmd;

Int AE_TII_applyFilterDirect(IAE_Handle handle,
        Int (*pFilter)(IAE_Handle handle, PAF_AudioData * pIn,
            PAF_AudioData * pOut, PAF_AudioData * pDelay,
            PAF_AudioData * pState, PAF_AudioData * pCoef,
            Int nSamples),
        PAF_AudioData * pIn, PAF_AudioData * pOut, 
        IAE_Cdl *pBuf, PAF_AudioData **pState, PAF_AudioData **pCoef,
        Int filterCount, Int sampleCount);
Int AE_TII_cdlClearDirect(IAE_Handle handle, IAE_Cdl *pBuf); 
  
Int AE_TII_applyFilterSimpleDMA(IAE_Handle handle,
        Int (*pFilter)(IAE_Handle handle, PAF_AudioData * pIn,
            PAF_AudioData * pOut, PAF_AudioData * pDelay,
            PAF_AudioData * pState, PAF_AudioData * pCoef,
            Int nSamples),
        PAF_AudioData * pIn, PAF_AudioData * pOut, 
        IAE_Cdl *pBuf, PAF_AudioData **pState, PAF_AudioData **pCoef,
        Int filterCount, Int sampleCount);
Int AE_TII_cdlClearDMA(IAE_Handle handle, IAE_Cdl *pBuf);

Int AE_TII_applyFilterConcurrentDMA(IAE_Handle handle,
        Int (*pFilter)(IAE_Handle handle, PAF_AudioData * pIn,
            PAF_AudioData * pOut, PAF_AudioData * pDelay,
            PAF_AudioData * pState, PAF_AudioData * pCoef,
            Int nSamples),
        PAF_AudioData * pIn, PAF_AudioData * pOut, 
        IAE_Cdl *pBuf, PAF_AudioData **pState, PAF_AudioData **pCoef,
        Int filterCount, Int sampleCount);


#endif  /* IAE_ */
