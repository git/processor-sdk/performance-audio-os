/****************************************************************************/
/*  c66_data_sim.h                                                          */
/****************************************************************************/

/* 
 *
 * Copyright (C) 1997-2010 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* Assume this is included in C6xSimulator.h */

#ifndef C66_DATA_SIM_H
#define C66_DATA_SIM_H

#if !defined(_TMS320C6X) && defined(TMS320C66X)

__x128_t   _ito128(uint32, uint32, uint32, uint32);
__x128_t   _fto128(float32, float32, float32, float32);
__x128_t   _llto128(int64_ll, int64_ll);
__x128_t   _dto128(double64, double64);
__x128_t   _f2to128(__float2_t, __float2_t);
__x128_t   _dup32_128(uint32);
double64   _lltod(int64_ll);
__float2_t _lltof2(int64_ll);
int64_ll   _dtoll(double64);
int64_ll   _f2toll(__float2_t);
float32    _hif(double64);
float32    _lof(double64);
float32    _hif2(__float2_t);
float32    _lof2(__float2_t);
int64_ll   _hi128(__x128_t);
double64   _hid128(__x128_t);
__float2_t _hif2_128(__x128_t);
int64_ll   _lo128(__x128_t);
double64   _lod128(__x128_t);
__float2_t _lof2_128(__x128_t);
uint32     _get32_128(__x128_t, unsigned);
float32    _get32f_128(__x128_t, unsigned);

#endif      // !defined(_TMS320C6X) && defined(TMS320C66X)

#endif      // C66_DATA_SIM_H

