################################################################################
Building library for a15 and c66x:
$PAF_ROOT/pa/build/Makefile
build the following dependent components  
DIRS =											\
	../asp/dm                                   \
	../dec/com                                  \
    ../asp/std   
################################################################################
Building test application for a15 and c66x:

building c66x:
To build this application expects following files
--> for profiling<--
$PAF_ROOT/pa/util/util_profiling/c66x/tsc_h.asm
--> for cache<--
$PAF_ROOT/pa/util/util_c66x_cache/util_c66x_cache.h

building a15:
To build this application expects following files
--> for profiling<--
$PAF_ROOT/pa/util/util_profiling/a15/timer64_api.h
$PAF_ROOT/pa/util/util_profiling/a15/timer64loc.h
$PAF_ROOT/pa/util/util_profiling/a15/type_defs.h
