/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/


#ifndef PCETEST_
#define PCETEST_

extern void DAT_wait_dummy (Uint32 eventid);
extern Uint32 DAT_cacheop_and_copy (void *src, void *dst, Uint16 cnt);
#if defined(ROM_BUILD)
#pragma DATA_SECTION(PAFHJT, ".const:PAFHJT");
const PAFHJT_t PAFHJT = {
#else /* ROM_BUILD */
const PAFHJT_t PAFHJT_RAM = {
#endif /* ROM_BUILD */

	NULL, //CPL_setAudioFrame_,
	NULL, //CPL_fft_,
	NULL, //CPL_Modulation_new_,
	NULL, //CPL_Demodulation_new_,
	NULL, //CPL_window_aac_ac3_,

	// COM ASP
	
	NULL, //COM_TII_snatchCommon_,
	COM_TII_activateCommon_,
	COM_TII_deactivateCommon_,
	
	// COM DEC

	NULL, //CPL_cdm_downmixSetUp_,
	NULL, //CPL_cdm_samsiz_,
	NULL, //CPL_cdm_downmixApply_,
	NULL, //CPL_cdm_downmixConfig_,
	NULL, //CDMTo_Mono_,
	NULL, //CDMTo_Stereo_,
	NULL, //CDMTo_Phantom1_,
	NULL, //CDMTo_Phantom2_,
	NULL, //CDMTo_Phantom3_,
	NULL, //CDMTo_Phantom4_,
	NULL, //CDMTo_3Stereo_,
	NULL, //CDMTo_Surround1_,
	NULL, //CDMTo_Surround2_,
	NULL, //CDMTo_Surround3_,
	NULL, //CDMTo_Surround4_,

	NULL, //CPL_vecAdd_,
	NULL, //CPL_vecLinear2_,
	NULL, //CPL_vecScale_,
	NULL, //CPL_vecSet_,
	NULL, //CPL_ivecCopy_,
	NULL, //CPL_svecSet_,
	NULL, //CPL_ivecSet_,
	NULL, //CPL_imaskScale_,
	NULL, //CPL_smaskScale_,
	NULL, //CPL_vecStrideScale_,

	NULL, //CPL_vecLinear2Incr_,
	NULL, //CPL_vecLinear2Common_,
	NULL, //CPL_vecScaleAcc_,
	NULL, //CPL_vecScaleIncr_,
	NULL, //CPL_vecSetArr_,
	NULL, //CPL_GetPAFSampleRate_,
	NULL, //CPL_GetNumSat_,
	NULL, //CPL_intDelayProc_,
	NULL, //CPL_floatDelayProc_,
	NULL, //CPL_DelaySet_,
	NULL, //CPL_DelayInit_,
	NULL, //CPL_sumDiff_,
	NULL, //CPL_delay_,

	NULL, //CPL_fifoInit_,
	NULL, //CPL_fifoSize_,
	NULL, //CPL_fifoSizeFull_,
	NULL, //CPL_fifoSizeFree_,
	NULL, //CPL_fifoSizeFullLinear_,
	NULL, //CPL_fifoSizeFreeLinear_,
	NULL, //CPL_fifoRead_,
	NULL, //CPL_fifoWrite_,
	NULL, //CPL_fifoReadNoPosMv_,
	NULL, //CPL_fifoGetReadPtr_,
	NULL, //CPL_fifoReadDone_,
	NULL, //CPL_fifoGetWritePtr_,
	NULL, //CPL_fifoWrote_,
	NULL, //CPL_fifoMoveReadLocation_,
	NULL, //CPL_rotateLeft_,
	NULL, //CPL_rotateRight_,

#if defined(ROM_BUILD)
	NULL,													// DAT_copy
	NULL,													// DAT_wait
#else /* ROM_BUILD */
	DAT_cacheop_and_copy,//DAT_copy_dummy,											// DAT_copy
	DAT_wait_dummy,											// DAT_wait
#endif /* ROM_BUILD */

	// BIOS

	// CSL

	// dMAX

	// EDMA

};

/** @brief Test table representing PCE2 test case inputs*/
typedef struct {
    Int numSat;              // Sat Channels
    Int numSub;              //Sub channels
    Int vol;
    Int del;
    Int output;
}pce2TestTable, *pPce2TestTable;

#endif   /* PCETEST_ */

