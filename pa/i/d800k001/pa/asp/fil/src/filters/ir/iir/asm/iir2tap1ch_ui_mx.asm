
*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*

;******************************************************************************
;* TMS320C6x ANSI C Codegen                                      Version 4.20 *
;* Date/Time created: Mon Sep 01 15:38:07 2003                                *
;******************************************************************************

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C671x                                          *
;*   Optimization      : Enabled at level 3                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o3, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Memory Model      : Large                                                *
;*   Calls to RTS      : Far                                                  *
;*   Pipelining        : Enabled                                              *
;*   Speculative Load  : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : No Debug Info                                        *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss

	.sect	".text:Filter_iirT2Ch1_ui_mx"
	.global	_Filter_iirT2Ch1_ui_mx

;******************************************************************************
;* FUNCTION NAME: _Filter_iirT2Ch1_ui_mx                                      *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,B0,*
;*                           B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,DP,SP *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,B0,*
;*                           B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,DP,SP *
;*   Local Frame Size  : 0 Args + 0 Auto + 40 Save = 40 byte                  *
;******************************************************************************
_Filter_iirT2Ch1_ui_mx:
;** --------------------------------------------------------------------------*
           LDW     .D1T1   *+A4(8),A3        ; |75| 

           LDW     .D1T1   *A4,A0            ; |73| 
||         STW     .D2T2   B13,*SP--(40)     ; |65| 

           STW     .D2T1   A10,*+SP(4)       ; |65| 
           STW     .D2T1   A13,*+SP(16)      ; |65| 
           STW     .D2T1   A11,*+SP(8)       ; |65| 
           STW     .D2T1   A12,*+SP(12)      ; |65| 
           STW     .D2T1   A14,*+SP(20)      ; |65| 
           LDW     .D1T1   *A0,A0            ; |73| 
           LDW     .D1T1   *A3,A13           ; |75| 
           LDW     .D1T1   *+A4(16),A3

           LDW     .D1T1   *+A4(12),A5       ; |77| 
||         STW     .D2T2   B11,*+SP(32)      ; |65| 

           STW     .D2T2   B3,*+SP(24)       ; |65| 
           STW     .D2T2   B10,*+SP(28)      ; |65| 
           STW     .D2T2   B12,*+SP(36)      ; |65| 

           SUB     .S1     A3,3,A1
||         LDW     .D1T1   *A0,A3            ; (P) |90| 

           LDW     .D1T1   *A5,A12           ; |77| 
           LDDW    .D1T1   *A13,A5:A4
           NOP             2

           LDDW    .D1T1   *+A13(32),A9:A8
||         SPDP    .S1     A3,A11:A10        ; (P) |90| 

           LDDW    .D1T1   *+A12(8),A7:A6    ; |83| 
           MPYDP   .M1     A5:A4,A11:A10,A3:A2 ; (P) |90| 
           NOP             3
           MPYDP   .M1     A7:A6,A9:A8,A11:A10 ; (P) |90| 
           LDDW    .D1T2   *A12,B13:B12      ; |82| 
           LDDW    .D1T2   *+A13(24),B3:B2
           NOP             1
           MVC     .S2     CSR,B4

           MV      .S1X    B4,A14
||         AND     .S2     -2,B4,B4

           MVC     .S2     B4,CSR            ; interrupts off
||         LDDW    .D1T2   *+A13(16),B9:B8

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line               : 87
;*      Loop opening brace source line : 88
;*      Loop closing brace source line : 98
;*      Known Minimum Trip Count         : 16
;*      Known Maximum Trip Count         : 2048
;*      Known Max Trip Count Factor      : 4
;*      Loop Carried Dependency Bound(^) : 18
;*      Unpartitioned Resource Bound     : 10
;*      Partitioned Resource Bound(*)    : 12
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     2        7     
;*      .S units                     3        0     
;*      .D units                     2        0     
;*      .M units                     8       12*    
;*      .X cross paths               2        6     
;*      .T address paths             1        1     
;*      Long read paths              0        1     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           2        0     (.L or .S unit)
;*      Addition ops (.LSD)          1        2     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             4        4     
;*      Bound(.L .S .D .LS .LSD)     4        3     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 18 Schedule found with 4 iterations in parallel
;*      done
;*
;*      Epilog not removed
;*      Collapsed epilog stages     : 0
;*
;*      Prolog not removed
;*      Collapsed prolog stages     : 0
;*
;*      Minimum required memory pad : 0 bytes
;*
;*      For further improvement on this loop, try option -mh8
;*
;*      Minimum safe trip count     : 4
;*----------------------------------------------------------------------------*
;*   SINGLE SCHEDULED ITERATION
;*
;*   C38:
;*              LDW     .D1T1   *A0,A3            ; |90| 
;*              NOP             4
;*              SPDP    .S1     A3,A11:A10        ; |90| 
;*              NOP             1
;*              MPYDP   .M1     A5:A4,A11:A10,A3:A2 ; |90| 
;*              NOP             3
;*              MPYDP   .M1     A7:A6,A9:A8,A11:A10 ; |90| 
;*              NOP             6
;*              MPYDP   .M2     B13:B12,B3:B2,B7:B6 ;  ^ |90| 
;*              NOP             2
;*              ADDDP   .L1     A11:A10,A3:A2,A3:A2 ; |90| 
;*              MPYDP   .M2X    A7:A6,B9:B8,B1:B0 ; |93| 
;*              NOP             2
;*              MV      .S1X    B12,A6            ; |96| 
;*              NOP             1
;*              MPYDP   .M2     B13:B12,B11:B10,B5:B4 ; |93| 
;*              ADDDP   .L2X    B7:B6,A3:A2,B7:B6 ;  ^ |90| 
;*   ||         MV      .S1X    B13,A7            ; |96| 
;*              NOP             6
;*              MV      .S2     B6,B12            ;  ^ |97| 
;*   ||         MV      .D2     B7,B13            ;  ^ |97| 
;*              NOP             1
;*              ADDDP   .L2     B5:B4,B7:B6,B5:B4 ; |93| 
;*              NOP             6
;*              ADDDP   .L2     B1:B0,B5:B4,B5:B4 ; |93| 
;*              NOP             5
;*      [ A1]   SUB     .D1     A1,1,A1           ; |98| 
;*              DPSP    .L2     B5:B4,DP          ; |93| 
;*   || [ A1]   B       .S1     C38               ; |98| 
;*              NOP             3
;*              STW     .D1T2   DP,*A0++          ; |93| 
;*              NOP             1
;*              ; BRANCH OCCURS                   ; |98| 
;*----------------------------------------------------------------------------*
L1:    ; PIPED LOOP PROLOG

           MPYDP   .M2     B13:B12,B3:B2,B7:B6 ; (P)  ^ |90| 
||         LDW     .D1T1   *+A0(4),A3        ; (P) @|90| 

           NOP             2
           ADDDP   .L1     A11:A10,A3:A2,A3:A2 ; (P) |90| 

           LDDW    .D1T2   *+A13(8),B11:B10
||         MPYDP   .M2X    A7:A6,B9:B8,B1:B0 ; (P) |93| 

           SPDP    .S1     A3,A11:A10        ; (P) @|90| 
           NOP             1

           MV      .S1X    B12,A6            ; (P) |96| 
||         MPYDP   .M1     A5:A4,A11:A10,A3:A2 ; (P) @|90| 

           NOP             1
           MPYDP   .M2     B13:B12,B11:B10,B5:B4 ; (P) |93| 

           MV      .S1X    B13,A7            ; (P) |96| 
||         ADDDP   .L2X    B7:B6,A3:A2,B7:B6 ; (P)  ^ |90| 

           MPYDP   .M1     A7:A6,A9:A8,A11:A10 ; (P) @|90| 
           NOP             5

           MV      .D2     B6,B12            ; (P)  ^ |97| 
||         MV      .S2     B7,B13            ; (P)  ^ |97| 

           MPYDP   .M2     B13:B12,B3:B2,B7:B6 ; (P) @ ^ |90| 
||         LDW     .D1T1   *+A0(8),A3        ; (P) @@|90| 

           ADDDP   .L2     B5:B4,B7:B6,B5:B4 ; (P) |93| 
           MV      .S1X    DP,A13            ; save dp
;** --------------------------------------------------------------------------*
L2:    ; PIPED LOOP KERNEL
           ADDDP   .L1     A11:A10,A3:A2,A3:A2 ; @|90| 
           MPYDP   .M2X    A7:A6,B9:B8,B1:B0 ; @|93| 
           SPDP    .S1     A3,A11:A10        ; @@|90| 
           NOP             1

           MV      .S1X    B12,A6            ; @|96| 
||         MPYDP   .M1     A5:A4,A11:A10,A3:A2 ; @@|90| 

           ADDDP   .L2     B1:B0,B5:B4,B5:B4 ; |93| 
           MPYDP   .M2     B13:B12,B11:B10,B5:B4 ; @|93| 

           MV      .S1X    B13,A7            ; @|96| 
||         ADDDP   .L2X    B7:B6,A3:A2,B7:B6 ; @ ^ |90| 

           MPYDP   .M1     A7:A6,A9:A8,A11:A10 ; @@|90| 
           NOP             2
   [ A1]   SUB     .D1     A1,1,A1           ; |98| 

           DPSP    .L2     B5:B4,DP          ; |93| 
|| [ A1]   B       .S1     L2                ; |98| 

           NOP             1

           MV      .D2     B7,B13            ; @ ^ |97| 
||         MV      .S2     B6,B12            ; @ ^ |97| 

           MPYDP   .M2     B13:B12,B3:B2,B7:B6 ; @@ ^ |90| 
||         LDW     .D1T1   *+A0(12),A3       ; @@@|90| 

           STW     .D1T2   DP,*A0++          ; |93| 
||         ADDDP   .L2     B5:B4,B7:B6,B5:B4 ; @|93| 

           NOP             1
;** --------------------------------------------------------------------------*
L3:    ; PIPED LOOP EPILOG
           ADDDP   .L1     A11:A10,A3:A2,A3:A2 ; (E) @@|90| 
           MPYDP   .M2X    A7:A6,B9:B8,B1:B0 ; (E) @@|93| 
           SPDP    .S1     A3,A11:A10        ; (E) @@@|90| 
           NOP             1

           MV      .S1X    B12,A6            ; (E) @@|96| 
||         MPYDP   .M1     A5:A4,A11:A10,A3:A2 ; (E) @@@|90| 

           ADDDP   .L2     B1:B0,B5:B4,B5:B4 ; (E) @|93| 
           MPYDP   .M2     B13:B12,B11:B10,B5:B4 ; (E) @@|93| 

           ZERO    .D1     A4                ; |104| 
||         MV      .S1X    B13,A7            ; (E) @@|96| 
||         ADDDP   .L2X    B7:B6,A3:A2,B7:B6 ; (E) @@ ^ |90| 

           MPYDP   .M1     A7:A6,A9:A8,A11:A10 ; (E) @@@|90| 
           NOP             3
           DPSP    .L2     B5:B4,DP          ; (E) @|93| 
           NOP             1

           MV      .D2     B6,B12            ; (E) @@ ^ |97| 
||         MV      .S2     B7,B13            ; (E) @@ ^ |97| 

           MPYDP   .M2     B13:B12,B3:B2,B7:B6 ; (E) @@@ ^ |90| 

           ADDDP   .L2     B5:B4,B7:B6,B5:B4 ; (E) @@|93| 
||         STW     .D1T2   DP,*A0++          ; (E) @|93| 

           NOP             1
           ADDDP   .L1     A11:A10,A3:A2,A3:A2 ; (E) @@@|90| 
           MPYDP   .M2X    A7:A6,B9:B8,B1:B0 ; (E) @@@|93| 
           NOP             2
           MV      .S1X    B12,A6            ; (E) @@@|96| 
           ADDDP   .L2     B1:B0,B5:B4,B5:B4 ; (E) @@|93| 
           MPYDP   .M2     B13:B12,B11:B10,B5:B4 ; (E) @@@|93| 

           MV      .S1X    B13,A7            ; (E) @@@|96| 
||         ADDDP   .L2X    B7:B6,A3:A2,B7:B6 ; (E) @@@ ^ |90| 

           NOP             4
           DPSP    .L2     B5:B4,DP          ; (E) @@|93| 
           NOP             1

           MV      .D2     B7,B13            ; (E) @@@ ^ |97| 
||         MV      .S2     B6,B12            ; (E) @@@ ^ |97| 

           NOP             1

           STW     .D1T2   DP,*A0++          ; (E) @@|93| 
||         ADDDP   .L2     B5:B4,B7:B6,B5:B4 ; (E) @@@|93| 

           NOP             6
           ADDDP   .L2     B1:B0,B5:B4,B5:B4 ; (E) @@@|93| 
           NOP             6

           MV      .S2X    A14,B4
||         DPSP    .L2     B5:B4,DP          ; (E) @@@|93| 

           MVC     .S2     B4,CSR            ; interrupts on
           NOP             2

           MV      .S2X    A13,DP            ; restore dp
||         STW     .D1T2   DP,*A0++          ; (E) @@@|93| 

           LDW     .D2T1   *+SP(4),A10       ; |105| 
||         STW     .D1T2   B12,*A12          ; |101| 

           STW     .D1T2   B13,*+A12(4)      ; |101| 
||         LDW     .D2T1   *+SP(20),A14      ; |105| 

           LDW     .D2T2   *+SP(24),B3       ; |105| 

           LDW     .D2T2   *+SP(36),B12      ; |105| 
||         STW     .D1T1   A6,*+A12(8)       ; |102| 

           LDW     .D2T2   *+SP(32),B11      ; |105| 
||         STW     .D1T1   A7,*+A12(12)      ; |102| 

           LDW     .D2T1   *+SP(12),A12      ; |105| 
           LDW     .D2T2   *+SP(28),B10      ; |105| 
           LDW     .D2T1   *+SP(16),A13      ; |105| 

           LDW     .D2T1   *+SP(8),A11       ; |105| 
||         B       .S2     B3                ; |105| 

           LDW     .D2T2   *++SP(40),B13     ; |105| 
           NOP             4
           ; BRANCH OCCURS                   ; |105| 


