/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/**
 *  @file   testapp_coef.c
 *
 *  @brief  Consist of the coefficients for different filter designs
 *          used by test application.
 *
 *          To added new filter function's test coefficients, insert the
 *          respective coefficient array at the same index where the filter
 *          function is present in function param table.
 *
 *          For every filter coefficient the Matlab code is provided for reference.
 *****************************************************************************/

#include "testapp.h"
 
/** Coefficients for IIR filter design */
//************************************************************
//                      1 TAP IIR Filter
//************************************************************
/*
Rp=.18;
Rs=60;
Fp=3000/(fs/2);
[b,a]=ellip(1,Rp,Rs,Fp, 'low');
*/
const Float iir_Coeff_1tap_48_fc3k_lp[3] = {
    /*B0*/       /*B1*/
    0.49160,    0.49160,
    /*A1*/
    0.016805
 };

/*
Rp=.2;
Rs=30;
Fp=3000/(fs/2);
[b,a]=ellip(1, Rp ,Rs, Fp, 'high');
*/
const Float iir_Coeff_1tap_48_fc3k_hp[3] = {
    /*B0*/       /*B1*/
    0.95862,    -0.95862,
    /*A1*/
    0.91723
};

/*
Rp=1;
Rs=30;
Fp=800/(fs/2);
[b22,a22]=ellip(1, Rp ,Rs, Fp, 'high');
*/
const Float iir_Coeff_1tap_48_fc800_hp[3] = {
     /*B0*/       /*B1*/
     0.97403,   -0.97403,
     /*A1*/
     0.94807
};

//************************************************************
//                      2 TAP IIR Filter
//************************************************************
/*
Rp=.5;
Fp=2000/(fs/2);
[b,a]=cheby1(2,Rp,Fp, 'low');
*/
const Float iir_Coeff_2tap_48_lp[5] = {
    /*B0*/       /*B1*/      /*B2*/
    0.020437,   0.040873,   0.020437,
    /*A1*/       /*A2*/
    1.60420,    -0.69079
 };

/*
Rp=.009;
Fp=3000/(fs/2);
[b4,a4]=cheby1(2, Rp, Fp, 'low');
*/
 const Float iir_Coeff_2tap_48_lp_2[5] = {
     /*B0*/       /*B1*/      /*B2*/
    0.18516,    0.37031 ,   0.18516,
     /*A1*/       /*A2*/
    0.48187,    -0.22326
 };

/*Rp=6;
Rs=50;
Fp=10000/(fs/2);
[b5,a5]=ellip(2, Rp ,Rs, Fp, 'high');
*/
 const Float iir_Coeff_2tap_48_hp[5] = {
     /*B0*/       /*B1*/      /*B2*/
    0.19914,    -0.39322,   0.19914,
     /*A1*/       /*A2*/
    -0.010641,  -0.589888
 };

 /*
 Rp=.009;
Rs=50;
Fp=3000/(fs/2);
Fp2=5000/(fs/2);
[b5,a5]=cheby1(1, Rp, [Fp,Fp2] );  #bandpass*/
 const Float iir_Coeff_2tap_48_bdpass[5] = {
     /*B0*/       /*B1*/      /*B2*/
    0.74296,    0.00000,    -0.74296,
     /*A1*/       /*A2*/
    0.44904,    0.48593
 };

 /*
Rp=6;
Rs=60;
Fp=2000/(fs/2) ;
Fp2=7000/(fs/2);
[b7,a7]=ellip(1, Rp ,Rs, [Fp,Fp2], 'stop');  #bandstop*/
 const Float iir_Coeff_2tap_48_bdstop[5] = {
      /*B0*/       /*B1*/      /*B2*/
    0.63048 , -1.10721  , 0.63048,
      /*A1*/       /*A2*/
    1.10721 , -0.26096
 };



 const Float *iir_Coeff0[] = {
  iir_Coeff_1tap_48_fc3k_lp   /*ch1*/
};
 const Float *iir_Coeff1[] = {
  iir_Coeff_1tap_48_fc3k_lp,  /*ch1*/
  iir_Coeff_1tap_48_fc3k_hp   /*ch2*/
};
 const Float *iir_Coeff2[] = {
  iir_Coeff_1tap_48_fc3k_hp,  /*ch1*/
  iir_Coeff_1tap_48_fc800_hp,  /*ch2*/
  iir_Coeff_1tap_48_fc3k_lp   /*ch3*/
};
 const Float *iir_Coeff3[] = {
  iir_Coeff_1tap_48_fc3k_lp,  /*ch1*/
  iir_Coeff_1tap_48_fc3k_hp,  /*ch2*/
  iir_Coeff_1tap_48_fc800_hp, /*ch3*/
  iir_Coeff_1tap_48_fc3k_lp   /*ch4*/
};
 const Float *iir_Coeff4[] = {
  iir_Coeff_1tap_48_fc3k_hp,  /*ch1*/
};
 const Float *iir_Coeff5[] = {
  iir_Coeff_1tap_48_fc3k_hp,  /*ch1*/
};
 const Float *iir_Coeff6[] = {
  iir_Coeff_1tap_48_fc3k_hp,  /*ch1*/
};
 const Float *iir_Coeff7[] = {
  iir_Coeff_1tap_48_fc3k_hp,  /*ch1*/
  iir_Coeff_1tap_48_fc3k_hp,  /*ch2*/
  iir_Coeff_1tap_48_fc3k_lp,  /*ch3*/
  iir_Coeff_1tap_48_fc3k_lp,  /*ch4*/
  iir_Coeff_1tap_48_fc3k_lp,  /*ch5*/
  iir_Coeff_1tap_48_fc800_hp, /*ch6*/
  iir_Coeff_1tap_48_fc800_hp, /*ch7*/
  iir_Coeff_1tap_48_fc800_hp  /*ch8*/
};
 const Float *iir_Coeff8[] = {
  iir_Coeff_2tap_48_lp     /*ch1*/
};
 const Float *iir_Coeff9[] = {
  iir_Coeff_2tap_48_lp,    /*ch1*/
  iir_Coeff_2tap_48_lp_2   /*ch2*/
};
 const Float *iir_Coeff10[] = {
  iir_Coeff_2tap_48_bdpass,  /*ch1*/
  iir_Coeff_2tap_48_lp,      /*ch2*/
  iir_Coeff_2tap_48_bdpass   /*ch3*/
};
 const Float *iir_Coeff11[] = {
  iir_Coeff_2tap_48_bdstop,  /*ch1*/
};
 const Float *iir_Coeff12[] = {
  iir_Coeff_2tap_48_bdstop,  /*ch1*/
};
 const Float *iir_Coeff13[] = {
  iir_Coeff_2tap_48_lp_2,  /*ch1*/
  iir_Coeff_2tap_48_lp_2,  /*ch2*/
  iir_Coeff_2tap_48_hp,    /*ch3*/
  iir_Coeff_2tap_48_lp,    /*ch4*/
  iir_Coeff_2tap_48_lp,    /*ch5*/
  iir_Coeff_2tap_48_lp,    /*ch6*/
  iir_Coeff_2tap_48_hp,    /*ch7*/
  iir_Coeff_2tap_48_hp     /*ch8*/
};
 const Float *iir_Coeff14[] = {
  iir_Coeff_2tap_48_bdstop,  /*ch1*/
  iir_Coeff_2tap_48_bdstop,  /*ch2*/
  iir_Coeff_2tap_48_lp,      /*ch3*/
  iir_Coeff_2tap_48_lp,      /*ch4*/
  iir_Coeff_2tap_48_bdpass,  /*ch5*/
  iir_Coeff_2tap_48_bdpass,  /*ch6*/
  iir_Coeff_2tap_48_bdpass,  /*ch7*/
  iir_Coeff_2tap_48_bdpass   /*ch8*/
};



const Float **iir_coeff[] = {
    /*filter 0 coeff*/
    iir_Coeff0,
    /*filter 1 coeff*/
    iir_Coeff1,
    /*filter 2 coeff*/
    iir_Coeff2,
    /*filter 3 coeff*/
    iir_Coeff3,
    /*filter 4 coeff*/
    iir_Coeff4,
    /*filter 5 coeff*/
    iir_Coeff5,
    /*filter 6 coeff*/
    iir_Coeff6,
    /*filter 7 coeff*/
    iir_Coeff7,
    /*filter 8 coeff*/
    iir_Coeff8,
    /*filter 9 coeff*/
    iir_Coeff9,
    /*filter 10 coeff*/
    iir_Coeff10,
    /*filter 11 coeff*/
    iir_Coeff11,
    /*filter 12 coeff*/
    iir_Coeff12,
    /*filter 13 coeff*/
    iir_Coeff13,
    /*filter 14 coeff*/
    iir_Coeff14,
};


/** Coefficients for FIR filter design */
//************************************************************
//                      2 TAP FIR Filter
//************************************************************

/*
Fp=3000/(fs/2);
[b6] = fir1(2, Fp);
 a6 = 1;
*/
 const Float fir_Coeff_2tap_48[3] = {
    /*B0*/       /*B1*/      /*B2*/
    0.067468 ,  0.865065  , 0.067468
 };


//************************************************************
//                      4 TAP FIR Filter
//************************************************************
/*
Fp=3000/(fs/2);
Fp2=8000/(fs/2);
[b11] = fir1(4, [Fp,Fp2], 'stop');
 a11 = 1;
*/
 const Float fir_Coeff_4tap_48[] = {
     /*B0*/       /*B1*/        /*B2*/      /*B3*/       /*B4*/
    -0.0034108  ,-0.1341810 ,  1.2751836 , -0.1341810 , -0.0034108
 };

//************************************************************
//                      8 TAP FIR Filter
//************************************************************
/*
Fp=3000/(fs/2);
Fp2=8000/(fs/2);
[b12] = fir1(8, Fp, 'high');
 a12 = 1;
*/
 const Float fir_Coeff_8tap_48[] = {
     /*B0*/       /*B1*/        /*B2*/       /*B3*/       /*B4*/       /*B5*/       /*B6*/        /*B7*/      /*B8*/
     -0.0064067 , -0.0211030,  -0.0607852 , -0.1052904 ,  0.8815971,  -0.1052904,  -0.0607852 , -0.0211030 , -0.0064067
};

//************************************************************
//                      16 TAP FIR Filter
//************************************************************
/*
 Fp=3000/(fs/2);
[b12] = fir1(16, Fp, 'high');
 a12 = 1;
*/
 const Float fir_Coeff_16tap_48[17] = {
     /*B0*/          /*B1*/           /*B2*/       /*B3 .........*/
    -7.8093e-005 , -2.1041e-003 , -8.1993e-003 , -2.1533e-002 , -4.2952e-002 , -6.9893e-002 , -9.6739e-002 , -1.1663e-001,
    
    8.7562e-001 , -1.1663e-001 , -9.6739e-002,  -6.9893e-002,  -4.2952e-002 , -2.1533e-002 , -8.1993e-003 , -2.1041e-003,
     /*B16*/
    -7.8093e-005
};

//************************************************************
//                      20 TAP FIR Filter
//************************************************************
/*
Fp=3000/(fs/2);
[b6] = fir1(20, Fp);
 a6 = 1;
*/
 const Float fir_Coeff_20tap_48_lp[21] = {
    /*B0*/          /*B1*/           /*B2*/       /*B3 .........*/
    -1.8768e-003 , -1.3925e-003 , 1.7631e-004,  5.3075e-003  ,1.6347e-002 , 3.4377e-002 , 5.8385e-002,  8.5117e-002,
    
    1.0974e-001 , 1.2712e-001,  1.3340e-001 , 1.2712e-001 , 1.0974e-001 , 8.5117e-002 , 5.8385e-002 , 3.4377e-002,
                                                              /*B20*/
    1.6347e-002 , 5.3075e-003 , 1.7631e-004 , -1.3925e-003 , -1.8768e-003
};


/* 
Fp=3000/(fs/2);
[b6] = fir1(20, Fp, 'high');
 a6 = 1;
*/
 const Float fir_Coeff_20tap_48_hp[21] = {
     /*B0*/          /*B1*/           /*B2*/       /*B3 .........*/
    1.7414e-003 , 1.2921e-003  ,-1.6359e-004 , -4.9246e-003 , -1.5168e-002 , -3.1897e-002 , -5.4174e-002  ,-7.8978e-002,
    
    -1.0182e-001 , -1.1795e-001 , 8.7425e-001 , -1.1795e-001  ,-1.0182e-001 , -7.8978e-002 , -5.4174e-002 , -3.1897e-002,
                                                                /*B20*/
    -1.5168e-002 , -4.9246e-003 , -1.6359e-004  ,1.2921e-003 , 1.7414e-003,
};

//************************************************************
//                      40 TAP FIR Filter
//************************************************************
/*
Fp=3000/(fs/2);
Fp2=5000/(fs/2);
[b6] = fir1(40, [Fp,Fp2]);
 a6 = 1;
*/
 const Float fir_Coeff_40tap_48_bdpass[41] = {
      /*B0*/          /*B1*/           /*B2*/       /*B3 .........*/
    -9.3094e-004 , -2.0643e-003,  -3.3824e-003 , -4.2840e-003 ,  -3.4900e-003 , 5.1395e-004  ,8.4009e-003   , 1.8810e-002  ,

    2.7874e-002  ,3.0120e-002  ,2.0768e-002    , -1.4554e-003 ,  -3.2371e-002 , -6.2587e-002 , -8.0338e-002 , -7.6074e-002 ,

    -4.6946e-002 , 9.3362e-004 , 5.3776e-002   , 9.4733e-002  ,  1.1013e-001  ,9.4733e-002   , 5.3776e-002  ,9.3362e-004   ,

    -4.6946e-002 , -7.6074e-002,  -8.0338e-002 ,  -6.2587e-002,  -3.2371e-002 , -1.4554e-003 , 2.0768e-002  ,3.0120e-002   ,

    2.7874e-002  ,1.8810e-002  ,8.4009e-003    ,  5.1395e-004 , -3.4900e-003  ,-4.2840e-003  ,-3.3824e-003  ,-2.0643e-003  ,
    /*B40*/
    -9.3094e-004
  };

//************************************************************
//                      60 TAP FIR Filter
//************************************************************
/*
Fp=3000/(fs/2);
Fp2=5000/(fs/2);
[b6] = fir1(40, [Fp,Fp2]);
 a6 = 1;
*/
 const Float fir_Coeff_60tap_48_bdstop[61] = {
     /*B0*/          /*B1*/           /*B2*/       /*B3 .........*/
    -5.7648e-004 , -4.5766e-005 , -1.8366e-004 , -1.1764e-003 ,  -2.2757e-003 , -2.0483e-003  , 3.2763e-004   ,3.6919e-003   ,
    
    5.3230e-003  ,3.6116e-003   , 5.1669e-004  , 3.8982e-004  , 5.0873e-003   , 1.0196e-002   ,7.7457e-003    ,-5.4007e-003  ,
    
    -2.1723e-002 , -2.7785e-002 , -1.7436e-002 , -5.9999e-004 , 4.0443e-003   , -1.1035e-002  , -2.8810e-002  ,-1.8161e-002  ,
    
    3.5765e-002  ,1.0781e-001   ,  1.4223e-001 ,  9.4578e-002 , -2.6225e-002  ,  -1.5401e-001 ,  7.9234e-001  ,-1.5401e-001  ,
    
    -2.6225e-002 , 9.4578e-002  ,  1.4223e-001 ,  1.0781e-001 , 3.5765e-002   , -1.8161e-002  , -2.8810e-002  ,-1.1035e-002  ,
    
    4.0443e-003  ,-5.9999e-004  ,  -1.7436e-002,  -2.7785e-002,  -2.1723e-002 ,   -5.4007e-003,   7.7457e-003 , 1.0196e-002  ,
    
    5.0873e-003  ,3.8982e-004   , 5.1669e-004  ,  3.6116e-003 , 5.3230e-003   ,   3.6919e-003 ,  3.2763e-004  ,-2.0483e-003  ,
                                                                /*B60*/
    -2.2757e-003 , -1.1764e-003 , -1.8366e-004 , -4.5766e-005 , -5.7648e-004
  };


const Float *fir_Coeff0[] = {
  fir_Coeff_20tap_48_lp   /*ch1*/
};
 const Float *fir_Coeff1[] = {
  fir_Coeff_2tap_48,  /*ch1*/
  fir_Coeff_2tap_48,  /*ch2*/
  fir_Coeff_2tap_48,  /*ch3*/
  fir_Coeff_2tap_48,  /*ch4*/
  fir_Coeff_2tap_48,  /*ch5*/
  fir_Coeff_2tap_48,  /*ch6*/
  fir_Coeff_2tap_48,  /*ch7*/
  fir_Coeff_2tap_48   /*ch8*/
};

const Float **fir_coeff[] = {
    /*filter 0 coeff*/
    fir_Coeff0,
    /*filter 1 coeff*/
    fir_Coeff1
};



/** Coefficients for IIR MX filter design */
//************************************************************
//                      1 TAP IIR MX Filter
//************************************************************
/*
Rp=.2;
Rs=30;
Fp=3000/(fs/2);
[b,a]=ellip(1, Rp ,Rs, Fp, 'high');
*/
const double iir_mx_Coeff_1tap_48_fc3k_hp[3] = {
    /*B0*/       /*B1/B0*/
    0.95862,    -0.95862/0.95862,
    /*A1*/
    0.91723
};

//************************************************************
//                      2 TAP IIR MX Filter
//************************************************************

/*
Rp=.009;
Fp=3000/(fs/2);
[b4,a4]=cheby1(2, Rp, Fp, 'low');
*/
 const double iir_mx_Coeff_2tap_48_lp[5] = {
    /*B0*/       /*B1/B0*/         /*B2/B0*/
    0.18516  , 0.37031/0.18516 ,  0.18516/0.18516,
    /*A1*/       /*A2*/
    0.48187   ,-0.22326
 };


/*
Rp=6;
Rs=50;
Fp=10000/(fs/2);
[b5,a5]=ellip(2, Rp ,Rs, Fp, 'high');
*/
 const double iir_mx_Coeff_2tap_48_hp[5] = {
    /*B0*/       /*B1/B0*/         /*B2/B0*/
    0.19914 , -0.39322/0.19914 ,  0.19914/0.19914,
    /*A1*/       /*A2*/
    -0.010641  , -0.589888
 };

/*
Rp=.009;
Rs=50;
Fp=3000/(fs/2);
Fp2=5000/(fs/2);
[b5,a5]=cheby1(1, Rp, [Fp,Fp2] );  #bandpass*/
 const double iir_mx_Coeff_2tap_48_bdpass[5] = {
     /*B0*/       /*B1/B0*/         /*B2/B0*/
    0.74296  , 0.00000 , -0.74296/0.74296,
     /*A1*/       /*A2*/
    0.44904 ,  0.48593
 };

//************************************************************
//                      4 TAP IIR MX Filter
//************************************************************
/*
Rp=3;
Rs=60;
Fp=2000/(fs/2) ;
Fp2=5000/(fs/2);
[b77,a77]=ellip(2, Rp ,Rs, [Fp,Fp2], 'stop');  #bandstop */
const double iir_mx_Coeff_4tap_48_bdstop[9] = {
     /*B0*/       /*B1/B0*/         /*B2/B0*/           /*B3/B0*/       /*B4/B0*/
   0.57248 , -2.09385/0.57248,   3.05949/0.57248,  -2.09385/0.57248,   0.57248/0.57248
, /* A1 */       /* A2 */         /* A3 */           /* A4 */
 3.22514  ,-4.23148  , 2.69013,  -0.70747
 };

/*
Rp=.01;
Rs=60;
Fp=2000/(fs/2) ;
[b77,a77]=ellip(4, Rp ,Rs, Fp, 'low');
*/
const double iir_mx_Coeff_4tap_48_lp[9] = {
     /*B0*/        /*B1/B0*/                  /*B2/B0*/                 /*B3/B0*/                  /*B4/B0*/
    2.9534e-003 , -5.3572e-004/2.9534e-003,  3.9535e-003/2.9534e-003 , -5.3572e-004/2.9534e-003  ,2.9534e-003/2.9534e-003,
     /* A1 */   /* A2 */  /* A3 */    /* A4 */
    3.21618  ,-3.98701 ,  2.24532 , -0.48330
 };

/*
Rp=3;
Rs=60;
Fp=2000/(fs/2) ;
Fp2=5000/(fs/2);
[b78,a78]=ellip(2, Rp ,Rs, [Fp,Fp2], 'pass'); */
const double iir_mx_Coeff_4tap_48_bdpass[9] = {
     /*B0*/       /*B1/B0*/             /*B2/B0*/                /*B3/B0*/            /*B4/B0*/
    0.0180327 , -0.0031635/0.0180327,  -0.0297130/0.0180327 , -0.0031635/0.0180327 ,  0.0180327/0.0180327,
     /* A1 */   /* A2 */   /* A3 */    /* A4 */
    3.36631  ,-4.57415   ,2.96078 , -0.77827
 };




const double *iir_mx_Coeff0[] = {
  iir_mx_Coeff_1tap_48_fc3k_hp   /*ch1*/
};
const double *iir_mx_Coeff1[] = {
  iir_mx_Coeff_2tap_48_hp,       /*ch1*/
};
const double *iir_mx_Coeff2[] = {
  iir_mx_Coeff_2tap_48_lp     ,  /*ch1*/
};
const double *iir_mx_Coeff3[] = {
  iir_mx_Coeff_2tap_48_bdpass,  /*ch1*/
};
const double *iir_mx_Coeff4[] = {
  iir_mx_Coeff_2tap_48_lp,  /*ch1*/
  iir_mx_Coeff_2tap_48_hp,  /*ch2*/
};
const double *iir_mx_Coeff5[] = {
  iir_mx_Coeff_2tap_48_hp,  /*ch1*/
};
const double *iir_mx_Coeff6[] = {
  iir_mx_Coeff_2tap_48_lp,  /*ch1*/
};
const double *iir_mx_Coeff7[] = {
  iir_mx_Coeff_2tap_48_hp,  /*ch1*/
};
const double *iir_mx_Coeff8[] = {
  iir_mx_Coeff_2tap_48_lp   /*ch1*/
};
const double *iir_mx_Coeff9[] = {
  iir_mx_Coeff_2tap_48_bdpass,  /*ch1*/
  iir_mx_Coeff_2tap_48_lp,  /*ch2*/
  iir_mx_Coeff_2tap_48_hp   /*ch3*/
};
const double *iir_mx_Coeff10[] = {
  iir_mx_Coeff_4tap_48_bdpass,  /*ch1*/
};
const double *iir_mx_Coeff11[] = {
  iir_mx_Coeff_4tap_48_bdstop,  /*ch1*/
  iir_mx_Coeff_4tap_48_lp,  /*ch2*/
  iir_mx_Coeff_4tap_48_bdpass   /*ch3*/
};

const double **iir_mx_coeff[] = {
    /*filter 0 coeff*/
    iir_mx_Coeff0,
    /*filter 1 coeff*/
    iir_mx_Coeff1,
    /*filter 2 coeff*/
    iir_mx_Coeff2,
    /*filter 3 coeff*/
    iir_mx_Coeff3,
    /*filter 4 coeff*/
    iir_mx_Coeff4,
    /*filter 5 coeff*/
    iir_mx_Coeff5,
    /*filter 6 coeff*/
    iir_mx_Coeff6,
    /*filter 7 coeff*/
    iir_mx_Coeff7,
    /*filter 8 coeff*/
    iir_mx_Coeff8,
    /*filter 9 coeff*/
    iir_mx_Coeff9,
    /*filter 10 coeff*/
    iir_mx_Coeff10,
    /*filter 11 coeff*/
    iir_mx_Coeff11
};


/** Coefficients for IIR CAS filter design */
//************************************************************
//                      2 TAP IIR CAS Filter
//************************************************************
/*
Rp=6;
Rs=50;
Fp=10000/(fs/2);
[b5,a5]=ellip(2, Rp ,Rs, Fp, 'high');  2x*/
 const Float iir_cas_Coeff_2tap_c2_48_hp[1 + (2*4)] = {
   /*gain = B0_1*B0_2*/       /*B1_1/B0_1*/        /*B2_1/B0_1*/
   0.19914*0.19914       ,  -0.39322/0.19914 ,  0.19914/0.19914,
                              /*A1_1*/             /*A2_1*/
                            -0.010641        ,  -0.589888,
                              /*B1_2/B0_2*/        /*B1_2/B0_2*/
                            -0.39322/0.19914 ,  0.19914/0.19914,
                              /*A1_2*/             /*A2_2*/
                            -0.010641        ,  -0.589888

 };

 
/*
Rp=3;
Rs=50;
Fp=5000/(fs/2) ;
[b88,a88]=ellip(2, Rp ,Rs, Fp, 'low');
*/
/*
Rp=3;
Rs=50;
Fp=1500/(fs/2) ;
[b88,a88]=ellip(2, Rp ,Rs, Fp, 'high');
*/

 const Float iir_cas_Coeff_2tap_c2_48_lp_hp[1 + (2*4)] = {
   /*gain = B0_1*B0_2*/         /*B1_1/B0_1*/              /*B2_1/B0_1*/
     0.046988  * 0.64194   ,    0.084247/  0.046988 , 0.046988/  0.046988,
                                /*A1_1*/               /*A2_1*/
                                1.41228             , -0.66403,
                                /*B1_2/B0_2*/          /*B1_2/B0_2*/
                                -1.28371/ 0.64194   , 0.64194/0.64194,
                                /*A1_2*/               /*A2_2*/
                                1.78864             ,  -0.83818

 };

/*
Rp=6;
Rs=50;
Fp=10000/(fs/2);
[b5,a5]=ellip(2, Rp ,Rs, Fp, 'high');  3x*/
 const Float iir_cas_Coeff_2tap_c3_48[1 + (3*4)] = {
     /*gain = B0_1*B0_2*/       /*B1_1/B0_1*/       /*B2_1/B0_1*/
   0.19914*0.19914*0.19914  , -0.39322/0.19914  ,  0.19914/0.19914,
                                /*A1_1*/            /*A2_1*/
                              -0.010641         , -0.589888,
                                /*B1_2/B0_2*/       /*B1_2/B0_2*/
                              -0.39322/0.19914  ,  0.19914/0.19914,
                                /*A1_2*/            /*A2_2*/
                              -0.010641         ,- 0.589888,
                                /*B1_3/B0_2*/       /*B1_3/B0_3*/
                              -0.39322/0.19914  ,  0.19914/0.19914,
                                /*A1_3*/            /*A2_3*/
                              -0.010641         ,- 0.589888

 };

/*
Rp=3;
Rs=50;
Fp=5000/(fs/2) ;
[b88,a88]=ellip(2, Rp ,Rs, Fp, 'low'); x3*/
/*
Rp=3;
Rs=50;
Fp=1500/(fs/2) ;
[b88,a88]=ellip(2, Rp ,Rs, Fp, 'high');
*/

const Float iir_cas_Coeff_2tap_c4_48[1 + (4*4)] = {
   /*gain = B0_1*B0_2*/                             /*B1_1/B0_1*/            /*B2_1/B0_1*/
     0.046988  * 0.046988  * 0.046988 * 0.64194  ,  0.084247/  0.046988  ,  0.046988/  0.046988,
                                                    /*A1_1*/                 /*A2_1*/
                                                    1.41228              ,  -0.66403,
                                                    /*B1_2/B0_2*/            /*B1_2/B0_2*/
                                                    0.084247/  0.046988  ,  0.046988/  0.046988,
                                                    /*A1_2*/                 /*A2_2*/
                                                    1.41228              ,  -0.66403,
                                                    /*B1_3/B0_3*/            /*B1_3/B0_3*/
                                                    0.084247/  0.046988  ,  0.046988/  0.046988,
                                                    /*A1_3*/                 /*A2_3*/
                                                    1.41228              ,  -0.66403,
                                                    /*B1_4/B0_4*/            /*B1_4/B0_4*/
                                                    -1.28371/ 0.64194    ,   0.64194/0.64194,
                                                    /*A1_4*/                 /*A2_4*/
                                                    1.78864              ,   -0.83818
 };

/*
Rp=3;
Rs=50;
Fp=5000/(fs/2) ;
[b88,a88]=ellip(2, Rp ,Rs, Fp, 'low'); x3*/
/*
Rp=3;
Rs=50;
Fp=1500/(fs/2) ;
[b88,a88]=ellip(2, Rp ,Rs, Fp, 'high');
*/

const Float iir_cas_2tap_c4_ng_48_gain = (0.046988  * 0.046988  * 0.046988 * 0.64194);   // b0_1 * b0_2 * b0_3 * b0_4 ....
const Float iir_cas_Coeff_2tap_c4_ng_48[1 + (4*4)] = {
    /*B1_1/B0_1*/                /*B2_1/B0_1*/
    0.084247/0.046988       ,  0.046988/0.046988,
    /*A1_1*/                     /*A2_1*/
    1.41228                 ,  -0.66403,
    /*B1_2/B0_2*/                /*B1_2/B0_2*/
    0.084247/0.046988       ,  0.046988/0.046988,
    /*A1_2*/                     /*A2_2*/
    1.41228                 ,  -0.66403,
    /*B1_3/B0_3*/                /*B1_3/B0_3*/
    0.084247/0.046988       ,  0.046988/0.046988,
    /*A1_3*/                     /*A2_3*/
    1.41228                 ,  -0.66403,
    /*B1_4/B0_4*/                /*B1_4/B0_4*/
    -1.28371/0.64194        ,  0.64194/0.64194,
    /*A1_4*/                     /*A2_4*/
    1.78864                 ,  -0.83818
 };

/*
Rp=6;
Rs=50;
Fp=10000/(fs/2);
[b5,a5]=ellip(2, Rp ,Rs, Fp, 'high');  5x*/
 const Float iir_cas_Coeff_2tap_c5_48[1 + (5*4)] = {
  /*gain = B0_1*B0_2*/                          /*B1_1/B0_1*/       /*B2_1/B0_1*/
   0.19914*0.19914*0.19914*0.19914 *0.19914  ,  -0.39322/0.19914 ,  0.19914/0.19914,
                                                /*A1_1*/            /*A2_1*/
                                                -0.010641        , -0.589888,
                                                /*B1_2/B0_2*/       /*B1_2/B0_2*/
                                                -0.39322/0.19914 ,  0.19914/0.19914,
                                                /*A1_2*/            /*A2_2*/
                                                -0.010641        , -0.589888,
                                                /*B1_3/B0_3*/       /*B1_2/B0_3*/
                                                -0.39322/0.19914 ,  0.19914/0.19914,
                                                /*A1_3*/             /*A2_3*/
                                                -0.010641        , -0.589888,
                                                /*B1_4/B0_4*/       /*B1_4/B0_4*/
                                                -0.39322/0.19914 ,  0.19914/0.19914,
                                                /*A1_4*/            /*A2_4*/
                                                -0.010641        , -0.589888,
                                                /*B1_5/B0_5*/       /*B1_5/B0_5*/
                                                -0.39322/0.19914 ,  0.19914/0.19914,
                                                /*A1_5*/            /*A2_5*/
                                                -0.010641        , -0.589888
 };

/*
Rp=6;
Rs=50;
Fp=10000/(fs/2);
[b5,a5]=ellip(2, Rp ,Rs, Fp, 'high');  5x*/

const Float iir_cas_2tap_c5_ng_48_gain = (0.19914 *0.19914 *0.19914 *0.19914 *0.19914);   // b0_1 * b0_2 * b0_3 * b0_4 ....
const Float iir_cas_Coeff_2tap_c5_ng_48[1 + (5*4)] = {
    /*B1_1/B0_1*/           /*B2_1/B0_1*/
    -0.39322/0.19914    ,  0.19914/0.19914,
    /*A1_1*/                /*A2_1*/
    -0.010641           ,  -0.589888,
    /*B1_2/B0_2*/           /*B1_2/B0_2*/
    -0.39322/0.19914    ,  0.19914/0.19914,
    /*A1_2*/                /*A2_2*/
    -0.010641           ,  -0.589888,
    /*B1_3/B0_3*/           /*B1_3/B0_3*/
    -0.39322/0.19914    ,  0.19914/0.19914,
    /*A1_3*/                /*A2_3*/
    -0.010641           ,  -0.589888,
    /*B1_4/B0_4*/           /*B1_4/B0_4*/
    -0.39322/0.19914    ,  0.19914/0.19914,
    /*A1_4*/                /*A2_4*/
    -0.010641           ,  -0.589888,
    /*B1_5/B0_5*/           /*B1_5/B0_5*/
    -0.39322/0.19914    ,  0.19914/0.19914,
    /*A1_5*/                /*A2_5*/
    -0.010641           ,  -0.589888,
 };

/*
Rp=6;
Rs=50;
Fp=10000/(fs/2);
[b5,a5]=ellip(2, Rp ,Rs, Fp, 'high');  6x*/
const Float iir_cas_Coeff_2tap_c6_48[1 + (6*4)] = {
   /*gain = B0_1*B0_2....*/                                 /*B1_1/B0_1*/           /*B2_1/B0_1*/
   0.19914*0.19914*0.19914*0.19914*0.19914 *0.19914     ,   -0.39322/0.19914    ,  0.19914/0.19914,
                                                            /*A1_1*/                /*A2_1*/
                                                            -0.010641           , -0.589888,
                                                            /*B1_2/B0_2*/           /*B1_2/B0_2*/
                                                            -0.39322/0.19914    ,  0.19914/0.19914,
                                                            /*A1_2*/                /*A2_2*/
                                                            -0.010641           ,  -0.589888,
                                                            /*B1_3/B0_3*/           /*B1_3/B0_3*/
                                                            -0.39322/0.19914    ,  0.19914/0.19914,
                                                            /*A1_3*/                /*A2_3*/
                                                            -0.010641           ,  -0.589888,
                                                            /*B1_4/B0_4*/           /*B1_4/B0_4*/
                                                            -0.39322/0.19914    ,  0.19914/0.19914,
                                                            /*A1_4*/                /*A2_4*/
                                                            -0.010641           ,  -0.589888,
                                                            /*B1_5/B0_5*/           /*B1_5/B0_5*/
                                                            -0.39322/0.19914    ,  0.19914/0.19914,
                                                            /*A1_5*/                /*A2_5*/
                                                            -0.010641           ,  -0.589888,
                                                            /*B1_6/B0_6*/           /*B1_6/B0_6*/
                                                            -0.39322/0.19914    ,  0.19914/0.19914,
                                                            /*A1_6*/                /*A2_6*/
                                                            -0.010641           ,  -0.589888
 };


const Float *iir_cas_Coeff0[] = {
  iir_cas_Coeff_2tap_c2_48_lp_hp  /*ch1*/
};
const  Float *iir_cas_Coeff1[] = {
  iir_cas_Coeff_2tap_c3_48,  /*ch1*/
};
const Float *iir_cas_Coeff2[] = {
  iir_cas_Coeff_2tap_c4_48   /*ch1*/
};
const  Float *iir_cas_Coeff3[] = {
  iir_cas_Coeff_2tap_c4_ng_48,  /*ch1*/
};
const Float *iir_cas_Coeff4[] = {
  iir_cas_Coeff_2tap_c5_48   /*ch1*/
};
const  Float *iir_cas_Coeff5[] = {
  iir_cas_Coeff_2tap_c6_48,  /*ch1*/
};
const  Float *iir_cas_Coeff6[] = {
  iir_cas_Coeff_2tap_c6_48,  /*ch1*/
  iir_cas_Coeff_2tap_c6_48,  /*ch2*/
  iir_cas_Coeff_2tap_c6_48,  /*ch3*/
};
const  Float *iir_cas_Coeff7[] = {
  iir_cas_Coeff_2tap_c2_48_lp_hp,  /*ch1*/
  iir_cas_Coeff_2tap_c2_48_lp_hp,  /*ch2*/
};
const  Float *iir_cas_Coeff8[] = {
  iir_cas_Coeff_2tap_c5_ng_48,  /*ch1*/
};

const Float **iir_cas_coeff[] = {
    /*filter 0 coeff*/
    iir_cas_Coeff0,
    /*filter 1 coeff*/
    iir_cas_Coeff1,
    /*filter 2 coeff*/
    iir_cas_Coeff2,
    /*filter 3 coeff*/
    iir_cas_Coeff3,
    /*filter 4 coeff*/
    iir_cas_Coeff4,
    /*filter 5 coeff*/
    iir_cas_Coeff5,
    /*filter 6 coeff*/
    iir_cas_Coeff6,
    /*filter 7 coeff*/
    iir_cas_Coeff7,
    /*filter 8 coeff*/
    iir_cas_Coeff8,

};
