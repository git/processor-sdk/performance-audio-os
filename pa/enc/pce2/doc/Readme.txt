Test applicaiton 
----------------
Build
-----
- Modify the paths in \pa\enc\pce2\test\launch_ccs.bat file
- launch \pa\enc\pce2\test\launch_ccs.bat
- import CCS project from \pa\enc\pce2\test\testapp_ccs



Profiling
---------
One can modify "CFG_STEREO" and "TEST_CASE" macro in \pa\enc\pce2\test\src\pce2test.c,
test application source code to switch between test cases used for profiling.

    #define CFG_STEREO 0            // skeaper config control
    // Test case control
    // Value   Vol Del Output
    //   1 -   1    1    1
    //   2 -   1    1    0
    //   3 -   0    1    0
    #define TEST_CASE 1