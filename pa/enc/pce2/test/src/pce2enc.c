/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/


/*******************************************************************************
*                             INCLUDE FILES
*******************************************************************************/
#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include "ipce.h"
//#include "pce.h"
#include "pce2enc.h"


/** 
********************************************************************************
*  @fn     h264vdec.c
*  @brief  Creates an H264VDEC instance object (using parameters specified 
*          by prms)
*          
*  @param[in] fxns    : Function pointer
*  @param[in] prms    : Parameters used for the object creation
*
*  @return   IALG_EFAIL or IALG_EOK
********************************************************************************
*/
PCE_Handle PCE_create(
    const IPCE_Fxns *fxns,
    const PCE_Params *params,
    PAF_IALG_Config *pafConfig)
{
  return ((PCE_Handle) PAF_ALG_create_((IALG_Fxns *) fxns, NULL, 
      (IALG_Params*) params, NULL, (PAF_IALG_Config*) pafConfig));
}

/**
********************************************************************************
*  @fn     H264VDEC_delete
*  @brief  Deletes H264VDEC instance object 
*          
*  @param[in] handle  : H264 Decoder Hanlde 
*
*  @return None
********************************************************************************
*/
Void PCE_delete(PCE_Handle handle)
{
  PAF_ALG_delete((IALG_Handle) handle);
}

/**
********************************************************************************
*  @fn     H264VDEC_init
*  @brief  H264VDEC init
*          
*  @return None
********************************************************************************
*/
Void PCE_init(Void)
{
}

/**
********************************************************************************
*  @fn     H264VDEC_exit
*  @brief  H264VDEC Exit
*          
*  @return None
********************************************************************************
*/
Void PCE_exit(Void)
{
}

/**
********************************************************************************
*  @fn     H264VDEC_control
*  @brief  H264VDEC control
*
*  @param[in] handle      : H264 Decoder Hanlde 
*  @param[in] cmd         : Command 
*  @param[in/out] params  : Parameters 
*  @param[in/out] status  : Status
*          
*  @return IVIDDEC3_EOK or IVIDDEC3_EFAIL
********************************************************************************
*/
XDAS_Int32 PCE_control(PCE_Handle handle,
                            PCE_Cmd cmd,
                            PCE_Status * status
                           )
{
    XDAS_Int32  error = 0;

    error = handle->fxns->ialg.algControl((IALG_Handle) handle,
                                         (IALG_Cmd)cmd,
                                         (IALG_Status *) status
                                        );

  return error;
}

/**
********************************************************************************
*  @fn     H264VDEC_decodeFrame
*  @brief  This function decodes one frame
*
*  @param[in] handle     : H264 Decoder Hanlde 
*  @param[in] inBufs     : input buffer
*  @param[in] outBufs    : output buffer 
*  @param[in] inargs     : input arguments
*  @param[in] outargs    : output Arguments
*          
*  @return IVIDDEC3_EOK or IVIDDEC3_EFAIL
********************************************************************************
*/
XDAS_Int32 PCE_encodeFrame(IPCE_Handle PCEHandle,  
                           PAF_EncodeInStruct * inStruct,
 						   PAF_EncodeOutStruct * outStruct)
{
  int error;

  error = PCEHandle->fxns->encode(PCEHandle, NULL, inStruct, outStruct);

  return (error);
}

/*
 *  ======== PCE_info ========
 * 
 */
XDAS_Int32 PCE_Info(IPCE_Handle PCE_Handle, 
                    PAF_EncodeControl * Control, PAF_EncodeStatus * Status)
{
	UInt error;

    error = PCE_Handle->fxns->info(PCE_Handle, NULL, Control ,Status);
    return (error);
}
/*
 *  ======== PCE_reset ========
 * 
 */
XDAS_Int32 PCE_Reset(IPCE_Handle PCE_Handle,
                     PAF_EncodeControl * Control, PAF_EncodeStatus * Status)
{
	UInt error;
	error = PCE_Handle->fxns->reset(PCE_Handle, NULL, Control ,Status);
	return (error);
}



/*#if 0
XDAS_Int32 PCE_volumeInit(IPCE_Handle, 
                          IPCE_StatusPhase *,
                          IPCE_ConfigPhase *,
                          PAF_IALG_Common *,
                          PAF_ActivePhase *,
                          PAF_ScrachPhase *,
                          IPCE_ConfigPhase *,
                          PAF_IALG_Common *,
                          PAF_ActivePhase *,
                          PAF_ScrachPhase *)
{
  int error;

  error = handle->fxns->phaseInit[0](IPCE_Handle, IPCE_StatusPhase *,
                                     IPCE_ConfigPhase *,
                                     PAF_IALG_Common *,
                                     PAF_ActivePhase *,
                                     PAF_ScrachPhase *,
                                     IPCE_ConfigPhase *,
                                     PAF_IALG_Common *,
                                     PAF_ActivePhase *,
                                     PAF_ScrachPhase *);

  return (error);
}

XDAS_Int32 PCE_volumeReset(IPCE_Handle,
                          IPCE_StatusPhase *,
                          IPCE_ConfigPhase *,
                          PAF_IALG_Common *,
                          PAF_ActivePhase *,
                          PAF_ScrachPhase *,
                          IPCE_ConfigPhase *,
                          PAF_IALG_Common *,
                          PAF_ActivePhase *,
                          PAF_ScrachPhase *)
{
  int error;

  error = handle->fxns->phaseReset[0](IPCE_Handle,
                                     IPCE_StatusPhase *,
                                     IPCE_ConfigPhase *,
                                     PAF_IALG_Common *,
                                     PAF_ActivePhase *,
                                     PAF_ScrachPhase *,
                                     IPCE_ConfigPhase *,
                                     PAF_IALG_Common *,
                                     PAF_ActivePhase *,
                                     PAF_ScrachPhase *);

  return (error);
}

XDAS_Int32 PCE_volumeInfo(IPCE_Handle, 
                          PAF_EncodeControl *,
                          PAF_EncodeStatus *,
                          PAF_ActivePhase *,
                          PAF_ScrachPhase *)
{
  int error;

error = handle->fxns->phaseInfo[0](IPCE_Handle,
                                   PAF_EncodeControl *,
                                   PAF_EncodeStatus *,
                                   PAF_ActivePhase *,
                                   PAF_ScrachPhase *); 

  return (error);
}

XDAS_Int32 PCE_volume(IPCE_Handle,
                      PAF_EncodeInStruct *,
                      PAF_EncodeOutStruct *,
                      IPCE_StatusPhase *,
                      IPCE_ConfigPhase *,
                      PAF_IALG_Common *,
                      PAF_ActivePhase *,
                      PAF_ScrachPhase *,
                      PAF_ChannelMask_HD)
{
  int error;

error = handle->fxns->phase[0](IPCE_Handle,
                               PAF_EncodeInStruct *,
                               PAF_EncodeOutStruct *,
                               IPCE_StatusPhase *,
                               IPCE_ConfigPhase *,
                               PAF_IALG_Common *,
                               PAF_ActivePhase *,
                               PAF_ScrachPhase *,
                               PAF_ChannelMask_HD); 

  return (error);
}
#endif*/
