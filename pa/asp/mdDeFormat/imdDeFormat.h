/*
* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  This header defines all types, constants, and functions shared by all
 *  implementations of the algorithm.
 */
#ifndef IMDDEFORMAT_
#define IMDDEFORMAT_

#include <ialg.h>
#include <xdas.h>

#include "icom.h"
#include "paftyp.h"

#define MDDEFORMAT_APP_MEMTAB_NO    ( 1 ) /* number of memTabs configurable from application */
#define MDDEFORMAT_MEMTAB_NO        ( MDDEFORMAT_APP_MEMTAB_NO + 1 )   /* total number of memTabs */

#define MDDEFORMAT_MAXNUMCHANS      ( PAF_MAXNUMCHAN_HD )   /* max M */
#define MDDEFORMAT_MAXNUMSAMPS      ( 256 )                 /* max N */

/* Contains info for memTabs configurable from application */
typedef struct IMDDEFORMAT_memRec {
    IALG_MemSpace   space;      /* allocation space   */
    IALG_MemAttrs   attrs;      /* memory attributes  */
} IMDDEFORMAT_memRec;

/*
 *  ======== IMDDEFORMAT_Obj ========
 *  Every implementation of IMDDEFORMAT *must* declare this structure as
 *  the first member of the implementation's object.
 */
typedef struct IMDDEFORMAT_Obj {
    struct IMDDEFORMAT_Fxns *fxns;    /* function list: standard, public, private */
} IMDDEFORMAT_Obj;

/*
 *  ======== IMDDEFORMAT_Handle ========
 *  This type is a pointer to an implementation's instance object.
 */
typedef struct IMDDEFORMAT_Obj *IMDDEFORMAT_Handle;

/*
 *  ======== IMDDEFORMAT_Status ========
 *  This Status structure defines the parameters that can be changed or read
 *  during real-time operation of the algorithm.  This structure is actually
 *  instantiated and initialized in imdDeFormat.c.
 */
typedef struct IMDDEFORMAT_Status {
    Int size;               /* This value must always be here, and must be set to the
                               total size of this structure in 8-bit bytes, as the
                               sizeof() operator would do. */
    XDAS_Int8 mode;         /* enable/disable */

    // public variables
    XDAS_Int8 unused[3];
    XDAS_Int16 syncMarkerRxIdx;     /* index of sync marker in Rx audio frame */
    XDAS_Int16 rxPhase;             /* Rx phase of sample 0 in audio frame */
    XDAS_Int16 phase0Idx;           /* index of phase 0 in Rx audio frame */
    XDAS_Bool syncd;                /* indicates whether synchronization achieved */
    XDAS_UInt8 rxFrameCount;        /* used to check whether 2 frames received */

    XDAS_UInt32 syncCnt;            /* sync count */
} IMDDEFORMAT_Status;

/*
 *  ======== IMDDEFORMAT_Config ========
 *  Config structure defines the parameters that cannot be changed or read
 *  during real-time operation of the algorithm.
 */
typedef struct IMDDEFORMAT_Config {
    Int size;

    //  "init-time" controllable parameters
    XDAS_Int16 maxNumChs;       /* max M */
    XDAS_Int16 maxNumSamps;     /* max N */
    IMDDEFORMAT_memRec memRec[MDDEFORMAT_APP_MEMTAB_NO]; /* memTab config */
} IMDDEFORMAT_Config;

/*
 *  ======== IMDDEFORMAT_Params ========
 *  This structure defines the parameters necessary to create an
 *  instance of a MDDEFORMAT object.
 *
 *  Every implementation of IMDDEFORMAT *must* declare this structure as
 *  the first member of the implementation's parameter structure.  This structure is actually
 *  instantiated and initialized in imdDeFormat.c.
 */
typedef struct IMDDEFORMAT_Params {
    Int size;
    const IMDDEFORMAT_Status *pStatus;
    const IMDDEFORMAT_Config *pConfig;
} IMDDEFORMAT_Params;

/*
 *  ======== IMDDEFORMAT_PARAMS ========
 *  Default instance creation parameters (defined in imdDeFormat.c)
 */

extern  const IMDDEFORMAT_Params IMDDEFORMAT_PARAMS;
/*
 *  ======== IMDDEFORMAT_Fxns ========
 *  All implementation's of MDDEFORMAT must declare and statically
 *  initialize a constant variable of this type.
 *
 */
typedef struct IMDDEFORMAT_Fxns {
    /* public */
    IALG_Fxns   ialg;
    Int  (*reset)(IMDDEFORMAT_Handle, PAF_AudioFrame *);
    Int  (*apply)(IMDDEFORMAT_Handle, PAF_AudioFrame *);

    /* private */
} IMDDEFORMAT_Fxns;

/*
 *  ======== IMDDEFORMAT_Cmd ========
 *  The Cmd enumeration defines the control commands for the MDDEFORMAT
 *  control method.
 */
typedef enum IMDDEFORMAT_Cmd {
    IMDDEFORMAT_NULL                    = ICOM_NULL,
    IMDDEFORMAT_GETSTATUSADDRESS1       = ICOM_GETSTATUSADDRESS1,
    IMDDEFORMAT_GETSTATUSADDRESS2       = ICOM_GETSTATUSADDRESS2,
    IMDDEFORMAT_GETSTATUS               = ICOM_GETSTATUS,
    IMDDEFORMAT_SETSTATUS               = ICOM_SETSTATUS
} IMDDEFORMAT_Cmd;

#endif  /* IMDDEFORMAT_ */
