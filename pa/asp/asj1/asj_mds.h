
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) Audio Stream Join Algorithm interface declarations
//
//
//

/*
 *  Vendor specific (MDS) interface header for Audio Stream Join Algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular ASJ implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef ASJ_MDS_
#define ASJ_MDS_

#include <ialg.h>

#include "iasj.h"
#include "icom.h"

/*
 *  ======== ASJ_MDS_exit ========
 *  Required module finalization function
 */
#define ASJ_MDS_exit COM_TII_exit

/*
 *  ======== ASJ_MDS_init ========
 *  Required module initialization function
 */
#define ASJ_MDS_init COM_TII_init

/*
 *  ======== ASJ_MDS_IALG ========
 *  MDS's implementation of ASJ's IALG interface
 */
extern IALG_Fxns ASJ_MDS_IALG; 

/*
 *  ======== ASJ_MDS_IASJ ========
 *  MDS's implementation of ASJ's IASJ interface
 */
extern const IASJ_Fxns ASJ_MDS_IASJ; 

/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of MDS's Audio Stream Join Algorithm.
 *  However, other implementation specific operations can also
 *  be added.
 */

/*
 *  ======== ASJ_MDS_Handle ========
 */
typedef struct ASJ_MDS_Obj *ASJ_MDS_Handle;

/*
 *  ======== ASJ_MDS_Params ========
 *  We don't add any new parameters to the standard ones defined by IASJ.
 */
typedef IASJ_Params ASJ_MDS_Params;

/*
 *  ======== ASJ_MDS_Status ========
 *  We don't add any new status to the standard one defined by IASJ.
 */
typedef IASJ_Status ASJ_MDS_Status;

/*
 *  ======== ASJ_MDS_Config ========
 *  We don't add any new config to the standard one defined by IASJ.
 */
typedef IASJ_Config ASJ_MDS_Config;

/*
 *  ======== ASJ_MDS_PARAMS ========
 *  Define our default parameters.
 */
#define ASJ_MDS_PARAMS   IASJ_PARAMS

/*
 *  ======== ASJ_MDS_create ========
 *  Create a ASJ_MDS instance object.
 */
extern ASJ_MDS_Handle ASJ_MDS_create(const ASJ_MDS_Params *params);

/*
 *  ======== ASJ_MDS_delete ========
 *  Delete a ASJ_MDS instance object.
 */
#define ASJ_MDS_delete COM_TII_delete

#endif  /* ASJ_MDS_ */
