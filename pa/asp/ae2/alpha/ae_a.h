
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _AE_A
#define _AE_A

#define  readAEMode 0xf200,0x0400
#define writeAEModeDisable 0xfa00,0x0400
#define writeAEModeEnable 0xfa00,0x0401

#define  readAESpare 0xf200,0x0500
#define writeAESpare(N) 0xfa00,0x0500+((N)&0xff)

#define  readAERoomSize 0xf300,0x0006
#define writeAERoomSize(N) 0xfb00,0x0006,N
#define wroteAERoomSize 0xfb00,0x0006

#define  readAEDamping 0xf300,0x0008
#define writeAEDamping(N) 0xfb00,0x0008,N
#define wroteAEDamping 0xfb00,0x0008

#define  readAEPreScale 0xf300,0x000a
#define writeAEPreScale(N) 0xfb00,0x000a,N
#define wroteAEPreScale 0xfb00,0x000a

#define  readAEWetMix 0xf300,0x000c
#define writeAEWetMix(N) 0xfb00,0x000c,N
#define wroteAEWetMix 0xfb00,0x000c

#define  readAEXwetMix 0xf300,0x000e
#define writeAEXwetMix(N) 0xfb00,0x000e,N
#define wroteAEXwetMix 0xfb00,0x000e

#define  readAEDryMix 0xf300,0x0010
#define writeAEDryMix(N) 0xfb00,0x0010,N
#define wroteAEDryMix 0xfb00,0x0010

#define  readAESkipCombs 0xf200,0x1200
#define writeAESkipCombsEnable 0xfa00,0x1201
#define writeAESkipCombsDisable 0xfa00,0x1200

#define  readAESkipAllPass 0xf200,0x1400
#define writeAESkipAllPassEnable 0xfa00,0x1401
#define writeAESkipAllPassDisable 0xfa00,0x1400

#define  readAEStatus 0xf508,0x0000
#define  readAEControl readAEStatus

#endif /* _AE_A */
