
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Standard Speaker Location Delay algoritm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the Speaker Location Delay algoritm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the DEL
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef DEL_
#define DEL_

#include <ti/xdais/ialg.h>

#include <alg.h>
#include <paf_alg.h>

#include <idel.h>

#include <paftyp.h>

/*
 *  ======== DEL_Handle ========
 *  Speaker Location Delay algoritm instance handle
 */
typedef struct IDEL_Obj *DEL_Handle;

/*
 *  ======== DEL_Params ========
 *  Speaker Location Delay algoritm instance creation parameters
 */
typedef struct IDEL_Params DEL_Params;

/*
 *  ======== DEL_PARAMS ========
 *  Default instance parameters
 */
#define DEL_PARAMS IDEL_PARAMS

/*
 *  ======== DEL_Status ========
 *  Status structure for getting DEL instance attributes
 */
typedef volatile struct IDEL_Status DEL_Status;

/*
 *  ======== DEL_Cmd ========
 *  This typedef defines the control commands DEL objects
 */
typedef IDEL_Cmd   DEL_Cmd;

/*
 * ===== control method commands =====
 */
#define DEL_NULL IDEL_NULL
#define DEL_GETSTATUSADDRESS1 IDEL_GETSTATUSADDRESS1
#define DEL_GETSTATUSADDRESS2 IDEL_GETSTATUSADDRESS2
#define DEL_GETSTATUS IDEL_GETSTATUS
#define DEL_SETSTATUS IDEL_SETSTATUS

/*
 *  ======== DEL_create ========
 *  Create an instance of a DEL object.
 */
static inline DEL_Handle DEL_create(const IDEL_Fxns *fxns, const DEL_Params *prms)
{
    return ((DEL_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== DEL_delete ========
 *  Delete a DEL instance object
 */
static inline Void DEL_delete(DEL_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== DEL_apply ========
 */
extern Int DEL_apply(DEL_Handle, PAF_AudioFrame *);

/*
 *  ======== DEL_reset ========
 */
extern Int DEL_reset(DEL_Handle, PAF_AudioFrame *);

/*
 *  ======== DEL_exit ========
 *  Module finalization
 */
extern Void DEL_exit(Void);

/*
 *  ======== DEL_init ========
 *  Module initialization
 */
extern Void DEL_init(Void);

#endif  /* DEL_ */
