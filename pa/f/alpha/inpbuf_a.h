
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Input Buffer alpha codes
//
//
//

#ifndef _INPBUF_A
#define _INPBUF_A

#include <acpbeta.h>
#include <pafmac_a.h>
#include <paftyp_a.h>

#define  readIBMode                         0xc200+STD_BETA_IB,0x0400
#define writeIBModeN(NN)                    0xca00+STD_BETA_IB,(0x0400+(NN))

#define  readIBSampleRateOverride           0xc200+STD_BETA_IB,0x0800
#define writeIBSampleRateOverrideUnknown    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_UNKNOWN)
#define writeIBSampleRateOverrideNone       0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_NONE)
#define writeIBSampleRateOverride32000Hz    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_32000HZ)
#define writeIBSampleRateOverride44100Hz    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_44100HZ)
#define writeIBSampleRateOverride48000Hz    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_48000HZ)
#define writeIBSampleRateOverride88200Hz    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_88200HZ)
#define writeIBSampleRateOverride96000Hz    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_96000HZ)
#define writeIBSampleRateOverride192000Hz   0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_192000HZ)
#define writeIBSampleRateOverride64000Hz    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_64000HZ)
#define writeIBSampleRateOverride128000Hz   0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_128000HZ)
#define writeIBSampleRateOverride176400Hz   0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_176400HZ)
#define writeIBSampleRateOverride8000Hz     0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_8000HZ)
#define writeIBSampleRateOverride11025Hz    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_11025HZ)
#define writeIBSampleRateOverride12000Hz    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_12000HZ)
#define writeIBSampleRateOverride16000Hz    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_16000HZ)
#define writeIBSampleRateOverride22050Hz    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_22050HZ)
#define writeIBSampleRateOverride24000Hz    0xca00+STD_BETA_IB,(0x0800+PAF_SAMPLERATE_24000HZ)
#define writeIBSampleRateOverrideStandard   0xca00+STD_BETA_IB,0x0880
#define writeIBSampleRateOverrideData       0xca00+STD_BETA_IB,0x0881
#define writeIBSampleRateOverrideMeasured   0xca00+STD_BETA_IB,0x0882
#define writeIBSampleRateOverrideAuto       0xca00+STD_BETA_IB,0x0883
#define writeIBSampleRateOverrideOther      0xca00+STD_BETA_IB,0x0884

#define  readIBSampleRateData               0xc200+STD_BETA_IB,0x0900
#define wroteIBSampleRateDataUnknown        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_UNKNOWN
#define wroteIBSampleRateDataNone           0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_NONE
#define wroteIBSampleRateData32000Hz        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_32000HZ
#define wroteIBSampleRateData44100Hz        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_44100HZ
#define wroteIBSampleRateData48000Hz        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_48000HZ
#define wroteIBSampleRateData88200Hz        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_88200HZ
#define wroteIBSampleRateData96000Hz        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_96000HZ
#define wroteIBSampleRateData192000Hz       0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_192000HZ
#define wroteIBSampleRateData64000Hz        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_64000HZ
#define wroteIBSampleRateData128000Hz       0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_128000HZ
#define wroteIBSampleRateData176400Hz       0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_176400HZ
#define wroteIBSampleRateData8000Hz         0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_8000HZ
#define wroteIBSampleRateData11025Hz        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_11025HZ
#define wroteIBSampleRateData12000Hz        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_12000HZ
#define wroteIBSampleRateData16000Hz        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_16000HZ
#define wroteIBSampleRateData22050Hz        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_22050HZ
#define wroteIBSampleRateData24000Hz        0xca00+STD_BETA_IB,0x0900+PAF_SAMPLERATE_24000HZ

#define  readIBSampleRateMeasured           0xc200+STD_BETA_IB,0x0a00
#define wroteIBSampleRateMeasuredUnknown    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_UNKNOWN
#define wroteIBSampleRateMeasuredNone       0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_NONE
#define wroteIBSampleRateMeasured32000Hz    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_32000HZ
#define wroteIBSampleRateMeasured44100Hz    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_44100HZ
#define wroteIBSampleRateMeasured48000Hz    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_48000HZ
#define wroteIBSampleRateMeasured88200Hz    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_88200HZ
#define wroteIBSampleRateMeasured96000Hz    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_96000HZ
#define wroteIBSampleRateMeasured192000Hz   0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_192000HZ
#define wroteIBSampleRateMeasured64000Hz    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_64000HZ
#define wroteIBSampleRateMeasured128000Hz   0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_128000HZ
#define wroteIBSampleRateMeasured176400Hz   0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_176400HZ
#define wroteIBSampleRateMeasured8000Hz     0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_8000HZ
#define wroteIBSampleRateMeasured11025Hz    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_11025HZ
#define wroteIBSampleRateMeasured12000Hz    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_12000HZ
#define wroteIBSampleRateMeasured16000Hz    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_16000HZ
#define wroteIBSampleRateMeasured22050Hz    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_22050HZ
#define wroteIBSampleRateMeasured24000Hz    0xca00+STD_BETA_IB,0x0a00+PAF_SAMPLERATE_24000HZ

#define  readIBSampleRateStatus             0xc200+STD_BETA_IB,0x0b00
#define wroteIBSampleRateStatusUnknown      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_UNKNOWN
#define wroteIBSampleRateStatusNone         0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_NONE
#define wroteIBSampleRateStatus32000Hz      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_32000HZ
#define wroteIBSampleRateStatus44100Hz      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_44100HZ
#define wroteIBSampleRateStatus48000Hz      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_48000HZ
#define wroteIBSampleRateStatus88200Hz      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_88200HZ
#define wroteIBSampleRateStatus96000Hz      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_96000HZ
#define wroteIBSampleRateStatus192000Hz     0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_192000HZ
#define wroteIBSampleRateStatus64000Hz      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_64000HZ
#define wroteIBSampleRateStatus128000Hz     0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_128000HZ
#define wroteIBSampleRateStatus176400Hz     0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_176400HZ
#define wroteIBSampleRateStatus8000Hz       0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_8000HZ
#define wroteIBSampleRateStatus11025Hz      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_11025HZ
#define wroteIBSampleRateStatus12000Hz      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_12000HZ
#define wroteIBSampleRateStatus16000Hz      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_16000HZ
#define wroteIBSampleRateStatus22050Hz      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_22050HZ
#define wroteIBSampleRateStatus24000Hz      0xca00+STD_BETA_IB,0x0b00+PAF_SAMPLERATE_24000HZ

#define  readIBNonAudio                     0xc200+STD_BETA_IB,0x0c00

#define  readIBEmphasisData                 0xc200+STD_BETA_IB,0x0d00
#define  readIBEmphasis                     readIBEmphasisData
#define wroteIBEmphasisDataUnknown          0xca00+STD_BETA_IB,0x0d00+PAF_IEC_PREEMPHASIS_UNKNOWN
#define wroteIBEmphasisDataNo               0xca00+STD_BETA_IB,0x0d00+PAF_IEC_PREEMPHASIS_NO
#define wroteIBEmphasisDataYes              0xca00+STD_BETA_IB,0x0d00+PAF_IEC_PREEMPHASIS_YES

#define  readIBEmphasisOverride             0xc200+STD_BETA_IB,0x0e00
#define writeIBEmphasisOverrideDisable      0xca00+STD_BETA_IB,0x0e00
#define writeIBEmphasisOverrideNo           0xca00+STD_BETA_IB,0x0e80+PAF_IEC_PREEMPHASIS_NO
#define writeIBEmphasisOverrideYes          0xca00+STD_BETA_IB,0x0e80+PAF_IEC_PREEMPHASIS_YES

#define  readIBEmphasisStatus               0xc200+STD_BETA_IB,0x0f00
#define wroteIBEmphasisStatusNo             0xca00+STD_BETA_IB,0x0f00+PAF_IEC_PREEMPHASIS_NO
#define wroteIBEmphasisStatusYes            0xca00+STD_BETA_IB,0x0f00+PAF_IEC_PREEMPHASIS_YES

#define  readIBLock       0xc200+STD_BETA_IB,0x1000
#define wroteIBLockNo     0xca00+STD_BETA_IB,0x1000
#define wroteIBLockYes    0xca00+STD_BETA_IB,0x1001

#define  readIBLockOverride          0xc200+STD_BETA_IB,0x0600
#define writeIBLockOverrideNo        0xca00+STD_BETA_IB,0x0600
#define writeIBLockOverrideYes       0xca00+STD_BETA_IB,0x0601
#define writeIBLockOverrideNone      0xca00+STD_BETA_IB,0x0680

#define  readIBStatus 0xc508,STD_BETA_IB
#define  readIBControl \
         readIBMode, \
         readIBSampleRateOverride, \
         readIBEmphasisOverride

/* in support of inverse compilation only */
#define writeIBModeN__0__ writeIBModeN(0)
#define writeIBModeN__1__ writeIBModeN(1)

#define  readIBSioSelect 0xc200+STD_BETA_IB, 0x0500
#define writeIBSioSelectN(NN) 0xca00+STD_BETA_IB,(0x0500+(NN))

#define  readIBPrecisionDefault         0xc200+STD_BETA_IB,0x1400
#define writeIBPrecisionDefaultOriginal 0xca00+STD_BETA_IB,0x1410
#define writeIBPrecisionDefaultStandard 0xca00+STD_BETA_IB,0x1414
#define writeIBPrecisionDefaultFull     0xca00+STD_BETA_IB,0x1418

#define  readIBPrecisionDetect          0xc200+STD_BETA_IB,0x1500
#define wroteIBPrecisionDetectInvalid   0xca00+STD_BETA_IB,0x15fe /* Treat as None/TBD */
#define wroteIBPrecisionDetectTBD       0xca00+STD_BETA_IB,0x15ff /* Not yet implemented */
#define wroteIBPrecisionDetectNone      0xca00+STD_BETA_IB,0x1500 /* None or <= 20 */
#define wroteIBPrecisionDetect16        0xca00+STD_BETA_IB,0x1510
#define wroteIBPrecisionDetect17        0xca00+STD_BETA_IB,0x1511
#define wroteIBPrecisionDetect18        0xca00+STD_BETA_IB,0x1512
#define wroteIBPrecisionDetect19        0xca00+STD_BETA_IB,0x1513
#define wroteIBPrecisionDetect20        0xca00+STD_BETA_IB,0x1514
#define wroteIBPrecisionDetect21        0xca00+STD_BETA_IB,0x1515
#define wroteIBPrecisionDetect22        0xca00+STD_BETA_IB,0x1516
#define wroteIBPrecisionDetect23        0xca00+STD_BETA_IB,0x1517
#define wroteIBPrecisionDetect24        0xca00+STD_BETA_IB,0x1518 /* 24 or <= 24 */

#define  readIBPrecisionOverride        0xc200+STD_BETA_IB,0x1600
#define writeIBPrecisionOverrideDefault 0xca00+STD_BETA_IB,0x16ff
#define writeIBPrecisionOverrideDetect  0xca00+STD_BETA_IB,0x1600
#define writeIBPrecisionOverride16      0xca00+STD_BETA_IB,0x1610
#define writeIBPrecisionOverride17      0xca00+STD_BETA_IB,0x1611
#define writeIBPrecisionOverride18      0xca00+STD_BETA_IB,0x1612
#define writeIBPrecisionOverride19      0xca00+STD_BETA_IB,0x1613
#define writeIBPrecisionOverride20      0xca00+STD_BETA_IB,0x1614
#define writeIBPrecisionOverride21      0xca00+STD_BETA_IB,0x1615
#define writeIBPrecisionOverride22      0xca00+STD_BETA_IB,0x1616
#define writeIBPrecisionOverride23      0xca00+STD_BETA_IB,0x1617
#define writeIBPrecisionOverride24      0xca00+STD_BETA_IB,0x1618
#define writeIBPrecisionOverrideFloat   0xca00+STD_BETA_IB,0x1621

#define  readIBPrecisionInput           0xc200+STD_BETA_IB,0x1700
#define wroteIBPrecisionInputBitStream  0xca00+STD_BETA_IB,0x1700
#define wroteIBPrecisionInput16         0xca00+STD_BETA_IB,0x1710
#define wroteIBPrecisionInput17         0xca00+STD_BETA_IB,0x1711
#define wroteIBPrecisionInput18         0xca00+STD_BETA_IB,0x1712
#define wroteIBPrecisionInput19         0xca00+STD_BETA_IB,0x1713
#define wroteIBPrecisionInput20         0xca00+STD_BETA_IB,0x1714
#define wroteIBPrecisionInput21         0xca00+STD_BETA_IB,0x1715
#define wroteIBPrecisionInput22         0xca00+STD_BETA_IB,0x1716
#define wroteIBPrecisionInput23         0xca00+STD_BETA_IB,0x1717
#define wroteIBPrecisionInput24         0xca00+STD_BETA_IB,0x1718

#define  readIBZeroRunTrigger           0xc400+STD_BETA_IB,0x0018
#define writeIBZeroRunTriggerN(NN)      0xcc00+STD_BETA_IB,0x0018,LSW(NN),MSW(NN)

#define  readIBZeroRunRestart           0xc400+STD_BETA_IB,0x001C
#define writeIBZeroRunRestartN(NN)      0xcc00+STD_BETA_IB,0x001C,LSW(NN),MSW(NN)

#define  readIBUnknownTimeout           0xc400+STD_BETA_IB,0x0020
#define writeIBUnknownTimeoutN(NN)      0xcc00+STD_BETA_IB,0x0020,LSW(NN),MSW(NN)

#define  readIBScanAtHighSampleRateMode        0xc200+STD_BETA_IB,0x1100
#define writeIBScanAtHighSampleRateModeDisable 0xca00+STD_BETA_IB,0x1100
#define writeIBScanAtHighSampleRateModeEnable  0xca00+STD_BETA_IB,0x1101

#define   readIBZeroRun     0xc200+STD_BETA_IB,0x1200
#define  wroteIBZeroRunNo   0xca00+STD_BETA_IB,0x1200
#define  wroteIBZeroRunYes  0xca00+STD_BETA_IB,0x1201

#define   readIBRateTrackMode          0xc200+STD_BETA_IB,0x1300
#define  writeIBRateTrackDisable       0xca00+STD_BETA_IB,0x1300
#define  writeIBRateTrackEnable        0xca00+STD_BETA_IB,0x1301

#define   readIBreportDTS16AsDTSForLargeSampleRate         0xc200+STD_BETA_IB,0x2900
#define  writeIBreportDTS16AsDTSForLargeSampleRateDisable  0xca00+STD_BETA_IB,0x2900
#define  writeIBreportDTS16AsDTSForLargeSampleRateEnable   0xca00+STD_BETA_IB,0x2901

#define  readIBuseIECSubType           0xc200+STD_BETA_IB,0x2A00
#define  writeIBuseIECSubTypeDisable  0xca00+STD_BETA_IB,0x2A00
#define  writeIBuseIECSubTypeEnable    0xca00+STD_BETA_IB,0x2A01


#ifdef HSE
// DFI
#define  readDFIFileSelect             0xc300+STD_BETA_IB,0x0024
#define  writeDFIFileSelectN(NN)       0xcb00+STD_BETA_IB,0x0024,NN
#define  readDFIMediaSelect            0xc300+STD_BETA_IB,0x0026
#define  writeDFIMediaSelectN(NN)      0xcb00+STD_BETA_IB,0x0026,NN
#define  readDFIFileSize               0xc400+STD_BETA_IB,0x0028
#define  readDFIFilePosition           0xc400+STD_BETA_IB,0x002C
#define  readDFIPlayRate1              0xc300+STD_BETA_IB,0x0030
#define  readDFIPlayRate2              0xc300+STD_BETA_IB,0x0032

#endif //HSE

#define   readIBLastFrameMask            0xc400+STD_BETA_IB,0x0024
#define  writeIBLastFrameMaskN(NN)       0xcc00+STD_BETA_IB,0x0024,LSW(NN),MSW(NN)

#endif /* _INPBUF_A */
