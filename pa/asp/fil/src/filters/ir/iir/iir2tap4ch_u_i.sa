*
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*
*
*  ======== iir1tap4ch_u_i.sa ========
*  IIR filter implementation for tap-2 & channels-4, unicoeff/inplace, SP.
*
 
************************ IIR FILTER *****************************************/
****************** ORDER 2    and CHANNELS 4 ********************************/
*                                                                           */
* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2                           */
*                           ----------------------                          */
*                            1  - a1*z~1 - a2*z~2                           */
*                                                                           */
*   Direct form    : y(n) = b0*x(n) + b1*x(n-1) + a1*y(n-1) + a2*y(n-2)     */
*                                                                           */
*   Canonical form : w(n) = x(n)    + a1*w(n-1) + a2*w(n-2)                 */
*                    y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                 */
*                                                                           */
*   Filter variables :                                                      */
*      y(n) - *y,         x(n)   - *x                                       */
*      w(n) -             w(n-1) - w1         w(n-2) - w2                   */
*      b0 - filtCfsB[0], b1 - filtCfsB[1], b2 - filtCfsB[2]                 */
*      a1 - filtCfsA[0], a2 - filtCfsA[1]                                   */
*                                                                           */
*                                                                           */
*                              w(n)    b0                                   */
*     x(n)-->---[+]---->--------@------>>-----[+]--->--y(n)                 */
*                |              |              |                            */
*                ^              v              ^                            */
*                |     a1    +-----+    b1     |                            */
*               [+]----<<----| Z~1 |---->>----[+]                           */
*                |           +-----+           |                            */
*                |              |              |                            */
*                ^              v              ^                            */
*                |     a2    +-----+    b2     |                            */
*                 ----<<-----| Z~1 |---->>-----                             */
*                            +-----+                                        */
*                                                                           */
*****************************************************************************/ 
*Note:C equivalent of s-asm code lines are given as comments,for simplicity *

            .global _Filter_iirT2Ch4_ui ;Declared as Global function 
            .sect ".text:Filter_iirT2Ch4_ui" ;Memory section for the function             
_Filter_iirT2Ch4_ui: .cproc pParam ;Filter_iirT2Ch4_ui(*pParam), C callable fn

             .no_mdep ;no memory aliasing in this function
             
************* 1 *******************************
             .reg  x1 ;I/p ptr-O/p ptr   
             .reg  filtVars1 ;Var ptr 
             .reg  filtCfsB1, filtCfsA1 ;Coeff ptr, common to channels
             .reg  w1_1, w2_1 ;Filter states    
             .reg  accum1 ;O/p accumulator register  
             
************* 2 *******************************             
             .reg  x2 ;I/p ptr-O/p ptr 
             .reg  filtVars2 ;Var ptr 
             .reg  w1_2, w2_2 ;Filter states    
             .reg  accum2 ;O/p accumulator register  
             
************* 3 *******************************             
             .reg  x3 ;I/p ptr-O/p ptr 
             .reg  filtVars3 ;Var ptr 
             .reg  w1_3, w2_3 ;Filter states    
             .reg  accum3 ;O/p accumulator register  
             
************* 3 *******************************             
             .reg  x4 ;I/p ptr-O/p ptr 
             .reg  filtVars4 ;Var ptr 
             .reg  w1_4, w2_4 ;Filter states    
             .reg  accum4 ;O/p accumulator register              
             
********************************************             
             .reg  res1, res2, res3, res4, res5 ;Temp accumulators
             .reg  CfsB0_1,CfsB1_1,CfsB2_1,CfsA0_1,CfsA1_1 ;Coeff regs, common
                                     
             .reg  input_data ;I/p data reg
             
             .reg  pIn ;Ptr to, array of ch i/p  ptrs
             .reg  pCoef ;Ptr to, array of ch o/p  ptrs
             .reg  pVar ;Ptr to, array of ch var  ptrs
             .reg  count ;Sample counter
             
             LDW   *pParam[0],        pIn;pIn   = pParam[0]
             LDW   *pParam[2],        pCoef ;pCoef = pParam[2]
             LDW   *pParam[3],        pVar;pVar  = pParam[3]
             LDW   *pParam[4],        count;count = pParam[4]
*********************** X ptrs ******************
             LDW   *pIn[0],           x1 ;x1 = pIn[0]
             LDW   *pIn[1],           x2 ;x2 = pIn[1]
             LDW   *pIn[2],           x3 ;x3 = pIn[2]
             LDW   *pIn[3],           x4 ;x4 = pIn[3]
             
*********************** Feedfoward CFS ptrs ******************
             LDW   *pCoef[0],    filtCfsB1 ;filtCfsB1 = pCoef[0] /* Common */
             
*********************** Derive Feedback CFS ptrs ******************
             ADD  12, filtCfsB1,    filtCfsA1 ;filtCfsA1=filtCfsB1+8 /* Common */     

*********************** VAR ptrs ******************
             LDW   *pVar[0],   filtVars1 ;filtVars1 = pVar[0]  
             LDW   *pVar[1],   filtVars2 ;filtVars2 = pVar[1]        
             LDW   *pVar[2],   filtVars3 ;filtVars3 = pVar[2]
             LDW   *pVar[3],   filtVars4 ;filtVars4 = pVar[3]
                          
********************************************  
             ;Get filter states into regs
             LDW   *filtVars1[0],     w1_1 ;w1(n-1) = filtVars1[0] 
             LDW   *filtVars1[1],     w2_1 ;w1(n-2) = filtVars1[1]
                                          
             LDW   *filtVars2[0],     w1_2 ;w2(n-1) = filtVars2[0] 
             LDW   *filtVars2[1],     w2_2 ;w2(n-2) = filtVars2[1]
                                          
             LDW   *filtVars3[0],     w1_3 ;w3(n-1) = filtVars3[0] 
             LDW   *filtVars3[1],     w2_3 ;w3(n-2) = filtVars3[1]
                                          
             LDW   *filtVars4[0],     w1_4 ;w4(n-1) = filtVars4[0] 
             LDW   *filtVars4[1],     w2_4 ;w4(n-2) = filtVars4[1]

LOOP:
             ;Get the feedforward coeff ptr
             LDW   *pCoef[0],    filtCfsB1
             ;Get feedback coeffs             
             LDW   *filtCfsA1[0],      CfsA0_1
             LDW   *filtCfsA1[1],      CfsA1_1
             ;Get feedback coeffs
             LDW    *filtCfsB1[0],      CfsB0_1
             LDW    *filtCfsB1[1],      CfsB1_1   
             LDW    *filtCfsB1[2],      CfsB2_1   
                          
************* 1 *******************************             
             ;a1*w(n-1) & b1*w(n-1)             
             MPYSP  w1_1,               CfsA0_1,            res1 ;res1 =  w1_1*CfsA0_1
             MPYSP  w1_1,               CfsB1_1,            res3 ;res3 =  w1_1*CfsB1_1             
             
             ;Get one input sample & increment i/p ptr
             LDW    *x1,              input_data ;input_data = *x1++      

             ;a2*w(n-2) & b2*w(n-2)
             MPYSP  w2_1,               CfsA1_1,            res2 ;res2 = w2_1*CfsA1_1
             MPYSP  w2_1,               CfsB2_1,            res4 ;res4 = w2_1*CfsB2_1

             ;Shift filter states
             MV     w1_1,               w2_1 ;w2_1 = w1_1

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  input_data,         res2,               res2 ;res2 *= input_data
             ADDSP  res1,               res2,               w1_1 ;w1_1 = res1 + res2

             ;y(n) = b0*w(n) + 'b1*w(n-1)' + 'b2*w(n-2)'
             MPYSP  w1_1,               CfsB0_1,            res5    ;res5 = w1_1*CfsB0_1
             ADDSP  res3,               res4,               accum1  ;accum1 = res3 + res4
             ADDSP  accum1,             res5,                accum1 ;accum1 += res5
   
             ;Store the calculated o/p
             STW    accum1,            *x1++ ;*x1++ = accum1

************* 2 *******************************             
             ;a1*w(n-1) & b1*w(n-1)             
             MPYSP  w1_2,               CfsA0_1,            res1 ;res1 =  w1_2*CfsA0_1 
             MPYSP  w1_2,               CfsB1_1,            res3 ;res3 =  w1_2*CfsB1_1             
             
             ;Get one input sample & increment i/p ptr
             LDW    *x2,              input_data ;input_data = *x2++      

             ;a2*w(n-2) & b2*w(n-2)
             MPYSP  w2_2,               CfsA1_1,            res2 ;res2 = w2_2*CfsA1_1
             MPYSP  w2_2,               CfsB2_1,            res4 ;res4 = w2_2*CfsB2_1

             ;Shift filter states
             MV     w1_2,               w2_2 ;w2_2 = w1_2

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  input_data,         res2,               res2 ;res2 *= input_data
             ADDSP  res1,               res2,               w1_2 ;w1_2 = res1 + res2

             ;y(n) = b0*w(n) + 'b1*w(n-1)' + 'b2*w(n-2)'
             MPYSP  w1_2,               CfsB0_1,            res5    ;res5 = w1_2*CfsB0_1
             ADDSP  res3,               res4,               accum2  ;accum2 = res3 + res4
             ADDSP  accum2,              res5,               accum2 ;accum2 += res5
   
             ;Store the calculated o/p
             STW    accum2,              *x2++ ;*x2++ = accum2             

************* 3 *******************************             
             ;a1*w(n-1) & b1*w(n-1)             
             MPYSP  w1_3,               CfsA0_1,            res1 ;res1 =  w1_3*CfsA0_1
             MPYSP  w1_3,               CfsB1_1,            res3 ;res3 =  w1_3*CfsB1_1             
             
             ;Get one input sample & increment i/p ptr
             LDW    *x3,              input_data ;input_data = *x3++      

             ;a2*w(n-2) & b2*w(n-2)
             MPYSP  w2_3,               CfsA1_1,            res2 ;res2 = w2_3*CfsA1_1
             MPYSP  w2_3,               CfsB2_1,            res4 ;res4 = w2_3*CfsB2_1

             ;Shift filter states
             MV     w1_3,               w2_3 ;w2_3 = w1_3

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  input_data,         res2,               res2 ;res2 *= input_data
             ADDSP  res1,               res2,               w1_3 ;w1_3 = res1 + res2

             ;y(n) = b0*w(n) + 'b1*w(n-1)' + 'b2*w(n-2)'
             MPYSP  w1_3,               CfsB0_1,            res5    ;res5 = w1_3*CfsB0_1
             ADDSP  res3,               res4,               accum3  ;accum3 = res3 + res4
             ADDSP  accum3,              res5,               accum3 ;accum3 += res5
   
             ;Store the calculated o/p
             STW    accum3,            *x3++ ;*x3++ = accum3       
             
************* 4 *******************************             
             ;a1*w(n-1) & b1*w(n-1)             
             MPYSP  w1_4,               CfsA0_1,            res1 ;res1 =  w1_4*CfsA0_1
             MPYSP  w1_4,               CfsB1_1,            res3 ;res3 =  w1_4*CfsB1_1             
             
             ;Get one input sample & increment i/p ptr
             LDW    *x4,              input_data ;input_data = *x4++      

             ;a2*w(n-2) & b2*w(n-2)
             MPYSP  w2_4,               CfsA1_1,            res2 ;res2 = w2_4*CfsA1_1
             MPYSP  w2_4,               CfsB2_1,            res4 ;res4 = w2_4*CfsB2_1

             ;Shift filter states
             MV     w1_4,               w2_4 ;w2_4 = w1_4

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  input_data,         res2,               res2 ;res2 *= input_data
             ADDSP  res1,               res2,               w1_4 ;w1_4 = res1 + res2

             ;y(n) = b0*w(n) + 'b1*w(n-1)' + 'b2*w(n-2)'
             MPYSP  w1_4,               CfsB0_1,            res5   ;res5 = w1_4*CfsB0_1
             ADDSP  res3,               res4,               accum4 ;accum4 = res3 + res4
             ADDSP  accum4,             res5,               accum4 ;accum4 += res5
   
             ;Store the calculated o/p
             STW    accum4,            *x4++ ;*x4++ = accum4      
                          
********************************************             

             SUB    count,              1,            count ;count = count - 1
   [count]   B      LOOP ; if(count != 0) goto LOOP                            
   
          ;Update filter state memory
             STW    w1_1,           *filtVars1[0] ;filtVars1[0] = w1_1
             STW    w2_1,           *filtVars1[1] ;filtVars1[1] = w2_1                    
                                                 
             STW    w1_2,           *filtVars2[0] ;filtVars2[0] = w1_2
             STW    w2_2,           *filtVars2[1] ;filtVars2[1] = w2_2
                                                 
             STW    w1_3,           *filtVars3[0] ;filtVars3[0] = w1_3      
             STW    w2_3,           *filtVars3[1] ;filtVars3[1] = w2_3
                                                 
             STW    w1_4,           *filtVars4[0] ;filtVars4[0] = w1_4      
             STW    w2_4,           *filtVars4[1] ;filtVars4[1] = w2_4
             
             .return count ;return(count)
             
             .endproc ;C callable function, directive
             
