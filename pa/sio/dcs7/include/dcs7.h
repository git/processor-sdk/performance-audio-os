
/******************************************************************************
* Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// DA10x SPI, I2C and UART slave control status (DCS7) implementation
//


#ifndef __DCS7_H__
#define __DCS7_H__

#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/IHeap.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/hal/Cache.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/heaps/HeapMem.h>
/* UART LLD Header files */
#include <ti/csl/csl_chip.h>
#include <ti/csl/cslr_device.h>
#include <ti/csl/csl_cache.h>
#include <ti/csl/src/ip/uart/V0/cslr_uart.h>
#include <ti/board/board.h>
#include <ti/board/src/evmK2G/include/board_cfg.h>
#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/UART_stdio.h>
#include <ti/drv/uart/soc/UART_soc.h>

#if 0 // FL: PDK 1.0.1.2_eng
#include <ti/drv/uart/src/v0/UART_dma_v0.h>   
#else // FL: PDK 1.0.6 
#include <ti/drv/uart/src/v0/UART_v0.h> 
#endif

/* EDMA3 Header files */
#include <ti/sdo/edma3/drv/edma3_drv.h>
#include <ti/sdo/edma3/rm/edma3_rm.h>
#include <ti/sdo/edma3/rm/sample/bios6_edma3_rm_sample.h>
/* SPI LLD Header files */
#include <ti/drv/spi/SPI.h>
#include <ti/drv/spi/src/v0/SPI_v0.h>
#include <ti/drv/spi/soc/SPI_soc.h>
/* I2C LLD Header files */
#include <ti/drv/i2c/I2C.h>

#if 0 // FL: PDK 1.0.1.2_eng
#include <ti/drv/i2c/src/v0/I2C_v0.h>
#else // FL: PDK 1.0.6 
#include <ti/drv/i2c/soc/I2C_v0.h>    
#endif

#include <ti/drv/i2c/soc/I2C_soc.h>


#include <acp.h>
#include <acp_mds.h>
#include <acp_mds_priv.h>
#include <dcs7_params.h>

// Error return codes
#define RETURN_SUCCESS  0
#define RETURN_ERROR1   1
#define RETURN_ERROR2   2
#define RETURN_ERROR3   3
#define RETURN_ERROR4   4
#define RETURN_ERROR5   5

// Trace control
#ifdef TRACE_ENABLE
#define TRACE_TERSE0(a)             Log_info0(a)
#define TRACE_TERSE1(a,b)           Log_info1(a,b)
#define TRACE_TERSE2(a,b,c)         Log_info2(a,b,c)
#define TRACE_TERSE3(a,b,c,d)       Log_info3(a,b,c,d)
#define TRACE_TERSE4(a,b,c,d,e)     Log_info4(a,b,c,d,e)
#else
#define TRACE_TERSE0(a)
#define TRACE_TERSE1(a,b)
#define TRACE_TERSE2(a,b,c)
#define TRACE_TERSE3(a,b,c,d)
#define TRACE_TERSE4(a,b,c,d,e)
#endif


// Common parameters
typedef struct DCS7_CommonParam
{
    Uint32 rbuf_start;
    Uint16 rbuf_len;
    Uint16 xbuf_len;
    Uint32 xbuf_start;
    Uint16 rbuf_off;
    Uint16 rbuf_poff;
    Uint16 xbuf_off;
    Uint16 xbuf_poff;
} DCS7_CommonParam;

#define DCS7_CONFIG_CACHEMODE_DISABLE    0
#define DCS7_CONFIG_CACHEMODE_ENABLE     1

typedef struct DCS7_Config
{
    Uint32 size;               /* Size of structure */
    void *objMemSeg;           /* Object memmory segment */
    void *bufMemSeg;           /* Buffer memmory segment */
    Uint8 cacheMode;           /* Cache mode, Edable or Disable */
    Uint8 res;                 /* reserved */
    Uint16 cacheBufAlign;      /* Buffer alignment */
    Uint32 mediaLayerRxSize;   /* Internal DCS medialayer receive buffer size */
    Uint32 mediaLayerTxSize;   /* Internal DCS medialayer Transmit buffer size */
} DCS7_Config;

extern const DCS7_Config DCS7_CONFIG;


/* Handle definition */
typedef struct DCS7_Obj *DCS7_Handle;

/* Function Table definition */
typedef struct DCS7_Fxns *DCS7_Fxns_Ptr;

typedef struct DCS7_Fxns
{
    Uint32 (*msToTicks)(DCS7_Handle, Uint32);
    void* (*semAlloc)(DCS7_Handle);
    Uint32 (*semPend)(DCS7_Handle, void*);
    Uint32 (*semPendTimeout)(DCS7_Handle, void*, Uint32);
    void (*semPost)(DCS7_Handle, void*);
    void (*semReset)(DCS7_Handle, void*, Uint32);
    void* (*memAlloc)(DCS7_Handle, void*, Uint32, Uint32);
    void (*taskDisable)(DCS7_Handle);
    void (*taskEnable)(DCS7_Handle);
    void (*configureInt)(DCS7_Handle);
    void (*cacheInv)(DCS7_Handle, void*, Uint32);
    void (*cacheWbInv)(DCS7_Handle, void*, Uint32);
    void (*errorInit)();
    void (*errorAssert)();
    void (*errorDeassert)();
    void (*errorRecover)(DCS7_Handle);
    void (*slc)(DCS7_Handle);
    Uint32 (*read)(DCS7_Handle, Uint16*, Uint16);
    Uint32 (*write)(DCS7_Handle, Uint16*, Uint16);
    Uint32 (*configureSPI)(DCS7_Handle);
    Uint32 (*configureI2C)(DCS7_Handle);
    Uint32 (*configureUART)(DCS7_Handle);
    Uint32 (*configure)(DCS7_Handle);
    void (*resetSPI)(DCS7_Handle);
    void (*resetI2C)(DCS7_Handle);
    void (*resetUART)(DCS7_Handle);
    void (*reset)(DCS7_Handle);
    Uint32 (*resAllocMediaLayer)(DCS7_Handle);
    void (*resAllocSPI)(DCS7_Handle);
    void (*resAllocI2C)(DCS7_Handle);
    Uint32 (*resAllocUART)(DCS7_Handle);
    Uint32 (*resAlloc)(DCS7_Handle);
    DCS7_Handle (*open)(const DCS7_Params*, const DCS7_Config*,
                        const DCS7_Fxns_Ptr, ACP_Handle);
    Uint32 (*readIssue)(DCS7_Handle handle, Uint16 *aBuf,
                        Uint16 aSize);
    Uint32 (*writeIssue)(DCS7_Handle handle, Uint16 *aBuf,
                         Uint16 aSize);

} DCS7_Fxns;

extern const DCS7_Fxns DCS7_FXNS;

/* Internal DCS medialayer handle */
typedef struct DCS7_MediaLayerObj
{
        Uint32 size;
        Uint32 rxSize;
        Uint32 txSize;
        Uint16 *rxBuf;
        Uint16 *txBuf;
} DCS7_MediaLayerObj;

/* Handle definition */
typedef struct DCS7_MediaLayerObj *DCS7_MediaLayerHandle;

/* DCS object */
typedef struct DCS7_Obj
{
    Uint32 size;                    /* Size of structure */
    const DCS7_Fxns *fxns;          /* DCS function pointer */
    const DCS7_Params *params;      /* DCS Parameters */
    const DCS7_Config *config;      /* DCS Configurstion */
    DCS7_MediaLayerObj mediaLayer;  /* Internal DCS medialayer object */
    Uint16 *rxBuf;                  /* DCS receive buffer */
    Uint16 *txBuf;                  /* DCS transmit buffer */
    void *sem;                      /* Semaphore used internally */
    DCS7_CommonParam common;        /* DCS common parameters */
    Uint32 ecnt;                    /* DCS error count */
} DCS7_Obj;


/* Functions declarations */
DCS7_Handle DCS7_open(const DCS7_Params *p,
                      const DCS7_Config *c,
                      const DCS7_Fxns *f,
                      ACP_Handle a);
Uint32 DCS7_read(DCS7_Handle handle,
                 Uint16 *aBuf,
                 Uint16 aSize);
Uint32 DCS7_write(DCS7_Handle handle,
                  Uint16 *aBuf,
                  Uint16 aSize);

#endif
