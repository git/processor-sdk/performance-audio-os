
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// DA8xx SPI/I2C slave control status (DCS6) implementation
//
//

#include <dcs6.h>

void DCS6_log(
    DCS6_Handle handle,
    void *log,
    char *msg)
{
    if(log)
#ifdef BIOS6_NONLEGACY
        Log_print0((Uint32)log,msg);
#else
        LOG_printf(log,msg);
#endif
}

void DCS6_log1(
    DCS6_Handle handle,
    void *log,
    char *msg,
    Int32 arg)
{
    if(log)
#ifdef BIOS6_NONLEGACY
        Log_print1((Uint32)log,msg,arg);
#else
        LOG_printf(log,msg,arg);
#endif
}

void* DCS6_semAlloc(
    DCS6_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    return Semaphore_create(0,NULL,NULL);
#else
    return SEM_create(0,NULL);
#endif
}

Uint32 DCS6_semPend(
    DCS6_Handle handle,
    void *sem)
{
#ifdef BIOS6_NONLEGACY
    return Semaphore_pend(sem,BIOS_WAIT_FOREVER);
#else
    return SEM_pend(sem,SYS_FOREVER);
#endif
}

void DCS6_semPost(
    DCS6_Handle handle,
    void *sem)
{
#ifdef BIOS6_NONLEGACY
    Semaphore_post(sem);
#else
    SEM_post(sem);
#endif
}

void* DCS6_memAlloc(
    DCS6_Handle handle,
    void *memSeg,
    Uint32 size,
    Uint32 align)
{
#ifdef BIOS6_NONLEGACY
    return (void*)Memory_alloc((IHeap_Handle)*(Int32*)memSeg,size,align,NULL);
#else
    return MEM_alloc(*(Int32*)memSeg,size,align);
#endif
}

void DCS6_taskDisable(
    DCS6_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    Task_disable();
#else
    TSK_disable();
#endif
}

void DCS6_taskEnable(
    DCS6_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    Task_enable();
#else
    TSK_enable();
#endif
}

void DCS6_configureInt(
    DCS6_Handle handle)
{
#ifdef BIOS6_NONLEGACY
    Hwi_Params hwiParams;

    Hwi_Params_init(&hwiParams);
    hwiParams.arg=(xdc_UArg)handle;
    Hwi_create(handle->dmax->cpuint,handle->fxns->isr,&hwiParams,NULL);
    Hwi_enableInterrupt(handle->dmax->cpuint);
#else
    HWI_Attrs hwiAttrs;

    hwiAttrs=HWI_ATTRS;
    hwiAttrs.arg=(Arg)handle;
    HWI_dispatchPlug(handle->dmax->cpuint,(Fxn)handle->fxns->isr,-1,&hwiAttrs);
    C64_enableIER(1<<handle->dmax->cpuint);
#endif
}

void DCS6_cacheInv(
    DCS6_Handle handle,
    void *p,
    Uint32 cnt)
{
#ifdef BIOS6_NONLEGACY
    Cache_inv(p,cnt,Cache_Type_L1D|Cache_Type_L2D,TRUE);
#else
    Cache_inv(p,cnt,Cache_Type_L1D|Cache_Type_L2D,TRUE);
#endif
}

void DCS6_cacheWbInv(
    DCS6_Handle handle,
    void *p,
    Uint32 cnt)
{
#ifdef BIOS6_NONLEGACY
    Cache_wbInv(p,cnt,Cache_Type_L1D|Cache_Type_L2D,TRUE);
#else
    Cache_wbInv(p,cnt,Cache_Type_L1D|Cache_Type_L2D,TRUE);
#endif
}
