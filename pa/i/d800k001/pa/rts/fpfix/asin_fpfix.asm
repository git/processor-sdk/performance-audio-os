;******************************************************************************
;* TMS320C6x C/C++ Codegen                                          PC v6.1.5 *
;* Date/Time created: Thu Oct 23 14:03:32 2008                                *
;******************************************************************************
	.compiler_opts --c64p_l1d_workaround=off --endian=little --hll_source=on --mem_model:code=near --mem_model:const=data --mem_model:data=far_aggregates --quiet --silicon_version=6740 --symdebug:skeletal 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C674x                                          *
;*   Optimization      : Enabled at level 2                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o2, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Data Access Model : Far Aggregate Data                                   *
;*   Pipelining        : Enabled                                              *
;*   Speculate Loads   : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : DWARF Debug for Program Analysis w/Optimization      *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("MATH/asin.c")
	.dwattr $C$DW$CU, DW_AT_producer("TMS320C6x C/C++ Codegen PC v6.1.5 Copyright (c) 1996-2008 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\lib")
;*****************************************************************************
;* CINIT RECORDS                                                             *
;*****************************************************************************
	.sect	".cinit"
	.align	8
	.field  	$C$IR_1,32
	.field  	_asin_consts$1+0,32
	.word	05681e0b7h,03fa063cah		; _asin_consts$1[0] @ 0
	.word	0e7e9e772h,0bf90c714h		; _asin_consts$1[1] @ 64
	.word	0ec385ad3h,03f941d22h		; _asin_consts$1[2] @ 128
	.word	05ec7d203h,03f7a8babh		; _asin_consts$1[3] @ 192
	.word	04a1ce4c8h,03f88f2b5h		; _asin_consts$1[4] @ 256
	.word	074034981h,03f8c6eb4h		; _asin_consts$1[5] @ 320
	.word	0dd0b5bcch,03f91c6cfh		; _asin_consts$1[6] @ 384
	.word	0d4430d48h,03f96e89dh		; _asin_consts$1[7] @ 448
	.word	0d7048c64h,03f9f1c72h		; _asin_consts$1[8] @ 512
	.word	0b3d62932h,03fa6db6dh		; _asin_consts$1[9] @ 576
	.word	03337a6c6h,03fb33333h		; _asin_consts$1[10] @ 640
	.word	0555552c3h,03fc55555h		; _asin_consts$1[11] @ 704
$C$IR_1:	.set	96


$C$DW$1	.dwtag  DW_TAG_subprogram, DW_AT_name("sqrt")
	.dwattr $C$DW$1, DW_AT_TI_symbol_name("_sqrt")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$17)
	.dwendtag $C$DW$1

$C$DW$3	.dwtag  DW_TAG_variable, DW_AT_name("errno")
	.dwattr $C$DW$3, DW_AT_TI_symbol_name("_errno")
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$3, DW_AT_declaration
	.dwattr $C$DW$3, DW_AT_external
_asin_consts$1:	.usect	".far",96,8
;	c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\bin\opt6x.exe OBJ_CYGWIN_RTS6740_LIB\\asin.if OBJ_CYGWIN_RTS6740_LIB\\asin.opt 
	.sect	".text:_asin"
	.clink
	.global	_asin

$C$DW$4	.dwtag  DW_TAG_subprogram, DW_AT_name("asin")
	.dwattr $C$DW$4, DW_AT_low_pc(_asin)
	.dwattr $C$DW$4, DW_AT_high_pc(0x00)
	.dwattr $C$DW$4, DW_AT_TI_symbol_name("_asin")
	.dwattr $C$DW$4, DW_AT_external
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$4, DW_AT_TI_begin_file("MATH/asin.c")
	.dwattr $C$DW$4, DW_AT_TI_begin_line(0x09)
	.dwattr $C$DW$4, DW_AT_TI_begin_column(0x15)
	.dwattr $C$DW$4, DW_AT_frame_base[DW_OP_breg31 32]
	.dwattr $C$DW$4, DW_AT_TI_skeletal
	.dwattr $C$DW$4, DW_AT_TI_category("TI Library")
	.dwpsn	file "MATH/asin.c",line 10,column 1,is_stmt,address _asin
$C$DW$5	.dwtag  DW_TAG_formal_parameter, DW_AT_name("x")
	.dwattr $C$DW$5, DW_AT_TI_symbol_name("_x")
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$5, DW_AT_location[DW_OP_reg4]

;******************************************************************************
;* FUNCTION NAME: asin                                                        *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,SP,A16,A17,A18,  *
;*                           A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30, *
;*                           A31,B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26, *
;*                           B27,B28,B29,B30,B31                              *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,B0,B1, *
;*                           B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,SP,A16,A17,A18,  *
;*                           A19,A20,A21,A22,A23,A24,A25,A26,A27,A28,A29,A30, *
;*                           A31,B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,B26, *
;*                           B27,B28,B29,B30,B31                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 32 Save = 32 byte                  *
;******************************************************************************
_asin:
;** --------------------------------------------------------------------------*

           STW     .D2T1   A11,*SP--(8)      ; |10| 
||         ZERO    .L2     B9
||         ZERO    .S2     B8                ; |111| 

           STDW    .D2T2   B11:B10,*SP--     ; |10| 
||         MV      .L2X    A5,B11            ; |10| 
||         ZERO    .L1     A5
||         SET     .S2     B9,0x14,0x1d,B9

           MV      .L2X    A4,B10            ; |10| 
||         SET     .S1     A5,0x15,0x1d,A5
||         ZERO    .L1     A4                ; |111| 
||         STDW    .D2T1   A13:A12,*SP--     ; |10| 
||         MV      .D1X    B3,A13            ; |10| 
||         MVK     .S2     0xb,B0            ; |129| 

           ABSDP   .S2     B11:B10,B5:B4     ; |109| 
           STW     .D2T1   A10,*SP--(8)      ; |10| 
           MPYDP   .M2     B5:B4,B5:B4,B7:B6 ; |128| 
           MVKL    .S1     _asin_consts$1,A12
           MVKH    .S1     _asin_consts$1,A12
           ADD     .L1     8,A12,A3          ; |129| 
           CMPGTDP .S1X    B5:B4,A5:A4,A0    ; |111| 
           CMPGTDP .S2     B5:B4,B9:B8,B1    ; |113| 
   [!A0]   LDDW    .D1T1   *A12,A5:A4        ; |129| 
   [!A0]   BNOP    .S1     $C$L2,3           ; |111| 
           MV      .L1X    B6,A6             ; |128| Define a twin register
           MV      .L1X    B7,A7             ; |128| Define a twin register
           ; BRANCHCC OCCURS {$C$L2}         ; |111| 
;** --------------------------------------------------------------------------*

   [ B1]   MV      .L2     B9,B5             ; |113| 
|| [ B1]   MV      .S2     B8,B4             ; |113| 

           NOP             1
           SUBDP   .L1X    A5:A4,B5:B4,A7:A6 ; |115| 
           MV      .L2     B8,B4             ; |115| 
           MV      .L2X    A5,B5             ; |115| 
           NOP             4
           ADDDP   .L1     A5:A4,A7:A6,A7:A6 ; |115| 
           NOP             6
           MPYDP   .M1X    B5:B4,A7:A6,A11:A10 ; |115| 
           NOP             4
$C$DW$6	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$6, DW_AT_low_pc(0x00)
	.dwattr $C$DW$6, DW_AT_name("_sqrt")
	.dwattr $C$DW$6, DW_AT_TI_call
           CALL    .S1     _sqrt             ; |116| 
           MVK     .L2     1,B4              ; |113| 
           MVKL    .S2     _errno,B5
           MVKH    .S2     _errno,B5
   [ B1]   STW     .D2T2   B4,*B5            ; |113| 

           MV      .L1     A11,A5            ; |116| 
||         MV      .S1     A10,A4            ; |116| 
||         ADDKPC  .S2     $C$RL0,B3,0       ; |116| 

$C$RL0:    ; CALL OCCURS {_sqrt} {0}         ; |116| 
;** --------------------------------------------------------------------------*
           LDDW    .D1T1   *A12,A7:A6        ; |118| 
           ZERO    .L2     B5
           MVKH    .S2     0xc0000000,B5
           ZERO    .L2     B4                ; |116| 
           MPYDP   .M2X    B5:B4,A5:A4,B5:B4 ; |116| 
	.dwpsn	file "MATH\asinf_i.h",line 118,column 0,is_stmt

           MPYDP   .M1     A11:A10,A7:A6,A5:A4 ; |118| 
||         ADD     .L1     8,A12,A3          ; |118| 
||         MVK     .L2     0xb,B0            ; |118| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 118
;*      Loop opening brace source line   : 118
;*      Loop closing brace source line   : 118
;*      Known Minimum Trip Count         : 11                    
;*      Known Maximum Trip Count         : 11                    
;*      Known Max Trip Count Factor      : 11
;*      Loop Carried Dependency Bound(^) : 17
;*      Unpartitioned Resource Bound     : 4
;*      Partitioned Resource Bound(*)    : 4
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     1        0     
;*      .M units                     4*       0     
;*      .X cross paths               0        0     
;*      .T address paths             1        0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           2        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        0     
;*      Bound(.L .S .D .LS .LSD)     1        0     
;*
;*      Disqualified loop: Loop carried dependency bound too large
;*----------------------------------------------------------------------------*
$C$L1:    
$C$DW$L$_asin$4$B:
           LDDW    .D1T1   *A3++,A7:A6       ; |118| 
           NOP             8
$C$DW$L$_asin$4$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_asin$5$B:
           ADDDP   .L1     A7:A6,A5:A4,A5:A4 ; |118| 
           SUB     .L2     B0,1,B0           ; |118| 
   [ B0]   BNOP    .S1     $C$L1,4           ; |118| 
           MPYDP   .M1     A11:A10,A5:A4,A5:A4 ; |118| 
           ; BRANCHCC OCCURS {$C$L1}         ; |118| 
$C$DW$L$_asin$5$E:
;** --------------------------------------------------------------------------*
           ZERO    .L1     A3                ; |133| 
           MV      .L2X    A13,B3            ; |12| 
           LDW     .D2T1   *++SP(8),A10      ; |12| 
           SET     .S1     A3,31,31,A3       ; |133| 
           LDDW    .D2T1   *++SP,A13:A12     ; |12| 
           NOP             5
           MPYDP   .M2X    A5:A4,B5:B4,B7:B6 ; |121| 
           NOP             9
           ADDDP   .L2     B7:B6,B5:B4,B7:B6 ; |121| 
           MVKL    .S2     0x54442d18,B4
           MVKL    .S2     0x3fe921fb,B5
           MVKH    .S2     0x54442d18,B4
           MVKH    .S2     0x3fe921fb,B5
           MV      .L1X    B4,A4             ; |121| 
           MV      .L1X    B5,A5             ; |121| 
           ADDDP   .S2     B5:B4,B7:B6,B7:B6 ; |121| 
           ZERO    .L2     B5:B4             ; |133| 

           CMPLTDP .S2     B11:B10,B5:B4,B0  ; |133| 
||         LDDW    .D2T2   *++SP,B11:B10     ; |12| 

           BNOP    .S1     $C$L4,4           ; |125| 
||         LDW     .D2T1   *++SP(8),A11      ; |12| 

           ADDDP   .L1X    A5:A4,B7:B6,A5:A4 ; |121| 
           ; BRANCH OCCURS {$C$L4}           ; |125| 
;** --------------------------------------------------------------------------*
$C$L2:    
	.dwpsn	file "MATH\asinf_i.h",line 129,column 0,is_stmt
           MPYDP   .M1     A7:A6,A5:A4,A5:A4 ; |129| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop source line                 : 129
;*      Loop opening brace source line   : 129
;*      Loop closing brace source line   : 129
;*      Known Minimum Trip Count         : 11                    
;*      Known Maximum Trip Count         : 11                    
;*      Known Max Trip Count Factor      : 11
;*      Loop Carried Dependency Bound(^) : 17
;*      Unpartitioned Resource Bound     : 4
;*      Partitioned Resource Bound(*)    : 4
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     0        0     
;*      .S units                     0        0     
;*      .D units                     1        0     
;*      .M units                     4*       0     
;*      .X cross paths               0        0     
;*      .T address paths             1        0     
;*      Long read paths              0        0     
;*      Long write paths             0        0     
;*      Logical  ops (.LS)           2        0     (.L or .S unit)
;*      Addition ops (.LSD)          0        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             1        0     
;*      Bound(.L .S .D .LS .LSD)     1        0     
;*
;*      Disqualified loop: Loop carried dependency bound too large
;*----------------------------------------------------------------------------*
$C$L3:    
$C$DW$L$_asin$8$B:
           LDDW    .D1T1   *A3++,A9:A8       ; |129| 
           NOP             8
$C$DW$L$_asin$8$E:
;** --------------------------------------------------------------------------*
$C$DW$L$_asin$9$B:
           ADDDP   .L1     A9:A8,A5:A4,A5:A4 ; |129| 
           SUB     .L2     B0,1,B0           ; |129| 
   [ B0]   BNOP    .S1     $C$L3,4           ; |129| 
           MPYDP   .M1     A7:A6,A5:A4,A5:A4 ; |129| 
           ; BRANCHCC OCCURS {$C$L3}         ; |129| 
$C$DW$L$_asin$9$E:
;** --------------------------------------------------------------------------*
           ZERO    .L1     A3                ; |133| 
           MV      .L2X    A13,B3            ; |12| 
           LDW     .D2T1   *++SP(8),A10      ; |12| 
           SET     .S1     A3,31,31,A3       ; |133| 
           LDDW    .D2T1   *++SP,A13:A12     ; |12| 
           NOP             4
           MPYDP   .M1X    A5:A4,B5:B4,A5:A4 ; |131| 
           NOP             9
           ADDDP   .L1X    A5:A4,B5:B4,A5:A4 ; |131| 
           ZERO    .L2     B5:B4             ; |133| 

           CMPLTDP .S2     B11:B10,B5:B4,B0  ; |133| 
||         LDDW    .D2T2   *++SP,B11:B10     ; |12| 

           LDW     .D2T1   *++SP(8),A11      ; |12| 
;** --------------------------------------------------------------------------*
$C$L4:    
           NOP             1
$C$DW$7	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$7, DW_AT_low_pc(0x00)
	.dwattr $C$DW$7, DW_AT_TI_return
           RETNOP  .S2     B3,4              ; |12| 
	.dwpsn	file "MATH/asin.c",line 12,column 1,is_stmt
   [ B0]   XOR     .L1     A5,A3,A5          ; |133| 
           ; BRANCH OCCURS {B3}              ; |12| 

$C$DW$8	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$8, DW_AT_name("c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\lib\OBJ_CYGWIN_RTS6740_LIB\\asin.asm:$C$L3:1:1224750812")
	.dwattr $C$DW$8, DW_AT_TI_begin_file("MATH\asinf_i.h")
	.dwattr $C$DW$8, DW_AT_TI_begin_line(0x81)
	.dwattr $C$DW$8, DW_AT_TI_end_line(0x81)
$C$DW$9	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$9, DW_AT_low_pc($C$DW$L$_asin$8$B)
	.dwattr $C$DW$9, DW_AT_high_pc($C$DW$L$_asin$8$E)
$C$DW$10	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$10, DW_AT_low_pc($C$DW$L$_asin$9$B)
	.dwattr $C$DW$10, DW_AT_high_pc($C$DW$L$_asin$9$E)
	.dwendtag $C$DW$8


$C$DW$11	.dwtag  DW_TAG_TI_loop
	.dwattr $C$DW$11, DW_AT_name("c:\Program Files\Texas Instruments\CCSv4\tools\compiler\c6000\lib\OBJ_CYGWIN_RTS6740_LIB\\asin.asm:$C$L1:1:1224750812")
	.dwattr $C$DW$11, DW_AT_TI_begin_file("MATH\asinf_i.h")
	.dwattr $C$DW$11, DW_AT_TI_begin_line(0x76)
	.dwattr $C$DW$11, DW_AT_TI_end_line(0x76)
$C$DW$12	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$12, DW_AT_low_pc($C$DW$L$_asin$4$B)
	.dwattr $C$DW$12, DW_AT_high_pc($C$DW$L$_asin$4$E)
$C$DW$13	.dwtag  DW_TAG_TI_loop_range
	.dwattr $C$DW$13, DW_AT_low_pc($C$DW$L$_asin$5$B)
	.dwattr $C$DW$13, DW_AT_high_pc($C$DW$L$_asin$5$E)
	.dwendtag $C$DW$11

	.dwattr $C$DW$4, DW_AT_TI_end_file("MATH/asin.c")
	.dwattr $C$DW$4, DW_AT_TI_end_line(0x0c)
	.dwattr $C$DW$4, DW_AT_TI_end_column(0x01)
	.dwendtag $C$DW$4

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	_sqrt
	.global	_errno

;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)
$C$DW$T$29	.dwtag  DW_TAG_typedef, DW_AT_name("int8_t")
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$29, DW_AT_language(DW_LANG_C)
$C$DW$T$30	.dwtag  DW_TAG_typedef, DW_AT_name("int_least8_t")
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_language(DW_LANG_C)
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)
$C$DW$T$23	.dwtag  DW_TAG_typedef, DW_AT_name("uint8_t")
	.dwattr $C$DW$T$23, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$23, DW_AT_language(DW_LANG_C)
$C$DW$T$24	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least8_t")
	.dwattr $C$DW$T$24, DW_AT_type(*$C$DW$T$23)
	.dwattr $C$DW$T$24, DW_AT_language(DW_LANG_C)
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x02)
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)
$C$DW$T$31	.dwtag  DW_TAG_typedef, DW_AT_name("int16_t")
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$31, DW_AT_language(DW_LANG_C)
$C$DW$T$32	.dwtag  DW_TAG_typedef, DW_AT_name("int_least16_t")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$32, DW_AT_language(DW_LANG_C)
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)
$C$DW$T$33	.dwtag  DW_TAG_typedef, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$33, DW_AT_language(DW_LANG_C)
$C$DW$T$34	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least16_t")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$34, DW_AT_language(DW_LANG_C)
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)
$C$DW$T$35	.dwtag  DW_TAG_typedef, DW_AT_name("int32_t")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$35, DW_AT_language(DW_LANG_C)
$C$DW$T$36	.dwtag  DW_TAG_typedef, DW_AT_name("int_least32_t")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$36, DW_AT_language(DW_LANG_C)
$C$DW$T$37	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast8_t")
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$37, DW_AT_language(DW_LANG_C)
$C$DW$T$38	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast16_t")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$38, DW_AT_language(DW_LANG_C)
$C$DW$T$39	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast32_t")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$39, DW_AT_language(DW_LANG_C)
$C$DW$T$40	.dwtag  DW_TAG_typedef, DW_AT_name("intptr_t")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$40, DW_AT_language(DW_LANG_C)
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)
$C$DW$T$25	.dwtag  DW_TAG_typedef, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$25, DW_AT_language(DW_LANG_C)
$C$DW$T$41	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least32_t")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$41, DW_AT_language(DW_LANG_C)
$C$DW$T$42	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast8_t")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$42, DW_AT_language(DW_LANG_C)
$C$DW$T$43	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast16_t")
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$43, DW_AT_language(DW_LANG_C)
$C$DW$T$44	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast32_t")
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$44, DW_AT_language(DW_LANG_C)
$C$DW$T$45	.dwtag  DW_TAG_typedef, DW_AT_name("uintptr_t")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$45, DW_AT_language(DW_LANG_C)
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("long")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$12, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$12, DW_AT_bit_offset(0x18)
$C$DW$T$46	.dwtag  DW_TAG_typedef, DW_AT_name("int40_t")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$46, DW_AT_language(DW_LANG_C)
$C$DW$T$47	.dwtag  DW_TAG_typedef, DW_AT_name("int_least40_t")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_language(DW_LANG_C)
$C$DW$T$48	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast40_t")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$48, DW_AT_language(DW_LANG_C)
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned long")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)
	.dwattr $C$DW$T$13, DW_AT_bit_size(0x28)
	.dwattr $C$DW$T$13, DW_AT_bit_offset(0x18)
$C$DW$T$49	.dwtag  DW_TAG_typedef, DW_AT_name("uint40_t")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$49, DW_AT_language(DW_LANG_C)
$C$DW$T$50	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least40_t")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$50, DW_AT_language(DW_LANG_C)
$C$DW$T$51	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast40_t")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$51, DW_AT_language(DW_LANG_C)
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)
$C$DW$T$52	.dwtag  DW_TAG_typedef, DW_AT_name("int64_t")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$T$52, DW_AT_language(DW_LANG_C)
$C$DW$T$53	.dwtag  DW_TAG_typedef, DW_AT_name("int_least64_t")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_language(DW_LANG_C)
$C$DW$T$54	.dwtag  DW_TAG_typedef, DW_AT_name("int_fast64_t")
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$54, DW_AT_language(DW_LANG_C)
$C$DW$T$55	.dwtag  DW_TAG_typedef, DW_AT_name("intmax_t")
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$T$55, DW_AT_language(DW_LANG_C)
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)
$C$DW$T$26	.dwtag  DW_TAG_typedef, DW_AT_name("uint64_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$26, DW_AT_language(DW_LANG_C)
$C$DW$T$27	.dwtag  DW_TAG_typedef, DW_AT_name("mantissa_t")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$27, DW_AT_language(DW_LANG_C)
$C$DW$T$56	.dwtag  DW_TAG_typedef, DW_AT_name("uint_least64_t")
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$56, DW_AT_language(DW_LANG_C)
$C$DW$T$57	.dwtag  DW_TAG_typedef, DW_AT_name("uint_fast64_t")
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$57, DW_AT_language(DW_LANG_C)
$C$DW$T$58	.dwtag  DW_TAG_typedef, DW_AT_name("uintmax_t")
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$58, DW_AT_language(DW_LANG_C)
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

$C$DW$T$19	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x04)
$C$DW$14	.dwtag  DW_TAG_member
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$14, DW_AT_name("mantissa")
	.dwattr $C$DW$14, DW_AT_TI_symbol_name("_mantissa")
	.dwattr $C$DW$14, DW_AT_bit_offset(0x09), DW_AT_bit_size(0x17)
	.dwattr $C$DW$14, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$14, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$15	.dwtag  DW_TAG_member
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$15, DW_AT_name("exp")
	.dwattr $C$DW$15, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$15, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x08)
	.dwattr $C$DW$15, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$15, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$16	.dwtag  DW_TAG_member
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$16, DW_AT_name("sign")
	.dwattr $C$DW$16, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$16, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$16, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$16, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$19


$C$DW$T$20	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)
$C$DW$17	.dwtag  DW_TAG_member
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$17, DW_AT_name("f")
	.dwattr $C$DW$17, DW_AT_TI_symbol_name("_f")
	.dwattr $C$DW$17, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$17, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$18	.dwtag  DW_TAG_member
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$19)
	.dwattr $C$DW$18, DW_AT_name("fp_format")
	.dwattr $C$DW$18, DW_AT_TI_symbol_name("_fp_format")
	.dwattr $C$DW$18, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$18, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$20

$C$DW$T$64	.dwtag  DW_TAG_typedef, DW_AT_name("FLOAT2FORM")
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$20)
	.dwattr $C$DW$T$64, DW_AT_language(DW_LANG_C)

$C$DW$T$21	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)
$C$DW$19	.dwtag  DW_TAG_member
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$19, DW_AT_name("mantissa1")
	.dwattr $C$DW$19, DW_AT_TI_symbol_name("_mantissa1")
	.dwattr $C$DW$19, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x20)
	.dwattr $C$DW$19, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$19, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$20	.dwtag  DW_TAG_member
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$20, DW_AT_name("mantissa0")
	.dwattr $C$DW$20, DW_AT_TI_symbol_name("_mantissa0")
	.dwattr $C$DW$20, DW_AT_bit_offset(0x0c), DW_AT_bit_size(0x14)
	.dwattr $C$DW$20, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$20, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$21	.dwtag  DW_TAG_member
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$21, DW_AT_name("exp")
	.dwattr $C$DW$21, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$21, DW_AT_bit_offset(0x01), DW_AT_bit_size(0x0b)
	.dwattr $C$DW$21, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$21, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$22	.dwtag  DW_TAG_member
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$22, DW_AT_name("sign")
	.dwattr $C$DW$22, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$22, DW_AT_bit_offset(0x00), DW_AT_bit_size(0x01)
	.dwattr $C$DW$22, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$22, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$21


$C$DW$T$22	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x08)
$C$DW$23	.dwtag  DW_TAG_member
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$23, DW_AT_name("f")
	.dwattr $C$DW$23, DW_AT_TI_symbol_name("_f")
	.dwattr $C$DW$23, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$23, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$24	.dwtag  DW_TAG_member
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$21)
	.dwattr $C$DW$24, DW_AT_name("fp_format")
	.dwattr $C$DW$24, DW_AT_TI_symbol_name("_fp_format")
	.dwattr $C$DW$24, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$24, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$22

$C$DW$T$65	.dwtag  DW_TAG_typedef, DW_AT_name("DOUBLE2FORM")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$22)
	.dwattr $C$DW$T$65, DW_AT_language(DW_LANG_C)

$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_byte_size(0x10)
$C$DW$25	.dwtag  DW_TAG_member
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$24)
	.dwattr $C$DW$25, DW_AT_name("sign")
	.dwattr $C$DW$25, DW_AT_TI_symbol_name("_sign")
	.dwattr $C$DW$25, DW_AT_data_member_location[DW_OP_plus_uconst 0x0]
	.dwattr $C$DW$25, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$26	.dwtag  DW_TAG_member
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$26, DW_AT_name("exp")
	.dwattr $C$DW$26, DW_AT_TI_symbol_name("_exp")
	.dwattr $C$DW$26, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$26, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$27	.dwtag  DW_TAG_member
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$27, DW_AT_name("mantissa")
	.dwattr $C$DW$27, DW_AT_TI_symbol_name("_mantissa")
	.dwattr $C$DW$27, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$27, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$T$28

$C$DW$T$66	.dwtag  DW_TAG_typedef, DW_AT_name("realnum")
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$66, DW_AT_language(DW_LANG_C)
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C)

;***************************************************************
;* DWARF REGISTER MAP                                          *
;***************************************************************

$C$DW$28	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A0")
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg0]
$C$DW$29	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A1")
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg1]
$C$DW$30	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A2")
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg2]
$C$DW$31	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A3")
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg3]
$C$DW$32	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A4")
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg4]
$C$DW$33	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A5")
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg5]
$C$DW$34	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A6")
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg6]
$C$DW$35	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A7")
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg7]
$C$DW$36	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A8")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg8]
$C$DW$37	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A9")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg9]
$C$DW$38	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A10")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg10]
$C$DW$39	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A11")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg11]
$C$DW$40	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A12")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg12]
$C$DW$41	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A13")
	.dwattr $C$DW$41, DW_AT_location[DW_OP_reg13]
$C$DW$42	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A14")
	.dwattr $C$DW$42, DW_AT_location[DW_OP_reg14]
$C$DW$43	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A15")
	.dwattr $C$DW$43, DW_AT_location[DW_OP_reg15]
$C$DW$44	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B0")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg16]
$C$DW$45	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B1")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg17]
$C$DW$46	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B2")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg18]
$C$DW$47	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B3")
	.dwattr $C$DW$47, DW_AT_location[DW_OP_reg19]
$C$DW$48	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B4")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg20]
$C$DW$49	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B5")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg21]
$C$DW$50	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B6")
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg22]
$C$DW$51	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B7")
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg23]
$C$DW$52	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B8")
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg24]
$C$DW$53	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B9")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg25]
$C$DW$54	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B10")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg26]
$C$DW$55	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B11")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg27]
$C$DW$56	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B12")
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg28]
$C$DW$57	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B13")
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg29]
$C$DW$58	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DP")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg30]
$C$DW$59	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SP")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg31]
$C$DW$60	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FP")
	.dwattr $C$DW$60, DW_AT_location[DW_OP_regx 0x20]
$C$DW$61	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_regx 0x21]
$C$DW$62	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IRP")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_regx 0x22]
$C$DW$63	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IFR")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_regx 0x23]
$C$DW$64	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NRP")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_regx 0x24]
$C$DW$65	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A16")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x25]
$C$DW$66	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A17")
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x26]
$C$DW$67	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A18")
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x27]
$C$DW$68	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A19")
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x28]
$C$DW$69	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A20")
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x29]
$C$DW$70	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A21")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x2a]
$C$DW$71	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A22")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x2b]
$C$DW$72	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A23")
	.dwattr $C$DW$72, DW_AT_location[DW_OP_regx 0x2c]
$C$DW$73	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A24")
	.dwattr $C$DW$73, DW_AT_location[DW_OP_regx 0x2d]
$C$DW$74	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A25")
	.dwattr $C$DW$74, DW_AT_location[DW_OP_regx 0x2e]
$C$DW$75	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A26")
	.dwattr $C$DW$75, DW_AT_location[DW_OP_regx 0x2f]
$C$DW$76	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A27")
	.dwattr $C$DW$76, DW_AT_location[DW_OP_regx 0x30]
$C$DW$77	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A28")
	.dwattr $C$DW$77, DW_AT_location[DW_OP_regx 0x31]
$C$DW$78	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A29")
	.dwattr $C$DW$78, DW_AT_location[DW_OP_regx 0x32]
$C$DW$79	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A30")
	.dwattr $C$DW$79, DW_AT_location[DW_OP_regx 0x33]
$C$DW$80	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("A31")
	.dwattr $C$DW$80, DW_AT_location[DW_OP_regx 0x34]
$C$DW$81	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B16")
	.dwattr $C$DW$81, DW_AT_location[DW_OP_regx 0x35]
$C$DW$82	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B17")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_regx 0x36]
$C$DW$83	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B18")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_regx 0x37]
$C$DW$84	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B19")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_regx 0x38]
$C$DW$85	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B20")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_regx 0x39]
$C$DW$86	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B21")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_regx 0x3a]
$C$DW$87	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B22")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_regx 0x3b]
$C$DW$88	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B23")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_regx 0x3c]
$C$DW$89	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B24")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x3d]
$C$DW$90	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B25")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x3e]
$C$DW$91	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B26")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x3f]
$C$DW$92	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B27")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x40]
$C$DW$93	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B28")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x41]
$C$DW$94	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B29")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x42]
$C$DW$95	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B30")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x43]
$C$DW$96	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("B31")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x44]
$C$DW$97	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("AMR")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x45]
$C$DW$98	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CSR")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x46]
$C$DW$99	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISR")
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x47]
$C$DW$100	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ICR")
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x48]
$C$DW$101	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IER")
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x49]
$C$DW$102	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ISTP")
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x4a]
$C$DW$103	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IN")
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x4b]
$C$DW$104	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OUT")
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x4c]
$C$DW$105	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ACR")
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x4d]
$C$DW$106	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ADR")
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x4e]
$C$DW$107	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FADCR")
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x4f]
$C$DW$108	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FAUCR")
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x50]
$C$DW$109	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("FMCR")
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x51]
$C$DW$110	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GFPGFR")
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x52]
$C$DW$111	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DIER")
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x53]
$C$DW$112	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("REP")
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x54]
$C$DW$113	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCL")
	.dwattr $C$DW$113, DW_AT_location[DW_OP_regx 0x55]
$C$DW$114	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSCH")
	.dwattr $C$DW$114, DW_AT_location[DW_OP_regx 0x56]
$C$DW$115	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ARP")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_regx 0x57]
$C$DW$116	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ILC")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_regx 0x58]
$C$DW$117	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RILC")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_regx 0x59]
$C$DW$118	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DNUM")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_regx 0x5a]
$C$DW$119	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("SSR")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_regx 0x5b]
$C$DW$120	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYA")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_regx 0x5c]
$C$DW$121	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("GPLYB")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_regx 0x5d]
$C$DW$122	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TSR")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_regx 0x5e]
$C$DW$123	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ITSR")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_regx 0x5f]
$C$DW$124	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("NTSR")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_regx 0x60]
$C$DW$125	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("EFR")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x61]
$C$DW$126	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ECR")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_regx 0x62]
$C$DW$127	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("IERR")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_regx 0x63]
$C$DW$128	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DMSG")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x64]
$C$DW$129	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CMSG")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x65]
$C$DW$130	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_ADDR")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_regx 0x66]
$C$DW$131	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_DATA")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_regx 0x67]
$C$DW$132	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DT_DMA_CNTL")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_regx 0x68]
$C$DW$133	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCU_CNTL")
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x69]
$C$DW$134	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_REC_CNTL")
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x6a]
$C$DW$135	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_XMT_CNTL")
	.dwattr $C$DW$135, DW_AT_location[DW_OP_regx 0x6b]
$C$DW$136	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_CFG")
	.dwattr $C$DW$136, DW_AT_location[DW_OP_regx 0x6c]
$C$DW$137	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RDATA")
	.dwattr $C$DW$137, DW_AT_location[DW_OP_regx 0x6d]
$C$DW$138	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WDATA")
	.dwattr $C$DW$138, DW_AT_location[DW_OP_regx 0x6e]
$C$DW$139	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_RADDR")
	.dwattr $C$DW$139, DW_AT_location[DW_OP_regx 0x6f]
$C$DW$140	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("RTDX_WADDR")
	.dwattr $C$DW$140, DW_AT_location[DW_OP_regx 0x70]
$C$DW$141	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("MFREG0")
	.dwattr $C$DW$141, DW_AT_location[DW_OP_regx 0x71]
$C$DW$142	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DBG_STAT")
	.dwattr $C$DW$142, DW_AT_location[DW_OP_regx 0x72]
$C$DW$143	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("BRK_EN")
	.dwattr $C$DW$143, DW_AT_location[DW_OP_regx 0x73]
$C$DW$144	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0_CNT")
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x74]
$C$DW$145	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP0")
	.dwattr $C$DW$145, DW_AT_location[DW_OP_regx 0x75]
$C$DW$146	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP1")
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x76]
$C$DW$147	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP2")
	.dwattr $C$DW$147, DW_AT_location[DW_OP_regx 0x77]
$C$DW$148	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("HWBP3")
	.dwattr $C$DW$148, DW_AT_location[DW_OP_regx 0x78]
$C$DW$149	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("OVERLAY")
	.dwattr $C$DW$149, DW_AT_location[DW_OP_regx 0x79]
$C$DW$150	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("PC_PROF")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_regx 0x7a]
$C$DW$151	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("ATSR")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_regx 0x7b]
$C$DW$152	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TRR")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_regx 0x7c]
$C$DW$153	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("TCRR")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_regx 0x7d]
$C$DW$154	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DESR")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_regx 0x7e]
$C$DW$155	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("DETR")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_regx 0x7f]
$C$DW$156	.dwtag  DW_TAG_TI_assign_register, DW_AT_name("CIE_RETA")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_regx 0xe4]
	.dwendtag $C$DW$CU

