/****************************************************************************/
/*  _C6xSimulator_priv.h                                                    */
/****************************************************************************/

/* 
 *
 * Copyright (C) 1997-2005 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*                                                                          
 * Internal header file to support C6xSimulator intrinsics implementation  
 * Only for use by C6xSimulator implementation itself 
 * (not user code)
 */

#ifndef _C6XSIMULATOR_PRIV_H
#define _C6XSIMULATOR_PRIV_H


/* this internal header file is relevant *only* in host environment */
#ifndef _TMS320C6X         


/****************************************************************************/
/* the defines                                                              */

#define PASS                                0
#define FAIL                                1

#define MIN_INT8         ((int32)(0xFFFFFF80))
#define MAX_INT8         ((int32)(0x0000007F))

#define MIN_UINT8       ((uint32)(0x00000000))
#define MAX_UINT8       ((uint32)(0x000000FF))

#define MIN_INT16        ((int32)(0xFFFF8000))
#define MAX_INT16        ((int32)(0x00007FFF))

#define MIN_UINT16      ((uint32)(0x00000000))
#define MAX_UINT16      ((uint32)(0x0000FFFF))

#define MIN_INT32        ((int32)(0x80000000))
#define MAX_INT32        ((int32)(0x7FFFFFFF))

#define MIN_UINT32      ((uint32)(0x00000000))
#define MAX_UINT32      ((uint32)(0xFFFFFFFF))

#define MIN_INT40_HI      ((int8)(0xFFFFFF80))
#define MIN_INT40_LO    ((uint32)(0x00000000))

#define MAX_INT40_HI      ((int8)(0x0000007F))
#define MAX_INT40_LO    ((uint32)(0xFFFFFFFF))

#define MIN_UINT40_HI    ((uint8)(0x00000000))
#define MIN_UINT40_LO   ((uint32)(0x00000000))

#define MAX_UINT40_HI    ((uint8)(0x000000FF))
#define MAX_UINT40_LO   ((uint32)(0xFFFFFFFF))

#define MIN_INT64_HI     ((int32)(0x80000000))
#define MIN_INT64_LO    ((uint32)(0x00000000))

#define MAX_INT64_HI     ((int32)(0x7FFFFFFF))
#define MAX_INT64_LO    ((uint32)(0xFFFFFFFF))

#define MIN_UINT64_HI   ((uint32)(0x00000000))
#define MIN_UINT64_LO   ((uint32)(0x00000000))

#define MAX_UINT64_HI   ((uint32)(0xFFFFFFFF))
#define MAX_UINT64_LO   ((uint32)(0xFFFFFFFF))


/****************************************************************************/
/* the data structures                                                      */

#ifdef LITTLE_ENDIAN_HOST

typedef struct _INT32X2
{
  int16 lo;
  int16 hi;
} int32x2;

typedef struct _INT32X2U
{
  uint16 lo;
  uint16 hi;
} int32x2u;

typedef struct _INT32X4
{
  int8 lo1;
  int8 lo2;
  int8 hi1;
  int8 hi2;
} int32x4;

typedef struct _INT32X4U
{
  uint8 lo1;
  uint8 lo2;
  uint8 hi1;
  uint8 hi2;
} int32x4u;

/* 
The unused<N> fields below are part of the fix for BZ 2272.   These structs
are overlaid with type int40.  Since int40 is implemented as long long, 
these unused fields guarantee that the remaining "int8 hi" field picks up the
correct 8 bits.
*/

typedef struct _INT40X2
{
  int32 lo;
  int8  hi;
  int8  unused0;
  int8  unused1;
  int8  unused2;
} int40x2;

typedef struct _INT40X2U
{
  uint32 lo;
  uint8  hi;
  uint8  unused0;
  uint8  unused1;
  uint8  unused2;
} int40x2u;

typedef struct _INT64X2
{
  int32 lo;
  int32 hi;
} int64x2;

typedef struct _INT64X2U
{
  uint32 lo;
  uint32 hi;
} int64x2u;

typedef struct _INT64X2F
{
  float32 lo;
  float32 hi;
} int64x2f;

typedef struct _INT64X4
{
  int16 lo1;
  int16 lo2;
  int16 hi1;
  int16 hi2;
} int64x4;

typedef struct _INT64X4U
{
  uint16 lo1;
  uint16 lo2;
  uint16 hi1;
  uint16 hi2;
} int64x4u;

#endif     // LITTLE_ENDIAN_HOST


#ifdef BIG_ENDIAN_HOST

typedef struct _INT32X2
{
  int16 hi;
  int16 lo;
} int32x2;

typedef struct _INT32X2U
{
  uint16 hi;
  uint16 lo;
} int32x2u;

typedef struct _INT32X4
{
  int8 hi2;
  int8 hi1;
  int8 lo2;
  int8 lo1;
} int32x4;

typedef struct _INT32X4U
{
  uint8 hi2;
  uint8 hi1;
  uint8 lo2;
  uint8 lo1;
} int32x4u;

/* 
The unused<N> fields below are part of the fix for BZ 2272.   These structs
are overlaid with type int40.  Since int40 is implemented as long long, 
these unused fields guarantee that the remaining "int8 hi" field picks up
the correct 8 bits.
*/
typedef struct _INT40X2
{
  int8  unused3;
  int8  unused2;
  int8  unused1;
  int8  hi;
  int32 lo;
} int40x2;

typedef struct _INT40X2U
{
  uint8  unused3;
  uint8  unused2;
  uint8  unused1;
  uint8  hi;
  uint32 lo;
} int40x2u;

typedef struct _INT64X2
{
  int32 hi;
  int32 lo;
} int64x2;

typedef struct _INT64X2U
{
  uint32 hi;
  uint32 lo;
} int64x2u;

typedef struct _INT64X2F
{
  float32 hi;
  float32 lo;
} int64x2f;

typedef struct _INT64X4
{
  int16 hi2;
  int16 hi1;
  int16 lo2;
  int16 lo1;
} int64x4;

typedef struct _INT64X4U
{
  uint16 hi2;
  uint16 hi1;
  uint16 lo2;
  uint16 lo1;
} int64x4u;

#endif    // BIG_ENDIAN_HOST


/***************************************************************************/
/* the unions                                                              */
#if defined(LITTLE_ENDIAN_HOST) || defined(BIG_ENDIAN_HOST)

union reg32 
{
  int32    x1;
  int32x2  x2;
  int32x4  x4;

  uint32   x1u;
  int32x2u x2u;
  int32x4u x4u;

  float32    xf;
};

union reg40
{
  int40    x1;
  int40x2  x2;

  uint40   x1u;
  int40x2u x2u;
};

union reg64
{
  double64    x1_d64;
  int64_d     x1_d;
  int64_ll    x1_ll;
  int64x2     x2;
  int64x4     x4;

  uint64_d   x1u_d;
  uint64_ll  x1u_ll;
  int64x2u   x2u;
  int64x2f   x2f;
  int64x4u   x4u;
};

#endif          // LITTLE_ENDIAN_HOST || BIG_ENDIAN_HOST


#endif          /* #ifndef _TMS320C6X */      

#endif          /* _C6XSIMULATOR_PRIV_H */

/*
 *  Automated Revision Information
 *  Changed: $Date: 2011-03-11 10:16:52 -0600 (Fri, 11 Mar 2011) $
 *  Revision: $Revision: 10019 $
 */

