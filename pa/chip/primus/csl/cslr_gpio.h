
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*   ==========================================================================
 *   Copyright (c) Texas Instruments Inc , 2004
 *
 *   Use of this software is controlled by the terms and conditions found
 *   in the license agreement under which this software has been supplied
 *   provided
 *   ==========================================================================
*/
#ifndef _CSLR_GPIO_H_
#define _CSLR_GPIO_H_

#include <cslr.h>
#include <cslr_gpio_001.h>  //supposed to be cslr_gpio_002.h-edited by vishwa
#include <primus.h>

#define CSL_GPIO_NUM_PINS          (128)

/* #define CSL_GPIO_NUM_BANKS       (CSL_GPIO_NUM_PINS + 15)/16 */

#define CSL_GPIO_NUM_BANKS         (8)

           
typedef enum {
  CSL_GPIO_PIN0,
  CSL_GPIO_PIN1,
  CSL_GPIO_PIN2,
  CSL_GPIO_PIN3,
  CSL_GPIO_PIN4,
  CSL_GPIO_PIN5,
  CSL_GPIO_PIN6,
  CSL_GPIO_PIN7,
  CSL_GPIO_PIN8,
  CSL_GPIO_PIN9,
  CSL_GPIO_PIN10,
  CSL_GPIO_PIN11,
  CSL_GPIO_PIN12,
  CSL_GPIO_PIN13,
  CSL_GPIO_PIN14,
  CSL_GPIO_PIN15,
  CSL_GPIO_PIN16,
  CSL_GPIO_PIN17,
  CSL_GPIO_PIN18,
  CSL_GPIO_PIN19,
  CSL_GPIO_PIN20,
  CSL_GPIO_PIN21,
  CSL_GPIO_PIN22,
  CSL_GPIO_PIN23,
  CSL_GPIO_PIN24,
  CSL_GPIO_PIN25,
  CSL_GPIO_PIN26,
  CSL_GPIO_PIN27,
  CSL_GPIO_PIN28,
  CSL_GPIO_PIN29,
  CSL_GPIO_PIN30,
  CSL_GPIO_PIN31,
  CSL_GPIO_PIN32,
  CSL_GPIO_PIN33,
  CSL_GPIO_PIN34,
  CSL_GPIO_PIN35,
  CSL_GPIO_PIN36,
  CSL_GPIO_PIN37,
  CSL_GPIO_PIN38,
  CSL_GPIO_PIN39,
  CSL_GPIO_PIN40,
  CSL_GPIO_PIN41,
  CSL_GPIO_PIN42,
  CSL_GPIO_PIN43,
  CSL_GPIO_PIN44,
  CSL_GPIO_PIN45,
  CSL_GPIO_PIN46,
  CSL_GPIO_PIN47,
  CSL_GPIO_PIN48,
  CSL_GPIO_PIN49,
  CSL_GPIO_PIN50,
  CSL_GPIO_PIN51,
  CSL_GPIO_PIN52,
  CSL_GPIO_PIN53,
  CSL_GPIO_PIN54,
  CSL_GPIO_PIN55,
  CSL_GPIO_PIN56,
  CSL_GPIO_PIN57,
  CSL_GPIO_PIN58,
  CSL_GPIO_PIN59,
  CSL_GPIO_PIN60,
  CSL_GPIO_PIN61,
  CSL_GPIO_PIN62,
  CSL_GPIO_PIN63,
  CSL_GPIO_PIN64,
  CSL_GPIO_PIN65,
  CSL_GPIO_PIN66,
  CSL_GPIO_PIN67,
  CSL_GPIO_PIN68,
  CSL_GPIO_PIN69,
  CSL_GPIO_PIN70,
  CSL_GPIO_PIN71,
  CSL_GPIO_PIN72,
  CSL_GPIO_PIN73,
  CSL_GPIO_PIN74,
  CSL_GPIO_PIN75,
  CSL_GPIO_PIN76,
  CSL_GPIO_PIN77,
  CSL_GPIO_PIN78,
  CSL_GPIO_PIN79,
  CSL_GPIO_PIN80,
  CSL_GPIO_PIN81,
  CSL_GPIO_PIN82,
  CSL_GPIO_PIN83,
  CSL_GPIO_PIN84,
  CSL_GPIO_PIN85,
  CSL_GPIO_PIN86,
  CSL_GPIO_PIN87,
  CSL_GPIO_PIN88,
  CSL_GPIO_PIN89,
  CSL_GPIO_PIN90,
  CSL_GPIO_PIN91,
  CSL_GPIO_PIN92,
  CSL_GPIO_PIN93,
  CSL_GPIO_PIN94,
  CSL_GPIO_PIN95,
  CSL_GPIO_PIN96,
  CSL_GPIO_PIN97,
  CSL_GPIO_PIN98,
  CSL_GPIO_PIN99,
  CSL_GPIO_PIN100,
  CSL_GPIO_PIN101,
  CSL_GPIO_PIN102,
  CSL_GPIO_PIN103,
  CSL_GPIO_PIN104,
  CSL_GPIO_PIN105,
  CSL_GPIO_PIN106,
  CSL_GPIO_PIN107,
  CSL_GPIO_PIN108,
  CSL_GPIO_PIN109,
  CSL_GPIO_PIN110,
  CSL_GPIO_PIN111,
  CSL_GPIO_PIN112,
  CSL_GPIO_PIN113,
  CSL_GPIO_PIN114,
  CSL_GPIO_PIN115,
  CSL_GPIO_PIN116,
  CSL_GPIO_PIN117,
  CSL_GPIO_PIN118,
  CSL_GPIO_PIN119,
  CSL_GPIO_PIN120,
  CSL_GPIO_PIN121,
  CSL_GPIO_PIN122,
  CSL_GPIO_PIN123,
  CSL_GPIO_PIN124,
  CSL_GPIO_PIN125,
  CSL_GPIO_PIN126,
  CSL_GPIO_PIN127
 } CSL_GpioPinNum;

typedef enum {
  CSL_GPIO_BANK0,
  CSL_GPIO_BANK1,
  CSL_GPIO_BANK2,
  CSL_GPIO_BANK3,
  CSL_GPIO_BANK4,
  CSL_GPIO_BANK5,
  CSL_GPIO_BANK6,
  CSL_GPIO_BANK7
} CSL_GpioBankNum;

/*****************************************************************************\
             Overlay structure typedef definition
\*****************************************************************************/

CSL_IDEF_INLINE CSL_GpioRegsOvly _CSL_gpioGetBaseAddr (Uint16 gpioNum) {
  return (CSL_GpioRegsOvly)_CSL_gpiolookup[gpioNum];
}

#endif /*  */
/* Rev.No.   Date/Time               ECN No.          Modifier      */
/* -------   ---------               -------          --------      */

/* 5         July 2               			Vishwa      */
/*                                                                  */
/* Uploaded the CSL0.57 JAN 2005 Release and built the library for ARM and DSP*/
/********************************************************************/ 
