
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common control operation declarations
//
//
//
//

/*
 *  This header defines the types, constants, and functions shared by the
 *  IALG common control implementations.
 */
#ifndef ICOM_
#define ICOM_

#include <xdc/std.h>

//#include <ialg.h>
//#include  <std.h>
//#include <xdas.h>
//
#include <paf_ialg.h>
//#include <paftyp.h>

/*
 *  ======== COM_Status ========
 *  Create status structure with only the size parameter.
 */
typedef volatile struct COM_Status {Int size;} COM_Status;

/*
 *  ======== COM_Config ========
 *  Create config structure with only a dummy parameter.
 */
typedef struct COM_Config { Int dummy; } COM_Config;

typedef struct COM_TII_Obj {
    IALG_Obj alg;               /* MUST be first field of all XDAIS algs */\
    int mask;
    COM_Status *pStatus;        /* public interface */
    COM_Config *pConfig;        /* private interface */
    PAF_IALG_Common *pCommon;   /* common interface */
    Void *pScrach;
} COM_TII_Obj;

/*
 *  ======== ICOM_Cmd ========
 *  The Cmd enumeration defines the control commands for the COM
 *  control method.
 */
typedef enum ICOM_Cmd {
    ICOM_NULL,
    ICOM_GETSTATUSADDRESS1,
    ICOM_GETSTATUSADDRESS2,
    ICOM_GETSTATUS,
    ICOM_SETSTATUS
} ICOM_Cmd;

typedef struct COM_TII_Obj *COM_TII_Handle;

#endif  /* ICOM_ */
