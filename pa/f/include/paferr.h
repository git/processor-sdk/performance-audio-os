
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Error Declarations
//
//
//

#ifndef PAFERR_
#define PAFERR_

#define PAF_STATUS_UNKNOWN -1
#define PAF_STATUS_SUCCESS  0
#define PAF_STATUS_FAILURE  1

#define PAF_ERROR_UNKNOWN -1
#define PAF_ERROR_SUCCESS  0


/* Error mapping:
   - All audio stream processing errors are of type Int i.e 32bits. 
   - byte 0-1 represents the algorithm level errors
   - byte 2 is reserved.
   - bit 0-3 of byte2 denotes the PAF components that has caused the error.
   - bit 4-7 of byte2 denotes the Framwework execution status.
*/

/* 
* PAF_FW_FWPROCESSING   -> indicates that PAF is not yet ready to process
                        any input bitstream.
                        e.g 
                        - Input switching alpha code is either not sent or
                        has not been successfully executed.
                        - The input bitstream is not supported by PAF etc.

* PAF_FW_INITPROCESSING -> indicates initialization phase of framework.

* PAF_FW_PASSPROCESSING -> indicates the pass through phase of framework.
                           e.g when writeDECSourceSelectPass is sent.

* PAF_FW_AUTOPROCESSING -> indicates auto detection(detecting the input bitstream)
						   phase of framework i.e.

* PAF_FW_DECODEPROCESSING -> indicates the input decoding phase of framework when
							 input bitstream has been detected.
*/                        

#define PAF_FW_FWPROCESSING 		(int)(0<<20)
#define PAF_FW_INITPROCESSING 		(int)(1<<20)
#define PAF_FW_PASSPROCESSING 		(int)(2<<20)
#define PAF_FW_AUTOPROCESSING   	(int)(3<<20)
#define PAF_FW_DECODEPROCESSING 	(int)(4<<20)



/*
* PAF_COMPONENT_UNKNOWN		-> inidcates an unknown component primarily because
                               it doesn't fall into other category of components.

* PAF_COMPONENT_UNSUPPORTED	-> inidcates that PAF doesn't have support for this.

* PAF_COMPONENT_INPUT		-> inidcates the input driver.

* PAF_COMPONENT_DECODE		-> inidcates the decoder e.g PCM,AAC,DTS,AC3 etc.

* PAF_COMPONENT_ASP			-> inidcates the post processor e.g PL2X,NEO etc.

* PAF_COMPONENT_ENCODE		-> inidcates the PCE encoder.

* PAF_COMPONENT_OUTPUT		-> inidcates the output driver.

* PAF_COMPONENT_FW			-> inidcates the framework.

* PAF_COMPONENT_FW			-> inidcates either input driver or output driver.

*/

#define PAF_COMPONENT_UNKNOWN			(int)(0<<16)
#define PAF_COMPONENT_UNSUPPORTED		(int)(1<<16)
#define PAF_COMPONENT_INPUT				(int)(2<<16)
#define PAF_COMPONENT_DECODE			(int)(3<<16)
#define PAF_COMPONENT_ASP				(int)(4<<16)
#define PAF_COMPONENT_ENCODE			(int)(5<<16)
#define PAF_COMPONENT_OUTPUT			(int)(6<<16)
#define PAF_COMPONENT_FW				(int)(7<<16)
#define PAF_COMPONENT_IO				(int)(8<<16) /* Input or Output */





#define PAFERR_DEVINP           (PAF_COMPONENT_INPUT			  +0x700)
#define PAFERR_DEVOUT           (PAF_COMPONENT_OUTPUT			  +0x800)
#define PAFERR_ALGORITHM        (PAF_COMPONENT_UNSUPPORTED		  + 0x07)

#define PAFERR_AUTO_PROGRAM     (								   0xa01)
#define PAFERR_AUTO_LENGTH      (								   0xa02)

#define PAFERR_UNKNOWNSTATE     (									0x01)
#define PAFERR_ISSUE            (							  		0x02)
#define PAFERR_RECLAIM          (									0x03)
#define PAFERR_ABORT            (									0x04)
#define PAFERR_QUIT             (									0x05)


#define PAFERR_RESYNC       	(PAF_COMPONENT_INPUT		  		  +0x101)

#define PAFERR_INFO_PROGRAM     (								   0x201)
#define PAFERR_INFO_RATERATIO   (								   0x202)
#define PAFERR_INFO_RATECHANGE  (								   0x203)

#define PAFERR_MUTE             (								   0x400) /* 0x400-0x4ff */

#define PAFERR_IDLE             (								   0x500) /* 0x500-0x5ff */

#define PAFERR_RATE_CHECK       (								   0x680) /* 0x680-0x6ff */

#define PAFERR_PASS             (PAF_COMPONENT_UNKNOWN	     	  +0x900) /* 0x900-0x9ff */


#include <acperr.h>

#include <diberr.h>

#include <sngerr.h>
#include <pcmerr.h>
#if 0 /* commenting IP component header file inclusion */
#include <ac3err.h>
#include <dtserr.h>
#endif
#include <pceerr.h>

#include <bmerr.h>
#include <filerr.h>
#include <delerr.h>

#endif  /* PAFERR_ */
