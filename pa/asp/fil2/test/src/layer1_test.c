/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/  
// Unit test for All Layer-1 Filter implementation

// std headers
#include <xdc/std.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <limits.h>
#include <math.h>
#include <clk.h>
#include <log.h>
#include  <xdc/std.h>
#include <ti/xdais/xdas.h>
#include <paftyp.h>

// fil headers

#include <ifil.h>
#include <fil_tii.h>
#include <fil_tii_priv.h>
#include <filerr.h>
#include <filextern.h>
#include <filtyp.h>
#include <filters.h>
#include <fil_table.h>
#include <fil_macros.h>

#include <stdasp.h>

#include "layer1_test_table.h"


PAF_AudioFunctions audioFrameFunctions = {
    PAF_ASP_dB2ToLinear,
    PAF_ASP_channelMask,
	PAF_ASP_programFormat, 
	PAF_ASP_sampleRateHz,
    PAF_ASP_delay
   };

#define MAX_NUM_CHAN 16
#define MAX_NUM_SAMPLES 256

PAF_AudioFrame pAudioFrame;

float Audio_data[MAX_NUM_CHAN][MAX_NUM_SAMPLES];
float *pAudio[MAX_NUM_CHAN];



#define FIL_TEST_SAMPLE_COUNT  16
#define FIL_TEST_ITERATIONS    10

#define FIL_create(a, b)  (FIL_Handle)ALG_create((IALG_Fxns *)a , NULL, (IALG_Params*)b)
#define FIL_delete(a)	ALG_delete((ALG_Handle)a)

#pragma DATA_SECTION(varBuf,".varBuf")
unsigned char varBuf[( 8192 + 3 ) /4*4];


PAF_AudioData FilterCoeff0[256];
PAF_AudioData FilterCoeff1[256];
PAF_AudioData FilterCoeff2[256];
PAF_AudioData FilterCoeff3[256];
PAF_AudioData FilterCoeff4[256];
PAF_AudioData FilterCoeff5[256];
PAF_AudioData FilterCoeff6[256];
PAF_AudioData FilterCoeff7[256];

extern PAF_AudioData IIRFilterCoeff0[9];
extern PAF_AudioData IIRFilterCoeff1[9];
extern PAF_AudioData IIRFilterCoeff2[9];
extern PAF_AudioData IIRFilterCoeff3[9];
extern PAF_AudioData IIRFilterCoeff4[9];
extern PAF_AudioData IIRFilterCoeff5[9];
extern PAF_AudioData IIRFilterCoeff6[9];
extern PAF_AudioData IIRFilterCoeff7[9];

extern PAF_AudioData IIRFilterCoeff_32[9];
extern PAF_AudioData IIRFilterCoeff_44[9];
extern PAF_AudioData IIRFilterCoeff_48[9];
extern PAF_AudioData IIRFilterCoeff_64[9];
extern PAF_AudioData IIRFilterCoeff_88[9];
extern PAF_AudioData IIRFilterCoeff_96[9];
extern PAF_AudioData IIRFilterCoeff_128[9];
extern PAF_AudioData IIRFilterCoeff_192[9];


PAF_AudioData gInArray[2048];
PAF_AudioData gOutArrayRef[2048];
double gVarsRef[2048] = {0};

// Allocation for the param pointers 
PAF_AudioData *pIn[PAF_MAXNUMCHAN];
PAF_AudioData *pOut[PAF_MAXNUMCHAN];
double *pVar[PAF_MAXNUMCHAN];
PAF_AudioData *pCoef[PAF_MAXNUMCHAN];

// Param structure
PAF_FilParam gParamTest = {
 (void **)pIn,
 (void **)pOut,
 (void **)pCoef,
 (void **)pVar,
 FIL_TEST_SAMPLE_COUNT, // Int sampleCount
 0,			// Uchar channels
 0			// Uchar use
};

#define FIL_ERROR_THRESHOLD	 (0.000030517578125)      // - (15bit)

// 8388608 = 2^23
double BitError_array[] = 
{
    2/8388608.00, // 1
    4/8388608.00, // 2
    8/8388608.00, // 3
    16/8388608.00, // 4
    32/8388608.00, // 5
    64/8388608.00, // 6
    128/8388608.00, // 7
    256/8388608.00, // 8
    512/8388608.00, // 9
    1024/8388608.00, // 10
};
 
double testCase_error[TEST_DF2CASC];

struct FilterStructType{
 LgUns type;
 LgUns sampRate;
 PAF_AudioData *cPtr[12];
}FilterStruct = {
 0x00048042, 
 0x00000010, 
 {
  FilterCoeff0,
  FilterCoeff1,
  FilterCoeff2,
  FilterCoeff3,
  FilterCoeff4,
  FilterCoeff5,
  FilterCoeff6,
  FilterCoeff7,
  FilterCoeff6,
  FilterCoeff7,
  FilterCoeff6,
  FilterCoeff7
 }
};

IFIL_Status IR_PARAMS_STATUS = {
	sizeof(IFIL_Status), 
	0xdf37,              
	0x1,                 
	0x0001,              
	0x0                  
};

IFIL_Config IIIR_PARAMS_CONFIG = {
	sizeof(IFIL_Config),
	(const PAF_FilCoef *)&FilterStruct,
};

const IFIL_Params IR_PARAMS = {
	sizeof(IFIL_Params), 
	0x1,                 
	&IR_PARAMS_STATUS,
	&IIIR_PARAMS_CONFIG
};

extern testTable gTestArray_df2casc[TEST_DF2CASC];
extern testTable gTestArray_iir_mx1_df1[TEST_MX1_DF1];
extern testTable gTestArray_iir_mx1[TEST_IIR_MX1];
extern testTable gTestArray_fir[TEST_FIR];
extern testTable gTestArray_iir[TEST_IIR];
extern testTable gTestArray_iirdf2t[TEST_IIR_DF2T];
extern testTable gTestArray_df2tcasc[TEST_DF2CASC];

struct PAF_ALG_Fxns PAF_ALG_fxns =
{
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

void Audioframe_init(PAF_AudioFrame *pAudioFrame)
{
  int i;
  

  for(i=0; i < MAX_NUM_CHAN; i++ ) 		
    {
	 	pAudio[i] = Audio_data[i];
	 }
  pAudioFrame->sampleCount = FIL_TEST_SAMPLE_COUNT;  	
  pAudioFrame->data.sample = pAudio;
  pAudioFrame->sampleRate  =  PAF_SAMPLERATE_48000HZ;	
  pAudioFrame->channelConfigurationStream.part.sat = PAF_CC_SAT_SURROUND4;
  pAudioFrame->channelConfigurationStream.part.sub = PAF_CC_SUB_ONE;
  pAudioFrame->channelConfigurationStream.part.extMask = PAF_CC_EXTMASK_LwRw + PAF_CC_EXTMASK_LhRh ;
  pAudioFrame->pChannelConfigurationMaskTable = &PAF_ASP_stdCCMT;
  pAudioFrame->fxns = &audioFrameFunctions;

}

void PAF_ALG_delete(ALG_Handle handle)
{
}

void PAF_ALG_activate(ALG_Handle handle)
{
}

void PAF_ALG_deactivate(ALG_Handle handle)
{
}

static Int varsPerCh( Uint type )
{
    Int coeffPrec, bytesPerVar;
    Int i, count, adjPower2;

    coeffPrec = FilMacro_GetPrecField(type); /* Get coefficient precision */
    
    /* Identify the coeff prec and find the bytes/var-memory element */
    if( coeffPrec == PAF_AUDIODATATYPE_DOUBLE )
        bytesPerVar = 8;
    else if( coeffPrec == PAF_AUDIODATATYPE_FLOAT )
        bytesPerVar = 4;
    else if( coeffPrec == PAF_AUDIODATATYPE_INT )
        bytesPerVar = 4;
    else if( coeffPrec == PAF_AUDIODATATYPE_INT16 )
        bytesPerVar = 2;
    
    /* Switch to the given filter group */           
    switch(FilMacro_GetGroupField(type))
    {
        /* Impulse Response Group */
        case FIL_IR :
        {
            Int taps, subGroup;
            subGroup = FilMacro_GetSubGroupField(type);
            taps = FilMacro_GetIRTap(type) ;

            /* IIR Sub-group */
            if (subGroup == FIL_IR_IIR_DF1) {
#ifdef PAF_DEVICE
                if(FilMacro_GetIR_IIR_ProcField(type) == FIL_IR_IIR_PROC_MX1) {
                    bytesPerVar = 8; /* Internal processing is Double precision */
					if (taps == 2) return 24;		// MX1 T2 is DF1 implementation;
				}
#endif // PAF_DEVICE                    
 
                return( taps * bytesPerVar );
            }
			else if (subGroup == FIL_IR_IIR_DF2 || subGroup == FIL_IR_IIR_DF2T) {
#ifdef PAF_DEVICE
                if(FilMacro_GetIR_IIR_ProcField(type) == FIL_IR_IIR_PROC_MX1) {
                    bytesPerVar = 8; /* Internal processing is Double precision */
					if (taps == 2) return 24;		// MX1 T2 is DF1 implementation;
				}
#endif // PAF_DEVICE
				return (bytesPerVar * taps);	
			}
            /* FIR Sub-group */
            else
            {
            /* State mem is made ^2, for easy circularing */
                /* count=bits from MSB bit to left most '1' */                
                FilMacro_Left_1(taps, 16, count, i); 
                                
                adjPower2 = 0x8000 >> count;/* Get the ^2 just below 'taps' */
                /* If taps is not ^2 */
                if( taps & (~adjPower2) )
                    adjPower2 <<= 1; /* Shift, to get next ^2 no, above taps */
                                  
                return( adjPower2 * bytesPerVar );
            }
        } /* case FIL_IR */
               
        case FIL_CASCADE_IR :
        {
            Int taps, subGroup;
            subGroup = FilMacro_GetSubGroupField(type);

#ifdef PAF_DEVICE
            if(FilMacro_GetIR_IIR_ProcField(type) == FIL_IR_IIR_PROC_MX1)
                bytesPerVar = 8; /* Internal processing is Double precision */
#endif // PAF_DEVICE

            if( subGroup == FIL_IR_IIR_CASC_SOS_DF2 || subGroup == FIL_IR_IIR_CASC_SOS_DF2T)
            {
                // Get the no. of cascades 
                taps = FilMacro_GetIRTap(type) ;
                                
                // Return 2*cascades
                return( taps * bytesPerVar );
            }
            else
                return(FIL_ERROR);
        }
               
        default :  
            return(0);
            
    } /* switch(FilMacro_GetGroupField(type)) */

} /* Int FIL_varsPerCh() */

void error(char *fmt, ...)
{
    va_list ap;
    fprintf(stderr, "error: ");
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
    exit(2);
}
int generic_CASC_filter( PAF_FilParam *, testTable );
int iir_generic_cas( PAF_FilParam * );
int iir_generic1_cas( PAF_FilParam * );
int generic_FIR_filter( PAF_FilParam *, testTable);
int generic_IIRMX1_filter( PAF_FilParam *, testTable);
int generic_MX1DF1_filter( PAF_FilParam *, testTable);
int iirmx1_generic( PAF_FilParam *);
int iirmx1_generic1( PAF_FilParam *);
int iir_mx1df1_generic1(PAF_FilParam *);
int iir_mx1df1_generic(PAF_FilParam *);
int generic_IIRDF2T_filter( PAF_FilParam *, testTable);
int iirdf2t_generic_cas ( PAF_FilParam *);

int fil_test_casc(PAF_AudioFrame *pAudioFrame);
int fil_test_fir(PAF_AudioFrame *pAudioFrame);
int fil_test_iir_mx1(PAF_AudioFrame *pAudioFrame);
int fil_test_iir_mx1_df1(PAF_AudioFrame *pAudioFrame);
int fil_test_iir(PAF_AudioFrame *pAudioFrame);
int fil_test_iirdf2t(PAF_AudioFrame *pAudioFrame);
int fil_test_cascdf2t(PAF_AudioFrame *pAudioFrame);

void run_test(char *t)
{
    if (strcmp (t, "df2casc") == 0) {
		printf("Testing IIR Cacade DF2 Filters\n\n");
		fil_test_casc(&pAudioFrame);
	}
	else if(strcmp (t, "fir") == 0) {
		printf("Testing FIR Filters\n\n");
		fil_test_fir(&pAudioFrame);
	}
	else if(strcmp (t, "iirmx1") == 0) {
		printf("Testing IIR MX1 Filters\n\n");
		fil_test_iir_mx1(&pAudioFrame);
	}
	else if(strcmp (t, "mx1df1") == 0) {
		printf("Testing IIR MX1 DF1 Filters\n\n");
		fil_test_iir_mx1_df1(&pAudioFrame);
	}
	else if(strcmp (t, "iir") == 0) {
		printf("Testing Simple IIR \n\n");
		fil_test_iir(&pAudioFrame);
	}
	else if(strcmp (t, "iirdf2t") == 0) {
		printf("Testing Simple IIR in DF2t form\n\n");
		fil_test_iirdf2t(&pAudioFrame);
	}
	else if(strcmp (t, "df2tcasc") == 0) {
		printf("Testing IIR Cascade DF2t filters\n\n");
		fil_test_cascdf2t(&pAudioFrame);
	}
	else printf("Invalid Argument: Valid Arguments df2casc, fir, iirmx1, mx1df1, iirdf2t, df2tcasc \n");
}
int main(int c, char **v)
{
    char *tests[] = { "mx1df1", "iir", "iirmx1", "fir", "df2casc", "iirdf2t", "df2tcasc", NULL};
    int i;
    FIL_init();
	Audioframe_init(&pAudioFrame);

	if (c <= 1) {
        for (i = 0; tests[i]; ++i) {
            run_test(tests[i]);
        }
    } else {
        run_test(v[1]);
    }
	return 0;
}
int FIL_applyWrap(testTable *tab, int testCount, void *iirHandle, void *pAudioFrame)
{
    int ret;
    unsigned c1, c2;
    if (tab[testCount].ncalls == 0) {
        tab[testCount].max = 0;
        tab[testCount].min = 0xFFFFFFFF;
        tab[testCount].avg = 0;
    }
     c1 = clock();
    ret = FIL_apply(iirHandle, pAudioFrame);
     c2 = clock();
    if (c2 - c1 > tab[testCount].max)
        tab[testCount].max = c2 - c1;
    if (c2 - c1 < tab[testCount].min)
        tab[testCount].min = c2 - c1;
    tab[testCount].avg =
            (tab[testCount].ncalls*tab[testCount].avg +
                (double)(c2 - c1))/(double)(tab[testCount].ncalls+1);
    ++tab[testCount].ncalls;
    return ret;
}
#define print_stats(name, tab, testCount) \
            printf(name ## ": taps=%d, chan=%d, uniCoef=%d, inplace=%d, dPrec=%d, cPrec=%d, " \
                "cascade=%d, clocks=%d\n",  \
                tab[testCount].taps,  \
                tab[testCount].ch,  \
                tab[testCount].uniCoef,  \
                tab[testCount].inplace,  \
                tab[testCount].dPrec,  \
                tab[testCount].cPrec,  \
                tab[testCount].cascade,  \
                tab[testCount].max)
int fil_test_casc(PAF_AudioFrame *pAudioFrame)
{
    FIL_Handle iirHandle;
    int i,j,k, testIterations,ch,testCount,taps, varsperCh,cascade;
	void ** coeffsPtr; int coeffPtrPerCh = 0;void *varsPtr;
	Uint mask = 0x1;
	PAF_ChannelMask appCh, chTemplate;
	int sampIdx = 0;
	float error_max;void * pVars;
	pVars = varBuf;
	for( i = 0; i < 256; i += 2 )
    {
        FilterCoeff0[i] = ((i+1) & 0xF)*0.0015;
        FilterCoeff0[i+1] = ((i+2) & 0xF)*-0.0015;
		FilterCoeff1[i] = FilterCoeff2[i] = FilterCoeff3[i] = FilterCoeff4[i] = 
			FilterCoeff5[i] = FilterCoeff6[i] = FilterCoeff7[i] = FilterCoeff0[i] ; 
    	
    	FilterCoeff1[i+1]= FilterCoeff2[i+1] = FilterCoeff3[i+1] = FilterCoeff4[i+1] = 
    		FilterCoeff5[i+1] = FilterCoeff6[i+1] = FilterCoeff7[i+1] = FilterCoeff0[i+1] ;
	}
		for( i = 0; i < TEST_DF2CASC; i++ )
	        testCase_error[i] = 0.0;
	

	FilterStruct.cPtr[0] = FilterCoeff0;
	FilterStruct.cPtr[1] = FilterCoeff1;
	FilterStruct.cPtr[2] = FilterCoeff2;		
	FilterStruct.cPtr[3] = FilterCoeff3;		
	FilterStruct.cPtr[4] = FilterCoeff4;		
	FilterStruct.cPtr[5] = FilterCoeff5;		
	FilterStruct.cPtr[6] = FilterCoeff6;		
	FilterStruct.cPtr[7] = FilterCoeff7;

	for( testCount = 0; testCount < TEST_DF2CASC; testCount++ ) {

		FilterStruct.type = gTestArray_df2casc[testCount].type;
		IR_PARAMS_STATUS.maskSelect = gTestArray_df2casc[testCount].maskSelect;

		for( i = 0; i < 2048; i++ ) {
			gOutArrayRef[i] = 0;
	    	gVarsRef[i] = 0;
		}

		varsperCh = varsPerCh(gTestArray_df2casc[testCount].type);

		sampIdx = 0;

		error_max = 0.0;
		for(testIterations = 0; testIterations < FIL_TEST_ITERATIONS ; ++testIterations) {
			
			float error_value;
    		Char *charPtr = (Char *)pVars;
    
		    /* Reset the memory, byte-wise */
    		for( i=0; i < 8 * varsperCh; i++ )
        		*charPtr++ = 0;

			coeffPtrPerCh = 0;
			mask = 0x1;
			for (i = 0; i < 8192; ++i)
				varBuf[i] = 0;
			for( i = 0; i < 2048; i++ ) {
				gOutArrayRef[i] = 0;
	    		gVarsRef[i] = 0;
			}
		

    		for(j = 0; j < PAF_MAXNUMCHAN; ++j){
	    		for( i = 0; i < 256; i++ ) 
					pAudioFrame->data.sample[j][i] = (i & 0xF)* testIterations* 0.001;
			}
    		for( i = 0; i < 256; i++ )
				gInArray[i] = pAudioFrame->data.sample[0][i];

			ch   = gTestArray_df2casc[testCount].ch;
			taps = gTestArray_df2casc[testCount].taps;
			cascade = gTestArray_df2casc[testCount].cascade;
    	
    		gParamTest.channels = ch;
			gParamTest.use      = taps;
	        gParamTest.use     |= ( (cascade) << 16 );
			gParamTest.sampleCount = FIL_TEST_SAMPLE_COUNT;
	

    		coeffsPtr = (void *)&FilterStruct.cPtr[0];
			coeffsPtr  += sampIdx;
			varsPtr     = (void *)pVars;
			if (!gTestArray_df2casc[testCount].uniCoef)
				coeffPtrPerCh = 1;

    		appCh = gTestArray_df2casc[testCount].maskSelect;
    		chTemplate = IR_PARAMS_STATUS.mode;
    		for( i = 0, j = 0; i < PAF_MAXNUMCHAN; i++ )
		    {
        		/* If the channel bit is 1(select),fill its corresponding ptrs */
        		if(appCh & mask)
        		{
            		gParamTest.pIn[j] = gInArray;
					gParamTest.pVar[j] = varsPtr;
					gParamTest.pOut[j] = gOutArrayRef + j*FIL_TEST_SAMPLE_COUNT;
					gParamTest.pCoef[j] = *coeffsPtr;
            		j++;
        		}
        		/* If channel template has the next ch, offset the pointers */
        		if(chTemplate & mask)
        		{
        		    varsPtr   = (char *)varsPtr + varsperCh;
        		    coeffsPtr += coeffPtrPerCh;
        		}
        		/* Update for next iteration */
        		mask <<= 1;
    }
    		if((iirHandle = FIL_create((IALG_Fxns *)&FIL_TII_IFIL, (FIL_Params *)&IR_PARAMS)) != 0)
    		{
     			if (FIL_applyWrap(gTestArray_df2casc, testCount, iirHandle, pAudioFrame))
     				printf("Filter function call Returned Error");

    		} else {
				error("Memory Allocation failure");
			}

   			generic_CASC_filter( &gParamTest, gTestArray_df2casc[testCount] ); 
    
			for (i = 0,k=0; i < PAF_MAXNUMCHAN ; ++i) {
				
				if ((gTestArray_df2casc[testCount].maskSelect >> i ) & 1u) {
					for (j = 0; j < FIL_TEST_SAMPLE_COUNT; ++j) {
						//if ( fabs(gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j] - pAudioFrame->data.sample[i][j] ) > 0.00000005 )
						//	testfailure = 1;
						error_value = (gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j]  - pAudioFrame->data.sample[i][j]);
						if (error_value)
							error_value = fabs (error_value/ gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j]);
						
						if (error_value > FIL_ERROR_THRESHOLD)
					    	printf("Test case [%d], Iteration [%d] - failed(%ef) \n", testCount, testIterations, error_value );
				        if(fabs(error_value) > error_max)
				        	error_max = fabs(error_value);

					}
					k++;
				}
			}
		
    		FIL_reset(iirHandle, pAudioFrame);  //To clear the FilterStates
    		FIL_delete(iirHandle);
    		FIL_exit();
		}
		if(testCase_error[testCount] < error_max) 
            testCase_error[testCount] =  error_max;

        for(i = 0; i < 10; i++)
        {
        	if(testCase_error[testCount] <= BitError_array[i])
                break;
        }
		if((23-i) >= 15)
        	;//printf("Test case [%d], - %d bits match \n", testCount, (23 - i));
        else
            printf("Test case [%d], - %d bits match  (fail)\n", testCount, (23 - i));
        print_stats("DF2CASC", gTestArray_df2casc, testCount); 
                
	}
	return 0;


}

int fil_test_fir(PAF_AudioFrame *pAudioFrame) {
	FIL_Handle firHandle;
    int i,j,k, testIterations,ch,testCount,taps, varsperCh,cascade;
	void ** coeffsPtr; int coeffPtrPerCh = 0;void *varsPtr;
	Uint mask = 0x1;
	PAF_ChannelMask appCh, chTemplate;
	int sampIdx = 0, testfailure;
	void * pVars;

	for( i = 0; i < 256; i += 2 )
    {
        FilterCoeff0[i] = ((i+1) & 0xF)*0.0015;
        FilterCoeff0[i+1] = ((i+2) & 0xF)*-0.0015;
		FilterCoeff1[i] = FilterCoeff2[i] = FilterCoeff3[i] = FilterCoeff4[i] = 
			FilterCoeff5[i] = FilterCoeff6[i] = FilterCoeff7[i] = FilterCoeff0[i] ; 
    	
    	FilterCoeff1[i+1]= FilterCoeff2[i+1] = FilterCoeff3[i+1] = FilterCoeff4[i+1] = 
    		FilterCoeff5[i+1] = FilterCoeff6[i+1] = FilterCoeff7[i+1] = FilterCoeff0[i+1] ;
	}

	FilterStruct.cPtr[0] = FilterCoeff0;
	FilterStruct.cPtr[1] = FilterCoeff1;
	FilterStruct.cPtr[2] = FilterCoeff2;		
	FilterStruct.cPtr[3] = FilterCoeff3;		
	FilterStruct.cPtr[4] = FilterCoeff4;		
	FilterStruct.cPtr[5] = FilterCoeff5;		
	FilterStruct.cPtr[6] = FilterCoeff6;		
	FilterStruct.cPtr[7] = FilterCoeff7;

	pVars = varBuf;
	for( testCount = 0; testCount < TEST_FIR; testCount++ ) {

		FilterStruct.type = gTestArray_fir[testCount].type;
		IR_PARAMS_STATUS.maskSelect = gTestArray_fir[testCount].maskSelect;

		for( i = 0; i < 2048; i++ ) {
			gOutArrayRef[i] = 0;
	    	gVarsRef[i] = 0;
		}

		varsperCh = varsPerCh(gTestArray_fir[testCount].type);

		sampIdx = 0;

		for(testIterations = 0; testIterations < FIL_TEST_ITERATIONS ; ++testIterations) {
    		Char *charPtr = (Char *)pVars;
			sampIdx = 0;
    
		    /* Reset the memory, byte-wise */
    		for( i=0; i < 8 * varsperCh; i++ )
        		*charPtr++ = 0;

			coeffPtrPerCh = 0;
			mask = 0x1;
			for (i = 0; i < 8192; ++i)
				varBuf[i] = 0;
			for( i = 0; i < 2048; i++ ) {
				gOutArrayRef[i] = 0;
	    		gVarsRef[i] = 0;
			}
		

    		for(j = 0; j < PAF_MAXNUMCHAN; ++j){
	    		for( i = 0; i < 256; i++ ) 
					pAudioFrame->data.sample[j][i] = (i & 0xF)* (testIterations)* 0.001;
			}
    		for( i = 0; i < 256; i++ )
				gInArray[i] = pAudioFrame->data.sample[0][i];

			ch   = gTestArray_fir[testCount].ch;
			taps = gTestArray_fir[testCount].taps;
			cascade = gTestArray_fir[testCount].cascade;
    	
    		gParamTest.channels = ch;
			gParamTest.use      = taps;
	        gParamTest.use     |= ( (cascade) << 16 );
			gParamTest.sampleCount = FIL_TEST_SAMPLE_COUNT;
	

    		coeffsPtr = (void *)&FilterStruct.cPtr[0];
			coeffsPtr  += sampIdx;
			varsPtr     = (void *)pVars;
			if (!gTestArray_fir[testCount].uniCoef)
				coeffPtrPerCh = 1;

    		appCh = gTestArray_fir[testCount].maskSelect;
    		chTemplate = IR_PARAMS_STATUS.mode;
    		for( i = 0, j = 0; i < PAF_MAXNUMCHAN; i++ )
		    {
        		/* If the channel bit is 1(select),fill its corresponding ptrs */
        		if(appCh & mask)
        		{
            		gParamTest.pIn[j] = gInArray;
					gParamTest.pVar[j] = varsPtr;
					gParamTest.pOut[j] = gOutArrayRef + j*FIL_TEST_SAMPLE_COUNT;
					gParamTest.pCoef[j] = *coeffsPtr;
            		j++;
        		}
        		/* If channel template has the next ch, offset the pointers */
        		if(chTemplate & mask)
        		{
        		    varsPtr   = (char *)varsPtr + varsperCh;
        		    coeffsPtr += coeffPtrPerCh;
        		}
        		/* Update for next iteration */
        		mask <<= 1;
    }
    		if((firHandle = FIL_create((IALG_Fxns *)&FIL_TII_IFIL, (FIL_Params *)&IR_PARAMS)) != 0)
    		{
     			if (FIL_applyWrap(gTestArray_fir, testCount, firHandle, pAudioFrame))
     				printf("Filter function call Returned Error");

    		} else {
				error("Memory Allocation failure");
			}

   			generic_FIR_filter( &gParamTest, gTestArray_fir[testCount] ); 
    
			testfailure = 0;
			for (i = 0,k=0; i < PAF_MAXNUMCHAN ; ++i) {
				
				if ((gTestArray_fir[testCount].maskSelect >> i ) & 1u) {
					for (j = 0; j < FIL_TEST_SAMPLE_COUNT; ++j) {
						if ( fabs(gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j] - pAudioFrame->data.sample[i][j] ) > 0.00000005 )
							testfailure = 1;
					}
					k++;
				}
			}

			if (testfailure)
				printf("Test Failed at iteration %d for testCount %d, Channels tested = %d \n",testIterations,testCount, k) ; 

		
    		FIL_reset(firHandle, pAudioFrame);  //To clear the FilterStates
    		FIL_delete(firHandle);
    		FIL_exit();
            
            print_stats("FIR", gTestArray_fir, testCount);
		}
		if ( !testfailure)
			printf("Test Passed for TestCount %d\n", testCount);

	}
	return 0;
}

int fil_test_iir_mx1(PAF_AudioFrame *pAudioFrame) {

	FIL_Handle iirHandle;
    int i,j,k, testfailure = 0, testIterations,ch,testCount,taps, varsperCh;
	void ** coeffsPtr; int coeffPtrPerCh = 0;
	Uint mask = 0x1;
	PAF_ChannelMask appCh, chTemplate;
	int dSampleRate, sampIdx = 0, cSampRate;
	void * pVars;void *varsPtr;

	FilterStruct.cPtr[0] = IIRFilterCoeff0;
	FilterStruct.cPtr[1] = IIRFilterCoeff1;
	FilterStruct.cPtr[2] = IIRFilterCoeff2;		
	FilterStruct.cPtr[3] = IIRFilterCoeff3;		
	FilterStruct.cPtr[4] = IIRFilterCoeff4;		
	FilterStruct.cPtr[5] = IIRFilterCoeff5;		
	FilterStruct.cPtr[6] = IIRFilterCoeff6;		
	FilterStruct.cPtr[7] = IIRFilterCoeff7;	

	pVars = varBuf;
	for( testCount = 0; testCount < TEST_IIR_MX1; testCount++ ) {

		FilterStruct.type = gTestArray_iir_mx1[testCount].type;
		IR_PARAMS_STATUS.maskSelect = gTestArray_iir_mx1[testCount].maskSelect;

		for( i = 0; i < 2048; i++ ) {
			gOutArrayRef[i] = 0;
	    	gVarsRef[i] = 0;
		}

		varsperCh = varsPerCh(gTestArray_iir_mx1[testCount].type);

		testfailure = 0;
		
		sampIdx = 0;
		if( gTestArray_iir_mx1[testCount].type & FIL_BIT31 ) {
        
        	FilterStruct.sampRate = 0x000003fc;
			FilterStruct.cPtr[0] = IIRFilterCoeff_32;
			FilterStruct.cPtr[1] = IIRFilterCoeff_44;
			FilterStruct.cPtr[2] = IIRFilterCoeff_48;
			FilterStruct.cPtr[3] = IIRFilterCoeff_64;
			FilterStruct.cPtr[4] = IIRFilterCoeff_88;
			FilterStruct.cPtr[5] = IIRFilterCoeff_96;
			FilterStruct.cPtr[6] = IIRFilterCoeff_128;
			FilterStruct.cPtr[7] = IIRFilterCoeff_192;

			dSampleRate = pAudioFrame->sampleRate; 
        
        	/* Coefficient sample rate bit field */
        	cSampRate    = FilterStruct.sampRate;
        	    
        	/* Check whether Coeff structure contains sampRate matching the data */
        	if( !( cSampRate & ( 0x1 << dSampleRate ) ) )
        	    printf("Sample Rate Not supported.");
        	    
        	dSampleRate -= 2; /* Skip the 'none' and 'unknown' sample rates */
        	cSampRate  >>= 2; /* Do the same in coeff bit field also */
        
        	/* Find the no. of sample rates(1's) before the requested one(bit) */
        	FilMacro_1sToRight(cSampRate, dSampleRate, sampIdx, i);
        
		}
		for(testIterations = 0; testIterations < FIL_TEST_ITERATIONS ; ++testIterations) {
			
    		Char *charPtr = (Char *)pVars;
			coeffPtrPerCh = 0;
			mask = 0x1;
			for (i = 0; i < 8192; ++i)
				varBuf[i] = 0;
			for( i = 0; i < 2048; i++ ) {
				gOutArrayRef[i] = 0;
	    		gVarsRef[i] = 0;
			}
   
		    /* Reset the memory, byte-wise */
    		for( i=0; i < 8 * varsperCh; i++ )
        		*charPtr++ = 0;
    		for(j = 0; j < PAF_MAXNUMCHAN; ++j){
	    		for( i = 0; i < 256; i++ ) 
					pAudioFrame->data.sample[j][i] = i* testIterations* 0.001;
			}
    	
    		for( i = 0; i < 256; i++ )
				gInArray[i] = pAudioFrame->data.sample[0][i];

			ch   = gTestArray_iir_mx1[testCount].ch;
			taps = gTestArray_iir_mx1[testCount].taps;
    	
    		gParamTest.channels = ch;
			gParamTest.use      = taps;	
			gParamTest.sampleCount = FIL_TEST_SAMPLE_COUNT;	

    		varsPtr     = (void *)pVars;
    		coeffsPtr = (void *)&FilterStruct.cPtr[0];
			coeffsPtr  += sampIdx;
			if (!gTestArray_iir_mx1[testCount].uniCoef)
				coeffPtrPerCh = 1;

    		appCh = gTestArray_iir_mx1[testCount].maskSelect;
    		chTemplate = IR_PARAMS_STATUS.mode;
    		for( i = 0, j = 0; i < PAF_MAXNUMCHAN; i++ )
		    {
        		/* If the channel bit is 1(select),fill its corresponding ptrs */
        		if(appCh & mask)
        		{
            		gParamTest.pIn[j] = gInArray;
					gParamTest.pVar[j] = varsPtr;
					gParamTest.pOut[j] = gOutArrayRef + j*FIL_TEST_SAMPLE_COUNT;
					gParamTest.pCoef[j] = *coeffsPtr;
            		j++;
        		}
        		/* If channel template has the next ch, offset the pointers */
        		if(chTemplate & mask)
        		{
        		    varsPtr   = (char *)varsPtr + varsperCh;
        		    coeffsPtr += coeffPtrPerCh;
        		}
        		/* Update for next iteration */
        		mask <<= 1;
    }
    		if((iirHandle = FIL_create((IALG_Fxns *)&FIL_TII_IFIL, (FIL_Params *)&IR_PARAMS)) != 0)
    		{
     			if (FIL_applyWrap(gTestArray_iir_mx1, testCount, iirHandle, pAudioFrame))
     				printf("Filter function call Returned Error");

    		} else {
				error("Memory Allocation failure");
			}

   			generic_IIRMX1_filter( &gParamTest, gTestArray_iir_mx1[testCount] ); 
    
			for (i = 0,k=0; i < PAF_MAXNUMCHAN ; ++i) {
				
				if ((gTestArray_iir_mx1[testCount].maskSelect >> i ) & 1u) {
					for (j = 0; j < FIL_TEST_SAMPLE_COUNT; ++j) {
						if ( fabs(gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j] - pAudioFrame->data.sample[i][j] ) > 0.00000005 )
							testfailure = 1;
					}
					k++;
				}
			}
		
			if (testfailure)
				printf("Test Failed at iteration %d for testCount %d, Channels tested = %d \n",testIterations,testCount, k) ; 
    		FIL_reset(iirHandle, pAudioFrame);  //To clear the FilterStates
    		FIL_delete(iirHandle);
    		FIL_exit();
            print_stats("IIR_MX1", gTestArray_iir_mx1, testCount);
		}
		if ( !testfailure)
			printf("Test Passed for TestCount %d\n", testCount);
	}
	return 0;
}

int fil_test_iir_mx1_df1(PAF_AudioFrame *pAudioFrame)
{
	FIL_Handle iirHandle;
    int i,j,k, testfailure = 0, testIterations,ch,testCount,taps, varsperCh;
	void ** coeffsPtr; int coeffPtrPerCh = 0;
	Uint mask = 0x1;
	PAF_ChannelMask appCh, chTemplate;
	int dSampleRate, sampIdx = 0, cSampRate;
	void * pVars;void *varsPtr;

	FilterStruct.cPtr[0] = IIRFilterCoeff0;
	FilterStruct.cPtr[1] = IIRFilterCoeff1;
	FilterStruct.cPtr[2] = IIRFilterCoeff2;		
	FilterStruct.cPtr[3] = IIRFilterCoeff3;		
	FilterStruct.cPtr[4] = IIRFilterCoeff4;		
	FilterStruct.cPtr[5] = IIRFilterCoeff5;		
	FilterStruct.cPtr[6] = IIRFilterCoeff6;		
	FilterStruct.cPtr[7] = IIRFilterCoeff7;	

	pVars = varBuf;
	for( testCount = 0; testCount < TEST_MX1_DF1; testCount++ ) {

		FilterStruct.type = gTestArray_iir_mx1_df1[testCount].type;
		IR_PARAMS_STATUS.maskSelect = gTestArray_iir_mx1_df1[testCount].maskSelect;

		for( i = 0; i < 2048; i++ ) {
			gOutArrayRef[i] = 0;
	    	gVarsRef[i] = 0;
		}

		varsperCh = varsPerCh(gTestArray_iir_mx1_df1[testCount].type);

		testfailure = 0;
		
		sampIdx = 0;
		if( gTestArray_iir_mx1_df1[testCount].type & FIL_BIT31 ) {
        
        	FilterStruct.sampRate = 0x000003fc;
			FilterStruct.cPtr[0] = IIRFilterCoeff_32;
			FilterStruct.cPtr[1] = IIRFilterCoeff_44;
			FilterStruct.cPtr[2] = IIRFilterCoeff_48;
			FilterStruct.cPtr[3] = IIRFilterCoeff_64;
			FilterStruct.cPtr[4] = IIRFilterCoeff_88;
			FilterStruct.cPtr[5] = IIRFilterCoeff_96;
			FilterStruct.cPtr[6] = IIRFilterCoeff_128;
			FilterStruct.cPtr[7] = IIRFilterCoeff_192;

			dSampleRate = pAudioFrame->sampleRate; 
        
        	/* Coefficient sample rate bit field */
        	cSampRate    = FilterStruct.sampRate;
        	    
        	/* Check whether Coeff structure contains sampRate matching the data */
        	if( !( cSampRate & ( 0x1 << dSampleRate ) ) )
        	    printf("Sample Rate Not supported.");
        	    
        	dSampleRate -= 2; /* Skip the 'none' and 'unknown' sample rates */
        	cSampRate  >>= 2; /* Do the same in coeff bit field also */
        
        	/* Find the no. of sample rates(1's) before the requested one(bit) */
        	FilMacro_1sToRight(cSampRate, dSampleRate, sampIdx, i);
        
		}
		for(testIterations = 0; testIterations < FIL_TEST_ITERATIONS ; ++testIterations) {
			
    		Char *charPtr = (Char *)pVars;
			coeffPtrPerCh = 0;
			mask = 0x1;
			for (i = 0; i < 8192; ++i)
				varBuf[i] = 0;
			for( i = 0; i < 2048; i++ ) {
				gOutArrayRef[i] = 0;
	    		gVarsRef[i] = 0;
			}
   
		    /* Reset the memory, byte-wise */
    		for( i=0; i < 8 * varsperCh; i++ )
        		*charPtr++ = 0;
    		for(j = 0; j < PAF_MAXNUMCHAN; ++j){
	    		for( i = 0; i < 256; i++ ) 
					pAudioFrame->data.sample[j][i] = i* (testIterations+1)* 0.001;
			}
    	
    		for( i = 0; i < 256; i++ )
				gInArray[i] = pAudioFrame->data.sample[0][i];

			ch   = gTestArray_iir_mx1_df1[testCount].ch;
			taps = gTestArray_iir_mx1_df1[testCount].taps;
    	
    		gParamTest.channels = ch;
			gParamTest.use      = taps;	
			gParamTest.sampleCount = FIL_TEST_SAMPLE_COUNT;	

    		varsPtr     = (void *)pVars;
    		coeffsPtr = (void *)&FilterStruct.cPtr[0];
			coeffsPtr  += sampIdx;
			if (!gTestArray_iir_mx1_df1[testCount].uniCoef)
				coeffPtrPerCh = 1;

    		appCh = gTestArray_iir_mx1_df1[testCount].maskSelect;
    		chTemplate = IR_PARAMS_STATUS.mode;
    		for( i = 0, j = 0; i < PAF_MAXNUMCHAN; i++ )
		    {
        		/* If the channel bit is 1(select),fill its corresponding ptrs */
        		if(appCh & mask)
        		{
            		gParamTest.pIn[j] = gInArray;
					gParamTest.pVar[j] = varsPtr;
					gParamTest.pOut[j] = gOutArrayRef + j*FIL_TEST_SAMPLE_COUNT;
					gParamTest.pCoef[j] = *coeffsPtr;
            		j++;
        		}
        		/* If channel template has the next ch, offset the pointers */
        		if(chTemplate & mask)
        		{
        		    varsPtr   = (char *)varsPtr + varsperCh;
        		    coeffsPtr += coeffPtrPerCh;
        		}
        		/* Update for next iteration */
        		mask <<= 1;
    }
    		if((iirHandle = FIL_create((IALG_Fxns *)&FIL_TII_IFIL, (FIL_Params *)&IR_PARAMS)) != 0)
    		{
     			if (FIL_applyWrap(gTestArray_iir_mx1_df1, testCount, iirHandle, pAudioFrame))
     				printf("Filter function call Returned Error");

    		} else {
				error("Memory Allocation failure");
			}

   			generic_MX1DF1_filter( &gParamTest, gTestArray_iir_mx1_df1[testCount] ); 
    
			for (i = 0,k=0; i < PAF_MAXNUMCHAN ; ++i) {
				
				if ((gTestArray_iir_mx1_df1[testCount].maskSelect >> i ) & 1u) {
					for (j = 0; j < FIL_TEST_SAMPLE_COUNT; ++j) {
						if ( fabs(gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j] - pAudioFrame->data.sample[i][j] ) > 0.00000005 ) {
							testfailure = 1;
						}
					}
					k++;
				}
			}
		
			if (testfailure)
				printf("Test Failed at iteration %d for testCount %d, Channels tested = %d \n",testIterations,testCount, k) ; 
    		FIL_reset(iirHandle, pAudioFrame);  //To clear the FilterStates
    		FIL_delete(iirHandle);
    		FIL_exit();
            print_stats("IIR_MX_DF1", gTestArray_iir_mx1_df1, testCount);
		}
		if ( !testfailure)
			printf("Test Passed for TestCount %d\n", testCount);
	}
	return 0;
}

int fil_test_iir(PAF_AudioFrame *pAudioFrame) {
	FIL_Handle iirHandle;
    int i,j,k, testfailure = 0, testIterations,ch,testCount,taps, varsperCh;
	void ** coeffsPtr; int coeffPtrPerCh = 0;
	Uint mask = 0x1;
	PAF_ChannelMask appCh, chTemplate;
	int dSampleRate, sampIdx = 0, cSampRate;
	void * pVars;void *varsPtr;

	for( i = 0; i < 256; i += 2 )
    {
        FilterCoeff0[i] = ((i+1) & 0xF)*0.0015;
        FilterCoeff0[i+1] = ((i+2) & 0xF)*-0.0015;
		FilterCoeff1[i] = FilterCoeff2[i] = FilterCoeff3[i] = FilterCoeff4[i] = 
			FilterCoeff5[i] = FilterCoeff6[i] = FilterCoeff7[i] = FilterCoeff0[i] ; 
    	
    	FilterCoeff1[i+1]= FilterCoeff2[i+1] = FilterCoeff3[i+1] = FilterCoeff4[i+1] = 
    		FilterCoeff5[i+1] = FilterCoeff6[i+1] = FilterCoeff7[i+1] = FilterCoeff0[i+1] ;
	}	

	FilterStruct.cPtr[0] = FilterCoeff0;
	FilterStruct.cPtr[1] = FilterCoeff1;
	FilterStruct.cPtr[2] = FilterCoeff2;		
	FilterStruct.cPtr[3] = FilterCoeff3;		
	FilterStruct.cPtr[4] = FilterCoeff4;		
	FilterStruct.cPtr[5] = FilterCoeff5;		
	FilterStruct.cPtr[6] = FilterCoeff6;		
	FilterStruct.cPtr[7] = FilterCoeff7;

	pVars = varBuf;
	for( testCount = 0; testCount < TEST_IIR; testCount++ ) {

		FilterStruct.type = gTestArray_iir[testCount].type;
		IR_PARAMS_STATUS.maskSelect = gTestArray_iir[testCount].maskSelect;

		for( i = 0; i < 2048; i++ ) {
			gOutArrayRef[i] = 0;
	    	gVarsRef[i] = 0;
		}

		varsperCh = varsPerCh(gTestArray_iir[testCount].type);

		testfailure = 0;
		
		sampIdx = 0;
		for(testIterations = 0; testIterations < FIL_TEST_ITERATIONS ; ++testIterations) {
			
    		Char *charPtr = (Char *)pVars;
			coeffPtrPerCh = 0;
			mask = 0x1;
			for (i = 0; i < 8192; ++i)
				varBuf[i] = 0;
			for( i = 0; i < 2048; i++ ) {
				gOutArrayRef[i] = 0;
	    		gVarsRef[i] = 0;
			}
   
		    /* Reset the memory, byte-wise */
    		for( i=0; i < 8 * varsperCh; i++ )
        		*charPtr++ = 0;
    		for(j = 0; j < PAF_MAXNUMCHAN; ++j){
	    		for( i = 0; i < 256; i++ ) 
					pAudioFrame->data.sample[j][i] = i* testIterations* 0.001;
			}
    	
    		for( i = 0; i < 256; i++ )
				gInArray[i] = pAudioFrame->data.sample[0][i];

			ch   = gTestArray_iir[testCount].ch;
			taps = gTestArray_iir[testCount].taps;
    	
    		gParamTest.channels = ch;
			gParamTest.use      = taps;	
			gParamTest.sampleCount = FIL_TEST_SAMPLE_COUNT;	

    		varsPtr     = (void *)pVars;
    		coeffsPtr = (void *)&FilterStruct.cPtr[0];
			coeffsPtr  += sampIdx;
			if (!gTestArray_iir[testCount].uniCoef)
				coeffPtrPerCh = 1;

    		appCh = gTestArray_iir[testCount].maskSelect;
    		chTemplate = IR_PARAMS_STATUS.mode;
    		for( i = 0, j = 0; i < PAF_MAXNUMCHAN; i++ )
		    {
        		/* If the channel bit is 1(select),fill its corresponding ptrs */
        		if(appCh & mask)
        		{
            		gParamTest.pIn[j] = gInArray;
					gParamTest.pVar[j] = varsPtr;
					gParamTest.pOut[j] = gOutArrayRef + j*FIL_TEST_SAMPLE_COUNT;
					gParamTest.pCoef[j] = *coeffsPtr;
            		j++;
        		}
        		/* If channel template has the next ch, offset the pointers */
        		if(chTemplate & mask)
        		{
        		    varsPtr   = (char *)varsPtr + varsperCh;
        		    coeffsPtr += coeffPtrPerCh;
        		}
        		/* Update for next iteration */
        		mask <<= 1;
    		}
    		if((iirHandle = FIL_create((IALG_Fxns *)&FIL_TII_IFIL, (FIL_Params *)&IR_PARAMS)) != 0)
    		{
     			if (FIL_applyWrap(gTestArray_iir, testCount, iirHandle, pAudioFrame))
     				printf("Filter function call Returned Error");
    		} else {
				error("Memory Allocation failure");
			}

   			generic_IIR_filter( &gParamTest, gTestArray_iir[testCount] ); 
    
			for (i = 0,k=0; i < PAF_MAXNUMCHAN ; ++i) {
				
				if ((gTestArray_iir[testCount].maskSelect >> i ) & 1u) {
					for (j = 0; j < FIL_TEST_SAMPLE_COUNT; ++j) {
						if ( fabs(gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j] - pAudioFrame->data.sample[i][j] ) > 0.00000005 )
							testfailure = 1;
					}
					k++;
				}
			}
		
			if (testfailure)
				printf("Test Failed at iteration %d for testCount %d, Channels tested = %d \n",testIterations,testCount, k) ; 
    		FIL_reset(iirHandle, pAudioFrame);  //To clear the FilterStates
    		FIL_delete(iirHandle);
    		FIL_exit();
			print_stats("IIR", gTestArray_iir, testCount);
		}
		if ( !testfailure)
			printf("Test Passed for TestCount %d\n", testCount);
	}
	return 0;
}

int fil_test_iirdf2t(PAF_AudioFrame *pAudioFrame) {
	FIL_Handle iirHandle;
    int i,j,k, testfailure = 0, testIterations,ch,testCount,taps, varsperCh;
	void ** coeffsPtr; int coeffPtrPerCh = 0;
	Uint mask = 0x1;
	PAF_ChannelMask appCh, chTemplate;
	int dSampleRate, sampIdx = 0, cSampRate;
	void * pVars;void *varsPtr;

	for( i = 0; i < 256; i += 2 )
    {
        FilterCoeff0[i] = ((i+1) & 0xF)*0.0015;
        FilterCoeff0[i+1] = ((i+2) & 0xF)*-0.0015;
		FilterCoeff1[i] = FilterCoeff2[i] = FilterCoeff3[i] = FilterCoeff4[i] = 
			FilterCoeff5[i] = FilterCoeff6[i] = FilterCoeff7[i] = FilterCoeff0[i] ; 
    	
    	FilterCoeff1[i+1]= FilterCoeff2[i+1] = FilterCoeff3[i+1] = FilterCoeff4[i+1] = 
    		FilterCoeff5[i+1] = FilterCoeff6[i+1] = FilterCoeff7[i+1] = FilterCoeff0[i+1] ;
	}	

	FilterStruct.cPtr[0] = FilterCoeff0;
	FilterStruct.cPtr[1] = FilterCoeff1;
	FilterStruct.cPtr[2] = FilterCoeff2;		
	FilterStruct.cPtr[3] = FilterCoeff3;		
	FilterStruct.cPtr[4] = FilterCoeff4;		
	FilterStruct.cPtr[5] = FilterCoeff5;		
	FilterStruct.cPtr[6] = FilterCoeff6;		
	FilterStruct.cPtr[7] = FilterCoeff7;

	pVars = varBuf;
	for( testCount = 0; testCount < TEST_IIR_DF2T; testCount++ ) {

		FilterStruct.type = gTestArray_iirdf2t[testCount].type;
		IR_PARAMS_STATUS.maskSelect = gTestArray_iirdf2t[testCount].maskSelect;

		for( i = 0; i < 2048; i++ ) {
			gOutArrayRef[i] = 0;
	    	gVarsRef[i] = 0;
		}

		varsperCh = varsPerCh(gTestArray_iirdf2t[testCount].type);

		testfailure = 0;
		
		sampIdx = 0;
		for(testIterations = 0; testIterations < FIL_TEST_ITERATIONS ; ++testIterations) {
			
    		Char *charPtr = (Char *)pVars;
			coeffPtrPerCh = 0;
			mask = 0x1;
			for (i = 0; i < 8192; ++i)
				varBuf[i] = 0;
			for( i = 0; i < 2048; i++ ) {
				gOutArrayRef[i] = 0;
	    		gVarsRef[i] = 0;
			}
   
		    /* Reset the memory, byte-wise */
    		for( i=0; i < 8 * varsperCh; i++ )
        		*charPtr++ = 0;
    		for(j = 0; j < PAF_MAXNUMCHAN; ++j){
	    		for( i = 0; i < 256; i++ ) 
					pAudioFrame->data.sample[j][i] = i* testIterations* 0.001;
			}
    	
    		for( i = 0; i < 256; i++ )
				gInArray[i] = pAudioFrame->data.sample[0][i];

			ch   = gTestArray_iirdf2t[testCount].ch;
			taps = gTestArray_iirdf2t[testCount].taps;
    	
    		gParamTest.channels = ch;
			gParamTest.use      = taps;	
			gParamTest.sampleCount = FIL_TEST_SAMPLE_COUNT;	

    		varsPtr     = (void *)pVars;
    		coeffsPtr = (void *)&FilterStruct.cPtr[0];
			coeffsPtr  += sampIdx;
			if (!gTestArray_iirdf2t[testCount].uniCoef)
				coeffPtrPerCh = 1;

    		appCh = gTestArray_iirdf2t[testCount].maskSelect;
    		chTemplate = IR_PARAMS_STATUS.mode;
    		for( i = 0, j = 0; i < PAF_MAXNUMCHAN; i++ )
		    {
        		/* If the channel bit is 1(select),fill its corresponding ptrs */
        		if(appCh & mask)
        		{
            		gParamTest.pIn[j] = gInArray;
					gParamTest.pVar[j] = varsPtr;
					gParamTest.pOut[j] = gOutArrayRef + j*FIL_TEST_SAMPLE_COUNT;
					gParamTest.pCoef[j] = *coeffsPtr;
            		j++;
        		}
        		/* If channel template has the next ch, offset the pointers */
        		if(chTemplate & mask)
        		{
        		    varsPtr   = (char *)varsPtr + varsperCh;
        		    coeffsPtr += coeffPtrPerCh;
        		}
        		/* Update for next iteration */
        		mask <<= 1;
    		}
    		if((iirHandle = FIL_create((IALG_Fxns *)&FIL_TII_IFIL, (FIL_Params *)&IR_PARAMS)) != 0)
    		{
     			if (FIL_applyWrap(gTestArray_iirdf2t, testCount, iirHandle, pAudioFrame))
     				printf("Filter function call Returned Error");
    		} else {
				error("Memory Allocation failure");
			}

   			generic_IIRDF2T_filter( &gParamTest, gTestArray_iirdf2t[testCount] ); 
    
			for (i = 0,k=0; i < PAF_MAXNUMCHAN ; ++i) {
				
				if ((gTestArray_iirdf2t[testCount].maskSelect >> i ) & 1u) {
					for (j = 0; j < FIL_TEST_SAMPLE_COUNT; ++j) {
						if ( fabs(gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j] - pAudioFrame->data.sample[i][j] ) > 0.00000005 )
							testfailure = 1;
					}
					k++;
				}
			}
		
			if (testfailure)
				printf("Test Failed at iteration %d for testCount %d, Channels tested = %d \n",testIterations,testCount, k) ; 
    		FIL_reset(iirHandle, pAudioFrame);  //To clear the FilterStates
    		FIL_delete(iirHandle);
    		FIL_exit();
			print_stats("IIRDF2T", gTestArray_iirdf2t, testCount);
		}
		if ( !testfailure)
			printf("Test Passed for TestCount %d\n", testCount);
	}
	return 0;
}


int fil_test_cascdf2t(PAF_AudioFrame *pAudioFrame)
{
    FIL_Handle iirHandle;
    int i,j,k, testIterations,ch,testCount,taps, varsperCh,cascade;
	void ** coeffsPtr; int coeffPtrPerCh = 0;void *varsPtr;
	Uint mask = 0x1;
	PAF_ChannelMask appCh, chTemplate;
	int sampIdx = 0;
	float error_max;void * pVars;
	pVars = varBuf;
	for( i = 0; i < 256; i += 2 )
    {
        FilterCoeff0[i] = ((i+1) & 0xF)*0.0015;
        FilterCoeff0[i+1] = ((i+2) & 0xF)*-0.0015;
		FilterCoeff1[i] = FilterCoeff2[i] = FilterCoeff3[i] = FilterCoeff4[i] = 
			FilterCoeff5[i] = FilterCoeff6[i] = FilterCoeff7[i] = FilterCoeff0[i] ; 
    	
    	FilterCoeff1[i+1]= FilterCoeff2[i+1] = FilterCoeff3[i+1] = FilterCoeff4[i+1] = 
    		FilterCoeff5[i+1] = FilterCoeff6[i+1] = FilterCoeff7[i+1] = FilterCoeff0[i+1] ;
	}
		for( i = 0; i < TEST_DF2CASC; i++ )
	        testCase_error[i] = 0.0;
	

	FilterStruct.cPtr[0] = FilterCoeff0;
	FilterStruct.cPtr[1] = FilterCoeff1;
	FilterStruct.cPtr[2] = FilterCoeff2;		
	FilterStruct.cPtr[3] = FilterCoeff3;		
	FilterStruct.cPtr[4] = FilterCoeff4;		
	FilterStruct.cPtr[5] = FilterCoeff5;		
	FilterStruct.cPtr[6] = FilterCoeff6;		
	FilterStruct.cPtr[7] = FilterCoeff7;

	for( testCount = 0; testCount < TEST_DF2CASC; testCount++ ) {

		FilterStruct.type = gTestArray_df2tcasc[testCount].type;
		IR_PARAMS_STATUS.maskSelect = gTestArray_df2tcasc[testCount].maskSelect;

		for( i = 0; i < 2048; i++ ) {
			gOutArrayRef[i] = 0;
	    	gVarsRef[i] = 0;
		}

		varsperCh = varsPerCh(gTestArray_df2tcasc[testCount].type);

		sampIdx = 0;

		error_max = 0.0;
		for(testIterations = 0; testIterations < FIL_TEST_ITERATIONS ; ++testIterations) {
			
			float error_value;
    		Char *charPtr = (Char *)pVars;
    
		    /* Reset the memory, byte-wise */
    		for( i=0; i < 8 * varsperCh; i++ )
        		*charPtr++ = 0;

			coeffPtrPerCh = 0;
			mask = 0x1;
			for (i = 0; i < 8192; ++i)
				varBuf[i] = 0;
			for( i = 0; i < 2048; i++ ) {
				gOutArrayRef[i] = 0;
	    		gVarsRef[i] = 0;
			}
		

    		for(j = 0; j < PAF_MAXNUMCHAN; ++j){
	    		for( i = 0; i < 256; i++ ) 
					pAudioFrame->data.sample[j][i] = (i & 0xF)* testIterations* 0.001;
			}
    		for( i = 0; i < 256; i++ )
				gInArray[i] = pAudioFrame->data.sample[0][i];

			ch   = gTestArray_df2tcasc[testCount].ch;
			taps = gTestArray_df2tcasc[testCount].taps;
			cascade = gTestArray_df2tcasc[testCount].cascade;
    	
    		gParamTest.channels = ch;
			gParamTest.use      = taps;
	        gParamTest.use     |= ( (cascade) << 16 );
			gParamTest.sampleCount = FIL_TEST_SAMPLE_COUNT;
	

    		coeffsPtr = (void *)&FilterStruct.cPtr[0];
			coeffsPtr  += sampIdx;
			varsPtr     = (void *)pVars;
			if (!gTestArray_df2tcasc[testCount].uniCoef)
				coeffPtrPerCh = 1;

    		appCh = gTestArray_df2tcasc[testCount].maskSelect;
    		chTemplate = IR_PARAMS_STATUS.mode;
    		for( i = 0, j = 0; i < PAF_MAXNUMCHAN; i++ )
		    {
        		/* If the channel bit is 1(select),fill its corresponding ptrs */
        		if(appCh & mask)
        		{
            		gParamTest.pIn[j] = gInArray;
					gParamTest.pVar[j] = varsPtr;
					gParamTest.pOut[j] = gOutArrayRef + j*FIL_TEST_SAMPLE_COUNT;
					gParamTest.pCoef[j] = *coeffsPtr;
            		j++;
        		}
        		/* If channel template has the next ch, offset the pointers */
        		if(chTemplate & mask)
        		{
        		    varsPtr   = (char *)varsPtr + varsperCh;
        		    coeffsPtr += coeffPtrPerCh;
        		}
        		/* Update for next iteration */
        		mask <<= 1;
    }
    		if((iirHandle = FIL_create((IALG_Fxns *)&FIL_TII_IFIL, (FIL_Params *)&IR_PARAMS)) != 0)
    		{
     			if (FIL_applyWrap(gTestArray_df2tcasc, testCount, iirHandle, pAudioFrame))
     				printf("Filter function call Returned Error");

    		} else {
				error("Memory Allocation failure");
			}

   			generic_CASCDF2T_filter( &gParamTest, gTestArray_df2tcasc[testCount] ); 
    
			for (i = 0,k=0; i < PAF_MAXNUMCHAN ; ++i) {
				
				if ((gTestArray_df2tcasc[testCount].maskSelect >> i ) & 1u) {
					for (j = 0; j < FIL_TEST_SAMPLE_COUNT; ++j) {
						//if ( fabs(gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j] - pAudioFrame->data.sample[i][j] ) > 0.00000005 )
						//	testfailure = 1;
						error_value = fabs( (gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j]  - pAudioFrame->data.sample[i][j])/gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j] );
						/*error_value = (gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j]  - pAudioFrame->data.sample[i][j]);
						if (error_value)
							error_value = fabs (error_value/ gOutArrayRef[(k * FIL_TEST_SAMPLE_COUNT) + j]);*/
						if (error_value > FIL_ERROR_THRESHOLD)
					    	printf("Test case [%d], Iteration [%d], Channel [%d] - failed(%ef) \n", testCount, testIterations, i , error_value );
				        if(fabs(error_value) > error_max)
				        	error_max = fabs(error_value);

					}
					k++;
				}
			}
		
    		FIL_reset(iirHandle, pAudioFrame);  //To clear the FilterStates
    		FIL_delete(iirHandle);
    		FIL_exit();
		}
		if(testCase_error[testCount] < error_max) 
            testCase_error[testCount] =  error_max;

        for(i = 0; i < 10; i++)
        {
        	if(testCase_error[testCount] <= BitError_array[i])
                break;
        }
		if((23-i) >= 15)
        	;//printf("Test case [%d], - %d bits match \n", testCount, (23 - i));
        else
            printf("Test case [%d], - %d bits match  (fail)\n", testCount, (23 - i));
        print_stats("DF2TCASC", gTestArray_df2tcasc, testCount); 
                
	}
	return 0;


} 

int generic_CASC_filter( PAF_FilParam * param, testTable testElement)
{
    if( (testElement.dPrec == 1) && (testElement.cPrec == 0) )
        return( iir_generic1_cas( param ) );    
    else    
        return( iir_generic_cas( param ) );    
  
}

int iir_generic_cas( PAF_FilParam * pParam )
{
    Int count;
    Int i, t, taps, ch, casc, c;
    PAF_AudioData accY, accW;
    PAF_AudioData input;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtVars;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    PAF_AudioData filtVarsTemp0, filtVarsTemp1;
    
    count = pParam->sampleCount;
    taps  = (pParam->use) & 0xFFFF;
    casc  = ( (pParam->use) & 0xFFFF0000 ) >> 16;	

    for (ch = 0; ch < pParam->channels; ch++){
    	x = (PAF_AudioData *)pParam->pIn[ch];
		y = (PAF_AudioData *)pParam->pOut[ch];
			   
		for (i = 0; i < count; i++){
			filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
			input 	  =  filtCfsB[0] * x[i];
		    
		    for (c = 0; c < casc; c++){
		    	
		   		filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch] + c*taps*2 + 1;
		    	filtCfsA  = filtCfsB  + taps;
		    	filtVars  = (PAF_AudioData *)pParam->pVar[ch] + c*taps;
		    		
		    	accW = input;	
		    	accY = 0; 
		             
		      	for ( t = 0; t < taps; t++ ){
		      		accW += filtCfsA[t] * filtVars[t];   
		            accY += filtCfsB[t] * filtVars[t];
		        }
		            
		        accY += accW;
		        input = accY;
		        filtVarsTemp0 = filtVars[0];
		             
		        for	( t = 0; t < taps; t++ ){
		            
		            filtVarsTemp1 =  filtVars[t + 1];
		            filtVars[t + 1] = filtVarsTemp0;		                 
		            filtVarsTemp0 =  filtVarsTemp1; 
		        }
		                
		        filtVars[taps] = filtVarsTemp1;          
		        filtVars[0] = accW;
		             
	   		}
		         
			y[i] = input;
		}
		     
	}
	
	 
     return( FIL_SUCCESS );      
}
   
   
   
int iir_generic1_cas( PAF_FilParam * pParam )
{
    Int count;
    Int i, t, taps, ch;
    PAF_AudioData accY, accW;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtVars;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    PAF_AudioData filtVarsTemp0, filtVarsTemp1;
    
    count = pParam->sampleCount;
    taps  = pParam->use;
    
    for (ch = 0; ch < pParam->channels; ch++)
    {
        x = (PAF_AudioData *)pParam->pIn[ch];
        y = (PAF_AudioData *)pParam->pOut[ch];            

        filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;

        filtVars  = (PAF_AudioData *)pParam->pVar[ch];
        
        for (i = 0; i < count; i ++)   
        {
             accY = 0; 
             accW = *x * filtCfsB[0];
             
             for( t = 0; t < taps; t++ )
             {
                 accW += filtCfsA[t] * filtVars[t];  
                 
                 accY += filtCfsB[t+1] * filtVars[t]; 
             }
             
             accY += accW; 
             
             *y++ = accY;
             x++; 
             
             filtVarsTemp0 = filtVars[0];
             
             for( t = 0; t < taps; t++ )
             {
                 filtVarsTemp1 =  filtVars[t + 1];
                 
                 filtVars[t + 1] = filtVarsTemp0;
                 
                 filtVarsTemp0 =  filtVarsTemp1; 
             }   
             filtVars[taps] = filtVarsTemp1;
                       
             
             filtVars[0] = accW;
             
         }
     }
     return( FIL_SUCCESS );      
}

int generic_FIR_filter( PAF_FilParam * param, testTable testElement) {
    Int count;
    Int i, t, taps, ch, idx,k;
    
    PAF_AudioData * restrict ipPtr; /* input buffer */
	PAF_AudioData * restrict opPtr; /* output buffer */
	PAF_AudioData * restrict coef;  /* coefficient */
	PAF_AudioData * restrict var;   /* delay memory ptr */

    PAF_AudioData y0;
	PAF_FilParamFir *fir_param = (PAF_FilParamFir *)(&param->use);
	Int adjPower2, offset;
    
	taps  = (Int)fir_param->taps;
    idx   = (Int)fir_param->cIdx;
    k     = idx;
    FilMacro_Left_1(taps, 16, count, i);
	    
    adjPower2 = 0x8000 >> count;
	if( taps & (~adjPower2) )
	   adjPower2 <<= 1;
	        
	offset = adjPower2 - taps;
    adjPower2--;
    
    count = param->sampleCount;
    for (ch = 0; ch < param->channels; ch++) {
        ipPtr = (PAF_AudioData *)param->pIn[ch];
	    opPtr = (PAF_AudioData *)param->pOut[ch];
	    coef  = (PAF_AudioData *)param->pCoef[ch];
	    var   = (PAF_AudioData *)param->pVar[ch];
        
        for( i = 0; i < count; i++ )
        {
	        y0 = 0;
	        for( t = 0; t < taps; t++ )
	            y0 += coef[t+1]*var[(k++)&adjPower2];
	        
	        k += (offset-1);    
	        var[k&adjPower2] = *ipPtr;
	        
	        *opPtr++ = y0 + (*ipPtr++)*coef[0];
	    }	    
		    
		// Update the index.
	    idx = k & adjPower2;        

     }
     return( FIL_SUCCESS );      
   
}

int generic_IIRMX1_filter( PAF_FilParam * param, testTable testElement) {

	if( (testElement.dPrec == 1) && (testElement.cPrec == 0) )
        return( iirmx1_generic1( param ) );    
    else    
        return( iirmx1_generic( param ) );    
    
}

int iirmx1_generic( PAF_FilParam * pParam ) {
    Int count;
    Int i, t, taps, ch;
    PAF_AudioData accY,  accW;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtVars;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    PAF_AudioData filtVarsTemp0, filtVarsTemp1;
    
    count = pParam->sampleCount;
    taps  = pParam->use;
    
    for (ch = 0; ch < pParam->channels; ch++) {
        x = (PAF_AudioData *)pParam->pIn[ch];
        y = (PAF_AudioData *)pParam->pOut[ch];            

        filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;

        filtVars  = (PAF_AudioData *)pParam->pVar[ch];
        
        for (i = 0; i < count; i ++) {
             accY = 0; 
             accW = *x;
             
             for( t = 0; t < taps; t++ ) {
                 accW += filtCfsA[t]   * filtVars[t];  
                 
                 accY += filtCfsB[t+1] * filtVars[t]; 
             }
             
             accY += accW * filtCfsB[0]; 
             
             *y++ = accY;
             x++; 
             
             filtVarsTemp0 = filtVars[0];
             
             for( t = 0; t < taps; t++ ) {
                 filtVarsTemp1 =  filtVars[t + 1];
                 
                 filtVars[t + 1] = filtVarsTemp0;
                 
                 filtVarsTemp0 =  filtVarsTemp1; 
             }   
             filtVars[taps] = filtVarsTemp1;
                       
             
             filtVars[0] = accW;
             
         }
     }
     return( FIL_SUCCESS );      
}
   
int iirmx1_generic1( PAF_FilParam * pParam )
{
    Int count;
    Int i, t, taps, ch;
    double accY, accW;
    double * restrict filtVars;
    double filtVarsTemp0, filtVarsTemp1;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    count = pParam->sampleCount;
    taps  = pParam->use;
    
    for (ch = 0; ch < pParam->channels; ch++) {
        x = (PAF_AudioData *)pParam->pIn[ch];
        y = (PAF_AudioData *)pParam->pOut[ch];            

        filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;

        filtVars  = (double *)pParam->pVar[ch];
        
        for (i = 0; i < count; i ++) {
             accY = 0; 
             accW = *x * filtCfsB[0];
             
             for( t = 0; t < taps; t++ ) {
                 accW += filtCfsA[t] * filtVars[t];  
                 
                 accY += filtCfsB[t+1] * filtVars[t]; 
             }
             
             accY += accW; 
             
             *y++ = accY;
             x++; 
             
             filtVarsTemp0 = filtVars[0];
             
             for( t = 0; t < taps; t++ ) {
                 filtVarsTemp1 =  filtVars[t + 1];
                 
                 filtVars[t + 1] = filtVarsTemp0;
                 
                 filtVarsTemp0 =  filtVarsTemp1; 
             }   
             filtVars[taps] = filtVarsTemp1;
                       
             
             filtVars[0] = accW;
             
         }
     }
     return( FIL_SUCCESS );      
}

int generic_MX1DF1_filter( PAF_FilParam *param, testTable testElement)
{
    if(testElement.dPrec == 1 && testElement.cPrec == 0) return iir_mx1df1_generic1(param);
    else return iir_mx1df1_generic(param);
}

int iir_mx1df1_generic(PAF_FilParam * pParam)
{
    Int		count;
    Int		i, t, taps, ch;
    PAF_AudioData	acc, *restrict pVarsY;
    PAF_AudioData	*restrict x, *restrict y, *restrict pVarsX;
    PAF_AudioData	*restrict filtCfsB, *restrict filtCfsA;

    count = pParam->sampleCount;
    taps = pParam->use;
    
    for (ch = 0; ch < pParam->channels; ch++) {
        x = (PAF_AudioData *)pParam->pIn[ch];
        y = (PAF_AudioData *)pParam->pOut[ch];            

        filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;
		pVarsX = pParam->pVar[ch];
		pVarsY = pVarsX + taps + 1;
        
        for (i = 0; i < count; i ++) {
			acc = filtCfsB[2] * x[0];
       		for( t = 0; t < taps; t++ ) acc += filtCfsB[t] * pVarsX[t] + filtCfsA[t] * pVarsY[t];  
          
            *y++ = acc;
             
            for( t = 0; t < taps; t++ ) {
                 pVarsX[t] = pVarsX[t + 1];
                 pVarsY[t] = pVarsY[t + 1];
			}
            pVarsX[t] = *x++;
			pVarsY[t] = acc;
             
    	}
	}
	return FIL_SUCCESS;      
}

int iir_mx1df1_generic1(PAF_FilParam * pParam)
{
    Int				count, t, taps, ch, i;
    double			acc, *restrict pVarsY;
    PAF_AudioData	*restrict x, *restrict y, *restrict pVarsX;
    PAF_AudioData	*restrict filtCfsB, *restrict filtCfsA;

    count = pParam->sampleCount;
    taps = pParam->use;
    
    for (ch = 0; ch < pParam->channels; ch++) {
        x = (PAF_AudioData *)pParam->pIn[ch];
        y = (PAF_AudioData *)pParam->pOut[ch];            

        filtCfsB = (PAF_AudioData *)pParam->pCoef[ch];
        filtCfsA = filtCfsB  + taps + 1;
		pVarsX = pParam->pVar[ch];
		pVarsY = (double *)(pVarsX + taps);
        
        for (i = 0; i < count; i++) {
			acc = filtCfsB[2] * x[0];
       		for (t = 0; t < taps; t++) acc += (double)filtCfsB[t] * pVarsX[t] + filtCfsA[t] * pVarsY[t];  
          
            *y++ = (float)acc;
             
            for (t = 0; t < taps - 1; t++) {
                 pVarsX[t] = pVarsX[t + 1];
                 pVarsY[t] = pVarsY[t + 1];
			}
            pVarsX[t] = *x++;
			pVarsY[t] = acc;
             
    	}
	}
	return FIL_SUCCESS;      
}


int generic_IIR_filter( PAF_FilParam * pParam, testTable testElement) {
    Int count;
    Int i, t, taps, ch;
    PAF_AudioData accY,  accW, accYtemp;
    
    PAF_AudioData * restrict x;
    PAF_AudioData * restrict y;
    PAF_AudioData * restrict filtVars;
    PAF_AudioData * restrict filtCfsB, * restrict filtCfsA;

    PAF_AudioData filtVarsTemp0, filtVarsTemp1;
	float varW, varW_temp;
    
    count = pParam->sampleCount;
    taps  = pParam->use;
    
	for (ch = 0; ch < pParam->channels; ch++)
    {
        x = (float *)pParam->pIn[ch]; 
        y = (float *)pParam->pOut[ch];            

        filtVars  = (float *)pParam->pVar[ch];
        
        filtCfsB  = (float *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;
    
        accW = 0; 
        accY = 0;
    
        varW_temp = filtVars[0]; 
        accW = *x;
        
        for (i = 0, t = 0; i < (count * taps); i ++)
        {
            accW += filtCfsA[t] * varW_temp;
            
            accY += filtCfsB[t+1] * varW_temp;
            accYtemp = accY; 
              
            filtVars[t] = varW;                       
            varW = varW_temp; 

            t++;
            
            varW_temp = filtVars[t];
            
            
            if ( t == taps)
            {
                varW_temp = accW; 
                
                accY = 0; 
                t = 0;    
                
                accYtemp += ( accW )*( filtCfsB[0] ); 
                accW = *(++x); 
                *y++ = accYtemp;
            }
            
        }
        
        filtVars[0] = varW_temp; 

    }
     return( FIL_SUCCESS );      
}


int generic_IIRDF2T_filter( PAF_FilParam * pParam, testTable testElement) {
    Int count;
    Int i, t, taps, ch;
    PAF_AudioData accY,  accW, accYtemp;
    
    Float * restrict x, input, output, b0;
    Float * restrict y;
    Float *restrict filtCfsB, *restrict filtCfsA; /* Coeff ptrs */
    Float *restrict filtVars; /* Filter var mem ptr */

    count = pParam->sampleCount;
    taps  = pParam->use;
    
	for (ch = 0; ch < pParam->channels; ch++)
    {
        x = (float *)pParam->pIn[ch]; 
        y = (float *)pParam->pOut[ch];            

        filtVars  = (float *)pParam->pVar[ch];
        
        filtCfsB  = (float *)pParam->pCoef[ch];
        filtCfsA  = filtCfsB  + taps + 1;
    

		b0 = filtCfsB[0];
        
        for (i = 0; i < count ; i++)
        {
            input  = *x++;
			output = input * b0 + filtVars[0];
			for (t = 0 ; t < taps ; t++) {
				filtVars[t] = input * filtCfsB[t+1] + output * filtCfsA[t]; // + filtVars[t+1];
				if (t != taps - 1)
					filtVars[t] += filtVars[t+1];
			}
			*y++ = output ;
        }
        
    }
     return( FIL_SUCCESS );      
} 


int generic_CASCDF2T_filter( PAF_FilParam * param, testTable testElement)
{
    if( (testElement.dPrec == 1) && (testElement.cPrec == 0) )
        return( iirdf2t_generic_cas( param ) );    
    else    
        return( iirdf2t_generic_cas( param ) );    
  
}

int iirdf2t_generic_cas ( PAF_FilParam * pParam )
{
    Int count;
    Int i, t, taps, ch, casc, c;
    PAF_AudioData accY, accW;
    PAF_AudioData input, output;
    
    Float * restrict x;
    Float * restrict y;
    Float * restrict filtVars;
    Float * filtCfsB, * filtCfsA, b0;

    
    count = pParam->sampleCount;
    taps  = (pParam->use) & 0xFFFF;
    casc  = ( (pParam->use) & 0xFFFF0000 ) >> 16;	

    for (ch = 0; ch < pParam->channels; ch++){

    	x = (Float *)pParam->pIn[ch];
		y = (Float *)pParam->pOut[ch];
			   
		for (i = 0; i < count; i++){

			filtCfsB  = (PAF_AudioData *)pParam->pCoef[ch];
			b0 = filtCfsB[0];
			input 	  =  x[i];
		    
		    output = input * b0 ;
		    for (c = 0; c < casc; c++){
		    	
		   		filtCfsB  = (Float *)pParam->pCoef[ch] + c*taps*2 + 1;
		    	filtCfsA  = filtCfsB  + taps;
		    	filtVars  = (Float *)pParam->pVar[ch] + c*taps;
//		    	output = input * b0 + filtVars[0];
				output += filtVars[0];
		             
		      	for ( t = 0; t < taps; t++ ){
					
		      		filtVars[t] = input * filtCfsB[t] + output * filtCfsA[t]; // + filtVars[t+1];
					if (t != (taps - 1))
						filtVars[t] += filtVars[t+1];
		        }
				input = output ;

   			}
		         
			y[i] = input;
		}
		     
	}
	return( FIL_SUCCESS );      
}


