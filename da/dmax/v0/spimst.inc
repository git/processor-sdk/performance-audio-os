/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef SPIMST_
#define SPIMST_

/* SPIMST Control field constants */
#define SPIMST_TCINT_DIS 0       // SPIMST TCINT disable 
#define SPIMST_TCINT_ENA 1       // SPIMST TCINT enable 
#define SPIMST_TCINT_MSK 1       // SPIMST TCINT mask 
#define SPIMST_TCINT_SHFT 0      // SPIMST TCINT shift

#define SPIMST_LINK_DIS 0        // SPIMST LINK disable 
#define SPIMST_LINK_ENA 1        // SPIMST LINK enable 
#define SPIMST_LINK_MSK 1        // SPIMST LINK mask 
#define SPIMST_LINK_SHFT 1       // SPIMST LINK shift

#define SPIMST_XFIL_DIS 0        // SPIMST transmit fill processing disable 
#define SPIMST_XFIL_ENA 1        // SPIMST transmit fill processing enable 
#define SPIMST_XFIL_MSK 1        // SPIMST transmit fill mask 
#define SPIMST_XFIL_SHFT 2       // SPIMST transmit fill shift

#define SPIMST_RFIL_DIS 0        // SPIMST receive fill processing disable 
#define SPIMST_RFIL_ENA 1        // SPIMST receive fill processing enable 
#define SPIMST_RFIL_MSK 1        // SPIMST receive fill mask 
#define SPIMST_RFIL_SHFT 3       // SPIMST receive fill shift

#define SPIMST_SRDY_DIS 0        // SPIMST soft ready disable 
#define SPIMST_SRDY_ENA 1        // SPIMST soft ready enable 
#define SPIMST_SRDY_MSK 1        // SPIMST soft ready mask 
#define SPIMST_SRDY_SHFT 4       // SPIMST soft ready shift

// SPIMST parameter field lengths and offsets
#define SPIMST_CONTROL_OFF              0    // SPIMST control offset
#define SPIMST_CONTROL_LEN              2    // SPIMST control length
#define SPIMST_TCC_OFF                  2    // SPIMST tcc offset
#define SPIMST_TCC_LEN                  1    // SPIMST tcc length
#define SPIMST_TCINT_OFF                3    // SPIMST tcint offset
#define SPIMST_TCINT_LEN                1    // SPIMST tcint length
#define SPIMST_DRR_OFF                  4    // SPIMST drr offset
#define SPIMST_DRR_LEN                  4    // SPIMST drr length
#define SPIMST_DXR_OFF                  8    // SPIMST dxr offset
#define SPIMST_DXR_LEN                  4    // SPIMST dxr length
#define SPIMST_RBUF_OFF                 12   // SPIMST rbuf offset
#define SPIMST_RBUF_LEN                 4    // SPIMST rbuf length
#define SPIMST_XBUF_OFF                 16   // SPIMST xbuf offset
#define SPIMST_XBUF_LEN                 4    // SPIMST xbuf length
#define SPIMST_CNT_OFF                  20   // SPIMST cnt offset
#define SPIMST_CNT_LEN                  2    // SPIMST cnt length
#define SPIMST_PEND_OFF                 22   // SPIMST pend offset
#define SPIMST_PEND_LEN                 1    // SPIMST pend length
#define SPIMST_RES_OFF                  23   // SPIMST res offset
#define SPIMST_RES_LEN                  1    // SPIMST res length
#define SPIMST_XPAT_OFF                 24   // SPIMST xpat offset
#define SPIMST_XPAT_LEN                 2    // SPIMST xpat length
#define SPIMST_RPAT_OFF                 26   // SPIMST rpat offset
#define SPIMST_RPAT_LEN                 2    // SPIMST rpat length
#define SPIMST_LINK_OFF                 28   // SPIMST link offset
#define SPIMST_LINK_LEN                 4    // SPIMST link length

#endif // SPIMST_
