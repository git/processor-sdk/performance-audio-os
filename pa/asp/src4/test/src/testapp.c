/*
 *  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
 *  ALL RIGHTS RESERVED 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *  @file  testapp.c
 *
 *  @brief SRC test application main code.
 *
 *          This is a SRC test application.
 *          The application test the various implementation of SRC.
 *
 *          It expects the input from a wave file and generate the output
 *          wave file after sampling rate conversion.
 *
 *          The SRC parameters are recieved from external tables. The test application
 *          will configure the neccessary parameters and allocate memory
 *          for SRC input/output buffer and SRC memory states.
 ******************************************************************************/

/************************************************************
                     Include files
*************************************************************/
#include <stdio.h>
#include <math.h>

#include "testapp.h"
#include "util_file_io.h"
#include "util_paf_file_io.h"
#include "util_cache.h"

// To profile the SRC fucntions
#define PROFILER

#ifdef PROFILER
#include "util_profiling.h"
#endif

/************************************************************
                      Macro and Extern Definitions
*************************************************************/
#define AUDIO_SAMPLES 256

extern pTestTable testapp_srcTable[];
extern char (*testOutFileName[])[];
extern char (*testInFileName[])[];
extern const ISRC_Fxns SRC_TIH_ISRC;

/************************************************************
                  global variables
*************************************************************/
PAF_AudioFrame audioFrame;
const PAFHJT_t *pafhjt;

/************************************************************
                Function Definitions
*************************************************************/
int testApp_sampleRate_mapping(int samplingRate)
{
    if(samplingRate == 48000)
        return PAF_SAMPLERATE_48000HZ;
    else if(samplingRate == 44100)
        return PAF_SAMPLERATE_44100HZ;
    else if(samplingRate == 32000)
        return PAF_SAMPLERATE_32000HZ;
    else if(samplingRate == 88200)
        return PAF_SAMPLERATE_88200HZ;
    else if(samplingRate == 96000)
        return PAF_SAMPLERATE_96000HZ;
    else if(samplingRate == 192000)
            return PAF_SAMPLERATE_192000HZ;
    else if(samplingRate == 64000)
                return PAF_SAMPLERATE_64000HZ;
    else if(samplingRate == 128000)
                return PAF_SAMPLERATE_128000HZ;
    else if(samplingRate == 176400)
                return PAF_SAMPLERATE_176400HZ;
    else if(samplingRate == 8000)
                return PAF_SAMPLERATE_8000HZ;
    else if(samplingRate == 11025)
                return PAF_SAMPLERATE_11025HZ;
    else if(samplingRate == 12000)
                return PAF_SAMPLERATE_12000HZ;
    else if(samplingRate == 16000)
                return PAF_SAMPLERATE_16000HZ;
    else if(samplingRate == 22050)
                return PAF_SAMPLERATE_22050HZ;
    else if(samplingRate == 24000)
                return PAF_SAMPLERATE_24000HZ;
    else
        return 0;
}

int testApp_channel_mapping(int reqChCount)
{
    if(reqChCount == PAF_CC_SAT_MONO)
        return 1;
    else if(reqChCount == PAF_CC_SAT_STEREO)
        return 2;
    else if(reqChCount == PAF_CC_SAT_PHANTOM1)
        return 3;
    else if(reqChCount == PAF_CC_SAT_PHANTOM2)
        return 4;
    else if(reqChCount == PAF_CC_SAT_PHANTOM3)
        return 5;
    else if(reqChCount == PAF_CC_SAT_PHANTOM4)
        return 6;
    else if(reqChCount == PAF_CC_SAT_PHANTOM4)
        return 6;
    else if(reqChCount == PAF_CC_SAT_3STEREO)
        return 3;
    else if(reqChCount == PAF_CC_SAT_SURROUND1)
        return 4;
    else if(reqChCount == PAF_CC_SAT_SURROUND2)
        return 5;
    else if(reqChCount == PAF_CC_SAT_SURROUND3)
        return 6;
    else if(reqChCount == PAF_CC_SAT_SURROUND4)
        return 7;
    if(reqChCount == PAF_CC_SUB_ONE)
        return 1;
    return 0;
}

int main()
{
    int error,chSat,chSub;
    Int testCount,inBufSampleCount,outBufSampleCount,outputSampleRate,srcType;
    char *pTestName = NULL;
    const ISRC_Params *pSrcParams;
    ISRC_Handle srcHandle ;
    tTestTable *pTestTable;
    subchunk_data *subCunkData;

    // Configure the cache
    memarchcfg_cacheEnable();

    char outFileName[MAX_FILE_NAME_SIZE],inFileName[MAX_FILE_NAME_SIZE];
    PAF_AudioFunctions audioFrameFunctions = {
           NULL,
           PAF_ASP_channelMask,
           NULL,
           NULL,
           NULL
      };

    tFIOHandle *ppAF = (tFIOHandle*)calloc(1, sizeof(tFIOHandle));

#ifdef PROFILER
    tPrfl profiler;
#endif

    pafhjt = &PAFHJT_RAM;

    // test all SRC function for different SRC types
    for (srcType = TESTAPP_UPSAMPLE_2X; srcType < TESTAPP_SRC_COUNT; srcType++)
    {
        // get the SRC param table for a perticular SRC type
        pTestTable = testapp_srcTable[srcType];

        for (testCount = 0; pTestTable[testCount].srcParams; testCount++)
        {
            // Get a local copy of the SRC params
            pSrcParams = pTestTable[testCount].srcParams;

            //Creates SRC handle
            srcHandle = (ISRC_Handle) SRC_create((const ISRC_Fxns *)&SRC_TIH_ISRC,
                                    (const ISRC_Params *)pSrcParams);

            chSat = testApp_channel_mapping(pTestTable[testCount].chSat);
            chSub = testApp_channel_mapping(pTestTable[testCount].chSub);
            inBufSampleCount = pTestTable[testCount].inBufSampleCount;
            outBufSampleCount = pTestTable[testCount].outBufSampleCount;
            outputSampleRate = pTestTable[testCount].outputSampleRate;

            ppAF->inBufSampleCount = inBufSampleCount;
            ppAF->outBufSampleCount = outBufSampleCount;
            ppAF->outputSampleRate = outputSampleRate;

            // get the input file name
            pTestName = &((*testInFileName[srcType])
                            [testCount*MAX_FILE_NAME_SIZE]);
            sprintf(inFileName, "%s.wav", pTestName);

            printf("Test %s: ", pTestName);

            // get the output file name
            pTestName = &((*testOutFileName[srcType])
                            [testCount*MAX_FILE_NAME_SIZE]);
            sprintf(outFileName, "%s.wav", pTestName);

            printf("Test %s: ", pTestName);

            //Initialize PAF Audio frame structure
            audioFrame.fxns = &audioFrameFunctions;
            audioFrame.mode = 1;
            audioFrame.data.nChannels = MAX_CHANNEL;
            audioFrame.data.nSamples = MAX_NSAMPLES;
            audioFrame.channelConfigurationRequest.part.sat
                                                = pTestTable[testCount].chSat;
            audioFrame.channelConfigurationRequest.part.sub
                                                = pTestTable[testCount].chSub;
            audioFrame.channelConfigurationStream.part.sat
                                                = pTestTable[testCount].chSat;
            audioFrame.channelConfigurationStream.part.sub
                                                = pTestTable[testCount].chSub;

            /*Initialize and configure the audio file
            module as per test specification*/
            error = PAF_FIO_Init(ppAF, inFileName, outFileName);
            error |= PAF_FIO_Config(ppAF,&audioFrame, (chSat + chSub));

            if (error != RETRUN_SUCCESS)
            return 0;

            subCunkData = (subchunk_data *)ppAF->subchunk_data;

            //continue initializing PAF Audio frame structure
            audioFrame.sampleRate = testApp_sampleRate_mapping
                                    (subCunkData->sample_rate);
            audioFrame.sampleCount = ppAF->inBufSampleCount;
            audioFrame.pChannelConfigurationMaskTable = &PAF_ASP_stdCCMT;

            //Call reset before apply call
            SRC_reset(srcHandle, &audioFrame);

#ifdef PROFILER
            // Init and clear profiling variables
            Prfl_Init(&profiler);
#endif
            // process the input data in chuncks
            while (PAF_FIO_Read(ppAF,&audioFrame) == RETRUN_SUCCESS)
           {
                //Update original values for next frame
                audioFrame.sampleRate = testApp_sampleRate_mapping(
                                        subCunkData->sample_rate);
                audioFrame.sampleCount = ppAF->inBufSampleCount;

#ifdef PROFILER
                Prfl_Start(&profiler);
#endif

                //Actual Processing call for Sample rate conversion
                SRC_apply(srcHandle, &audioFrame);

#ifdef PROFILER
                Prfl_Stop(&profiler);
#endif

                PAF_FIO_Write(ppAF,&audioFrame);
            }

#ifdef PROFILER
            printf("Total Process Cycles %llu, Peak Process Cycles %llu Process Blocks size %d Process Count %u Input sample rate %d \n****************\n",
                    profiler.total_process_cycles, profiler.peak_process_cycles, AUDIO_SAMPLES, profiler.process_count, subCunkData->sample_rate);
            {   // write porfiling info in file
                FILE *pStdout_profile = NULL;
                pStdout_profile = fopen("..//..//test_vectors//output//src4_profile.txt", "a");
                fprintf(pStdout_profile, "SRC Test Case %s: Total Process Cycles %llu, Peak Process Cycles %llu Process Blocks size %d Process Count %u Input sample rate %d",
                        pTestName, profiler.total_process_cycles, profiler.peak_process_cycles, AUDIO_SAMPLES, profiler.process_count, subCunkData->sample_rate);
                fprintf(pStdout_profile, "\n****************\n");
                fclose(pStdout_profile);
            }

#endif
            PAF_FIO_DeInit(ppAF,&audioFrame);
        }
    }
    return 0;
}
/***************************End of file***************************************/
