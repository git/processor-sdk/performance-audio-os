#ifndef __STATUSOP_COMMON_H__
#define __STATUSOP_COMMON_H__

// Common, status structure operations

#include <xdc/std.h>
#include <stdbeta.h>
#include <oembeta.h>
#include <cusbeta.h>
#include <altbeta.h>

/* Init return */
#define STATUSOP_INIT_FAIL     0
#define STATUSOP_INIT_SUCCESS  1



/* GateMP Index Macros */
#define GATEMP_INDEX_DEC  0
#define GATEMP_INDEX_DDP  1
#define GATEMP_INDEX_THD  2
#define GATEMP_INDEX_PCM  3
#define GATEMP_INDEX_DTS  4
#define GATEMP_INDEX_DSD 5
#define GATEMP_INDEX_AAC 6
#define GATEMP_INDEX_MAX 7


/**
 *  @brief     Common Initialization parameter for shared status
 *             structures. Will Initialize gatesMP on ARM
 *             and Open on DSP.
 *
 *  @param[in] gateIdx   gateMP Index for shared status.
 *
 *  @return    STATUSOP_INIT_FAIL or STATUSOP_INIT_SUCCESS
 */
Int statusOp_Init(Int gateMP_index);

/**
 *  @brief     Common write implementation for parameters in status structure.
 *             GateMP protection mechanism will be used for only shared
 *             status structures.
 *
 *  @param[out] dstAddr      Destination buffer to write
 *
 *  @param[in] sourceAddr    Source buffer
 *
 *  @param[in] copySize      Number of bytes to write
 *
 *  @param[in] beta          Beta number of component.
 *
 */
Void statusOp_write_beta(void* dstAddr, void* sourceAddr, Int copySize, Int beta);

/**
 *  @brief     Common read implementation for parameters in status structure.
 *             GateMP protection mechanism will be used for only shared
 *             status structures.
 *
 *  @param[out] dstAddr      Destination buffer, containing the read data
 *
 *  @param[in] sourceAddr    Source buffer
 *
 *  @param[in] copySize      Number of bytes to read
 *
 *  @param[in] beta          Beta number of component.
 *
 *
 */
Void statusOp_read_beta(void* dstAddr, void* sourceAddr, Int copySize, Int beta);


/**
 *  @brief     Common write implementation for parameters in status structure.
 *             GateMP protection mechanism will be used for the shared
 *             status structures that is associated to gateIdx.
 *
 *  @param[out] dstAddr      Destination buffer to write
 *
 *  @param[in] sourceAddr    Source buffer
 *
 *  @param[in] copySize      Number of bytes to write
 *
 *  @param[in] gateIdx       GateMP index
 */
Void statusOp_write(void* dstAddr, void* sourceAddr, Int copySize, Int gateIdx);

/**
 *  @brief     Common read implementation for parameters in status structure.
 *             GateMP protection mechanism will be used for the shared
 *             status structures that is associated to gateIdx.
 *
 *  @param[out] dstAddr      Destination buffer, containing the read data
 *
 *  @param[in] sourceAddr    Source buffer
 *
 *  @param[in] copySize      Number of bytes to read
 *
 *  @param[in] beta          Beta number of component.
 *
 *  @param[in] gateIdx       GateMP index
 */
Void statusOp_read(void* dstAddr, void* sourceAddr, Int copySize, Int gateIdx);

#endif
