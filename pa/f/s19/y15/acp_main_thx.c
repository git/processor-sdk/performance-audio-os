
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Initial version obtained from ver 1.5 of s3/acp_main_thx.c
//
//
/*
 *  Update IACP default instance creation parameters
 */

#include <std.h>
#include <ialg.h>
#include <string.h>

#include <acptype.h>
#include <iacp.h>
#include <paftyp.h>
#include <thxsys_a.h>

Void
ACP_main_thx ()
{
    //
    // Patch Series 17 Framework Standard Alpha Code Shortcuts
    //
    //   Can have additions as appropriate.
    //

    {
        extern const ACP_Unit lm_thxcinema_s[];
        extern const ACP_Unit lm_thxmusic_s[];
        extern const ACP_Unit lm_thxex_s[];
        extern const ACP_Unit lm_thxgames_s[];

        extern struct {
            Int size;
            const ACP_Unit *pSequence[64];
        } IACP_STD_SIGMA_TABLE;

        const ACP_Unit **shortcut = IACP_STD_SIGMA_TABLE.pSequence;

        shortcut[execTHXListeningModeTHXCinema & 0xff]    = (Ptr )lm_thxcinema_s;
        shortcut[execTHXListeningModeTHXMusic & 0xff]     = (Ptr )lm_thxmusic_s;
        shortcut[execTHXListeningModeTHXEX & 0xff]        = (Ptr )lm_thxex_s;
        shortcut[execTHXListeningModeTHXGames & 0xff]     = (Ptr )lm_thxgames_s;
    }
}

