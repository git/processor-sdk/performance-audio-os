/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
//

#ifndef KPT_
#define KPT_

// The KPT service routine kills the pending transfer indicated 
// in the parameter table. Parameter Table: 
//
// 31          24          16          8     1          0 
//  ____________________________________________________
// | TCC       | TCINT     | PEND_PRI  | RES | TCINTFLG |
// |___________|___________|___________|_____|__________|
//
#define KPT_TCINT_DIS   0        // TCINT Disable 
#define KPT_TCINT_ENA   1        // TCINT Enable
#define KPT_TCINT_MSK   1        // TCINT Mask 
#define KPT_TCINT_SHFT  0        // TCINT Shift 

// KPT parameter field lengths and offsets
#define KPT_CTRL_OFF    0        // KPT ctrl offset 
#define KPT_CTRL_LEN    1        // KPT ctrl length 
#define KPT_PEND_OFF    1        // KPT pend offset 
#define KPT_PEND_LEN    1        // KPT pend length 
#define KPT_TCINT_OFF   2        // KPT tcint offset 
#define KPT_TCINT_LEN   1        // KPT tcint length 
#define KPT_TCC_OFF     3        // KPT tcc offset 
#define KPT_TCC_LEN     1        // KPT tcc length 

#endif // KPT_
