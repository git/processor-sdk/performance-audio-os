
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// ASP error numbers for Series 2 Number 1 -- Audio Stream 1
//
//
//

#ifndef ASPERR_
#define ASPERR_

#include <acpbeta.h>

#define ASPERR                  (int )0x80000000

#define ASPERR_UNSPECIFIED      (ASPERR+0x00)
#define ASPERR_UNKNOWNSTATE     (ASPERR+0x01)
#define ASPERR_ISSUE            (ASPERR+0x02)
#define ASPERR_RECLAIM          (ASPERR+0x03)
#define ASPERR_ABORT            (ASPERR+0x04)
#define ASPERR_QUIT             (ASPERR+0x05)
#define ASPERR_SLEEP            (ASPERR+0x06)
#define ASPERR_ALGORITHM        (ASPERR+0x07)
#define ASPERR_DECODE           (ASPERR+0x08)

#define ASPERR_SYNC             (ASPERR+0x100) /* 0x100-0x1ff */

#define ASPERR_INFO             (ASPERR+0x200) /* 0x200-0x2ff */
#define ASPERR_INFO_PROGRAM     (ASPERR+0x201)
#define ASPERR_INFO_RATERATIO   (ASPERR+0x202)
#define ASPERR_INFO_RATECHANGE  (ASPERR+0x203)

#define ASPERR_FILL             (ASPERR+0x300) /* 0x300-0x3ff */

#define ASPERR_MUTE             (ASPERR+0x400) /* 0x400-0x4ff */

#define ASPERR_IDLE             (ASPERR+0x500) /* 0x500-0x5ff */

#define ASPERR_RATE_RESET       (ASPERR+0x600) /* 0x600-0x67f */

#define ASPERR_RATE_CHECK       (ASPERR+0x680) /* 0x680-0x6ff */

#define ASPERR_DEVINP           (ASPERR+0x700) /* 0x700-0x7ff */
#define ASPERR_DEVINP_CREATE    (ASPERR+0x701)
#define ASPERR_DEVINP_OPEN      (ASPERR+0x702)
#define ASPERR_DEVINP_CLOSE     (ASPERR+0x703)
#define ASPERR_DEVINP_IDLE      (ASPERR+0x704)

#define ASPERR_DEVOUT           (ASPERR+0x800) /* 0x800-0x8ff */
#define ASPERR_DEVOUT_CREATE    (ASPERR+0x801)
#define ASPERR_DEVOUT_OPEN      (ASPERR+0x802)
#define ASPERR_DEVOUT_CLOSE     (ASPERR+0x803)
#define ASPERR_DEVOUT_IDLE      (ASPERR+0x804)

#define ASPERR_PASS             (ASPERR+0x900) /* 0x900-0x9ff */
                                               /* see as1.c */

#define ASPERR_AUTO             (ASPERR+0xa00) /* 0xa00-0xaff */
#define ASPERR_AUTO_PROGRAM     (ASPERR+0xa01)
#define ASPERR_AUTO_LENGTH      (ASPERR+0xa02)

#endif  /* ASPERR_ */
