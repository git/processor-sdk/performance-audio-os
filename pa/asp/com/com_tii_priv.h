
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common control operation declarations (TII)
//
//
//

/*
 *  This header defines the types, constants, and functions shared by the
 *  IALG common control implementations.
 */
#ifndef COM_TII_PRIV_
#define COM_TII_PRIV_

#include <xdc/std.h>
#include <ti/xdais/ialg.h>
#include <icom.h>

Void COM_TII_null ();
Int COM_TII_free (IALG_Handle handle, IALG_MemRec memTab[]);
Int COM_TII_control (IALG_Handle handle, IALG_Cmd cmd, IALG_Status * pStatus);
Void COM_TII_deactivate (IALG_Handle handle);
Int COM_TII_activateInternal (IALG_Handle handle);

Int COM_TII_snatchCommon_ (IALG_Handle handle, PAF_IALG_Common * common);
Int COM_TII_activateCommon_ (IALG_Handle handle, PAF_IALG_Common * common);
Void COM_TII_deactivateCommon_ (IALG_Handle handle, PAF_IALG_Common * common);

#define COM_TII_init	COM_TII_null
#define COM_TII_exit	COM_TII_null
#define COM_TII_moved	NULL

#define COM_TII_activate ((Void (*)(IALG_Handle handle) )COM_TII_activateInternal)

#if (((PAF_DEVICE&0xFF000000) == 0xD8000000) || \
     ((PAF_DEVICE&0xFF000000) == 0xDD000000) || \
     ((PAF_DEVICE&0xFF000000) == 0xDA000000))
#else /* PAF_DEVICE = 0xD8000000 */

#define COM_TII_snatchCommon		COM_TII_snatchCommon_
#define COM_TII_activateCommon		COM_TII_activateCommon_
#define COM_TII_deactivateCommon	COM_TII_deactivateCommon_

#endif /* PAF_DEVICE = 0xD8000000 */

#endif  /* COM_TII_PRIV */
