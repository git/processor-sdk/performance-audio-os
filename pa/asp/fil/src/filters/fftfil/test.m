% /*
% * Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
% *   All rights reserved.
% * 
% *   Redistribution and use in source and binary forms, with or without
% *   modification, are permitted provided that the following conditions are met:
% *       * Redistributions of source code must retain the above copyright
% *         notice, this list of conditions and the following disclaimer.
% *       * Redistributions in binary form must reproduce the above copyright
% *         notice, this list of conditions and the following disclaimer in the
% *         documentation and/or other materials provided with the distribution.
% *       * Neither the name of Texas Instruments Incorporated nor the
% *         names of its contributors may be used to endorse or promote products
% *         derived from this software without specific prior written permission.
% * 
% *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% *   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% *   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% *   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% *   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% *   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% *   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% *   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% *   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% *   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
% *   THE POSSIBILITY OF SUCH DAMAGE.
% */

% .............................................................................
function test( verbose, bref, bplot, bcsv, bquick)
%TEST performs regression testing of "fftfil.c"
%   test( verbose, bref, bplot, bcsv, bquick)
% verbose -- non-zero => displays status info.
%   bref -- non-zero => self-checks model "fftfil.m" versus basic ref. "sigfil.m"
%  bplot -- non-zero => plots error signal
%   bcsv -- non-zero => creates "fftfil.csv" (comparison info.)
% bquick -- non-zero => just a "quick" test, not a full regression

% .............................................................................

% /*
% * Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
% * All rights reserved.	
% *
% *  Redistribution and use in source and binary forms, with or without
% *  modification, are permitted provided that the following conditions
% *  are met:
% *
% *    Redistributions of source code must retain the above copyright
% *    notice, this list of conditions and the following disclaimer.
% *
% *    Redistributions in binary form must reproduce the above copyright
% *    notice, this list of conditions and the following disclaimer in the
% *    documentation and/or other materials provided with the
% *    distribution.
% *
% *    Neither the name of Texas Instruments Incorporated nor the names of
% *    its contributors may be used to endorse or promote products derived
% *    from this software without specific prior written permission.
% *
% *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
% *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% *
% */

% .............................................................................

% Default arguments

  if( !( nargin >= 0)) % how to print? -- make "verbose" required?
    usage( 'test( [ verbose, [, bref [, bplot [, bcsv [, bquick]]]]])');
  else
    if( !( nargin >= 5))
      bquick = 0;
      if( !( nargin >= 4))
        bcsv = 0;
        if( !( nargin >= 3))
          bplot = 0;
          if( !( nargin >= 2))
            bref = 0;
            if( !( nargin >= 1))
              verbose = 0;
            end % if
          end % if
        end % if
      end % if
    end % if
  end % if

  %verbose = 4+8+16; % H, X, Y output

  st = 'fftfil'; % base name of ".out", ".csv" files

  if( bquick)
    N_tests = [512];
    fil_tests = {'lp512.fil'};
  else
    N_tests = [512 1024 2048];
    fil_tests = {'lp512.fil'; 'rand4096.fil'; 'EqFiltCar.fil'};
  end % if

% .............................................................................

% Create ".csv" output file

  if( bcsv)

    csvFile = [st '.csv'];

    % if( strcmp( version(), '2.1.72'))
    [fcsv, msg] = fopen( csvFile, 'wt');
    if (fcsv < 0)
      error( ['test: ' msg]);
    end

  end % if

% .............................................................................

% Simulation settings

% Retrieve "supersim" program/path name from environment variable "SUPERSIM",
% if available, otherwise use default 'supersim'

  if( ~bref) % i.e., sim?
    supersim = getenv( 'SUPERSIM');
    if( ~length( supersim))
      supersim = 'supersim';
    end % if

    simopt = '-mem8x32';
    % simopt = '';

    inFile = 'in.txt';
    outFile = 'out.txt';

  end % if

% .............................................................................

% Test settings / parameters

  if( bcsv)
    fprintf( fcsv, ',%d', N_tests);
    fprintf( fcsv, '\n');
  end % if

  for filIndex = 1:length( fil_tests)
  filFile = fil_tests{ filIndex};

  if( bcsv)
    fprintf( fcsv, '\"%s\"', filFile);
  end % if

  for N = N_tests

  fprintf( stderr, '--------------------------------> ');
  fprintf( stderr, 'N = %d, filFile = %s\n', N, filFile);

% .............................................................................

% Generate test input

    % generate 0.1 sec of 1000-Hz tone @ -10 dB
    [t, x] = mksin( 48000, 0.1, 1000, -10); % (row)
    % [t, x] = mksin( 48000, 0.02, 1000, -10);
    % plot( x); pause;

% .............................................................................

% Run reference model(s)

    fil = filread( filFile); % row

    ys = fftfil( x, fil, N, verbose); % col

    if( bref)
      y= sigfil( x, fil);
    else

% .............................................................................

% Simulate on target DSP

      txtFile = strrep( filFile, '.fil', '.txt');

      % if( strcmp( version(), '2.1.72'))
      [fid, msg] = fopen( txtFile, 'wt');
      if (fid < 0)
        error( ['test: ' msg]);
      end
      fprintf( fid, '%.13g\n', fil.num(1).c);
      fclose( fid);

      % if( strcmp( version(), '2.1.72'))
      [fid, msg] = fopen( inFile, 'wt');
      if (fid < 0)
        error( ['test: ' msg]);
      end
      fprintf( fid, '%.13g\n', x);
      fclose( fid);

      simFile = [st, '.out'];

      simcmd = [supersim ...
        ' -q' ...
        ' ' simopt, ...
        ' ' simFile, ...
        ' -f"' txtFile, '"', ...
        ' -n"' int2str( N), '"', ...
        ' < ' inFile, ...
        ' > ' outFile] ;

      if( exist( 'OCTAVE_VERSION'))
        [output, status] = system( simcmd);
      else
        [status, output] = system( simcmd);
      end % if

      % "output" only captures "stdout", not "stderr"
      % *shouldn't* ever be non-null, in conjunction w/ "> ..." as used above
      if( length( output))
        error( [st ': supersim message:\n' output ]);
      end % if

      % non-zero status occurs from "system()" or "supersim", but not target ".out"
      if( status)
        exit( status);
      end % if

      eval( ['load ' outFile]);
      y = out;

    end % if

% .............................................................................

% Examine results

    yd = y - ys;
    ydb = 20*log10( std( yd));

    fprintf( stderr, 'variance = %g dB\n', ydb);

    if( bcsv)
      fprintf( fcsv, ',%g', ydb);
    end % if

% .............................................................................

% Plot results

    if( bplot)
      syscmd = 'enumwin | grep "Cygwin/X"';

      if( exist( 'OCTAVE_VERSION'))
        [output, status] = system( syscmd);
      else
        [status, output] = system( syscmd);
      end % if

      if( status)
        error( 'X Windows needs to be running in order to use plotting');
      end % if

      %%plot( [1:length(ys)], [y, ys, yd]);
      plot( yd);
    end % if


% .............................................................................

  end % for

  if( bcsv)
    fprintf( fcsv, '\n');
  end % if

  end % for

  if( bcsv)
    fclose( fcsv);
  end % if

% .............................................................................

end % function
