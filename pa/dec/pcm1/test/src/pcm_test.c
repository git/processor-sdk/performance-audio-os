/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/**
 *  @file  pcm_test.c
 *
 *  @brief PCM1 test application main code.
 *
 *          This is a PCM decoder test application.
 *          The application test the various implementations like Mapping, Input decoding and Downmix.
 *
 *          Configurations are loaded from the test_cases.c file for various test cases.
 *          It expects the input from a wave file and generate the output.
 *          Configurations of the test cases, input and output filenames are given from the test vector tables.
 *
 *
 *****************************************************************************/
/************************************************************
                     Include files
*************************************************************/
#include <stdio.h>
#include <math.h>
#include <xdc/std.h>
#include <alg.h>
#include <pcm.h>
#include <stdlib.h>
#include <string.h>
#include <pafhjt.h>

#include "pcm_test.h"
#include "pcm_mds_priv.h"
#include "util_c66x_cache.h"

/************************************************************
                      Macro Definitions
*************************************************************/
#define INT_MAX_POSITIVE_ABS   2147483647.0f
#define INT_MAX_NEGATIVE_ABS   2147483648.0f
#define AMPLITUDE_FACTOR       1.0f
#define MAX_CHANNELS 16
#ifdef ARMCOMPILE
#define FRAME_SIZE 1536
#else
#define FRAME_SIZE 256
#endif

/*To profile the PCM fucntionality*/
#define PROFILER
#ifdef PROFILER
#include "util_profiling.h"
#endif

/* Macros to map from or to wav file from Audio frame */
#define MAP_LEFT 0
#define MAP_RGHT 1
#define MAP_CNTR 2
#define MAP_LSUR 8
#define MAP_RSUR 9
#define MAP_LBAK 10
#define MAP_RBAK 11
#define MAP_SUBW 12
/************************************************************
                  global variables
*************************************************************/
const PAFHJT_t *pafhjt;

extern const IPCM_Fxns PCM_MDS_IPCM;
extern const PAF_AudioFunctions audioFrameFunctions;
const PAF_AudioFunctions audioFrameFunctions = {
    PAF_ASP_dB2ToLinear,
    PAF_ASP_channelMask,
    PAF_ASP_programFormat,
    PAF_ASP_sampleRateHz,
    PAF_ASP_delay
};
extern const test_case pcm_test_cases[28];

#ifdef ARMCOMPILE
void idle_func()
{
    printf("In idle_func\n");
}
#endif
/* Order of the channels to be written into the wav file*/
int out_map_with_subw[13][8] = {
		{
				-1, -1, -1, -1, -1, -1, -1, -1 //PAF_CC_SAT_UNKNOWN
		},{
				-1, -1, -1, -1, -1, -1, -1, -1 //PAF_CC_SAT_NONE
		},{
				MAP_CNTR, -1, -1, -1, -1, -1, -1, -1 // PAF_CC_SAT_MONO
		},{
				MAP_LEFT, MAP_RGHT, -1, -1, -1, -1, -1, -1 // PAF_CC_SAT_STEREO
		},{
				MAP_LEFT, MAP_RGHT, MAP_LSUR, -1, -1, -1, -1, -1 // PAF_CC_SAT_PHANTOM1
		},{
				MAP_LEFT, MAP_RGHT, MAP_LSUR, MAP_RSUR, -1, -1, -1, -1 // PAF_CC_SAT_PHANTOM2
		},{
				MAP_LEFT, MAP_RGHT, MAP_LSUR, MAP_RSUR, MAP_LBAK, -1, -1, -1 // PAF_CC_SAT_PHANTOM3
		},{
				MAP_LEFT, MAP_RGHT, MAP_LSUR, MAP_RSUR, MAP_LBAK, MAP_RBAK, -1, -1 // PAF_CC_SAT_PHANTOM4
		},{
				MAP_LEFT, MAP_RGHT, MAP_CNTR, MAP_SUBW, -1, -1, -1, -1 // PAF_CMAP_LEFT_SAT_3STEREO
		},{
				MAP_LEFT, MAP_RGHT, MAP_CNTR, MAP_SUBW, MAP_LSUR, -1, -1, -1 // PAF_CC_SAT_SURROUND1
		},{
				MAP_LEFT, MAP_RGHT, MAP_CNTR, MAP_SUBW, MAP_LSUR, MAP_RSUR, -1, -1 // PAF_CC_SAT_SURROUND2
		},{
				MAP_LEFT, MAP_RGHT, MAP_CNTR, MAP_SUBW, MAP_LSUR, MAP_RSUR, MAP_LBAK, -1 // PAF_CC_SAT_SURROUND3
		},{
				MAP_LEFT, MAP_RGHT, MAP_CNTR, MAP_SUBW, MAP_LSUR, MAP_RSUR, MAP_LBAK, MAP_RBAK // PAF_CC_SAT_SURROUND4
		}
};
int out_map[13][8] = {
		{
				-1, -1, -1, -1, -1, -1, -1, -1 //PAF_CC_SAT_UNKNOWN
		},{
				-1, -1, -1, -1, -1, -1, -1, -1 //PAF_CC_SAT_NONE
		},{
				MAP_CNTR, -1, -1, -1, -1, -1, -1, -1 // PAF_CC_SAT_MONO
		},{
				MAP_LEFT , MAP_RGHT, -1, -1, -1, -1, -1, -1 // PAF_CC_SAT_STEREO
		},{
				MAP_LEFT, MAP_RGHT, MAP_LSUR, -1, -1, -1, -1, -1 // PAF_CC_SAT_PHANTOM1
		},{
				MAP_LEFT, MAP_RGHT, MAP_LSUR, MAP_RSUR, -1, -1, -1, -1 // PAF_CC_SAT_PHANTOM2
		},{
				MAP_LEFT, MAP_RGHT, MAP_LSUR, MAP_RSUR, MAP_LBAK, -1, -1, -1 // PAF_CC_SAT_PHANTOM3
		},{
				MAP_LEFT, MAP_RGHT, MAP_LSUR, MAP_RSUR, MAP_LBAK, MAP_RBAK, -1, -1 // PAF_CC_SAT_PHANTOM4
		},{
				MAP_LEFT, MAP_RGHT, MAP_CNTR, -1, -1, -1, -1, -1 // PAF_CC_SAT_3STEREO
		},{
				MAP_LEFT, MAP_RGHT, MAP_CNTR, MAP_LSUR, -1, -1, -1, -1 // PAF_CC_SAT_SURROUND1
		},{
				MAP_LEFT, MAP_RGHT, MAP_CNTR, MAP_LSUR, MAP_RSUR, -1, -1, -1 // PAF_CC_SAT_SURROUND2
		},{
				MAP_LEFT, MAP_RGHT, MAP_CNTR, MAP_LSUR, MAP_RSUR, MAP_LBAK, -1, -1 // PAF_CC_SAT_SURROUND3
		},{
				MAP_LEFT, MAP_RGHT, MAP_CNTR, MAP_LSUR, MAP_RSUR, MAP_LBAK, MAP_RBAK, -1 // PAF_CC_SAT_SURROUND4
		}
};
/************************************************************
                Function Definitions
*************************************************************/

int main()
{
    float **audio_data;
    int sampleCount, bytesRead, j;
    float *bufferData;
    bufferData = (float*)calloc(FRAME_SIZE * MAX_CHANNELS, sizeof(float));
    int ret;

    ALG_Handle sioHandle;
    PAF_DecodeInStruct *pDecodeInStruct = (PAF_DecodeInStruct *)malloc(sizeof(PAF_DecodeInStruct));
    PAF_DecodeOutStruct *pDecodeOutStruct = (PAF_DecodeOutStruct *)malloc(sizeof(PAF_DecodeOutStruct));
    PAF_DecodeControl *pDecodeControl = (PAF_DecodeControl *)malloc(sizeof(PAF_DecodeControl));
    PAF_AudioFrame *pAudioFrame = (PAF_AudioFrame *)malloc(sizeof(PAF_AudioFrame));
    PAF_DecodeStatus *pDecodeStatus = (PAF_DecodeStatus *)malloc(sizeof(PAF_DecodeStatus));
    PAF_InpBufConfig *pInpBufConfig ;
    int frameCount = 0;int numberOfChanels;
    FILE *pInFile, *pOutFile;int outputIndex, outChIndex, channelIndex, testIndex;
    char *pCharTempBuf;

#ifdef ARMCOMPILE
   char filePath[70] = "../";
#else
   char filePath[70] = "../../";
#endif
#ifdef PROFILER
    tPrfl profiler;
#endif
#ifndef ARMCOMPILE
    memarchcfg_cacheEnable();
#endif

    PCM_Handle pcmHandle = PCM_create(&PCM_MDS_IPCM, &IPCM_PARAMS);

    PCM_MDS_Obj *pcm = (Void *)pcmHandle;

    float fltTempData;int intTempData;
    header_1p  pHeader1 = (header_1p)malloc(sizeof(header_1));
    header_2p  pHeader2 = (header_2p)malloc(sizeof(header_2));
    subchunk_data *pSubchunkData = (subchunk_data *)calloc(1,50);

    float tempFixedPoint = 0, typeConvScale = 1.0;
    char *pntrPrime;
    int one = 1;

    /* Test cases are executed one after the other*/
    for(testIndex = 0; testIndex < 28; testIndex++)
    {
        test_case testCase = pcm_test_cases[testIndex];
        numberOfChanels = testCase.numOfInChannels;

        /* Interface function calls*/
        pafhjt = &PAFHJT_RAM;
        
        printf("Executing test case[%d]: %s\n",testIndex+1, testCase.charTestCaseName);
        
        /* Input and output file pointer initializations*/
#ifdef ARMCOMPILE
        strcpy(filePath,"../");
#else
        strcpy(filePath,"../../");
#endif
        strcat(filePath, testCase.charInFileName);
        pInFile = fopen(filePath, "rb");
        if(pInFile == 0)
        {
            printf(" %s Input file read error! exiting.\n",filePath);
            return 0;
        }
#ifdef ARMCOMPILE
        strcpy(filePath,"../");
#else
        strcpy(filePath,"../../");
#endif
        strcat(filePath, testCase.charOutFileName);
        pOutFile = fopen(filePath, "wb");
        if(pOutFile == 0)
        {
            printf("Output file read error! exiting. %s \n", filePath);
            return 0;
        }
        /* Reading the input file header*/
        fread((void*)pHeader1, 1, sizeof(header_1), pInFile);
        fread((void*)pSubchunkData, 1, pHeader1->subchunk1_size, pInFile);
        fread((void*)pHeader2, 1, sizeof(header_2), pInFile);

        /* Modify parameters according to the output wav file based on the output config*/
        pSubchunkData->num_channels = testCase.numOfOutChannels;
        pHeader2->subchunk2_size = pHeader2->subchunk2_size * ((float) testCase.numOfOutChannels * testCase.numOfInChannels);

        /* Write the modified output header*/
        fwrite((void*)pHeader1, 1, sizeof(header_1), pOutFile);
        fwrite((void*)pSubchunkData, 1, pHeader1->subchunk1_size, pOutFile);
        fwrite((void*)pHeader2, 1, sizeof(header_2), pOutFile);
        frameCount = 0;

        /* Create buffer to read data into*/
        pCharTempBuf = (char*)calloc(1,numberOfChanels*sizeof(float)*FRAME_SIZE);

#ifdef PROFILER
        /* Init and clear profiling variables*/
        Prfl_Init(&profiler);
#endif

        /* While for file read, exits EOF */
        while(1)
        {
            pcm->pStatus->scaleVolume = testCase.scaleVolume;
            pDecodeInStruct->sampleCount = FRAME_SIZE;
            pDecodeInStruct->outputFlag = 0;
            pDecodeInStruct->errorFlag = 0;
            pAudioFrame->channelConfigurationStream.full = testCase.channelConfigurationStream.full;
            pInpBufConfig = (PAF_InpBufConfig *)malloc(sizeof(PAF_InpBufConfig));
            if(frameCount == 0)
            {
            	pAudioFrame->mode = 1; /*Alg Enabled*/
            	pAudioFrame->sampleRate = 4; /*48KHz*/
            	pAudioFrame->sampleCount = FRAME_SIZE * MAX_CHANNELS;
            	pAudioFrame->data.nChannels = MAX_CHANNELS;
            	pAudioFrame->data.nSamples = FRAME_SIZE * MAX_CHANNELS;
                pAudioFrame->data.samsiz = (PAF_AudioSize *)malloc(sizeof(float) * MAX_CHANNELS);
                pAudioFrame->fxns = &audioFrameFunctions;
                audio_data = (float**)malloc(sizeof(float*) * MAX_CHANNELS);
                audio_data[0] = (void*)malloc(FRAME_SIZE * sizeof(float) * MAX_CHANNELS);
                for(channelIndex = 1; channelIndex < MAX_CHANNELS; channelIndex++)
                {
                    audio_data[channelIndex] = audio_data[channelIndex - 1 ] + (FRAME_SIZE * sizeof(float));
                }
                pAudioFrame->data.sample = audio_data;
                pAudioFrame->pChannelConfigurationMaskTable = &PAF_ASP_stdCCMT;
                pcm->pStatus->channelConfigurationProgram.full = testCase.channelConfigurationStream.full;
                pDecodeControl->pAudioFrame = pAudioFrame;
                ret = pcmHandle->fxns->reset(pcmHandle, sioHandle, pDecodeControl, pDecodeStatus);
                pcm->pStatus->CntrMixLev = -9; // in dB
                pcm->pStatus->SurrMixLev = -9; // in dB
            }
            pInpBufConfig->base.pFloat = bufferData;
            pInpBufConfig->pntr.pFloat= bufferData;
            /*Buffer size = MAX_CHANNELS * byest_per_sample * samples_per_frame*/
            pInpBufConfig->sizeofBuffer = MAX_CHANNELS * 4 * FRAME_SIZE;
            pInpBufConfig->sizeofElement = 4;/* Four bytes per sample*/
            if(testCase.typeConversionEnabled)
            {
                pInpBufConfig->precision = 24;/* 24 bit Precision*/
            }
            else
            {
                pInpBufConfig->precision = 33; /* Condition > 32 is checked for float to float copying from buffer to audioframe data*/
            }
            pInpBufConfig->stride = MAX_CHANNELS;
            pInpBufConfig->deliverZeros = 0;
            pInpBufConfig->frameLength = FRAME_SIZE * MAX_CHANNELS;
            pInpBufConfig->lengthofData = FRAME_SIZE * MAX_CHANNELS;
            pDecodeStatus->channelMap = testCase.channelMap;
            pDecodeStatus->decBypass = 0;
            pDecodeStatus->channelConfigurationRequest.full = testCase.channelConfigurationRequest.full;
            pDecodeStatus->channelConfigurationDownmix.full = testCase.channelConfigurationRequest.full;

            /* Read data from the wav file into a char buffer */
            bytesRead = fread(pCharTempBuf, 1, sizeof(float) * numberOfChanels * FRAME_SIZE, pInFile);
            if(bytesRead <= 0)
            {
                break;
            }

            /*Data read from the wav file is mapped onto the two dimensional array that is pointing to the "sample" field of pAudioFrame.data
            For Example, consider the stereo input, we dump it into the first two channels, other channels are maintained as silence to avoid junk data*/
            j = 0;
            for(sampleCount = 0; sampleCount < numberOfChanels * FRAME_SIZE ; sampleCount++)
            {
                /*read a 32-bit sample from buffer*/
                memcpy(&intTempData, &pCharTempBuf[sampleCount*sizeof(float)], sizeof(float));
                /*int to float conversion*/
                fltTempData = (float)((intTempData / INT_MAX_POSITIVE_ABS) * AMPLITUDE_FACTOR);
                j = sampleCount / numberOfChanels;
                if(!testCase.typeConversionEnabled)
                {
                    bufferData[j*MAX_CHANNELS + (sampleCount % numberOfChanels)] = fltTempData;
                }
                else
                {
                    typeConvScale = 1.0;
                    pntrPrime = (char*)&tempFixedPoint;
                    if (typeConvScale)
                    {   // input is assumed to be floating point, needing to be scaled.
                        Int value, tempValue;

                		typeConvScale *= one << pInpBufConfig->precision - 1;

						intTempData = (int)((fltTempData * typeConvScale) );
						value = intTempData << (32 - pInpBufConfig->precision);
                		*pntrPrime = 0;
						tempValue = value << 16;
						*(pntrPrime+1) = tempValue >> 24;
						tempValue = value << 8;
						*(pntrPrime+2) = tempValue >> 24;
						tempValue = value << 0;
						*(pntrPrime+3) = tempValue >> 24;

                	}
			        bufferData[j*MAX_CHANNELS + (sampleCount % numberOfChanels)] = (float)tempFixedPoint;
                }
            }
            pInpBufConfig->base.pFloat = bufferData;
            pInpBufConfig->pntr.pFloat = bufferData;
            pDecodeControl->pInpBufConfig = pInpBufConfig;
            pDecodeInStruct->pAudioFrame = pAudioFrame;

            /*Info call, pointers and configurations are assigned*/
            ret = PCM_info(pcmHandle, sioHandle, pDecodeControl, pDecodeStatus);
            if(ret != 0)
            {
                return 0;
            }

#ifdef PROFILER
    Prfl_Start(&profiler);
#endif
            /*Decode call*/
            ret = PCM_decode(pcmHandle, sioHandle, pDecodeInStruct, pDecodeOutStruct);
            if(ret != 0)
            {
                return 0;
            }
#ifdef PROFILER
    Prfl_Stop(&profiler);
#endif
            /*After decode, the data on the pAudioFrame is mapped onto an output buffer and dumped into the wav file*/
            outputIndex = 0; outChIndex = 0;
            for (j = 0; j < FRAME_SIZE; j++)
            {
                for (channelIndex = 0; channelIndex < testCase.numOfOutChannels; channelIndex++)
                {
                	if(testCase.channelConfigurationRequest.part.sub == 1){
                		outChIndex = out_map_with_subw[testCase.channelConfigurationRequest.part.sat][channelIndex];
                	}else{
                		outChIndex = out_map[testCase.channelConfigurationRequest.part.sat][channelIndex];
                	}
                	fltTempData = pDecodeInStruct->pAudioFrame->data.sample[outChIndex][j];
                    if (fltTempData >= 0)
                    {
                        intTempData = (int)(fltTempData * INT_MAX_POSITIVE_ABS / AMPLITUDE_FACTOR);
                    }
                    else
                    {
                        intTempData = (int)(fltTempData * INT_MAX_NEGATIVE_ABS / AMPLITUDE_FACTOR);
                    }
                    memcpy(&pCharTempBuf[outputIndex*4], &intTempData, 4);
                    outputIndex++;
                }
            }

            /*write data into output file*/
            fwrite(pCharTempBuf, 1, bytesRead * testCase.numOfOutChannels / testCase.numOfInChannels, pOutFile);
            frameCount++;
        }
        /*Free memories and close I/O wav files*/
        free(pCharTempBuf);
        fclose(pOutFile);
        fclose(pInFile);
        free(audio_data[0]);

#ifdef PROFILER
        printf("Total Process Cycles %llu, Peak Process Cycles %llu Process Blocks size %d Process Count %u Input sample rate %d \n****************\n",
            profiler.total_process_cycles, profiler.peak_process_cycles, FRAME_SIZE, profiler.process_count, pSubchunkData->sample_rate);
        {
            /*write porfiling info in file*/
#ifdef ARMCOMPILE
        strcpy(filePath,"..//test_vectors//output//fil_profile.txt");
#else
        strcpy(filePath,"..//..//test_vectors//output//fil_profile.txt");
#endif
            FILE *pStdout_profile = NULL;
            pStdout_profile = fopen(filePath, "a");
            fprintf(pStdout_profile, "PCM Test Case %s: Total Process Cycles %llu, Peak Process Cycles %llu Process Blocks size %d Process Count %u Input sample rate %d",
            testCase.charTestCaseName, profiler.total_process_cycles, profiler.peak_process_cycles, FRAME_SIZE, profiler.process_count, pSubchunkData->sample_rate);
            fprintf(pStdout_profile, "\n****************\n");
            fclose(pStdout_profile);
        }
#endif
    }

    /*Free memories used from heap*/
    free(bufferData);
    free(pDecodeInStruct);
    free(pDecodeOutStruct);
    free(pDecodeControl);
    free(pAudioFrame);
    PCM_delete(pcmHandle);
    free(pSubchunkData);
    free(bufferData);
#ifndef ARMCOMPILE
    memarchcfg_cacheFlush();
#endif
    return (0);
}
