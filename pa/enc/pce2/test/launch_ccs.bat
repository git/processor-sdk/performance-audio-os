@ECHO OFF
rem:   launch_ccs.bat
rem:   batch file to launch CCS to build the SRC test app.

rem:  OS_SDK_ROOT is where the open source version of PA resides.
rem:  This directory contains pa, da, etc.
rem:  Use double backslashes so that the path can be used by sed.

rem:  TI_DIR is where different tools reside.
 
rem:  CG_TOOLS is where c6xx compiler reside.

rem:  Now launch code composer with these environment variables set.
rem:  c:
rem:  cd "C:\ti\ccsv5\eclipse"
rem:  "C:ti\ccsv5\eclipse\ccstudio.exe"
rem:  ccstudio


if "%COMPUTERNAME%"=="CPU-158" (
    echo "Launching Code Composer using settings for CPU-158."
    set PAF_ROOT=E:\\K2G_paf\\code\\work\\os_paf_dev_pce\\os_paf_dev
    set TI_DIR=C:\\ti
    set CG_TOOLS=${TI_DIR}\\ccsv6\\tools\\compiler\\c6000_8.1.0
    c:
    cd "C:\\ti\\ccsv6\\eclipse"
    rem "C:\PA_Tools\CCSV5_3_0\ccsv5\eclipse\ccstudio.exe"
    start ccstudio -data E:\K2G_paf\code\work\os_paf_dev_pce\workspace_v6
    goto end
)

:end
