
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// dMAX INT Service Routine api's
//
//
//

#include <std.h>
#include <hwi.h>

#include <com.h>
#include <int.h>
#include <dmax.h>
#include <dmax_struct.h>

extern far cregister volatile unsigned int IER;

/* INT Open */
Uint32 dMAX_intOpen(
    dMAX_Handle handle,
    Uint32 evt,
    Uint32 maxid,
    Uint32 tcc,
    Uint32 tcint,
    Fxn fxn)
{
    Uint32 pt;
    if(handle->fxns->allocEvt(handle,evt)==NONE_EVENT)
        return 0;

    if(handle->fxns->allocTCC(handle,tcc)==dMAX_TCCNONE){
        handle->fxns->freeEvt(handle,evt);
        return 0;
    }

    if(tcint!=dMAX_INT0&&tcint!=dMAX_INT1){
        if(handle->fxns->allocInt(handle,tcint)==dMAX_INTNONE){
            handle->fxns->freeEvt(handle,evt);
            handle->fxns->freeTCC(handle,tcc);
            return 0;
        }
    }

    if(!(pt=(Uint32)handle->fxns->allocPT(handle,maxid,sizeof(intp)))){
        handle->fxns->freeEvt(handle,evt);
        handle->fxns->freeTCC(handle,tcc);
        if(tcint!=dMAX_INT0&&tcint!=dMAX_INT1)
            handle->fxns->freeInt(handle,tcint);
        return 0;
    }

    if(fxn){
        HWI_dispatchPlug(handle->fxns->mapInt(handle,tcint),
                         (Fxn)fxn,-1,NULL);
        invalid_ist();
        IER|=1<<handle->fxns->mapInt(handle,tcint);
    }
    else
        return 0;

    handle->fxns->evtPol(handle,evt,dMAX_POL_HIGH);
    handle->fxns->evtPri(handle,maxid,evt);
    handle->fxns->clrEvtFlg(handle,evt);

    return pt;
}

/* INT Close */
void dMAX_intClose(
    dMAX_Handle handle,
    Uint32 evt,
    Uint32 maxid,
    Uint32 tcc,
    Uint32 tcint,
    Uint32 pt)
{
    handle->fxns->disEvt(handle,evt);
    handle->fxns->freeEvt(handle,evt);
    handle->fxns->freeTCC(handle,tcc);
    if(tcint!=dMAX_INT0&&tcint!=dMAX_INT1){
        IER&=~(1<<handle->fxns->mapInt(handle,tcint));
        handle->fxns->freeInt(handle,tcint);
    }
    handle->fxns->freePT(handle,maxid,(Uint32*)pt,sizeof(intp));
}

/* INT Configure Event Entry */
void dMAX_intCfgEE(
    dMAX_Handle handle,
    Uint32 evt,
    Uint32 maxid,
    Uint32 ee)
{
    handle->fxns->updateEET(handle,maxid,evt,ee);
}

/* INT Generation */
void dMAX_intGen(
    dMAX_Handle handle,
    Uint32 evt,
    Uint32 pt,
    intp *pint)
{
    handle->fxns->cfgPT(handle,(Uint32*)pt,(Uint32*)pint,sizeof(intp));
    handle->fxns->enaEvt(handle,evt);
}

/* INT No Wait */
void dMAX_intNoWait(
    dMAX_Handle handle,
    Uint32 tcc)
{
    handle->fxns->clrTCC(handle,tcc);
}

