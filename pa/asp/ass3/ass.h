
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Non-Standard Audio Stream Split Algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the Audio Stream Split Algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the ASS
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef ASS_
#define ASS_

#include <alg.h>
#include <ialg.h>
#include <paf_alg.h>

#include "iass.h"
#include "paftyp.h"

/*
 *  ======== ASS_Handle ========
 *  Audio Stream Split Algorithm instance handle
 */
typedef struct IASS_Obj *ASS_Handle;

/*
 *  ======== ASS_Params ========
 *  Audio Stream Split Algorithm instance creation parameters
 */
typedef struct IASS_Params ASS_Params;

/*
 *  ======== ASS_PARAMS ========
 *  Default instance parameters
 */
#define ASS_PARAMS IASS_PARAMS1

/*
 *  ======== ASS_Status ========
 *  Status structure for getting ASS instance attributes
 */
typedef volatile struct IASS_Status ASS_Status;

/*
 *  ======== ASS_Cmd ========
 *  This typedef defines the control commands ASS objects
 */
typedef IASS_Cmd   ASS_Cmd;

/*
 * ===== control method commands =====
 */
#define ASS_NULL IASS_NULL
#define ASS_GETSTATUSADDRESS1 IASS_GETSTATUSADDRESS1
#define ASS_GETSTATUSADDRESS2 IASS_GETSTATUSADDRESS2
#define ASS_GETSTATUS IASS_GETSTATUS
#define ASS_SETSTATUS IASS_SETSTATUS

/*
 *  ======== ASS_create ========
 *  Create an instance of a ASS object.
 */
static inline ASS_Handle ASS_create(const IASS_Fxns *fxns, const ASS_Params *prms)
{
    return ((ASS_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== ASS_delete ========
 *  Delete a ASS instance object
 */
static inline Void ASS_delete(ASS_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== ASS_apply ========
 */
extern Int ASS_apply(ASS_Handle, PAF_AudioFrame *);

/*
 *  ======== ASS_reset ========
 */
extern Int ASS_reset(ASS_Handle, PAF_AudioFrame *);

/*
 *  ======== ASS_exit ========
 *  Module finalization
 */
extern Void ASS_exit(Void);

/*
 *  ======== ASS_init ========
 *  Module initialization
 */
extern Void ASS_init(Void);

#endif  /* ASS_ */
