/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
#ifndef _TIMER64LOC_H
#define _TIMER64LOC_H
/**************************************************************************************
 * FILE PURPOSE: Definitions for the timer64 driver
 **************************************************************************************
 * FILE NAME: timer64loc.h
 *
 * DESCRIPTION: Defines the timer64 memory map and bitfield definitions
 *
 **************************************************************************************/

/* Register offsets */
#define TIMER64_REG_GPINT_EN    0x08
#define TIMER64_REG_GPDIR_DAT   0x0c
#define TIMER64_REG_TIM12       0x10
#define TIMER64_REG_TIM34       0x14
#define TIMER64_REG_PRD12       0x18
#define TIMER64_REG_PRD34       0x1c
#define TIMER64_REG_TCR         0x20
#define TIMER64_REG_TGCR        0x24

/* Register values. The timer is only defined to run in 64 bit mode */
#define TIMER64_REG_VAL_TCR_DISABLE     0      /* Timer is disabled */
#define TIMER64_REG_VAL_TCR_1SHOT       0x40   /* Timer runs 1 time */
#define TIMER64_REG_VAL_TCR_RELOAD      0x80   /* Timer runs continuously */

#define TIMER64_REG_VAL_TGCR_DISABLE    0       /* Timer is disabled */
#define TIMER64_REG_VAL_TGCR_64         0x3     /* Timer running 64 bit mode */

#define TIMER64_REG_VAL_GPINT_EN        0       /* All pins timer, not gpio */







#endif /* _TIMER64LOC_H */
