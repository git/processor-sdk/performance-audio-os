/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/
/************************************************************
                     Include files
*************************************************************/
#include <stdio.h>
#include <math.h>
#include <xdc/std.h>
#include <alg.h>
#include <dm.h>
#include <stdlib.h>
#include <string.h>
#include "paftyp.h"
#include "testapp.h"

const test_case dm_test_cases[] = 
{

 {//1
         "Stereo Pass through case", //charTestCaseName
         //Channel Configuraion
         PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0, //channelConfigurationStream
         "../../test_vectors/input/2_1_wav.wav", //charInFileName
         "../../test_vectors/output/output_1.wav", //charOutFileName
         0,
         2,
         2,
         PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//2
         "Stereo 2 Mono channel Map",
         //Channel Configuraion
         PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/2_1_wav.wav",
         "../../test_vectors/output/output_2.wav",
         0,
         2,
         1,
         PAF_CC_SAT_MONO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//3
         "5.1 input Pass through ",
         //Channel configuration
         PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/5_1_wav.wav",
         "../../test_vectors/output/output_6.wav",
         0,
         6,
         6,
         PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         0
 },{//4
         "5.1 input to 3stereo ",
         //Channel configuration
         PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/5_1_wav.wav",
         "../../test_vectors/output/output_12.wav",
         0,
         6,
         3,
         PAF_CC_SAT_3STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//5
         "5.1 input to Stereo Downmix",
         //Channel configuration
         PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/5_1_wav.wav",
         "../../test_vectors/output/output_9.wav",
         0,
         6,
         2,
         PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//6
         "5.1 input to mono",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/5_1_wav.wav",
         "../../test_vectors/output/output_59.wav",
         0,
         6,
         1,
         PAF_CC_SAT_MONO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         1
 },{//7
         "7.1 input Pass through ",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_16.wav",
         0,
         8,
         8,
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         0
 },{//8
         "7.1 input to 5.1 downmix",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_20.wav",
         0,
         8,
         6,
         PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         0
 },{//9
         "7.1 input to stereo-3 downmix ",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_28.wav",
         0,
         8,
         3,
         PAF_CC_SAT_3STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//10
         "7.1 input to stereo downmix ",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_24.wav",
         0,
         8,
         2,
         PAF_CC_SAT_STEREO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//11
         "7.1 input to mono",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_57.wav",
         0,
         8,
         1,
         PAF_CC_SAT_MONO, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//12
         "Surround4 to panthom3 ",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
          "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_36.wav",
         0,
         8,
         5,
         PAF_CC_SAT_PHANTOM3, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//13
         "Surround4 to Panthom2 ",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_32.wav",
         0,
         8,
         4,
         PAF_CC_SAT_PHANTOM2, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//14
         "Surround4 to panthom1 ",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_40.wav",
         0,
         8,
         3,
         PAF_CC_SAT_PHANTOM1, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//15
         "Surround4 to panthom4 ",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_44.wav",
         0,
         8,
         3,
         PAF_CC_SAT_PHANTOM4, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//16
         "Surround4 to surround3 ",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_49.wav",
         0,
         8,
         6,
         PAF_CC_SAT_SURROUND3, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//17
         "Surround4 to surround2 ",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_45.wav",
         0,
         8,
         5,
         PAF_CC_SAT_SURROUND2, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 },{//18
         "Surround4 to surround1 ",
         //Channel configuration
         PAF_CC_SAT_SURROUND4, PAF_CC_SUB_ONE, 0, 0, 0, 0, 0, 0,
         "../../test_vectors/input/7_1_wav.wav",
         "../../test_vectors/output/output_53.wav",
         0,
         8,
         4,
         PAF_CC_SAT_SURROUND1, PAF_CC_SUB_ZERO, 0, 0, 0, 0, 0, 0,
         0
 }
};
