
/******************************************************************************
* Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// DA10x SPI, I2C and UART slave control status (DCS7) implementation
//
//
//

#ifndef __DCS7_PARAMS_H__
#define __DCS7_PARAMS_H__ 


#include <xdc/std.h>
#include <ti/drv/uart/UART.h>
#include <ti/drv/spi/SPI.h>
#include <ti/drv/i2c/I2C.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/knl/Clock.h>

// Internally used
#define MAX_S1_COUNT 18


/* Parameters declaration */
#define DCS7_PARAMS_DEV_SPI_CLEN_8     8
#define DCS7_PARAMS_DEV_SPI_CLEN_16    16
#define DCS7_PARAMS_DEV_SPI_NP_3       0
#define DCS7_PARAMS_DEV_SPI_NP_4_SCS   1
//#define DCS7_PARAMS_DEV_SPI_NP_4_ENA   2
//#define DCS7_PARAMS_DEV_SPI_NP_5       3
//#define DCS7_PARAMS_DEV_SPI_NP_5_SRDY  4
//#define DCS7_PARAMS_DEV_SPI_POL_LOW    0
//#define DCS7_PARAMS_DEV_SPI_POL_HIGH   1
//#define DCS7_PARAMS_DEV_SPI_PHA_IN     0
//#define DCS7_PARAMS_DEV_SPI_PHA_OUT    1

#define DCS7_PARAMS_DEV_SPI_ENAHIZ_DIS 0
#define DCS7_PARAMS_DEV_SPI_ENAHIZ_ENA 1
typedef struct DCS7_Params_Dev_Spi{
    Uint32 size;                    /* size of the structure */
    Uint8 np;                       /* number of pins */
    Uint8 enahiz;                   /* SPIENA pin mode */
    Uint8 reserved[2];
    Uint32 emask;                   /* error mask */
    SPI_Handle hSpi;                /* handle to SPI lld */
    SPI_Params *spiParams;          /* SPI Initialization param */
    SPI_Transaction transaction;    /* SPI transfer info */
}DCS7_Params_Dev_Spi;

typedef struct DCS7_Params_Dev_I2c
{
    Uint32 size;                    /* size of the structure */
    I2C_Handle hI2c;                /* handle to I2C lld, only dynamic init parameter */
    I2C_Params i2cParams;           /* I2C Initialization parameters */
    I2C_Transaction transaction;    /* I2C transfer info */
    Uint8 ownSlaveAddress;          /* Own slave address of K2G */
    Uint8 reserved[3];              /* reserved */
    Uint32 emask;                   /* Error mask */ 
} DCS7_Params_Dev_I2c;

#ifdef UARTCALLBACK
typedef struct DCS7_swiObj *swi_handle;
typedef struct DCS7_callbackObj *callback_handle;
#endif

typedef struct DCS7_Params_Dev_Uart
{
    Uint32 size;              /* size of the structure */
    Uint32 emask;             /* error mask */
    UART_Handle hUart;        /* handle to uart lld, only dynamic init parameter */
#ifdef UARTCALLBACK
    UART_Callback transferCallbackFxn;     /* function pointer to handle
                                              the transfer operations routine */
    swi_handle hSwi;                       /* Handle for swi used in medialayer */
    callback_handle hCallback;             /* Handle for swi used in medialayer */
#endif
    Uint16 isEMDAConfigured;  /* Flag to indicate EDMA configuration completion */
    Uint16 scratchBufSize;    /* Scratch buffer size allocated for internal operation */
    Uint8 *scratchBuf;        /* Scratch buffer allocated for internal operation */
    UART_Params uartParams;   /* UART Initialization parameters */
    Uint32 baudRate;          /* UART baudrate */
} DCS7_Params_Dev_Uart;

typedef struct DCS7_Params_Dev
{
    union
    {
        Uint32 size;
        DCS7_Params_Dev_Spi *pSpi;
        DCS7_Params_Dev_I2c *pI2c;
        DCS7_Params_Dev_Uart *pUart;
    } pDevData; 
} DCS7_Params_Dev;

#define DCS7_PARAMS_DEV_SPI0    0
#define DCS7_PARAMS_DEV_SPI1    1 
#define DCS7_PARAMS_DEV_I2C0    2
#define DCS7_PARAMS_DEV_I2C1    3
#define DCS7_PARAMS_DEV_UART0   4
#define DCS7_PARAMS_DEV_UART1   5
#define DCS7_PARAMS_DEV_UART2   6
#define DCS7_PARAMS_EDR_DIS     0
#define DCS7_PARAMS_EDR_ENA     1 

typedef struct DCS7_Params
{
    Uint32 size;    /* size of the structure */
    Uint8 dev;      /* device */
    Uint8 edr;      /* error detection and reporting */
    Uint8 res[2];   /* reserved */
    Uint16 rxSize;  /* receive buffer size */
    Uint16 rxHwm;   /* receive buffer high watermark,  */
    Uint16 rxLwm;   /* receive buffer low watermark */
    Uint16 txSize;  /* transmit buffer size */
    DCS7_Params_Dev *pDev;    /* device parameters */
} DCS7_Params;
extern const DCS7_Params DCS7_PARAMS;


#endif
