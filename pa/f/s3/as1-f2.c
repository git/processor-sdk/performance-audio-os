/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Performance Audio Framework Series 3 #2 -- Function Definitions
//
//     Audio Framework is Audio Streams 1-N for IROM.
//
//
//

#include <stdio.h>
#include <std.h>
#include <alg.h>
#include <ialg.h>
#include <sio.h>
#include <mem.h>
#include <tsk.h>
#include <string.h> // memset
#include <sts.h>
#include <clk.h>


// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
// 
#include <logp.h>
#include <dp.h>

// #define DP_TRACE_ARC
#ifdef DP_TRACE_ARC
#define TRACE_ARC(a) dp a
#else
#define TRACE_ARC(a) 
#endif

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
// For release, you might set the mask to 0, but I'd always leave it at 1.
#define CURRENT_TRACE_MASK      0x01   // terse only

#define TRACE_MASK_TERSE        0x01   // only flag errors and show init
#define TRACE_MASK_GENERAL      0x02   // half dozen lines per frame
#define TRACE_MASK_VERBOSE      0x04   // trace full operation
#define TRACE_MASK_DATA         0x08   // Show data
#define TRACE_MASK_TIME         0x10   // Timing related traces

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
 #define TRACE_DATA(a) LOG_printf a
#else
 #define TRACE_DATA(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a

// consolidate list of processing strings, indexed by PAF_SOURCE
static char *procName[] =
{
        "", //PAF_SOURCE_UNKNOWN
        "", //PAF_SOURCE_NONE
        "AS%d: Pass processing ...",   //PAF_SOURCE_PASS
        "AS%d: SNG processing ...",    //PAF_SOURCE_SNG
        "AS%d: Auto processing ...",   //PAF_SOURCE_AUTO
        "AS%d: Auto processing ...",   //PAF_SOURCE_BITSTREAM
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTSALL
        "AS%d: PCM processing ...",    //PAF_SOURCE_PCMAUTO
        "AS%d: PCM processing ...",    //PAF_SOURCE_PCM
        "AS%d: PCN processing ...",    //PAF_SOURCE_PC8
        "AS%d: AC3 processing ...",    //PAF_SOURCE_AC3
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS
        "AS%d: AAC processing ...",    //PAF_SOURCE_AAC
        "AS%d: MPG processing ...",    //PAF_SOURCE_MPEG
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS12
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS13
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS14
        "AS%d: DTS processing ...",    //PAF_SOURCE_DTS16
        "AS%d: WMP processing ...",    //PAF_SOURCE_WMA9PRO
        "AS%d: MP3 processing ...",    //PAF_SOURCE_MP3
        "AS%d: DSD processing ...",    //PAF_SOURCE_DSD1
        "AS%d: DSD processing ...",    //PAF_SOURCE_DSD2
        "AS%d: DSD processing ...",    //PAF_SOURCE_DSD3
        "AS%d: DDP processing ...",    //PAF_SOURCE_DDP
        "AS%d: DTSHD processing ...",  //PAF_SOURCE_DTSHD
        "AS%d: THD processing ...",    //PAF_SOURCE_THD
        "AS%d: DXP processing ...",    //PAF_SOURCE_DXP
        "AS%d: WMA processing ...",    //PAF_SOURCE_WMA
};

#else
 #define TRACE_VERBOSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_TIME)
 #define TRACE_TIME(a) LOG_printf a
 #define TIME_MOD  trace // this could be different
 static Int dtime()
 {
     static Int old_time = 0;
     Int time = TSK_time();
     Int delta_time = time - old_time;
     old_time = time;
     return( delta_time);
 }
 
 static char *stateName[11] =
 {
     "INIT",
     "INFO1",
     "AGAIN",
     "INFO2",
     "CONT",
     "TIME",
     "DECODE",
     "STREAM",
     "ENCODE",
     "FINAL",
     "QUIT"
 };

#else
 #define TRACE_TIME(a)
#endif

// This works to set a breakpoint
#define SW_BREAKPOINT       asm( " SWBP 0" );
/* Software Breakpoint to Code Composer */
// SW_BREAKPOINT;


// .............................................................................

#include <paf_alg.h>
#include <paf_ialg.h>
#include <pafsio.h>
#include <paferr.h>

#include <acp.h>
#include <acp_mds.h>

#include <pcm.h>

#include <pce.h>
#include <pceerr.h>

#include <dob.h>
#include <doberr.h>
#include <outbuf.h>

#include <dib.h>
#include <diberr.h>
#include <inpbuf.h>

#include <paftyp.h>
#include <stdasp.h>
#include <acpbeta.h>
#include <ccm.h>

#include <as0.h>
#include <asp0.h>
#include <asperr.h>

#include <paf_alg_print.h>
#include "pafhjt.h"

//
// Framework Declarations
//

#include <as1-f2.h>
#include <as1-f2-params.h>
#include <as1-f2-patchs.h>
#include <as1-f2-config.h>

// .............................................................................

/* ---------------------------------------------------------------- */
/* For historical reasons, macro definitions (#define ...) are used */
/* to hide parameter references (pP->...) throughout this file, but */
/* only for select quantities and not for all parameter elements.   */
/*                                                                  */
/*             Parameter macro definitions start here.              */
/* ---------------------------------------------------------------- */

//
// Audio Topology (Zone) Definitions
//
//   The ZONE, a historical term here, is a letter identifying the Audio
//   Topology with a single letter or "*" for a variable quantization of
//   same.
//
//   The Zone Elements listed here indicate the cardinality of the corre-
//   sponding element:
//
//     INPUTS   Number of inputs.
//     DECODES  Number of decodes.
//     STREAMS  Number of streams.
//     ENCODES  Number of encodes.
//     OUTPUTS  Number of outputs.
//
//   The Zone Element Counts listed here indicate the first (1) and after-
//   last (N) values suitable for use in a loop:
//
//     INPUTS[1N]  for inputs.
//     DECODES[1N] for decodes.
//     STREAMS[1N] for streams.
//     ENCODES[1N] for encodes.
//     OUTPUTS[1N] for outputs.
//
//   The Zone Element Count Limits listed here establish array sizes:
//
//     DECODEN_MAX for decodes.
//     STREAMN_MAX for streams.
//     ENCODEN_MAX for encodes.
//
//   The Zone Master is important in multi-input frameworks:
//
//     MASTER   In a uni-input zone, the count of the input.
//              In a multi-input zone, the count of the primary which controls
//              the secondary or secondaries.
//

#ifndef ZONE

#define ZONE "*" /* 19.53 kB of 38.80 kB FW */

#define MASTER  pP->zone.master
#define INPUTS  pP->zone.inputs
#define DECODES pP->zone.decodes
#define STREAMS pP->zone.streams
#define ENCODES pP->zone.encodes
#define OUTPUTS pP->zone.outputs

#endif /* ZONE */

#ifndef ZONE

#define ZONE "I" /* ~2.5 kB less than above */

#define MASTER  0
#define INPUTS  1
#define DECODES 1
#define STREAMS 1
#define ENCODES 1
#define OUTPUTS 1

#endif /* ZONE */

#define INPUT1  pP->zone.input1
#define INPUTN  pP->zone.inputN

#define DECODE1 pP->zone.decode1
#define DECODEN pP->zone.decodeN
#define DECODEN_MAX 3

#define STREAM1 0
#define STREAMN STREAMS
#define STREAMN_MAX 5

#define ENCODE1 pP->zone.encode1
#define ENCODEN pP->zone.encodeN
#define ENCODEN_MAX 3

#define OUTPUT1 pP->zone.output1
#define OUTPUTN pP->zone.outputN

// .............................................................................

//
// Heap & Memory Allocation Definitions
//

#define HEAP_INTERNAL *pP->heap.pIntern
#define HEAP_INTERNAL1 *pP->heap.pIntern1
#define HEAP_EXTERNAL *pP->heap.pExtern
#define HEAP_CLEAR pP->heap.clear

#define HEAP_INPBUF *pP->heap.pInpbuf
#define HEAP_OUTBUF *pP->heap.pOutbuf
#define HEAP_FRMBUF *pP->heap.pFrmbuf

#define COMMONSPACE pP->common.space

//
// Audio Stream Definitions
//

#define DEC_MODE_CONTINUOUS (1<<1)

#define RX_BUFSIZE(Z) pP->z_rx_bufsiz[Z]

#define NUM_TX_CHAN(Z) pP->z_numchan[Z] /* as per stream numchan */

//
// Audio Data Representation Definitions
//

/* audio frame "width" in channels */

#define numchan pP->z_numchan

/* audio frame "length" in samples */

#define FRAMELENGTH pP->framelength

Int PAF_AST_decodeHandleErrorInput (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int z, Int error);

// minimum audio frame "length" in samples (must be multiple of PA_MODULO)
#define MINFRAMELENGTH 24
#define PA_MODULO       8   // also defined independently in ARC2 code, and may be hard coded other places.

Int PAF_AST_computeRateRatio (const PAF_AST_Params *pP, PAF_AST_Config *pC, double *arcRatio, Int frame); 

//
// Decoder Definitions
//

#define decLinkInit pQ->i_decLinkInit

//
// Audio Stream Processing Definitions
//

#define aspLinkInit pQ->i_aspLinkInit

//
// Encoder Definitions
//

#define encLinkInit pQ->i_encLinkInit

/* ---------------------------------------------------------------- */
/*              Parameter macro definitions end here.               */
/* ---------------------------------------------------------------- */

//
// Standardized Definitions
//

#define DEC_Handle PCM_Handle /* works for all: SNG, PCM, AC3, DTS, AAC */
#define ENC_Handle PCE_Handle /* works for all: PCE */

#define __TASK_NAME__  "as1-f2"

// -----------------------------------------------------------------------------
// Audio Stream Task
//
//   Name:      audioStream1Task
//   Purpose:   BIOS Task Function for Audio Framework Number 2.
//   From:      BIOS
//   Uses:      See code.
//   States:    x
//   Return:    Returns void on initialization failure.
//              Otherwise, does not return.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information on initialization.
//              * State information on processing.
//              * Memory allocation errors.
//              * Error number macros.
//              * Line number macros.
//

LINNO_DECL (audioStream1Task); /* Line number macros */
ERRNO_DECL (audioStream1Task); /* Error number macros */

void
audioStream1Task (Int betaPrimeValue, const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ)
{
    // Task data

    PAF_AST_Config PAF_AST_config;      /* Local configuration */
    PAF_AST_Config *pC;                 /* Local configuration pointer */

    // Local data
    Int as = betaPrimeValue + 1;        /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* input/encode/stream/decode/output */
                                        /* counter                           */
    Int i;                              /* phase */
    Int errno;                          /* error number */
    Int zMD, zMI, zMS, zX;

    Int loopCount = 0;	// used to stop trace to see startup behavior.

    //
    // Audio Framework Configuration (*pC):
    //
    //   Set default.
    //

    pC = &PAF_AST_config;
    pC->as = as;

    TRACE_TERSE((&TR_MOD, "as1_f2 task started with betaPrimeValue %d.", betaPrimeValue));

    //
    // Audio Framework Parameters & Patch (*pP, *pQ):
    //

    if (! pP) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: AS%d: No Parameters defined. Exiting.", __TASK_NAME__, __FUNCTION__, __LINE__));
        LINNO_RPRT (audioStream1Task, -1);
        return;
    }

    if (! pQ) {
        TRACE_TERSE((&TR_MOD, "%s: %s.%d: AS%d: No Patchs defined. Exiting.", __TASK_NAME__, __FUNCTION__, __LINE__));
        LINNO_RPRT (audioStream1Task, -1);
        return;
    }

    //
    // Initialize message log trace and line number reporting
    //

    for (z=STREAM1; z < STREAMN; z++)
        TRACE_TERSE((&TR_MOD, "as1_f2.%d: AS%d: initiated", __LINE__, as+z));
    LINNO_RPRT (audioStream1Task, -1);

    //
    // Determine stream and decoder indices associated with the master input
    //
    zMI = pP->zone.master;
    pC->masterDec = zMI;
    pC->masterStr = zMI;
    for (zX = DECODE1; zX < DECODEN; zX++) {
        if (pP->inputsFromDecodes[zX] == zMI) {
            pC->masterDec = zX;
            pC->masterStr = pP->streamsFromDecodes[zX];
            break;
        }
    }
    zMD = pC->masterDec;
    zMS = pC->masterStr;

    // Initialize as per parameterized phases:
    //
    //   In standard form these are:
    //   - Malloc: Memory Allocation
    //   - Config: Configuration Initialization
    //   - AcpAlg: ACP Algorithm Initialization and Local Attachment
    //   - Common: Common Algorithm Initialization
    //   - Device: I/O Device Initialization
    //   - Unused: (available)
    //   - Unused: (available)
    //

	

    LINNO_RPRT (audioStream1Task, -2);
    for (i=0; i < lengthof (pP->fxns->initPhase); i++) {
        Int linno;
        if (pP->fxns->initPhase[i]) {
            if (linno = pP->fxns->initPhase[i] (pP, pQ, pC)) {
                LINNO_RPRT (audioStream1Task, linno);
                return;
            }
        }
        else {
            TRACE_TERSE((&TR_MOD, "as1_f2.%d: AS%d: initialization phase - null", __LINE__, as+zMS));
        }
        TRACE_TERSE((&TR_MOD, "as1_f2.%d: AS%d: initialization phase - %d completed", __LINE__,as+zMS, i));
        LINNO_RPRT (audioStream1Task, -i-3);
    }

#ifdef _WIN32
	{
		extern PAF_AST_Config *pPAFConfig;                 /* PAF configuration pointer */
		
		pPAFConfig = pC;
	}
#endif

    //
    // End of Initialization -- final memory usage report.
    //
    if(pP->fxns->memStatusPrint)
                pP->fxns->memStatusPrint(HEAP_INTERNAL,HEAP_EXTERNAL,HEAP_INTERNAL1);
    //
    // Main processing loop
    //

    for (z=STREAM1; z < STREAMN; z++)
        TRACE_VERBOSE((&TR_MOD, "as1_f2.%d: AS%d: running", __LINE__, as+z));

    TRACE_TERSE((&TR_MOD, "as1_f2.%d: Entering Main Loop.", __LINE__));

    errno = 0;
    for (;;) {

        Int sourceSelect;
        XDAS_Int8 sourceProgram;

        loopCount++;

#if 0   // enable and tune to see startup behavior.
        // this is an alternative to fixed/circular setting in pa.cfg.
        // If you are searching for a memory allocation failure, disable on first round.
        // All allocation has already happened.
        // This is the outer loop.  This loop count goes up when the stream resets.
        // If the stream is running without problems, this does not increment.
        // If the stream is repeatedly resetting, this loop count will go up rapidly.
        if (loopCount > 10)  // see traces for a few of the passes through the main loop.
        {
           	TRACE_TERSE((&TR_MOD, "as1_f2.%d: Trace stopped at loop %d.", __LINE__, loopCount));
         	LOG_disable(&TR_MOD);  // stop tracing
        }
#endif

        TRACE_GEN((&TR_MOD, "as1_f2.%d (begin Main loop %d) (errno 0x%x)", __LINE__, loopCount, errno));
        TRACE_TIME((&TIME_MOD, "as1_f2... + %d = %d (begin Main loop)", dtime(), TSK_time()));

        // since not decoding indicate such
        pP->fxns->sourceDecode (pP, pQ, pC, PAF_SOURCE_NONE);

        // any error forces idling of input
        if (errno) {
            for (z=INPUT1; z < INPUTN; z++)
                if (pC->xInp[z].hRxSio)
                    SIO_idle (pC->xInp[z].hRxSio);

            TRACE_TERSE((&TR_MOD, "as1_f2.%d: AS%d: errno = 0x%x", __LINE__, as+zMS, errno));
            ERRNO_RPRT (audioStream1Task, errno);
        }

        // Execute a TSK_sleep to ensure that any non-blocking code paths are broken
        // up to allow lower priority tasks to run. This may seem odd to be at the top
        // of the state machine but provides for a cleaner flow even though the very
        // first time we enter we do a sleep which is non-intuitive.
        TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: AS%d: ... sleeping ...", __LINE__, as+zMS));
        TRACE_TIME((&TIME_MOD, "as1-f2... + %d = %d (begin SLEEP)", dtime(), TSK_time()));
        TSK_sleep (1);

        TRACE_GEN((&TR_MOD, "as1-f2.%d: AS%d: Device selection ...", __LINE__, as+zMS));
        if (errno = pP->fxns->selectDevices (pP, pQ, pC))
        {
            TRACE_TERSE((&TR_MOD, "as1-f2: selectDevices returned errno = 0x%04x at line %d. AS%d", errno, __LINE__, as+zMS));
            continue;
        }

        // if no master input selected then we don't know what may be at the input
        // so set to unkown and skip any remaining processing
        if (! pC->xInp[zMI].hRxSio) {
            pC->xDec[zMD].decodeStatus.sourceProgram = PAF_SOURCE_UNKNOWN;
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: AS%d: No input selected...", __LINE__, as+zMS));
            continue;
        }

        // if here then we have a valid input so query its status
        if (errno = pP->fxns->updateInputStatus (pC->xInp[zMI].hRxSio, &pC->xInp[zMI].inpBufStatus, &pC->xInp[zMI].inpBufConfig))
        {
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: continue as updateInputStatus returns 0x%x", __LINE__, errno));
            continue;
        }

        // If master decoder is not enabled, or the input is unlocked, then do nothing
        if (!pC->xDec[zMD].decodeStatus.mode || !pC->xInp[zMI].inpBufStatus.lock)
        {
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: Not locked, continue", __LINE__));
            continue;
        }

        // if special continuous mode then jump straight to decodeProcessing
        if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS) {
            errno = pP->fxns->decodeProcessing (pP, pQ, pC, NULL);
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: DEC_MODE_CONTINUOUS: continue", __LINE__));
            continue;
        }

        // If no source selected then do nothing
        if (pC->xDec[zMD].decodeStatus.sourceSelect == PAF_SOURCE_NONE) {
            pC->xDec[zMD].decodeStatus.sourceProgram = PAF_SOURCE_NONE;
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d.  AS%d: no source selected, continue", __LINE__, as+zMS));
            continue;
        }

        // If we want pass processing then proceed directly
        if (pC->xDec[zMD].decodeStatus.sourceSelect == PAF_SOURCE_PASS) {
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d, AS%d: Pass processing ...\n", __LINE__, as+zMS));
            pC->xDec[zMD].decodeStatus.sourceProgram = PAF_SOURCE_PASS;
            pP->fxns->sourceDecode (pP, pQ, pC, PAF_SOURCE_PASS);
            if (pP->fxns->passProcessing)
                errno = pP->fxns->passProcessing (pP, pQ, pC, NULL);
            else {
                TRACE_TERSE((&TR_MOD, "as1-f2.%d: Pass Processing not supported, errno 0x%x", __LINE__, as+zMS, ASPERR_PASS));
                errno = ASPERR_PASS;
            }
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: continue", __LINE__));
            continue;
        }

        // .....................................................................
        // At this point we have an enabled input and want to decode something.
        // If no decoder selected then do nothing. Need to reset the sourceProgram, since
        // when no decoder is selected there are no calls to IB

        if (errno = pP->fxns->autoProcessing (pP, pQ, pC, pC->xDec[zMD].decodeStatus.sourceSelect, pC->xDec[zMD].decAlg[PAF_SOURCE_PCM]))
        {
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: autoProcessing returns 0x%x, continue", __LINE__, errno));
            continue;
        }

        // query for input type
        if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_SOURCEPROGRAM, (Arg )&sourceProgram))
        {
            TRACE_TERSE((&TR_MOD, "as1-f2.%d: SIO_ctrl returns 0x%x, then 0x%x, continue", __LINE__, errno, ASPERR_AUTO_PROGRAM));
            errno = ASPERR_AUTO_PROGRAM;
            continue;
        }
        pC->xDec[zMD].decodeStatus.sourceProgram = sourceProgram;

        // if input is unclassifiable then do nothing
        if (sourceProgram == PAF_SOURCE_UNKNOWN)
        {
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: Source program unknown. continue", __LINE__));
            continue;
        }

        // now that we have some input classification, and possibly an outstanding
        // input frame, we determine whether or not to call decodeProcessing and with
        // what decAlg.
        sourceSelect = PAF_SOURCE_NONE;
        switch (pC->xDec[zMD].decodeStatus.sourceSelect) {

            // If autodetecting, decoding everything, and input is something
            // (i.e. bitstream or PCM) then decode.
            case PAF_SOURCE_AUTO:
                if (sourceProgram >= PAF_SOURCE_PCM)
                    sourceSelect = sourceProgram;
                break;

            // If autodetecting, decoding only PCM, and input is PCM then decode.
            case PAF_SOURCE_PCMAUTO:
                if (sourceProgram == PAF_SOURCE_PCM)
                    sourceSelect = sourceProgram;
                break;

            // If autodetecting, decoding only bitstreams, and input is a bitstream then decode.
            case PAF_SOURCE_BITSTREAM:
                if (sourceProgram >= PAF_SOURCE_AC3)
                    sourceSelect = sourceProgram;
                break;

            // If autodetecting, decoding only DTS, and input is DTS then decode.
            case PAF_SOURCE_DTSALL:
                switch (sourceProgram) {
                    case PAF_SOURCE_DTS11:
                    case PAF_SOURCE_DTS12:
                    case PAF_SOURCE_DTS13:
                    case PAF_SOURCE_DTS14:
                    case PAF_SOURCE_DTS16:
                    case PAF_SOURCE_DTSHD:
                        sourceSelect = sourceProgram;
                        break;
                }
                break;

            // All others, e.g., force modes, fall through to here.
            // If user made specific selection then program must match select.
            // (NB: this compare relies on ordering of PAF_SOURCE)
            default:
                sourceSelect = pC->xDec[zMD].decodeStatus.sourceSelect;
                if ((sourceSelect >= PAF_SOURCE_PCM) && (sourceSelect <= PAF_SOURCE_N)) {
                    if (sourceProgram != sourceSelect)
                        sourceSelect = PAF_SOURCE_NONE;
                }
                break;
        }

        // if we didn't find any matches then skip
        if (sourceSelect == PAF_SOURCE_NONE)
        {
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: no matching source type, continue", __LINE__));
            continue;
        }

        // set to unknown so that we can ensure, for IOS purposes, that sourceDecode = NONE
        // iff we are in this top level state machine and specifically not in decodeProcessing
        pP->fxns->sourceDecode (pP, pQ, pC, PAF_SOURCE_UNKNOWN);

        TRACE_VERBOSE((&TR_MOD, procName[sourceProgram], as+zMS));

        TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: calling decodeProcessing.", __LINE__));
        errno = pP->fxns->decodeProcessing(pP, pQ, pC, pC->xDec[zMD].decAlg[sourceSelect]);
        if (errno)
        {
            TRACE_TERSE((&TR_MOD, "as1-f2.%d: decodeProcessing returns 0x%x, continue", __LINE__, errno));
        }
        else
        {
        	TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: decodeProcessing complete with no error.", __LINE__));
        }

    }  // End of main processing loop for (;;) .

} //audioStream1Task

// -----------------------------------------------------------------------------
// AST Initialization Function - Memory Allocation
//
//   Name:      PAF_AST_initPhaseMalloc
//   Purpose:   Audio Stream Task Function for initialization of data pointers
//              by allocation of memory.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on MEM_calloc failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//

Int
PAF_AST_initPhaseMalloc (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int zMS = pC->masterStr;

    TRACE_TERSE((&TR_MOD, "\nas1-f2: AS%d: initialization phase - memory allocation", as+zMS));

    if (! (pC->xInp = MEM_calloc (HEAP_INTERNAL, INPUTN * sizeof (*pC->xInp), 4))) {
        TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: MEM_calloc failed", as+zMS, __FUNCTION__, __LINE__));
        printf("AS%d: %s.%d: MEM_calloc failed", as+zMS, __FUNCTION__, __LINE__);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc.%d:", __LINE__));
    TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc. (pC->xInp) %d bytes from space %d at 0x%x.",
    		INPUTN * sizeof (*pC->xInp),
    		HEAP_INTERNAL, pC->xInp));

    if (! (pC->xDec = MEM_calloc (HEAP_INTERNAL, DECODEN * sizeof (*pC->xDec), 4))) {
        TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: MEM_calloc failed", as+zMS, __FUNCTION__, __LINE__));
        printf("\nAS%d: %s.%d: MEM_calloc failed.  See LOG_printf trace.\n", as+zMS, __FUNCTION__, __LINE__);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc.%d:", __LINE__));
    TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc. (pC->xDec) %d bytes from space %d at 0x%x.",
    		DECODEN * sizeof (*pC->xDec),
    		HEAP_INTERNAL, pC->xDec));

    if (! (pC->xStr = MEM_calloc (HEAP_INTERNAL, STREAMN * sizeof (*pC->xStr), 4))) {
        TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: MEM_calloc failed", as+zMS, __FUNCTION__, __LINE__));
        printf("\nAS%d: %s.%d: MEM_calloc failed.  See LOG_printf trace.\n", as+zMS, __FUNCTION__, __LINE__);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc.%d:", __LINE__));
    TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc. (pC->xStr) %d bytes from space %d at 0x%x.",
    		STREAMN * sizeof (*pC->xStr),
    		HEAP_INTERNAL, pC->xStr));

    {
        Int z;                          /* stream counter */

        PAF_AudioFrame *fBuf;

        if (! (fBuf = (PAF_AudioFrame *)MEM_calloc (HEAP_INTERNAL, STREAMS * sizeof (*fBuf), 4))) {
            TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: MEM_calloc failed", as+zMS, __FUNCTION__, __LINE__));
            printf("\nAS%d: %s.%d: MEM_calloc failed.  See LOG_printf trace.\n", as+zMS, __FUNCTION__, __LINE__);
            SW_BREAKPOINT;
            return __LINE__;
        }
        TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc.%d:", __LINE__));
        TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc. (fBuf) %d bytes from space %d at 0x%x.",
        		STREAMS * sizeof (*fBuf),
        		HEAP_INTERNAL, fBuf));

        for (z=STREAM1; z < STREAMN; z++)
        {
            pC->xStr[z].pAudioFrame = &fBuf[z-STREAM1];
            TRACE_TERSE((&TR_MOD, "pC->xStr[%d].pAudioFrame = 0x%x", z, pC->xStr[z].pAudioFrame));
        }
    }

    if (! (pC->xEnc = MEM_calloc (HEAP_INTERNAL, ENCODEN * sizeof (*pC->xEnc), 4))) {
        TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: MEM_calloc failed", as+zMS, __FUNCTION__, __LINE__));
        printf("\nAS%d: %s.%d: MEM_calloc failed.  See LOG_printf trace.\n", as+zMS, __FUNCTION__, __LINE__);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc.%d:", __LINE__));
    TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc. (pC->xEnc) %d bytes from space %d at 0x%x.",
    		ENCODEN * sizeof (*pC->xEnc),
    		HEAP_INTERNAL, pC->xEnc));

    if (! (pC->xOut = MEM_calloc (HEAP_INTERNAL, OUTPUTN * sizeof (*pC->xOut), 4))) {
        TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: MEM_calloc failed", as+zMS, __FUNCTION__, __LINE__));
        printf("\nAS%d: %s.%d: MEM_calloc failed.  See LOG_printf trace.\n", as+zMS, __FUNCTION__, __LINE__);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc.%d:", __LINE__));
    TRACE_TERSE((&TR_MOD, "PAF_AST_initPhaseMalloc. (pC->xOut) %d bytes from space %d at 0x%x.",
    		OUTPUTN * sizeof (*pC->xOut),
    		HEAP_INTERNAL, pC->xOut));

    TRACE_TERSE((&TR_MOD, "as1-f2: AS%d: initialization phase - memory allocation complete.\n", as+zMS));
    return 0;
} //PAF_AST_initPhaseMalloc

// -----------------------------------------------------------------------------
// AST Initialization Function - Memory Initialization from Configuration
//
//   Name:      PAF_AST_initPhaseConfig
//   Purpose:   Audio Stream Task Function for initialization of data values
//              from parameters.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Other as per initFrame0 and initFrame1.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_initPhaseConfig (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* input/encode/stream/decode/output */
                                        /* counter                           */
    Int zMS = pC->masterStr;

    (void)zMS;  (void)as;

    TRACE_TERSE((&TR_MOD, "\nAS%d: %s.%d: initialization phase - configuration ",
                   as+zMS, __FUNCTION__, __LINE__));

    // Unspecified elements have been initialized to zero during alloc.

    for (z=INPUT1; z < INPUTN; z++) {
        pC->xInp[z].inpBufStatus = *pP->pInpBufStatus;
        pC->xInp[z].inpBufConfig.pBufStatus = &pC->xInp[z].inpBufStatus;
    }

    for (z=DECODE1; z < DECODEN; z++) {
        Int zI = pP->inputsFromDecodes[z];
        Int zS = pP->streamsFromDecodes[z];
        pC->xDec[z].decodeControl.size = sizeof(pC->xDec[z].decodeControl);
        pC->xDec[z].decodeControl.pAudioFrame = pC->xStr[zS].pAudioFrame;
        pC->xDec[z].decodeControl.pInpBufConfig =
            (const PAF_InpBufConfig *) &pC->xInp[zI].inpBufConfig;
        pC->xDec[z].decodeStatus = *pP->z_pDecodeStatus[z];
        pC->xDec[z].decodeInStruct.pAudioFrame = pC->xStr[zS].pAudioFrame;
    }

    for (z=STREAM1; z < STREAMN; z++) {
        Int linno;
        if (linno = pP->fxns->initFrame0 (pP, pQ, pC, z))
            return linno;
        if (linno = pP->fxns->initFrame1 (pP, pQ, pC, z, -1))
            return linno;
    }

    for (z=ENCODE1; z < ENCODEN; z++) {
        Int zO = pP->outputsFromEncodes[z];
        Int zS = pP->streamsFromEncodes[z];
        pC->xEnc[z].encodeControl.size = sizeof(pC->xEnc[z].encodeControl);
        pC->xEnc[z].encodeControl.pAudioFrame = pC->xStr[zS].pAudioFrame;
        pC->xEnc[z].encodeControl.pVolumeStatus = &pC->xEnc[z].volumeStatus;
        pC->xEnc[z].encodeControl.pOutBufConfig = &pC->xOut[zO].outBufConfig;
        pC->xEnc[z].encodeStatus = *pP->z_pEncodeStatus[z];
        pC->xEnc[z].encodeControl.encActive = pC->xEnc[z].encodeStatus.select;
        pC->xEnc[z].volumeStatus = *pP->pVolumeStatus;
        pC->xEnc[z].encodeInStruct.pAudioFrame = pC->xStr[zS].pAudioFrame;
    }

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        pC->xOut[z].outBufStatus = *pP->pOutBufStatus;
    }

    TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: initialization phase - configuration complete.\n",
                   as+zMS, __FUNCTION__, __LINE__));
    return 0;
} //PAF_AST_initPhaseConfig

// -----------------------------------------------------------------------------
// AST Initialization Function - ACP Algorithm Instantiation
//
//   Name:      PAF_AST_initPhaseAcpAlg
//   Purpose:   Audio Stream Task Function for initialization of ACP by
//              instantiation of the algorithm.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on ACP Algorithm creation failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//

Int
PAF_AST_initPhaseAcpAlg (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */

    Int z;                              /* input/encode/stream/decode/output */
                                        /* counter                           */
    Int betaPrimeOffset;
    ACP_Handle acp;
    Int zMS = pC->masterStr;
    Int zS, zX;

    TRACE_TERSE((&TR_MOD, "\nAS%d: %s.%d: initialization phase - ACP Algorithm",
                   as+zMS, __FUNCTION__, __LINE__));

    ACP_MDS_init();

    if (! (acp = (ACP_Handle )ACP_MDS_create (NULL))) {
        TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: ACP algorithm instance creation  failed",
                     as+zMS, __FUNCTION__, __LINE__));
        return __LINE__;
    }
    pC->acp = acp;

    ((ALG_Handle) acp)->fxns->algControl ((ALG_Handle) acp,
                                          ACP_GETBETAPRIMEOFFSET, (IALG_Status *) &betaPrimeOffset);

    for (z=INPUT1; z < INPUTN; z++) {
        zS = z;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->inputsFromDecodes[zX] == z) {
                zS = pP->streamsFromDecodes[zX];
                break;
            }
        }
        acp->fxns->attach(acp, ACP_SERIES_STD,
                          STD_BETA_IB + betaPrimeOffset * (as-1+zS),
                          (IALG_Status *)&pC->xInp[z].inpBufStatus);
        /* Ignore errors, not reported. */
    }

    for (z=DECODE1; z < DECODEN; z++) {
        zS = pP->streamsFromDecodes[z];
        acp->fxns->attach(acp, ACP_SERIES_STD,
                          STD_BETA_DECODE + betaPrimeOffset * (as-1+zS),
                          (IALG_Status *)&pC->xDec[z].decodeStatus);
        /* Ignore errors, not reported. */
    }

    for (z=ENCODE1; z < ENCODEN; z++) {
        zS = pP->streamsFromEncodes[z];
        acp->fxns->attach(acp, ACP_SERIES_STD,
                          STD_BETA_ENCODE + betaPrimeOffset * (as-1+zS),
                          (IALG_Status *)&pC->xEnc[z].encodeStatus);
        acp->fxns->attach(acp, ACP_SERIES_STD,
                          STD_BETA_VOLUME + betaPrimeOffset * (as-1+zS),
                          (IALG_Status *)&pC->xEnc[z].volumeStatus);
        /* Ignore errors, not reported. */
    }

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        zS = z;
        for (zX = ENCODE1; zX < ENCODEN; zX++) {
            if (pP->outputsFromEncodes[zX] == z) {
                zS = pP->streamsFromEncodes[zX];
                break;
            }
        }
        acp->fxns->attach(acp, ACP_SERIES_STD,
                          STD_BETA_OB + betaPrimeOffset * (as-1+zS),
                          (IALG_Status *)&pC->xOut[z].outBufStatus);
        /* Ignore errors, not reported. */
    }

    TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: initialization phase - ACP Algorithm complete.\n",
                   as+zMS, __FUNCTION__, __LINE__));

    return 0;
} //PAF_AST_initPhaseAcpAlg

// -----------------------------------------------------------------------------
// AST Initialization Function - Common Memory and Algorithms
//
//   Name:      PAF_AST_initPhaseCommon
//   Purpose:   Audio Stream Task Function for initialization of data pointers
//              by allocation for common memory and by instantiation for
//              algorithms.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on PAF_ALG_alloc failure.
//              Source code line number on PAF_ALG_mallocMemory failure.
//              Source code line number on Decode Chain initialization failure.
//              Source code line number on ASP Chain initialization failure.
//              Source code line number on Encode Chain initialization failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//

#include <pafsio_ialg.h>

#define inpLinkInit pP->i_inpLinkInit
#define outLinkInit pP->i_outLinkInit

Int
PAF_AST_initPhaseCommon (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* stream counter */
    Int g;                              /* gear */
    ACP_Handle acp = pC->acp;
    PAF_IALG_Config pafAlgConfig;
    IALG_MemRec common[3][PAF_IALG_COMMON_MEMN+1];
   
    TRACE_TERSE((&TR_MOD, "\n%s.%d: initialization phase - Common Algorithms",
                   __FUNCTION__, __LINE__));

    //
    // Determine memory needs and instantiate algorithms across audio streams
    //

    TRACE_TERSE((&TR_MOD, "%s.%d: calling PAF_ALG_setup.",__FUNCTION__, __LINE__));
    PAF_ALG_setup (&pafAlgConfig, HEAP_INTERNAL, HEAP_EXTERNAL,HEAP_INTERNAL1,HEAP_CLEAR);
    // TRACE_TERSE((&TR_MOD, "%s: internal: 0x%x.  External %x.", __FUNCTION__, HEAP_INTERNAL, HEAP_EXTERNAL));
    // TRACE_TERSE((&TR_MOD, "%s: internal1: 0x%x.  clear %x.", __FUNCTION__, HEAP_INTERNAL1, 3));

    if(pP->fxns->headerPrint)
        pP->fxns->headerPrint();

    for (z=STREAM1; z < STREAMN; z++) {
        Int zD, zE, zX;

        TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: initialization phase - Common Algorithms",
                       as+z, __FUNCTION__, __LINE__));

        //
        // Determine common memory needs of Decode, ASP, and Encode Algorithms
        //

        PAF_ALG_init (common[z], lengthof (common[z]), COMMONSPACE);

        zD = -1;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->streamsFromDecodes[zX] == z) {
                zD = zX;
                break;
            }
        }

        zE = -1;
        for (zX = ENCODE1; zX < ENCODEN; zX++) {
            if (pP->streamsFromEncodes[zX] == z) {
                zE = zX;
                break;
            }
        }

        if (zD >= 0) {
            TRACE_TERSE((&TR_MOD, "%s.%d: calling PAF_ALG_ALLOC for decoder common[%d].",__FUNCTION__, __LINE__, z));
            if (PAF_ALG_ALLOC (decLinkInit[zD-DECODE1], common[z])) {
                TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: PAF_ALG_alloc failed", as+z, __FUNCTION__, __LINE__));
                TRACE_TERSE((&TR_MOD, "Failed to alloc %d bytes from space %d", common[z]->size, common[z]->space));

                printf("\nAS%d: %s.%d: PAF_ALG_alloc failed.  See LOG_printf trace.\n", as+z, __FUNCTION__, __LINE__);
                SW_BREAKPOINT;
                return __LINE__;
            }
            // I don't think the base is useful in this trace.  Don't know how to see this address
            TRACE_TERSE((&TR_MOD, "alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, common[z]->base));
            if(pP->fxns->allocPrint)
                pP->fxns->allocPrint ((const PAF_ALG_AllocInit *)(decLinkInit[z-DECODE1]),sizeof (*(decLinkInit[z-DECODE1])), &pafAlgConfig);

        }

        TRACE_TERSE((&TR_MOD, "%s.%d: calling PAF_ALG_ALLOC for stream common[%d].",__FUNCTION__, __LINE__, z));
        if (PAF_ALG_ALLOC (aspLinkInit[z-STREAM1][0], common[z])) {
            TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: PAF_ALG_alloc failed", as+z, __FUNCTION__, __LINE__));
            TRACE_TERSE((&TR_MOD, "Failed to alloc %d bytes from space %d ", common[z]->size, common[z]->space));
            printf("\nAS%d: %s.%d: PAF_ALG_alloc failed.  See LOG_printf trace.\n", as+z, __FUNCTION__, __LINE__);
            SW_BREAKPOINT;
            return __LINE__;
        }
        // I don't think the base is useful in this trace.  Don't know how to see this address
        TRACE_TERSE((&TR_MOD, "alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, common[z]->base));
        if(pP->fxns->allocPrint)
            pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(aspLinkInit[z-STREAM1][0]), sizeof (*(aspLinkInit[z-STREAM1][0])), &pafAlgConfig);

        if (zE >= 0) {
            TRACE_TERSE((&TR_MOD, "%s.%d: calling PAF_ALG_ALLOC for encoder common[%d].",__FUNCTION__, __LINE__, z));
            if (PAF_ALG_ALLOC (encLinkInit[zE-ENCODE1], common[z])) {
                TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: PAF_ALG_alloc failed",
                             as+z, __FUNCTION__, __LINE__));
                TRACE_TERSE((&TR_MOD, "Failed to alloc %d bytes from space %d", common[z]->size, common[z]->space));
                SW_BREAKPOINT;
                return __LINE__;
            }
            // I don't think the base is useful in this trace.  Don't know how to see this address
            TRACE_TERSE((&TR_MOD, "alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, common[z]->base));
            if(pP->fxns->allocPrint)
                pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(encLinkInit[z-ENCODE1]), sizeof (*(encLinkInit[z-ENCODE1])), &pafAlgConfig);
        }

        //
        // Determine common memory needs of Logical IO drivers
        //

        // really need to loop over all inputs for this stream using the tables
        // inputsFromDecodes and streamsFromDecodes. But these don't exist for this
        // patch, and not needed for FS11, since there is only one input.
        if (INPUT1 <= z && z < INPUTN) {
            TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_initPhaseCommon.%d: alloc inpLinkInit common[%d]", as+z, __LINE__, z));
            if (PAF_ALG_ALLOC (inpLinkInit[z-INPUT1], common[z])) {
                TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: PAF_ALG_alloc failed", as+z, __FUNCTION__, __LINE__));
                TRACE_TERSE((&TR_MOD, "failed to alloc %d bytes from space %d", common[z]->size, common[z]->space));
                printf("\nAS%d: %s.%d: PAF_ALG_alloc failed.  See LOG_printf trace.\n", as+z, __FUNCTION__, __LINE__);
                SW_BREAKPOINT;
                return __LINE__;
            }
            TRACE_TERSE((&TR_MOD, "alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, common[z]->base));
            if(pP->fxns->allocPrint)
                pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(inpLinkInit[z-INPUT1]), sizeof (*(inpLinkInit[z-INPUT1])), &pafAlgConfig);
        }

        if (OUTPUT1 <= z && z < OUTPUTN) {
            TRACE_TERSE((&TR_MOD, "%s.%d: calling PAF_ALG_ALLOC outLinkInit common[%d].",__FUNCTION__, __LINE__, z));
            if (PAF_ALG_ALLOC (outLinkInit[z-OUTPUT1], common[z])) {
                TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: PAF_ALG_alloc failed", as+z, __FUNCTION__, __LINE__));
                TRACE_TERSE((&TR_MOD, "Failed to alloc %d bytes from space %d", common[z]->size, common[z]->space));
                printf("AS%d: %s.%d: PAF_ALG_alloc failed", as+z, __FUNCTION__, __LINE__);
                SW_BREAKPOINT;
                return __LINE__;
            }
            TRACE_TERSE((&TR_MOD, "alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, common[z]->base));
            if(pP->fxns->allocPrint)
                pP->fxns->allocPrint((const PAF_ALG_AllocInit *)(outLinkInit[z-INPUT1]), sizeof (*(outLinkInit[z-INPUT1])), &pafAlgConfig);
        }
    }
    {
    	 // Changes made to share scratch between zones
    	    // Assume maximum 3 zones and scratch common memory is at offset 0;
   int max=0;
    for(z=STREAM1; z < STREAMN; z++)
    {
    	if(max<common[z][0].size)
    		max=common[z][0].size;
    }
    common[STREAM1][0].size=max;
    for(z=STREAM1+1; z < STREAMN; z++)
    	common[z][0].size=0;
    }
        //
        // Provide common memory needs of Decode, ASP, and Encode Algorithms
        //

    for (z=STREAM1; z < STREAMN; z++) {
            Int zD, zE, zX;
            zD = -1;
                   for (zX = DECODE1; zX < DECODEN; zX++) {
                       if (pP->streamsFromDecodes[zX] == z) {
                           zD = zX;
                           break;
                       }
                   }

                   zE = -1;
                   for (zX = ENCODE1; zX < ENCODEN; zX++) {
                       if (pP->streamsFromEncodes[zX] == z) {
                           zE = zX;
                           break;
                       }
                   }

        TRACE_TERSE((&TR_MOD, "%s.%d: calling PAF_ALG_mallocMemory for common space.",__FUNCTION__, __LINE__));
        if (PAF_ALG_mallocMemory (common[z], &pafAlgConfig)) {
            TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: PAF_ALG_mallocMemory failed", as+z, __FUNCTION__, __LINE__));
            TRACE_TERSE((&TR_MOD, "AS%d: z: %d.  Size 0x%x", as+z, z, common[z][0].size));
            printf("\nAS%d: %s.%d: PAF_ALG_mallocMemory of %d bytes failed.  See LOG_printf trace.\n", as+z, __FUNCTION__, __LINE__, common[z][0].size);
            SW_BREAKPOINT;
            return __LINE__;
        }
        TRACE_TERSE((&TR_MOD, "alloced %d bytes from space %d at 0x%x", common[z]->size, common[z]->space, common[z]->base));
        // share zone0 scratch with all zones 
        common[z][0].base=common[0][0].base;
        if(pP->fxns->commonPrint)
                pP->fxns->commonPrint (common[z], &pafAlgConfig);

        //
        // Instantiate Decode, ASP, and Encode Algorithms
        //

        if (zD >= 0) {
        	PAF_ASP_Chain *chain;
            TRACE_TERSE((&TR_MOD, "%s.%d: calling PAF_ASP_chainInit for decode.",__FUNCTION__, __LINE__));
            chain =
                PAF_ASP_chainInit (&pC->xDec[zD].decChainData, pP->pChainFxns,
                                   HEAP_INTERNAL, as+z, acp, &trace,
                                   decLinkInit[zD-DECODE1], NULL, common[z], &pafAlgConfig);
            if (! chain) {
                TRACE_TERSE((&TR_MOD, "AS%d: Decode chain initialization failed", as+z));
                return __LINE__;
            }
        }

        pC->xStr[z].aspChain[0] = NULL;
        for (g=0; g < GEARS; g++) {
        	PAF_ASP_Chain *chain;
            TRACE_TERSE((&TR_MOD, "%s.%d: calling PAF_ASP_chainInit for ASPs.",__FUNCTION__, __LINE__));
            chain =
                PAF_ASP_chainInit (&pC->xStr[z].aspChainData[g], pP->pChainFxns,
                                   HEAP_INTERNAL, as+z, acp, &trace,
                                   aspLinkInit[z-STREAM1][g], pC->xStr[z].aspChain[0], common[z], &pafAlgConfig);
            if (! chain) {
                TRACE_TERSE((&TR_MOD, "AS%d: ASP chain %d initialization failed", as+z, g));
                return __LINE__;
            }
            else
                pC->xStr[z].aspChain[g] = chain;
        }

        if (zE >= 0) {
        	PAF_ASP_Chain *chain;
            TRACE_TERSE((&TR_MOD, "%s.%d: calling PAF_ASP_chainInit for encode.",__FUNCTION__, __LINE__));
            chain =
                PAF_ASP_chainInit (&pC->xEnc[zE].encChainData, pP->pChainFxns,
                                   HEAP_INTERNAL, as+z, acp, &trace,
                                   encLinkInit[zE-ENCODE1], NULL, common[z], &pafAlgConfig);
            if (! chain) {
                TRACE_TERSE((&TR_MOD, "AS%d: Encode chain initialization failed", as+z));
                return __LINE__;
            }
        }

        //
        // Allocate non-common memories for Logical IO drivers
        //    Since these structures are used at run-time we allocate from external memory

       if (INPUT1 <= z && z < INPUTN) {
            PAF_ASP_Chain *chain;
            TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_initPhaseCommon.%d: non-common input chain init for %d",
                           as+z, __LINE__, z));
            chain = PAF_ASP_chainInit (&pC->xInp[z].inpChainData, pP->pChainFxns,
                    HEAP_EXTERNAL, as+z, acp, &trace,
                    inpLinkInit[z-INPUT1], NULL, common[z], &pafAlgConfig);
            if (! chain) {
                TRACE_TERSE((&TR_MOD, "AS%d: Input chain initialization failed", as+z));
                return __LINE__;
            }
        }

        if (OUTPUT1 <= z && z < OUTPUTN) {
        	PAF_ASP_Chain *chain;
            TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_initPhaseCommon.%d: non-common output chain init for %d",
                           as+z, __LINE__, z));
            chain = PAF_ASP_chainInit (&pC->xOut[z].outChainData, pP->pChainFxns,
                    HEAP_EXTERNAL, as+z, acp, &trace,
                    outLinkInit[z-OUTPUT1], NULL, common[z], &pafAlgConfig);
            if (! chain) {
                TRACE_TERSE((&TR_MOD, "AS%d: Output chain initialization failed", as+z));
                return __LINE__;
            }
        }
    }
    TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_initPhaseCommon.%d: Returning complete.\n",as+z, __LINE__));

    return 0;
} //PAF_AST_initPhaseCommon




// -----------------------------------------------------------------------------
// AST Initialization Function - Algorithm Keys
//
//   Name:      PAF_AST_initPhaseAlgKey
//   Purpose:   Audio Stream Task Function for initialization of data values
//              from parameters for Algorithm Keys.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//


// .............................................................................

Int
PAF_AST_initPhaseAlgKey (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/encode counter */
    Int s;                              /* key number */
    PAF_ASP_Link *that;

#if ((PAF_DEVICE&0xFF000000) == 0xD8000000)
#else
    volatile Uint32 ROM_revision = 0x0000000C;     /* ROM revision address */
#endif
    (void)as;  // clear warning.

    TRACE_VERBOSE((&TR_MOD, "AS%d: PAF_AST_initPhaseAlgKey.%d: initialization phase - Algorithm Keys",
                   as, __LINE__));

    for (z=DECODE1; z < DECODEN; z++) {
        for (s=0; s < pP->pDecAlgKey->length; s++) {
            if (pP->pDecAlgKey->code[s].full != 0
                && (that = PAF_ASP_chainFind (&pC->xDec[z].decChainData, pP->pDecAlgKey->code[s]))) {

                    pC->xDec[z].decAlg[s] = (ALG_Handle )that->alg;
                        /* Cast in interface, for now --Kurt */
            }
            else
                pC->xDec[z].decAlg[s] = NULL;
        }
    }

    

    for (z=ENCODE1; z < ENCODEN; z++) {
        for (s=0; s < pP->pEncAlgKey->length; s++) {
            if (pP->pEncAlgKey->code[s].full != 0
                && (that = PAF_ASP_chainFind (&pC->xEnc[z].encChainData, pP->pEncAlgKey->code[s])))
                pC->xEnc[z].encAlg[s] = (ALG_Handle )that->alg;
            /* Cast in interface, for now --Kurt */
            else
                pC->xEnc[z].encAlg[s] = NULL;
        }
    }

    return 0;
} //PAF_AST_initPhaseAlgKey

// -----------------------------------------------------------------------------
// AST Initialization Function - I/O Devices
//
//   Name:      PAF_AST_initPhaseDevice
//   Purpose:   Audio Stream Task Function for initialization of I/O Devices.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on device allocation failure.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * Memory allocation errors.
//

Int
PAF_AST_initPhaseDevice (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* input/output counter */
    PAF_SIO_IALG_Obj    *pObj;
    PAF_SIO_IALG_Config *pAlgConfig;
        PAF_IALG_Config pafAlgConfig;

    (void)as;

    TRACE_TERSE((&TR_MOD, "\nAS%d: %s.%d: initialization phase - I/O Devices",
                   as, __FUNCTION__, __LINE__));

    if(pP->fxns->bufMemPrint)
    {
        PAF_ALG_setup (&pafAlgConfig, HEAP_INTERNAL, HEAP_EXTERNAL,HEAP_INTERNAL1,HEAP_CLEAR);
    	TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: calling PAF_ALG_setup with clear at %d.\n", as, __FUNCTION__, __LINE__, HEAP_CLEAR));
    }

    for (z=INPUT1; z < INPUTN; z++) {
        PAF_InpBufConfig *pConfig = &pC->xInp[z].inpBufConfig;

        pObj = (PAF_SIO_IALG_Obj *) pC->xInp[z].inpChainData.head->alg;
        pAlgConfig = &pObj->config;

        pC->xInp[z].hRxSio = NULL;

        pConfig->base.pVoid       = pAlgConfig->pMemRec[0].base;
        pConfig->pntr.pVoid       = pAlgConfig->pMemRec[0].base;
        pConfig->head.pVoid       = pAlgConfig->pMemRec[0].base;
        pConfig->futureHead.pVoid = pAlgConfig->pMemRec[0].base;
        pConfig->allocation       = pAlgConfig->pMemRec[0].size;
        pConfig->sizeofElement    = 2;
        pConfig->precision        = 16;

        if(pP->fxns->bufMemPrint)
            pP->fxns->bufMemPrint(z,pAlgConfig->pMemRec[0].size,PAF_ALG_memSpace(&pafAlgConfig,pAlgConfig->pMemRec[0].space),0);

    }

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        PAF_OutBufConfig *pConfig = &pC->xOut[z].outBufConfig;

        pObj = (PAF_SIO_IALG_Obj *) pC->xOut[z].outChainData.head->alg;
        pAlgConfig = &pObj->config;

        pC->xOut[z].hTxSio = NULL;
        pConfig->base.pVoid     = pAlgConfig->pMemRec[0].base;
        pConfig->pntr.pVoid     = pAlgConfig->pMemRec[0].base;
        pConfig->head.pVoid     = pAlgConfig->pMemRec[0].base;
        pConfig->allocation     = pAlgConfig->pMemRec[0].size;
        pConfig->sizeofElement  = 3;
        pConfig->precision      = 24;
        if(pP->fxns->bufMemPrint)
            pP->fxns->bufMemPrint(z,pAlgConfig->pMemRec[0].size,PAF_ALG_memSpace(&pafAlgConfig,pAlgConfig->pMemRec[0].space),1);
    }
    TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: initialization phase - I/O Devices complete.\n",
                   as, __FUNCTION__, __LINE__));

    return 0;
} //PAF_AST_initPhaseDevice

// -----------------------------------------------------------------------------
// AST Initialization Function Helper - Initialization of Audio Frame
//
//   Name:      PAF_AST_initFrame0
//   Purpose:   Audio Stream Task Function for initialization of the Audio
//              Frame(s) by memory allocation and loading of data pointers
//              and values.
//   From:      AST Parameter Function -> decodeInfo
//   Uses:      See code.
//   States:    x
//   Return:    0 on success.
//              Source code line number on MEM_calloc failure.
//              Source code line number on unsupported option.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * Memory allocation errors.
//              * Unsupported option errors.
//

// MID 314
extern const char AFChanPtrMap[PAF_MAXNUMCHAN+1][PAF_MAXNUMCHAN];
extern PAF_ChannelConfigurationMaskTable PAF_ASP_stdCCMT_patch;

Int
PAF_AST_initFrame0 (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int z)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int ch;
    Int aLen;
    Int aLen_int=0,aLen_ext=0;
    Int aSize = sizeof(PAF_AudioData);
    Int aAlign = aSize < sizeof (int) ? sizeof (int) : aSize;
    Int maxFrameLength = pP->maxFramelength;
    Int zX;
    PAF_AudioData *aBuf;
     PAF_AudioData *aBuf_int,*aBuf_ext;
   XDAS_UInt8 *metadataBuf;
    char i;

    // Compute maximum framelength (needed for ARC support)
    maxFrameLength += PA_MODULO - maxFrameLength % PA_MODULO;
    aLen = numchan[z] * maxFrameLength;
	for (i=0; i < numchan[z]; i++)
	{
		if(pP->pAudioFrameBufStatus->space[i] == IALG_SARAM)
			aLen_int += maxFrameLength;
		else
			aLen_ext += maxFrameLength;
	}

    //
    // Initialize audio frame elements directly
    //

    pC->xStr[z].pAudioFrame->fxns = pP->pAudioFrameFunctions;
    pC->xStr[z].pAudioFrame->data.nChannels = PAF_MAXNUMCHAN; ///
///    pC->xStr[z].pAudioFrame->data.nChannels = PAF_MAXNUMCHAN_AF;
    pC->xStr[z].pAudioFrame->data.nSamples = FRAMELENGTH;
    pC->xStr[z].pAudioFrame->data.sample = pC->xStr[z].audioFrameChannelPointers;
    pC->xStr[z].pAudioFrame->data.samsiz = pC->xStr[z].audioFrameChannelSizes;
    pC->xStr[z].pAudioFrame->pChannelConfigurationMaskTable = &PAF_ASP_stdCCMT;

    //
    // Allocate memory for and initialize pointers to audio data buffers
    //
    //   The NUMCHANMASK is used to identify the channels for which data
    //   buffers can be allocated. Using this mask and switch statement
    //   rather than some other construct allows efficient code generation,
    //   providing just the code necessary (with significant savings).
    //
    if(pP->fxns->bufMemPrint)
	{
        pP->fxns->bufMemPrint(z, aLen_int*aSize, HEAP_FRMBUF, 2);
		pP->fxns->bufMemPrint(z, aLen_ext*aSize, HEAP_EXTERNAL, 2);
	}

    TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: MEM_calloc for audio buffers", as+z, __FUNCTION__, __LINE__));
    if (! (aBuf_int = (PAF_AudioData *)MEM_calloc (HEAP_FRMBUF, aLen_int*aSize, aAlign))) {
        TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: MEM_calloc failed", as+z, __FUNCTION__, __LINE__));
        TRACE_TERSE((&TR_MOD, "  maxFrameLength: %d.  aLen_int*aSize: %d", maxFrameLength, aLen_int*aSize));
        printf("\nAS%d: %s.%d: MEM_calloc of %d bytes failed.  See LOG_printf trace. \n", as+z, __FUNCTION__, __LINE__, aLen_int*aSize);
        SW_BREAKPOINT;
		return __LINE__;
    }
 	if (! (aBuf_ext = (PAF_AudioData *)MEM_calloc (HEAP_EXTERNAL, aLen_ext*aSize, aAlign))) {
        TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: MEM_calloc failed", as+z, __FUNCTION__, __LINE__));
        TRACE_TERSE((&TR_MOD, "  maxFrameLength: %d.  aLen_ext*aSize: %d", maxFrameLength, aLen_ext*aSize));
        printf("\nAS%d: %s.%d: MEM_calloc of %d bytes failed.  See LOG_printf trace. \n", as+z, __FUNCTION__, __LINE__, aLen_ext*aSize);
        SW_BREAKPOINT;
        return __LINE__;
    }
    TRACE_TERSE((&TR_MOD, "  maxFrameLength: %d.  aLen*aSize: %d.  aBuf: 0x%x", maxFrameLength, aLen*aSize, aBuf));

    TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: MEM_calloc for metadata buffers", as+z, __FUNCTION__, __LINE__));
    if (! (metadataBuf = (XDAS_UInt8 *)MEM_calloc (*(pP->pMetadataBufStatus->pSpace), pP->pMetadataBufStatus->bufSize*pP->pMetadataBufStatus->NumBuf, pP->pMetadataBufStatus->alignment))) {
        TRACE_TERSE((&TR_MOD, "AS%d: MEM_calloc failed at " __FILE__ ":%d", as+z, __LINE__));
        TRACE_TERSE((&TR_MOD, "  bufSize*NumBuf: %d", pP->pMetadataBufStatus->bufSize*pP->pMetadataBufStatus->NumBuf));
        printf("\nAS%d: %s.%d: MEM_calloc of %d bytes failed.  See LOG_printf trace. \n", as+z, __FUNCTION__, __LINE__, pP->pMetadataBufStatus->bufSize*pP->pMetadataBufStatus->NumBuf);
        SW_BREAKPOINT;
        return __LINE__;
    }

    {
        Int i;

#pragma UNROLL(1)
        for (i=0; i < PAF_MAXNUMCHAN_AF; i++)
            pC->xStr[z].audioFrameChannelPointers[i] = NULL;
    }

    // MID 314
    if((numchan[z] > PAF_MAXNUMCHAN) || (numchan[z] < 1)) {
        TRACE_TERSE((&TR_MOD, "AS%d: %s.%d: unsupported option",
                     as+z, __FUNCTION__, __LINE__));
        return __LINE__;
    }
    else {
		Int j = 0;
		Int k = 0;
        TRACE_TERSE((&TR_MOD, "PAF_AST_initFrame0.%d: AFChanPtrMap[%d][i]", __LINE__, numchan[z]));
        for(i=0;i<numchan[z];i++)
		{
            char chan = AFChanPtrMap[numchan[z]][i];
            if(chan != -1)
            {
				if(pP->pAudioFrameBufStatus->space[i] == IALG_SARAM)
				{
					pC->xStr[z].audioFrameChannelPointers[chan] = aBuf_int + maxFrameLength*(j+1) - FRAMELENGTH;
					j++;
				}
				else
				{		
					pC->xStr[z].audioFrameChannelPointers[chan] = aBuf_ext + maxFrameLength*(k+1) - FRAMELENGTH;
					k++;
				}	
                TRACE_TERSE((&TR_MOD, "PAF_AST_initFrame0: chan = %d = AFChanPtrMap[%d][%d].", chan, numchan[z], i));
                TRACE_TERSE((&TR_MOD, "PAF_AST_initFrame0.%d: audioFrameChannelPointers[%d]: 0x%x", __LINE__, chan, pC->xStr[z].audioFrameChannelPointers[chan]));
            }
        }
    }

    for (ch=PAF_LEFT; ch < PAF_MAXNUMCHAN_AF; ch++) {
        if (pC->xStr[z].audioFrameChannelPointers[ch])
            pC->xStr[z].origAudioFrameChannelPointers[ch] = pC->xStr[z].audioFrameChannelPointers[ch];
    }

    //
    // Initialize meta data elements
    //
    pC->xStr[z].pAudioFrame->pafBsMetadataUpdate = XDAS_FALSE;
    pC->xStr[z].pAudioFrame->numPrivateMetadata = 0;
    pC->xStr[z].pAudioFrame->bsMetadata_offset = 0;
    pC->xStr[z].pAudioFrame->bsMetadata_type = PAF_bsMetadata_channelData;
    pC->xStr[z].pAudioFrame->privateMetadataBufSize = pP->pMetadataBufStatus->bufSize;
    for(i=0;i<pP->pMetadataBufStatus->NumBuf;i++)
    {
        pC->xStr[z].pAudioFrame->pafPrivateMetadata[i].offset = 0;
        pC->xStr[z].pAudioFrame->pafPrivateMetadata[i].size = 0;
        pC->xStr[z].pAudioFrame->pafPrivateMetadata[i].pMdBuf = metadataBuf + pP->pMetadataBufStatus->bufSize*i;
    }

    //
    // Initialize decoder elements directly
    //

    for (zX = DECODE1; zX < DECODEN; zX++) {
        if (pP->streamsFromDecodes[zX] == z) {
#ifdef NOAUDIOSHARE
            pC->xDec[zX].decodeInStruct.audioShare.nSamples = 0;
            pC->xDec[zX].decodeInStruct.audioShare.sample = NULL;
#else /* NOAUDIOSHARE */
            pC->xDec[zX].decodeInStruct.audioShare.nSamples = aLen_int;
            pC->xDec[zX].decodeInStruct.audioShare.sample = aBuf_int;
#endif /* NOAUDIOSHARE */
        }
    }

    return 0;
} //PAF_AST_initFrame0

// -----------------------------------------------------------------------------
// AST Initialization Function Helper - Reinitialization of Audio Frame
// AST Decoding Function              - Reinitialization of Audio Frame
//
//   Name:      PAF_AST_initFrame1
//   Purpose:   Audio Stream Task Function for initialization or reinitiali-
//              zation of the Audio Frame(s) by loading of data values of a
//              time-varying nature.
//   From:      audioStream1Task or equivalent
//              AST Parameter Function -> decodeInfo
//              AST Parameter Function -> decodeDecode
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     None.
//

Int
PAF_AST_initFrame1 (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int z, Int apply)
{
    //
    // Reinitialize audio frame elements:
    //
    //   Channel Configurations during init             = Unknown
    //      "          "        during info or decode   = None
    //
    //   Sample Rate / Count    during init or info     = Unknown
    //     "     "   /   "      during decode           = unchanged
    //

    Int i;

    if (apply < 0) {
        pC->xStr[z].pAudioFrame->channelConfigurationRequest.legacy = PAF_CC_UNKNOWN;
        pC->xStr[z].pAudioFrame->channelConfigurationStream.legacy = PAF_CC_UNKNOWN;
    }
    else {
        pC->xStr[z].pAudioFrame->channelConfigurationRequest.legacy = PAF_CC_NONE;
        pC->xStr[z].pAudioFrame->channelConfigurationStream.legacy = PAF_CC_NONE;
    }

    if (apply < 1) {
        pC->xStr[z].pAudioFrame->sampleRate = PAF_SAMPLERATE_UNKNOWN;
        pC->xStr[z].pAudioFrame->sampleCount = 0;
    }

    return 0;
} //PAF_AST_initFrame1

// -----------------------------------------------------------------------------
// AST Processing Function - Pass-Through Processing
//
//   Name:      PAF_AST_passProcessing
//   Purpose:   Audio Stream Task Function for processing audio data as a
//              pass-through from the input driver to the output driver
//              for development and testing.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information on initialization.
//              * State information on processing.
//

#pragma CODE_SECTION(PAF_AST_passProcessing,".text:_PAF_AST_passProcessing")
/* Pass Processing is often omitted from builds to save memory, */
/* and CODE_SECTION/clink constructs facilitate this omission.  */

Int
PAF_AST_passProcessing (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int hack)
{
    Int z;                              /* input/output counter */
    Int errno = 0;                      /* error number */
    Int getVal;
    Int rxNumChan, txNumChan;
    Int first;
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;


    asm (" .clink"); /* See comment above regarding CODE_SECTION/clink. */

    TRACE_VERBOSE((&TR_MOD, "PAF_AST_passProcessing: initializing"));

    //
    // Determine that receive/transmit channels are compatible
    //

    // Can handle handle only master input
    for (z=INPUT1; z < INPUTN; z++) {
        if (z != zMI && pC->xInp[z].hRxSio)
            return (ASPERR_PASS + 0x01);
    }

    /* Number of receive/transmit channels */

    if (! pC->xInp[zMI].hRxSio)
        return (ASPERR_PASS + 0x11);
    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &rxNumChan))
        return (ASPERR_PASS + 0x12);
    if (rxNumChan > NUM_TX_CHAN(zMI))
        return (ASPERR_PASS + 0x13);

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (! pC->xOut[zMI].hTxSio)
            return (ASPERR_PASS + 0x10*(z+2) + 0x01);
        if (SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &txNumChan))
            return (ASPERR_PASS + 0x10*(z+2) + 0x02);
        if (txNumChan > NUM_TX_CHAN(zMI))
            return (ASPERR_PASS + 0x10*(z+2) + 0x03);
    }

    //
    // Set up receive/transmit
    //

    SIO_idle (pC->xInp[zMI].hRxSio);
    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if(SIO_idle (pC->xOut[z].hTxSio))
            return ASPERR_IDLE;
    }

    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_SET_SOURCESELECT, PAF_SOURCE_PCM))
        return (ASPERR_PASS + 0x14);

    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, FRAMELENGTH))
        return (ASPERR_PASS + 0x15);

    for (z=OUTPUT1; z < OUTPUTN; z++)
        pC->xOut[z].outBufConfig.lengthofFrame = FRAMELENGTH;

    if (SIO_issue (pC->xInp[zMI].hRxSio, &pC->xInp[zMI].inpBufConfig, sizeof (pC->xInp[zMI].inpBufConfig), PAF_SIO_REQUEST_SYNC))
        return ASPERR_PASS + 0x16;

    if (SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr )&pC->xInp[zMI].pInpBuf, NULL) != sizeof (PAF_InpBufConfig))
        return ASPERR_PASS + 0x17;

    //
    // Receive and transmit the data in single-frame buffers
    //

    first = 1;
    while (pC->xDec[zMD].decodeStatus.sourceSelect == PAF_SOURCE_PASS) {
        PAF_OutBufConfig *pOutBuf;
        PAF_InpBufConfig *pInpBuf;

        if (first) {
            first = 0;

            TRACE_VERBOSE((&TR_MOD, "PAF_AST_passProcessing: starting output"));

            for (z=OUTPUT1; z < OUTPUTN; z++) {
                getVal = SIO_issue (pC->xOut[z].hTxSio, &pC->xOut[z].outBufConfig, sizeof(pC->xOut[z].outBufConfig), 0);
                if (getVal > 0) {
                    errno = ASPERR_ISSUE;
                    break;
                }
                else if (getVal < 0) {
                    errno = -getVal;
                    break;
                }

                if (getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_UNMUTE, 0))
                    return (getVal & 0xff) | ASPERR_MUTE;
            }
            if (errno)
                break;

        }

        getVal = SIO_issue (pC->xInp[zMI].hRxSio, &pC->xInp[zMI].inpBufConfig, sizeof (pC->xInp[zMI].inpBufConfig), PAF_SIO_REQUEST_NEWFRAME);
        if (getVal > 0) {
            errno = ASPERR_ISSUE;
            break;
        }

        TRACE_VERBOSE((&TR_MOD, "PAF_AST_passProcessing: awaiting frame -- input size %d", rxNumChan * FRAMELENGTH));

        getVal = SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr) &pInpBuf, NULL);
        if (getVal < 0) {
            errno = -getVal;
            break;
        }

        for (z=OUTPUT1; z < OUTPUTN; z++) {
            getVal = SIO_reclaim (pC->xOut[z].hTxSio, (Ptr) &pOutBuf, NULL);
            if (getVal < 0) {
                errno = -getVal;
                break;
            }
        }
        if( errno )
            break;

        TRACE_VERBOSE((&TR_MOD, "PAF_AST_passProcessing: copying frame"));

        if (errno = pP->fxns->passProcessingCopy (pP, pQ, pC))
            break;

        for (z=OUTPUT1; z < OUTPUTN; z++) {
            getVal = SIO_issue (pC->xOut[z].hTxSio, &pC->xOut[z].outBufConfig, sizeof(pC->xOut[z].outBufConfig), 0);
            if (getVal > 0) {
                errno = ASPERR_ISSUE;
                break;
            }
            else if (getVal < 0) {
                errno = -getVal;
                break;
            }
        }
        if( errno )
            break;
    }

    //
    // Close down receive/transmit
    //

    TRACE_TERSE((&TR_MOD, "PAF_AST_passProcessing: finalizing"));

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_MUTE, 0)) {
            if (! errno)
                errno = (getVal & 0xff) | ASPERR_MUTE;
            /* convert to sensical errno */
        }
    }

    SIO_idle (pC->xInp[zMI].hRxSio);
    for (z=OUTPUT1; z < OUTPUTN; z++)
        SIO_idle (pC->xOut[z].hTxSio);

    return errno;

} //PAF_AST_passProcessing

// -----------------------------------------------------------------------------
// AST Processing Function Helper - Pass-Through Processing Patch Point
//
//   Name:      PAF_AST_passProcessingCopy
//   Purpose:   Pass-Through Processing Function for copying audio data
//              from the input buffer to the output buffer.
//   From:      AST Parameter Function -> passProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              x
//

#pragma CODE_SECTION(PAF_AST_passProcessingCopy,".text:_PAF_AST_passProcessingCopy")
/* Pass Processing is often omitted from builds to save memory, */
/* and CODE_SECTION/clink constructs facilitate this omission.  */

Int
PAF_AST_passProcessingCopy (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int z;                              /* output counter */
    Int errno;                          /* error number */
    Int i;
    Int rxNumChan, txNumChan;
    Int zMI = pP->zone.master;


    asm (" .clink"); /* See comment above regarding CODE_SECTION/clink. */

    // Copy data from input channels to output channels one of two ways:

    if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &rxNumChan))
        return (ASPERR_PASS + 0x12);

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_GET_NUMCHANNELS, (Arg) &txNumChan))
            return (ASPERR_PASS + 0x22);

        if( txNumChan <= rxNumChan ) {

            // Copy one to one, ignoring later rx channels as needed.

            for( i=0; i < txNumChan; i++ ) {
                errno = pP->fxns->copy( i, &pC->xInp[zMI].inpBufConfig, i, &pC->xOut[z].outBufConfig );
                if( errno )
                    return errno;
            }
        }
        else {

            // Copy one to many, repeating earlier rx channels as needed.

            Int from, to;

            from = 0;
            to   = 0;
            while( to < txNumChan ) {
                errno = pP->fxns->copy( from, &pC->xInp[zMI].inpBufConfig, to, &pC->xOut[z].outBufConfig );
                if( errno )
                    return errno;

                from++;
                to++;
                if( from == rxNumChan )
                    from = 0;
            }
        }
    }

    return 0;
} //PAF_AST_passProcessingCopy

// -----------------------------------------------------------------------------
// AST Processing Function - Auto Processing
//
//   Name:      PAF_AST_autoProcessing
//   Purpose:   Audio Stream Task Function for processing audio data to
//              determine the input type without output.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information on initialization.
//

#define DECSIOMAP(X)                                                \
    pP->pDecSioMap->map[(X) >= pP->pDecSioMap->length ? 0 : (X)]

Int
PAF_AST_autoProcessing (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int inputTypeSelect, ALG_Handle pcmAlgMaster)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int errno = 0;                      /* error number */
    Int nbytes;
    Int frameLength;
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;
    Int zMS = pC->masterStr;

    TRACE_VERBOSE((&TR_MOD, "AS%d: PAF_AST_autoProcessing.%d",
                   as+zMS, __LINE__));

    if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio,
                          PAF_SIO_CONTROL_SET_SOURCESELECT,
                          DECSIOMAP (pC->xDec[zMD].decodeStatus.sourceSelect)))
    {
        TRACE_VERBOSE((&TR_MOD, "AS%d: PAF_AST_autoProcessing.%d: source select returns 0x%x",
                       as+zMS, __LINE__, errno));
        return errno;
    }
    frameLength = pP->fxns->computeFrameLength (pcmAlgMaster, FRAMELENGTH,
                                                pC->xDec[zMD].decodeStatus.bufferRatio);

    if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio,
                          PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, frameLength))
    {
        TRACE_VERBOSE((&TR_MOD, "PAF_AST_autoProcessing.%d: SET_PCMFRAMELENGTH returns 0x%x, returning ASPERR_AUTO_LENGTH, 0x%x",
                       __LINE__, errno, ASPERR_AUTO_LENGTH));
        return ASPERR_AUTO_LENGTH;
    }

    if (errno = SIO_issue (pC->xInp[zMI].hRxSio,
                           &pC->xInp[zMI].inpBufConfig, sizeof (pC->xInp[zMI].inpBufConfig),
                           PAF_SIO_REQUEST_SYNC))
    {
        TRACE_VERBOSE((&TR_MOD, "PAF_AST_autoProcessing.%d: REQUEST_SYNC returns 0x%x, returning ASPERR_ISSUE, 0x%x",
                       __LINE__, errno, ASPERR_ISSUE));
        return ASPERR_ISSUE;
    }

    TRACE_VERBOSE((&TR_MOD, "AS%d: PAF_AST_autoProcessing.%d: awaiting sync",
                   as+zMS, __LINE__));

    // all of the sync scan work is done in this call. If the error returned
    // is DIBERR_SYNC then that just means we didn't find a sync, not a real I/O
    // error so we mask it off.
    nbytes = SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr )&pC->xInp[zMI].pInpBuf, NULL);
    if (nbytes == -DIBERR_SYNC)
    {
        TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_autoProcessing.%d: SIO_reclaim returned 0x%x, ignoring",
                     as+zMS, __LINE__, nbytes));
        return 0;
    }
    if (nbytes != sizeof (PAF_InpBufConfig))
    {
        TRACE_TERSE((&TR_MOD, "PAF_AST_autoProcessing. SIO_reclaim returned %d, not %d, returning ASPERR_RECLAIM (0x%x)",
                     nbytes, sizeof (PAF_InpBufConfig), ASPERR_RECLAIM));
        return ASPERR_RECLAIM;
    }
    if (errno)
    {
        TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_autoProcessing.%d: returning errno 0x%x",
                     as+zMS, __LINE__, errno));
    }
    return errno;
} //PAF_AST_autoProcessing

// -----------------------------------------------------------------------------
// AST Processing Function - Decode Processing
//
//   Name:      PAF_AST_decodeProcessing
//   Purpose:   Audio Stream Task Function for processing audio data for
//              output on a continuous basis, including detection of the
//              input type.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information on initialization (via children).
//              * State information on processing (via children).
//              * Decode warnings.
//

// When "writeDECModeContinuous" is used for zMI input/decode:
// PAF_AST_decodeProcessing() loop may be (is designed to be) exited:
// (a) if "writeDECCommandRestart" is used
//    (or "writeDECCommandAbort", but this performs no cleanup whatsoever, and so its use is discouraged)
// (b) if "writeDECSourceSelectNone" is used
// [ the latter events lead to QUIT state, simply for exiting (errme = errno = ASPERR_QUIT)
// (c) if an error occurs in
//     INIT
//     CONT ("subsequent block state", which "Establish[es] secondary timing")
//         -> PAF_AST_decodeCont(): "Await slave inputs"
//     STREAM (errno |= PAF_COMPONENT_ASP)
//     ENCODE (errno |= PAF_COMPONENT_ENCODE)
// [ the latter errors lead to "switch_break:"
//         -> PAF_AST_decodeComplete(), which always returns 0 (no error) ]
//
// [ Notably, in FINAL ("frame-finalization state")
//         -> PAF_AST_decodeFinalTest() is *not* called,
//   and so any other (asynchronous) changes in pC->xDec[zMD].decodeStatus.sourceSelect are ignored. ]
// [ For completeness, note also: "default" state, internal check (errme = errno = ASPERR_UNKNOWNSTATE) ]
//
// States in which error can't occur:
//     AGAIN ("subsequent initial state")
//
// States in which (some) errors must be handled:
//     INFO1 ("first frame state")
//         -> PAF_AST_decodeInfo(): pass on ASPERR_INFO_RATECHANGE, ASPERR_INFO_PROGRAM ("bad" internal error)
//            -> *DONE* must "catch" ASPERR_RECLAIM from SIO_reclaim (pC->xInp[zMI].hRxSio) -- note zMI only **
//               ?*? but what about ASPERR_RESYNC from same call ?*?
//            -> *for now, at least, pass on error from pP->fxns->updateInputStatus ()*
//            -> *DONE* must "catch" error from (zMI) dec->fxns->info() **
//         -> PAF_AST_decodeInfo1(): pass on any errors which occur here:
//            - pP->fxns->streamChainFunction (... PAF_ASP_CHAINFRAMEFXNS_RESET)
//            - enc->fxns->info()
//            - pP->fxns->setCheckRateX()
//            - pP->fxns->startOutput()
//            - "Start slave inputs if necessary"
//     INFO2 ("subsequent frame state")
//         -> PAF_AST_decodeInfo(): (see above)
//         -> PAF_AST_decodeInfo2(): pass on any errors which occur here:
//            - pP->fxns->setCheckRateX()
//     TIME ("timing state")
//         -> PAF_AST_decodeTime(): "Special timing consideations for AC-3"
//         -> performs SIO_issue (... PAF_SIO_REQUEST_FULLFRAME) & SIO_reclaim() *for zMI only*
//         -> now, DIB_issue [PAF_SIO_REQUEST_FULLFRAME] would only return SYS_EINVAL for "bad" internal error
//            (*OK* don't try to recover from this*)
//         -> much more likely would be SIO_reclaim() error (ASPERR_RECLAIM)
//         -> *DONE* must "catch" (just) ASPERR_RECLAIM error -- note zMI only,
//            possibly in PAF_AST_decodeProcessing() itself **
//     DECODE ("decode state")
//         -> PAF_AST_decodeDecode(): pass on error from
//            - PAF_SIO_CONTROL_GET_NUM_REMAINING ("bad" internal error)
//            - dec->fxns->reset()
//            - PAF_SIO_CONTROL_SET_PCMFRAMELENGTH
//         -> *DONE* must catch error from (zMI) dec->fxns->decode()
//         -> *?* must catch ASPERR_ISSUE from (zMI) SIO_issue()

Int
PAF_AST_decodeProcessing (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlgMaster)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode counter */
    Int frame, block;
    Int errno;                          /* error number */
    Int getVal;
    enum { INIT, INFO1, AGAIN, INFO2, CONT, DECODE, STREAM, ENCODE, FINAL, QUIT } state;
    ALG_Handle alg[DECODEN_MAX];
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;
    Int zMS = pC->masterStr;
    Int resyncLoopCount, remainingSlaves;


    if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS) {

        if (errno = SIO_idle (pC->xInp[zMI].hRxSio))
        {
            TRACE_TERSE((&TR_MOD, "as1-f2.%d continuous, master AS%d: returning errno 0x%x",
                                            __LINE__, zMI, errno));
            return errno;
        }

        // indicates (primary) input not running
        pC->xDec[zMD].decodeStatus.sourceDecode = PAF_SOURCE_NONE;

        // will be changed after next reclaim, to PAF_SOURCE_UNKNOWN or other
        pC->xDec[zMD].decodeStatus.sourceProgram = PAF_SOURCE_NONE;

        // in support of PAF_SIO_REQUEST_AUTO
        decAlgMaster = pC->xDec[zMD].decAlg[PAF_SOURCE_PCM];
    }

    if (! decAlgMaster)
    {
        TRACE_TERSE((&TR_MOD, "as1-f2.%d !decAlgMaster returning ASPERR_ALGORITHM (0x%x)",
                                                    __LINE__, ASPERR_ALGORITHM));
        return ASPERR_ALGORITHM;
    }

    for (z=DECODE1; z < DECODEN; z++)
        alg[z] = pC->xDec[z].decAlg[PAF_SOURCE_PCM];
    alg[zMD] = decAlgMaster;

    //
    // Receive, process, and transmit the data in single-frame buffers
    //

    state = INIT;
    errno = 0; /* error number */

    TRACE_TERSE((&TR_MOD, "as1-f2.%d, PAF_AST_decodeProcessing.  sourceSelect is %d", __LINE__, pC->xDec[zMD].decodeStatus.sourceSelect));

    for (;;) {

    	// void dp_traceData(int *inBuf, int stride, int count);
    	// void dp_tracePAF_Data(float *lBuf, float *rBuf, int count);

        if (pC->xDec[zMD].decodeStatus.sourceSelect == PAF_SOURCE_NONE)
        {
            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: sourceSelect == PAF_SOURCE_NONE", __LINE__));
            state = QUIT;
        }

        // Process commands (decode)
        // dp_traceData((int*)pC->xInp->pInpBuf->base.pLgInt, 12, 256);

        if (getVal = pP->fxns->decodeCommand (pP, pQ, pC)) {
            if (state != INIT)   // no need to restart/abort if not yet started
            {
                if (getVal == ASPERR_QUIT)
                {
                    state = QUIT;
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: state = QUIT", __LINE__));
                }
                else if (getVal == ASPERR_ABORT)
                {
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: return getVal", __LINE__));
                    return getVal;
                }
                else
                    /* ignore */;
            }
            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: state == INIT", __LINE__));
        }

        // Process commands (encode)
        if (getVal = pP->fxns->encodeCommand (pP, pQ, pC)) {
            /* ignore */;
        }

        TRACE_TIME((&TIME_MOD,         "... + %d = %d ->", dtime(), TSK_time()));
        TRACE_TIME((&TIME_MOD,         "                 state = %s", stateName[state]));

        // Process state (decode)

        switch (state) {

            case INIT: // first initial state

                // reset audio frame pointers to original values
                // (may be needed if error occured)
                for (z=STREAM1; z < STREAMN; z++) {
                    int ch;
                    for (ch=PAF_LEFT; ch < PAF_MAXNUMCHAN_AF; ch++) {
                        if (pC->xStr[z].audioFrameChannelPointers[ch])
                            pC->xStr[z].audioFrameChannelPointers[ch] = pC->xStr[z].origAudioFrameChannelPointers[ch];
                    }

                    // (MID 1933) reset nChannels -- temp. changed for PCE2
///                    pC->xStr[z].pAudioFrame->data.nChannels = PAF_MAXNUMCHAN_AF;
                }
				
				// Initialize meta data elements

				{
					int i;

					for (z=STREAM1; z < STREAMN; z++) {
						pC->xStr[z].pAudioFrame->pafBsMetadataUpdate = XDAS_FALSE;
						pC->xStr[z].pAudioFrame->numPrivateMetadata = 0;
						pC->xStr[z].pAudioFrame->bsMetadata_offset = 0;
						pC->xStr[z].pAudioFrame->bsMetadata_type = PAF_bsMetadata_channelData;

						for(i=0;i<pP->pMetadataBufStatus->NumBuf;i++)
						{
							pC->xStr[z].pAudioFrame->pafPrivateMetadata[i].offset = 0;
							pC->xStr[z].pAudioFrame->pafPrivateMetadata[i].size = 0;
						}
					}
				}
                if (errno = pP->fxns->decodeInit (pP, pQ, pC, alg))
                    break;

                // initialize value (just in case)
                resyncLoopCount = 0;

                frame = 0;
                block = 0;
                state = INFO1;
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: state: INIT->INFO", __LINE__));
                continue;

            case INFO1: // first frame state

                // Establish primary timing
                if (errno = pP->fxns->decodeInfo (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "as1-f2.%d: INFO1: errno 0x%x after decodeInfo, primary timing", __LINE__, errno));
                    break;
                }

                // Don't start output untill major access unit is found.
                if( (pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_THD ||
                        pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_DXP ||
                        pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_DTSHD) &&
                                (pC->xStr[zMS].pAudioFrame->sampleRate == PAF_SAMPLERATE_UNKNOWN)) {
                    int z;
                    for (z=DECODE1; z < DECODEN; z++) {
                        Int zI = pP->inputsFromDecodes[z];
                        if (pC->xInp[zI].hRxSio && pC->xDec[z].decodeStatus.mode)
                        {
                            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: INFO1, SIO_issue", __LINE__));
                            if (SIO_issue (pC->xInp[zI].hRxSio, &pC->xInp[zI].inpBufConfig,
                            sizeof (pC->xInp[zI].inpBufConfig), PAF_SIO_REQUEST_NEWFRAME))
                            {
                                TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: INFO1, return (ASPERR_ISSUE)", __LINE__));
                                return (ASPERR_ISSUE);
                            }
                        }
                    }
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: INFO1: frame %d, not major access unit", __LINE__, frame));
                    frame++;
                    state = INFO1;
                    continue;
                }
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: INFO1: frame %d, major access unit found", __LINE__, frame));

                // Establish secondary timing
                if (errno = pP->fxns->decodeInfo1 (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: INFO1, errno 0x%x.  break after decodeInfo1", __LINE__, errno));
                    break;
                }

                state = DECODE;
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: state: INFO1->DECODE", __LINE__));
                continue;

            case AGAIN: // subsequent initial state

                block = 0;
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: state: AGAIN->INFO2", __LINE__));
                state = INFO2;
                continue;

            case INFO2: // subsequent frame state

                // Establish primary timing
                if (errno = pP->fxns->decodeInfo (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: INFO2 break on decodeInfo. errno 0x%x", __LINE__, errno));
                    break;
                }

                if (errno = pP->fxns->decodeInfo2 (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing. %d: INFO2 break on decodeInfo2. errno 0x%x", __LINE__, errno));
                    break;
                }

                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: INFO2->CONT", __LINE__));
                state = CONT;
                continue;

            case CONT: // subsequent block state

                remainingSlaves = 0x7FFFFFFF;

                if (pC->xInp[zMI].hRxSio)
                {   
                    if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                        // primary input not currently running
                        pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_NONE)
                    {
                        TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: CONT", __LINE__));

                        for (z=DECODE1; z < DECODEN; z++)
                        {
                            Int remaining;
                            Int zI = pP->inputsFromDecodes[z];
                            if (z == zMD
                                || ! pC->xInp[zI].hRxSio
                                || ! pC->xDec[z].decodeStatus.mode)
                            {
                                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: CONT: continue", __LINE__));
                                continue;
                            }

                            if (errno = SIO_ctrl (pC->xInp[zI].hRxSio, PAF_SIO_CONTROL_GET_NUM_REMAINING, (Arg )&remaining))
                            {
                                errno |= PAF_COMPONENT_IO;
                                {
                                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: CONT: break", __LINE__));
                                    break;
                                }
                            }

                            if (remaining < remainingSlaves) // new minimum?
                                remainingSlaves = remaining;
                        }

                        remainingSlaves /= 2;  // adjust for two channels
                    }
                }   

                // Establish secondary timing
                if (errno = pP->fxns->decodeCont (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: decodeProcessing.%d: state: CONT: decodeCont returns errno 0x%x", __LINE__, errno));
                    break;
                }

                if (pC->xInp[zMI].hRxSio)
                {
                    if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                        // primary input not currently running
                        pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_NONE) {

                        TRACE_TIME((&TIME_MOD, "as1-f2: primary input not yet restarted -- remainingSlaves = %d, resyncLoopCount = %d", 
                                     remainingSlaves, resyncLoopCount));

                        // if remainingSlaves is (significantly) negative,
                        // need to wait until *next* block to re-sync timing
                        if (remainingSlaves >= -50)
                        {
                            TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: CONT", __LINE__));
                            resyncLoopCount = 0;

                            // indicates primary input is (about to be) re-started
                            pC->xDec[zMD].decodeStatus.sourceDecode = PAF_SOURCE_UNKNOWN;

                            TRACE_TIME((&TIME_MOD, "as1-f2: restarting (primary) input"));
                            if (errno = SIO_issue (pC->xInp[zMI].hRxSio,
                                                           &pC->xInp[zMI].inpBufConfig,
                                                           sizeof (pC->xInp[zMI].inpBufConfig),
                                                           PAF_SIO_REQUEST_AUTO))
                            {
                                TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: CONT.  SIO_issue err 0x%x", __LINE__, errno));
                                errno |= PAF_COMPONENT_IO;
                                break;
                            }
                        }
                        else if (++resyncLoopCount > 2)
                        {
                            TRACE_TIME((&TIME_MOD, "as1-f2: FAILED to restart (primary) input"));
                            errno = ASPERR_UNKNOWNSTATE;
                            errno |= PAF_COMPONENT_IO;
                            break;
                        }

                    } // DEC_MODE_CONTINUOUS && PAF_SOURCE_NONE
                }

                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: CONT->DECODE", __LINE__));
                state = DECODE;
                continue;

            case DECODE: // decode state
                if (errno = pP->fxns->decodeDecode (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: CONT.  decodeDecode err 0x%x", __LINE__, errno));
                    break;
                }
#ifdef HSE
                // Hack for wma decoder to work. Needs to be discussed
                if( pC->xDec[0].decodeInStruct.pAudioFrame->sampleCount == 0)
                {
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: DECODE->AGAIN", __LINE__));
                    state = AGAIN;
                    continue;
                }
#endif
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: DECODE->STREAM", __LINE__));
                state = STREAM;
                continue;

            case STREAM: // stream state
                if (errno = pP->fxns->decodeStream (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: STREAM.  decodeStream err 0x%x", __LINE__, errno));
                    break;
                }
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: STREAM->ENCODE", __LINE__));
                state = ENCODE;
                continue;

            case ENCODE: // encode state
                if (errno = pP->fxns->decodeEncode (pP, pQ, pC, alg, frame, block))
                {
                    TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: ENCODE.  decodeEncode err 0x%x", __LINE__, errno));
                    break;
                }
                block++;

                // Check result to determine next state

                for (z=DECODE1; z < DECODEN; z++) {
                    if (pC->xDec[z].decodeStatus.mode) {
                        if (pC->xDec[z].decodeOutStruct.errorFlag)
                            TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing: %s: AS%d: decode warning (0x%02x)", 
                                          as+z, pC->xDec[z].decodeOutStruct.errorFlag));
                    }
                }

                // reset audio frame pointers (may have been adjusted by ARC or the like)
                for (z=STREAM1; z < STREAMN; z++) {
                    int ch;
                    for (ch=PAF_LEFT; ch < PAF_MAXNUMCHAN_AF; ch++) {
                        if (pC->xStr[z].audioFrameChannelPointers[ch])
                            pC->xStr[z].audioFrameChannelPointers[ch] = pC->xStr[z].origAudioFrameChannelPointers[ch];
                    }

                    // (MID 1933) reset nChannels -- temp. changed for PCE2
///                    pC->xStr[z].pAudioFrame->data.nChannels = PAF_MAXNUMCHAN_AF;
                }

                state = pC->xDec[zMD].decodeOutStruct.outputFlag & 1 ? FINAL : CONT;
                if (state == FINAL)
                {
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: ENCODE->FINAL", __LINE__));
                }
                else
                {
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: ENCODE->CONT", __LINE__));
                }

                // we've completed a frame, give someone else a chance to run.
                // TSK_yield();  
                // part of a time slicing experiement.  Performance was not great.

                continue;

            case FINAL: // frame-finalization state

                // Check for final frame, and if indicated:
                // - Update audio flag to cause output buffer flush rather than
                //   the default truncate in "decode complete" processing.
                // - Exit state machine to "decode complete" processing.

                if (!(pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS))
                {
                    if (pP->fxns->decodeFinalTest (pP, pQ, pC, alg, frame, block)) {
                        for (z=OUTPUT1; z < OUTPUTN; z++)
                        {
                            if ((pC->xOut[z].outBufStatus.audio & 0x0f) == PAF_OB_AUDIO_SOUND)
                            {
                                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: FINAL: SOUND -> QUIET", __LINE__));
                                pC->xOut[z].outBufStatus.audio++; // SOUND -> QUIET
                            }
                        }
                        break;
                    }
                }

                frame++;
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: FINAL->AGAIN", __LINE__));
                state = AGAIN;
                continue;

            case QUIT: // exit state

                // Quit:
                // - Set error number registers.
                // - Exit state machine to "decode complete" processing.

                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: QUIT", __LINE__));
                errno = ASPERR_QUIT;
                break;

            default: // unknown state

                // Unknown:
                // - Set error number registers.
                // - Exit state machine to "decode complete" processing.

                TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeProcessing.%d: state: unknown, 0x%x", __LINE__, state));
                errno = ASPERR_UNKNOWNSTATE;
                break;

        }  // End of switch (state).

        TRACE_VERBOSE((&TR_MOD, "as1-f2: Calling decode complete"));
        if (pP->fxns->decodeComplete (pP, pQ, pC, alg, frame, block))
            /* ignored? */;

        TRACE_TIME((&TIME_MOD, "as1-f2: ... + %d = ?? (final? %d)", dtime(), state == FINAL));

        return errno;
    }  // End of for (;;) to Receive, process, and transmit the data.
} //PAF_AST_decodeProcesing

// -----------------------------------------------------------------------------
// AST Decoding Function - Decode Command Processing
//
//   Name:      PAF_AST_decodeCommand
//   Purpose:   Decoding Function for processing Decode Commands.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * Command execution.
//

Int
PAF_AST_decodeCommand (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode counter */
    Int zS;


    for (z=DECODE1; z < DECODEN; z++) {
        zS = pP->streamsFromDecodes[z];
        if (! (pC->xDec[z].decodeStatus.command2 & 0x80)) {
            switch (pC->xDec[z].decodeStatus.command2) {
                case 0: // command none - process
                    pC->xDec[z].decodeStatus.command2 |= 0x80;
                    break;
                case 1: // command abort - leave now
                    TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_decodeCommand: decode command abort (0x%02x)",
                                 as+zS, 1));
                    pC->xDec[z].decodeStatus.command2 |= 0x80;
                    return (ASPERR_ABORT);
                case 2: // command restart - leave later
                    TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_decodeCommand: decode command quit (0x%02x)",
                                 as+zS, 2));
                    pC->xDec[z].decodeStatus.command2 |= 0x80;
                    return (ASPERR_QUIT);
                default: // command unknown - ignore
                    break;
            }
        }
    }

    return 0;
} //PAF_AST_decodeCommand

// -----------------------------------------------------------------------------
// AST Decoding Function - Encode Command Processing
//
//   Name:      PAF_AST_encodeCommand
//   Purpose:   Decoding Function for processing Encode Commands.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * Command execution.
//              * SIO control errors.
//              * Error number macros.
//

Int
PAF_AST_encodeCommand (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* encode counter */
    Int errno = 0;                      /* error number */
    Int zO, zS;


    for (z=ENCODE1; z < ENCODEN; z++) {
        zO = pP->outputsFromEncodes[z];
        zS = pP->streamsFromEncodes[z];
        if (! (pC->xEnc[z].encodeStatus.command2 & 0x80)) {
            switch (pC->xEnc[z].encodeStatus.command2) {
                case 0: // command none - process
                    pC->xEnc[z].encodeStatus.command2 |= 0x80;
                    break;
                case 1: // mute command
                    TRACE_VERBOSE((&TR_MOD, "AS%d: PAF_AST_encodeCommand: encode command mute (0x%02x)",
                                   as+zS, 1));
                    if ((pC->xOut[zO].outBufStatus.audio & 0x0f) != PAF_OB_AUDIO_QUIET
                        && pC->xOut[zO].hTxSio
                        && (errno = SIO_ctrl (pC->xOut[zO].hTxSio, PAF_SIO_CONTROL_MUTE, 0))) {
                        errno = (errno & 0xff) | ASPERR_MUTE;
                        /* convert to sensical errno */
                        TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_encodeCommand: SIO control failed (mute)", as+zS));
                        TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_encodeCommand: errno = 0x%04x <ignored>", as+zS, errno));
                    }
                    else {
                        pC->xOut[zO].outBufStatus.audio |= PAF_OB_AUDIO_MUTED;
                    }
                    pC->xEnc[z].encodeStatus.command2 |= 0x80;
                    break;
                case 2: // unmute command
                    TRACE_VERBOSE((&TR_MOD, "AS%d: PAF_AST_encodeCommand: encode command unmute (0x%02x)", as+zS, 2));
                    if ((pC->xOut[zO].outBufStatus.audio & 0x0f) != PAF_OB_AUDIO_QUIET
                        && pC->xOut[zO].hTxSio
                        && (errno = SIO_ctrl (pC->xOut[zO].hTxSio, PAF_SIO_CONTROL_UNMUTE, 0))) {
                        errno = (errno & 0xff) | ASPERR_MUTE;
                        /* convert to sensical errno */
                        TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_encodeCommand: SIO control failed (unmute)", as+zS));
                        TRACE_TERSE((&TR_MOD, "AS%d: PAF_AST_encodeCommand: errno = 0x%04x <ignored>", as+zS, errno));
                    }
                    else {
                        pC->xOut[zO].outBufStatus.audio &= ~PAF_OB_AUDIO_MUTED;
                    }
                    pC->xEnc[z].encodeStatus.command2 |= 0x80;
                    break;
                default: // command unknown - ignore
                    break;
            }
        }
    }

    ERRNO_RPRT (audioStream1Task, errno);

    return 0;
} //PAF_AST_encodeCommand

// -----------------------------------------------------------------------------
// AST Decoding Function - Reinitialization of Decode
//
//   Name:      PAF_AST_decodeInit
//   Purpose:   Decoding Function for reinitializing the decoding process.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeInit (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[])
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/encode counter */
    Int errno;                          /* error number */
    Int zI, zO, zS;
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;

    (void)as;  // in case not used with tracing disabled.

    // reset frameCount
    for (z=DECODE1; z < DECODEN; z++)
        if (pC->xDec[z].decodeStatus.mode)
            pC->xDec[z].decodeStatus.frameCount = 0;

    // reset drift register. Do this here so that register keeps its value until
    // decodeProcessing is restarted. Mostly useful in tightly coupled testing
    // where, e.g., a custom system stream can read the register. But not useful
    // in general since the timing window is so small.
    pC->xDec[zMD].decodeStatus.bufferDrift = 0;

    // infoSize = alg[zMI]->fxns->algControl(alg[zMI], DEC_MININFO, NULL);
    // vs. FRAMEINFO? --Kurt

    for (z=DECODE1; z < DECODEN; z++) {
        DEC_Handle dec = (DEC_Handle )decAlg[z];
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS;
        if (pC->xInp[zI].hRxSio && pC->xDec[z].decodeStatus.mode) {
            Uns gear;
            Int frameLength;
            TRACE_VERBOSE((&TR_MOD, "AS%d: PAF_AST_decodeInit: initializing decode", as+zS));
            if (decAlg[z]->fxns->algActivate)
                decAlg[z]->fxns->algActivate (decAlg[z]);
            if (dec->fxns->reset
                && (errno = dec->fxns->reset (dec, NULL,
                                              &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus)))
                return errno;
            gear = pC->xDec[z].decodeStatus.aspGearControl;
            pC->xDec[z].decodeStatus.aspGearStatus = gear < GEARS ? gear : 0;
            frameLength = pP->fxns->computeFrameLength (decAlg[z], FRAMELENGTH,
                                                        pC->xDec[z].decodeStatus.bufferRatio);
            pC->xDec[z].decodeControl.frameLength = frameLength;
            pC->xDec[z].decodeInStruct.sampleCount = frameLength;
            pC->xDec[z].decodeControl.sampleRate = PAF_SAMPLERATE_UNKNOWN;

            if (pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS) {
                if (errno = SIO_ctrl (pC->xInp[zI].hRxSio,
                                      PAF_SIO_CONTROL_SET_AUTOREQUESTSIZE, 2*frameLength))
                    return ASPERR_AUTO_LENGTH;    // ** need a different error code here? **
            }
            else if (z != zMD) {
                if (errno = SIO_idle (pC->xInp[zI].hRxSio))
                    return errno;
            }

            if (errno = SIO_ctrl (pC->xInp[zI].hRxSio,
                                  PAF_SIO_CONTROL_SET_SOURCESELECT,
                                  DECSIOMAP (pC->xDec[z].decodeStatus.sourceSelect)))
                return errno;
            if (errno = SIO_ctrl (pC->xInp[zI].hRxSio,
                                  PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, frameLength))
                return errno;
            if (errno = pP->fxns->updateInputStatus (pC->xInp[zI].hRxSio,
                                                     &pC->xInp[zI].inpBufStatus, &pC->xInp[zI].inpBufConfig))
                return errno;
        }
    }

    if (pC->xInp[zMI].hRxSio) {
        // if in master decoder is in continous mode and
        // not running then change sourceDecode to force start
        if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
            pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_NONE)
            pC->xDec[zMD].decodeStatus.sourceDecode = PAF_SOURCE_UNKNOWN;

        errno = SIO_issue (pC->xInp[zMI].hRxSio,
                           &pC->xInp[zMI].inpBufConfig,
                           sizeof (pC->xInp[zMI].inpBufConfig),
                           pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                           pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_UNKNOWN ?
                           PAF_SIO_REQUEST_AUTO :
                           PAF_SIO_REQUEST_NEWFRAME);
        if (errno)
            return errno;
    }

    // TODO: move this to start of this function so that it doesn't affect IO timing
    for (z=ENCODE1; z < ENCODEN; z++) {
        zO = pP->outputsFromEncodes[z];
        zS = pP->streamsFromEncodes[z];
        if (pC->xOut[zO].hTxSio && pC->xEnc[z].encodeStatus.mode) {
            Int select = pC->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pC->xEnc[z].encAlg[select];
            ENC_Handle enc = (ENC_Handle )encAlg;
            TRACE_VERBOSE((&TR_MOD, "AS%d: PAF_AST_decodeInit: initializing encode", as+zS));
            if (encAlg->fxns->algActivate)
                encAlg->fxns->algActivate (encAlg);
            if (enc->fxns->reset
                && (errno = enc->fxns->reset (enc, NULL,
                                              &pC->xEnc[z].encodeControl, &pC->xEnc[z].encodeStatus)))
                return errno;
        }
    }

    return 0;
} //PAF_AST_decodeInit

// -----------------------------------------------------------------------------
// AST Decoding Function - Info Processing, Common
//
//   Name:      PAF_AST_decodeInfo
//   Purpose:   Decoding Function for processing information in a manner that
//              is common for both initial and subsequent frames of input data.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeInfo (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* input/decode/stream counter */
    Int errno;                          /* error number */
    Int sioErr;                         /* error number, SIO */
    Int zD, zI, zS, zX;
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;
    Int zMS = pC->masterStr;

    (void)zMS;  (void)as;  // in case not used with tracing disabled.

    // Set decode control: sample rate, emphasis
    for (z=INPUT1; z < INPUTN; z++) {
        zD = z;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->inputsFromDecodes[zX] == z) {
                zD = zX;
                break;
            }
        }

        if (pC->xInp[z].hRxSio) {
            //determine associated decoder
            if (pC->xInp[z].inpBufStatus.sampleRateStatus != pC->xDec[zD].decodeControl.sampleRate) {
                if (pC->xDec[zD].decodeControl.sampleRate == PAF_SAMPLERATE_UNKNOWN) {
                    pC->xDec[zD].decodeControl.sampleRate = pC->xInp[z].inpBufStatus.sampleRateStatus;
                }
                else
                {
                    TRACE_TERSE((&TR_MOD, "as1-f2.%d: AS%d: return error ASPERR_INFO_RATECHANGE", __LINE__, as+pC->masterStr));
                    TRACE_TERSE((&TR_MOD, "as1-f2 inpBufStatus.sampleRateStatus: 0x%x, decodeControl.sampleRate: 0x%x", 
                                 pC->xInp[z].inpBufStatus.sampleRateStatus, pC->xDec[zD].decodeControl.sampleRate));
                    // return (ASPERR_INFO_RATECHANGE);
                }
            }
            pC->xDec[zD].decodeControl.emphasis =
                pC->xDec[zD].decodeStatus.sourceDecode != PAF_SOURCE_PCM
                ? PAF_IEC_PREEMPHASIS_NO // fix for Mantis ID #119
                : pC->xInp[z].inpBufStatus.emphasisStatus;
        }
        else {
            pC->xDec[zD].decodeControl.sampleRate = PAF_SAMPLERATE_UNKNOWN;
            pC->xDec[zD].decodeControl.emphasis = PAF_IEC_PREEMPHASIS_UNKNOWN;
        }
    }

    // Wait for info input
    TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeInfo.%d: AS%d: awaiting frame %d -- sync+info+data", __LINE__, as+zMS, frame));

    if (pC->xInp[zMI].hRxSio) {
        if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS) {
            // only perform reclaim if input really operating
            if (!(pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                  pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_NONE)) {

                sioErr = SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr )&pC->xInp[zMI].pInpBuf, NULL);
                if (sioErr != sizeof (pC->xInp[zMI].inpBufConfig)) {
                    // only attempt error recovery for DIBERR_SYNC
                    if (sioErr != -DIBERR_SYNC ||
                        // attempted error recovery failed?
                        (errno = PAF_AST_decodeHandleErrorInput (pP, pQ, pC, decAlg, zMI, -sioErr)) )
                    {
                        TRACE_TERSE((&TR_MOD, "as1-f2.%d: return error ASPERR_RECLAIM.  sioErr: 0x%x. errno 0x%x.", __LINE__, sioErr, errno));
                        return ASPERR_RECLAIM;
                    }
                }
                else {
                    if (pC->xDec[zMD].decodeStatus.mode) {
                        if (SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_SOURCEPROGRAM,
                                      (Arg )&pC->xDec[zMD].decodeStatus.sourceProgram))
                        {
                            TRACE_TERSE((&TR_MOD, "as1-f2.%d: return error ASPERR_INFO_PROGRAM.  errno 0x%x.", __LINE__, errno));
                            return ASPERR_INFO_PROGRAM;
                        }
                        if (pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                            // actual source detected? (* relies on ordering of PAF_SOURCE_* *)
                            pC->xDec[zMD].decodeStatus.sourceProgram >= PAF_SOURCE_PCM)
                        {   
                            // algorithm not supported for detected source type?
                            if ( (!(pC->xDec[zMD].decodeStatus.sourceProgram < pP->pDecAlgKey->length) ||
                                  (! pC->xDec[zMD].decAlg[pC->xDec[zMD].decodeStatus.sourceProgram])) &&
                                 // attempted error recovery failed?
                                 (errno = PAF_AST_decodeHandleErrorInput (pP, pQ, pC, decAlg, zMI, ASPERR_INFO_PROGRAM)) )
                            {
                                TRACE_TERSE((&TR_MOD, "as1-f2.%d: return error ASPERR_INFO_PROGRAM.  errno 0x%x.", __LINE__, errno));
                                return ASPERR_INFO_PROGRAM;
                            }
                        }   
                    }
                }
            }
        } // end of DEC_MODE_CONTINUOUS
        else 
        {   // normal case
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: call SIO_reclaim to get input buffer.", __LINE__));
            sioErr = SIO_reclaim (pC->xInp[zMI].hRxSio, (Ptr )&pC->xInp[zMI].pInpBuf, NULL);
            if (sioErr != sizeof (pC->xInp[zMI].inpBufConfig))
            {
                TRACE_TERSE((&TR_MOD, "as1-f2.%d: SIO_reclaim on input returned error ASPERR_RECLAIM.  sioErr: 0x%x. errno 0x%x.", __LINE__, sioErr, errno));
                return ASPERR_RECLAIM;
            }
        }
    } //pC->xInp[zMI].hRxSio

    // Decode info
    for (z=DECODE1; z < DECODEN; z++) {
        DEC_Handle dec = (DEC_Handle )decAlg[z];
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS;
        if (pC->xInp[zI].hRxSio && pC->xDec[z].decodeStatus.mode) {

            TRACE_GEN((&TR_MOD, "as1-f2.%d: PAF_AST_decodeInfo: AS%d: processing frame %d -- info", __LINE__, as+zS, frame));

            if (errno = pP->fxns->updateInputStatus (pC->xInp[zI].hRxSio,
                                                     &pC->xInp[zI].inpBufStatus, &pC->xInp[zI].inpBufConfig))
            {
                TRACE_TERSE((&TR_MOD, "as1-f2.%d: return error errno 0x%x.", __LINE__, errno));
                return errno;
            }
            if (dec->fxns->info
                && (errno = dec->fxns->info (dec, NULL,
                                             &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus))) {
                if (pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS) {

                    // if error recovery successful, retry info w/ PCM decoder w/ zero input
                    if ( !(errno = PAF_AST_decodeHandleErrorInput (pP, pQ, pC, decAlg, z, errno))) {
                        DEC_Handle dec = (DEC_Handle )decAlg[z];

                        if (dec->fxns->info
                            && (errno = dec->fxns->info (dec, NULL,
                                                         &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus)))
                        {
                            TRACE_TERSE((&TR_MOD, "as1-f2.%d: return error errno 0x%x.", __LINE__, errno));
                            return errno;
                        }
                    }
                }
                else
                {
                    TRACE_TERSE((&TR_MOD, "as1-f2.%d: return error errno 0x%x.", __LINE__, errno));
                    return errno;
                }
            }
            // increment decoded frame count
            pC->xDec[z].decodeStatus.frameCount += 1;
        }
    } // z=DECODE1 to DECODEN

    // query IB for latest sourceProgram (needed if we started decoding due to a force mode)
    if (pC->xDec[zMD].decodeStatus.mode && (pC->xDec[zMD].decodeStatus.mode != DEC_MODE_CONTINUOUS)) {
        XDAS_Int8 sourceProgram;
        if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio,
                              PAF_SIO_CONTROL_GET_SOURCEPROGRAM,
                              (Arg )&sourceProgram))
        {
            TRACE_TERSE((&TR_MOD, "as1-f2.%d: return error ASPERR_AUTO_PROGRAM. errno 0x%x.", __LINE__, errno));
            return ASPERR_AUTO_PROGRAM;
        }
        pC->xDec[zMD].decodeStatus.sourceProgram = sourceProgram;
    }

    // since now decoding update decode status for all enabled decoders
    for (z=DECODE1; z < DECODEN; z++) {
        if (pC->xDec[z].decodeStatus.mode && (pC->xDec[z].decodeStatus.mode != DEC_MODE_CONTINUOUS)) {
            pC->xDec[z].decodeStatus.sourceDecode = pC->xDec[z].decodeStatus.sourceProgram;
            if (pC->xDec[z].decodeStatus.sourceSelect == PAF_SOURCE_SNG)
                pC->xDec[z].decodeStatus.sourceDecode = PAF_SOURCE_SNG;
        }
    }

    // TODO: move this to start of this function so that it doesn't affect IO timing
    // Initialize audio frame(s)
    //    Re-initialize audio frame if there is an assocatiated decode and
    //    that decode doesn't have a valid input or is turned off
    for (z=STREAM1; z < STREAMN; z++) {
        Int reset = 0;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->streamsFromDecodes[zX] == z) {
                zI = pP->inputsFromDecodes[zX];
                if (!pC->xDec[zX].decodeStatus.mode || !pC->xInp[zI].hRxSio)
                    reset = 1;
            }
        }
        if (reset) {
            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo: AS%d: initializing block %d -- info", as+z, frame));
            pP->fxns->initFrame1 (pP, pQ, pC, z, 0);
        }
        else
            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo: AS%d: initializing block %d -- info <ignored>", as+z, frame));
    }

    return 0;
} //PAF_AST_decodeInfo

// -----------------------------------------------------------------------------
// AST Decoding Function - Info Processing, Initial
//
//   Name:      PAF_AST_decodeInfo1
//   Purpose:   Decoding Function for processing information in a manner that
//              is unique to initial frames of input data.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeInfo1 (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/encode counter */
    Int errno;                          /* error number */
    Int zMD = pC->masterDec;
    double arcRatio; 

    (void)as;  // in case not used with tracing disabled.

    // run the chain of ASP's on the stream.
    TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeInfo1.%d: calling streamChainFunction.", __LINE__));
    if (errno = pP->fxns->streamChainFunction (pP, pQ, pC, PAF_ASP_CHAINFRAMEFXNS_RESET, 1, frame))
    {
        TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo1.%d: streamChainFunction returns errno 0x%x ", __LINE__, errno));
        return errno;
    }

    // Compute ArcRatio after running stream function to that the stream sample rate 
    // can be corrected by the SRC
    // Was previously called before the chain function.
    TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeInfo1.%d: calling PAF_AST_computeRateRatio.", __LINE__));
    if (errno = PAF_AST_computeRateRatio (pP, pC, &arcRatio, frame))
    {
        TRACE_TERSE((&TR_MOD, "PAF_AST_decodeInfo1.%d: PAF_AST_computeRateRatio returns errno 0x%x ", __LINE__, errno));
        return errno;
    }

    // If rate tracking, set ARC Q24 register based on rate computed above, otherwise skip this so
    // ARC Q24 register's current value (which is read every ARC reset/apply call) is used.
    // I.e. for non-tracking, host can set/change Q24 value at init and/or while ARC is running.
    if (pC->xInp[0].inpBufStatus.rateTrackMode && pC->xOut[0].outBufStatus.rateTrackMode)
    {
        if (pP->fxns->controlRate)
        {
            TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeInfo1.%d: calling controlRate.", __LINE__));
            errno = pP->fxns->controlRate(pC->xInp[0].hRxSio, pC->xOut[0].hTxSio, pC->acp, arcRatio);
            if (errno)
            {
                TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo1.%d: controlRate returns errno 0x%x ", __LINE__, errno));
                return errno;
            }
        }
    }

    TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeInfo1.%d: calling enc->info.", __LINE__));
    for (z=ENCODE1; z < ENCODEN; z++) {
        Int zO = pP->outputsFromEncodes[z];
        if (pC->xOut[zO].hTxSio && pC->xEnc[z].encodeStatus.mode) {
            Int select = pC->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pC->xEnc[z].encAlg[select];
            ENC_Handle enc = (ENC_Handle )encAlg;
            if (enc->fxns->info
                && (errno = enc->fxns->info(enc, NULL,
                                            &pC->xEnc[z].encodeControl, &pC->xEnc[z].encodeStatus)))
            {
                TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo1.%d: info returns errno 0x%x ", __LINE__, errno));
                return errno;
            }
        }
    }


    if (errno = pP->fxns->setCheckRateX (pP, pQ, pC, 0))
    {
        // ignore if rateX has changed since we haven't, but are about to,
        // start the output. If we didn't ignore this case then the state machine
        // would restart unnecessarily, e.g. in the case of SRC, resulting in
        // added latency.
        if (errno != ASPERR_INFO_RATECHANGE)
        {
            TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo1.%d: setCheckRateX returns errno 0x%x, not RATECHANGE", __LINE__, errno));
            return errno;
        }
        else
        {
            TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo1.%d: RATECHANGE returns RATECHANGE, ignoring", __LINE__));
        }
    }

    if (errno = pP->fxns->startOutput (pP, pQ, pC, arcRatio)) 
    {
        if (errno == 0x105) {
            TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo1.%d: startOutput returns RING BUFFER FULL (0x%x)", __LINE__, errno));
        }
        else
        {
            TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo1.%d: startOutput returns errno 0x%x", __LINE__, errno));
        }
        return errno;
    }

    // Start slave inputs if necessary
    for (z=DECODE1; z < DECODEN; z++) {
        Int zI = pP->inputsFromDecodes[z];
        Int zS = pP->streamsFromDecodes[z];
        (void)zS;  // in case not used with tracing disabled.
        if (z == zMD
            || ! pC->xInp[z].hRxSio
            || ! pC->xDec[z].decodeStatus.mode)
            continue;

        TRACE_TERSE((&TR_MOD, "as1-f2: decodeInfo1.%d: About to request SIO SYNC[%d]",__LINE__, zI));
        if (errno = SIO_issue (pC->xInp[zI].hRxSio, &pC->xInp[zI].inpBufConfig,
                       sizeof (pC->xInp[zI].inpBufConfig), PAF_SIO_REQUEST_SYNC))  // sync only, not data
        {
            TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo1.%d: SIO_REQUEST_SYNC returns errno 0x%x, we return ASPERR_ISSUE (0x%x)",
                            __LINE__, errno, ASPERR_ISSUE));
            return (ASPERR_ISSUE);
        }
        if ( (errno = SIO_reclaim (pC->xInp[zI].hRxSio, (Ptr )&pC->xInp[zI].pInpBuf, NULL))
            != sizeof (pC->xInp[zI].inpBufConfig))
        {   // will return OK because last call was REQUEST_SYNC
            TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo1.%d: SIO_reclaim returns wrong size (%d), we return ASPERR_RECLAIM (0x%x)",
                            __LINE__, errno, ASPERR_RECLAIM));
            return (ASPERR_RECLAIM);
        }

        TRACE_VERBOSE((&TR_MOD, "as1-f2: decodeInfo1: AS%d: input started", as+zS));
    }

    return 0;
} //PAF_AST_decodeInfo1

// -----------------------------------------------------------------------------
// AST Decoding Function - Info Processing, Subsequent
//
//   Name:      PAF_AST_decodeInfo2
//   Purpose:   Decoding Function for processing information in a manner that
//              is unique to frames of input data other than the initial one.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     None.
//

Int
PAF_AST_decodeInfo2 (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;
    Int zMS = pC->masterStr;
    Int errno;
    Int inpNumEvents;
    Int outNumEvents;


    // assume output for comparison is indexed same as master stream (zMS)
    // only perform this calculation if both input and output are active and
    // store in the master decoder (zMD) status
    if (pC->xInp[zMI].hRxSio && pC->xOut[zMS].hTxSio) {
        const Int oldMask = HWI_disable ();

        if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_NUM_EVENTS, (Arg) &inpNumEvents)) {
            errno |= PAF_COMPONENT_IO;
            TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo2.%d: SIO_ctrl errno 0x%x", __LINE__, errno));
            HWI_restore (oldMask);
            return errno;
        }

        if (errno = SIO_ctrl (pC->xOut[zMS].hTxSio, PAF_SIO_CONTROL_GET_NUM_EVENTS, (Arg) &outNumEvents)) {
            errno |= PAF_COMPONENT_IO;
            TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo2.%d: SIO_ctrl errno 0x%x", __LINE__, errno));
            HWI_restore (oldMask);
            return errno;
        }

        HWI_restore (oldMask);

        pC->xDec[zMD].decodeStatus.bufferDrift = outNumEvents - inpNumEvents;
        TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo2: outNumEvents: %u,  inpNumEvents: %u.  bufferDrift: %d", 
                      outNumEvents, inpNumEvents, pC->xDec[zMD].decodeStatus.bufferDrift));
    }

    errno = pP->fxns->setCheckRateX (pP, pQ, pC, 1);
    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeInfo2.%d: return 0x%x", __LINE__, errno));
    return errno;
} //PAF_AST_decodeInfo2

// -----------------------------------------------------------------------------
// AST Decoding Function - Continuation Processing
//
//   Name:      PAF_AST_decodeCont
//   Purpose:   Decoding Function for processing that occurs subsequent to
//              information processing but antecedent to timing processing
//              for frames of input data other than the initial one.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeCont (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode counter */
    Int zI, zS;
    Int zMD = pC->masterDec;

    (void)as;  // in case not used with tracing disabled.

    // Await slave inputs
    for (z=DECODE1; z < DECODEN; z++) {
        zI = pP->inputsFromDecodes[z];
        zS = pP->streamsFromDecodes[z];
        (void)zS;
        if (z == zMD
            || ! pC->xInp[zI].hRxSio
            || ! pC->xDec[z].decodeStatus.mode)
            continue;
        TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeCont: AS%d: awaiting frame %d -- data", as+zS, frame));
        if (SIO_reclaim (pC->xInp[zI].hRxSio, (Ptr )&pC->xInp[zI].pInpBuf, NULL)
            != sizeof (pC->xInp[zI].inpBufConfig))
            return (ASPERR_RECLAIM);
    }

    return 0;
} //PAF_AST_decodeCont

// -----------------------------------------------------------------------------
// AST Decoding Function - Decode Processing
//
//   Name:      PAF_AST_decodeDecode
//   Purpose:   Decoding Function for processing of input data by the
//              Decode Algorithm.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeDecode (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* decode/stream counter */
    Int errno;                          /* error number */
    Int ch;
    Int zMD = pC->masterDec;     // index of master decoder
    Int zMI = pP->zone.master;   // master input (DIB/DRI)
    Int remaining = 0x7FFFFFFF;
    Int shortenedFrame = 0;

    (void)as;

    // Clear samsiz for all channels - MID 208.
    for (z=STREAM1; z < STREAMN; z++) {
        for (ch=0; ch < PAF_MAXNUMCHAN_AF; ch++) {
            pC->xStr[z].pAudioFrame->data.samsiz[ch] = 0;
        }
    }

    if (pC->xInp[zMI].hRxSio &&
        pC->xDec[zMD].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
        // still performing auto-detection, i.e., still using PAF_SIO_REQUEST_AUTO?
        pC->xDec[zMD].decodeStatus.sourceDecode == PAF_SOURCE_UNKNOWN &&
        // actual source detected? (* relies on ordering of PAF_SOURCE_* *)
        pC->xDec[zMD].decodeStatus.sourceProgram >= PAF_SOURCE_PCM) {

        if (errno = SIO_ctrl (pC->xInp[zMI].hRxSio, PAF_SIO_CONTROL_GET_NUM_REMAINING, (Arg )&remaining))
        {
            // ** change to ASPERR_INFO_NUM_REMAINING, or similar **
            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeDecode.%d: SIO GET_NUM_REMAINING returns 0x%x", __LINE__, errno));
            return errno;
        }
        remaining /= 2;  // adjust for two channels

        // opportunity to adjust timing of secondary to better match primary?
        if ( FRAMELENGTH + MINFRAMELENGTH <= remaining && remaining < 2*FRAMELENGTH) {
            shortenedFrame = remaining - FRAMELENGTH;

            // assure adjusted framelength is multiple of 8 (ASP granularity)
            shortenedFrame /= PA_MODULO;
            shortenedFrame *= PA_MODULO;
            TRACE_TIME((&TIME_MOD, "as1-f2: PAF_AST_decodeDecode: AS%d: adjustment frame length = %d", as+zMD, shortenedFrame));
        }
    }

    // Decode data
    for (z=DECODE1; z < DECODEN; z++) {
        DEC_Handle dec = (DEC_Handle )decAlg[z];
        Int zI = pP->inputsFromDecodes[z];
        Int zS = pP->streamsFromDecodes[z];
        (void)zS;
        if (pC->xInp[zI].hRxSio && pC->xDec[z].decodeStatus.mode)
        {
            TRACE_GEN((&TR_MOD, "as1-f2.%d: AS%d: decodeDecode: processing block %d -- decode", __LINE__, as+zS, block));

            TRACE_VERBOSE((&TR_MOD, "as1-f2: AS%d: decodeDecode: decoding from 0x%x (base) 0x%x (ptr)",
            		as+zS,
            		pC->xInp[z].pInpBuf->base.pVoid,
            		pC->xInp[z].pInpBuf->head.pVoid));

            if (dec->fxns->decode
                && (errno = dec->fxns->decode (dec, NULL,
                                               &pC->xDec[z].decodeInStruct, &pC->xDec[z].decodeOutStruct))) {

                if (pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS) {
                    // if error recovery successful, retry decoding w/ PCM decoder w/ zero input
                    if ( !(errno = PAF_AST_decodeHandleErrorInput (pP, pQ, pC, decAlg, z, errno))) {
                        DEC_Handle dec = (DEC_Handle )decAlg[z];

                        if (dec->fxns->info
                            && (errno = dec->fxns->info (dec, NULL,
                                                         &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus)))
                        {
                            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeDecode.%d: fxns->info returns 0x%x", __LINE__, errno));
                            return errno;
                        }
                        if (dec->fxns->decode
                            && (errno = dec->fxns->decode (dec, NULL,
                                                           &pC->xDec[z].decodeInStruct, &pC->xDec[z].decodeOutStruct)))
                        {
                            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeDecode.%d: fxns->decode returns 0x%x", __LINE__, errno));
                            return errno;
                        }
                    }
                }
                else // not continuous
                {
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeDecode.%d: fxns->decode returns 0x%x", __LINE__, errno));
                    return errno;
                }
            }

#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
            as_traceChannels(pC, z);
#endif

            if (pC->xDec[z].decodeOutStruct.outputFlag & 1) {
                Int frameLength;

                frameLength = pP->fxns->computeFrameLength (decAlg[z],
                                                            FRAMELENGTH, pC->xDec[z].decodeStatus.bufferRatio);

// ............................................................................

                if (shortenedFrame > 0)
                    frameLength = shortenedFrame;
                else if (pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                         // per above calculations, this will only be true if ready for decoder change
                         remaining < FRAMELENGTH + MINFRAMELENGTH)
                {
                    DEC_Handle dec;

                    if (decAlg[z]->fxns->algDeactivate)
                        decAlg[z]->fxns->algDeactivate (decAlg[z]);

                    pC->xDec[z].decodeStatus.sourceDecode = pC->xDec[z].decodeStatus.sourceProgram;

                    decAlg[z] = pC->xDec[z].decAlg[pC->xDec[z].decodeStatus.sourceDecode];
                    dec = (DEC_Handle )decAlg[z];

                    TRACE_TIME((&TIME_MOD, "as1-f2: PAF_AST_decodeDecode: AS%d: changing decoder", as+z));

                    if (decAlg[z]->fxns->algActivate)
                        decAlg[z]->fxns->algActivate (decAlg[z]);
                    if (dec->fxns->reset
                        && (errno = dec->fxns->reset (dec, NULL,
                                                      &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus)))
                    {
                        TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeDecode.%d: fxns->reset returns 0x%x", __LINE__, errno));
                        return errno;
                    }
                }

// ............................................................................

                pC->xDec[z].decodeControl.frameLength = frameLength;
                pC->xDec[z].decodeInStruct.sampleCount = frameLength;
                if (errno = SIO_ctrl (pC->xInp[zI].hRxSio,
                                      PAF_SIO_CONTROL_SET_PCMFRAMELENGTH, frameLength))
                {
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeDecode.%d: SIO SET_PCMFRAMELENGTH returns 0x%x", __LINE__, errno));
                    return errno;
                }

                // only perform issue if input really operating
                // if not, must wait until appropriate time to restart
                if ( !(pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                       // assures initial reclaim
                       pC->xDec[z].decodeStatus.sourceProgram == PAF_SOURCE_NONE) )
                {
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeDecode.%d: calling SIO_issue[%d]", __LINE__, zI));
                    if (errno = SIO_issue (pC->xInp[zI].hRxSio, &pC->xInp[zI].inpBufConfig,
                                   sizeof (pC->xInp[zI].inpBufConfig),
                                   pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS &&
                                   pC->xDec[z].decodeStatus.sourceDecode == PAF_SOURCE_UNKNOWN ?
                                   PAF_SIO_REQUEST_AUTO :
                                   PAF_SIO_REQUEST_NEWFRAME))
                    {
                        TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeDecode.%d: SIO_issue returns 0x%x, we return ASPERR_ISSUE (0x%x)", __LINE__, errno, ASPERR_ISSUE));
                        return (ASPERR_ISSUE);
                    }
                }
            } // outputFlag & 1
        } // hRxSio && decodeStatus.mode
        else
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: AS%d: PAF_AST_decodeDecode: processing block %d -- decode <ignored>", __LINE__, as+zS, block));
    } // z=DECODE1 to DECODEN

    // Set up audio frames not decoded into
    //    Re-initialize audio frame if there is an assocatiated decode and
    //    that decode doesn't have a valid input or is turned off
    for (z=STREAM1; z < STREAMN; z++) {
        Int zX;
        Int reset = 0;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->streamsFromDecodes[zX] == z) {
                Int zI = pP->inputsFromDecodes[zX];
                if (!pC->xDec[zX].decodeStatus.mode || !pC->xInp[zI].hRxSio)
                    reset = 1;
            }
        }
        if (reset) {
            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeDecode: AS%d: initializing block %d -- decode", as+z, frame));
            pP->fxns->initFrame1 (pP, pQ, pC, z, 0);
        }
        else
            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_decodeDecode: AS%d: initializing block %d -- decode <ignored>", as+z, frame));
    }
    return 0;
} //PAF_AST_decodeDecode

// -----------------------------------------------------------------------------
// AST Decoding Function - Stream Processing
//
//   Name:      PAF_AST_decodeStream
//   Purpose:   Decoding Function for processing of audio frame data by the
//              ASP Algorithms.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent/child.
//

Int
PAF_AST_decodeStream (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int errno;
    double arcRatio;  

    TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeStream.%d: calling streamChainFunction.", __LINE__));
    errno = pP->fxns->streamChainFunction (pP, pQ, pC, PAF_ASP_CHAINFRAMEFXNS_APPLY, 1, block);
    if (errno )
    {
        TRACE_TERSE((&TR_MOD, "PAF_AST_decodeStream.%d: streamChainFunction returns errno 0x%x ", __LINE__, errno));
        return errno;
    }

    // Compute ArcRatio after running stream function to that the stream sample rate 
    // can be corrected by the SRC
    // Was previously called before the chain function.
    TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeStream.%d: calling PAF_AST_computeRateRatio.", __LINE__));
    if (errno = PAF_AST_computeRateRatio (pP, pC, &arcRatio, frame))
    {
        TRACE_TERSE((&TR_MOD, "PAF_AST_decodeStream.%d: PAF_AST_computeRateRatio returns errno 0x%x ", __LINE__, errno));
        return errno;
    }

    // If rate tracking, set ARC Q24 register based on rate computed above, otherwise skip this so
    // ARC Q24 register's current value (which is read every ARC reset/apply call) is used.
    // I.e. for non-tracking, host can set/change Q24 value at init and/or while ARC is running.
    if (pC->xInp[0].inpBufStatus.rateTrackMode && pC->xOut[0].outBufStatus.rateTrackMode)
    {
        if (pP->fxns->controlRate)
        {
            TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeStream.%d: calling controlRate.", __LINE__));
            errno = pP->fxns->controlRate(pC->xInp[0].hRxSio, pC->xOut[0].hTxSio, pC->acp, arcRatio);
            if (errno)
            {
                TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeStream.%d: controlRate returns errno 0x%x ", __LINE__, errno));
                return errno;
            }
        }
    }
    return 0;

} //PAF_AST_decodeStream

// -----------------------------------------------------------------------------
// AST Decoding Function - Encode Processing
//
//   Name:      PAF_AST_decodeEncode
//   Purpose:   Decoding Function for processing of audio frame data by the
//              Encode Algorithm.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeEncode (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* encode/output counter */
    Int errno;                          /* error number */
    Int zX, zE, zS;


    // Await output buffers (but not first time)
    for (z=OUTPUT1; z < OUTPUTN; z++) {
        // determine encoder associated with this output
        zE = z;
        for (zX = ENCODE1; zX < ENCODEN; zX++) {
            if (pP->outputsFromEncodes[zX] == z) {
                zE = zX;
                break;
            }
        }
        zS = pP->streamsFromEncodes[zE];

        if (pC->xOut[z].hTxSio) {
            // update length (e.g. ARC may have changed)
            pC->xOut[z].outBufConfig.lengthofFrame = pC->xEnc[zE].encodeInStruct.pAudioFrame->sampleCount;
            TRACE_GEN((&TR_MOD, "as1-f2.%d: AS%d: PAF_AST_decodeEncode: processing block %d -- idle", __LINE__, as+zS, block));
            errno = SIO_reclaim (pC->xOut[z].hTxSio,(Ptr *) &pC->xOut[z].pOutBuf, NULL);
            if (errno < 0 ) {
                SIO_idle (pC->xOut[z].hTxSio);
                TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeEncode: AS%d: SIO_reclaim returns error %d", as+zS, -errno));
                return -errno; // SIO negates error codes
            }
            // TODO: use pC->xOut[z].pOutBuf in following ->encode call
        }
        else {
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: AS%d: PAF_AST_decodeEncode: processing block %d -- idle <ignored>", __LINE__, as+zS, block));
        }
    }

    // Encode data
    for (z=ENCODE1; z < ENCODEN; z++) {
        Int zO = pP->outputsFromEncodes[z];
        Int zS = pP->streamsFromEncodes[z];
        (void)zS;
        if (pC->xOut[zO].hTxSio && pC->xEnc[z].encodeStatus.mode) {
            Int select = pC->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pC->xEnc[z].encAlg[select];
            ENC_Handle enc = (ENC_Handle )encAlg;
            if(select != pC->xEnc[z].encodeControl.encActive){
                pC->xEnc[z].encodeControl.encActive = select;
                TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeEncode: return error line %d", __LINE__));
                return (-1);
            }
            TRACE_GEN((&TR_MOD, "as1-f2.%d: AS%d: PAF_AST_decodeEncode: processing block %d -- encode", __LINE__, as+zS, block));

            // (MID 1933) temp. workaround for PCE2
            pC->xEnc[z].encodeInStruct.pAudioFrame->data.nChannels = PAF_MAXNUMCHAN;

          /*
          #if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
            {
                PAF_AudioFrame *pAudioFrame = pC->xEnc[z].encodeInStruct.pAudioFrame;
                int *wp;
                wp = (int*)pAudioFrame->data.sample[0];
                TRACE_DATA((&TR_MOD, "as1-f2: AS%d PAF_AST_decodeEncode: encoding from ch 0 0x%x. line %d", z, wp, __LINE__));
                TRACE_DATA((&TR_MOD, "as1-f2: [0]: 0x%x, [16]: 0x%x, [99]: 0x%x (ch0)", wp[0], wp[16], wp[99]));
                wp = (int*)pAudioFrame->data.sample[1];
                TRACE_DATA((&TR_MOD, "as1-f2: PAF_AST_decodeEncode: encoding from ch 1 0x%x. line %d", wp, __LINE__));
                TRACE_DATA((&TR_MOD, "as1-f2: [0]: 0x%x, [16]: 0x%x, [99]: 0x%x (ch1)", wp[0], wp[16], wp[99]));
                wp = (int*)pAudioFrame->data.sample[2];
                TRACE_DATA((&TR_MOD, "as1-f2: PAF_AST_decodeEncode: encoding from ch 2 0x%x. line %d", wp, __LINE__));
                TRACE_DATA((&TR_MOD, "as1-f2: [0]: 0x%x, [16]: 0x%x, [99]: 0x%x (ch2)", wp[0], wp[16], wp[99]));
            }
          #endif
          */

            if (enc->fxns->encode)
            {
            	pC->xEnc[z].encodeOutStruct.bypassFlag =
            			pP->z_pEncodeStatus[z]->encBypass;
                if  (errno = enc->fxns->encode (enc, NULL, &pC->xEnc[z].encodeInStruct, &pC->xEnc[z].encodeOutStruct))
                {
                    if (errno != PCEERR_OUTPUT_POINTERNULL)
                    {
                        TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeEncode: return error %d line %d", errno, __LINE__));
                        return errno;
                    }
                }
            /*  #if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
                else
                {
                    int *wp = (int*)pC->xOut[z].pOutBuf->pntr.pVoid;
                    TRACE_DATA((&TR_MOD, "as1-f2: PAF_AST_decodeEncode: encoded to 0x%x. line %d", wp, __LINE__));
                    TRACE_DATA((&TR_MOD, "as1-f2: [0]: 0x%x, [16]: 0x%x, [99]: 0x%x", wp[0], wp[16], wp[99]));
                }
              #endif
              */
            }
        }
        else {
            TRACE_VERBOSE((&TR_MOD, "as1-f2.%d: AS%d: PAF_AST_decodeEncode: processing block %d -- encode <ignored>",
            		__LINE__, as+pP->streamsFromEncodes[z], block));
        }
    }

    // Transmit data
    for (z=OUTPUT1; z < OUTPUTN; z++) {
        // determine encoder associated with this output
        zE = z;
        for (zX = ENCODE1; zX < ENCODEN; zX++) {
            if (pP->outputsFromEncodes[zX] == z) {
                zE = zX;
                break;
            }
        }
        zS = pP->streamsFromEncodes[zE];

        if (pC->xOut[z].hTxSio) {
            TRACE_GEN((&TR_MOD, "as1-f2.%d: AS%d: PAF_AST_decodeEncode: processing block %d -- output", __LINE__, as+zS, block));
            errno = SIO_issue (pC->xOut[z].hTxSio, &pC->xOut[z].outBufConfig, sizeof (pC->xOut[z].outBufConfig), 0);
            if (errno)
            {
                SIO_idle (pC->xOut[z].hTxSio);
                if (errno == 0x105)     // 0x105 == RINGIO_EBUFFULL
                {
//                    statStruct_LogFullRing(STATSTRUCT_AS1_F2);
                    TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeEncode.%d: SIO_idle returned RINGIO_EBUFFULL (0x%x)", __LINE__, errno));
                }
                if (errno > 0)
                {
                    TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeEncode: return error 0x%x line %d", errno, __LINE__));
                    return (ASPERR_ISSUE + (z << 4));
                }
                else if (errno < 0)
                {
                    TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_decodeEncode: return neg error 0x%x line %d", -errno, __LINE__));
                    return -errno; // SIO negates error codes
                }
            }
            if (errno > 0)
                return (ASPERR_ISSUE + (z << 4));
            else if (errno < 0)
                return -errno; // SIO negates error codes
        }
        else {
            TRACE_GEN((&TR_MOD, "as1-f2.%d: AS%d: PAF_AST_decodeEncode: processing block %d -- output <ignored>", __LINE__, as+zS, block));
        }
    }

    return 0;
} //PAF_AST_decodeEncode

// -----------------------------------------------------------------------------
// AST Decoding Function - Frame-Final Processing
//
//   Name:      PAF_AST_decodeFinalTest
//   Purpose:   Decoding Function for determining whether processing of the
//              current frame is complete.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    0 if incomplete, and 1 if complete.
//   Trace:     None.
//

Int
PAF_AST_decodeFinalTest (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int           zMD = pC->masterDec;
    Int  sourceSelect;
    Int sourceProgram;


    sourceSelect  = pC->xDec[zMD].decodeStatus.sourceSelect;
    sourceProgram = pC->xDec[zMD].decodeStatus.sourceProgram;

    if ((sourceSelect == PAF_SOURCE_NONE) || (sourceSelect == PAF_SOURCE_PASS))
        return 1;

    // The following allows for Force modes to switch without command deferral. This might
    // be better suited for inclusion in DIB_requestFrame, but for now will reside here.
    if ((sourceSelect == PAF_SOURCE_SNG) || (sourceSelect > PAF_SOURCE_BITSTREAM)) {
        if (sourceSelect == PAF_SOURCE_DTSALL) {
            if (sourceProgram != PAF_SOURCE_DTS11 &&
                    sourceProgram != PAF_SOURCE_DTS12 &&
                    sourceProgram != PAF_SOURCE_DTS13 &&
                    sourceProgram != PAF_SOURCE_DTS14 &&
                    sourceProgram != PAF_SOURCE_DTS16 &&
                    sourceProgram != PAF_SOURCE_DTSHD)
                return 1;
        }
        else if (sourceSelect == PAF_SOURCE_PCMAUTO) {
            if (sourceProgram != PAF_SOURCE_PCM)
                return 1;
        }
        else {
            if (pC->xDec[zMD].decodeStatus.sourceSelect != pC->xDec[zMD].decodeStatus.sourceDecode)
                return 1;
        }
    }

    return 0;
} //PAF_AST_decodeFinalTest

// -----------------------------------------------------------------------------
// AST Decoding Function - Stream-Final Processing
//
//   Name:      PAF_AST_decodeComplete
//   Purpose:   Decoding Function for terminating the decoding process.
//   From:      AST Parameter Function -> decodeProcessing
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_decodeComplete (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int frame, Int block)
{
    Int as  = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                               /* decode/encode counter */

    (void)as;  // in case not used with tracing disabled.

#ifdef PAF_ASP_FINAL
    /* This material is currently not utilized */
#endif /* PAF_ASP_FINAL */
    for (z=DECODE1; z < DECODEN; z++) {
#ifdef PAF_ASP_FINAL
        DEC_Handle dec = (DEC_Handle )decAlg[z];
#endif /* PAF_ASP_FINAL */
        Int zI = pP->inputsFromDecodes[z];
        if (pC->xInp[zI].hRxSio && pC->xDec[z].decodeStatus.mode) {
            TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeComplete: AS%d: finalizing decode", as+z));
#ifdef PAF_ASP_FINAL
            if (dec->fxns->final)
                dec->fxns->final(dec, NULL, &pC->xDec[z].decodeControl,
                                 &pC->xDec[z].decodeStatus);
#endif /* PAF_ASP_FINAL */
            if (decAlg[z]->fxns->algDeactivate)
                decAlg[z]->fxns->algDeactivate (decAlg[z]);
        }
        else {
            TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeComplete: AS%d: processing decode <ignored>", as+z));
        }
    }

    pP->fxns->streamChainFunction (pP, pQ, pC, PAF_ASP_CHAINFRAMEFXNS_FINAL, 0, frame);

    for (z=ENCODE1; z < ENCODEN; z++) {
        Int zO = pP->outputsFromEncodes[z];
        if (pC->xOut[zO].hTxSio && pC->xEnc[z].encodeStatus.mode) {
            Int select = pC->xEnc[z].encodeStatus.select;
            ALG_Handle encAlg = pC->xEnc[z].encAlg[select];
#ifdef PAF_ASP_FINAL
            ENC_Handle enc = (ENC_Handle )encAlg;
#endif /* PAF_ASP_FINAL */
            TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeComplete: AS%d: finalizing encode", as+z));
#ifdef PAF_ASP_FINAL
            if (enc->fxns->final)
                enc->fxns->final(enc, NULL, &pC->xEnc[z].encodeControl,
                                 &pC->xEnc[z].encodeStatus);
#endif /* PAF_ASP_FINAL */
            if (encAlg->fxns->algDeactivate)
                encAlg->fxns->algDeactivate (encAlg);
        }
        else {
            TRACE_VERBOSE((&TR_MOD, "PAF_AST_decodeComplete: AS%d: finalizing encode <ignored>", as+z));
        }
    }

    // wait for remaining data to be output
    pP->fxns->stopOutput (pP, pQ, pC);

    return 0;
} //PAF_AST_decodeComplete

// -----------------------------------------------------------------------------
// AST Selection Function - Device Selection
//
//   Name:      PAF_AST_selectDevices
//   Purpose:   Audio Stream Task Function for selecting the devices used
//              for input and output.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_selectDevices (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* input/output counter */
    Int errno = 0;                      /* error number */
    Int errme;                          /* error number, local */
    Int device;
    Int zMD = pC->masterDec;

    (void)as;  // in case not used with tracing disabled.

    // Select output devices
    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if ((device = pC->xOut[z].outBufStatus.sioSelect) >= 0) {
            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_selectDevices: AS%d: output device %d selecting ...", as+z, device));

            /* check for valid index into device array */
            if (device >= pQ->devout->n)
                device = 0; /* treat as device None */

            errme = pP->fxns->deviceSelect (&pC->xOut[z].hTxSio, SIO_OUTPUT, HEAP_OUTBUF, (Ptr )pQ->devout->x[device]);
            if (errme)
            {
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_selectDevices.%d: errme 0x%x, errno 0x%x", __LINE__, errme, errno));
                if (! errno)
                    errno = ASPERR_DEVOUT + errme;
                pC->xOut[z].outBufStatus.sioSelect = 0x80;
            }
            else {
                Int zE;

                pC->xOut[z].outBufStatus.sioSelect = device | 0x80;
                // register outBufStatus and encodeStatus pointers with output devices
                // This enables proper IEC encapsulation.
                if (pC->xOut[z].hTxSio) {
                    // set max # of output buffers (use override if necessary)
                    if (pC->xOut[z].outBufStatus.maxNumBufOverride == 0)
                        SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_MAX_NUMBUF,(Arg) pP->poutNumBufMap[z]->maxNumBuf);
                    else
                        SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_MAX_NUMBUF,(Arg) pC->xOut[z].outBufStatus.maxNumBufOverride);

                    // register PAF_SIO_IALG object address
                    SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_IALGADDR,(Arg) pC->xOut[z].outChainData.head->alg);
                    SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_BUFSTATUSADDR, (Arg) &pC->xOut[z].outBufStatus);
                    for (zE=ENCODE1; zE < ENCODEN; zE++) {
                        if (pP->outputsFromEncodes[zE] == z) {
                            SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_ENCSTATUSADDR, (Arg) &pC->xEnc[zE].encodeStatus);
                            break;
                        }
                    }
                }
            }
        }

        // if device selected and valid then enable stat tracking if
        // required and start clocking
        if ((pC->xOut[z].outBufStatus.sioSelect < 0) && (pC->xOut[z].hTxSio)) {

            // enable status tracking (needed for ARC)
            if (pC->xOut[z].outBufStatus.rateTrackMode)
            {
                Int outputRate = (pC->xOut[z].outBufStatus.sampleRate == PAF_SAMPLERATE_UNKNOWN ? \
                                  PAF_SAMPLERATE_48000HZ : pC->xOut[z].outBufStatus.sampleRate);
                float rateO = pP->pAudioFrameFunctions->sampleRateHz (NULL, outputRate, PAF_SAMPLERATEHZ_STD);
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_selectDevices.%d: enable output status tracking (needed for ARC)", __LINE__));
                errno = SIO_ctrl (pC->xOut[z].hTxSio, (Uns) PAF_SIO_CONTROL_ENABLE_STATS, rateO);
                if (errno)
                {
                    TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_selectDevices.%d, errno 0x%x", __LINE__, errno));
                    return errno;
                }
            }
            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_selectDevices.%d: start SIO clocks", __LINE__));
            errme = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_OUTPUT_START_CLOCKS, 0);
            if (errme)
            {
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_selectDevices.%d: errme 0x%x, errno 0x%x", __LINE__, errme, errno));
                SIO_idle (pC->xOut[z].hTxSio);
                if (!errno)
                    errno = ASPERR_DEVOUT + errme;
            }
        }
    }

    // Select input devices
    for (z=INPUT1; z < INPUTN; z++) {
        if ((device = pC->xInp[z].inpBufStatus.sioSelect) >= 0) {
            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_selectDevices: AS%d: input device %d selecting ...", as+z, device));

            // check for valid index into device array
            if (device >= pQ->devinp->n)
                device = 0; /* treat as device None */

            errme = pP->fxns->deviceSelect (&pC->xInp[z].hRxSio, SIO_INPUT,
                                            HEAP_INPBUF, (Ptr )pQ->devinp->x[device]);

            if (errme)
            {
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_selectDevices.%d: errme 0x%x, errno 0x%x", __LINE__, errme, errno));
                if (! errno)
                    errno = ASPERR_DEVINP + errme;
                pC->xInp[z].inpBufStatus.sioSelect = 0x80;
            }
            else {
                pC->xInp[z].inpBufStatus.sioSelect = device | 0x80;
                // register decodeStatus pointer with input devices
                // This allows, e.g., autoProcessing to exit when sourceSelect = none
                // Use zMIs decodeStatus for all inputs
                if (pC->xInp[z].hRxSio) {
                    // register PAF_SIO_IALG object address
                    SIO_ctrl (pC->xInp[z].hRxSio, PAF_SIO_CONTROL_SET_IALGADDR,(Arg) pC->xInp[z].inpChainData.head->alg);
                    SIO_ctrl (pC->xInp[z].hRxSio, PAF_SIO_CONTROL_SET_DECSTATUSADDR, (Arg) &pC->xDec[zMD].decodeStatus);
                }
            }
        }

        // enable status tracking (needed for ARC)
        if (pC->xInp[z].inpBufStatus.rateTrackMode && (pC->xInp[z].inpBufStatus.sioSelect < 0) && (pC->xInp[z].hRxSio))
        {
            Int inputRate;
            float rateI;

            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_selectDevices.%d: enable input status tracking (needed for ARC)", __LINE__));
            errno = pP->fxns->updateInputStatus (pC->xInp[z].hRxSio,&pC->xInp[z].inpBufStatus, &pC->xInp[z].inpBufConfig);
            if (errno)
            {
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_selectDevices.%d: errno 0x%x", __LINE__, errno));
                return errno;
            }
            inputRate = (pC->xInp[z].inpBufStatus.sampleRateStatus == PAF_SAMPLERATE_UNKNOWN ? \
                            PAF_SAMPLERATE_48000HZ : pC->xInp[z].inpBufStatus.sampleRateStatus);

            rateI = pP->pAudioFrameFunctions->sampleRateHz (NULL, inputRate, PAF_SAMPLERATEHZ_STD);
            errno = SIO_ctrl (pC->xInp[z].hRxSio, (Uns) PAF_SIO_CONTROL_ENABLE_STATS, rateI);
            if (errno)
            {
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_selectDevices.%d: errno 0x%x", __LINE__, errno));
                return errno;
            }
        }
    }

    return errno;
} //PAF_AST_selectDevices

// -----------------------------------------------------------------------------
// AST Selection Function - Source Selection
//
//   Name:      PAF_AST_sourceDecode
//   Purpose:   Audio Stream Task Function for selecting the sources used
//              for decoding of input to output.
//   From:      audioStream1Task or equivalent
//   Uses:      See code.
//   States:    x
//   Return:    0.
//   Trace:     None.
//

Int
PAF_AST_sourceDecode (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int x)
{
    Int z;                              /* decode counter */

    for (z=DECODE1; z < DECODEN; z++)
        if (pC->xDec[z].decodeStatus.mode) {
            pC->xDec[z].decodeStatus.sourceDecode = x;
        }
    return 0;
} //PAF_AST_sourceDecode

// -----------------------------------------------------------------------------
// AST Decoding Function Helper - SIO Driver Start
//
//   Name:      PAF_AST_startOutput
//   Purpose:   Decoding Function for initiating output.
//   From:      AST Parameter Function -> decodeInfo1
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//              * SIO control errors.
//
#define DEC_OUTNUMBUF_MAP(X) \
      pP->poutNumBufMap[z]->map[(X) >= pP->poutNumBufMap[z]->length ? 0 : (X)]

Int
PAF_AST_startOutput (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, double arcRatio) 
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* output counter */
    Int errno,nbufs;                    /* error number */
    Int zE, zS, zX;
    Int zMD = pC->masterDec;
    PAF_SIO_IALG_Obj    *pObj;
    PAF_SIO_IALG_Config *pAlgConfig;


    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (pC->xOut[z].hTxSio) {

            // determine associated encoder and stream
            zE = z;
            zS = z;
            for (zX = ENCODE1; zX < ENCODEN; zX++) {
                if (pP->outputsFromEncodes[zX] == z) {
                    zE = zX;
                    zS = pP->streamsFromEncodes[zE];
                    break;
                }
            }

            // Set sample count so that DOB knows how much data to send

            // if estimating output rate then assume be need the initial estimate
            // of framelength needs to be close, otherwise there will be an IO error
            // shortly after startOutput. Specifically, we start the output with
            // two buffers with size based on the arc ratio seed.
            if (pC->xOut[z].outBufStatus.rateTrackMode) {
                pC->xOut[z].outBufConfig.lengthofFrame =
                    pC->xEnc[zE].encodeInStruct.pAudioFrame->sampleCount * arcRatio;
            }
            else {
                pC->xOut[z].outBufConfig.lengthofFrame =
                    pC->xEnc[zE].encodeInStruct.pAudioFrame->sampleCount;
            }

            if (pC->xOut[z].outBufStatus.markerMode == PAF_OB_MARKER_ENABLED) {
                pObj = (PAF_SIO_IALG_Obj *) pC->xOut[z].outChainData.head->alg;
                pAlgConfig = &pObj->config;
                memset (pC->xOut[z].outBufConfig.base.pVoid, 0xAA, pAlgConfig->pMemRec[0].size);
            }

            // The index to DEC_OUTNUMBUF_MAP will always come from the primary/master
            // decoder. How should we handle the sourceProgram for multiple decoders?
            // Override as needed
            nbufs = DEC_OUTNUMBUF_MAP(pC->xDec[zMD].decodeStatus.sourceProgram);
            if (pC->xOut[z].outBufStatus.numBufOverride[pC->xDec[zMD].decodeStatus.sourceProgram] > 0)
                nbufs = pC->xOut[z].outBufStatus.numBufOverride[pC->xDec[zMD].decodeStatus.sourceProgram];
            SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_NUMBUF, nbufs);

            if (errno = SIO_issue (pC->xOut[z].hTxSio,
                                   &pC->xOut[z].outBufConfig, sizeof(pC->xOut[z].outBufConfig), 0)) {
                SIO_idle (pC->xOut[z].hTxSio);
                TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_startOutput: AS%d: SIO_issue failed (0x%x)", as+zS, errno));
                return errno;
            }

            if (! (pC->xOut[z].outBufStatus.audio & 0xf0)
                && (errno =  SIO_ctrl (pC->xOut[z].hTxSio,
                                       PAF_SIO_CONTROL_UNMUTE, 0))) {
                errno = (errno & 0xff) | ASPERR_MUTE;
                /* convert to sensical errno */
                TRACE_TERSE((&TR_MOD, "as1-f2: PAF_AST_startOutput: AS%d: SIO control failed (unmute) 0x%x", as+zS, errno));
                return (errno);
            }
            else
                pC->xOut[z].outBufStatus.audio
                    = (pC->xOut[z].outBufStatus.audio & 0xf0) | PAF_OB_AUDIO_SOUND;

            TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_startOutput: AS%d: output started", as+zS ));
        }
    }

    return 0;
} //PAF_AST_startOutput

// -----------------------------------------------------------------------------
// AST Decoding Function Helper - SIO Driver Stop
//
//   Name:      PAF_AST_stopOutput
//   Purpose:   Decoding Function for terminating output.
//   From:      AST Parameter Function -> decodeProcessing
//              AST Parameter Function -> decodeComplete
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard or SIO form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * SIO control errors.
//

Int
PAF_AST_stopOutput (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* output counter */
    Int errno = 0, getVal;
    Int zS, zX;
    PAF_SIO_IALG_Obj    *pObj;
    PAF_SIO_IALG_Config *pAlgConfig;

    (void)as;  // in case not used with tracing disabled.

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (pC->xOut[z].hTxSio) {

            // determine associated encoder and stream
            zS = z;
            (void)zS;
            for (zX = ENCODE1; zX < ENCODEN; zX++) {
                if (pP->outputsFromEncodes[zX] == z) {
                    zS = pP->streamsFromEncodes[zX];
                    break;
                }
            }

            // Mute output before audio data termination in the usual case,
            // where such termination is due to decode error or user command.
            // Identification of this as the usual case is provided by the
            // "decode processing" state machine.

            if (! (pC->xOut[z].outBufStatus.audio & 0xf0)
                && (pC->xOut[z].outBufStatus.audio & 0x0f) == PAF_OB_AUDIO_SOUND
                && (getVal = SIO_ctrl (pC->xOut[z].hTxSio,
                                       PAF_SIO_CONTROL_MUTE, 0))) {
                if (! errno) {
                    errno = (getVal & 0xff) | ASPERR_MUTE;
                    /* convert to sensical errno */
                }
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_stopOutput:  AS%d: SIO control failed (mute)", as+zS));
            }

            TRACE_TIME((&TIME_MOD, "... + %d = %d (stopOutput -- begin PAF_SIO_CONTROL_IDLE)", dtime(), TSK_time()));

            // Terminate audio data output, truncating (ignore) or flushing
            // (play out) final samples as per (1) control register set by
            // the user and (2) the type of audio data termination:

#if 0
            // This form is not used because driver support for truncating
            // data is not supported for internal clocks, although it is
            // for external clocks.
            getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_IDLE,
                               pC->xOut[z].outBufStatus.flush
                               & (pC->xOut[z].outBufStatus.audio & 0x0f) == PAF_OB_AUDIO_FLUSH
                               ? 1 : 0);
            /* UNTESTED */
#else
            // This form should be used when driver support for truncating
            // data is supported for both internal and external clocks.
            getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_IDLE,
                               pC->xOut[z].outBufStatus.flush ? 1 :
                               (pC->xOut[z].outBufStatus.audio & 0x0f) == PAF_OB_AUDIO_FLUSH
                               ? 1 : 0);
            /* TESTED */
#endif

            TRACE_TIME((&TIME_MOD, "... + %d = %d (stopOutput -- after PAF_SIO_CONTROL_IDLE)", dtime(), TSK_time()));

            if (! errno)
                errno = getVal;

            // Mute output after audio data termination in a special case,
            // where such termination is due to processing of a final frame
            // or user command. Identification of this as a special case is
            // provided by the "decode processing" state machine.

            if (! (pC->xOut[z].outBufStatus.audio & 0xf0)
                && (pC->xOut[z].outBufStatus.audio & 0x0f) == PAF_OB_AUDIO_FLUSH
                && (getVal = SIO_ctrl (pC->xOut[z].hTxSio,
                                       PAF_SIO_CONTROL_MUTE, 0))) {
                if (! errno) {
                    errno = (getVal & 0xff) | ASPERR_MUTE;
                    /* convert to sensical errno */
                }
                TRACE_VERBOSE((&TR_MOD, "as1-f2: PAF_AST_stopOutput:  AS%d: SIO control failed (mute)", as+zS));
            }

            pC->xOut[z].outBufStatus.audio &= ~0x0f;

            // zero output buffers
            pObj = (PAF_SIO_IALG_Obj *) pC->xOut[z].outChainData.head->alg;
            pAlgConfig = &pObj->config;
            memset (pC->xOut[z].outBufConfig.base.pVoid, 0, pAlgConfig->pMemRec[0].size);
        } //pC->xOut[z].hTxSio
    }//OUTPUT

    return (errno);
} //PAF_AST_stopOutput

// -----------------------------------------------------------------------------
// AST Decoding Function Helper - SIO Driver Change
//
//   Name:      PAF_AST_setCheckRateX
//   Purpose:   Decoding Function for reinitiating output.
//   From:      AST Parameter Function -> decodeInfo1
//              AST Parameter Function -> decodeInfo2
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     None.
//

/* 0: set, 1: check, unused for now. --Kurt */
Int
PAF_AST_setCheckRateX (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int check)
{
    float rateX;
    PAF_SampleRateHz rateO /* std */, rateI /* inv */;
    Int z;                              /* output counter */
    Int zx;                             /* output re-counter */
    Int getVal;
    int inputRate, inputCount, outputRate, outputCount;
    Int zMD = pC->masterDec;
    Int zMI = pP->zone.master;
    Int zMS = pC->masterStr;
    Int zE, zX;


    inputRate = pC->xInp[zMI].inpBufStatus.sampleRateStatus;
    inputCount = pC->xDec[zMD].decodeStatus.frameLength;
    rateI = pC->xStr[zMS].pAudioFrame->fxns->sampleRateHz
        (pC->xStr[zMS].pAudioFrame, inputRate, PAF_SAMPLERATEHZ_INV);

    for (z=OUTPUT1; z < OUTPUTN; z++) {
        if (pC->xOut[z].hTxSio && (pC->xOut[z].outBufStatus.clock & 0x01)) {

            // determine associated encodr
            zE = z;
            for (zX = ENCODE1; zX < ENCODEN; zX++) {
                if (pP->outputsFromEncodes[zX] == z) {
                    zE = zX;
                    break;
                }
            }

            outputRate = pC->xEnc[zE].encodeStatus.sampleRate;
            outputCount = pC->xEnc[zE].encodeStatus.frameLength;
            rateO = pC->xStr[zMS].pAudioFrame->fxns->sampleRateHz
                (pC->xStr[zMS].pAudioFrame, outputRate, PAF_SAMPLERATEHZ_STD);
            if (rateI > 0 && rateO > 0)
                rateX = rateO /* std */ * rateI /* inv */;
            else if (inputCount != 0)
                rateX = (float )outputCount / inputCount;
            else
                return ( ASPERR_INFO_RATERATIO );

            getVal = SIO_ctrl (pC->xOut[z].hTxSio, PAF_SIO_CONTROL_SET_RATEX, (Arg) &rateX);
            if (getVal == DOBERR_RATECHANGE) {
                for (zx=OUTPUT1; zx < OUTPUTN; zx++)
                    if (pC->xOut[zx].hTxSio)
                        SIO_idle (pC->xOut[zx].hTxSio);

                // this forces an exit from the calling state machine which will
                // eventually call startOutput which calls setCheckRateX for all outputs
                // and so it is ok, in the presence of a rate change on any output, to
                // exit this loop /function early.
                return ASPERR_INFO_RATECHANGE;
            }
            else if( getVal != SYS_OK )
                return ((getVal & 0xff) | ASPERR_RATE_CHECK);
        }
    }

    return (0);
} //PAF_AST_setCheckRateX

// -----------------------------------------------------------------------------
// AST Decoding Function Helper - Chain Processing
//
//   Name:      PAF_AST_streamChainFunction
//   Purpose:   Common Function for processing algorithm chains.
//   From:      AST Parameter Function -> decodeInfo1
//              AST Parameter Function -> decodeStream
//              AST Parameter Function -> decodeComplete
//   Uses:      See code.
//   States:    x
//   Return:    Error number in standard form (0 on success).
//   Trace:     Message Log "trace" in Debug Project Configuration reports:
//              * State information as per parent.
//

Int
PAF_AST_streamChainFunction (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, Int iChainFrameFxns, Int abortOnError, Int logArg)
{
    Int as = pC->as;                    /* Audio Stream Number (1, 2, etc.) */
    Int z;                              /* stream counter */
    Int errno;                          /* error number */
    Int dFlag, eFlag, gear;
    Int zX;
    Int zS;

    (void)as;

    for (zS = STREAM1; zS < STREAMN; zS++)
    {
        z = pP->streamOrder[zS];  // Select stream order from streamOrder parameter - MID 788

        // apply stream
        //      unless the stream is associated with a decoder and it is not running
        // or
        //      unless the stream is associated with an encoder and it is not running
        // Also gear control only works for streams with an associated decoder
        // if no such association exists then gear 0 (All) is used
        dFlag = 1;
        gear = 0;
        for (zX = DECODE1; zX < DECODEN; zX++) {
            if (pP->streamsFromDecodes[zX] == z) {
                dFlag = pC->xDec[zX].decodeStatus.mode;
                gear = pC->xDec[zX].decodeStatus.aspGearStatus;
                break;
            }
        }
        eFlag = 1;
        for (zX = ENCODE1; zX < ENCODEN; zX++) {
            if (pP->streamsFromEncodes[zX] == z) {
                eFlag = pC->xEnc[zX].encodeStatus.mode;
                break;
            }
        }

        if (dFlag && eFlag) {
            PAF_ASP_Chain *chain = pC->xStr[z].aspChain[gear];
            PAF_AudioFrame *frame = pC->xStr[z].pAudioFrame;
            Int (*func) (PAF_ASP_Chain *, PAF_AudioFrame *) =
                chain->fxns->chainFrameFunction[iChainFrameFxns];

            TRACE_GEN((&TR_MOD,
                       iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_RESET
                       ? "AS%d: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (reset)"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_APPLY
                       ? "AS%d: PAF_AST_streamChainFunction.%d: processing block %d -- audio stream (apply)"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_FINAL
                       ? "AS%d: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (final)"
                       : "AS%d: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (?????)",
                       as+z, __LINE__, logArg));
            errno = (*func) (chain, frame);
            TRACE_VERBOSE((&TR_MOD, "AS%d: PAF_AST_streamChainFunction.%d: errno 0x%x.",
                           as+z, __LINE__, errno));

            if (errno && abortOnError)
                return errno;
        }
        else {
            TRACE_GEN((&TR_MOD,
                       iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_RESET
                       ? "AS%d: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (reset) <ignored>"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_APPLY
                       ? "AS%d: PAF_AST_streamChainFunction.%d: processing block %d -- audio stream (apply) <ignored>"
                       : iChainFrameFxns == PAF_ASP_CHAINFRAMEFXNS_FINAL
                       ? "AS%d: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (final) <ignored>"
                       : "AS%d: PAF_AST_streamChainFunction.%d: processing frame %d -- audio stream (?????) <ignored>",
                       as+z, __LINE__, logArg));
        }

        /*
        {
            void dp_tracePAF_Data(float *lBuf, float *rBuf, int count);
            PAF_AudioFrameData *afd;
            float ** afPtr;

            afd = &(pC->xStr->pAudioFrame->data);
            afPtr = (float**)afd->sample;
            dp_tracePAF_Data(afPtr[4], afPtr[5], 256);

        }
        */

    }

    return 0;
} //PAF_AST_streamChainFunction

// -----------------------------------------------------------------------------
#ifdef TRACE_ARC_RATIO
#define ARC_TRACE_LENGTH    512
float TX_DV_START_LOG[ARC_TRACE_LENGTH];
float RX_DV_START_LOG[ARC_TRACE_LENGTH];
Int DV_START_LOG_index=0;

float TX_DV_LOG[ARC_TRACE_LENGTH];
float RX_DV_LOG[ARC_TRACE_LENGTH];
Int DV_LOG_index=0;
#endif // TRACE_ARC_RATIO

Int
PAF_AST_computeRateRatio (const PAF_AST_Params *pP, PAF_AST_Config *pC, 
                          double *arcRatio,  // computes this
                          Int frame)  // used to identify startup case
{
    PAF_SIO_Stats *pRxStats, *pTxStats;
    Int errno;
    double arcRatio1, inputRateRatio, streamRateRatio;
    Int inputRate, streamRate, outputRate;
    float rateI, rateS, rateO;

    arcRatio1 = 1.0;  // default
    *arcRatio = arcRatio1;

    // need valid rx and tx and both need to have stats tracking enabled
    if (!pC->xInp[0].hRxSio || !pC->xOut[0].hTxSio)
        return 0;
    if (!pC->xInp[0].inpBufStatus.rateTrackMode || !pC->xOut[0].outBufStatus.rateTrackMode)
        return 0;

    errno = SIO_ctrl (pC->xInp[0].hRxSio, PAF_SIO_CONTROL_GET_STATS, (Arg) &pRxStats);
    if (errno) {
        TRACE_TERSE((&TR_MOD, "as1-f2: Error 0x%x, retrieving Rx xfer stats", errno));
        return errno;
    }

    errno = SIO_ctrl (pC->xOut[0].hTxSio, PAF_SIO_CONTROL_GET_STATS, (Arg) &pTxStats);
    if (errno) {
        TRACE_TERSE((&TR_MOD, "as1-f2: Error %0x%x, retrieving Tx xfer stats", errno));
        return errno;
    }

    // compute nominal rate ratio for reference.
    inputRate = (pC->xInp[0].inpBufStatus.sampleRateStatus == PAF_SAMPLERATE_UNKNOWN ? \
                     PAF_SAMPLERATE_48000HZ : pC->xInp[0].inpBufStatus.sampleRateStatus);
    rateI = pP->pAudioFrameFunctions->sampleRateHz (NULL, inputRate, PAF_SAMPLERATEHZ_STD);

    // rely on rate in the frame, as decoder or SRC could have changed it.
    // This is hard coded to use stream zero. Might have to be changed in a more complicated system...
    streamRate = (pC->xStr[0].pAudioFrame->sampleRate == PAF_SAMPLERATE_UNKNOWN ? \
                     PAF_SAMPLERATE_48000HZ : pC->xStr[0].pAudioFrame->sampleRate);
    rateS = pP->pAudioFrameFunctions->sampleRateHz (NULL, streamRate, PAF_SAMPLERATEHZ_STD);

    outputRate = (pC->xOut[0].outBufStatus.sampleRate == PAF_SAMPLERATE_UNKNOWN ? \
                      PAF_SAMPLERATE_48000HZ : pC->xOut[0].outBufStatus.sampleRate);
    rateO = pP->pAudioFrameFunctions->sampleRateHz (NULL, outputRate, PAF_SAMPLERATEHZ_STD);

    inputRateRatio  = rateO / rateI;
    streamRateRatio = rateO / rateS;
    TRACE_ARC((&TR_MOD, "as1-f2: inputRateRatio %f. streamRateRatio: %f.  rateI: %f. rateS: %f. rateO: %f.\n", 
       inputRateRatio, streamRateRatio, rateI, rateS, rateO));

    if ((pRxStats->dpll.dv == 0) || (pTxStats->dpll.dv == 0) || (frame < 6)) 
    { // To ensure stable startup, use sampleRate ratio for first n frames
        arcRatio1 = streamRateRatio;
        TRACE_ARC((&TR_MOD, "as1-f2: arcRatio %f. rateI: %f.  rateO: %f\n", arcRatio1, rateI, rateO));
        TRACE_GEN((&TR_MOD, "as1-f2: arcRatio1 %d.%07d due to nominal rates.",
                       (int) arcRatio1, (int) (1.e7 * (arcRatio1 - (int) arcRatio1))));
    }
    else
    {
        arcRatio1 = pRxStats->dpll.dv / pTxStats->dpll.dv;
        TRACE_ARC((&TR_MOD, "as1-f2: arcRatio %f. RxDv: %e.  TxDv: %e\n", arcRatio1, pRxStats->dpll.dv, pTxStats->dpll.dv));
        TRACE_GEN((&TR_MOD, "as1-f2: arcRatio1 %d.%07d due to dv.",
                       (int) arcRatio1, (int) (1.e7 * (arcRatio1 - (int) arcRatio1))));

        if (inputRateRatio  != streamRateRatio)
        {  // implies rate change happened, or HDMI input of coded data
            arcRatio1 = arcRatio1/inputRateRatio;
            TRACE_ARC((&TR_MOD, "as1-f2: Adjusted arcRatio by inputRateRatio.  %f.\n", arcRatio1));
        }
    }

#ifdef TRACE_ARC_RATIO
    // trace buffers
    if (DV_START_LOG_index<ARC_TRACE_LENGTH) {
        RX_DV_START_LOG[DV_START_LOG_index  ]=(float)pRxStats->dpll.dv;
        TX_DV_START_LOG[DV_START_LOG_index++]=(float)pTxStats->dpll.dv;
    }
    if (DV_LOG_index==ARC_TRACE_LENGTH) DV_LOG_index=0;
    RX_DV_LOG[DV_LOG_index  ]=(float)pRxStats->dpll.dv;
    TX_DV_LOG[DV_LOG_index++]=(float)pTxStats->dpll.dv;
#endif // TRACE_ARC_RATIO

    // Tolerance allowed around AS_Output sample rate / AS_Input sample rate ratio;
    // This is used as a workaround for observed deviations in RX PLL dv for startup
    // and re-startup of AS_Input timing.  If tolerance is exceeded, previous ("last")
    // valid ratio is used, i.e. previous block's ratio.
    // Value of this symbol should be 0.00 <= n <= 0.50, otherwise no action is taken.
    // Tested with value of 0.03 (+/- 3%).
    // 6% is a half step.  .5% should not be noticable to anyone.
    #define ARC_RATIO_TOLERANCE     0.03    // +/- percentage deviation allowed
    // compare measured ratio against nominal ratio
    if ((arcRatio1 < (streamRateRatio*(1.0 - ARC_RATIO_TOLERANCE))) ||
        (arcRatio1 > (streamRateRatio*(1.0 + ARC_RATIO_TOLERANCE))))
    {   // Typical reason for being out of tolerance is that RX PLL dv value
        // has deviated, observed at such as AC3 6 block boundaries and
        // startup/re-start of AS_Input compressed bitstream.
        TRACE_ARC((&TR_MOD, "as1-f2: arcRatio %f exceeds ARC_RATIO_TOLERANCE.  Forcing nominal %f.\n", arcRatio1, streamRateRatio));
        TRACE_TERSE((&TR_MOD, "AS_InputB: arcRatio exceeds ARC_RATIO_TOLERANCE.  Using nominal."));
        arcRatio1 = streamRateRatio;
    }
    *arcRatio = arcRatio1;
    return 0;
} //PAF_AST_computeRateRatio

// -----------------------------------------------------------------------------

Int
PAF_AST_decodeHandleErrorInput (const PAF_AST_Params *pP, const PAF_AST_Patchs *pQ, PAF_AST_Config *pC, ALG_Handle decAlg[], Int z, Int error)
{
    Int errno = 0;
    Int zMD = pC->masterDec;
    Int zI = pP->inputsFromDecodes[z];

    // only handle real errors, on primary input, for writeDECModeContinuous
    if ( !( error &&
            z == zMD &&
            pC->xDec[z].decodeStatus.mode & DEC_MODE_CONTINUOUS ))
        return error;

    TRACE_TIME((&TIME_MOD, "AS%d: PAF_AST_decodeHandleErrorInput: (primary) input error caught = %d", pC->as+z, error));
    TRACE_TIME((&TIME_MOD, "AS%d: PAF_AST_decodeHandleErrorInput: old sourceProgram = %d", pC->as+z, pC->xDec[z].decodeStatus.sourceProgram));

    if (pC->xInp[zI].hRxSio) {
        DEC_Handle dec;

        if (errno = SIO_idle (pC->xInp[zI].hRxSio))
            return errno;

        // indicates (primary) input not running
        pC->xDec[z].decodeStatus.sourceDecode = PAF_SOURCE_NONE;

        pC->xInp[zI].inpBufConfig.deliverZeros = 1;

        // will be changed after next reclaim, to PAF_SOURCE_UNKNOWN or other
        pC->xDec[z].decodeStatus.sourceProgram = PAF_SOURCE_NONE;

        if (decAlg[z]->fxns->algDeactivate)
            decAlg[z]->fxns->algDeactivate (decAlg[z]);

        decAlg[z] = pC->xDec[z].decAlg[PAF_SOURCE_PCM];
        dec = (DEC_Handle )decAlg[z];

        TRACE_TIME((&TIME_MOD, "AS%d: PAF_AST_decodeHandleErrorInput: resetting to PCM decoder",
                    pC->as+z));

        if (decAlg[z]->fxns->algActivate)
            decAlg[z]->fxns->algActivate (decAlg[z]);

        if (dec->fxns->reset
            && (errno = dec->fxns->reset (dec, NULL,
                                          &pC->xDec[z].decodeControl, &pC->xDec[z].decodeStatus)))
            return errno;
    }

    return errno;
} //PAF_AST_decodeHandleErrorInput

#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)

//------------------------------------------------------------------------------
static int sSkipCount = 0;
int gReportBuffers = 0;
void as_traceChannels(PAF_AST_Config *pC, int z)
{
    PAF_AudioFrame *pAudioFrame = pC->xDec[z].decodeInStruct.pAudioFrame;
    int i;
         
// #ifdef THIS_IS_DSPA
    sSkipCount++;
    if (sSkipCount<1)
    	return;
    sSkipCount = 0;
// #endif

#ifdef THIS_IS_DSPB
    if (!gReportBuffers)
        return;
    gReportBuffers = 0;
#endif

    dp(NULL, "\n");
    for (i=0; i<PAF_MAXNUMCHAN; i++)
    {
    	if (pAudioFrame->data.sample[i] != 0)
    	{
    	    float *wp = (float*)pAudioFrame->data.sample[i];
   	    	dp(NULL, "i: %d.  p: 0x%x.  %f, %f, %f, %f\n",
   	    			i, pAudioFrame->data.sample[i], wp[0], wp[1], wp[2], wp[3]);
    	}
    }
}
#endif

// .............................................................................
// Audio Stream Task Parameter Functions - PA17
//
//   Name:      PAF_AST_params_fxnsPA17
//   Purpose:   Collect the functions that embody the implementation of
//              Audio Framework Number 2 for use as a jump table.
//   From:      PAF_AST_Params
//   Uses:      See contents.
//   States:    N.A.
//   Return:    N.A.
//   Trace:     None.
//

const PAF_AST_Fxns PAF_AST_params_fxnsPA17 =
{
    {   // initPhase[8]
        PAF_AST_initPhaseMalloc,
        PAF_AST_initPhaseConfig,
        PAF_AST_initPhaseAcpAlg,
        PAF_AST_initPhaseCommon,
        PAF_AST_initPhaseAlgKey,
        PAF_AST_initPhaseDevice,
        NULL,
        NULL,
    },
    PAF_AST_initFrame0,             // initFrame0
    PAF_AST_initFrame1,             // initFrame1
    NULL, /* PAF_AST_passProcessing, */     // passProcessing        
    NULL, /* PAF_AST_passProcessingCopy, */ // passProcessingCopy    
    PAF_AST_autoProcessing,         // autoProcessing
    PAF_AST_decodeProcessing,       // decodeProcessing
    PAF_AST_decodeCommand,          // decodeCommand
    PAF_AST_encodeCommand,          // encodeCommand
    PAF_AST_decodeInit,             // decodeInit
    PAF_AST_decodeInfo,             // decodeInfo
    PAF_AST_decodeInfo1,            // decodeInfo1
    PAF_AST_decodeInfo2,            // decodeInfo2
    PAF_AST_decodeCont,             // decodeCont
    PAF_AST_decodeDecode,
    PAF_AST_decodeStream,           // decodeStream
    PAF_AST_decodeEncode,           // decodeEncode
    PAF_AST_decodeFinalTest,        // decodeFinalTest
    PAF_AST_decodeComplete,         // decodeComplete
    PAF_AST_selectDevices,          // selectDevices
    PAF_AST_sourceDecode,           // sourceDecode
    PAF_AST_startOutput,            // startOutput
    PAF_AST_stopOutput,             // stopOutput
    PAF_AST_setCheckRateX,          // setCheckRateX
    PAF_AST_streamChainFunction,    // streamChainFunction
    PAF_DEC_deviceAllocate,         // deviceAllocate
    PAF_DEC_deviceSelect,           // deviceSelect
    PAF_DEC_computeFrameLength,     // computeFrameLength
    PAF_DEC_updateInputStatus,      // updateInputStatus
    NULL, /* PAF_BUF_copy, */       // copy
    NULL, /*headerPrint*/           // headerPrint
    NULL, /*allocPrint*/            // allocPrint
    NULL, /*commonPrint*/           // commonPrint
    NULL, /*bufMemPrint*/           // bufMemPrint
    NULL, /*memStatusPrint*/        // memStatusPrint
    // For ARC                      
    PAF_ARC_controlRate,            // controlRate
};

// .............................................................................

// EOF
