/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/**
 *  @file  dmtest.c
 *
 *  @brief dm test application main code.
 *
 *          This is a dm test application.
 *          The application test the various implementation of dm.
 *
 *          It expects the input from a wave file and generate the output
 *          wave file after sampling rate conversion.
 *
 *          The dm parameters are recieved from external tables.
 *          The test application will configure the neccessary parameters
 *          and allocate memory for dm input/output buffer
 *          and dm memory states.
 *****************************************************************************/

/************************************************************
                     Include files
*************************************************************/
#ifndef __TESTAPP_H__
#define __TESTAPP_H__

#include <xdc/std.h>
#include <pafhjt.h>
#include <stdasp.h>
#include <paftyp_a.h>

/************************************************************
                     Include files
*************************************************************/
#include <stdio.h>
#include <math.h>
#include <alg.h>
#include <stdlib.h>
#include <string.h>
#include "paftyp.h"
#include "procsdk_audio_typ.h"
#define MAX_CHAR 512

typedef struct test_case_s{
    char charTestCaseName[MAX_CHAR];/* Test case name*/
    PAF_ChannelConfiguration channelConfigurationStream;/* Ip configuration*/
    char charInFileName[MAX_CHAR];/* Input audio file name */
    char charOutFileName[MAX_CHAR];/* Output audio file name */
    int scaleVolume;/* Volume in dB*/
    int numOfInChannels;/* Number of input channels*/
    int numOfOutChannels;/* Number of output channels*/
    PAF_ChannelConfiguration channelConfigurationRequest;/* Op configuration*/
    int samsize_enable;/* Fixed point to floating point conversion*/
} test_case;


#endif  /* __TESTAPP_H__ */
/***************************End of file***************************************/

