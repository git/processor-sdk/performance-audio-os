/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/


/**
 *  @file   fil_literals.h
 *
 *  @brief  This header defines all inner FIL literals, etc.
 *
 */
/*
 *  ======== fil_literals.h ========
 *  This header defines all inner FIL literals,etc. 
 */
   
#ifndef FIL_LITERALS_
#define FIL_LITERALS_  

/** @defgroup  FIL_bit_mask FIL bit field mask constants 
 *@{
 */
/*@{*/
/* FIL bit field mask constants */
#define FIL_TYPE_CPRECMASK (0x60000000)
#define FIL_TYPE_CGMASK    (0x1F800000)
#define FIL_TYPE_CSGMASK   (0x00780000)
#define FIL_BIT31          (0x80000000)
#define FIL_IR             (0x0)
#define FIL_CASCADE_IR     (0x1)
/*@}*/
/** @} */

/** @defgroup  IR_constants IR specific defined constants 
 *@{
 */
/*@{*/
/* IR specific defined constants */
#define FIL_TYPE_IRTAPMASK (0x7FE0)
#define FIL_TYPE_IRCHMASK  (0x1F)
#define FIL_TYPE_IRCASCADEMASK (0x00038000)

/* IR sub-groups */
#define FIL_IR_IIR    	FIL_IR_IIR_DF2
#define	FIL_IR_IIR_DF2	(0x0)
#define	FIL_IR_IIR_DF1	(0x2)
#define	FIL_IR_IIR_DF2T	(0x3)
    #ifdef PAF_DEVICE
        #define FIL_IR_IIR_PROC_MX1 (0x1)  /* Processing type : Coeff = SP and all others are DP */
    #endif  // PAF_DEVICE
#define FIL_IR_FIR    (0x1)
/* Cascade IR sub-groups */
#define FIL_IR_IIR_CASC_SOS_DF1    (0x0)
#define FIL_IR_IIR_CASC_SOS_DF2    (0x1)
#define FIL_IR_IIR_CASC_SOS_DF2T    (0x2)

#define FIL_IR_IIRTAPMAX 1
#define FIL_IR_FIRTAPMAX 1
#define FIL_IR_IIRTAPGEN 5 
#define FIL_IR_IIRCHGEN  5
#define FIL_IR_FIRTAPGEN 1
#define FIL_IR_FIRCHGEN  2
/* Cascaded IR */
#define FIL_CASC_IR_SOS_DF2_CASC_GEN 5
#define FIL_CASC_IR_SOS_DF2_CH_GEN   2
/*@}*/
/** @} */

#define FIL_UNICOEFFSET 0x00040000

/** @defgroup  FIL_return FIL return values 
 *@{
 */
/*@{*/
/* FIL return values */
#define FIL_SUCCESS   0
#define FIL_FAIL      1
#define FIL_ERROR    -1
/*@}*/
/** @} */

#define FIL_BITSPERBYTE 8

/** @defgroup  FIL_data_type FIL data types 
 *@{
 */
/*@{*/
/* FIL data types */
#define FIL_AUDIODATATYPE_DOUBLE 0
#define FIL_AUDIODATATYPE_FLOAT  1
#define FIL_AUDIODATATYPE_INT32  2
#define FIL_AUDIODATATYPE_INT16  3
#define FIL_AUDIODATATYPE_INT8   4
/*@}*/
/** @} */


/* FIL consts modification for new kernels available for Antara */
#ifdef PAF_DEVICE

    #undef  FIL_IR_IIRCHGEN
    #define FIL_IR_IIRCHGEN 9

    #undef  FIL_CASC_IR_SOS_DF2_CH_GEN
    #define FIL_CASC_IR_SOS_DF2_CH_GEN 3

#endif //PAF_DEVICE

#endif
