
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library -- Vector Operations
//
//
//

//#include <ti/procsdk_audio/procsdk_audio_typ.h>
#include <procsdk_audio_typ.h>
#include "cpl.h"
#include "paftyp.h"
#include <stdasp.h>

#if (((PAF_DEVICE&0xFF000000) == 0xD8000000) || \
     ((PAF_DEVICE&0xFF000000) == 0xDA000000))
#include <pafhjt.h>
#else  /* PAF_DEVICE = 0xD8000000 */
#ifdef _TMS320C6700_PLUS // only for Antara 
#include <dmax_dat.h>
#endif
#endif /* PAF_DEVICE = 0xD8000000 */

#define SOS .34713 /* speed of sound in m/ms */


Int CPL_delay_(PAF_AudioFrame * restrict pAudioFrame, PAF_DelayState * restrict state, Int apply, Int channel, Int unit)
{
    static const Float sampleRateScale[] = { 
        0.0,                            // unused
        0.0010,                         // milliseconds (Q0)
        0.0005,                         // milliseconds (Q1)
        0.0010 / (SOS * 100.0),         // milliseconds per centimeter
        0.0010 / (SOS * 3.28084),       // milliseconds per foot
        0.0010 / (SOS * 1.093613),      // milliseconds per yard
        0.0010 / (SOS * 1.0),           // milliseconds per meter
        0.0001,                         // milliseconds (tenths)
    };

    PAF_AudioData * restrict data = pAudioFrame->data.sample[channel];

    //
    // Reset/apply according to flag and presence of data storage.
    //
    // Note that the channel configuration is not checked here to determine
    // the validity of the data in the buffer or actual need for apply/reset.
    // It is expected that such a check will occur in the caller to determine
    // the apply flag value.
    //
    // To facilitate this expected usage, the apply flag is checked only for 
    // zero/non-zero value here.
    //

    if (! apply || ! data) {

        // Reset delay:

        state->count = 0;
        state->pntr = state->base;

    }
    else {

        // Apply delay:

        Uns sampleCount = pAudioFrame->sampleCount;
        Uns startCount;

        Uns delay;

        Uns i;

        // Compute delay according to units, and saturate to buffer length.

        if (unit >= lengthof (sampleRateScale))
            unit = 0;

        if (! unit)
            delay = state->delay;
        else
            delay = sampleRateScale[unit] 
                * pAudioFrame->fxns->sampleRateHz 
                    (pAudioFrame, pAudioFrame->sampleRate, PAF_SAMPLERATEHZ_STD)
                * state->delay;

        if (delay >= state->length)
            delay = state->length;

        // Function as per frame/state length relation & delay

        if (sampleCount <= state->length)
        {
            if (delay == 0) {

                // Zero delay -- save to buffer & count, only.
                {
                    PAF_AudioData * restrict pntr = state->pntr;
                    Uns spaceCount = state->base + state->length - state->pntr;
                    i = 0;
                    if (spaceCount <= sampleCount) {
                       for (; i < spaceCount; i++)
                           *pntr++ = *data++;
                       if (i == spaceCount)
                           pntr = state->base;
                    }
                    for (; i < sampleCount; i++)
                        *pntr++ = *data++;
                    state->pntr = pntr;
                }
                if (state->count < state->length)
                    state->count += sampleCount;

            }
            else {

                // Non-zero delay -- implement in zero & non-zero result phases.

                if (state->count < delay) {
                    if ((startCount = delay - state->count) > sampleCount)
                        startCount = sampleCount;
                    state->count += sampleCount;
                }
                else {
                    startCount = 0;
                    if (state->count < state->length)
                        state->count += sampleCount;
                }

                {
                    PAF_AudioData * restrict pntr = state->pntr;
                    Int spaceCount = state->base + state->length - state->pntr;
                    i = 0;
                    if (spaceCount <= startCount) {
                        for (; i < spaceCount; i++) {
                            *pntr++ = *data;
                            *data++ = 0;
                        }
                        if (i == spaceCount)
                            pntr = state->base;
                    }
                    for (; i < startCount; i++) {
                        *pntr++ = *data;
                        *data++ = 0;
                    }
                    state->pntr = pntr;
                }

                {
                    PAF_AudioData *pntr = state->pntr;
                    PAF_AudioData *next = pntr - delay;
                    PAF_AudioData x;

                    if (next < state->base)
                        next += state->length;

                    /* Vanilla code: 3 cycles per sample (likes -mh8). */
                    for (; i < sampleCount; i++) {
                        x = *next++;
                        *pntr++ = *data;
                        *data++ = x;
                        if (next == state->base + state->length)
                            next = state->base;
                        if (pntr == state->base + state->length)
                            pntr = state->base;
                    }
                    state->pntr = pntr;
                }
            }
        }
        else /* if (sampleCount > state->length) */ {

            if (delay == 0) {

                // Zero delay -- save data tail to buffer, only.

                PAF_AudioData *save = state->base;

                /* Chocolate code: 1 cycle per state (likes -mh24). */
                for (i=0; i < state->length; i++)
                    save[i] = data[i+sampleCount-state->length];

                state->pntr = state->base;
            }
            else {

                // Non-zero delay -- implement in zero & non-zero state phases.

                PAF_AudioData *pntr;
                PAF_AudioData *next;
                PAF_AudioData x;

                /* Chocolate code: 3 cycles per sample (likes -mh8). */

                if (state->count == 0) {
                    // First pass: initialize pointers & load from zero
                    pntr = state->base;
                    next = state->base + state->length - delay;
                    for (i=0; i < state->length; i++) {
                        *next++;
                        *pntr++ = *data;
                        *data++ = 0;
                        if (next == state->base + state->length)
                            next = state->base;
                        if (pntr == state->base + state->length)
                            pntr = state->base;
                    }
                }
                else {
                    // Subsequent pass: set pointers & load from state
                    pntr = state->pntr;
                    next = pntr - delay;
                    if (next < state->base)
                        next += state->length;
                    i=0;
                }

                for (; i < sampleCount; i++) {
                    x = *next++;
                    *pntr++ = *data;
                    *data++ = x;
                    if (next == state->base + state->length)
                        next = state->base;
                    if (pntr == state->base + state->length)
                        pntr = state->base;
                }
                state->pntr = pntr;
            }

            state->count = state->length;
        }

    }
	return 0;
}

Int CPL_delayDAT_(PAF_AudioFrame * restrict pAudioFrame, PAF_DelayState * restrict state, PAF_AudioData * restrict swap, Int apply, Int channel, Int unit)
{
    static const Float sampleRateScale[] = {
        0.0,                             // unused
        0.0010,                          // milliseconds (Q0)
        0.0005,                          // milliseconds (Q1)
        0.0010 / (SOS * 100.0),          // milliseconds per centimeter
        0.0010 / (SOS * 3.28084),        // milliseconds per foot
        0.0010 / (SOS * 1.093613),       // milliseconds per yard
        0.0010 / (SOS * 1.0),            // milliseconds per meter
        0.0001,                          // milliseconds (tenths)
    };

    PAF_AudioData * restrict data = pAudioFrame->data.sample[channel];
    PAF_AudioData * restrict outptr = pAudioFrame->data.sample[channel];
    Int datid;

    if(!apply || !data) {
        // Reset delay
        state->count = 0;
        state->pntr = state->base;
    }
    else {
        // Apply delay
		Int sizeofElement = sizeof(PAF_AudioData);
        Uns sampleCount = pAudioFrame->sampleCount;
        Uns sampleLength = sampleCount * sizeofElement;
        Uns totlen, buflen, dellen, len, off;
        Uns startCount;
        Uns delay;
        Uns i;
        Int wraplen;
        Int eob;

        // Compute delay according to units and saturate to buffer length
        if(unit >= lengthof(sampleRateScale))
            unit = 0;

        if(!unit)
            delay = state->delay;
        else
            delay = sampleRateScale[unit] *
                    pAudioFrame->fxns->sampleRateHz(pAudioFrame,  
                        pAudioFrame->sampleRate, 
                        PAF_SAMPLERATEHZ_STD) *
                    state->delay;

        if(delay >= state->length)
            delay = state->length;

        if(delay == 0) {
            // zero delay -- save to buffer & output the same
            for(i=0;i<sampleCount;++i) {
                // Compute the value and save and also copy to output buffer
                *outptr++ = *swap++ = *data++;
            }

            swap -= sampleCount; 
            wraplen = state->length * sizeofElement - ((Int)state->pntr - (Int)state->base);
            datid = DAT_copy(swap, (SmInt *)state->pntr, wraplen < sampleLength ? wraplen : sampleLength);
            DAT_wait(datid);
            if(wraplen < sampleLength) {
                datid = DAT_copy((SmInt *)swap+wraplen, (SmInt *)state->base, sampleLength - wraplen);
                DAT_wait(datid);
            }
            state->pntr = (PAF_AudioData *)((Int)state->pntr + sampleLength);
            eob = (Int)state->base + state->length * sizeofElement;
            if((Int)state->pntr >= eob)
                state->pntr = (PAF_AudioData *)((Int)state->pntr - state->length * sizeofElement);
            if(state->count < state->length)
                state->count += sampleCount;
        } 
        else {
            // non-zero delay -- implement in zero and non-zero phases
            PAF_AudioData * restrict next;

            if(state->count < delay) {
                if((startCount = delay - state->count) > sampleCount)
                    startCount = sampleCount;
                state->count += sampleCount;
            }
            else {
                startCount = 0;
                if(state->count < state->length)
                    state->count += sampleCount;
            }

            for(i=0;i<startCount;++i) {
                // Compute the value and save
                *swap++ = *data++;
                // output zero's 
                *outptr++ = 0;
            }

            for(;i<sampleCount;++i) {
                // Save the sample value
                *swap++ = *data++;
             }

            swap -= sampleCount;
            if(sampleCount - startCount) {
                next = (PAF_AudioData *)((Int)state->pntr + startCount * sizeofElement - delay * sizeofElement);
                if((Uns)next < (Uns)state->base)
                    next = (PAF_AudioData *)((Int)next + state->length * sizeofElement);
                totlen = (sampleCount-startCount) * sizeofElement;
                wraplen = state->length * sizeofElement - ((Int)next - (Int)state->base);
                buflen = state->length * sizeofElement;
                dellen = delay * sizeofElement;
                if((Int)next == (Int)state->pntr && startCount == delay) {
                    datid = DAT_copy((SmInt*)swap, outptr, totlen);
                    DAT_wait(datid);
                }
                else {
                    if( totlen <= dellen) {
                        datid = DAT_copy(next, outptr, totlen > wraplen ? wraplen : totlen);
                        DAT_wait(datid);
                        if(totlen > wraplen) {
                            datid = DAT_copy((SmInt*)state->base, (SmInt*)outptr+wraplen, 
                                     totlen <= buflen ? totlen - wraplen : buflen - wraplen);
                            DAT_wait(datid);
                        }
                        if(totlen > buflen) {
                            datid = DAT_copy((SmInt*)swap, (SmInt*)outptr+buflen, totlen - buflen);
                            DAT_wait(datid);
                        }
                    }
                    else {
                        len = dellen > wraplen ? wraplen : dellen;
                        datid = DAT_copy(next, outptr, len);
                        DAT_wait(datid);
                        off = len;
                        if(dellen > wraplen) {
                            datid = DAT_copy((SmInt*)state->base, (SmInt*)outptr+off,dellen - wraplen); 
                            DAT_wait(datid);
                            off = dellen;
                        }
                        datid = DAT_copy((SmInt*)swap, (SmInt*)outptr+off, totlen - dellen);
                        DAT_wait(datid);
                    }
                }
            }
            totlen = sampleLength; 
            wraplen = state->length * sizeofElement - ((Int)state->pntr - (Int)state->base);
            buflen = state->length * sizeofElement;
            datid = DAT_copy(swap, (SmInt *)state->pntr, totlen > wraplen ? wraplen : totlen);
            DAT_wait(datid);
            if(totlen > wraplen) {
                totlen -= wraplen;
                off = wraplen;
                while(totlen) {
                    datid = DAT_copy((SmInt*)swap+off, (SmInt *)state->base, totlen > buflen ? buflen : totlen);
                    DAT_wait(datid);
                    totlen -= totlen > buflen ? buflen : totlen;
                    off += totlen > buflen ? buflen : totlen;
                } 
            }

            state->pntr = (PAF_AudioData *)((Int)state->pntr + sampleLength);
            eob = (Int)state->base + state->length * sizeofElement;
            while((Int)state->pntr >= eob)
                state->pntr = (PAF_AudioData *)((Int)state->pntr - state->length * sizeofElement);
        } 
    }
    return 0;

}
