
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library -- Vector Operations
//
//
//

#ifndef CPL_VECOPS_
#define CPL_VECOPS_

#ifndef _TMS320C6X
#define restrict
#endif

//------------------Non-Interpolated Delay Structure(SFX)---------------//

/*
 * Macros to call virtual functions.
 */

#define CPL_Delay(d, in, out, n) d->proc(d, in, out, n)


/*
 *  common datatype 
 */
typedef struct delay_tag {
   int  type;
   int delay;           /* current delay */
   void *buf;		/* pointer to INTEGER delay buffer */
   int nbuf;		/* size of buf, power of 2 for native C implementation */

   int addr_mask;       /* address mask = nbuf - 1 */
   int addr;		/* next write goes here, postincremented */

   /*
    * Virtual functions.
    */
   void (*proc)(struct delay_tag *, float *, float *, int);
} WaDelay;

#define DELAY_TYPE_FLOAT 0
#define DELAY_TYPE_INT16 1


//-------------------Common Struct Used by SFX---------------------//

void CPL_vecAdd(float *pIn1, float *pIn2, float *pOut, int n);
void CPL_vecAdd_(float *pIn1, float *pIn2, float *pOut, int n);

void CPL_vecLinear2(float c1, float *pIn1, float c2, float *pIn2, float *pOut, int n);
void CPL_vecLinear2_(float c1, float *pIn1, float c2, float *pIn2, float *pOut, int n);

void CPL_vecScale(float c, float *pIn, float *pOut, int n);
void CPL_vecScale_(float c, float *pIn, float *pOut, int n);

void CPL_vecSet(float val, float *pOut, int n);
void CPL_vecSet_(float val, float *pOut, int n);

void CPL_ivecCopy(int *pIn, int *pOut, int n);
void CPL_ivecCopy_(int *pIn, int *pOut, int n);

void CPL_svecSet(int val, short *pOut, int n);
void CPL_svecSet_(int val, short *pOut, int n);

void CPL_ivecSet(int val, int *pOut, int n);
void CPL_ivecSet_(int val, int *pOut, int n);

void CPL_imaskScale(int mask, float scale, int * restrict pIn, 
        float * restrict pOut, int stride, int n);
void CPL_imaskScale_(int mask, float scale, int * restrict pIn, 
        float * restrict pOut, int stride, int n);

void CPL_smaskScale(int mask, float scale, short * restrict pIn, 
        float * restrict pOut, int stride, int n);
void CPL_smaskScale_(int mask, float scale, short * restrict pIn, 
        float * restrict pOut, int stride, int n);

void CPL_vecStrideScale(float scale, float * restrict pIn, 
        float * restrict pOut, int stride, int n);
void CPL_vecStrideScale_(float scale, float * restrict pIn, 
        float * restrict pOut, int stride, int n);


void CPL_vecClip(float *pIn, float max, float min, float *pOut, int n);
void CPL_vecClip_(float *pIn, float max, float min, float *pOut, int n);

float CPL_vecClip1(float );
float CPL_vecClip1_(float );

void CPL_vecLinear2Incr(float c1, float Incr, float * restrict pIn1, float c2, float * restrict pIn2, float * restrict pOut, int nSamples);
void CPL_vecLinear2Incr_(float c1, float Incr, float * restrict pIn1, float c2, float * restrict pIn2, float * restrict pOut, int nSamples);

void CPL_vecLinear2Common(float c, float * restrict pIn1, float * restrict pIn2, float * restrict pOut, int nSamples);
void CPL_vecLinear2Common_(float c, float * restrict pIn1, float * restrict pIn2, float * restrict pOut, int nSamples);

void CPL_vecScaleAcc(float c, float * restrict pIn, float * restrict pOut, int n);
void CPL_vecScaleAcc_(float c, float * restrict pIn, float * restrict pOut, int n);

void CPL_vecScaleIncr(float c, float Incr, float * restrict pIn, float * restrict pOut, int n);
void CPL_vecScaleIncr_(float c, float Incr, float * restrict pIn, float * restrict pOut, int n);

void CPL_vecSetArr(float val, float ** restrict pOut, int n, int a);
void CPL_vecSetArr_(float val, float ** restrict pOut, int n, int a);

float CPL_GetPAFSampleRate(int sampRateEnum);
float CPL_GetPAFSampleRate_(int sampRateEnum);

int  CPL_GetNumSat(int config); 
int  CPL_GetNumSat_(int config); 

void CPL_intDelayProc(WaDelay *d, float *input, float *output, int n);
void CPL_intDelayProc_(WaDelay *d, float *input, float *output, int n);

void CPL_floatDelayProc(WaDelay *d, float *input, float *output, int n);
void CPL_floatDelayProc_(WaDelay *d, float *input, float *output, int n);

void CPL_DelaySet(WaDelay *d, int delay);
void CPL_DelaySet_(WaDelay *d, int delay);

void CPL_DelayInit(WaDelay *d, int type, int max_delay, int delay);
void CPL_DelayInit_(WaDelay *d, int type, int max_delay, int delay);

void CPL_sumDiff(float *p1, float *p2, int n);
void CPL_sumDiff_(float *p1, float *p2, int n);

/* Redundant entry points */
#define CPL_vecCopy(pIn, pOut, n)       CPL_vecScale(1.0f, pIn, pOut, n)

#endif
