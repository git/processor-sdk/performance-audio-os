/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Vendor-specific (MDS) PCM algorithm internal interface declarations
//
//


/*
 *  Internal vendor specific (MDS) interface header for PCM
 *  algorithm. Only the implementation source files include
 *  this header; this header is not shipped as part of the
 *  algorithm.
 *
 *  This header contains declarations that are specific to
 *  this implementation and which do not need to be exposed
 *  in order for an application to use the PCM algorithm.
 */
#ifndef PCM_MDS_PRIV_
#define PCM_MDS_PRIV_

#include <ti/xdais/ialg.h> //<ialg.h>
#include <ipcm.h>
//#include <log.h>

#include "paftyp.h"
#include "com_tii_priv.h"
typedef struct PCM_MDS_Obj {
    IALG_Obj alg;           /* MUST be first field of all XDAS algs */
	int mask;
    PCM_MDS_Status *pStatus; /* public interface (not shared) */
    PCM_MDS_Config *pConfig; /* private interface (not shared) */
    PCM_MDS_Active *pActive; /* private interface (stream-shared) */
    PCM_MDS_Scrach *pScrach; /* private interface (invocation-shared) */
} PCM_MDS_Obj;

#define PCM_MDS_activate COM_TII_activate

#define PCM_MDS_deactivate COM_TII_deactivate

extern Int PCM_MDS_alloc(const IALG_Params *algParams, IALG_Fxns **pf,
                        IALG_MemRec memTab[]);

#define PCM_MDS_free COM_TII_free

extern Int PCM_MDS_control(IALG_Handle, IALG_Cmd, IALG_Status *);

extern Int PCM_MDS_initObj(IALG_Handle handle,
                          const IALG_MemRec memTab[], IALG_Handle parent,
                          const IALG_Params *algParams);
                
#define PCM_MDS_moved COM_TII_moved
                
extern Int PCM_MDS_decode(IPCM_Handle, ALG_Handle, PAF_DecodeInStruct *, PAF_DecodeOutStruct *);

extern Int PCM_MDS_info(IPCM_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);

extern Int PCM_MDS_reset(IPCM_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);

extern Int PCM_MDS_input(IPCM_Handle, PAF_DecodeInStruct *, PAF_DecodeOutStruct *, PAF_ChannelMask);

extern Int PCM_MDS_rampart(IPCM_Handle, PAF_DecodeInStruct *, PAF_DecodeOutStruct *, PAF_ChannelMask);

extern Int PCM_MDS_downmix(IPCM_Handle, PAF_DecodeInStruct *, PAF_DecodeOutStruct *, PAF_ChannelMask);

extern Int PCM_MDS_inputCheck(IPCM_Handle, Int);

extern Int PCM_MDS_inputAudioSmInt(IPCM_Handle, SmInt *, SmInt, Int, Int);
extern Int PCM_MDS_inputAudioMdInt(IPCM_Handle, MdInt *, MdInt, Int, Int);
extern Int PCM_MDS_inputAudioLgInt(IPCM_Handle, LgInt *, LgInt, Int, Int);
extern Int PCM_MDS_inputAudioFloat(IPCM_Handle, float *, float, Int, Int);
extern Int PCM_MDS_inputAudioDouble(IPCM_Handle, double *, double, Int, Int);

#define PCM_MDS_setAudioFrame CPL_setAudioFrame
//extern Int PCM_MDS_setAudioFrame(IPCM_Handle, PAF_AudioFrame *, Int, PAF_DecodeStatus *);

#endif  /* PCM_MDS_PRIV_ */
