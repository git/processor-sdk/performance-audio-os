
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) Alpha Code Processor Algorithm IALG table definitions
//
//
//

/*
 *  This file contains the function table definitions for all
 *  interfaces implemented by the ACP_MDS module that derive
 *  from IALG
 *
 *  We place these tables in a separate file for two reasons:
 *     1. We want allow one to one to replace these tables
 *        with different definitions.  For example, one may
 *        want to build a system where the ACP is activated
 *        once and never deactivated, moved, or freed.
 *
 *     2. Eventually there will be a separate "system build"
 *        tool that builds these tables automatically 
 *        and if it determines that only one implementation
 *        of an API exists, "short circuits" the vtable by
 *        linking calls directly to the algorithm's functions.
 */
#include <xdc/std.h>

#include <ti/xdais/ialg.h>
#include <iacp.h>

#include <acp_mds.h>
#include <acp_mds_priv.h>
//#ifdef IROM
void ACP_MDS_sprintf1(IACP_Handle handle , char *p, int arg1, int arg2);
void ACP_MDS_sprintf2(IACP_Handle handle , char *p, int arg1, int arg2);
void ACP_MDS_sprintf3(IACP_Handle handle , char *p, int arg1);
//#endif

#define IALGFXNS \
    &ACP_MDS_IALG,       /* module ID */                         \
    ACP_MDS_activate,    /* activate */                          \
    ACP_MDS_alloc,       /* alloc */                             \
    ACP_MDS_control,     /* control */			         \
    ACP_MDS_deactivate,  /* deactivate */                        \
    ACP_MDS_free,        /* free */                              \
    ACP_MDS_initObj,     /* init */                              \
    ACP_MDS_moved,       /* moved */                             \
    NULL                 /* numAlloc (NULL => IALG_MAXMEMRECS) */\

/*
 *  ======== ACP_MDS_IACP ========
 *  This structure defines MDS's implementation of the IACP interface
 *  for the ACP_MDS module.
 */

#if !(PAF_IROM_BUILD == 0xD610A003) && !(PAF_IROM_BUILD == 0xD610A004) && !(PAF_IROM_BUILD == 0xD710E001)
IACP_Fxns ACP_MDS_IACP = {       /* module_vendor_interface */
    /* standard */
    IALGFXNS,
    /* public */
    ACP_MDS_attach,
    ACP_MDS_apply,
    ACP_MDS_length,
    ACP_MDS_htgnel,
    ACP_MDS_sequence,
    ACP_MDS_srecord,
    /* private */
    ACP_MDS_escape,
    ACP_MDS_legacy,
//#ifdef IROM
    /* IROM specific pointers */
    ACP_MDS_sprintf1,
    ACP_MDS_sprintf2,
    ACP_MDS_sprintf3,
//#endif
};
#else /* PAF_IROM_BUILD */
IACP_Fxns ACP_MDS_IACP = {       /* module_vendor_interface */
    /* standard */
    IALGFXNS,
    /* public */
    ACP_MDS_attach,
    ACP_MDS_apply,
    ACP_MDS_length,
    ACP_MDS_htgnel,
    ACP_MDS_sequence,
    NULL,
    /* private */
    ACP_MDS_escape,
    ACP_MDS_legacy,
//#ifdef IROM
    /* IROM specific pointers */
    NULL,
    NULL,
    NULL,
//#endif
};
#endif /* PAF_IROM_BUILD */
/*
 *  ======== ACP_MDS_IALG ========
 *  This structure defines MDS's implementation of the IALG interface
 *  for the ACP_MDS module.
 */
#ifdef _TMS320C6X
#ifdef __TI_EABI__
asm("ACP_MDS_IALG .set ACP_MDS_IACP");
#else /* _EABI_*/
asm("_ACP_MDS_IALG .set _ACP_MDS_IACP");
#endif /* _EABI_*/
#else

/*
 *  We duplicate the structure here to allow this code to be compiled and
 *  run non-DSP platforms at the expense of unnecessary data space
 *  consumed by the definition below.
 */
IALG_Fxns ACP_MDS_IALG = {       /* module_vendor_interface */
    IALGFXNS
};

#endif

