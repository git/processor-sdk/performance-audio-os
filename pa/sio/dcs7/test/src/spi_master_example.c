/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/



/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <stdio.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>

/* SPI Header files */
#include <ti/drv/spi/SPI.h>
#include <ti/drv/spi/src/v0/SPI_v0.h>
#include <ti/drv/spi/soc/SPI_soc.h>
#include <ti/drv/spi/test/src/SPI_log.h>
#include <ti/drv/spi/test/src/SPI_board.h>


/**********************************************************************
 ********************** SPI Master ************************************
 **********************************************************************/
// Comment/Uncomment to enable or disable profiling
//#define PROFILER
#ifdef PROFILER
#include "util_profiling.h"
#include "pa_i13_evmk2g_io_a.h"
#include "acptype.h"
#define REGRESS_TEST
#endif


// Comment/Uncomment to enable or disable printf messages,
// These when enabled will effect the regress test behaviour.
//#define REGRESS_TEST
#ifdef REGRESS_TEST
#define printf(fmt, ...)
#endif

// Comment/Uncomment to enable or disable SLC (serial link control) test
//#define SENDSLC

// Comment/Uncomment to enable or disable callback mode
//#define SPICALLBACK


Uint8 gRxBuffer[512] = {0};
Uint8 gTxBuffer[512] = {0};

#define SPI_PORT 0

SPI_Handle gSpiHandle;

/* Buffer containing the known data that needs to be written to flash */
Uint32 txBuf[1024];

/* Buffer containing the received data */
Uint32 rxBuf[1024];

/* transfer length */
Uint32 transferLength;

#ifdef SPICALLBACK
/* global semaphore */
void *globalSem = NULL;
#endif

/******************************************************************************
 * SPI defines and enums
 *****************************************************************************/

typedef union DCS7_uint16
{
    struct
    {
        Uint8 low_byte;
        Uint8 high_byte;
    } part;
    Uint16 word_2byte;
} DCS7_uint16;

// sizes in Bytes
typedef enum spi_size_e
{
    SPI_ACK_SIZE       = 2,        // spiSlaveAck size
    SPI_CMD_COUNT_SIZE     = 2,    // Alpha command Count size
    SPI_RES_COUNT_SIZE     = 2     // Alpha command response Count size
} spi_size;

// sizes in Bytes
typedef enum testapp_size_e
{
    ALPHA_CMD_MAX_SIZE     = 256,
    ALPHA_CMD_RES_MAX_SIZE = 256

} testapp_size;

// Counts
typedef enum testapp_count_e
{
#ifdef PROFILER
    TESTAPP_ALPHA_MAX_CNT  = 8,
#else
    TESTAPP_ALPHA_MAX_CNT  = 22,
#endif
    TESTAPP_SLC_MAX_CNT    = 3

} testapp_count;

#define SPI_SLAVE_ACK 0xF101
// Set count value as per dataSize, (x) in bytes
#define NUM_TRANSFER_ELEMENT(x) (gSpiParams.dataSize == 8 ? (x):(x/2))

#ifdef SENDSLC
Uint16 SlcSequenceSend[TESTAPP_SLC_MAX_CNT][ALPHA_CMD_MAX_SIZE] =
{
        {0x8001},                                  // 0 reset query
        {0x8101},                                  // 1 ready query
        {0x8201}                                   // 2 not implemented
};
#endif

Uint16 alphaSequenceSend[TESTAPP_ALPHA_MAX_CNT][ALPHA_CMD_MAX_SIZE] =
{
#ifdef PROFILER
        {readENCListeningFormat},
        {readDECSourceProgram},
        {readDECProgramFormat},
        {writeVOLControlMasterN(0)},
        {writeSYSSpeakerMainLarge2},
        {writeENCChannelMapTo16(0,8,-3,-3,1,9,-3,-3,-3,-3,2,10,3,11,-3,-3)},
        {writeSYSChannelConfigurationRequestSurround4_1},
        {readPCEControl}
#else
        {0x0002, 0xC625, 0x6020},                                  // 0
        {0x0002, 0xC024, 0x0A49},                                  // 1
        {0x0002, 0xC805, 0x0D00},                                  // 2
        {0x0001, 0xC100},                                          // 3
        {0x0001, 0xDEAD},                                          // 4
        {0x0002, 0xC224, 0x0400},                                  // 5
        {0x0002, 0xCA24, 0x2E01},                                  // 6
        {0x0002, 0xC324, 0x0030},                                  // 7
        {0x0003, 0xCB20, 0x0024, 0x0000},                          // 8
        {0x0002, 0xC424, 0x0040},                                  // 9
        {0x0004, 0xCC24, 0x0038, 0x0108, 0x0000},                  // 10
        {0x0006, 0xCE34, 0x1808, 0x0000, 0x0000, 0x0000, 0x0000},  // 11
        {0x0002, 0xC509, 0x0400},                                  // 12
        {0x0002, 0xCD09, 0x0400},                                  // 13
        {0x0002, 0xC425, 0x0088},                                  // 14
        {0x0002, 0xC224, 0x0A00},                                  // 15
        {0x0002, 0xC424, 0x00A0},                                  // 16
        {0x0003, 0xCB26, 0x0010, 0x0000},                          // 17
        {0x0002, 0xCA20, 0x0732},                                  // 18
        {0x000A, 0xCE25, 0x6010, 0x0800, 0xFDFD, 0x0901, 0xFDFD,
         0xFDFD, 0x0A02, 0x0B03, 0xFDFD},                          // 19
        {0x0004, 0xCC20, 0x0020, 0x010C, 0x0000},                  // 20
        {0x0096, 0xc22e, 0x0400, 0xc22e, 0x0600, 0xc22e, 0x0700,
         0xc22e, 0x0800, 0xc22e, 0x0900, 0xc22e, 0x0a00, 0xc22e,
         0x0b00, 0xc22e, 0x0c00, 0xc22e, 0x0d00, 0xc22e, 0x0e00,
         0xc22e, 0x0f00, 0xc22e, 0x1000, 0xc22e, 0x1100, 0xc22e,
         0x1300, 0xc22e, 0x1400, 0xc32e, 0x0016, 0xc32e, 0x0018,
         0xc32e, 0x001a, 0xc32e, 0x001a, 0xc32e, 0x001c, 0xc32e,
         0x001e, 0xc32e, 0x001e, 0xc32e, 0x0020, 0xc32e, 0x0022,
         0xc32e, 0x0022, 0xc32e, 0x0024, 0xc32e, 0x0026, 0xc32e,
         0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a,
         0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024,
         0xc32e, 0x0026, 0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e,
         0x002a, 0xc32e, 0x002a, 0xc32e, 0x002c, 0xc32e, 0x002e,
         0xc32e, 0x002e, 0x0024, 0xc32e, 0x0026, 0xc32e, 0x0026,
         0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a, 0xc32e,
         0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024, 0xc32e,
         0x0026, 0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a,
         0xc32e, 0x002a, 0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e,
         0x002e, 0x0024, 0xc32e, 0x0026, 0xc32e, 0x0026, 0xc32e,
         0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a, 0xc32e, 0x002c,
         0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024, 0xc32e, 0x0026,
         0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e,
         0x002a, 0xc32e, 0x002c, 0xc32e}                            // 21
#endif
};

Uint16 alphaSequenceReceive[TESTAPP_ALPHA_MAX_CNT][ALPHA_CMD_RES_MAX_SIZE] =
{
#ifdef PROFILER
        {0x0004, 0xCC25, 0x0088, 0x0000, 0x0000},
        {0x0002, 0xCA24, 0x0A00},
        {0x0004, 0xCC24, 0x00A0, 0x0000, 0x0000},
        {0x0000},
        {0x0000},
        {0x0000},
        {0x0006, 0xCE20, 0x2008, 0x010C, 0x0200,
         0x007F, 0x0000},
        {0x004A, 0xc22e, 0x0400, 0xc22e, 0x0600, 0xc22e, 0x0700,
         0xc22e, 0x0800, 0xc22e, 0x0900, 0xc22e, 0x0a00, 0xc22e,
         0x0b00, 0xc22e, 0x0c00, 0xc22e, 0x0d00, 0xc22e, 0x0e00,
         0xc22e, 0x0f00, 0xc22e, 0x1000, 0xc22e, 0x1100, 0xc22e,
         0x1300, 0xc22e, 0x1400, 0xc32e, 0x0016, 0xc32e, 0x0018,
         0xc32e, 0x001a, 0xc32e, 0x001a, 0xc32e, 0x001c, 0xc32e,
         0x001e, 0xc32e, 0x001e, 0xc32e, 0x0020, 0xc32e, 0x0022,
         0xc32e, 0x0022, 0xc32e, 0x0024, 0xc32e, 0x0026, 0xc32e,
         0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a,
         0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0xc32e,
         0x0030, 0xc32e, 0x0032, 0xc32e, 0x0034},  // readPCEControl
#else
        {0x0012, 0xCE25, 0x6020, 0xFDFD, 0xFDFD, 0xFDFD,
         0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD,
         0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD, 0xFDFD,
         0xFDFD},                                   // 0
        {0x0002, 0x0000, 0x0000},                   // 1
        {0x0002, 0xD300, 0x0002},                   // 2
        {0x0001, 0xC100},                           // 3
        {0x0001, 0xDEAD},                           // 4
        {0x0002, 0xCA24, 0x0401},                   // 5
        {0x0000},                                   // 6
        {0x0003, 0xCB24, 0x0030, 0x0000},           // 7
        {0x0000},                                   // 8
        {0x0004, 0xCC24, 0x0040, 0x0000, 0x0000},   // 9
        {0x0000},                                   // 10
        {0x0000},                                   // 11
        {0x0002, 0xCD09, 0x0400},                   // 12
        {0x0000},                                   // 13
        {0x0004, 0xCC25, 0x0088, 0x0000, 0x0000},   // 14
        {0x0002, 0xCA24, 0x0A00},                   // 15
        {0x0004, 0xCC24, 0x00A0, 0x0000, 0x0000},   // 16
        {0x0000},                                   // 17
        {0x0000},                                   // 18
        {0x0000},                                   // 19
        {0x0006, 0xCE20, 0x2008, 0x010C, 0x0200,
         0x007F, 0x0000},                           // 20
        {0x0096, 0xc22e, 0x0400, 0xc22e, 0x0600, 0xc22e, 0x0700,
         0xc22e, 0x0800, 0xc22e, 0x0900, 0xc22e, 0x0a00, 0xc22e,
         0x0b00, 0xc22e, 0x0c00, 0xc22e, 0x0d00, 0xc22e, 0x0e00,
         0xc22e, 0x0f00, 0xc22e, 0x1000, 0xc22e, 0x1100, 0xc22e,
         0x1300, 0xc22e, 0x1400, 0xc32e, 0x0016, 0xc32e, 0x0018,
         0xc32e, 0x001a, 0xc32e, 0x001a, 0xc32e, 0x001c, 0xc32e,
         0x001e, 0xc32e, 0x001e, 0xc32e, 0x0020, 0xc32e, 0x0022,
         0xc32e, 0x0022, 0xc32e, 0x0024, 0xc32e, 0x0026, 0xc32e,
         0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a,
         0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024,
         0xc32e, 0x0026, 0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e,
         0x002a, 0xc32e, 0x002a, 0xc32e, 0x002c, 0xc32e, 0x002e,
         0xc32e, 0x002e, 0x0024, 0xc32e, 0x0026, 0xc32e, 0x0026,
         0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a, 0xc32e,
         0x002c, 0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024, 0xc32e,
         0x0026, 0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a,
         0xc32e, 0x002a, 0xc32e, 0x002c, 0xc32e, 0x002e, 0xc32e,
         0x002e, 0x0024, 0xc32e, 0x0026, 0xc32e, 0x0026, 0xc32e,
         0x0028, 0xc32e, 0x002a, 0xc32e, 0x002a, 0xc32e, 0x002c,
         0xc32e, 0x002e, 0xc32e, 0x002e, 0x0024, 0xc32e, 0x0026,
         0xc32e, 0x0026, 0xc32e, 0x0028, 0xc32e, 0x002a, 0xc32e,
         0x002a, 0xc32e, 0x002c, 0xc32e}            // 21
#endif
};

#ifdef PROFILER
extern Int alpha_length(const ACP_Unit *from);
#endif

void SPI_transferCallback(SPI_Handle handle,
                                    SPI_Transaction *transaction);

/* SPI parameters structure Master mode*/
SPI_Params gSpiParams = {
#ifdef SPICALLBACK
    SPI_MODE_CALLBACK,  /* transferMode */
#else
    SPI_MODE_BLOCKING,  /* transferMode */
#endif
    ti_sysbios_knl_Semaphore_PendState_WAIT_FOREVER,/* transferTimeout */
#ifdef SPICALLBACK
    &SPI_transferCallback,               /* transferCallbackFxn */
#else
    NULL,
#endif
    SPI_MASTER,         /* mode */
    1000000,            /* bitRate */
    16,                  /* dataSize 8 or 16 */
    SPI_POL0_PHA0,      /* frameFormat */
    NULL                /* custom */
};

#ifdef SPICALLBACK
void SPI_transferCallback(SPI_Handle handle,
                                    SPI_Transaction *transaction)
{

    Semaphore_post(globalSem);
}
#endif
/*
 *  ======== Board_initSPI ========
 */
void Board_initSPI(void)
{
    Board_initCfg boardCfg;
    SPI_v0_HWAttrs spi_cfg;

    /* Get the default SPI init configurations */
    SPI_socGetInitCfg(SPI_PORT, &spi_cfg);

    spi_cfg.pinMode = SPI_PINMODE_4_PIN; // SPI_PINMODE_4_PIN :
                                         // SPI_PINMODE_3_PIN;

    /* Set the default SPI init configurations */
    SPI_socSetInitCfg(SPI_PORT, &spi_cfg);

    boardCfg = BOARD_INIT_PINMUX_CONFIG |
               BOARD_INIT_MODULE_CLOCK |
               BOARD_INIT_UART_STDIO;

    Board_init(boardCfg);
}

// support inplace swap
void DCS7_swap(DCS7_uint16 *dst, DCS7_uint16 *src, Uint32 size)
{
    Uint32 i;
    Uint8 temp = 0;

    for (i = 0; i < size; i++)
    {
        temp = src[i].part.high_byte;
        dst[i].part.high_byte = src[i].part.low_byte;
        dst[i].part.low_byte = temp;
    }
}

/*
 *  ======== test function ========
 */
void spi_test(UArg arg0, UArg arg1)
{
    SPI_Transaction transaction;
    Uint32        xferEnable;
    DCS7_uint16 spiSlaveAck;
    Uint8 slaveSyncFlag = 0;
    Uint32 alphaCmdNum = 0;  // TODO
    Uint32 semReturn = 0;
#ifdef PROFILER
    tPrfl profiler;
    Int prlf_flag = 0;
#endif

    spiSlaveAck.word_2byte = SPI_SLAVE_ACK;

    memset(gTxBuffer, 0, 100);
    memset(gRxBuffer, 0, 100);

#ifdef SPICALLBACK
    globalSem = Semaphore_create(0, NULL, NULL);
#endif

    /* Init SPI driver */
    SPI_init();

    /* Default SPI configuration parameters */
    //SPI_Params_init(&spiParams);
    //gSpiParams.frameFormat  = SPI_POL0_PHA1;

    /* Open SPI driver */
    gSpiHandle = SPI_open(0, &gSpiParams);

    if (gSpiHandle == NULL)
        printf("\n SPI Master Init error \n");

    printf("\n Master Tx started \n");
    /* Enable transfer */
    xferEnable = 1;
    SPI_control(gSpiHandle, SPI_V0_CMD_XFER_ACTIVATE, (void *)&xferEnable);

#ifdef SENDSLC
    while (alphaCmdNum < TESTAPP_SLC_MAX_CNT)
#else
    while (alphaCmdNum < TESTAPP_ALPHA_MAX_CNT)
#endif
    {

#ifdef PROFILER
        if (prlf_flag == 0)  // new test case
        {   /* Init and clear profiling variables*/
            Prfl_Init(&profiler);
        }
#endif

        memset(gRxBuffer, 0, 100);
        memset(gTxBuffer, 0, 100);
        slaveSyncFlag = 0;
        /* Set Transfer info */
        transaction.count = NUM_TRANSFER_ELEMENT(SPI_ACK_SIZE);
        transaction.txBuf = gTxBuffer;
        transaction.rxBuf = gRxBuffer;
    
#ifdef PROFILER
        if (prlf_flag != 0)
            Prfl_Stop(&profiler);
#endif
        SPI_transfer(gSpiHandle, &transaction);

#ifdef SPICALLBACK
        semReturn = Semaphore_pend(globalSem, BIOS_WAIT_FOREVER/*((transaction.count * 8) / Clock_tickPeriod) + 1*/);

        if (semReturn == Semaphore_PendState_TIMEOUT)
        {
            printf("write timeout \n");
            SPI_transferCancel(gSpiHandle);
            Semaphore_reset(globalSem, 0);
            continue;
        }
#endif
#ifdef PROFILER
        Prfl_Start(&profiler);
        prlf_flag = 1;
#endif

        printf("waiting for Sync .. \n");
        Task_sleep(1);

        if ((gRxBuffer[0] == spiSlaveAck.part.low_byte) &&
            (gRxBuffer[1] == spiSlaveAck.part.high_byte))
            slaveSyncFlag = 1;

        if (slaveSyncFlag == 1)
        {
            //Send count
            Uint32 i, j;
#ifdef PROFILER
            Uint16 alphaSendCnt = alphaCmdNum < 7 ?
                                  alpha_length(alphaSequenceSend[alphaCmdNum]) :
                                  0x004A; // special case for readPCEControl
#else

    #ifdef SENDSLC
            Uint16 alphaSendCnt = 1; //SlcSequenceSend[alphaCmdNum][0];
    #else
            Uint16 alphaSendCnt = alphaSequenceSend[alphaCmdNum][0];
    #endif
#endif
            Uint16 alphaResCnt = 0;  // pp change from 1

            printf("*************** receive Sync %x \n", *((Uint16*)gRxBuffer));

            memset(gRxBuffer, 0, 100);
            memset(gTxBuffer, 0, 100);

            //gTxBuffer[0] = alphaSendCnt;

            /* Set Transfer info */
            if (gSpiParams.dataSize == 8)
            {
                Uint32 ByteCnt = alphaSendCnt * 2;  // Byte count
                // Check the count for > 256
                if (ByteCnt < 256)
                {
                    gTxBuffer[0] = 0;
                    gTxBuffer[1] = (Uint8)(ByteCnt & 0x00FF);
                }
                else
                {
                    gTxBuffer[0] = (Uint8)((ByteCnt & 0x00FF) + 1 /* 1 Byte for extra count byte*/);
                    gTxBuffer[1] = (Uint8)((ByteCnt & 0xFF00) >> 8);
                }
            }
            else
            {  // TODO: Correct
                gTxBuffer[0] = (Uint8)(alphaSendCnt & 0x00FF);
                gTxBuffer[1] = (Uint8)((alphaSendCnt & 0xFF00) >> 8);
            }
            transaction.count = NUM_TRANSFER_ELEMENT(SPI_CMD_COUNT_SIZE); //1;
            transaction.txBuf = gTxBuffer;
            transaction.rxBuf = NULL;

#ifdef PROFILER
            Prfl_Stop(&profiler);
#endif
            SPI_transfer(gSpiHandle, &transaction);

#ifdef SPICALLBACK
            semReturn = Semaphore_pend(globalSem, ((transaction.count * 8) / Clock_tickPeriod) + 1);
            if (semReturn == Semaphore_PendState_TIMEOUT)
            {
                printf("write timeout \n");
                SPI_transferCancel(gSpiHandle);
                Semaphore_reset(globalSem, 0);
                continue;
            }
#endif
#ifdef PROFILER
            Prfl_Start(&profiler);
#endif

            // printf("receive data %x \n", *gRxBuffer);
            Task_sleep(1);

            // send paylod
            memset(gRxBuffer, 0, 100);
            memset(gTxBuffer, 0, 100);

            j = 0;

#ifdef PROFILER
            for (i = 0; i < (alphaSendCnt + 1); i++)
            {
                gTxBuffer[j] = alphaSequenceSend[alphaCmdNum][i] & (0x00FF); // low byte
                j++;
                gTxBuffer[j] = (alphaSequenceSend[alphaCmdNum][i] & (0xFF00)) >> 8;  // high byte
                j++;
            }
#else
    #ifdef SENDSLC
            for (i = 0; i < (alphaSendCnt + 1); i++)
            {
                gTxBuffer[j] = SlcSequenceSend[alphaCmdNum][i] & (0x00FF); // low byte
                j++;
                gTxBuffer[j] = (SlcSequenceSend[alphaCmdNum][i] & (0xFF00)) >> 8;  // high byte
                j++;
            }
    #else
            for (i = 1; i < (alphaSendCnt + 1); i++)
            {
                gTxBuffer[j] = alphaSequenceSend[alphaCmdNum][i] & (0x00FF); // low byte
                j++;
                gTxBuffer[j] = (alphaSequenceSend[alphaCmdNum][i] & (0xFF00)) >> 8;  // high byte
                j++;
            }
    #endif
#endif

            /* Set Transfer info */
            transaction.count = NUM_TRANSFER_ELEMENT(((alphaSendCnt)*2));
            transaction.txBuf = gTxBuffer;
            transaction.rxBuf = NULL;
#ifdef PROFILER
            Prfl_Stop(&profiler);
#endif

            SPI_transfer(gSpiHandle, &transaction);

#ifdef SPICALLBACK
            semReturn = Semaphore_pend(globalSem, ((transaction.count * 8) / Clock_tickPeriod) + 1);
            if (semReturn == Semaphore_PendState_TIMEOUT)
            {
                printf("write timeout \n");
                SPI_transferCancel(gSpiHandle);
                Semaphore_reset(globalSem, 0);
                continue;
            }
#endif
#ifdef PROFILER
            Prfl_Start(&profiler);
#endif
            //printf("receive data %x \n", *gRxBuffer);
            Task_sleep(1);

            // Get response from Slave
            // get res count
            do
            {
                memset(gRxBuffer, 0, SPI_RES_COUNT_SIZE);
                memset(gTxBuffer, 0, SPI_RES_COUNT_SIZE);

                /* Set Transfer info */
                transaction.count = NUM_TRANSFER_ELEMENT(SPI_RES_COUNT_SIZE);
                transaction.txBuf = gTxBuffer;
                transaction.rxBuf = gRxBuffer;
#ifdef PROFILER
                Prfl_Stop(&profiler);
#endif

                SPI_transfer(gSpiHandle, &transaction);

#ifdef SPICALLBACK
                semReturn = Semaphore_pend(globalSem, ((transaction.count * 8) / Clock_tickPeriod) + 1);
                if (semReturn == Semaphore_PendState_TIMEOUT)
                {
                    printf("write timeout \n");
                    SPI_transferCancel(gSpiHandle);
                    Semaphore_reset(globalSem, 0);
                    continue;
                }
#endif
#ifdef PROFILER
                Prfl_Start(&profiler);
#endif

                if (gSpiParams.dataSize == 8)
                {
                    alphaResCnt = (gRxBuffer[0] | (gRxBuffer[1] << 8));  // Byte count
//                  if ((gRxBuffer[1] & 0xFF) != 1)  // not a Null or SLS
//                  {
                        if (((gRxBuffer[1] & 0xFF) % 2) == 0)  // if LSB is even or not
                        {
                            DCS7_swap((DCS7_uint16*)&alphaResCnt, (DCS7_uint16*)gRxBuffer, 1);
                        }
                        if (alphaResCnt > 254)
                        {
                            alphaResCnt -= 1; // remove 1 byte for extended count field
                        }
//                  }
//                  else
//                  {
//                      alphaResCnt = SPI_RES_COUNT_SIZE;
//                  }
                }
                else
                {
                    alphaResCnt = (gRxBuffer[0] | (gRxBuffer[1] << 8)) *2;  // Byte count
                }

                //alphaResCnt = alphaResCnt;  // bytes count
                printf("waiting for reponse ..  \n");
               Task_sleep(1);
            } while (alphaResCnt == 0);

            printf("receive cnt data %x \n", NUM_TRANSFER_ELEMENT(alphaResCnt));
            memset(gRxBuffer, 0, alphaResCnt);
            memset(gTxBuffer, 0, alphaResCnt);

            transaction.count = NUM_TRANSFER_ELEMENT(alphaResCnt);
            transaction.txBuf = gTxBuffer;
            transaction.rxBuf = gRxBuffer;

#ifdef PROFILER
            Prfl_Stop(&profiler);
#endif
            SPI_transfer(gSpiHandle, &transaction);
#ifdef SPICALLBACK
            semReturn = Semaphore_pend(globalSem, ((transaction.count * 8) / Clock_tickPeriod) + 1);
            if (semReturn == Semaphore_PendState_TIMEOUT)
            {
                printf("write timeout \n");
                SPI_transferCancel(gSpiHandle);
                Semaphore_reset(globalSem, 0);
                continue;
            }
#endif

#ifdef PROFILER
            {
                // write porfiling info in file
                FILE *pStdout_profile =
                              fopen("..\\..\\test_vectors\\output\\"
                                    "dcs7_profile_spi_master.txt", "a");
                fprintf(pStdout_profile,
                        "Total Process Cycles %llu, "
                         "Peak Process Cycles %llu "
                         "Process Count %u",
                         profiler.total_process_cycles,
                         profiler.peak_process_cycles,
                         profiler.process_count);
                fprintf(pStdout_profile, "\n****************\n");
                fclose(pStdout_profile);
                // Clear profiling data
                Prfl_Init(&profiler);
                prlf_flag = 0;
                Task_sleep(5);  // wait for writing of profiling info into file at
                                // target side.
            }
#endif

            for (i = 0; i < alphaResCnt/2; ++i)
            {
               printf("receive res data %x \n", *(((Uint16*)gRxBuffer)+i));
            }

#ifndef SENDSLC
            // Verify received data
            if (memcmp(gRxBuffer, &alphaSequenceReceive[alphaCmdNum][1], alphaResCnt))
            {
                printf("Test FAILED: cmd index %d\n", alphaCmdNum);
#ifdef REGRESS_TEST
                while(1);  // Test failed
#endif
            }
            else
            {
                printf("Test PASSED \n");
            }
#endif

            printf("\n");
            Task_sleep(1);

            alphaCmdNum++;
        }
    }

    SPI_close(gSpiHandle);

    printf("\n Master Tx DONE \n");

    while(1);
}

/*
 *  ======== main ========
 */
int main(void)
{
    /* Call board init functions */
    Board_initSPI();

    /* Start BIOS */
    BIOS_start();
    return (0);

}

