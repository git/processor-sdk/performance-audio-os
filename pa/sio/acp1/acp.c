
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Standard Alpha Code Processor Algorithm functionality implementation
//
//
//

/*
 *  ACP Module - implements all functions and defines all constant
 *  structures common to all ACP Algorithm implementations.
 */
#include <xdc/std.h>
#include <alg.h>
#include "acp.h"

#include "acptype.h"
#if 0
/*
 *  ======== ACP_attach ========
 */
Int ACP_attach(ACP_Handle handle, Int series, Int beta, IALG_Status *pStatus)
{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->attach(handle, series, beta, pStatus);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

/*
 *  ======== ACP_apply ========
 */
Int ACP_apply(ACP_Handle handle, const ACP_Unit *from, ACP_Unit *to)
{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->apply(handle, from, to);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

/*
 *  ======== ACP_sequence ========
 */
Int ACP_sequence(ACP_Handle handle, const ACP_Unit *from, ACP_Unit *to)
{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->sequence(handle, from, to);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

/*
 *  ======== ACP_srecord ========
 */
Int ACP_srecord(ACP_Handle handle, char *from, char *to)
{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->srecord(handle, from, to);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

/*
 *  ======== ACP_escape ========
 */
Int ACP_escape(ACP_Handle handle, const ACP_Unit *from, ACP_Unit *to)
{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->escape(handle, from, to);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

/*
 *  ======== ACP_legacy ========
 */
Int ACP_legacy(ACP_Handle handle, const ACP_Unit *from, ACP_Unit *to)
{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->legacy(handle, from, to);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

/*
 *  ======== ACP_length ========
 */
Int ACP_length(ACP_Handle handle, const ACP_Unit *alpha)
{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->length(handle, alpha);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}

/*
 *  ======== ACP_htgnel ========
 */
Int ACP_htgnel(ACP_Handle handle, const ACP_Unit *alpha)
{
    Int returnValue;

    PAF_ALG_activate((IALG_Handle)handle);

    returnValue = handle->fxns->htgnel(handle, alpha);

    PAF_ALG_deactivate((IALG_Handle)handle);

    return ((Int)returnValue);
}
#endif
/*
 *  ======== ACP_exit ========
 *  Module finalization
 */
Void ACP_exit()
{
}

/*
 *  ======== ACP_init ========
 *  Module initialization
 */
Void ACP_init()
{
    extern void ACP_MDS_init ();
    ACP_MDS_init ();
}

