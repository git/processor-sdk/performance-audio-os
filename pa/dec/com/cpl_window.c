
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common PA Library -- common modulation across AAC and AC3
//
//
//

#include "cpl_window.h"

#if 1

void CPL_window_aac_ac3_(const float *dnmixptr1,const float *delayptr1, 
					const float *windptr1,int N,int mul,
                    float *restrict pcmptr1,float *restrict pcmptr2,int sign)
{
	int count;	
    const float *windptr2;

	float temp1,temp2;

/*	Window downmixed data */

	windptr2 = windptr1 + (N - 1);
	temp1 = (sign *(*delayptr1)) * (*windptr2);
	temp2 = (*dnmixptr1) * (*windptr2--);
	
	for (count = 0; count < N/(2*mul); count++)
	{
		*pcmptr1 = (temp1 - (*dnmixptr1) * (*windptr1)); 
		*pcmptr2 = ((sign) *(*delayptr1++) * (*windptr1++) + temp2);
		dnmixptr1 = dnmixptr1 - (sign);
		temp1 = (sign * (*delayptr1)) * (*windptr2);
		pcmptr1 += mul ;
		pcmptr2 -= mul ;	
		temp2 = (*dnmixptr1) * (*windptr2--);
	}
}


#else

void CPL_window_aac_ac3_(const float *dnmixptr1,const float *delayptr1, 
					const float *windptr1,int N,int mul,
                    float *restrict pcmptr1,float *restrict pcmptr2,
                    int sign,const float *windptr2)
{
	int count;	
//    const float *windptr2;

	float temp1,temp2;

/*	Window downmixed data */

//	windptr2 = windptr1 + (N - 1);
	temp1 = (*delayptr1) * (*windptr2);
	temp2 = (*dnmixptr1) * (*windptr2--);
	
	for (count = 0; count < N/(2*mul); count++)
	{
		*pcmptr1 = (sign) * (temp1 + (*dnmixptr1) * (*windptr1)); 
		*pcmptr2 = (sign) * ((*delayptr1) * (*windptr1++)- temp2) ;
		dnmixptr1 = dnmixptr1 - (sign);
		delayptr1 = delayptr1 - (sign);
		temp1 = (*delayptr1) * (*windptr2);
		pcmptr1 += mul ;
		pcmptr2 -= mul ;	
		temp2 = (*dnmixptr1) * (*windptr2--);
	}
}

#endif  
