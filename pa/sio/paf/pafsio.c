
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Common SIO code for MDS port drivers.
//
//
//

#include "fwkPort.h"

#include <xdc/std.h> //<std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Memory.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Queue.h> //<que.h>

#include <sio.h>

#include <pafsio.h>
#include "pafhjt.h"

#include "paf_heapMgr.h"

/* .......................................................................... */

//#include <log.h>
#include <stringp.h>

/* .......................................................................... */

#define MAX_DEVICE_NAME_LENGTH 32

typedef struct PAF_SIO_Entry
{
    Queue_Elem        link; //QUE_Elem          link;
    char              name[MAX_DEVICE_NAME_LENGTH];
    SIO_Handle        hSio;
    Int               inUse;
} PAF_SIO_Entry;

/* -------------------------------------------------------------------------- */

//static QUE_Handle hSioQue[2] =
static Queue_Handle hSioQue[2] =
{
    NULL,    /* Rx */
    NULL,    /* Tx */
};

/* -------------------------------------------------------------------------- */

Void PAF_SIO_free_( SIO_Handle hSio, int mode )
{
    Queue_Handle   hHoldingQue;     //QUE_Obj        holdingQue;
    Queue_Handle   hQue;            //QUE_Handle     hQue;
    PAF_SIO_Entry  *pEntry;
    Queue_Struct   holdingQue;

    if ((mode != SIO_INPUT) && (mode != SIO_OUTPUT))
        return;

    hQue = hSioQue[mode];
    if (!hQue)
        return;

    Queue_construct(&holdingQue, NULL); //QUE_new( &holdingQue );
    hHoldingQue = Queue_handle(&holdingQue);

    // locate entry
    while (!Queue_empty(hQue)) //while (!QUE_empty(hQue)) {
    {
        pEntry = (PAF_SIO_Entry *) Queue_dequeue(hQue); //pEntry = (PAF_SIO_Entry *) QUE_dequeue(hQue);
        Queue_enqueue( hHoldingQue, &pEntry->link ); //QUE_enqueue( &holdingQue, pEntry );

        if (hSio == pEntry->hSio)
            pEntry->inUse = 0;
    }

    /* replace entries from holding queue */
    while( !Queue_empty(hHoldingQue) ) //while( !QUE_empty(&holdingQue) )
    {
        Queue_enqueue( hQue, Queue_dequeue(hHoldingQue) ); //QUE_enqueue( hQue, QUE_dequeue(&holdingQue) );
    }

    return;
} // PAF_SIO_free_

/* -------------------------------------------------------------------------- */

#ifdef BOARD_E4
extern Int SDRAM;
#endif

SIO_Handle
PAF_SIO_recreate_( PAF_SIO_Params *pParams, int mode, LOG_Obj *pLog, int heapID )
{
    Int result;
    SIO_Handle hSio = NULL;
    PAF_SIO_Entry *pEntry;
    Queue_Handle   hHoldingQue; //QUE_Obj        holdingQue;
    Queue_Handle   hQue;        //QUE_Handle     hQue;
    char           tempName[MAX_DEVICE_NAME_LENGTH+5];
    Queue_Struct   holdingQue;
    Error_Block    eb;

    if ((mode != SIO_INPUT) && (mode != SIO_OUTPUT))
        return NULL;

    // Not supporting NULL devices
    if ((pParams == NULL) && (mode == SIO_INPUT))
        return NULL;

    // Initialize error block
    Error_init(&eb); 
    
    hQue = hSioQue[mode];
    if (!hQue) {
        hQue = Queue_create(NULL, NULL); //hQue = QUE_create( NULL );
        if (!hQue)
            return NULL;
        hSioQue[mode] = hQue;
    }

    Queue_construct(&holdingQue, NULL); //QUE_new( &holdingQue );    
    hHoldingQue = Queue_handle(&holdingQue);

    /* try to find handle which we can re-use, any such entries must necessarily
       have a valid device handle and name
    */
    while( !Queue_empty(hQue) ) //while( !QUE_empty(hQue) )
    {
        pEntry = (PAF_SIO_Entry *) Queue_dequeue(hQue); //pEntry = (PAF_SIO_Entry *) QUE_dequeue(hQue);
        Queue_enqueue( hHoldingQue, &pEntry->link ); //QUE_enqueue( &holdingQue, pEntry );

        if (pEntry->inUse)
            continue;

        if( !strcmp(pParams->sio.name, &pEntry->name) ) {
            hSio = pEntry->hSio;
            break;
        }
    }

    /* replace entries from holding queue */
    while( !Queue_empty(hHoldingQue) ) //while( !QUE_empty(&holdingQue) )
    {
        Queue_enqueue( hQue, Queue_dequeue(hHoldingQue) ); //QUE_enqueue( hQue, QUE_dequeue(&holdingQue) );
    }

    /* if no handle available create new one */
    if (!hSio) {
        SIO_Attrs sioAttrs;

        sioAttrs.nbufs = 1;
#ifndef BOARD_E4        
        sioAttrs.segid = heapID; /* don't really care since SIO_ISSUERECLAIM */
#else
        sioAttrs.segid = SDRAM;
#endif
        sioAttrs.align = 128;    /* don't really care since SIO_ISSUERECLAIM */
        sioAttrs.flush = FALSE; 
        sioAttrs.model = SIO_ISSUERECLAIM;
        sioAttrs.timeout = BIOS_WAIT_FOREVER; //sioAttrs.timeout = SYS_FOREVER;

        
        //pEntry = MEM_calloc( heapID, sizeof(PAF_SIO_Entry), 128 );
        pEntry = Memory_calloc((IHeap_Handle)pafHeapMgr_readHeapHandle(heapID), sizeof(PAF_SIO_Entry), 128, &eb);
        if (!pEntry)
            return NULL;

        // HACK? --> only stack if device name doesn't begin with "/"
        if (pParams->sio.name[0] != '/') {
            if (mode == SIO_OUTPUT) {
                strcpy (tempName, "/DOB/");
                strcat (tempName, pParams->sio.name);
            }
            else {
                strcpy (tempName, "/DIB/");
                strcat (tempName, pParams->sio.name);
            }
        }
        else
            strcpy (tempName, pParams->sio.name);

        hSio = SIO_create (tempName, mode, 0, &sioAttrs);
        if (!hSio)
            return NULL;

        strncpy( ((char *)pEntry->name), pParams->sio.name, MAX_DEVICE_NAME_LENGTH );

        pEntry->hSio = hSio;
        Queue_enqueue( hQue, &pEntry->link ); //QUE_enqueue( hQue, pEntry );
    }

    pEntry->inUse = 1;

    //this is redundant after the 1st time, but allows for better error handling
    result = SIO_ctrl( hSio, PAF_SIO_CONTROL_TRACE, (Arg) pLog );
    if (result)
        return NULL;

    return hSio;
} // PAF_SIO_recreate_

/* -------------------------------------------------------------------------- */

// stub -- for now
Int PAF_SIO_pllEnable_ (SIO_Handle hMaster, SIO_Handle hSlave, double seed)
{
    return 0;
} //PAF_SIO_pllEnable_

/* -------------------------------------------------------------------------- */

struct PAF_SIO_Fxns PAF_SIO_fxns = {
    PAF_SIO_free_,
    PAF_SIO_recreate_,
    PAF_SIO_pllEnable_,
};

/* .......................................................................... */
