/******************************************************************************
* Copyright (c) 2017, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// DA8xx SPI/I2C slave control status (DCS7) implementation
//
//
#include "dcs7.h"
#include "dcs7_priv.h"  // DCS internal function declarations
#include "dcs7_medialayer.h"

extern IHeap_Handle Heap;

const DCS7_Config DCS7_CONFIG =
{
    sizeof(DCS7_Config),                      /* Size of structure */
    (void*) &Heap,                            /* Object memmory segment */
    (void*) &Heap,                            /* Buffer memmory segment */
    DCS7_CONFIG_CACHEMODE_ENABLE,             /* Cache mode, Edable or Disable */
    0,                                        /* reserved */
    CACHE_L1D_LINESIZE,                       /* Buffer alignment */
    ASCII_SRECORD_START_SIZE 
    + ASCII_SRECORD_STOP_SIZE 
    + (ASCII_SRECORD_S1_MAX_SIZE * SRECORD_REC_MAX_S1_CNT),  /* Internal DCS medialayer receive buffer size */
    ASCII_SRECORD_START_SIZE 
    + ASCII_SRECORD_STOP_SIZE 
    + (ASCII_SRECORD_S1_MAX_SIZE * SRECORD_RES_MAX_S1_CNT),  /* Internal DCS medialayer Transmit buffer size */
};

/* DCS function structure */
const DCS7_Fxns DCS7_FXNS =
{
        DCS7_msToTicks,
        DCS7_semAlloc,
        DCS7_semPend,
        DCS7_semPendTimeout,
        DCS7_semPost,
        DCS7_semReset,
        DCS7_memAlloc,
        DCS7_taskDisable,
        DCS7_taskEnable,
        DCS7_configureInt,
        DCS7_cacheInv,
        DCS7_cacheWbInv,
        DCS7_errorInit,
        DCS7_errorAssert,
        DCS7_errorDeassert,
        DCS7_errorRecover,
        DCS7_slc,
        DCS7_read,
        DCS7_write,
        DCS7_configureSPI,
        DCS7_configureI2C,
        DCS7_configureUART,
        DCS7_configure,
        DCS7_resetSPI,
        DCS7_resetI2C,
        DCS7_resetUART,
        DCS7_reset,
        DCS7_resAllocMediaLayer,
        DCS7_resAllocSPI,
        DCS7_resAllocI2C,
        DCS7_resAllocUART,
        DCS7_resAlloc,
        DCS7_open,
        DCS7_ReadIssue,
        DCS7_WriteIssue
};

