/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef I2CRX_
#define I2CRX_

// The I2CRX service routine facilitates I2C receive transfer. The
// following Parameter Table is used to specify I2CRX transfer. 
//
// 31        24        16        8         0  
//  _______________________________________
// | RXPEND  | PROPEND | RES     | CTRL    |
// |_________|_________|_________|_________|
// |                  DRR                  |
// |_______________________________________|
// |                  BUF                  |
// |_______________________________________|
// | TCC     | TCINT   |       LEN         |
// |_________|_________|___________________|
// | PAYBIG  | PAYNUM  |       PAYCNT      |
// |_________|_________|___________________|
// |     FREELEN       |       WPTR        |
// |_________ _________|___________________|
// | RES1    | AVANUM  |       RPTR        |
// |_________|_________|___________________|
// | RES2    | PRONUM  |       PROLEN      |
// |_________|_________|___________________|
//

// I2CRX parameter field lengths and offsets
#define I2CRX_CTRL_OFF    0      // I2CRX ctrl offset
#define I2CRX_CTRL_LEN    1      // I2CRX ctrl length
#define I2CRX_RES_OFF     1      // I2CRX res offset
#define I2CRX_RES_LEN     1      // I2CRX res length
#define I2CRX_PROPEND_OFF 2      // I2CRX propend offset
#define I2CRX_PROPEND_LEN 1      // I2CRX propend length 
#define I2CRX_RXPEND_OFF  3      // I2CRX rxpend offset
#define I2CRX_RXPEND_LEN  1      // I2CRX rxpend length 
#define I2CRX_DRR_OFF     4      // I2CRX drr offset
#define I2CRX_DRR_LEN     4      // I2CRX drr length
#define I2CRX_BUF_OFF     8      // I2CRX buf offset
#define I2CRX_BUF_LEN     4      // I2CRX buf length
#define I2CRX_LEN_OFF     12     // I2CRX len offset
#define I2CRX_LEN_LEN     2      // I2CRX len length
#define I2CRX_TCINT_OFF   14     // I2CRX tcint offset
#define I2CRX_TCINT_LEN   1      // I2CRX tcint length
#define I2CRX_TCC_OFF     15     // I2CRX tcc offset
#define I2CRX_TCC_LEN     1      // I2CRX tcc length
#define I2CRX_PAYCNT_OFF  16     // I2CRX paycnt offset
#define I2CRX_PAYCNT_LEN  2      // I2CRX paycnt length
#define I2CRX_PAYNUM_OFF  18     // I2CRX paynum offset
#define I2CRX_PAYNUM_LEN  1      // I2CRX paynum length
#define I2CRX_PAYBIG_OFF  19     // I2CRX paybig offset
#define I2CRX_PAYBIG_LEN  1      // I2CRX paybig length 
#define I2CRX_WPTR_OFF    20     // I2CRX wptr offset
#define I2CRX_WPTR_LEN    2      // I2CRX wptr length
#define I2CRX_FREELEN_OFF 22     // I2CRX freelen offset
#define I2CRX_FREELEN_LEN 2      // I2CRX freelen length
#define I2CRX_RPTR_OFF    24     // I2CRX rptr offset
#define I2CRX_RPTR_LEN    2      // I2CRX rptr length
#define I2CRX_AVANUM_OFF  26     // I2CRX avanum offset
#define I2CRX_AVALEN_LEN  1      // I2CRX avanum length
#define I2CRX_RES1_OFF    27     // I2CRX res1 offset
#define I2CRX_RES1_LEN    1      // I2CRX res1 length
#define I2CRX_PROLEN_OFF  28     // I2CRX prolen offset
#define I2CRX_PROLEN_LEN  2      // I2CRX prolen length
#define I2CRX_PRONUM_OFF  30     // I2CRX pronum offset
#define I2CRX_PRONUM_LEN  1      // I2CRX pronum length
#define I2CRX_RES2_OFF    31     // I2CRX res1 offset
#define I2CRX_RES2_LEN    1      // I2CRX res1 length

#endif // I2XRX_
