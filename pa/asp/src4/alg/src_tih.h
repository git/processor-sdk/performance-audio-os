
/*
*  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
*  ALL RIGHTS RESERVED 
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/*
 *  Vendor specific (TIH) interface header for Synchronous Rate Conversion 
 *  Algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular SRC implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef SRC_TIH_
#define SRC_TIH_

#include <ti/xdais/ialg.h>

#include "isrc.h"
#include "icom.h"

/*
 *  ======== SRC_TIH_exit ========
 *  Required module finalization function
 */
#define SRC_TIH_exit COM_TII_exit

/*
 *  ======== SRC_TIH_init ========
 *  Required module initialization function
 */
#define SRC_TIH_init COM_TII_init

/*
 *  ======== SRC_TIH_IALG ========
 *  TIH's implementation of SRC's IALG interface
 */
extern IALG_Fxns SRC_TIH_IALG; 

/*
 *  ======== SRC_TIH_ISRC ========
 *  TIH's implementation of SRC's ISRC interface
 */
extern const ISRC_Fxns SRC_TIH_ISRC; 


/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of TIH's Synchronous Rate 
 *  Conversion Algorithm. However, other implementation specific operations 
 *  can also be added.
 */

/*
 *  ======== SRC_TIH_Handle ========
 */
typedef struct SRC_TIH_Obj *SRC_TIH_Handle;

/*
 *  ======== SRC_TIH_Params ========
 *  We don't add any new parameters to the standard ones defined by ISRC.
 */
typedef ISRC_Params SRC_TIH_Params;

/*
 *  ======== SRC_TIH_Status ========
 *  We don't add any new status to the standard one defined by ISRC.
 */
typedef ISRC_Status SRC_TIH_Status;

/*
 *  ======== SRC_TIH_Config ========
 *  We don't add any new config to the standard one defined by ISRC.
 */
typedef ISRC_Config SRC_TIH_Config;

typedef struct SRC_TIH_Obj {
    IALG_Obj alg;           /* MUST be first field of all XDAS algs */
    int mask;
    SRC_TIH_Status *pStatus; /* public interface */
    SRC_TIH_Config config;   /* private interface */
    void *pDummy;       // CPL_TII_Fxns   *cplFxns;    /* CPL function table */
} SRC_TIH_Obj;

/*
 *  ======== SRC_TIH_PARAMS ========
 *  Define our default parameters.
 */
#define SRC_TIH_PARAMS        ISRC_PARAMS
#define SRC_TIH_PARAMS_MAX192 ISRC_PARAMS_MAX192
#define SRC_TIH_PARAMS_MAX48  ISRC_PARAMS_MAX48
#define SRC_TIH_PARAMS_MIN32  ISRC_PARAMS_MIN32

/*
 *  ======== SRC_TIH_create ========
 *  Create a SRC_TIH instance object.
 */
extern SRC_TIH_Handle SRC_TIH_create(const SRC_TIH_Params *params);

/*
 *  ======== SRC_TIH_delete ========
 *  Delete a SRC_TIH instance object.
 */
#define SRC_TIH_delete COM_TII_delete

#endif  /* SRC_TIH_ */
