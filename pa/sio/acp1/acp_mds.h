
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Vendor-specific (MDS) Alpha Code Processor Algorithm interface declarations
//
//
//

/*
 *  Vendor specific (MDS) interface header for ACP Algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  and minimal overhead at the expense of being tied to a
 *  particular ACP implementation.
 *
 *  This header only contains declarations that are specific
 *  to this implementation.  Thus, applications that do not
 *  want to be tied to a particular implementation should never
 *  include this header (i.e., it should never directly
 *  reference anything defined in this header.)
 */
#ifndef ACP_MDS_
#define ACP_MDS_

#include <ti/xdais/ialg.h>

#include <iacp.h>
#include <icom.h>

/*
 *  ======== ACP_MDS_exit ========
 *  Required module finalization function
 */
#define ACP_MDS_exit COM_TII_exit

/*
 *  ======== ACP_MDS_init ========
 *  Required module initialization function
 */
extern Void ACP_MDS_init(Void);

/*
 *  ======== ACP_MDS_IALG ========
 *  MDS's implementation of ACP's IALG interface
 */
extern IALG_Fxns ACP_MDS_IALG; 

/*
 *  ======== ACP_MDS_IACP ========
 *  MDS's implementation of ACP's IACP interface
 */
extern IACP_Fxns ACP_MDS_IACP; 


/*
 *  ======== Vendor specific methods  ========
 *  The remainder of this file illustrates how a vendor can
 *  extend an interface with custom operations.
 *
 *  The operations below simply provide a type safe interface 
 *  for the creation, deletion, and application of MDS's ACP Algorithm.
 *  However, other implementation specific operations can also
 *  be added.
 */

/*
 *  ======== ACP_MDS_Handle ========
 */
typedef struct ACP_MDS_Obj *ACP_MDS_Handle;

/*
 *  ======== ACP_MDS_Params ========
 *  We don't add any new parameters to the standard ones defined by IACP.
 */
typedef IACP_Params ACP_MDS_Params;

/*
 *  ======== ACP_MDS_Status ========
 *  We don't add any new status to the standard one defined by IACP.
 */
typedef IACP_Status ACP_MDS_Status;

/*
 *  ======== ACP_MDS_Config ========
 *  We don't add any new config to the standard one defined by IACP.
 */
typedef IACP_Config ACP_MDS_Config;

/*
 *  ======== ACP_MDS_PARAMS ========
 *  Define our default parameters.
 */
#define ACP_MDS_PARAMS   IACP_PARAMS
#define ACP_MDS_PARAMS_SRECORD_SUPPORT   IACP_PARAMS_SRECORD_SUPPORT

/*
 *  ======== ACP_MDS_create ========
 *  Create a ACP_MDS instance object.
 */
extern ACP_MDS_Handle ACP_MDS_create(const ACP_MDS_Params *params);

/*
 *  ======== ACP_MDS_delete ========
 *  Delete a ACP_MDS instance object.
 */
#define ACP_MDS_delete COM_TII_delete

#endif  /* ACP_MDS_ */
