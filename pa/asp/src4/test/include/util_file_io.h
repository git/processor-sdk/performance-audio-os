/*
 *  Copyright {C} 2016 Texas Instruments Incorporated - http://www.ti.com/ 
 *  ALL RIGHTS RESERVED 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *  @file   util_file_io.h
 *
 *  @brief  This file contains the common declarations for operations
 *          used to read/write audio data from files.
 *
 *          Presently, .wav files are only supported (32bit PCM).
 *
 *  @todo   Further modifications are required to handle metadata and read
 *          encoded (.mlp, .mat, .ec3 and etc.) file.
 *
 *****************************************************************************/
#ifndef __UTIL_FILE_IO_H__
#define __UTIL_FILE_IO_H__

/************************************************************
                     Include files
*************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/************************************************************
                      Macro Definitions
*************************************************************/
#define RETRUN_SUCCESS 0
#define RETRUN_ERROR   -1

#ifndef PAF_AudioData
typedef float PAF_AudioData;
#endif

/************************************************************
                     Enum's & Struct's
*************************************************************/
/**
 *  @brief .wav file header format
 */
typedef struct
{
    char chunk_id[4];
    int chunk_size;
    char format[4];
    char subchunk1_id[4];
    int subchunk1_size;
    short int audio_format;
    short int num_channels;
    int sample_rate;
    int byte_rate;
    short int block_align;
    short int bits_per_sample;
    char subchunk2_id[4];
    int subchunk2_size;
} header, *header_p;

/**
 *  @brief Structure for audio file.
 *         Consist of parameters used for audio file operations.
 */
typedef struct
{
    char chunk_id[4];
    int chunk_size;
    char format[4];
    char subchunk1_id[4];
    int subchunk1_size;
}header_1, *header_1p;
typedef struct
{
    char subchunk2_id[4];
    int subchunk2_size;
} header_2, *header_2p;

typedef struct{
    short int audio_format;
    short int num_channels;
    int sample_rate;
    int byte_rate;
    short int block_align;
    short int bits_per_sample;
}subchunk_data;

typedef struct
{
  /**< Input FILE pointer */
  FILE          *inFile;
  /**< Output FILE pointer */
  FILE          *outFile;
  /**< Buffer to store input audio data (non interleaved), before processing */
  PAF_AudioData *inBuf;
  /**< Buffer to store output audio data (non interleaved), after processing */
  PAF_AudioData *outBuf;
  /**< Requested number of channels */
  int           bufChannelCount;
  /**< Requested number of samples of input file*/
  int           inBufSampleCount;
  /**< Requested number of samples of output file*/
  int           outBufSampleCount;
  /**< Requested output sample rate */
  int           outputSampleRate;
  /**< Common temporary buffer to store interleaved data */
  unsigned char *tempBuf;
  /**< Input file header structure */
  header_1      header_1_data;
  char          subchunk_data[50];
  header_2      header_2_data;
} tFIOHandle;

/************************************************************
                Function Declarations
*************************************************************/
/**
 *  @brief     Initialization
 *
 *  @param[in] file      Handle to audio files
 *
 *  @param[in] inFile    Input file name
 *
 *  @param[in] outFile   Output file name
 *
 *  @retval    RETRUN_SUCCESS on Success, RETRUN_ERROR on error
 *
 *  @remark    Read input/ouput files specified and get the input file info
 *
 */
int FIO_Init(tFIOHandle *file, char *inFile, char *outFile);

/**
 *  @brief     Configuration call
 *
 *  @param[in] file             Handle to audio files
 *
 *  @param[in] bufChannelCount  Requested number of audio channels in buffers
 *
 *  @param[in] inBufSampleCount   Requested number of samples per
 *                                channel in buffers
 *
 *  @retval    RETRUN_SUCCESS on Success, RETRUN_ERROR on error
 *
 *  @remark    Configure the requested parameters for future use.
 *             Allocate the memory required for input/ouput buffers as per
 *             number of channels and samples requested.
 *
 *             The data will be provided in inBuf and outBuf as per these
 *             configuration.
 *
 */
int FIO_Config(tFIOHandle *file, int bufChannelCount);

/**
 *  @brief     Data read from input file
 *
 *  @param[in] file   Handle to audio files
 *
 *  @retval    RETRUN_SUCCESS on Success, RETRUN_ERROR on error/end of file
 *
 *  @remark    Read data from input file and arrange (non interleaved)
 *             it in input buffer as configured for number
 *             of channels and samples.
 *
 */
int FIO_Read(tFIOHandle *file);

/**
 *  @brief     Data write to output file
 *
 *  @param[in] file   Handle to audio files
 *
 *  @retval    RETRUN_SUCCESS on Success, RETRUN_ERROR on error
 *
 *  @remark    Write data present in output buffer to output file (interleaved).
 *
 */
int FIO_Write(tFIOHandle *file);

/**
 *  @brief     Deinitialization
 *
 *  @param[in] file   Handle to audio files
 *
 *  @retval    RETRUN_SUCCESS on Success, RETRUN_ERROR on error
 *
 *  @remark    free the allocated memory for buffers and close the input/ouput files.
 *
 */
int FIO_DeInit(tFIOHandle *file);

#endif  /* __UTIL_FILE_IO_H__ */
/***************************End of file***************************************/
