*
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*
*  ======== iir2tap2ch_c2_i_df2.sa ========
*  IIR filter implementation(DF2) for tap-2, channels-2, SP, cascade 2, inplace.
*
*
 
************************ IIR FILTER *****************************************
****************** ORDER 2, CHANNELS 2, CASCADE 2 ***************************
*Single section equations :                                                 *
*                                                                           *
* Filter equation  : H(z) =  1  + b1*z~1 + b2*z~2                           *
*                           ----------------------                          *
*                            1  - a1*z~1 - a2*z~2                           *
*                                                                           *
*   Direct form    : y(n) = x(n) + b1*x(n-1) + a1*y(n-1) + a2*y(n-2)        *
*                                                                           *
*   Canonical form : w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                    *
*                    y(n) = w(n) + b1*w(n-1) + b2*w(n-2)                    *
*                                                                           *
*                                                                           *
* Implementation structure of one cascade section                           *
*           g                   w(n)                                        *
*     x(n)-->---[+]---->--------@-------------[+]--->--y(n)                 *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a1    +-----+    b1     |                            *
*               [+]----<<----| Z~1 |---->>----[+]                           *
*                |           +-----+           |                            *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a2    +-----+    b2     |                            *
*                 ----<<-----| Z~1 |---->>-----                             *
*                            +-----+                                        *
*                                                                           *
* The input gain(b0) of all sections are made into a common input gain.     *
*****************************************************************************
* Note: C equivalent of Serial Assembly(SA) code are given as comments.     *

            .global _Filter_iirT2Ch2_c2_i_df2_sub ;Declared as Global function 
            .sect ".text:Filter_iirT2Ch2_c2_i_df2" ;Memory section for the function 
            
_Filter_iirT2Ch2_c2_i_df2_sub: .cproc pParam ;Filter_iirT2Ch2_c2_i_df2_sub(*pParam), C callable fn

             .no_mdep ;no memory aliasing in this function
             
************* General registers *******************************
             .reg   yL ;O/p ptr   
             .reg   xL ;I/p ptr
             .reg   yR ;O/p ptr   
             .reg   xR ;I/p ptr
             .reg   filtVarsL ;Var ptr 
             .reg   filtCfsL ;Coeff ptr  
             .reg   filtVarsR ;Var ptr 
             .reg   filtCfsR ;Coeff ptr              
             .reg   res1, res2, res3, res4, res5 ;Temp accumulators 
             .reg   pIn ;Ptr to, array of ch i/p  ptrs
             .reg   pOut ;Ptr to, array of ch o/p  ptrs
             .reg   pCoef ;Ptr to, array of ch o/p  ptrs
             .reg   pVar ;Ptr to, array of ch var  ptrs
             .reg   count ;Sample counter              

************* Coefficient registers(Few are register pairs) ********             
             .reg   Gain_R:Gain_L, CfsA1_L1:CfsB1_L1, CfsA2_L1:CfsB2_L1 ;Section 1(Left)
             .reg   CfsB1_L2, CfsA1_L2, CfsB2_L2, CfsA2_L2 ;Section 2(Left)
             .reg   CfsA1_R1:CfsB1_R1, CfsA2_R1:CfsB2_R1 ;Section 1(Right)
             .reg   CfsA1_R2:CfsB1_R2, CfsA2_R2:CfsB2_R2 ;Section 2(Right) 

************* Filter state registers *******************************             
             .reg   w1_A_L, w2_A_L 
             .reg   w1_B_L, w2_B_L 
             .reg   w1_A_R, w2_A_R 
             .reg   w1_B_R, w2_B_R   

             .reg   accum1 ;O/p accumulator register              
             .reg   accum2 ;O/p accumulator register 
             .reg   accum3 ;O/p accumulator register 
             .reg   accum4 ;O/p accumulator register                          

             .reg   input_dataL ;I/p data reg(Left)
             .reg   input_dataR ;I/p data reg(Right)
             
             LDW    *pParam[0],         pIn   ;pIn   = pParam[0]
             LDW    *pParam[1],         pOut  ;pOut  = pParam[1]
             LDW    *pParam[2],         pCoef ;pCoef = pParam[2]
             LDW    *pParam[3],         pVar  ;pVar  = pParam[3]
             LDW    *pParam[4],         count ;count = pParam[4]
             
             ;Left
             LDW    *pIn[0],            xL ;xL = pIn[0]
             LDW    *pOut[0],           yL ;yL = pOut[0] 
             LDW    *pCoef[0],          filtCfsL ;filtCfsL = pCoef[0]
             LDW    *pVar[0],           filtVarsL ;filtVarsL = pVar[0]
                                        
             ;Right
             LDW    *pIn[1],            xR ;xR = pIn[0]
             LDW    *pOut[1],           yR ;yR = pOut[0] 
             LDW    *pCoef[1],          filtCfsR ;filtCfsR = pCoef[0]
             LDW    *pVar[1],           filtVarsR ;filtVarsR = pVar[0]  
                                          
             ;Get filter states into regs
             LDW    *filtVarsL[0],      w1_A_L ;w1(n-1) = filtVarsL[0] 
             LDW    *filtVarsL[1],      w2_A_L ;w1(n-2) = filtVarsL[1]           
                                                                        
             LDW    *filtVarsL[2],      w1_B_L ;w2(n-1) = filtVarsL[2]            
             LDW    *filtVarsL[3],      w2_B_L ;w2(n-2) = filtVarsL[3]           
                                                                        
             LDW    *filtVarsR[0],      w1_A_R ;w3(n-1) = filtVarsR[0]            
             LDW    *filtVarsR[1],      w2_A_R ;w3(n-2) = filtVarsR[1]           
                                                                        
             LDW    *filtVarsR[2],      w1_B_R ;w4(n-1) = filtVarsR[2]            
             LDW    *filtVarsR[3],      w2_B_R ;w4(n-2) = filtVarsR[3]                        

             ; Gain
             LDW    *filtCfsL[0],       Gain_L ;Gain_L = filtCfsL[0]
             LDW    *filtCfsL[1],       Gain_R ;Gain_R = filtCfsL[1] 

             ;Stage 1
             LDW    *filtCfsL[2],       CfsB1_L1 ;CfsB1_L1 = filtCfsL[2]           
             LDW    *filtCfsL[3],       CfsA1_L1 ;CfsA1_L1 = filtCfsL[3]           
             LDW    *filtCfsL[4],       CfsB2_L1 ;CfsB2_L1 = filtCfsL[4]                      
             LDW    *filtCfsL[5],       CfsA2_L1 ;CfsA1_L1 = filtCfsL[5]           
             ;Stage 2
             LDW    *filtCfsL[6],       CfsB1_L2 ;CfsB1_L2 = filtCfsL[6]           
             LDW    *filtCfsL[7],       CfsA1_L2 ;CfsA1_L2 = filtCfsL[7]                  
             LDW    *filtCfsL[8],       CfsB2_L2 ;CfsB2_L2 = filtCfsL[8]                             
             LDW    *filtCfsL[9],       CfsA2_L2 ;CfsA2_L2 = filtCfsL[9]                   
             ;Stage 3
             LDW    *filtCfsL[10],      CfsB1_R1 ;CfsB1_R1 = filtCfsL[10]
             LDW    *filtCfsL[11],      CfsA1_R1 ;CfsA1_R1 = filtCfsL[11]                  
             LDW    *filtCfsL[12],      CfsB2_R1 ;CfsB2_R1 = filtCfsL[12]                
             LDW    *filtCfsL[13],      CfsA2_R1 ;CfsA2_R1 = filtCfsL[13]                   
             ;Stage 4
             LDW    *filtCfsL[14],      CfsB1_R2 ;CfsB1_R2 = filtCfsL[14]
             LDW    *filtCfsL[15],      CfsA1_R2 ;CfsA1_R2 = filtCfsL[15]                  
             LDW    *filtCfsL[16],      CfsB2_R2 ;CfsB2_R2 = filtCfsL[16]                
             LDW    *filtCfsL[17],      CfsA2_R2 ;CfsA2_R2 = filtCfsL[17]                  

LOOP:        ;.trip 16, 2048, 4
                                           
************* Stage 1 ******************************* L 
 
             ;Get coeffs into regs           
             LDDW   *filtCfsL[1],       CfsA1_L1:CfsB1_L1 ;CfsA1_L1:CfsB1_L1 = filtCfsL[1]   
             LDDW   *filtCfsL[0],       Gain_R:Gain_L     ;Gain_R:Gain_L     = filtCfsL[0]               
             LDDW   *filtCfsL[2],       CfsA2_L1:CfsB2_L1 ;CfsA2_L1:CfsB2_L1 = filtCfsL[2]                

             ;a1*w(n-1) & b1*w(n-1)                                                
             MPYSP  w1_A_L,             CfsA1_L1,           res1 ;res1 =  w1_A_L*CfsA1_L1
             MPYSP  w1_A_L,             CfsB1_L1,           res3 ;res3 =  w1_A_L*CfsB1_L1             
                                                                     
             ;Get one input_sample*gain             
             LDW    *xL,                input_dataL ;input_data = *x       
             MPYSP  Gain_L,             input_dataL,        input_dataL ; input_dataL *= Gain_L
             
             ;a2*w(n-2) & b2*w(n-2) 
             MPYSP  w2_A_L,             CfsA2_L1,           res2 ;res2 = w2_A_L*CfsA2_L1
             MPYSP  w2_A_L,             CfsB2_L1,           res4 ;res4 = w2_A_L*CfsB2_L1

             ;Shift filter states
             MV     w1_A_L,             w2_A_L ;w2_A_L = w1_A_L

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  input_dataL,        res2,               res2 ;res2 += input_dataL
             ADDSP  res1,               res2,               w1_A_L ;w1_A_L = res1 + res2
             
             ;y(n) = b0*w(n) + 'b1*w(n-1)' + 'b2*w(n-2)' 
             ADDSP  res4,               res3,               accum1 ;accum1 = res3 + res4
             ADDSP  accum1,             w1_A_L,             accum1 ;accum1 += w1_A_L

************* Stage 2 *******************************
             
             ;Get coeffs into regs  
             LDW    *filtCfsL[6],       CfsB1_L2 ;CfsB1_L2 = filtCfsL[6]   
             LDW    *filtCfsL[8],       CfsB2_L2 ;CfsB2_L2 = filtCfsL[8]                       
             LDW    *filtCfsL[7],       CfsA1_L2 ;CfsA1_L2 = filtCfsL[7]                                     
             LDW    *filtCfsL[9],       CfsA2_L2 ;CfsA2_L2 = filtCfsL[9]                       
                          
             ;a1*w(n-1) & b1*w(n-1)
             MPYSP  w1_B_L,             CfsA1_L2,           res1 ;res1 = w1_B_L*CfsA1_L2
             MPYSP  w1_B_L,             CfsB1_L2,           res3 ;res3 = w1_B_L*CfsB1_L2             

             ;a2*w(n-2) & b2*w(n-2)
             MPYSP  w2_B_L,             CfsA2_L2,           res2 ;res2 = w2_B_L*CfsA2_L2
             MPYSP  w2_B_L,             CfsB2_L2,           res4 ;res4 = w2_B_L*CfsB2_L2
             
             ;Shift filter states
             MV     w1_B_L,             w2_B_L ;w2_B_L = w1_B_L

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  accum1,             res2,               res2   ;res2 += input_data
             ADDSP  res1,               res2,               w1_B_L ;w1_B_L = res1 + res2

             ;y(n) = b0*w(n) + 'b1*w(n-1)' + 'b2*w(n-2)'
             ADDSP  res4,               res3,               accum2 ;accum2 = res3 + res4
             ADDSP  accum2,             w1_B_L,             accum2 ;accum2 += w1_B_L

             ;Store the calculated o/p      
             STW    accum2,             *xL++ ;*xL++ = accum2                       
             
************* Stage 1 ******************************* R
             
             ;Get coeffs into regs  
             LDDW   *filtCfsL[5],       CfsA1_R1:CfsB1_R1 ;CfsA1_R1:CfsB1_R1 = filtCfsL[5]   
             LDDW   *filtCfsL[6],       CfsA2_R1:CfsB2_R1 ;CfsA2_R1:CfsB2_R1 = filtCfsL[6]                 
             
             ;a1*w(n-1) & b1*w(n-1)
             MPYSP  w1_A_R,             CfsA1_R1,           res1 ;res1 = w1_A_R*CfsA1_R1
             MPYSP  w1_A_R,             CfsB1_R1,           res3 ;res3 = w1_A_R*CfsB1_R1             

             ;Get one input sample             
             LDW    *xR,                input_dataR ;input_data = *x       
             MPYSP  Gain_R,             input_dataR,        input_dataR ; input_dataR *= Gain_R
             
             ;a2*w(n-2) & b2*w(n-2)
             MPYSP  w2_A_R,             CfsA2_R1,           res2 ;res2 = w2_A_R*CfsA2_R1
             MPYSP  w2_A_R,             CfsB2_R1,           res4 ;res4 = w2_A_R*CfsB2_R1

             ;Shift filter states
             MV     w1_A_R,             w2_A_R ;w2_A_R = w1_A_R

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  input_dataR,        res2,               res2 ;res2 += input_dataR
             ADDSP  res1,               res2,               w1_A_R ;w1_A_R = res1 + res2

             ;y(n) = b0*w(n) + 'b1*w(n-1)' + 'b2*w(n-2)'
             ADDSP  res4,               res3,               accum3 ;accum3 = res3 + res4
             ADDSP  accum3,             w1_A_R,             accum3 ;accum3 += w1_A_R

************* Stage 2 *******************************
             
             ;Get coeffs into regs  
             LDDW   *filtCfsL[7],       CfsA1_R2:CfsB1_R2 ;CfsA1_R2:CfsB1_R2 = filtCfsL[7]   
             LDDW   *filtCfsL[8],       CfsA2_R2:CfsB2_R2 ;CfsA2_R2:CfsB2_R2 = filtCfsL[8]                 
             
             ;a1*w(n-1) & b1*w(n-1)
             MPYSP  w1_B_R,             CfsA1_R2,           res1 ;res1 = w1_B_R*CfsA1_R2
             MPYSP  w1_B_R,             CfsB1_R2,           res3 ;res3 = w1_B_R*CfsB1_R2                         
             
             ;a2*w(n-2) & b2*w(n-2)
             MPYSP  w2_B_R,             CfsA2_R2,           res2 ;res2 = w2_B_R*CfsA2_R2
             MPYSP  w2_B_R,             CfsB2_R2,           res4 ;res4 = w2_B_R*CfsB2_R2            

             ;Shift filter states
             MV     w1_B_R,             w2_B_R ;w2_B_R = w1_B_R

             ;w(n) = x(n) + 'a1*w(n-1)' + 'a2*w(n-2)'
             ADDSP  accum3,             res2,               res2 ;res2 += input_data
             ADDSP  res1,               res2,               w1_B_R ;w1_B_R = res1 + res2

             ;y(n) = b0*w(n) + 'b1*w(n-1)' + 'b2*w(n-2)'
             ADDSP  res4,               res3,               accum4 ;accum3 = res3 + res4
             ADDSP  accum4,             w1_B_R,             accum4 ;accum3 += w1_B_R

             ;Store the calculated o/p      
             STW    accum4,             *xR++ ;*xR++ = accum4                       
                          
********************************************  
           
             SUB    count,              1,                  count ;count = count - 1
   [count]   B      LOOP ; if(count != 0) goto LOOP                         
   
             ;Update filter state memory
             STW    w1_A_L,             *filtVarsL[0] ;filtVarsL[0] = w1_A_L
             STW    w2_A_L,             *filtVarsL[1] ;filtVarsL[1] = w2_A_L          
             STW    w1_B_L,             *filtVarsL[2] ;filtVarsL[2] = w1_B_L          
             STW    w2_B_L,             *filtVarsL[3] ;filtVarsL[3] = w2_B_L          
             ;Update filter state memory
             STW    w1_A_R,             *filtVarsR[0] ;filtVarsR[0] = w1_A_L
             STW    w2_A_R,             *filtVarsR[1] ;filtVarsR[1] = w2_A_L          
             STW    w1_B_R,             *filtVarsR[2] ;filtVarsR[2] = w1_B_L          
             STW    w2_B_R,             *filtVarsR[3] ;filtVarsR[3] = w2_B_L          

             .return count ;return(count)
             
             .endproc ;C callable function, directive

*******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss

	.sect	".text:Filter_iirT2Ch2_c2_i_df2"
	.global	_Filter_iirT2Ch2_c2_i_df2

;******************************************************************************
;* FUNCTION NAME: _Filter_iirT2Ch2_c2                                         *
;******************************************************************************
_Filter_iirT2Ch2_c2_i_df2:
;** --------------------------------------------------------------------------*

           MV      .D1     A4,A10             
||         STW     .D2T1   A10,*SP--(104)     
||         MV      .S1X    SP,A9              

           LDW     .D1T1   *+A10(8),A3        
           STW     .D1T1   A14,*-A9(20)
           STW     .D2T2   B13,*+SP(100)
           STW     .D2T2   B12,*+SP(96)
           STW     .D2T2   B11,*+SP(92)
           LDW     .D1T1   *+A3(4),A0         
           STW     .D2T2   B10,*+SP(88)
           MV      .S1X    B3,A14
           LDW     .D1T2   *A3,B10            
           NOP             1
           LDW     .D1T1   *+A0(28),A5        
           LDW     .D1T2   *A0,B2             
           LDW     .D1T1   *+A0(20),A6        
           LDW     .D1T1   *+A0(24),A7        
           LDW     .D1T1   *+A0(32),A8        
           LDW     .D1T2   *+A0(4),B3         
           STW     .D2T1   A5,*+SP(68)        
           STW     .D2T2   B2,*+SP(12)        
           STW     .D2T1   A6,*+SP(64)        

           STW     .D2T1   A7,*+SP(72)        
||         LDW     .D1T2   *+A0(8),B12        

           STW     .D2T1   A8,*+SP(76)        
||         LDW     .D1T2   *+A0(12),B11       

           LDW     .D2T2   *+B10(12),B4       
           STW     .D2T2   B3,*+SP(48)        
           LDW     .D2T2   *+B10(8),B6        
           LDW     .D2T2   *+B10(16),B7       
           LDW     .D2T2   *+B10(20),B8       
           LDW     .D2T2   *+B10(4),B5        
           STW     .D2T2   B12,*+SP(56)       
           LDW     .D2T2   *+B10(28),B9       
           LDW     .D2T2   *+B10(24),B0       
           LDW     .D2T2   *+B10(32),B1       
           STW     .D2T2   B11,*+SP(52)       
           LDW     .D2T2   *B10,B13           
           STW     .D2T2   B4,*+SP(20)        
           STW     .D2T2   B6,*+SP(24)        

           MVKL    .S1     _Filter_iirT2Ch2_c2_i_df2_sub,A5  
||         STW     .D2T2   B7,*+SP(28)        

           MVKH    .S1     _Filter_iirT2Ch2_c2_i_df2_sub,A5 
||         STW     .D2T2   B8,*+SP(32)        

           STW     .D2T2   B5,*+SP(16)        
||         LDW     .D1T1   *+A0(16),A0        
||         CALL    .S2X    A5                 

           STW     .D2T2   B9,*+SP(36)        
           STW     .D2T2   B0,*+SP(40)        
           STW     .D2T2   B1,*+SP(44)        

           MVKL    .S2     RL0,B3             
||         ADD     .L2     8,SP,B4            
||         STW     .D2T2   B13,*+SP(8)        

           MVKH    .S2     RL0,B3             
||         STW     .D2T1   A0,*+SP(60)        
||         STW     .D1T2   B4,*A3             

RL0:       ; CALL OCCURS                      
           LDW     .D1T1   *+A10(8),A0        
           MV      .S1X    SP,A9              
           MV      .S2X    A14,B3             
           LDW     .D1T1   *+A9(84),A14       
           ZERO    .D1     A4                 
           STW     .D1T2   B10,*A0            
           LDDW    .D2T2   *+SP(96),B13:B12   

           RET     .S2     B3                 
||         LDDW    .D2T2   *+SP(88),B11:B10   

           LDW     .D2T1   *++SP(104),A10     
           NOP             4
           ; BRANCH OCCURS                    

;Equivalent C code for the re-allocation of coeffs into stack area

;#pragma CODE_SECTION(Filter_iirT2Ch2_c2_i_df2, ".text:Filter_iirT2Ch2_c2_i_df2")
;Int Filter_iirT2Ch2_c2_i_df2( PAF_FilParam *pParam ) 
;{ 
;    double coef_array[9];
;    float *coef_ptr = (float *)coef_array;
;    float *coef;
    
;    float *coef_temp;
    
;    coef_temp    = (Float *)pParam->pCoef[0];    
;    coef_ptr[0]  = coef_temp[0]; // gain
;    coef_ptr[2]  = coef_temp[1]; // b1
;    coef_ptr[3]  = coef_temp[3]; // a1
;    coef_ptr[4]  = coef_temp[2]; // b2       
;    coef_ptr[5]  = coef_temp[4]; // a2
;    coef_ptr[6]  = coef_temp[5]; // b1'
;    coef_ptr[7]  = coef_temp[7]; // a1'
;    coef_ptr[8]  = coef_temp[6]; // b2'        
;    coef_ptr[9]  = coef_temp[8]; // a2'    
;    
;    coef_temp = (Float *)pParam->pCoef[1];    
;    coef_ptr[1]  = coef_temp[0]; // gain
;    coef_ptr[10] = coef_temp[1]; // b1
;    coef_ptr[11] = coef_temp[3]; // a1
;    coef_ptr[12] = coef_temp[2]; // b2          
;    coef_ptr[13] = coef_temp[4]; // a2
;    coef_ptr[14] = coef_temp[5]; // b1'
;    coef_ptr[15] = coef_temp[7]; // a1'
;    coef_ptr[16] = coef_temp[6]; // b2'         
;    coef_ptr[17] = coef_temp[8]; // a2' 
;    
;    coef_temp    = (Float *)pParam->pCoef[0];
;    pParam->pCoef[0] = coef_ptr;    
;    
;    Filter_iirT2Ch2_c2_sub( pParam );  
;    
;    pParam->pCoef[0] = coef_temp;
;    
;    return(FIL_SUCCESS);
;    
;}
    
