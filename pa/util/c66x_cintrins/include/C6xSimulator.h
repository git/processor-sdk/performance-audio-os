/****************************************************************************/
/*  C6xSimulator.h                                                          */
/****************************************************************************/

/*  
 *  Header file which defines the prototypes for the intrinsics.
 *
 */

/* 
 *
 * Copyright (C) 1997-2005 Texas Instruments Incorporated - http://www.ti.com/ 
 * 
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/****************************************************************************/
/* compiler directive to ensure only one copy of C6xSimulator.h gets        */
/* included                                                                 */
#ifndef C6XSIMULATOR_H
#define C6XSIMULATOR_H

#define TMS320C66X
#define LITTLE_ENDIAN_HOST
/****************************************************************************/
/* the includes                                                             */
#include "C6xSimulator_base_types.h"

/* This part of the header file is relevant only in host environment */
#ifndef _TMS320C6X      

/* check to ensure that the target DSP is defined */
#if    !defined(TMS320C62X)                       \
    && !defined(TMS320C64X)                       \
    && !defined(TMS320C64PX)                      \
    && !defined(TMS320C67X)                       \
    && !defined(TMS320C66X)
#error "TMS320C62X, TMS320C64X, TMS320C64PX, TMS320C67X, or TMS320C66X (i.e. the target DSP) must be defined."
#endif


/****************************************************************************/
/* the typedefs                                                             */

#ifdef TMS320C64X
#define TMS320C62X
#endif          // TMS320C64X

#ifdef TMS320C64PX
#define TMS320C62X
#define TMS320C64X
#endif          // TMS320C64PX 

#ifdef TMS320C67X
#define TMS320C62X
#endif          // TMS320C67X

#ifdef TMS320C66X
#define TMS320C62X
#define TMS320C64X
#define TMS320C64PX
#define TMS320C67X
#endif          // TMS320C66X


/* memory intrinsics */
#ifdef TMS320C62X
#define _amem2(ptr)             (*((uint16 *) (ptr)))
#define _amem2_const(ptr)       (*((const uint16 *) (ptr)))
#define _amem4(ptr)             (*((uint32 *) (ptr)))
#define _amem4_const(ptr)       (*((const uint32 *) (ptr)))
/*
Change _amemd8 and _amemd8_const from these macros ...
#define _amemd8(ptr)            (*((int64_d *) (ptr)))
#define _amemd8_const(ptr)      (*((const int64_d *) (ptr)))
to macros which invoke helper functions, so the helper functions can emit
messages about how these intrinsics are deprecated.
*/
#define _amemd8(ptr)            (* _amemd8_helper((void *) ptr))   
int64_d *_amemd8_helper(void *);
#define _amemd8_const(ptr)            (* _amemd8_const_helper((void *) ptr))   
const int64_d *_amemd8_const_helper(void *);
#endif          // TMS320C62X

#ifdef TMS320C64X
#define _amem8(ptr)             (*((int64_ll *) (ptr)))
#define _amem8_const(ptr)       (*((const int64_ll *) (ptr)))
#define _amem8_f2(ptr)          (*((__float2_t *) (ptr)))
#define _amem8_f2_const(ptr)    (*((const __float2_t *) (ptr)))

#define _mem2(ptr)              (*((uint16 *) (ptr)))
#define _mem2_const(ptr)        (*((const uint16 *) (ptr)))
#define _mem4(ptr)              (*((uint32 *) (ptr)))
#define _mem4_const(ptr)        (*((const uint32 *) (ptr)))
#define _mem8(ptr)              (*((int64_ll *) (ptr)))
#define _mem8_const(ptr)        (*((const int64_ll *) (ptr)))
#define _mem8_f2(ptr)           (*((__float2_t *) (ptr)))
#define _mem8_f2_const(ptr)     (*((const __float2_t *) (ptr)))
/*
This change is just like the one for _amemd8 etc. above
#define _memd8(ptr)             (*((int64_d *) (ptr)))
#define _memd8_const(ptr)       (*((const int64_d *) (ptr)))
*/
#define _memd8(ptr)            (* _memd8_helper((void *) ptr))   
int64_d *_memd8_helper(void *);
#define _memd8_const(ptr)            (* _memd8_const_helper((void *) ptr))   
const int64_d *_memd8_const_helper(void *);
#endif          // TMS320C64X

/* pseudo-operations */
#ifdef TMS320C64X
#define _cmplt2(a,b)      _cmpgt2(b,a)
#define _cmpltu4(a,b)     _cmpgtu4(b,a)
#define _dotpnrus2(a,b)   _dotpnrsu2(b,a)
#define _dotprus2(a,b)    _dotprsu2(b,a)
#define _dotpus4(a,b)     _dotpsu4(b,a)
#define _mpyih(a,b)       _mpyhi(b,a)
#define _mpyihr(a,b)      _mpyhir(b,a)
#define _mpyil(a,b)       _mpyli(b,a)
#define _mpyilr(a,b)      _mpylir(b,a)
#define _mpyus4(a,b)      _mpysu4(b,a)
#define _saddsu2(a,b)     _saddus2(b,a)
#define _swap2(a)         _packlh2(a,a)
#endif          // TMS320C64X



/****************************************************************************/
/* the typedefs                                                             */


/****************************************************************************/
/* the globals                                                              */


/****************************************************************************/
/* function prototypes for the TMS320C6X DSP                                */

#ifdef TMS320C62X

int32 _abs(int32 a);
int32 _add2(int32 a,int32 b);
uint32 _clr(uint32 a,uint32 b,uint32 c);
uint32 _clrr(uint32 a,int32 b);
int40 _dtol(uint64_d a);
int40 _f2tol(__float2_t a);
int32 _ext(int32 a,uint32 b,uint32 c);
int32 _extr(int32 a,int32 b);
uint32 _extu(uint32 a,uint32 b,uint32 c);
uint32 _extur(uint32 a,int32 b);
uint32 _ftoi(float32 a);
uint32 _hi(uint64_d a);
uint32 _hill(int64_ll a);
uint64_d _itod(uint32 a,uint32 b);
double64 _ftod(float32 a, float32 b);      // Added in v7.2.x compiler
__float2_t _ftof2(float32 a, float32 b);   // Added in v7.2.x compiler
float32 _itof(uint32 a);
/* Fix for BZ 1679.  Return type must be *signed* 64-bit long to match   */
/* c6x.h                                                                 */
int64_ll _itoll(uint32 a,uint32 b);
int40 _labs(int40 a);
uint32 _lmbd(uint32 a,uint32 b);
uint32 _lnorm(int40 a);
uint32 _lo(uint64_d a);
int40 _lsadd(int32 a,int40 b);
int40 _lssub(int32 a,int40 b);
uint32 _loll(int64_ll a);
uint64_d _ltod(int40 a);
__float2_t _ltof2(int40 a);
int32 _mpy(int32 a,int32 b);
int32 _mpyh(int32 a,int32 b);
int32 _mpyhsu(int32 a,uint32 b);
uint32 _mpyhu(uint32 a,uint32 b);
int32 _mpyhus(uint32 a,int32 b);
int32 _mpyhl(int32 a,int32 b);
uint32 _mpyhlu(uint32 a,uint32 b);
int32 _mpyhslu(int32 a,uint32 b);
int32 _mpyhuls(uint32 a,int32 b);
int32 _mpylh(int32 a,int32 b);
uint32 _mpylhu(uint32 a,uint32 b);
int32 _mpylshu(int32 a,uint32 b);
int32 _mpyluhs(uint32 a,int32 b);
int32 _mpysu(int32 a,uint32 b);
uint32 _mpyu(uint32 a,uint32 b);
int32 _mpyus(uint32 a,int32 b);
uint32 _norm(int32 a);
int32 _sadd(int32 a,int32 b);
int32 _sat(int40 a);
uint32 _set(uint32 a,uint32 b,uint32 c);
uint32 _setr(uint32 a,int32 b);
int32 _smpy(int32 a,int32 b);
int32 _smpyh(int32 a,int32 b);
int32 _smpyhl(int32 a,int32 b);
int32 _smpylh(int32 a,int32 b);
int32 _sshl(int32 a,uint32 b);
int32 _ssub(int32 a,int32 b);
int32 _sub2(int32 a,int32 b);
uint32 _subc(uint32 a,uint32 b);

#endif      // TMS320C62X


/****************************************************************************/
/* additional function prototypes for the TMS320C64X DSP                    */

#ifdef TMS320C64X

int32 _abs2(int32 a);
int32 _add4(int32 a,int32 b);
int32 _avg2(int32 a,int32 b);
uint32 _avgu4(uint32 a,uint32 b);
uint32 _bitc4(uint32 a);
uint32 _bitr(uint32 a);
int32 _cmpeq2(int32 a,int32 b);
int32 _cmpeq4(int32 a,int32 b);
int32 _cmpgt2(int32 a,int32 b);
uint32 _cmpgtu4(uint32 a,uint32 b);
uint32 _deal(uint32 a);
int32 _dotp2(int32 a,int32 b);
int32 _dotpn2(int32 a,int32 b);
int32 _dotpnrsu2(int32 a,uint32 b);
int32 _dotprsu2(int32 a,uint32 b);
int32 _dotpsu4(int32 a,uint32 b);
uint32 _dotpu4(uint32 a,uint32 b);
int32 _gmpy4(int32 a,int32 b);
int40 _ldotp2(int32 a,int32 b);
int32 _max2(int32 a,int32 b);
uint32 _maxu4(uint32 a,uint32 b);
int32 _min2(int32 a,int32 b);
uint32 _minu4(uint32 a,uint32 b);
uint64_d _mpy2(int32 a,int32 b);
int64_ll _mpy2ll(int32 a,int32 b);
uint64_d _mpyhi(int32 a,int32 b);
int64_ll _mpyhill(int32 a,int32 b);
int32 _mpyhir(int32 a,int32 b);
uint64_d _mpyli(int32 a,int32 b);
int64_ll _mpylill(int32 a,int32 b);
int32 _mpylir(int32 a,int32 b);
uint64_d _mpysu4(int32 a,uint32 b);
int64_ll _mpysu4ll(int32 a,uint32 b);
uint64_d _mpyu4(uint32 a,uint32 b);
int64_ll _mpyu4ll(uint32 a,uint32 b);
int32 _mvd(int32 a);
uint32 _pack2(uint32 a,uint32 b);
uint32 _packh2(uint32 a,uint32 b);
uint32 _packh4(uint32 a,uint32 b);
uint32 _packhl2(uint32 a,uint32 b);
uint32 _packl4(uint32 a,uint32 b);
uint32 _packlh2(uint32 a,uint32 b);

/* MSVC defines its own _rotl operation.  This re-definition effectively     */
/* disables that rotl.  That's OK with the vast majority of MSVC users.      */
/* Note the type of the second argument is signed int.  That matches how     */
/* MSVC prototypes _rotl in stdlib.h.  That change silences a useless        */
/* MSVC warning.                                                             */
#ifdef _MSC_VER
#define _rotl _ti_rotl
uint32 _rotl(uint32 a, int32 b);
#else
uint32 _rotl(uint32 a, uint32 b);
#endif

int32 _sadd2(int32 a,int32 b);
uint32 _saddu4(uint32 a,uint32 b);
int32 _saddus2(uint32 a,int32 b);
uint32 _shfl(uint32 a);
uint32 _shlmb(uint32 a,uint32 b);
int32 _shr2(int32 a,uint32 b);
uint32 _shrmb(uint32 a,uint32 b);
uint32 _shru2(uint32 a,uint32 b);
uint64_d _smpy2(int32 a,int32 b);
int64_ll _smpy2ll(int32 a,int32 b);
int32 _spack2(int32 a,int32 b);
uint32 _spacku4(int32 a,int32 b);
int32 _sshvl(int32 a,int32 b);
int32 _sshvr(int32 a,int32 b);
int32 _sub4(int32 a,int32 b);
int32 _subabs4(int32 a,int32 b);
uint32 _swap4(uint32 a);
uint32 _unpkhu4(uint32 a);
uint32 _unpklu4(uint32 a);
uint32 _xpnd2(uint32 a);
uint32 _xpnd4(uint32 a);

#endif      // TMS320C64X


/****************************************************************************/
/* additional function prototypes for the TMS320C64+X DSP                   */

#ifdef TMS320C64PX

int64_ll _addsub(int32 a,int32 b);
int64_ll _addsub2(uint32 a,uint32 b);
int64_ll _cmpy(uint32 a,uint32 b);
uint32 _cmpyr(uint32 a,uint32 b);
uint32 _cmpyr1(uint32 a,uint32 b);
int64_ll _ddotp4(uint32 a,uint32 b);
int64_ll _ddotph2(int64_ll a,uint32 b);
uint32 _ddotph2r(int64_ll a,uint32 b);
int64_ll _ddotpl2(int64_ll a,uint32 b);
uint32 _ddotpl2r(int64_ll a,uint32 b);
int64_ll _dmv(uint32 a,uint32 b);
double64   _fdmv(float32 a, float32 b);
__float2_t _fdmv_f2(float32 a, float32 b);
int64_ll _dpack2(uint32 a,uint32 b);
int64_ll _dpackx2(uint32 a,uint32 b);
uint32 _gmpy(uint32 a,uint32 b);
int64_ll _mpy2ir(uint32 a,int32 b);
int32 _mpy32(int32 a,int32 b);
int64_ll _mpy32ll(int32 a,int32 b);
int64_ll _mpy32su(int32 a,uint32 b);
int64_ll _mpy32u(uint32 a,uint32 b);
int64_ll _mpy32us(uint32 a,int32 b);
uint32 _rpack2(int32 a,int32 b);
int64_ll _saddsub(int32 a,int32 b);
int64_ll _saddsub2(uint32 a,uint32 b);
int64_ll _shfl3(uint32 a,uint32 b);
int32 _smpy32(int32 a,int32 b);
int32 _ssub2(int32 a,int32 b);
uint32 _xormpy(uint32 a,uint32 b);

#endif      // TMS320C64PX


/****************************************************************************/
/* function prototypes for the TMS320C67X DSP                               */

#ifdef TMS320C67X
double64 _fabs(double64 a); 
float32 _fabsf(float32 a);
#endif      // TMS320C67X

#if defined(TMS320C66X) || defined(TMS320C67X)
#include "c66_data_sim.h"
#include "c66_conversion_macros.h"
#include "c66_ag_intrins.h"
#endif

#ifdef TMS320C66X
__float2_t _complex_mpysp(__float2_t a, __float2_t b);
__float2_t _complex_conjugate_mpysp(__float2_t a, __float2_t b);
double64   _fdmvd(float32 a, float32 b);
__float2_t _fdmvd_f2(float32 a, float32 b);
#endif

#else       // Using TI C6000 Compiler

#ifndef __FLOAT2_DEFINES__
#define __FLOAT2_DEFINES__

typedef double                 __float2_t;

#define _lltof2   _lltod
#define _f2toll   _dtoll
#define _hif2_128 _hid128
#define _lof2_128 _lod128
#define _ftof2    _ftod
#define _hif2     _hif
#define _lof2     _lof
#define _fdmv_f2  _fdmv
#define _fdmvd_f2 _fdmvd
#define _f2to128  _dto128
#define _f2tol    _dtol
#define _ltof2    _ltod

#define _amem8_f2       _amemd8
#define _amem8_f2_const _amemd8_const
#define _mem8_f2        _memd8
#define _mem8_f2_const  _memd8_const

#endif      // #ifndef __FLOAT2_DEFINES__

#endif      // #ifndef _TMS320C6X      

#endif      // C6XSIMULATOR_H

