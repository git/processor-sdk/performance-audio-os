* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.

;******************************************************************************
;* TMS320C6x ANSI C Codegen                                      Version 4.20 *
;* Date/Time created: Mon Sep 01 15:36:55 2003                                *
;******************************************************************************

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : TMS320C671x                                          *
;*   Optimization      : Enabled at level 3                                   *
;*   Optimizing for    : Speed                                                *
;*                       Based on options: -o3, no -ms                        *
;*   Endian            : Little                                               *
;*   Interrupt Thrshld : Disabled                                             *
;*   Memory Model      : Small                                                *
;*   Calls to RTS      : Near                                                 *
;*   Pipelining        : Enabled                                              *
;*   Speculative Load  : Disabled                                             *
;*   Memory Aliases    : Presume are aliases (pessimistic)                    *
;*   Debug Info        : No Debug Info                                        *
;*                                                                            *
;******************************************************************************

	.asg	A15, FP
	.asg	B14, DP
	.asg	B15, SP
	.global	$bss

*
*  ======== iir4tap1ch_mx.sa ========
*  IIR filter implementation for tap-4 & channels-1, SP-DP.
*

************************ IIR FILTER *****************************************
****************** ORDER 4    and CHANNELS 1 ********************************
*                                                                           *
* Filter equation  : H(z) =  b0 + b1*z~1 + b2*z~2 b3*z~3 + b4*z~4           *
*                           --------------------------------------          *
*                            1  - a1*z~1 - a2*z~2 - a3*z~3 - a4*z~4         *
*                                                                           *
*   Direct form    : y(n) = b0*x(n)+b1*x(n-1)+b2*x(n-2)+b3*x(n-3)+b4*x(n-4) * 
*                                  +a1*y(n-1)+a2*y(n-2)+a3*y(n-3)+a4*y(n-4) *
*                                                                           *
*   Implementation : w(n) = b0*x(n)+a1*w(n-1)+a2*w(n-2)+a3*w(n-3)+a4*w(n-4) *
*                    y(n) = w(n)+b1/b0*w(n-1)+b2/b0*w(n-2)                  *
*                               +b3/b0*w(n-3)+b4/b0*w(n-4)                  *
*                                                                           *
*   Filter variables :                                                      *
*      y(n) - *y,   x(n)   - *x                                             *
*      w(n) -       w1=w(n-1)-filtVars[0]  w2=w(n-2)-filtVars[1]            * 
*                   w3=w(n-3)-filtVars[2]  w4=w(n-4)-filtVars[3]            *
*      c0=b0    - filtCfsB[0],                                              *
*      c1=b1/b0 - filtCfsB[1], c2=b2/b0 - filtCfsB[2],                      *
*      c3=b1/b0 - filtCfsB[1], c4=b2/b0 - filtCfsB[2],                      *
*      c5=a1    - filtCfsA[0], c6=a2    - filtCfsA[1],                      *
*      c7=a1    - filtCfsA[0], c8=a2    - filtCfsA[1],                      *
*                                                                           *
*                                                                           *
*           b0                 w(n)                                         *
*     x(n)-->>--[+]---->--------@------->-----[+]--->--y(n)                 *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a1    +-----+  b1/b0    |                            *
*               [+]----<<----| Z~1 |---->>----[+]                           *
*                |           +-----+           |                            *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a2    +-----+  b2/b0    |                            *
*                 ----<<-----| Z~1 |---->>-----                             *
*                |           +-----+           |                            *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a3    +-----+  b3/b0    |                            *
*                 ----<<-----| Z~1 |---->>-----                             *
*                |           +-----+           |                            *
*                |              |              |                            *
*                ^              v              ^                            *
*                |     a4    +-----+  b4/b0    |                            *
*                 ----<<-----| Z~1 |---->>-----                             *
*                            +-----+                                        *
*                                                                           *
*****************************************************************************
*Note:C equivalent of s-asm code lines are given as comments,for simplicity *

            .global _Filter_iirT4Ch1_mx ;Declared as Global function
            .sect	".text:Filter_iirT4Ch1_mx" ;Memory section for the function                        
	.sect	".text:Filter_iirT4Ch1_mx"

;******************************************************************************
;* FUNCTION NAME: _Filter_iirT4Ch1_mx                                         *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP                                           *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,   *
;*                           B13,SP                                           *
;******************************************************************************
_Filter_iirT4Ch1_mx:
;** --------------------------------------------------------------------------*
; _Filter_iirT4Ch1_mx: .cproc pParam ;Filter_iirT4Ch1_mx(*pParam),C-call fn
;              .no_mdep ;no memory aliasing in this function
; 			.reg    w_10:w_11, w_20:w_21, w_30:w_31, w_40:w_41 ;Filter states
; 			.asg    "w_10:w_11", w_1
; 			.asg    "w_20:w_21", w_2
; 			.asg    "w_30:w_31", w_3
; 			.asg    "w_40:w_41", w_4
;             .reg    sum0:sum1 ; O/p accumulator registers
;             .asg    "sum0:sum1", sum ;Naming O/p accumulator register pairs
; 			.reg    accum10:accum11, accum20:accum21, accum30:accum31
; 			.reg    accum40:accum41, accum50:accum51, accum60:accum61
; 			.reg    accum70:accum71, accum80:accum81, accum90:accum91			
; 			.reg    accum00:accum01
; 			.asg    "accum10:accum11", accum1
; 			.asg    "accum20:accum21", accum2
; 			.asg    "accum30:accum31", accum3
; 			.asg    "accum40:accum41", accum4
; 			.asg    "accum50:accum51", accum5
; 			.asg    "accum60:accum61", accum6																		
; 			.asg    "accum70:accum71", accum7
; 			.asg    "accum80:accum81", accum8
; 			.asg    "accum90:accum91", accum9																		
; 			.asg    "accum00:accum01", accum
; 			.reg    c00:c01, c10:c11, c20:c21, c30:c31, c40:c41, c50:c51 
;             .reg    c60:c61, c70:c71, c80:c81 
; 			.asg    "c00:c01", c_0
; 			.asg    "c10:c11", c_1
; 			.asg    "c20:c21", c_2
; 			.asg    "c30:c31", c_3
; 			.asg    "c40:c41", c_4
; 			.asg    "c50:c51", c_5
; 			.asg    "c60:c61", c_6
; 			.asg    "c70:c71", c_7
; 			.asg    "c80:c81", c_8
;             .reg    input_data0:input_data1 ;Input data regs
;             .asg    "input_data0:input_data1", input_data
;             .reg  pIn   ;Ptr to, array of ch i/p  ptrs
;             .reg  pOut  ;Ptr to, array of ch o/p  ptrs
;             .reg  pCoef ;Ptr to, array of ch coeff  ptrs
;             .reg  pVar  ;Ptr to, array of ch var  ptrs
;             .reg  count ;Sample counter
;             .reg  x, y ;I/p and O/p ptrs
;             .reg  filtCfsA, filtCfsB, filtVars ;Coeff and var ptrs
; LOOP:        .trip 16, 2048, 4
           STW     .D2T2   B13,*SP--(48)     ; |75| 
           STW     .D2T1   A10,*+SP(8)       ; |75| 
           STW     .D2T1   A13,*+SP(20)      ; |75| 
           STW     .D2T1   A11,*+SP(12)      ; |75| 
           STW     .D2T1   A12,*+SP(16)      ; |75| 
           STW     .D2T1   A14,*+SP(24)      ; |75| 
           STW     .D2T2   B10,*+SP(36)      ; |75| 
           STW     .D2T1   A15,*+SP(28)      ; |75| 
           STW     .D2T2   B3,*+SP(32)       ; |75| 
           STW     .D2T2   B11,*+SP(40)      ; |75| 
           STW     .D2T2   B12,*+SP(44)      ; |75| 
            
           LDW     .D1T1   *+A4(12),A0       ; |142| pVar  = pParam[3]
           LDW     .D1T1   *+A4(8),A5        ; |141| pCoef = pParam[2]
           LDW     .D1T2   *A4,B5            ; |139| pIn   = pParam[0]
           LDW     .D1T1   *+A4(4),A3        ; |140| pOut  = pParam[1]
           LDW     .D1T1   *+A4(16),A4       ; |143| count = pParam[4]
           LDW     .D1T1   *A0,A14
           LDW     .D1T2   *A5,B12           ; |149| filtCfsB = pCoef[0]
           LDW     .D2T1   *B5,A0            ; |145| x = pIn[0]
           NOP             3
           LDDW    .D2T2   *+B12(48),B3:B2   ; (P) |168| c_6 = filtCfs[6]

           LDW     .D1T1   *A0++,A5          ; (P) |173| input_data0 = *x++
||         MV      .S2X    A14,B4            ; |145| 
||         LDDW    .D2T2   *B12,B1:B0        ; (P) |161| c_0 = filtCfs[0] // I/p gain

           LDDW    .D2T2   *+B4(24),B11:B10  ; |156| w4 = filtVars[3]
           LDDW    .D2T1   *+B4(8),A9:A8     ; |154| w2 = filtVars[1]
           LDDW    .D2T1   *B4,A7:A6         ; |153| w1 = filtVars[0]

           LDW     .D1T2   *A3,B13           ; |146| y = pOut[0]
||         LDDW    .D2T1   *+B4(16),A3:A2    ; |155| w3 = filtVars[2]
||         MVC     .S2     CSR,B4

           SUB     .D1     A4,2,A1
||         MV      .L1X    B4,A15
||         AND     .S2     -2,B4,B4
||         SPDP    .S1     A5,A5:A4          ; (P) |174| input_data=(double)input_data0

           LDDW    .D2T1   *+B12(24),A11:A10 ; (P) |164| c_3 = filtCfs[3]
||         MVC     .S2     B4,CSR            ; interrupts off

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop label : LOOP
;*      Known Minimum Trip Count         : 16
;*      Known Maximum Trip Count         : 2048
;*      Known Max Trip Count Factor      : 4
;*      Loop Carried Dependency Bound(^) : 18
;*      Unpartitioned Resource Bound     : 18
;*      Partitioned Resource Bound(*)    : 20
;*      Resource Partition:
;*                                A-side   B-side
;*      .L units                     6       11     
;*      .S units                     3        0     
;*      .D units                     1       10     
;*      .M units                    20*      16     
;*      .X cross paths              16       18     
;*      .T address paths             3        8     
;*      Long read paths              0        1     
;*      Long write paths             2        7     
;*      Logical  ops (.LS)           0        2     (.L or .S unit)
;*      Addition ops (.LSD)          7        0     (.L or .S or .D unit)
;*      Bound(.L .S .LS)             5        7     
;*      Bound(.L .S .D .LS .LSD)     6        8     
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 20 Did not find schedule
;*         ii = 21 Did not find schedule
;*         ii = 22 Did not find schedule
;*         ii = 23 Did not find schedule
;*         ii = 24 Did not find schedule
;*         ii = 25 Did not find schedule
;*         ii = 26 Cannot allocate machine registers
;*                   Regs Live Always   :  2/2  (A/B-side)
;*                   Max Regs Live      : 15/14
;*                   Max Cond Regs Live :  1/0 
;*         ii = 26 Did not find schedule
;*         ii = 27 Did not find schedule
;*         ii = 28 Schedule found with 3 iterations in parallel
;*      done
;*
;*      Epilog not removed
;*      Collapsed epilog stages     : 0
;*
;*      Prolog not removed
;*      Collapsed prolog stages     : 0
;*
;*      Minimum required memory pad : 0 bytes
;*
;*      For further improvement on this loop, try option -mh4
;*
;*      Minimum safe trip count     : 3
;*----------------------------------------------------------------------------*
;*   SINGLE SCHEDULED ITERATION
;*
;*   LOOP:
;*              LDDW    .D2T2   *B12,B1:B0        ; |161| c_0 = filtCfs[0] // I/p gain
;*   ||         LDW     .D1T1   *A0++,A5          ; |173| input_data0 = *x++
;*              NOP             1
;*              LDDW    .D2T2   *+B12(48),B3:B2   ; |168| c_6 = filtCfs[6]
;*              NOP             1
;*              LDDW    .D2T2   *+B12(32),B9:B8   ; |165| c_4 = filtCfs[4]            
;*              SPDP    .S1     A5,A5:A4          ; |174| input_data=(double)input_data0
;*              LDDW    .D2T1   *+B12(24),A11:A10 ; |164| c_3 = filtCfs[3]
;*              MPYDP   .M2X    A5:A4,B1:B0,B9:B8 ; |177| accum1=input_data*c_0
;*   ||         MPYDP   .M1X    B3:B2,A9:A8,A11:A10 ; |179| accum3=w_2*c_6
;*              NOP             2
;*              LDDW    .D2T2   *+B12(56),B5:B4   ; |169| c_7 = filtCfs[7]
;*              LDDW    .D2T2   *+B12(64),B7:B6   ; |170| c_8 = filtCfs[8]
;*   ||         MPYDP   .M1     A11:A10,A3:A2,A11:A10 ; |186| accum8=w_3*c_3
;*              MPYDP   .M2     B9:B8,B11:B10,B7:B6 ; |187| accum9=w_4*c_4
;*              NOP             1
;*              LDDW    .D2T1   *+B12(16),A13:A12 ; |163| c_2 = filtCfs[2]
;*              LDDW    .D2T2   *+B12(8),B5:B4    ; |162| c_1 = filtCfs[1]
;*              MPYDP   .M1X    B5:B4,A3:A2,A13:A12 ; |180| accum4=w_3*c_7
;*   ||         MPYDP   .M2     B7:B6,B11:B10,B5:B4 ; |181| accum3=w_4*c_8
;*              ADDDP   .L2X    B9:B8,A11:A10,B9:B8 ; |190| accum1=accum1+accum3
;*              NOP             3
;*              MPYDP   .M2X    B5:B4,A7:A6,B5:B4 ; |184| accum6=w_1*c_1
;*   ||         MPYDP   .M1     A13:A12,A9:A8,A5:A4 ; |185| accum7=w_2*c_2
;*              NOP             2
;*              LDDW    .D2T2   *+B12(40),B1:B0   ; |167| c_5 = filtCfs[5]
;*              NOP             1
;*              ADDDP   .L1X    B5:B4,A13:A12,A13:A12 ; |191| accum4=accum5+accum4
;*              MV      .S2X    A3,B11            ; |196|  w3  = w4
;*              MV      .S2X    A2,B10            ; |197| 
;*              MPYDP   .M1X    B1:B0,A7:A6,A7:A6 ;  ^ |178| accum2=w_1*c_5
;*   ||         ADDDP   .L2X    B7:B6,A11:A10,B7:B6 ; |207| accum9=accum9+accum8
;*              MV      .D1     A9,A3             ; |198|  w2  = w3
;*              ADDDP   .L2X    A5:A4,B5:B4,B5:B4 ; |208| accum6=accum7+accum6
;*              MV      .S1     A8,A2             ; |199| 
;*   ||         MV      .D1     A7,A9             ; |200|  w1  = w2
;*              ADDDP   .L1X    A13:A12,B9:B8,A5:A4 ; |192| sum=accum4+accum1
;*              MV      .D1     A6,A8             ; |201| 
;*              NOP             3
;*              ADDDP   .L2     B5:B4,B7:B6,B3:B2 ; |209| accum =accum6+accum9
;*              NOP             1
;*              ADDDP   .L1     A5:A4,A7:A6,A5:A4 ;  ^ |193| sum+=accum2
;*              NOP             6
;*              ADDDP   .L2X    B3:B2,A5:A4,B3:B2 ; |210| accum +=sum
;*              MV      .S1     A5,A7             ;  ^ |202|  sum = w1
;*   ||         MV      .D1     A4,A6             ;  ^ |203| 
;*              NOP             4
;*      [ A1]   ADD     .D1     0xffffffff,A1,A1  ; |217| if(count) count=count-1;
;*      [ A1]   B       .S1     LOOP              ; |218|  if(count) goto LOOP 
;*              DPSP    .L2     B3:B2,B3          ; |213| accum00 = (float)accum 
;*              NOP             3
;*              STW     .D2T2   B3,*B13++         ; |214| *y++ = accum00
;*              ; BRANCH OCCURS                   ; |218| 
;*----------------------------------------------------------------------------*
L1:    ; PIPED LOOP PROLOG

           LDDW    .D2T2   *+B12(32),B9:B8   ; (P) |165| c_4 = filtCfs[4]            
||         MPYDP   .M2X    A5:A4,B1:B0,B9:B8 ; (P) |177| accum1=input_data*c_0
||         MPYDP   .M1X    B3:B2,A9:A8,A11:A10 ; (P) |179| accum3=w_2*c_6

           NOP             2
           LDDW    .D2T2   *+B12(56),B5:B4   ; (P) |169| c_7 = filtCfs[7]

           LDDW    .D2T2   *+B12(64),B7:B6   ; (P) |170| c_8 = filtCfs[8]
||         MPYDP   .M1     A11:A10,A3:A2,A11:A10 ; (P) |186| accum8=w_3*c_3

           MPYDP   .M2     B9:B8,B11:B10,B7:B6 ; (P) |187| accum9=w_4*c_4
           NOP             1
           LDDW    .D2T1   *+B12(16),A13:A12 ; (P) |163| c_2 = filtCfs[2]
           LDDW    .D2T2   *+B12(8),B5:B4    ; (P) |162| c_1 = filtCfs[1]

           MPYDP   .M2     B7:B6,B11:B10,B5:B4 ; (P) |181| accum3=w_4*c_8
||         MPYDP   .M1X    B5:B4,A3:A2,A13:A12 ; (P) |180| accum4=w_3*c_7

           ADDDP   .L2X    B9:B8,A11:A10,B9:B8 ; (P) |190| accum1=accum1+accum3
           NOP             3

           MPYDP   .M1     A13:A12,A9:A8,A5:A4 ; (P) |185| accum7=w_2*c_2
||         MPYDP   .M2X    B5:B4,A7:A6,B5:B4 ; (P) |184| accum6=w_1*c_1

           NOP             2
           LDDW    .D2T2   *+B12(40),B1:B0   ; (P) |167| c_5 = filtCfs[5]
           NOP             1
           ADDDP   .L1X    B5:B4,A13:A12,A13:A12 ; (P) |191| accum4=accum5+accum4
           MV      .S2X    A3,B11            ; (P) |196|  w3  = w4

           MV      .S2X    A2,B10            ; (P) |197| 
||         LDW     .D1T1   *A0++,A5          ; (P) @|173| input_data0 = *x++
||         LDDW    .D2T2   *B12,B1:B0        ; (P) @|161| c_0 = filtCfs[0] // I/p gain

           MPYDP   .M1X    B1:B0,A7:A6,A7:A6 ; (P)  ^ |178| accum2=w_1*c_5
||         ADDDP   .L2X    B7:B6,A11:A10,B7:B6 ; (P) |207| accum9=accum9+accum8

           MV      .D1     A9,A3             ; (P) |198|  w2  = w3
||         LDDW    .D2T2   *+B12(48),B3:B2   ; (P) @|168| c_6 = filtCfs[6]

           ADDDP   .L2X    A5:A4,B5:B4,B5:B4 ; (P) |208| accum6=accum7+accum6
;** --------------------------------------------------------------------------*
LOOP:    ; PIPED LOOP KERNEL

           MV      .D1     A7,A9             ; |200|  w1  = w2
||         MV      .S1     A8,A2             ; |199| 
||         LDDW    .D2T2   *+B12(32),B9:B8   ; @|165| c_4 = filtCfs[4]            

           ADDDP   .L1X    A13:A12,B9:B8,A5:A4 ; |192| sum=accum4+accum1
||         SPDP    .S1     A5,A5:A4          ; @|174| input_data=(double)input_data0

           MV      .D1     A6,A8             ; |201| 
||         LDDW    .D2T1   *+B12(24),A11:A10 ; @|164| c_3 = filtCfs[3]

           MPYDP   .M1X    B3:B2,A9:A8,A11:A10 ; @|179| accum3=w_2*c_6
||         MPYDP   .M2X    A5:A4,B1:B0,B9:B8 ; @|177| accum1=input_data*c_0

           NOP             2

           ADDDP   .L2     B5:B4,B7:B6,B3:B2 ; |209| accum =accum6+accum9
||         LDDW    .D2T2   *+B12(56),B5:B4   ; @|169| c_7 = filtCfs[7]

           LDDW    .D2T2   *+B12(64),B7:B6   ; @|170| c_8 = filtCfs[8]
||         MPYDP   .M1     A11:A10,A3:A2,A11:A10 ; @|186| accum8=w_3*c_3

           ADDDP   .L1     A5:A4,A7:A6,A5:A4 ;  ^ |193| sum+=accum2
||         MPYDP   .M2     B9:B8,B11:B10,B7:B6 ; @|187| accum9=w_4*c_4

           NOP             1
           LDDW    .D2T1   *+B12(16),A13:A12 ; @|163| c_2 = filtCfs[2]
           LDDW    .D2T2   *+B12(8),B5:B4    ; @|162| c_1 = filtCfs[1]

           MPYDP   .M1X    B5:B4,A3:A2,A13:A12 ; @|180| accum4=w_3*c_7
||         MPYDP   .M2     B7:B6,B11:B10,B5:B4 ; @|181| accum3=w_4*c_8

           ADDDP   .L2X    B9:B8,A11:A10,B9:B8 ; @|190| accum1=accum1+accum3
           NOP             1
           ADDDP   .L2X    B3:B2,A5:A4,B3:B2 ; |210| accum +=sum

           MV      .D1     A4,A6             ;  ^ |203| 
||         MV      .S1     A5,A7             ;  ^ |202|  sum = w1

           MPYDP   .M1     A13:A12,A9:A8,A5:A4 ; @|185| accum7=w_2*c_2
||         MPYDP   .M2X    B5:B4,A7:A6,B5:B4 ; @|184| accum6=w_1*c_1

           NOP             2
           LDDW    .D2T2   *+B12(40),B1:B0   ; @|167| c_5 = filtCfs[5]
   [ A1]   ADD     .D1     0xffffffff,A1,A1  ; |217| if(count) count=count-1;

   [ A1]   B       .S1     LOOP              ; |218|  if(count) goto LOOP 
||         ADDDP   .L1X    B5:B4,A13:A12,A13:A12 ; @|191| accum4=accum5+accum4

           DPSP    .L2     B3:B2,B3          ; |213| accum00 = (float)accum 
||         MV      .S2X    A3,B11            ; @|196|  w3  = w4

           MV      .S2X    A2,B10            ; @|197| 
||         LDDW    .D2T2   *B12,B1:B0        ; @@|161| c_0 = filtCfs[0] // I/p gain
||         LDW     .D1T1   *A0++,A5          ; @@|173| input_data0 = *x++

           MPYDP   .M1X    B1:B0,A7:A6,A7:A6 ; @ ^ |178| accum2=w_1*c_5
||         ADDDP   .L2X    B7:B6,A11:A10,B7:B6 ; @|207| accum9=accum9+accum8

           MV      .D1     A9,A3             ; @|198|  w2  = w3
||         LDDW    .D2T2   *+B12(48),B3:B2   ; @@|168| c_6 = filtCfs[6]

           STW     .D2T2   B3,*B13++         ; |214| *y++ = accum00
||         ADDDP   .L2X    A5:A4,B5:B4,B5:B4 ; @|208| accum6=accum7+accum6

;** --------------------------------------------------------------------------*
L3:    ; PIPED LOOP EPILOG

           MV      .D1     A7,A9             ; (E) @|200|  w1  = w2
||         MV      .S1     A8,A2             ; (E) @|199| 
||         LDDW    .D2T2   *+B12(32),B9:B8   ; (E) @@|165| c_4 = filtCfs[4]            

           ADDDP   .L1X    A13:A12,B9:B8,A5:A4 ; (E) @|192| sum=accum4+accum1
||         SPDP    .S1     A5,A5:A4          ; (E) @@|174| input_data=(double)input_data0

           MV      .D1     A6,A8             ; (E) @|201| 
||         LDDW    .D2T1   *+B12(24),A11:A10 ; (E) @@|164| c_3 = filtCfs[3]

           MPYDP   .M1X    B3:B2,A9:A8,A11:A10 ; (E) @@|179| accum3=w_2*c_6
||         MPYDP   .M2X    A5:A4,B1:B0,B9:B8 ; (E) @@|177| accum1=input_data*c_0

           NOP             2

           LDDW    .D2T2   *+B12(56),B5:B4   ; (E) @@|169| c_7 = filtCfs[7]
||         ADDDP   .L2     B5:B4,B7:B6,B3:B2 ; (E) @|209| accum =accum6+accum9

           MPYDP   .M1     A11:A10,A3:A2,A11:A10 ; (E) @@|186| accum8=w_3*c_3
||         LDDW    .D2T2   *+B12(64),B7:B6   ; (E) @@|170| c_8 = filtCfs[8]

           ADDDP   .L1     A5:A4,A7:A6,A5:A4 ; (E) @ ^ |193| sum+=accum2
||         MPYDP   .M2     B9:B8,B11:B10,B7:B6 ; (E) @@|187| accum9=w_4*c_4

           NOP             1
           LDDW    .D2T1   *+B12(16),A13:A12 ; (E) @@|163| c_2 = filtCfs[2]
           LDDW    .D2T2   *+B12(8),B5:B4    ; (E) @@|162| c_1 = filtCfs[1]

           MPYDP   .M1X    B5:B4,A3:A2,A13:A12 ; (E) @@|180| accum4=w_3*c_7
||         MPYDP   .M2     B7:B6,B11:B10,B5:B4 ; (E) @@|181| accum3=w_4*c_8

           ADDDP   .L2X    B9:B8,A11:A10,B9:B8 ; (E) @@|190| accum1=accum1+accum3
           NOP             1
           ADDDP   .L2X    B3:B2,A5:A4,B3:B2 ; (E) @|210| accum +=sum

           MV      .D1     A4,A6             ; (E) @ ^ |203| 
||         MV      .S1     A5,A7             ; (E) @ ^ |202|  sum = w1

           MPYDP   .M1     A13:A12,A9:A8,A5:A4 ; (E) @@|185| accum7=w_2*c_2
||         MPYDP   .M2X    B5:B4,A7:A6,B5:B4 ; (E) @@|184| accum6=w_1*c_1

           NOP             2
           LDDW    .D2T2   *+B12(40),B1:B0   ; (E) @@|167| c_5 = filtCfs[5]
           LDW     .D2T2   *+SP(44),B12      ; |231| 
           ADDDP   .L1X    B5:B4,A13:A12,A13:A12 ; (E) @@|191| accum4=accum5+accum4

           MV      .S2X    A3,B11            ; (E) @@|196|  w3  = w4
||         DPSP    .L2     B3:B2,B3          ; (E) @|213| accum00 = (float)accum 

           MV      .S2X    A2,B10            ; (E) @@|197| 

           LDW     .D2T1   *+SP(8),A10       ; |231| 
||         MPYDP   .M1X    B1:B0,A7:A6,A7:A6 ; (E) @@ ^ |178| accum2=w_1*c_5
||         ADDDP   .L2X    B7:B6,A11:A10,B7:B6 ; (E) @@|207| accum9=accum9+accum8

           LDW     .D2T1   *+SP(12),A11      ; |231| 
||         MV      .D1     A9,A3             ; (E) @@|198|  w2  = w3

           STW     .D2T2   B3,*B13++         ; (E) @|214| *y++ = accum00
||         ADDDP   .L2X    A5:A4,B5:B4,B5:B4 ; (E) @@|208| accum6=accum7+accum6

           MV      .D1     A8,A2             ; (E) @@|199| 
||         MV      .S1     A7,A9             ; (E) @@|200|  w1  = w2

           LDW     .D2T1   *+SP(16),A12      ; |231| 
||         ADDDP   .L1X    A13:A12,B9:B8,A5:A4 ; (E) @@|192| sum=accum4+accum1

           LDW     .D2T1   *+SP(20),A13      ; |231| 
||         MV      .D1     A6,A8             ; (E) @@|201| 

           NOP             3
           ADDDP   .L2     B5:B4,B7:B6,B3:B2 ; (E) @@|209| accum =accum6+accum9

           LDW     .D2T1   *+SP(28),A15      ; |231| 
||         MV      .S2X    A15,B4

           LDW     .D2T1   *+SP(24),A14      ; |231| 
||         MV      .L2X    A14,B4
||         MVC     .S2     B4,CSR            ; interrupts on
||         ADDDP   .L1     A5:A4,A7:A6,A5:A4 ; (E) @@ ^ |193| sum+=accum2

           STW     .D2T2   B10,*+B4(24)      ; |227| filtVars[3]=w4
           STW     .D2T2   B11,*+B4(28)      ; |228| 
           LDW     .D2T2   *+SP(40),B11      ; |231| 
           LDW     .D2T2   *+SP(36),B10      ; |231| 
           STW     .D2T1   A9,*+B4(12)       ; |224| 
           STW     .D2T1   A2,*+B4(16)       ; |225| filtVars[2]=w3

           STW     .D2T1   A8,*+B4(8)        ; |223| filtVars[1]=w2
||         MV      .D1     A5,A7             ; (E) @@ ^ |202|  sum = w1
||         MV      .S1     A4,A6             ; (E) @@ ^ |203| 
||         ADDDP   .L2X    B3:B2,A5:A4,B3:B2 ; (E) @@|210| accum +=sum

           MV      .D1     A1,A4
||         STW     .D2T1   A6,*B4            ; |221| filtVars[0]=w1

           STW     .D2T1   A7,*+B4(4)        ; |222| 
           STW     .D2T1   A3,*+B4(20)       ; |226| 
           NOP             3
           DPSP    .L2     B3:B2,B3          ; (E) @@|213| accum00 = (float)accum 
           NOP             3
           STW     .D2T2   B3,*B13++         ; (E) @@|214| *y++ = accum00
           LDW     .D2T2   *+SP(32),B3       ; |231| 
           LDW     .D2T2   *++SP(48),B13     ; |231| 
           NOP             3
           B       .S2     B3                ; |231| 
           NOP             5
           ; BRANCH OCCURS                   ; |231| 


;              .endproc ;C callable function, directive
