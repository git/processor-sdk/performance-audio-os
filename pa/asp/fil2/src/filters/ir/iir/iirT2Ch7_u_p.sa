*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*  
*                                                                           * 
* This routine has following C prototype:                                   * 
*                                                                           * 
*    void Filter_iirT2Ch7_u(PAF_FilParam *pParam)                                  * 
*                                                                           * 
*    where pParam� is a pointer to the lower layer generic filter handle    * 
*    structure �PAF_FilParam�.                                              * 
*                                                                           * 
*    typedef struct PAF_FilParam {                                          * 
*        void  ** pIn ;                                                     * 
*        void  ** pOut ;                                                    * 
*        void  ** pCoef ;                                                   * 
*        void  ** pVar ;                                                    * 
*        int      sampleCount;                                              * 
*        uchar    channels;                                                 * 
*        uint     use;                                                      * 
*     }  PAF_FilParam;                                                      * 
*                                                                           * 
*   Let N be the maximum number of channels that are processed at a time.   * 
*                                                                           * 
*   pIn  -�Pointer to the array of �input channel data� pointers.           * 
*                                                                           * 
*      pIn [0][X0(n)], where X(n) is the input data block, of length �      * 
*          [1][ X1(n) ]                                      sampleCount    * 
*          ............                                                     * 
*          [N-1][ XN(n) ]                                                   * 
*                                                                           * 
*   pOut�- Pointer to the array of �output channel data� pointers.          * 
*                                                                           * 
*      pOut[0][Y0(n)]   , where Y(n) is the input data block, of length     * 
*          [1][ Y1(n) ]                                      sampleCount    * 
*          ............                                                     * 
*          [N-1][ YN(n) ]                                                   * 
*                                                                           * 
*                                                                           * 
*   pCoef - Pointer to the array of�channel coefficient� pointers.          * 
*                                                                           * 
*      pCoef[0][ C0(n) ] , where C(n) is the coefficient sequence of        * 
*           [1][ C1(n) ]                                      a channel.    * 
*           ............                                                    * 
*           [N-1][ CN(n) ]                                                  * 
*                                                                           * 
*   pVar - Pointer to the array of channel state/private memory�pointers.   * 
*                                                                           * 
*      pVar[0][ m0(n) ] , m(n) is the memory sequence belonged to           * 
*           [1][ m1(n) ]                                        a channel.  * 
*          ............                                                     * 
*           [N-1][ mN(n) ]                                                  * 
*                                                                           * 
*   channels - Specifies the number of channels that are to be filtered,    * 
*              starting sequentially from channel 0.                        * 
*                                                                           * 
*   sampleCount - Specifies the sample length of the input data block.      * 
*                                                                           * 
*   use - This is a 32 bit field that can be used, to fit in some extra     * 
*          parameters or as a pointer to additional filter parameters.      * 
*                                                                           * 
*                                                                           * 
* DESCRIPTION                                                               * 
*                                                                           * 
*      This routine implements IIR filter implementation for tap-2 &        * 
*      channels-7, unicoefficient,SP.                                       * 
*                                                                           * 
*                  ORDER 2    and CHANNELS 7                                * 
*                                                                           * 
* ilter equation  : H(z) =  b0 + b1*z~1 + b2*z~2                            * 
*                          ----------------------                           * 
*                           1  - a1*z~1 - a2*z~2                            * 
*                                                                           * 
*  Direct form    : y(n) = b0*x(n)+b1*x(n-1)+b2*x(n-2)+a1*y(n-1)+a2*y(n-2)  * 
*                                                                           * 
*  Canonical form : w(n) = x(n)    + a1*w(n-1) + a2*w(n-2)                  * 
*                   y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                  * 
*                                                                           * 
*  Filter variables :                                                       * 
*     y(n) - *y,         x(n)   - *x                                        * 
*     w(n) -             w(n-1) - w1         w(n-2) - w2                    * 
*     b0 - filtCfsB[0], b1 - filtCfsB[1], b2 - filtCfsB[2]                  * 
*     a1 - filtCfsA[0], a2 - filtCfsA[1]                                    * 
*                                                                           * 
*                                                                           * 
*                             w(n)    b0                                    * 
*    x(n)-->---[+]---->--------@------>>-----[+]--->--y(n)                  * 
*               |              |              |                             * 
*               ^              v              ^                             * 
*               |     a1    +-----+    b1     |                             * 
*              [+]----<<----| Z~1 |---->>----[+]                            * 
*               |           +-----+           |                             * 
*               |              |              |                             * 
*               ^              v              ^                             * 
*               |     a2    +-----+    b2     |                             * 
*                ----<<-----| Z~1 |---->>-----                              * 
*                           +-----+                                         * 
*                                                                           * 
*                                                                           * 
* Int Filter_iirT2Ch7_u( PAF_FilParam *pParam )                                    * 
*                                                                           * 
*   int count, samp;  //Loop counters                                       * 
*   float *restrict x_1,*restrict x_2; // Input ptr                         * 
*   float *restrict x_3,*restrict x_4; // Input ptr                         * 
*   float *restrict x_5,*restrict x_6; // Input ptr                         * 
*   float *restrict x_7; // Input ptr                                       * 
*   float *restrict y_1,*restrict y_2; // Output ptr                        * 
*   float *restrict y_3,*restrict y_4; // Output ptr                        * 
*   float *restrict y_5,*restrict y_6; // Output ptr                        * 
*   float *restrict y_7; // Output ptr                                      * 
*   float *restrict filtVars_1,*restrict filtVars_2; // Filter var mem ptr  * 
*   float *restrict filtVars_3,*restrict filtVars_4; // Filter var mem ptr  * 
*   float *restrict filtVars_5,*restrict filtVars_6; // Filter var mem ptr  * 
*   float *restrict filtVars_7; // Filter var mem ptr                       * 
*   float *filtCfsB, *filtCfsA;  //Feedforward ptrs                         * 
*   float accum1,accum2,accum3,accum4;  //Accumulator regs                  * 
*   float input_1, input_2, input_3, input_4;                               * 
*   float input_5, input_6, input_7;                                        * 
*   float w1_1, w2_1, w1_2, w2_2, w1_3, w2_3, w1_4, w2_4;//Filter states    * 
*   float w1_5, w2_5, w1_6, w2_6, w1_7, w2_7;//Filter states                * 
*                                                                           * 
*    //Get i/p ptr                                                          * 
*   x_1 = (float *)pParam->pIn[0];                                          * 
*   x_2 = (float *)pParam->pIn[1];                                          * 
*   x_3 = (float *)pParam->pIn[2];                                          * 
*   x_4 = (float *)pParam->pIn[3];                                          * 
*   x_5 = (float *)pParam->pIn[4];                                          * 
*   x_6 = (float *)pParam->pIn[5];                                          * 
*   x_7 = (float *)pParam->pIn[6];                                          * 
*                                                                           * 
*    //Get o/p ptr                                                          * 
*   y_1 = (float *)pParam->pOut[0];                                         * 
*   y_2 = (float *)pParam->pOut[1];                                         * 
*   y_3 = (float *)pParam->pOut[2];                                         * 
*   y_4 = (float *)pParam->pOut[3];                                         * 
*   y_5 = (float *)pParam->pOut[4];                                         * 
*   y_6 = (float *)pParam->pOut[5];                                         * 
*   y_7 = (float *)pParam->pOut[6];                                         * 
*                                                                           * 
*   filtCfsB  = (float *)pParam->pCoef[0];  //Feedforward ptr               * 
*   filtCfsA  = filtCfsB + 3;  //Derive feedback coeff ptr                  * 
*                                                                           * 
*    //Get filter var ptr                                                   * 
*   filtVars_1 = (float *)pParam->pVar[0];                                  * 
*   filtVars_2 = (float *)pParam->pVar[1];                                  * 
*   filtVars_3 = (float *)pParam->pVar[2];                                  * 
*   filtVars_4 = (float *)pParam->pVar[3];                                  * 
*   filtVars_5 = (float *)pParam->pVar[4];                                  * 
*   filtVars_6 = (float *)pParam->pVar[5];                                  * 
*   filtVars_7 = (float *)pParam->pVar[6];                                  * 
*                                                                           * 
*   count = pParam->sampleCount;  //I/p sample block-length                 * 
*                                                                           * 
*    //Get the filter states into corresponding regs                        * 
*   w1_1 = filtVars_1[0];                                                   * 
*   w2_1 = filtVars_1[1];                                                   * 
*                                                                           * 
*   w1_2 = filtVars_2[0];                                                   * 
*   w2_2 = filtVars_2[1];                                                   * 
*                                                                           * 
*   w1_3 = filtVars_3[0];                                                   * 
*   w2_3 = filtVars_3[1];                                                   * 
*                                                                           * 
*   w1_4 = filtVars_4[0];                                                   * 
*   w2_4 = filtVars_4[1];                                                   * 
*                                                                           * 
*   w1_5 = filtVars_5[0];                                                   * 
*   w2_5 = filtVars_5[1];                                                   * 
*                                                                           * 
*   w1_6 = filtVars_6[0];                                                   * 
*   w2_6 = filtVars_6[1];                                                   * 
*                                                                           * 
*   w1_7 = filtVars_7[0];                                                   * 
*   w2_7 = filtVars_7[1];                                                   * 
*                                                                           * 
*    //IIR filtering for i/p block length                                   * 
*                                                                           * 
*   #pragma MUST_ITERATE(16, 2048, 4)                                       * 
*   for (samp = 0; samp < count; samp++)                                    * 
*   {                                                                       * 
* /Channel-1                                                                * 
*       accum1 = filtCfsA[0]*w1_1;  //a1*w(n-1)                             * 
*       accum2 = filtCfsA[1]*w2_1;  //a2*w(n-2)                             * 
*       accum3 = filtCfsB[1]*w1_1;  //b1*w(n-1)                             * 
*       accum4 = filtCfsB[2]*w2_1;  //b2*w(n-2)                             * 
*                                                                           * 
*       input_1 = *x_1++;  //Get an input sample                            * 
*                                                                           * 
*       w2_1 = w1_1;  //Shift state registers                               * 
*                                                                           * 
*        //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              * 
*       w1_1 = input_1 + accum2 + accum1;                                   * 
*                                                                           * 
*        //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           * 
*       *y_1++ = filtCfsB[0]*w1_1 + accum3 + accum4;                        * 
*                                                                           * 
* /Channel-2                                                                * 
*       accum1 = filtCfsA[0]*w1_2;  //a1*w(n-1)                             * 
*       accum2 = filtCfsA[1]*w2_2;  //a2*w(n-2)                             * 
*       accum3 = filtCfsB[1]*w1_2;  //b1*w(n-1)                             * 
*       accum4 = filtCfsB[2]*w2_2;  //b2*w(n-2)                             * 
*                                                                           * 
*       input_2 = *x_2++;  //Get an input sample                            * 
*                                                                           * 
*       w2_2 = w1_2;  //Shift state registers                               * 
*                                                                           * 
*        //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              * 
*       w1_2 = input_2 + accum2 + accum1;                                   * 
*                                                                           * 
*        //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           * 
*       *y_2++ = filtCfsB[0]*w1_2 + accum3 + accum4;                        * 
*                                                                           * 
* /Channel-3                                                                * 
*       accum1 = filtCfsA[0]*w1_3;  //a1*w(n-1)                             * 
*       accum2 = filtCfsA[1]*w2_3;  //a2*w(n-2)                             * 
*       accum3 = filtCfsB[1]*w1_3;  //b1*w(n-1)                             * 
*       accum4 = filtCfsB[2]*w2_3;  //b2*w(n-2)                             * 
*                                                                           * 
*       input_3 = *x_3++;  //Get an input sample                            * 
*                                                                           * 
*       w2_3 = w1_3;  //Shift state registers                               * 
*                                                                           * 
*        //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              * 
*       w1_3 = input_3 + accum2 + accum1;                                   * 
*                                                                           * 
*        //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           * 
*       *y_3++ = filtCfsB[0]*w1_3 + accum3 + accum4;                        * 
*                                                                           * 
* /Channel-4                                                                * 
*       accum1 = filtCfsA[0]*w1_4;  //a1*w(n-1)                             * 
*       accum2 = filtCfsA[1]*w2_4;  //a2*w(n-2)                             * 
*       accum3 = filtCfsB[1]*w1_4;  //b1*w(n-1)                             * 
*       accum4 = filtCfsB[2]*w2_4;  //b2*w(n-2)                             * 
*                                                                           * 
*       input_4 = *x_4++;  //Get an input sample                            * 
*                                                                           * 
*       w2_4 = w1_4;  //Shift state registers                               * 
*                                                                           * 
*        //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              * 
*       w1_4 = input_4 + accum2 + accum1;                                   * 
*                                                                           * 
*        //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           * 
*       *y_4++ = filtCfsB[0]*w1_4 + accum3 + accum4;                        * 
*                                                                           * 
*                                                                           * 
* /Channel-5                                                                * 
*       accum1 = filtCfsA[0]*w1_5;  //a1*w(n-1)                             * 
*       accum2 = filtCfsA[1]*w2_5;  //a2*w(n-2)                             * 
*       accum3 = filtCfsB[1]*w1_5;  //b1*w(n-1)                             * 
*       accum4 = filtCfsB[2]*w2_5;  //b2*w(n-2)                             * 
*                                                                           * 
*       input_5 = *x_5++;  //Get an input sample                            * 
*                                                                           * 
*       w2_5 = w1_5;  //Shift state registers                               * 
*                                                                           * 
*        //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              * 
*       w1_5 = input_5 + accum2 + accum1;                                   * 
*                                                                           * 
*        //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           * 
*       *y_5++ = filtCfsB[0]*w1_5 + accum3 + accum4;                        * 
*                                                                           * 
* /Channel-6                                                                * 
*       accum1 = filtCfsA[0]*w1_6;  //a1*w(n-1)                             * 
*       accum2 = filtCfsA[1]*w2_6;  //a2*w(n-2)                             * 
*       accum3 = filtCfsB[1]*w1_6;  //b1*w(n-1)                             * 
*       accum4 = filtCfsB[2]*w2_6;  //b2*w(n-2)                             * 
*                                                                           * 
*       input_6 = *x_6++;  //Get an input sample                            * 
*                                                                           * 
*       w2_6 = w1_6;  //Shift state registers                               * 
*                                                                           * 
*        //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              * 
*       w1_6 = input_6 + accum2 + accum1;                                   * 
*                                                                           * 
*        //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           * 
*       *y_6++ = filtCfsB[0]*w1_6 + accum3 + accum4;                        * 
*                                                                           * 
* /Channel-7                                                                * 
*       accum1 = filtCfsA[0]*w1_7;  //a1*w(n-1)                             * 
*       accum2 = filtCfsA[1]*w2_7;  //a2*w(n-2)                             * 
*       accum3 = filtCfsB[1]*w1_7;  //b1*w(n-1)                             * 
*       accum4 = filtCfsB[2]*w2_7;  //b2*w(n-2)                             * 
*                                                                           * 
*       input_7 = *x_7++;  //Get an input sample                            * 
*                                                                           * 
*       w2_7 = w1_7;  //Shift state registers                               * 
*                                                                           * 
*        //w(n) = x(n) + a1*w(n-1) + a2*w(n-2)                              * 
*       w1_7 = input_7 + accum2 + accum1;                                   * 
*                                                                           * 
*        //y(n) = b0*w(n) + b1*w(n-1) + b2*w(n-2)                           * 
*       *y_7++ = filtCfsB[0]*w1_7 + accum3 + accum4;                        * 
*                                                                           * 
*   }                                                                       * 
*                                                                           * 
*    //Update state memory                                                  * 
*                                                                           * 
*   filtVars_1[0] = w1_1;                                                   * 
*   filtVars_1[1] = w2_1;                                                   * 
*   filtVars_2[0] = w1_2;                                                   * 
*   filtVars_2[1] = w2_2;                                                   * 
*   filtVars_3[0] = w1_3;                                                   * 
*   filtVars_3[1] = w2_3;                                                   * 
*   filtVars_4[0] = w1_4;                                                   * 
*   filtVars_4[1] = w2_4;                                                   * 
*                                                                           * 
*   filtVars_5[0] = w1_5;                                                   * 
*   filtVars_5[1] = w2_5;                                                   * 
*   filtVars_6[0] = w1_6;                                                   * 
*   filtVars_6[1] = w2_6;                                                   * 
*   filtVars_7[0] = w1_7;                                                   * 
*   filtVars_7[1] = w2_7;                                                   * 
*                                                                           * 
*                                                                           * 
* NOTES                                                                     * 
*                                                                           * 
*      1. Endian: This code is LITTLE ENDIAN.                               * 
*      2. interruptibility: This code is interrupt-tolerant but not         * 
*         interruptible.                                                    * 
* ========================================================================= *
		.global _Filter_iirT2Ch7_u
                .sect ".text:psa"

*                .include "Filter_iirT2Ch7_u_p.h67"

* ======================================================================== *
*S Place file level definitions here.                                     S*
* ======================================================================== *

_Filter_iirT2Ch7_u:   .cproc  pParam

        .no_mdep 
        .reg  pIn ;// Ptr to, array of ch i/p  ptrs
        .reg  pOut ;// Ptr to, array of ch o/p  ptrs
        .reg  pCoef ;// Ptr to, array of ch coeff  ptrs
        .reg  pVar ;// Ptr to, array of ch var  ptrs
	.reg  count; //Loop counters 

 	.reg x_1, x_2, x_3, x_4,x_5, x_6, x_7; // Input ptr                                       
 	.reg y_1, y_2, y_3, y_4,y_5, y_6, y_7; // Outputi ptr
                                       
 	.reg filtVars_1, filtVars_2, filtVars_3, filtVars_4;// Filter var mem ptr
        .reg filtVars_5, filtVars_6, filtVars_7; // Filter var mem ptr                                       
  	.reg filtptr, filtCfsB0, filtCfsB1, filtCfsB2, filtCfsA1, filtCfsA2;  //Feedforward ptrs
                         
  	.reg accum1,accum2,accum3,accum4;  //Accumulator regs                  
  	.reg input_1, input_2, input_3, input_4;                               
  	.reg input_5, input_6, input_7;                                        
  	.reg w1_1, w2_1, w1_2, w2_2, w1_3, w2_3, w1_4, w2_4;//Filter states    
  	.reg w1_5, w2_5, w1_6, w2_6, w1_7, w2_7;//Filter states                
  	.reg input_a1, input_a2, input_a3, input_a4;                               
  	.reg input_a5, input_a6, input_a7;                                        

* ======================================================================== *
*S Place source code here.                                                S*
* ======================================================================== *
             LDW   *pParam[0],        pIn   ;pIn   = pParam[0]
             LDW   *pParam[1],        pOut  ;pOut  = pParam[1]
             LDW   *pParam[2],        pCoef ;pCoef = pParam[2]
             LDW   *pParam[3],        pVar  ;pVar  = pParam[3]
             LDW   *pParam[4],        count ;count = pParam[4]
*********************** X ptrs ******************
             LDW   .1 *pIn[0], x_1 ;x_1 = pIn[0]
             LDW   .1 *pIn[1], x_2 ;x_2 = pIn[1]
             LDW   .1 *pIn[2], x_3 ;x_3 = pIn[2]
             LDW   .1 *pIn[3], x_4 ;x_4 = pIn[3]
             LDW   .1 *pIn[4], x_5 ;x_5 = pIn[4]
             LDW   .1 *pIn[5], x_6 ;x_6 = pIn[5]
             LDW   .1 *pIn[6], x_7 ;x_7 = pIn[6]
             
*********************** Y ptrs ******************
             LDW   .2 *pOut[0], y_1 ;y_1 = pOut[0]
             LDW   .2 *pOut[1], y_2 ;y_2 = pOut[1]
             LDW   .2 *pOut[2], y_3 ;y_3 = pOut[2]
             LDW   .2 *pOut[3], y_4 ;y_4 = pOut[3]
             LDW   .2 *pOut[4], y_5 ;y_5 = pOut[4]
             LDW   .2 *pOut[5], y_6 ;y_6 = pOut[5]
             LDW   .2 *pOut[6], y_7 ;y_7 = pOut[6]
            
*********************** filter coeffs************
             LDW    *pCoef[0], filtptr ;filtptr = *pCoef[0]
             LDW   .1 *filtptr[0], filtCfsB0 ;filtCfsB0 = *filtptr[0]
             LDW      *filtptr[1], input_a1 ;filtCfsB1 = *filtptr[1]
             MV    .2 input_a1,filtCfsB1; filtCfsB1 =input_a1
             LDW      *filtptr[2], filtCfsB2 ;filtCfsB2 = *filtptr[2]
             LDW   .1 *filtptr[3], filtCfsA1 ;filtCfsA1 = *filtptr[3]
             LDW   .1 *filtptr[4], filtCfsA2 ;filtCfsA2 = *filtptr[4]
             
********************VarR ptrs ******************
             LDW   *pVar[0],   filtVars_1 ;filtVars_1 = pVar[0]  
             LDW   *pVar[1],   filtVars_2 ;filtVars_2 = pVar[1]        
             LDW   *pVar[2],   filtVars_3 ;filtVars_3 = pVar[2]
             LDW   *pVar[3],   filtVars_4 ;filtVars_4 = pVar[3]
             LDW   *pVar[4],   filtVars_5 ;filtVars_5 = pVar[4]  
             LDW   *pVar[5],   filtVars_6 ;filtVars_6 = pVar[5]        
             LDW   *pVar[6],   filtVars_7 ;filtVars_7 = pVar[6]
	     
*********************Get filter states into regs************
             LDW   .1 *filtVars_1[0],     w1_1 ;w1(n-1) = filtVars_1[0]  
             LDW      *filtVars_1[1],     w2_1 ;w1(n-2) = filtVars_1[1] 
                                          
             LDW   .1 *filtVars_2[0],     w1_2 ;w2(n-1) = filtVars_2[0]  
             LDW      *filtVars_2[1],     w2_2 ;w2(n-2) = filtVars_2[1] 
                                          
             LDW   .1 *filtVars_3[0],     w1_3 ;w3(n-1) = filtVars_3[0]  
             LDW      *filtVars_3[1],     w2_3 ;w3(n-2) = filtVars_3[1] 
             
             LDW   .1 *filtVars_4[0],     w1_4 ;w4(n-1) = filtVars_4[0] 
             LDW      *filtVars_4[1],     w2_4 ;w4(n-2) = filtVars_4[1]

             LDW   .1 *filtVars_5[0],     w1_5 ;w2(n-1) = filtVars_2[0]  
             LDW      *filtVars_5[1],     w2_5 ;w2(n-2) = filtVars_2[1] 
                                          
             LDW   .1 *filtVars_6[0],     w1_6 ;w3(n-1) = filtVars_3[0]  
             LDW      *filtVars_6[1],     w2_6 ;w3(n-2) = filtVars_3[1] 
             
             LDW   .1 *filtVars_7[0],     w1_7 ;w4(n-1) = filtVars_4[0] 
             LDW      *filtVars_7[1],     w2_7 ;w4(n-2) = filtVars_4[1]

LOOP:      .trip 16,2048,4

;CHANNEL-1
	   MPYSP  filtCfsA1,w1_1,accum1 
	   MPYSP  filtCfsA2,w2_1,accum2 
	   MPYSP  filtCfsB1,w1_1,accum3 
	   MPYSP  filtCfsB2,w2_1,accum4

           LDW .1 *x_1++,input_1
           MV  .2 w1_1,w2_1 
	   ADDSP  input_1,accum2,accum2
	   ADDSP accum1,accum2,w1_1
	   MPYSP w1_1,filtCfsB0,input_a1
	   ADDSP input_a1,accum3,accum3
	   ADDSP accum3,accum4,accum4
           STW   accum4,*y_1++

;CHANNEL-2
	   MPYSP filtCfsA1,w1_2,accum1 
	   MPYSP filtCfsA2,w2_2,accum2 
           MPYSP filtCfsB1,w1_2,accum3 
           MPYSP filtCfsB2,w2_2,accum4

           LDW .1 *x_2++,input_2
           MV  .2 w1_2,w2_2 
           ADDSP input_2,accum2,accum2
           ADDSP accum1,accum2,w1_2
           MPYSP w1_2,filtCfsB0,input_a2
           ADDSP input_a2,accum3,accum3
           ADDSP accum3,accum4,accum4
           STW   accum4,*y_2++
 
;CHANNEL-3
           MPYSP filtCfsA1,w1_3,accum1 
           MPYSP filtCfsA2,w2_3,accum2 
           MPYSP filtCfsB1,w1_3,accum3 
           MPYSP filtCfsB2,w2_3,accum4

	   LDW .1 *x_3++,input_3
	   MV  .2 w1_3,w2_3 
           ADDSP input_3,accum2,accum2
	   ADDSP accum1,accum2,w1_3
	   MPYSP w1_3,filtCfsB0,input_a3
	   ADDSP input_a3,accum3,accum3
	   ADDSP accum3,accum4,accum4
	   STW   accum4,*y_3++

;CHANNEL-4
	   MPYSP filtCfsA1,w1_4,accum1 
	   MPYSP filtCfsA2,w2_4,accum2 
	   MPYSP filtCfsB1,w1_4,accum3 
	   MPYSP filtCfsB2,w2_4,accum4

	   LDW .1 *x_4++,input_4
	   MV  .2 w1_4,w2_4 
	   ADDSP input_4,accum2,accum2
	   ADDSP accum1,accum2,w1_4
	   MPYSP w1_4,filtCfsB0,input_a4
	   ADDSP input_a4,accum3,accum3
	   ADDSP accum3,accum4,accum4
	   STW   accum4,*y_4++
;CHANNEL-5
	   MPYSP filtCfsA1,w1_5,accum1 
	   MPYSP filtCfsA2,w2_5,accum2 
	   MPYSP filtCfsB1,w1_5,accum3 
	   MPYSP filtCfsB2,w2_5,accum4
	
	   LDW .1 *x_5++,input_5
	   MV  .2 w1_5,w2_5 
	   ADDSP input_5,accum2,accum2
	   ADDSP accum1,accum2,w1_5
	   MPYSP w1_5,filtCfsB0,input_a5
	   ADDSP input_a5,accum3,accum3
	   ADDSP accum3,accum4,accum4
	   STW   accum4,*y_5++
;CHANNEL-6
	   MPYSP filtCfsA1,w1_6,accum1 
	   MPYSP filtCfsA2,w2_6,accum2 
	   MPYSP filtCfsB1,w1_6,accum3 
	   MPYSP filtCfsB2,w2_6,accum4
	
	   LDW .1 *x_6++,input_6
	   MV  .2 	w1_6,w2_6 
	   ADDSP input_6,accum2,accum2
	   ADDSP accum1,accum2,w1_6
	   MPYSP w1_6,filtCfsB0,input_a6
	   ADDSP input_a6,accum3,accum3
	   ADDSP accum3,accum4,accum4
	   STW   accum4,*y_6++
	   
;CHANNEL-7
	   MPYSP filtCfsA1,w1_7,accum1 
	   MPYSP filtCfsA2,w2_7,accum2 
	   MPYSP filtCfsB1,w1_7,accum3 
	   MPYSP filtCfsB2,w2_7,accum4
	
	   LDW .1 *x_7++,input_7
	   MV  .2  w1_7,w2_7 
	   ADDSP input_7,accum2,accum2
	   ADDSP accum1,accum2,w1_7
	   MPYSP w1_7,filtCfsB0,input_a7
	   ADDSP input_a7,accum3,accum3
	   ADDSP accum3,accum4,accum4
	   STW   accum4,*y_7++

	   SUB count,1,count
[count]    B LOOP

; //restore the state registers
	   STW w1_1,*filtVars_1[0]
	   STW w2_1,*filtVars_1[1]
           STW w1_2,*filtVars_2[0]
 	   STW w2_2,*filtVars_2[1]
 	   STW w1_3,*filtVars_3[0]
	   STW w2_3,*filtVars_3[1]
           STW w1_4,*filtVars_4[0]
           STW w2_4,*filtVars_4[1]
           STW w1_5,*filtVars_5[0]
           STW w2_5,*filtVars_5[1]
           STW w1_6,*filtVars_6[0]
           STW w2_6,*filtVars_6[1]
           STW w1_7,*filtVars_7[0]
           STW w2_7,*filtVars_7[1]
* ======================================================================== *
*S Place source code here.                                                S*
* ======================================================================== *
		.return count
                .endproc

* ======================================================================== *
*  End of file: Filter_iirT2Ch7_u_p.sa                                            *
* ======================================================================== *
