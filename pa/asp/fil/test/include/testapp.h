/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

/**
 *  @file   testapp.h
 *
 *  @brief  Filter test application declarations.
 *
 *          This is a filter Layer 2 test application. 
 *          The application test the various implementations of filters.
 *
 *          It expects the input from a wave file and generate the output
 *          wave file after filter operation.
 *
 *          The filter coefficients and implementation parameters are recieved
 *          from external tables. The test application will configure the neccessary
 *          parameters and allocate memory for filter input/output buffer and filter
 *          memory states.
 *
 *****************************************************************************/

#ifndef __TESTAPP_H__
#define __TESTAPP_H__

#include "fil_datatype.h"
#include "fil_table.h"

typedef Int (*FIL_filterFnPtr)(PAF_FilParam *);

// Test table structure
typedef struct {
    FIL_filterFnPtr filterFn; // Function ptr that is tested
    Int taps;                 // Taps per cascade
    Int ch;                   // Channels
    Int uniCoef;              // Unicoef(1) or not(0)
    Int inplace;              // Inplace(1) or not(0)
    Int cascade;              // No of cascades - 1. ie if 4 taps, then this field is 1 (2(cascades) - 1)
}tTestTable, *pTestTable;



typedef struct {
    Short  taps;
    Short  cIdx;
} PAF_FilParam_use;



/** @brief Global filter table index*/ 
typedef enum {
    TESTAPP_IIR_FILTER,
    TESTAPP_FIR_FILTER,
    TESTAPP_IIR_MX_FILTER,
    TESTAPP_IIR_CAS_FILTER,
    TESTAPP_FILTER_COUNT
} TESTAPP_FILTER_INDEX;

#define MAX_FILE_NAME_SIZE  35

#ifndef PAF_AudioData
#define PAF_AudioData    float
#endif

#endif

