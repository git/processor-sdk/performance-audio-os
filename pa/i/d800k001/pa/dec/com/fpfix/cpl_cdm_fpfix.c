
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <cpl.h>
#include <paftyp_a.h>
#include <paftyp.h>
#include <pafdec.h>
#include <math.h>

void CDMTo_Mono_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	char FromAux = pCDMConfig->channelConfigurationFrom.part.aux;
	float clev = pCDMConfig->clev;
	float slev = pCDMConfig->slev;
	float blev = 1.0f;
	char dualMonoMode  = pCDMConfig->sourceDual;	
	
	switch(FromSat)
	{
		case PAF_CC_SAT_STEREO: // C = M3DB(L + R) or for Dual Mono with MonoMix c = M6DB(L+R)
   			if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 0)
			{
				pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			}
   			else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 1)
			{
				pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = ONE;
			}
   			else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 2)
			{
				pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = ONE;
			}
   			else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 3)
			{
				pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M6DB;
				pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M6DB;				
			}
			else
			{
				pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			}
			break;
			
		case PAF_CC_SAT_PHANTOM1://C = 3db(L + R) + Ls*slev
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] =slev;
			break;
						
		case PAF_CC_SAT_PHANTOM2://C = 3db(L + R) + 3db*slev*(Ls + Rs)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			break;

		case PAF_CC_SAT_PHANTOM3: // C = 3db*(L+R) + 3db*slev*(Ls+Rs) + slev*blev(Lb)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LBAK] = slev*blev;
			break;

		case PAF_CC_SAT_PHANTOM4: // C = 3db*(L+R) + 3db*slev*(Ls+Rs) + 3db*slev*blev(Lb+Rb)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LBAK] = M3DB*slev*blev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RBAK] = M3DB*slev*blev;
			break;

		case PAF_CC_SAT_SURROUND0: //C = 2*M3DB*clev*C + M3DB(L+R)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = 2*M3DB*clev;
			break;    

		case PAF_CC_SAT_SURROUND1: //C = 2*M3DB*clev*C + 3db(L + R) + Ls*slev
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = 2*M3DB*clev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = slev;
			break;    

		case PAF_CC_SAT_SURROUND2: //C = 2*M3DB*clev*C + 3db(L + R) + M3DB*slev*(Ls+Rs)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = 2*M3DB*clev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			break;    

		case PAF_CC_SAT_SURROUND3: // C = C + 3db*(L+R) + 3db*slev*(Ls+Rs) + slev*blev(Lb)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LBAK] = slev*blev;
			break;

		case PAF_CC_SAT_SURROUND4: //C = C + 3db(L + R) + M3DB*slev*(Ls + Rs) + M3DB*slev*blev*(Lb+Rb)
			pCDMConfig->coeff[PAF_CNTR][CDM_LEFT] = M3DB;
			pCDMConfig->coeff[PAF_CNTR][CDM_RGHT] = M3DB;
			//pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			pCDMConfig->coeff[PAF_CNTR][CDM_LSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RSUR] = M3DB*slev;
			pCDMConfig->coeff[PAF_CNTR][CDM_LBAK] = M3DB*slev*blev;
			pCDMConfig->coeff[PAF_CNTR][CDM_RBAK] = M3DB*slev*blev;
			break;
	}
	
}

/*****************************************************************************/	
void CDMTo_Stereo_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{		
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	char FromAux = pCDMConfig->channelConfigurationFrom.part.aux;
	char ToAux = pCDMConfig->channelConfigurationTo.part.aux;
	float clev = pCDMConfig->clev;
	float slev = pCDMConfig->slev;
	float blev = 1.0f;
	char dualMonoMode  = pCDMConfig->sourceDual;
		
	switch(FromSat)
	{
		case PAF_CC_SAT_MONO: // L = M3DB*C, R = M3DB*C
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
			break;
			
		case PAF_CC_SAT_STEREO: 
		/* What if 
		input = LtRt and downmix = something STEREO else 
		input = LoRo and downmix = something STEREO else
		input = StereoMono and downmix = something STEREO else  
		input = StereoDual and downmix = something STEREO else 
		Check in all sucj conditions what is Downmix.part.aux?
		*/
   		//	if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 0) //L = L, R = R
		//	{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
		//	}
   		//	else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 1) //L = R =  M3DB*L,
   		    if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 1) //L = R =  M3DB*L,
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R = L
				
				pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
			}
   			else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 2)//L = R = M3DB*R
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ZERO; //As it was earlier made "ONE"
				
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;//After getting L, R = L

				pCDMConfig->coeff[PAF_RGHT][PAF_RGHT] = ZERO; //As it was earlier made "ONE"										
			}
   			else if(FromAux == PAF_CC_AUX_STEREO_DUAL && dualMonoMode == 3) // L = R = M6DB(L + R)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M6DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M6DB;
				
				pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;//After getting L, R = L	
				
				pCDMConfig->coeff[PAF_RGHT][PAF_RGHT] = ZERO; //As it was earlier made "ONE"										
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L = R = M3DB(L + R)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				
				pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;//After getting L, R = L
				
				pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
			}
		//	else
			//{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
		//	}
			break;
			
		case PAF_CC_SAT_PHANTOM1:
			/* If Request is LtRt, then Downmix LtRt
			 else in all other conditions, it should be LoRo
			 And Similar for other input channel config.
			 */
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt-=Ls*3db   Rt+=Ls*3db
			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L=R=M3DB*(L+R) + slev*Ls
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
			
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L
			        
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
			}
			else // Lo+=Ls*M3DB*slev   Ro+=Ls*M3DB*slev

			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
				
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;									
			}
			break;
						
		case PAF_CC_SAT_PHANTOM2:

			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt-=3db*(Ls+Rs)   Rt+=3db*(Ls+Rs)

			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L=R=M3DB(L+R) + M3DB*slev(Ls+Rs)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
			
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L
			        
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
			}			
			else // Lo+=Ls*slev   Ro+=Rs*slev

			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
				
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;			
			}
			break;

		case PAF_CC_SAT_PHANTOM3:
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt = L - 3db*(Ls+Rs) - Lb; Rt = R + 3db*(Ls+Rs) + Lb
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = -ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = ONE;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L = R = 3db(L+R) + 3db*slev(Ls+Rs) + slev*blev(Lb)
			{
			    pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
			    pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
			    pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
			    pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
			    pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = slev*blev;
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO;
			}
			else // Lo = L + slev*(Ls) + 3db*slev*blev(Lb); Ro = R + slev*(Rs) + 3db*slev*blev(Lb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*slev*blev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB*slev*blev;
            }
			break;

		case PAF_CC_SAT_PHANTOM4:
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt = L - 3db*(Ls+Rs) - 3db*(Lb+Rb); Rt = R + 3db*(Ls+Rs) + 3db*(Lb+Rb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RBAK] = -M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L = R = 3db(L+R) + 3db*slev(Ls+Rs) + 3db*slev*blev(Lb+Rb)
			{
			    pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
			    pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
			    pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
			    pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
			    pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*slev*blev;
			    pCDMConfig->coeff[PAF_LEFT][CDM_RBAK] = M3DB*slev*blev;
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO;
			}
			else // Lo = L + slev*(Ls) + slev*blev(Lb); Ro = R + slev*(Rs) + slev*blev(Rb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = slev*blev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = slev*blev;
            }
			break;

		case PAF_CC_SAT_SURROUND0: 
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt+= M3DB*C Rt+= M3DB*C   
			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO)//L=R=M3DB*(L+R)+2*M3DB*clev*C
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = 2*M3DB*clev;
				
				pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L	
				
                pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"										
			}
			else//Lo+= clev*C, Ro+= clev*C   
			{
				//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				
				//pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;			
			}
			break;    

		case PAF_CC_SAT_SURROUND1: 
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) //Lt+= 3db*C - Ls*3db Rt+= 3db*C + Ls*3db

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L=R=M3DB(L+R) + 2*M3DB*clev*C + slev*Ls
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = 2*M3DB*clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
			
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L
			    
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
			}
			else // Lo+= clev*C + Ls*M3DB*slev   Ro+= clev*C + Ls*M3DB*slev

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;				
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;
			}
			break;    

		case PAF_CC_SAT_SURROUND2: 
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) //Lt+= 3db*C - 3db*(Ls+RS) Rt+= 3db*C + 3db*(Ls+RS)

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L=R=3db(L+R) + 2*M3DB*clev*C + M3DB*slev*(Ls+Rs)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = 2*M3DB*clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
			
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L
			    
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
			}
			else // Lo+= clev*C + slev*Ls   Ro+= clev*C + slev*Rs

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;				
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
			}
			break;  
			  
		case PAF_CC_SAT_SURROUND3:
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) // Lt = L + 3db*(C) - 3db*(Ls+Rs) - Lb; Rt = R + 3db*(C) + 3db*(Ls+Rs) + Lb
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = -ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = ONE;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L = R = 2*M3DB*clev*C + 3db(L+R) + 3db*slev(Ls+Rs) + slev*blev(Lb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = 2*M3DB*clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = slev*blev;
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE;
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO;
			}
			else // Lo = L + clev*(C) + slev*(Ls) + 3db*slev*blev(Lb); Ro = R + clev*(C) + slev*(Rs) + 3db*slev*blev(Lb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*slev*blev;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB*slev*blev;
            }
			break;

		case PAF_CC_SAT_SURROUND4: 
			if(ToAux == PAF_CC_AUX_STEREO_LTRT) //Lt+= 3db*C - 3db*(Ls+RS) - 3db*(Lb+Rb) Rt+= 3db*C + 3db*(Ls+RS) + 3db*(Lb+Rb)

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RBAK] = -M3DB;

				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = M3DB;
			}
			else if(ToAux == PAF_CC_AUX_STEREO_MONO) // L=R=3db(L+R) + 2*M3DB*clev*C + 3db(Ls+Rs) + 3db(Lb+Rb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_RGHT] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = 2*M3DB*clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_RSUR] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*slev;
				pCDMConfig->coeff[PAF_LEFT][CDM_RBAK] = M3DB*slev;
			
			    pCDMConfig->coeff[PAF_RGHT][CDM_LEFT] = ONE; //After getting L, R=L
			    
			    pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ZERO; //As it was earlier made "ONE"
			}
			else // Lo+= clev*C + 3db*Ls + 3db*blev*Lb   Ro+= clev*C + 3db*Rs + 3db*blev*Rb

			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB;
				pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*blev;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
				pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = M3DB*blev;
			}
			break;    

	}
	return;
}

/*****************************************************************************/	
void CDMTo_Phantom1_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	
	float clev = pCDMConfig->clev;
	float blev = 1.0f;

	switch(FromSat)
	{						
		case PAF_CC_SAT_PHANTOM2://L=L R=R Ls=3db(Ls+Rs)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			break;

		case PAF_CC_SAT_PHANTOM3: // Ls = 3db*(Ls+Rs) + blev*(Lb)
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
			break;

		case PAF_CC_SAT_PHANTOM4: // Ls = 3db*(Ls+Rs) + 3db*blev*(Lb+Rb)
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB*blev;
			pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = M3DB*blev;
			break;

		case PAF_CC_SAT_SURROUND1: //L+= clev*C R+= clev*C Ls=Ls
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;			
			break;    

		case PAF_CC_SAT_SURROUND2: //L+= clev*C R+= clev*C Ls=3db(Ls+Rs)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			
		    pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			break;  

		case PAF_CC_SAT_SURROUND3: // L = L + clev*(C); R = R + clev*(C); Ls = 3db*(Ls+Rs) + blev*(Lb)
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
			break;

		case PAF_CC_SAT_SURROUND4: //L+= clev*C R+= clev*C Ls=3db(Ls+Rs)+ 3db(Lb+Rb)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = M3DB;	
	}
	return;
}

/*****************************************************************************/	
void CDMTo_Phantom2_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	char ToAux = pCDMConfig->channelConfigurationTo.part.aux;
		
	float clev = pCDMConfig->clev;	
	float blev = 1.0f;
	
	switch(FromSat)
	{						
		case PAF_CC_SAT_PHANTOM1://L=L R=R Ls=Rs=3db*Ls   
			//pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			
			pCDMConfig->coeff[PAF_RSUR][CDM_LSUR] = ONE;////After getting Ls, Rs = Ls						
			break;

		case PAF_CC_SAT_PHANTOM3: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT)// L=L R=R Ls' =  Ls - 0.707*Lb  Rs' = Rs + 0.707*Lb
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
			}
			else // L=L R=R Ls = Ls + 3db*blev*(Lb); Rs = Rs + 3db*blev*(Lb)
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB*blev;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB*blev;
			}
			break;

		case PAF_CC_SAT_PHANTOM4: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT)// L=L R=R Ls' = Ls - 0.707 * ( Lb + Rb ) Rs' = Rs + 0.707 * ( Lb + Rb )
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = -M3DB;

				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = M3DB;
			}
			else // Ls = Ls + blev*(Lb); Rs = Rs + blev*(Rb)
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
				pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = blev;
			}
			break;

		case PAF_CC_SAT_SURROUND1: //L+= clev*C R+= clev*C Ls=Rs=3db*Ls  
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			
			pCDMConfig->coeff[PAF_RSUR][CDM_LSUR] = ONE;////After getting Ls, Rs = Ls
			break;    

		case PAF_CC_SAT_SURROUND2: //L+= clev*C R+= clev*C Ls = Ls Rs = Rs
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
			
		//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
			
		//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
			break;    

		case PAF_CC_SAT_SURROUND3: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT)//L' = L + clev*C R' = R + clev*C Ls' =  Ls - 0.707*Lb Rs' = Rs + 0.707*Lb
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
			}
			else // L = L + clev*(C); R = R + clev*(C); Ls = Ls + 3db*blev*(Lb); Rs = Rs + 3db*blev*(Lb)
			{
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB*blev;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB*blev;
			}
			break;

		case PAF_CC_SAT_SURROUND4: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT) //L' = L + clev*C R' = R + clev*C Ls' = Ls - 0.707 * ( Lb + Rb )  Rs' = Rs + 0.707 * ( Lb + Rb ) 
			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				
			//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = -M3DB;
												
			//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = M3DB;

			}
			else//L+= clev*C R+= clev*C Ls = Ls + Lb Rs = Rs + Rb
			{
			//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
				pCDMConfig->coeff[PAF_LEFT][CDM_CNTR] = clev;
				
			//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
				pCDMConfig->coeff[PAF_RGHT][CDM_CNTR] = clev;
				
			//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
				
			//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
				pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = blev;
			}
			break;
	}
	return;
}

/*****************************************************************************/	
void CDMTo_3Stereo_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	
	float slev = pCDMConfig->slev;
	float blev = 1.0f;
	
	switch(FromSat)
	{						
		case PAF_CC_SAT_SURROUND1://L+=M3DB*slev*Ls R+=M3DB*slev*Ls C = C
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB*slev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_LSUR] = M3DB*slev;

		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			break;

		case PAF_CC_SAT_SURROUND2: //L+= slev*Ls R+= slev*Rs C = C
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			break;    

		case PAF_CC_SAT_SURROUND3: // L = L + slev*(Ls) + 3db*slev*blev*(Lb); R = R + slev(Rs) + 3db*slev*blev*(Lb)
			pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = slev;
			pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = M3DB*slev*blev;
			pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = slev;
			pCDMConfig->coeff[PAF_RGHT][CDM_LBAK] = M3DB*slev*blev;
			break;

		case PAF_CC_SAT_SURROUND4: //L+= M3DB*Ls + slev*blev*Lb R+= 3db*Rs + slev*blev*Rb  C = C
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
			pCDMConfig->coeff[PAF_LEFT][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LEFT][CDM_LBAK] = slev*blev;
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			pCDMConfig->coeff[PAF_RGHT][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_RGHT][CDM_RBAK] = slev*blev;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			break;    

	}
	return;
}

/*****************************************************************************/	
void CDMTo_Surround1_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	
	float blev = 1.0f;

	switch(FromSat)
	{						
		case PAF_CC_SAT_SURROUND2: //L= L R = R C = C Ls=3db*(Ls+Rs)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;			
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			break;    

		case PAF_CC_SAT_SURROUND3: // Ls = 3db*(Ls+Rs) + blev*(Lb)
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
			break;

		case PAF_CC_SAT_SURROUND4: //L= L R = R C = C Ls=3db*(Ls+Rs)+3db*blev*(Lb+Rb)
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;			
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_RSUR] = M3DB;
			pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB*blev;
			pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = M3DB*blev;
			break;    

	}
	return;
}

/*****************************************************************************/	
void CDMTo_Surround2_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	char FromSat = pCDMConfig->channelConfigurationFrom.part.sat;
	char ToAux = pCDMConfig->channelConfigurationTo.part.aux;
		
	float blev = 1.0f;

	switch(FromSat)
	{						
		case PAF_CC_SAT_SURROUND1: //L= L R = R C = C Ls=Rs=3db*Ls
		//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;			
			
		//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
			
		//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
			
			pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = M3DB;
			
			pCDMConfig->coeff[PAF_RSUR][CDM_LSUR] = ONE;////After getting Ls, Rs = Ls						
			break;    

		case PAF_CC_SAT_SURROUND3: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT)//Ls� = Ls - 0.707 * Lb Rs� = Rs + 0.707 * Lb
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
			}
			else// Ls = Ls + 3db*blev*(Lb); Rs = Rs + 3db*blev*(Lb)
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = M3DB*blev;
				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB*blev;
			}
			break;

		case PAF_CC_SAT_SURROUND4: 
			if(ToAux == PAF_CC_AUX_SURROUND2_LTRT)//Ls� = Ls - 0.707 * ( Lb + Rb ) Rs� = Rs + 0.707 * ( Lb + Rb )  
			{
				pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = -M3DB;
				pCDMConfig->coeff[PAF_LSUR][CDM_RBAK] = -M3DB;

				pCDMConfig->coeff[PAF_RSUR][CDM_LBAK] = M3DB;
				pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = M3DB;
			}			
			else//L= L R = R C = C Ls+=blev*Lb Rs+=blev*Rb
			{
				//	pCDMConfig->coeff[PAF_LEFT][CDM_LEFT] = ONE;
					
				//	pCDMConfig->coeff[PAF_RGHT][CDM_RGHT] = ONE;
					
				//	pCDMConfig->coeff[PAF_CNTR][CDM_CNTR] = ONE;
					
				//	pCDMConfig->coeff[PAF_LSUR][CDM_LSUR] = ONE;
					pCDMConfig->coeff[PAF_LSUR][CDM_LBAK] = blev;
					
				//	pCDMConfig->coeff[PAF_RSUR][CDM_RSUR] = ONE;
					pCDMConfig->coeff[PAF_RSUR][CDM_RBAK] = blev;
			}
			break;
	}
	return;
}

#if CDM_LEFT != 0 || CDM_RGHT != 1 || CDM_CNTR != 2 && \
    CDM_LSUR != 3 || CDM_RSUR != 4 || CDM_LBAK != 5 && \
	CDM_RBAK != 6 || CDM_SUBW != 7
#error CdmChToPafChMap is not valid because CDM_XXXX has changed
#endif

extern const char CdmChToPafChMap[CDM_CHAN_N];

/*************************************************************************/
/*
This function requires that the follwoing field has already been populated
	- ToMask
	- coeff
*/

#ifndef _TMS320C6X
#define	_fabsf		fabs_c
float fabs_c(float src1);
#endif

void CPL_cdm_samsiz_(void *pv,PAF_AudioFrame *pAudioFrame,CPL_CDM *pCDMConfig)
{        
    PAF_AudioSize * restrict samsiz = pAudioFrame->data.samsiz;
    
    PAF_ChannelMask ToMask = pCDMConfig->ToMask;

    char i, j; 
    
    PAF_AudioData SigmaCoeff;
         
    for(i=0;i<PAF_HEAD_N;i++)
    {
    	if((ToMask & (1 << i)))
    	{
    		SigmaCoeff=0;
    		for(j=0;j<CDM_CHAN_N;j++)
    			SigmaCoeff += _fabsf(pCDMConfig->coeff[i][j]);

    		if(SigmaCoeff)
//    		 samsiz[i] = (int)ceil(2*20*log10(SigmaCoeff));
    		 samsiz[i] = 2*20*log10(SigmaCoeff);
    	}
    }
}
/************************************************************************/
/*
This function requires that the follwoing field has been populated by the decoder
	- channelConfigurationFrom
	- channelConfigurationTo
	- sourceDual
	- clev
	- slev
	- LFEDmxVolume
*/
void CPL_cdm_downmixSetUp_(void *pv,PAF_AudioFrame *pAudioFrame, CPL_CDM *pCDMConfig)
{
	
	PAF_ChannelMask FromMask, ToMask;
	
	char FromSub,ToSub;	
	
	PAF_ChannelConfiguration FromConfig, ToConfig;
	
	XDAS_Int8 ToSat;
	
	
	PAF_AudioData LFEDmxVolume = pCDMConfig->LFEDmxVolume;
	
	char i,j;
/*
	coeff[][0] = coeff multiplied with L;
	coeff[][1] = coeff multiplied with R;
	coeff[][2] = coeff multiplied with C;
	coeff[][3] = coeff multiplied with Ls;
	coeff[][4] = coeff multiplied with Rs;
	coeff[][5] = coeff multiplied with Lb;
	coeff[][6] = coeff multiplied with Rb;
	coeff[][7] = coeff multiplied with Sub;
	coeff[PAF_LEFT][] = coeff for L; 
	coeff[PAF_RGHT][] = coeff for R; 
	coeff[PAF_CNTR][] = coeff for C; 
	coeff[PAF_LSUR][] = coeff for Ls; 
	coeff[PAF_RSUR][] = coeff for Rs; 
	coeff[PAF_LBAK][] = coeff for Lb; 
	coeff[PAF_RBAK][] = coeff for Rb; 
	coeff[PAF_SUBW][] = coeff for Sub; , this is not required
*/


	FromConfig = pCDMConfig->channelConfigurationFrom ;
	ToConfig = pCDMConfig->channelConfigurationTo ; 
	ToSat = pCDMConfig->channelConfigurationTo.part.sat;
	FromSub = pCDMConfig->channelConfigurationFrom.part.sub;
	ToSub = pCDMConfig->channelConfigurationTo.part.sub;
					
	FromMask = pAudioFrame->fxns->channelMask (pAudioFrame, FromConfig);
	ToMask = pAudioFrame->fxns->channelMask (pAudioFrame, ToConfig);
	
	pCDMConfig->FromMask = FromMask;
	pCDMConfig->ToMask = ToMask;
		
    	for(i=0;i<CDM_CHAN_N;i++)
    	{
    		pCDMConfig->coeff[PAF_LEFT][i]=0;
    		pCDMConfig->coeff[PAF_RGHT][i]=0;
    		pCDMConfig->coeff[PAF_CNTR][i]=0;
    		pCDMConfig->coeff[PAF_LSUR][i]=0;
    		pCDMConfig->coeff[PAF_RSUR][i]=0;
    		pCDMConfig->coeff[PAF_LBAK][i]=0;
    		pCDMConfig->coeff[PAF_RBAK][i]=0;
    		pCDMConfig->coeff[PAF_SUBW][i]=0;
    	} 

	for(i=0;i<PAF_HEAD_N;i++)
	{          
        if(i<=2) 
         j = i;
        else
         j = i-5;
     // PAF_MAXNUMCHAN is 16.
          
         if ((ToMask & (1 << i)) && (FromMask & (1 << i)))
          pCDMConfig->coeff[i][j]=ONE;
	}
    	    	  
    	switch(ToSat)
	{
		case PAF_CC_SAT_MONO:
			CDMTo_Mono(pv,pAudioFrame,pCDMConfig);
		break;
		
		case PAF_CC_SAT_STEREO:
			CDMTo_Stereo(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_PHANTOM1:
			CDMTo_Phantom1(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_PHANTOM2:
			CDMTo_Phantom2(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_PHANTOM3:
			CDMTo_Phantom3(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_PHANTOM4:
			CDMTo_Phantom4(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_3STEREO:
			CDMTo_3Stereo(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_SURROUND1:
			CDMTo_Surround1(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_SURROUND2:
			CDMTo_Surround2(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_SURROUND3:
			CDMTo_Surround3(pv,pAudioFrame,pCDMConfig);
		break;

		case PAF_CC_SAT_SURROUND4:
			CDMTo_Surround4(pv,pAudioFrame,pCDMConfig);
		break;
	}

	if(FromSub == 1 && ToSub == 0)
	{ 
	  if (ToMask & (1 << PAF_LEFT))
	   pCDMConfig->coeff[PAF_LEFT][CDM_SUBW] =  M3DB*LFEDmxVolume;
	   
	  if (ToMask & (1 << PAF_RGHT))
	   pCDMConfig->coeff[PAF_RGHT][CDM_SUBW] =  M3DB*LFEDmxVolume;

	  if ((ToMask & (1 << PAF_CNTR)) && !(ToMask & (1 << PAF_LEFT)) && !(ToMask & (1 << PAF_RGHT)))
	   pCDMConfig->coeff[PAF_CNTR][CDM_SUBW] =  LFEDmxVolume;
	  
	}
}
