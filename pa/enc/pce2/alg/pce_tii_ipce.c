
/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Vendor-specific (TII) PCM Encoder algoritm functionality implementation
//
//
//

/*
 *  PCE Module implementation - TII implementation of a PCM Encoder algoritm.
 */

#include <xdc/std.h> //<std.h>

#include <string.h> /* memset */

#include <ipce.h>
#include <pce_tii.h>
#include <pce_tii_priv.h>
#include <pceerr.h>

#include "paftyp.h"

#include <pafhjt.h>

// -----------------------------------------------------------------------------
// Debugging Trace Control, local to this file.
// 
#include <logp.h>

// allows you to set a different trace module in pa.cfg
#define TR_MOD  trace

// Allow a developer to selectively enable tracing.
#define CURRENT_TRACE_MASK      0x00   // terse only

#define TRACE_MASK_TERSE        0x01   // only flag errors
#define TRACE_MASK_GENERAL      0x02   // general parameters
#define TRACE_MASK_VERBOSE      0x04   // not yet in use
#define TRACE_MASK_DATA         0x08   // not yet in use
#define TRACE_MASK_TIME         0x10   // not yet in use

#if (CURRENT_TRACE_MASK & TRACE_MASK_TERSE)
 #define TRACE_TERSE(a) LOG_printf a
#else
 #define TRACE_TERSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_GENERAL)
 #define TRACE_GEN(a) LOG_printf a
#else
 #define TRACE_GEN(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_VERBOSE)
 #define TRACE_VERBOSE(a) LOG_printf a
#else
 #define TRACE_VERBOSE(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_DATA)
 #define TRACE_DATA(a) LOG_printf a
#else
 #define TRACE_DATA(a)
#endif

#if (CURRENT_TRACE_MASK & TRACE_MASK_TIME)
 #define TRACE_TIME(a) LOG_printf a
#else
 #define TRACE_TIME(a)
#endif

// -----------------------------------------------------------------------------


/*
 *  ======== PCE_TII_encode ========
 *  TII's implementation of the encode operation.
 */

Int 
PCE_TII_encode(IPCE_Handle handle, ALG_Handle sioHandle, PAF_EncodeInStruct *pEncodeInStruct, PAF_EncodeOutStruct *pEncodeOutStruct)
{
    PCE_TII_Obj *pce = (Void *)handle;

    PAF_AudioFrame *pAudioFrame = pEncodeInStruct->pAudioFrame;

    Int sampleCount = pAudioFrame->sampleCount;

    Int i;

    Int errno;
    
    PAF_ChannelMask_HD streamMask;

    streamMask =
        pAudioFrame->fxns->channelMask (pAudioFrame,
        pAudioFrame->channelConfigurationStream); 
 
    TRACE_GEN((&TR_MOD, "PCE_TII_encode: pAudioFrame = 0x%x, sampleCount = %d, streamMask = 0x%x",
        pAudioFrame, sampleCount, streamMask));
    TRACE_GEN((&TR_MOD, "PCE_TII_encode: pEncodeInStruct = 0x%x, pEncodeOutStruct = 0x%x",
        pEncodeInStruct, pEncodeOutStruct));

    // Implement encoding phases (nominal):
    // 1. Volume
    // 2. SLD (delay)
    // 3. Output
    // 4. Null

    pce->pActive->bitstreamMask.bits = 0;
    // copy metadata type into status so that it can be read via alpha.
    pce->pStatus->bsMetadata_type = (XDAS_Int8)pAudioFrame->bsMetadata_type;
    for (i=0; i < lengthof (handle->fxns->phase); i++) {

        TRACE_GEN((&TR_MOD, "PCE_TII_encode: phase[%d]() address = 0x%x", i, handle->fxns->phase[i]));

        if (pce->pStatus->phase[i].mode) {
            if ((!pce->pConfig->pConfigCommon[i] && 
                 handle->fxns->phase[i]) ||
                (pce->pConfig->pConfigCommon[i] &&
                 !COM_TII_activateCommon((IALG_Handle)handle, pce->pConfig->pConfigCommon[i]) &&
                 handle->fxns->phase[i])) { 
                errno = handle->fxns->phase[i](handle,
                            pEncodeInStruct, pEncodeOutStruct,
                            (IPCE_StatusPhase *)&pce->pStatus->phase[i],
                            (IPCE_ConfigPhase *)pce->pConfig->pConfigPhase[i],
                            (PAF_IALG_Common *)pce->pConfig->pConfigCommon[i],
                            (PAF_ActivePhase *)pce->pActive->pActivePhase[i],
                            (PAF_ScrachPhase *)pce->pScrach->pScrachPhase[i],
                            streamMask);
                if (errno)
                {
                    TRACE_TERSE((&TR_MOD, "PCE_TII_encode: phase[%d]() returned errno = %d", i, errno));
                    return errno;
                }
            }
        } 
        else {
            if (pce->pConfig->pConfigCommon[i])
                COM_TII_deactivateCommon((IALG_Handle)handle, pce->pConfig->pConfigCommon[i]);
        }
    }

    // Set flags for exit.

    pEncodeOutStruct->outputFlag = 0;
    pEncodeOutStruct->errorFlag = 0;
    pEncodeOutStruct->sampleCount = sampleCount;

    return 0;
}

/*
 *  ======== PCE_TII_info ========
 *  TII's implementation of the information operation.
 */
Int 
PCE_TII_info(IPCE_Handle handle, ALG_Handle sioHandle, PAF_EncodeControl *pEncodeControl, PAF_EncodeStatus *pEncodeStatus)
{
    PCE_TII_Obj *pce = (Void *)handle;

    Int i;

    for(i=0; i < IPCE_PHASES; i++) {
        if (pce->pActive->pActivePhase[i] && handle->fxns->phaseInfo[i])
            handle->fxns->phaseInfo[i](handle, 
                                 pEncodeControl, pEncodeStatus,
                                 pce->pActive->pActivePhase[i],
                                 pce->pScrach->pScrachPhase[i]); 
    }

    return 0;
}

/*
 *  ======== PCE_TII_reset ========
 *  TII's implementation of the reset operation.
 */
Int 
PCE_TII_reset(IPCE_Handle handle, ALG_Handle sioHandle, PAF_EncodeControl *pEncodeControl, PAF_EncodeStatus *pEncodeStatus)
{
    PCE_TII_Obj *pce = (Void *)handle;

    Int i;
    Int errno;

    for (i=0; i < lengthof (handle->fxns->phaseReset); i++) {
        if (pce->pStatus->phase[i].mode) {
            if ((!pce->pConfig->pConfigCommon[i] && 
                 handle->fxns->phaseReset[i]) ||
                (pce->pConfig->pConfigCommon[i] &&
                 !COM_TII_activateCommon((IALG_Handle)handle, pce->pConfig->pConfigCommon[i]) &&
                 handle->fxns->phaseReset[i])) { 
                errno = handle->fxns->phaseReset[i](handle, sioHandle,
                            pEncodeControl, pEncodeStatus,
                            (IPCE_StatusPhase *)&pce->pStatus->phase[i],
                            (IPCE_ConfigPhase *)pce->pConfig->pConfigPhase[i],
                            (PAF_IALG_Common *)pce->pConfig->pConfigCommon[i],
                            (PAF_ActivePhase *)pce->pActive->pActivePhase[i],
                            (PAF_ScrachPhase *)pce->pScrach->pScrachPhase[i]);
                if (errno)
                {
                    TRACE_TERSE((&TR_MOD, "PCE_TII_reset: phaseReset() returned errno = %d", errno));
                    return errno;
                }
            }
        } 
        else {
            if (pce->pConfig->pConfigCommon[i])
                COM_TII_deactivateCommon((IALG_Handle)handle, pce->pConfig->pConfigCommon[i]);
        }
    }

    return 0;
}

