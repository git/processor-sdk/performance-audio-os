/******************************************************************************
* Copyright (c) 2016, Texas Instruments Incorporated - http://www.ti.com
*   All rights reserved.
*
*   Redistribution and use in source and binary forms, with or without
*   modification, are permitted provided that the following conditions are met:
*       * Redistributions of source code must retain the above copyright
*         notice, this list of conditions and the following disclaimer.
*       * Redistributions in binary form must reproduce the above copyright
*         notice, this list of conditions and the following disclaimer in the
*         documentation and/or other materials provided with the distribution.
*       * Neither the name of Texas Instruments Incorporated nor the
*         names of its contributors may be used to endorse or promote products
*         derived from this software without specific prior written permission.
*
*   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
*   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
*   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
*   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
*   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
*   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
*   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
*   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
*   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
*   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
*   THE POSSIBILITY OF SUCH DAMAGE.
*****************************************************************************/

//
//
// Standard PCM algorithm interface declarations
//
//

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the PCM algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the PCM
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef PCM_
#define PCM_

#include <alg.h>
#include <ipcm.h>
#include <ti/xdais/ialg.h> //<ialg.h>
#include <paf_alg.h>

/*
 *  ======== PCM_Handle ========
 *  PCM algorithm instance handle
 */
typedef struct IPCM_Obj *PCM_Handle;

/*
 *  ======== PCM_Params ========
 *  PCM algorithm instance creation parameters
 */
typedef struct IPCM_Params PCM_Params;

/*
 *  ======== PCM_PARAMS ========
 *  Default instance parameters
 */
#define PCM_PARAMS IPCM_PARAMS

/*
 *  ======== PCM_Status ========
 *  Status structure for getting PCM instance attributes
 */
typedef volatile struct IPCM_Status PCM_Status;

/*
 *  ======== PCM_Cmd ========
 *  This typedef defines the control commands PCM objects
 */
typedef IPCM_Cmd   PCM_Cmd;

/*
 * ===== control method commands =====
 */
#define PCM_NULL		IPCM_NULL
#define PCM_GETSTATUSADDRESS1	IPCM_GETSTATUSADDRESS1
#define PCM_GETSTATUSADDRESS2	IPCM_GETSTATUSADDRESS2
#define PCM_GETSTATUS		IPCM_GETSTATUS
#define PCM_SETSTATUS		IPCM_SETSTATUS
#define PCM_MINSAMGEN		IPCM_MINSAMGEN
#define PCM_MININFO		IPCM_MININFO
#define PCM_MAXSAMGEN		IPCM_MAXSAMGEN

/*
 *  ======== PCM_create ========
 *  Create an instance of a PCM object.
 */
static inline PCM_Handle PCM_create(const IPCM_Fxns *fxns, const PCM_Params *prms)
{
    return ((PCM_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== PCM_decode ========
 */
extern Int PCM_decode(PCM_Handle, ALG_Handle, PAF_DecodeInStruct *, PAF_DecodeOutStruct *);

/*
 *  ======== PCM_delete ========
 *  Delete a PCM instance object
 */
static inline Void PCM_delete(PCM_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== PCM_exit ========
 *  Module finalization
 */
extern Void PCM_exit(Void);

/*
 *  ======== PCM_info ========
 */
extern Int PCM_info(PCM_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);

/*
 *  ======== PCM_init ========
 *  Module initialization
 */
extern Void PCM_init(Void);

/*
 *  ======== PCM_reset ========
 */
extern Int PCM_reset(PCM_Handle, ALG_Handle, PAF_DecodeControl *, PAF_DecodeStatus *);

#endif  /* PCM_ */
