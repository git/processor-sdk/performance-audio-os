
/*
* Copyright (C) 2004-2014 Texas Instruments Incorporated - http://www.ti.com/
* All rights reserved.	
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

//
//
// Standard One-to-Many Audio Mixer Algorithm interface declarations
//
//
//

/*
 *  This header defines all types, constants, and functions used by 
 *  applications that use the AMIX Algorithm.
 *
 *  Applications that use this interface enjoy type safety and
 *  the ability to incorporate multiple implementations of the AMIX
 *  algorithm in a single application at the expense of some
 *  additional indirection.
 */
#ifndef AMIX_
#define AMIX_

#include <alg.h>
#include <ialg.h>
#include <paf_alg.h>

#include "paftyp.h"
#include "iamix.h"

#if (PAF_MAXNUMCHAN != 16)
  #error "AMIX is only tested for PAF_MAXNUMCHAN == 16"
#endif

// modes of operation.
#define AMIX_MODE_BYPASS                0
#define AMIX_MODE_MIX_WITH_CHANNEL_0    1   // mix streams 0 and 1 into stream 0
#define AMIX_MODE_MIX_OVER_CHANNEL_0    2   // mix streams 1 and 2 into stream 0


// The gain matrix is 16 bit integers specifying half dB.  Valid gain ranges are +24 down to -240.
// These are defined as MAX_GAIN and MIN_GAIN.
#define MAX_GAIN     24
#define MIN_GAIN    -240


/*
 *  ======== AMIX_Handle ========
 *  One-to-Many Audio Mixer Algorithm instance handle
 */
typedef struct IAMIX_Obj *AMIX_Handle;

/*
 *  ======== AMIX_Params ========
 *  One-to-Many Audio Mixer Algorithm instance creation parameters
 */
typedef struct IAMIX_Params AMIX_Params;

/*
 *  ======== AMIX_PARAMS ========
 *  Default instance parameters
 */
#define AMIX_PARAMS IAMIX_PARAMS

/*
 *  ======== AMIX_Status ========
 *  Status structure for getting AMIX instance attributes
 */
typedef volatile struct IAMIX_Status AMIX_Status;

/*
 *  ======== AMIX_Cmd ========
 *  This typedef defines the control commands AMIX objects
 */
typedef IAMIX_Cmd   AMIX_Cmd;

/*
 * ===== control method commands =====
 */
#define AMIX_NULL IAMIX_NULL
#define AMIX_GETSTATUSADDRESS1 IAMIX_GETSTATUSADDRESS1
#define AMIX_GETSTATUSADDRESS2 IAMIX_GETSTATUSADDRESS2
#define AMIX_GETSTATUS IAMIX_GETSTATUS
#define AMIX_SETSTATUS IAMIX_SETSTATUS

/*
 *  ======== AMIX_create ========
 *  Create an instance of a AMIX object.
 */
static inline AMIX_Handle AMIX_create(const IAMIX_Fxns *fxns, const AMIX_Params *prms)
{
    return ((AMIX_Handle)PAF_ALG_create((IALG_Fxns *)fxns, NULL, (IALG_Params *)prms,NULL,NULL));
}

/*
 *  ======== AMIX_delete ========
 *  Delete a AMIX instance object
 */
static inline Void AMIX_delete(AMIX_Handle handle)
{
    PAF_ALG_delete((ALG_Handle)handle);
}

/*
 *  ======== AMIX_apply ========
 */
extern Int AMIX_apply(AMIX_Handle, PAF_AudioFrame *);

/*
 *  ======== AMIX_reset ========
 */
extern Int AMIX_reset(AMIX_Handle, PAF_AudioFrame *);

/*
 *  ======== AMIX_exit ========
 *  Module finalization
 */
extern Void AMIX_exit(Void);

/*
 *  ======== AMIX_init ========
 *  Module initialization
 */
extern Void AMIX_init(Void);

#endif  /* AMIX_ */
