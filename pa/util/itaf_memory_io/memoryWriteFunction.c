#include "memoryFunctions.h"

unsigned int pADDRWriteIndex = 0;
unsigned int pADDRWriteMaxIndex = 0;

/* Base address of memory write pointer */
unsigned char *ADDRWRITE = (unsigned char *)0xD0000000;

/* Initialization required to use memory write modules */
void mWriteInit()
{
    pADDRWriteIndex = 0;
    pADDRWriteMaxIndex = 0;
    memset(ADDRWRITE, 0xDD, (30*1024*1024));
}

/* Equivalent to  C library function fwrite */
/* Instead of writing to file write predefined memory address */
size_t mWrite(void *ptr, size_t size, size_t count,FILE* fp)
{
        memcpy(ADDRWRITE+pADDRWriteIndex,ptr, size*count);
        pADDRWriteIndex += (size*count);
        if(pADDRWriteMaxIndex < pADDRWriteIndex)
        {
          pADDRWriteMaxIndex = pADDRWriteIndex;
        }
        return (count);
}

/* Equivalent to  C library function fsetpos */
/* Insted of aligning FIlE Pointer function align memory index */
int mWriteSetPos(FILE *fp, const fpos_t *pos)
{
    pADDRWriteIndex = *pos;
    if(pADDRWriteMaxIndex < pADDRWriteIndex)
    {
      pADDRWriteMaxIndex = pADDRWriteIndex;
    }
    return 0;
}

/* Equivalent to  C library function fgetpos */
/* Insted of reading FIlE Pointer position reads memory address index*/
int mWriteGetPos(FILE *fp, fpos_t *pos)
{
    *pos = pADDRWriteIndex;
    return 0;
}

/* Equivalent to  C library function fgetpos */
/* Align memory address index */
size_t mWriteSeek(FILE* fp,  long offset, int whence)
{
        switch(whence)
        {
        case SEEK_SET:
            pADDRWriteIndex = 0;
            pADDRWriteIndex += offset;
            if(pADDRWriteMaxIndex < pADDRWriteIndex)
            {
              pADDRWriteMaxIndex = pADDRWriteIndex;
            }
            break;
        case SEEK_CUR:
            pADDRWriteIndex += offset;
            if(pADDRWriteMaxIndex < pADDRWriteIndex)
            {
              pADDRWriteMaxIndex = pADDRWriteIndex;
            }
            break;
        case SEEK_END:
            pADDRWriteIndex = pADDRWriteMaxIndex;
            pADDRWriteIndex += offset;
            if(pADDRWriteMaxIndex < pADDRWriteIndex)
            {
              pADDRWriteMaxIndex = pADDRWriteIndex;
            }
            break;
        default: break;
        }
        return 0;        
}
